#!/usr/bin/env python3

import numpy as np
from scipy.spatial.transform import Rotation as R
import rospy 

from std_msgs.msg import Float32 as f32
from std_msgs.msg import Float32MultiArray as f32A
from geometry_msgs.msg import PoseStamped as PS
from pcc_estimator.msg import estimation_message
class Estimator:
    def __init__(self):
        self.length1=np.array([0.0])
        self.length2 = np.array([0.0])
        self.zedOrientation=np.array([0])
        self.zedAccel=np.array([0])
        self.zedAngVel=np.array([0])
        self.h = rospy.get_param("/height")#Check .yaml for units and values 
        self.r = rospy.get_param("/radius")
        self.M0 = rospy.get_param("/M0")
        self.M0 = np.array(self.M0)
        #TODO: Check that M0 is the right array
        self.Moffset = rospy.get_param("Moffset")
        self.Moffset = np.array(self.Moffset)
        #TODO: Check that Moffset is an array
        self.JOINT_IS_STRAIGHT_ANGLE_THRESHOLD = rospy.get_param("/JOINT_IS_STRAIGHT_ANGLE_THRESHOLD") #rads 
        self.JOINT_IS_STRAIGHT_LENGTH_THRESHOLD = rospy.get_param("/JOINT_IS_STRAIGHT_LENGTH_THRESHOLD") #in
        self.Alpha1=0
        self.Alpha2=0
        self.Theta=0
        self.IP=0
        self.ZEDFACINGBACKWARDS = rospy.get_param("/INVERT_ZED_ORIENTATION")
        self.initialized = np.array([0,0,0]) #Used to make sure that we've received data on both orientaton and length before we start estimating.
        
        #Set Up subscribers that will listen for updated measurements
        rospy.init_node("Estimator Node",anonymous=True)
        self.zedsub = rospy.Subscriber("zedm/zed_node/pose", PS, callback = self.setOrientation)
        self.length1sub = rospy.Subscriber("length1",f32, callback = self.setLength1)
        self.length2sub = rospy.Subscriber("length2", f32, callback = self.setLength2)
        #Set Up Publisher to report estimation
        self.pub_rate = rospy.Rate(100)
        self.pub = rospy.Publisher("Estimation",estimation_message,queue_size=100)
        
        
        
    
    def estimate(self,SHOW=0,FAILEDIPMEANSCONSTANT=1):
            #M1 and M2 correspond with Theta, and M3 and M4 correspond with Psi
            print("Here")
            try:
                
                M = np.array([self.length1,self.length2])
                self.Theta = -(self.zedOrientation[0]) #Positive Theta is rotation towards the strings, negative is defined away. (I think)
                
                self.IP = (M[0]*self.h**2- self.M0[0]*self.h**2-M[0]*self.M0[1]*self.h+self.M0[0]*self.M0[1]*self.h+self.M0[0]*self.M0[1]*self.Theta*self.r - self.M0[0]*self.Theta*self.h*self.r)/(M[0]*self.h**2-M[1]*self.h**2-self.M0[0]*self.h**2+self.M0[1]*self.h**2+M[1]*self.M0[0]*self.h-M[0]*self.M0[1]*self.h-self.M0[0]*self.Theta*self.h*self.r+self.M0[1]*self.Theta*self.h*self.r)
                            #IP2 =  (M[2]*self.h**2- self.M0[2]*self.h**2-M[2]*self.M0[3]*self.h+self.M0[2]*self.M0[3]*self.h+self.M0[2]*self.M0[3]*Theta*self.r - self.M0[2]*Theta*self.h*self.r)/(M[2]*self.h**2-M[3]*self.h**2-self.M0[2]*self.h**2+self.M0[3]*self.h**2+M[3]*self.M0[2]*self.h-M[2]*self.M0[3]*self.h-self.M0[2]*Theta*self.h*self.r+self.M0[3]*Theta*self.h*self.r)
                self.Alpha1 = -(self.M0[0]*self.Theta**2*self.r**2 + M[0]*M[1]*self.h -M[1]*self.M0[0]*self.h - M[0]*self.M0[1]*self.h + self.M0[0]*self.M0[1]*self.h -M[1]*self.M0[0]*self.Theta*self.r + self.M0[0]*self.M0[1]*self.Theta*self.r - M[0]*self.Theta*self.h*self.r + self.M0[0]*self.Theta*self.h*self.r)/(self.r*(M[0]*self.h-M[1]*self.h - self.M0[0]*self.h + self.M0[1]*self.h + M[1]*self.M0[0] - M[0]*self.M0[1] - self.M0[0]*self.Theta*self.r + self.M0[1]*self.Theta*self.r))
                self.Alpha2 = self.Theta-self.Alpha1
                
                if self.IP<0:
                    print("NEGATIVE")
            
                TestAlpha2 = self.Alpha2+self.Alpha1
                if abs(TestAlpha2) < self.JOINT_IS_STRAIGHT_ANGLE_THRESHOLD and abs(self.M0[0]-M[0])<self.JOINT_IS_STRAIGHT_LENGTH_THRESHOLD :
                    self.IP=0
                    self.Alpha1=0
                    self.Alpha2=0
                if FAILEDIPMEANSCONSTANT and (abs(self.IP)-1>0 or self.IP<0):#abs((self.M0[0]-M[0])/self.M0[0]-(self.M0[1]-M[1])/self.M0[1])<self.NO_INFLECTION_THRESHOLD:
                    self.IP = 1000
                    self.Alpha1 = 1000
                    self.Alpha2 = 1000
                #TestAlpha22 = Psi-Alpha22
                if SHOW: #TODO IMPLEMENT VISUALIZATION
                    print(("IP : {}").format(self.IP))
                    print(("Alpha1 : {}").format(self.Alpha1))
                    print(("Alpha 12 : {}").format(self.Alpha2))
                    print(("TestAlpha2 : {}").format(TestAlpha2)) 
                    pass
            except:
                #print("Estimation Failed, Still initializing?")
                pass
            msg = estimation_message()
            msg.IP = self.IP
            msg.Alpha1 = self.Alpha1
            msg.Alpha2 = self.Alpha2
            return(msg)
    def setOrientation(self,data):
        orientation = data.pose.orientation
        
        rot = R.from_quat([orientation.x,orientation.y,orientation.z,orientation.w])
        rot = rot.as_euler('ZYX')
        self.zedOrientation = rot[0]
        #print(self.zedOrientation)
        self.initialized[0] = 1
    def setLength1(self,data):
        self.length1 = data.data
        self.initialized[1] = 1
    def setLength2(self,data):
        self.length2 = data.data
        self.initialized[2] = 1 
if __name__ == '__main__':
    estim = Estimator()
    
    try:
        
        while not rospy.is_shutdown():
            if np.sum(estim.initialized)>2:
                
                estim.pub.publish(estim.estimate())
                estim.pub_rate.sleep()
    except rospy.ROSInterruptException:
        pass
            