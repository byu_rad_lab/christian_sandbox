#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import rospy

from std_msgs.msg import Float32 as f32
from std_msgs.msg import Float32MultiArray as f32A
from geometry_msgs.msg import PoseStamped as PS
from pcc_estimator.msg import estimation_message
from pcc_estimator.msg import validation_message
class Plotter:
    def __init__(self):
        #Measurements and parameters for drawing
        self.StartEndOffset = .25
        self.length1=np.array([0.0])
        self.length2 = np.array([0.0])
        self.zedOrientation=np.array([0])
        self.zedAccel=np.array([0])
        self.zedAngVel=np.array([0])
        self.h = rospy.get_param("/height")#Check .yaml for units and values 
        self.r = rospy.get_param("/radius")
        self.M0 = rospy.get_param("/M0")
        self.M0 = np.array(self.M0)
        #TODO: Check that M0 is the right array
        self.Moffset = rospy.get_param("Moffset")
        self.Moffset = np.array(self.Moffset)
        #TODO: Check that Moffset is an array
        self.JOINT_IS_STRAIGHT_ANGLE_THRESHOLD = rospy.get_param("/JOINT_IS_STRAIGHT_ANGLE_THRESHOLD") #rads 
        self.JOINT_IS_STRAIGHT_LENGTH_THRESHOLD = rospy.get_param("/JOINT_IS_STRAIGHT_LENGTH_THRESHOLD") #in
        self.Alpha1=0
        self.Alpha2=0
        self.Theta=0
        self.IP=0
        self.ZEDFACINGBACKWARDS = rospy.get_param("/INVERT_ZED_ORIENTATION")
        self.initialized = 0
        self.msg =validation_message()
        self.GroundTruthArray = np.array([])
        rospy.init_node('PlotterNode',anonymous=True)
        
        self.plot_sub = rospy.Subscriber("Estimation",estimation_message,callback = self.setEstimationParams)
        self.plot_pub =  rospy.Publisher("ValidationData",validation_message,queue_size=100)
        #Plot Objects
        self.fig = plt.figure(figsize=(9,7))
        self.steps = 50 #Number of steps to break curves into (for plotting)
        self.ax = self.fig.add_subplot(autoscale_on=False, xlim=(-self.h, self.h), ylim=(-2,self.h*1.5))
        self.ax.set_aspect('equal')
        self.ax.grid()
        self.labels = self.ax.text(-5,10,[])
        self.Left_Line_1,=self.ax.plot([],[],'r')
        self.Left_Line_2,=self.ax.plot([],[],'r')
        self.Right_Line_1,=self.ax.plot([],[],'b')
        self.Right_Line_2,=self.ax.plot([],[],'b')
        self.Center_Line_1,=self.ax.plot([],[],'ko',markersize=.9)
        self.Center_Line_2, = self.ax.plot([],[],'ko',markersize=.9)
        self.Base_Line_1, = self.ax.plot([],[],'k')
        self.Base_Line_2, = self.ax.plot([],[],'y')
        self.End_Line, = self.ax.plot([],[],'g')
        self.Start_Marker, = self.ax.plot([],[],'k*',markersize=.9)
        self.End_Marker, = self.ax.plot([],[],'k*',markersize=.9)
        self.CenterlineMarkers = 20 # Number of markers to put along the length of the whole centerline
        self.CortexMarkers, = self.ax.plot([],[],'r*',markersize=.9)
        
        #self.pub_rate = rospy.Rate(10)
        
    def setEstimationParams(self,data):
        self.IP = data.IP
        self.Alpha1 = data.Alpha1
        self.Alpha2 = data.Alpha2
        self.Theta = self.Alpha1+self.Alpha2
        self.initialized = 1
    def drawRobo(self,test=0,RECORD=0):
        # if test:
        #     self.Alpha1=-.01
        #     self.Alpha2= -.8
        #     self.IP = 1000
        #     self.arduinoData[0]=9
        #     self.Theta=self.Alpha1+self.Alpha2
        if self.IP == 1000:
            base_x = np.array(list(range(-1,2)))*self.r
            base_y = base_x*0
            Center_line_r = self.h/self.Theta
            if np.sign(self.Theta)>0:
                Left_line_r = Center_line_r-self.r
                
                Right_line_r = Center_line_r+self.r
                center = np.array([-Center_line_r,0])
                thetasteps= np.array(list(range(0,self.steps)))*abs(self.Theta/(self.steps-1))
                Left_line_x = np.cos(thetasteps)*Left_line_r + center[0]
                Left_line_y = np.sin(thetasteps)*Left_line_r + center[1]
                Right_line_x = np.cos(thetasteps)*Right_line_r + center[0]
                Right_line_y = np.sin(thetasteps)*Right_line_r + center[1]
                Center_line_x = np.cos(thetasteps)*Center_line_r + center[0]
                Center_line_y = np.sin(thetasteps)*Center_line_r + center[1]

            else:

                Left_line_r = Center_line_r+self.r
                Right_line_r = Center_line_r-self.r
                center = np.array([Center_line_r,0])
                thetasteps= np.array(list(range(0,self.steps)))*abs(self.Theta/(self.steps-1))-np.pi
                Left_line_x = -(np.cos(thetasteps)*Left_line_r + center[0])
                Left_line_y = (np.sin(thetasteps)*Left_line_r + center[1])
                Right_line_x = -(np.cos(thetasteps)*Right_line_r + center[0])
                Right_line_y = (np.sin(thetasteps)*Right_line_r + center[1])
                Center_line_x = -(np.cos(thetasteps)*Center_line_r + center[0])
                Center_line_y = (np.sin(thetasteps)*Center_line_r + center[1])
            
            translation_x = Center_line_x[-1]
            translation_y = Center_line_y[-1]

            end_line_x = base_x
            end_line_y = base_y
            end_line_x_rot_transl = (np.cos(self.Theta)*end_line_x-np.sin(self.Theta)*end_line_y)+translation_x
            end_line_y_rot_transl = (np.sin(self.Theta)*end_line_x+np.cos(self.Theta)*end_line_y)+translation_y
            # plt.plot(base_x,base_y,'k-')
            # plt.plot(Left_line_x,Left_line_y,'r-')
            # plt.plot(Right_line_x,Right_line_y,'b-')
            # plt.plot(end_line_x_rot_transl,end_line_y_rot_transl,'y-')
            # plt.show()
            #Reset all of drawing variables to be empty
            self.Left_Line_1.set_data([],[])
            self.Left_Line_2.set_data([],[])
            self.Right_Line_1.set_data([],[])
            self.Right_Line_2.set_data([],[])
            self.Center_Line_1.set_data([],[])
            self.Center_Line_2.set_data([],[])
            self.Base_Line_1.set_data([],[])
            self.Base_Line_2.set_data([],[])
            self.End_Line.set_data([],[])
            self.Start_Marker.set_data([],[])
            self.End_Marker.set_data([],[])
            #Update just the ones we need to draw for this set
            self.labels.set_text('IP : {} , Alpha1 : {}, Alpha2 : {}'.format(self.IP,self.Alpha1,self.Alpha2,".4f"))
            self.Left_Line_1.set_data(Left_line_x,Left_line_y)
            self.Right_Line_1.set_data(Right_line_x,Right_line_y)
            self.Base_Line_1.set_data(base_x,base_y)
            self.End_Line.set_data(end_line_x_rot_transl,end_line_y_rot_transl)
            self.Start_Marker.set_data(0,-self.StartEndOffset)
            self.End_Marker.set_data(Center_line_x[-1]+np.cos(self.Theta+np.pi/2)*self.StartEndOffset,Center_line_y[-1]+np.sin(self.Theta+np.pi/2)*self.StartEndOffset)
            skip1 =int((self.steps)/(self.CenterlineMarkers))
            self.Center_Line_1.set_data(Center_line_x[0:-1:skip1],Center_line_y[0:-1:skip1])
            plt.pause(.01)

            return 'CC'

            
        elif self.IP == 0:
            base_x = np.array(list(range(-1,2)))*self.r
            base_y = base_x*0
            Left_line_x = np.array([-self.r,-self.r])
            Left_line_y = np.array([0,self.h])
            Right_line_x = np.array([self.r,self.r])
            Right_line_y = np.array([0,self.h])
            end_line_x = base_x
            end_line_y = base_y + self.h
            centerline_y = np.linspace(0,self.h,self.CenterlineMarkers)
            centerline_x = centerline_y*0
            # plt.plot(base_x,base_y,'k-')
            # plt.plot(Left_line_x,Left_line_y,'r-')
            # plt.plot(Right_line_x,Right_line_y,'b-')
            # plt.plot(end_line_x,end_line_y,'y-')
            # plt.show()

            self.Left_Line_1.set_data([],[])
            self.Left_Line_2.set_data([],[])
            self.Right_Line_1.set_data([],[])
            self.Right_Line_2.set_data([],[])
            self.Center_Line_1.set_data([],[])
            self.Center_Line_2.set_data([],[])
            self.Base_Line_1.set_data([],[])
            self.Base_Line_2.set_data([],[])
            self.End_Line.set_data([],[])
            self.Start_Marker.set_data([],[])
            self.End_Marker.set_data([],[])

            self.labels.set_text('IP : {} , Alpha1 : {}, Alpha2 : {}'.format(self.IP,self.Alpha1,self.Alpha2,".4f"))
            self.Left_Line_1.set_data(Left_line_x,Left_line_y)
            self.Right_Line_1.set_data(Right_line_x,Right_line_y)
            self.Base_Line_1.set_data(base_x,base_y)
            self.End_Line.set_data(end_line_x,end_line_y)
            self.Start_Marker.set_data(0,-self.StartEndOffset)
            self.End_Marker.set_data(0,base_y[1]+self.h+self.StartEndOffset)
            self.Center_Line_1.set_data(centerline_x,centerline_y)

            plt.pause(.01)

            return 'Straight'
        else:
            if np.sign(self.Alpha1)>0:                          # Sign of Alpha1 determines on which side of the robot the center of rotation is.  Positive its towards strings, Negative its away from the strings.
                base_x= np.array(list(range(-1,2)))*self.r# For drawing the BaseLine
                base_y= base_x*0 # For drawing the BaseLine
                center_r_base= self.IP*self.h/self.Alpha1 #r=S/theta. Negative makes it so positive rotation is in the direction of the strings (left in the graph)
                center_base = np.array([-center_r_base,0])  #First seg will be defined as flat so the center has only x.
                thetasteps= np.array(list(range(0,self.steps)))*self.Alpha1/(self.steps-1) #Break the arcs into 50 steps

                centerline_x = center_r_base*np.cos(thetasteps)+center_base[0] #For drawing the centerline of the first seg
                centerline_y = center_r_base*np.sin(thetasteps)+center_base[1] #For drawing the centerline of the first seg

                Left_line_r = (center_r_base-self.r)               #Radius subtracts here because center of rotation is on the same side as the strings
                Left_line_x = Left_line_r*np.cos(thetasteps)+center_base[0]
                Left_line_y = Left_line_r*np.sin(thetasteps)+center_base[1]

                Right_line_r = (center_r_base+self.r)               #Radius subtracts here because center of rotation is on the opposite side as the strings
                Right_line_x = Right_line_r*np.cos(thetasteps)+center_base[0]
                Right_line_y = Right_line_r*np.sin(thetasteps)+center_base[1]
            else:
                base_x= np.array(list(range(-1,2)))*self.r# For drawing the BaseLine
                base_y= base_x*0 # For drawing the BaseLine
                center_r_base=-self.IP*self.h/self.Alpha1 #r=S/theta.Positive means the first seg is bent away from the strings (right in the graph)
                center_base = np.array([center_r_base,0])  #First seg will be defined as flat so the center has only x.
                thetasteps= np.array(list(range(0,self.steps)))*abs(self.Alpha1/(self.steps-1))+-np.pi #Break the arcs into 50 steps

                centerline_x = center_r_base*np.cos(thetasteps)+center_base[0] #For drawing the centerline of the first seg
                centerline_y = -center_r_base*np.sin(thetasteps)+center_base[1] # Y is negative for all of these because it makes it right...

                Left_line_r = (center_r_base+self.r)           #Radius adds here because center of rotation is on the side opposite the strings
                Left_line_x = Left_line_r*np.cos(thetasteps)+center_base[0]
                Left_line_y = - Left_line_r*np.sin(thetasteps)+center_base[1] #Negative sign flips the plot so it looks right...

                Right_line_r = (center_r_base-self.r)          #Radius subtracts here because center of rotation is on the same side as the strings
                Right_line_x = Right_line_r*np.cos(thetasteps)+center_base[0]
                Right_line_y = -Right_line_r*np.sin(thetasteps)+center_base[1] #Negative sign flips the plot so it looks right...

            if np.sign(self.Alpha2)>0: #Repeat again for the second segment, we calculate what all the points should be if it were located at the origin, and then rotate them by Alpha1 and translate them so the new origin is the end of the last curve.
                translation = np.array([centerline_x[-1], centerline_y[-1]])
                
                upper_x = np.array(list(range(-1,2)))*self.r# For drawing the UpperLine
                upper_y = upper_x*0 # For drawing the UpperLine        
                upper_x_rot_transl = (np.cos(self.Alpha1)*upper_x-np.sin(self.Alpha1)*upper_y)+translation[0]
                upper_y_rot_transl = (np.sin(self.Alpha1)*upper_x+np.cos(self.Alpha1)*upper_y)+translation[1]
                
                center_r_upper=(1-self.IP)*self.h/self.Alpha2 #r=S/theta. Negative makes it so positive rotation is in the direction of the strings (left in the graph)
                center_upper= np.array([-center_r_upper,0])
                center_upper_rot_transl = np.array([[np.cos(self.Alpha1), -np.sin(self.Alpha1)],[np.sin(self.Alpha1),np.cos(self.Alpha1)]])@center_upper + translation  #First seg will be defined as flat so the center has only x.
                
                thetasteps = np.array(list(range(0,self.steps)))*self.Alpha2/(self.steps-1) #Break the arcs into 50 steps

                centerline_x_2 = center_r_upper*np.cos(thetasteps)+center_upper[0] #For drawing the centerline of the second seg
                centerline_y_2 = center_r_upper*np.sin(thetasteps)+center_upper[1] #For drawing the centerline of the second seg

                centerline_x_rot_transl = (np.cos(self.Alpha1)*centerline_x_2-np.sin(self.Alpha1)*centerline_y_2)+translation[0]
                centerline_y_rot_transl = (np.sin(self.Alpha1)*centerline_x_2+np.cos(self.Alpha1)*centerline_y_2)+translation[1]

                Left_line_r_2 = (center_r_upper-self.r)               #Radius subtracts here because center of rotation is on the same side as the strings
                Left_line_x_2 = Left_line_r_2*np.cos(thetasteps)+center_upper[0]
                Left_line_y_2 = Left_line_r_2*np.sin(thetasteps)+center_upper[1]
                Left_line_x_rot_transl = (np.cos(self.Alpha1)*Left_line_x_2-np.sin(self.Alpha1)*Left_line_y_2)+translation[0]
                Left_line_y_rot_transl = (np.sin(self.Alpha1)*Left_line_x_2+np.cos(self.Alpha1)*Left_line_y_2)+translation[1]


                Right_line_r_2 = (center_r_upper+self.r)               #Radius subtracts here because center of rotation is on the opposite side as the strings
                Right_line_x_2 = Right_line_r_2*np.cos(thetasteps)+center_upper[0]
                Right_line_y_2 = Right_line_r_2*np.sin(thetasteps)+center_upper[1]
                Right_line_x_rot_transl = (np.cos(self.Alpha1)*Right_line_x_2-np.sin(self.Alpha1)*Right_line_y_2)+translation[0]
                Right_line_y_rot_transl = (np.sin(self.Alpha1)*Right_line_x_2+np.cos(self.Alpha1)*Right_line_y_2)+translation[1]



            else:
                translation = np.array([centerline_x[-1], centerline_y[-1]])
                
                upper_x = np.array(list(range(-1,2)))*self.r# For drawing the UpperLine
                upper_y = upper_x*0 # For drawing the UpperLine        
                upper_x_rot_transl = (np.cos(self.Alpha1)*upper_x-np.sin(self.Alpha1)*upper_y)+translation[0]
                upper_y_rot_transl = (np.sin(self.Alpha1)*upper_x+np.cos(self.Alpha1)*upper_y)+translation[1]
                
                center_r_upper=-(1-self.IP)*self.h/self.Alpha2 #r=S/theta. Negative makes it so positive rotation is in the direction of the strings (left in the graph)
                center_upper= np.array([center_r_upper,0])
                center_upper_rot_transl = np.array([[np.cos(self.Alpha1), -np.sin(self.Alpha1)],[np.sin(self.Alpha1),np.cos(self.Alpha1)]])@center_upper + translation  #First seg will be defined as flat so the center has only x.
                
                thetasteps = np.array(list(range(0,self.steps)))*abs(self.Alpha2/(self.steps-1))+-np.pi #Break the arcs into 50 steps

                centerline_x_2 = center_r_upper*np.cos(thetasteps)+center_upper[0] #For drawing the centerline of the second seg
                centerline_y_2 = -center_r_upper*np.sin(thetasteps)+center_upper[1] #For drawing the centerline of the second seg

                centerline_x_rot_transl = (np.cos(self.Alpha1)*centerline_x_2-np.sin(self.Alpha1)*centerline_y_2)+translation[0]
                centerline_y_rot_transl = (np.sin(self.Alpha1)*centerline_x_2+np.cos(self.Alpha1)*centerline_y_2)+translation[1]

                Left_line_r_2 = (center_r_upper+self.r)               #Radius subtracts here because center of rotation is on the same side as the strings
                Left_line_x_2 = Left_line_r_2*np.cos(thetasteps)+center_upper[0]
                Left_line_y_2 = -Left_line_r_2*np.sin(thetasteps)+center_upper[1]
                Left_line_x_rot_transl = (np.cos(self.Alpha1)*Left_line_x_2-np.sin(self.Alpha1)*Left_line_y_2)+translation[0]
                Left_line_y_rot_transl = (np.sin(self.Alpha1)*Left_line_x_2+np.cos(self.Alpha1)*Left_line_y_2)+translation[1]


                Right_line_r_2 = (center_r_upper-self.r)               #Radius subtracts here because center of rotation is on the opposite side as the strings
                Right_line_x_2 = Right_line_r_2*np.cos(thetasteps)+center_upper[0]
                Right_line_y_2 = -Right_line_r_2*np.sin(thetasteps)+center_upper[1]
                Right_line_x_rot_transl = (np.cos(self.Alpha1)*Right_line_x_2-np.sin(self.Alpha1)*Right_line_y_2)+translation[0]
                Right_line_y_rot_transl = (np.sin(self.Alpha1)*Right_line_x_2+np.cos(self.Alpha1)*Right_line_y_2)+translation[1]
            translation=np.array([centerline_x_rot_transl[-1],centerline_y_rot_transl[-1]])
            end_line_x=base_x
            end_line_y=base_y
            end_line_x_rot_transl = (np.cos(self.Theta)*end_line_x-np.sin(self.Theta)*end_line_y)+translation[0]
            end_line_y_rot_transl = (np.sin(self.Theta)*end_line_x+np.cos(self.Theta)*end_line_y)+translation[1]

            # plt.plot(base_x,base_y,'k')
            # plt.plot(upper_x_rot_transl,upper_y_rot_transl,'k')
            # plt.plot(Left_line_x,Left_line_y,'b')
            # plt.plot(Left_line_x_rot_transl,Left_line_y_rot_transl,'b')
            # plt.plot(Right_line_x,Right_line_y,'r')
            # plt.plot(Right_line_x_rot_transl,Right_line_y_rot_transl,'r')
            # plt.plot(end_line_x_rot_transl,end_line_y_rot_transl,'k')
            # plt.show()

            self.Left_Line_1.set_data([],[])
            self.Left_Line_2.set_data([],[])
            self.Right_Line_1.set_data([],[])
            self.Right_Line_2.set_data([],[])
            self.Center_Line_1.set_data([],[])
            self.Center_Line_2.set_data([],[])
            self.Base_Line_1.set_data([],[])
            self.Base_Line_2.set_data([],[])
            self.End_Line.set_data([],[])
            self.Start_Marker.set_data([],[])
            self.End_Marker.set_data([],[])

            self.labels.set_text('IP : {:.4f} , Alpha1 : {:.4f}, Alpha2 : {:.4f}'.format(self.IP,self.Alpha1,self.Alpha2,".4f"))
            self.Left_Line_1.set_data(end_line_x_rot_transl[-2]+np.cos(self.Theta+np.pi/2)*self.StartEndOffset,end_line_y_rot_transl[-2]+np.sin(self.Theta+np.pi/2)*self.StartEndOffset)
            skip1 =int((self.steps)/(self.CenterlineMarkers*self.IP))
            skip2 = int((self.steps)/(self.CenterlineMarkers*(1-self.IP)))
            self.Center_Line_1.set_data(centerline_x[0:-1:skip1],centerline_y[0:-1:skip1])
            self.Center_Line_2.set_data(centerline_x_rot_transl[0:-1:skip2],centerline_y_rot_transl[0:-1:skip2])
            
            #TODO: setup self.msg =
            
            self.msg.CortexPointsArray = self.GroundTruthArray
            self.msg.EstimationPointsArray = np.append(self.Center_Line_1,self.Center_Line_2)
            self.msg.Alpha1 = self.Alpha1
            self.msg.Alpha2 = self.Alpha2
            self.msg.IP = self.IP
            plt.pause(.01)

if __name__ == '__main__':
    plotter= Plotter()
    try:
        while not rospy.is_shutdown():
            if plotter.initialized:
                plotter.drawRobo()
                plotter.plot_pub.publish(plotter.msg)
                #plotter.pub_rate.sleep()
    except rospy.ROSInterruptException:
        pass
            