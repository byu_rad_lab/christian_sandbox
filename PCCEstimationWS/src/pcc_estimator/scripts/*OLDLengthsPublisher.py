#!/usr/bin/env python3

import rospy
import numpy as np
from std_msgs.msg import Float32MultiArray as f32A
import serial

def LengthPublisher():
    arduino = serial.Serial(port='COM3', baudrate=9600, timeout=.1)
    pub = rospy.Publisher('LengthPub',f32A, queue_size = 100)
    rospy.init_node('LengthNode', anonymous=True)
    rate = rospy.Rate(100)
    arduinoData=np.array([0.0,0.0,0.0,0.0])
    
    while not rospy.is_shutdown():
        serialData=[] 
        serialData = arduino.readline()
        serialData = serialData.decode("utf-8")
        try: 
            i=int(serialData[0])
            arduinoData[i]=float(serialData[2:8])/25.4-Moffset[i]
        except: #Not needed could pass here but more readable.  Try to assign to the ith element of the vector, if it fails then self.arduinoData doesn't change.
            arduinoData=arduinoData
        #TODO: READ THE VALUES IN HERE AND PUT THEM INTO THE MESSAGE
        Length_array = arduinoData
        layout = "L1,L2,L3,L4"
        pub.publish(layout,Length_array)
        print('Published')
if __name__=='__main__':
    try:
        LengthPublisher()
    except rospy.ROSInterruptException:
        pass