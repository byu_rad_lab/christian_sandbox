#!/usr/bin/env python3

import serial
import time
import numpy as np
import cv2 as cv

import pyzed.sl as sl
from scipy.spatial.transform import Rotation as R
from sympy import Q
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import datetime

import rospy
import numpy as np
from std_msgs.msg import Float32MultiArray as f32A
import serial
class stringPotZEDm:
    def __init__(self, h=None,r=None, M0=None, Moffset=None):
        self.arduino = serial.Serial(port='COM3', baudrate=9600, timeout=.1)
        self.zed = sl.Camera()
        self.init_params = sl.InitParameters()
        self.init_params.depth_mode = sl.DEPTH_MODE.NONE
        err = self.zed.open(self.init_params)
        if err != sl.ERROR_CODE.SUCCESS :
            print(repr(err))
            self.zed.close()
            exit(1)
        info = self.zed.get_camera_information()
        self.cam_model = info.camera_model
        self.sensors_data = sl.SensorsData()
        self.arduinoData=np.array([0.0] * 4)
        self.zedOrientation=np.array([0])
        self.zedAccel=np.array([0])
        self.zedAngVel=np.array([0])
        self.zedMagnet=np.array([0])
        self.zedPressure=np.array([0])
        #Defaults to the grub joint that is built on Panda's table as of 2/14/2022
        self.h = h or 8.5#*25.4 #in*mm/in 
        self.r = r or 2.0#*25.4 #in *mm/in
        self.M0 = np.array(M0) or np.array([179.75, 172.65, 0, 0])/25.4 #mm/spacer*spacersspanned np.array([175.75, 170.41, 0, 0])/25.4
        self.Moffset = Moffset or np.array([37.5, 37.5, 0, 0])/25.4 #mm from string pot to bottom of endplate
        self.JOINT_IS_STRAIGHT_ANGLE_THRESHOLD = .05 #rads 
        self.JOINT_IS_STRAIGHT_LENGTH_THRESHOLD = 1.5/25.4 # mm/mm/in=in
        self.ZED_THETA_OFFSET = .02
        self.NO_INFLECTION_THRESHOLD = .02
        self.Alpha11=0
        self.Alpha12=0
        self.Theta=0
        self.IP1=0
        self.ZEDFACINGBACKWARDS = -1 #Used to flip the axis of the zed camera.
        #Plotting member variables
        self.fig = plt.figure(figsize=(9,7))
        self.ax = self.fig.add_subplot(autoscale_on=False, xlim=(-self.h, self.h), ylim=(-2,self.h*1.5))
        self.ax.set_aspect('equal')
        self.ax.grid()
        self.Left_Line_1,=self.ax.plot([],[],'r')
        self.Left_Line_2,=self.ax.plot([],[],'r')
        self.Right_Line_1,=self.ax.plot([],[],'b')
        self.Right_Line_2,=self.ax.plot([],[],'b')
        self.Center_Line_1,=self.ax.plot([],[],'k-')
        self.Center_Line_2, = self.ax.plot([],[],'k-')
        self.Base_Line_1, = self.ax.plot([],[],'k')
        self.Base_Line_2, = self.ax.plot([],[],'y')
        self.End_Line, = self.ax.plot([],[],'g')

    def readArduino(self):
        serialData=[] 
        serialData = self.arduino.readline()
        serialData = serialData.decode("utf-8")
        try: 
            i=int(serialData[0])
            self.arduinoData[i]=float(serialData[2:8])/25.4-self.Moffset[i]
        except: #Not needed could pass here but more readable.  Try to assign to the ith element of the vector, if it fails then self.arduinoData doesn't change.
            self.arduinoData=self.arduinoData
        
    def readZed(self):
        # Filtered orientation quaternion
        orient = R.from_quat(self.sensors_data.get_imu_data().get_pose().get_orientation().get())
        self.zedOrientation=self.ZEDFACINGBACKWARDS*orient.as_euler('zyx', degrees=False) #Transform the quat into ZYX euler angles
        #print(" \t Orientation: [ Ox: {0}, Oy: {1}, Oz {2}, Ow: {3} ]".format(quaternion[0], quaternion[1], quaternion[2], quaternion[3]))
        
        # linear acceleration
        self.zedAccel = self.sensors_data.get_imu_data().get_linear_acceleration()
        #print(" \t Acceleration: [ {0} {1} {2} ] [m/sec^2]".format(linear_acceleration[0], linear_acceleration[1], linear_acceleration[2]))

        # angular velocities
        self.zedAngVel = self.sensors_data.get_imu_data().get_angular_velocity()
        #print(" \t Angular Velocities: [ {0} {1} {2} ] [deg/sec]".format(angular_velocity[0], angular_velocity[1], angular_velocity[2]))

        # Check if Magnetometer data has been updated (not the same frequency than IMU)
        
        self.zedMagnet = self.sensors_data.get_magnetometer_data().get_magnetic_field_calibrated()
        #print(" - Magnetometer\n \t Magnetic Field: [ {0} {1} {2} ] [uT]".format(magnetic_field_calibrated[0], magnetic_field_calibrated[1], magnetic_field_calibrated[2]))
        
        
        self.zedPressure = self.sensors_data.get_barometer_data().pressure
        #print(" - Barometer\n \t Atmospheric pressure: {0} [hPa]".format(self.sensors_data.get_barometer_data().pressure))
    def estimate(self,SHOW=0,FAILEDIPMEANSCONSTANT=1):
        #M1 and M2 correspond with Theta, and M3 and M4 correspond with Psi
        try:
            M = self.arduinoData
            self.Theta = -(self.zedOrientation[0]) #Positive Theta is rotation towards the strings, negative is defined away. (I think)
            
            self.IP1 = (M[0]*self.h**2- self.M0[0]*self.h**2-M[0]*self.M0[1]*self.h+self.M0[0]*self.M0[1]*self.h+self.M0[0]*self.M0[1]*self.Theta*self.r - self.M0[0]*self.Theta*self.h*self.r)/(M[0]*self.h**2-M[1]*self.h**2-self.M0[0]*self.h**2+self.M0[1]*self.h**2+M[1]*self.M0[0]*self.h-M[0]*self.M0[1]*self.h-self.M0[0]*self.Theta*self.h*self.r+self.M0[1]*self.Theta*self.h*self.r)
                        #IP2 =  (M[2]*self.h**2- self.M0[2]*self.h**2-M[2]*self.M0[3]*self.h+self.M0[2]*self.M0[3]*self.h+self.M0[2]*self.M0[3]*Theta*self.r - self.M0[2]*Theta*self.h*self.r)/(M[2]*self.h**2-M[3]*self.h**2-self.M0[2]*self.h**2+self.M0[3]*self.h**2+M[3]*self.M0[2]*self.h-M[2]*self.M0[3]*self.h-self.M0[2]*Theta*self.h*self.r+self.M0[3]*Theta*self.h*self.r)
            self.Alpha11 = -(self.M0[0]*self.Theta**2*self.r**2 + M[0]*M[1]*self.h -M[1]*self.M0[0]*self.h - M[0]*self.M0[1]*self.h + self.M0[0]*self.M0[1]*self.h -M[1]*self.M0[0]*self.Theta*self.r + self.M0[0]*self.M0[1]*self.Theta*self.r - M[0]*self.Theta*self.h*self.r + self.M0[0]*self.Theta*self.h*self.r)/(self.r*(M[0]*self.h-M[1]*self.h - self.M0[0]*self.h + self.M0[1]*self.h + M[1]*self.M0[0] - M[0]*self.M0[1] - self.M0[0]*self.Theta*self.r + self.M0[1]*self.Theta*self.r))
            self.Alpha12 = self.Theta-self.Alpha11
            
            if self.IP1<0:
                print("NEGATIVE")
            #IP2 =  (M[2]*self.h**2- self.M0[2]*self.h**2-M[2]*self.M0[3]*self.h+self.M0[2]*self.M0[3]*self.h+self.M0[2]*self.M0[3]*Psi*self.r - self.M0[2]*Psi*self.h*self.r)/(M[2]*self.h**2-M[3]*self.h**2-self.M0[2]*self.h**2+self.M0[3]*self.h**2+M[3]*self.M0[2]*self.h-M[2]*self.M0[3]*self.h-self.M0[2]*Psi*self.h*self.r+self.M0[3]*Psi*self.h*self.r)
            #Alpha21 = -(self.M0[2]*Psi**2*self.r**2 + M[2]*M[3]*self.h -M[3]*self.M0[2]*self.h - M[2]*self.M0[3]*self.h + self.M0[2]*self.M0[3]*self.h -M[3]*self.M0[2]*Psi*self.r + self.M0[2]*self.M0[3]*Psi*self.r - M[2]*Psi*self.h*self.r + self.M0[2]*Psi*self.h*self.r)/(self.r*(M[2]*self.h-M[3]*self.h - self.M0[2]*self.h + self.M0[3]*self.h + M[3]*self.M0[2] - M[2]*self.M0[3] - self.M0[2]*Psi*self.r + self.M0[3]*Psi*self.r))
            #Alpha22 = (self.M0[3]*Psi**2*self.r**2 + M[2]*M[3]*self.h - M[3]*self.M0[2]*self.h - M[2]*self.M0[3]*self.h + self.M0[2]*self.M0[3]*self.h - M[2]*self.M0[3]*Psi*self.r + self.M0[2]*self.M0[3]*Psi*self.r - M[3]*Psi*self.h*self.r + self.M0[3]*Psi*self.h*self.r)/(self.r*(M[2]*self.h - M[3]*self.h - self.M0[2]*self.h + self.M0[3]*self.h + M[3]*self.M0[2] - M[2]*self.M0[3] - self.M0[2]*Psi*self.r + self.M0[3]*Psi*self.r))
    

        
            TestAlpha12 = self.Alpha12+self.Alpha11
            if abs(TestAlpha12) < self.JOINT_IS_STRAIGHT_ANGLE_THRESHOLD and abs(self.M0[0]-M[0])<self.JOINT_IS_STRAIGHT_LENGTH_THRESHOLD :
                self.IP1=0
                self.Alpha11=0
                self.Alpha12=0
            if FAILEDIPMEANSCONSTANT and (abs(self.IP1)-1>0 or self.IP1<0):#abs((self.M0[0]-M[0])/self.M0[0]-(self.M0[1]-M[1])/self.M0[1])<self.NO_INFLECTION_THRESHOLD:
                self.IP1 = 1000
                self.Alpha11 = 1000
                self.Alpha12 = 1000
            #TestAlpha22 = Psi-Alpha22
            if SHOW: #TODO IMPLEMENT VISUALIZATION
                print(("IP1 : {}").format(self.IP1))
                print(("Alpha11 : {}").format(self.Alpha11))
                print(("Alpha 12 : {}").format(self.Alpha12))
                print(("TestAlpha12 : {}").format(TestAlpha12)) 
                pass
        except:
           print("Estimation Failed, Still initializing?")
    def calcIP(self): # Could implement this to reduce computation time i.e. make repeated terms calculate 1x instead of 4x
        pass
    def calcRotOffset(self):
        Accel=self.zedAccel
    def showMeasurements(self, data=1):
        if data==1:
            print("Arduino Data : ")
            print(self.arduinoData)
        elif data ==2:
            print("Zed Orientation (zyx) : ")
            print(self.zedOrientation)
        elif data ==3:
            #print("Zed Accelerations")
            print(self.zedAccel)
        else:
            print(self.arduinoData)
            print(self.zedOrientation)
    def drawRobo(self,test=0,RECORD=0):
        if test:
            self.Alpha11=-.01
            self.Alpha12= -.8
            self.IP1 = 1000
            self.arduinoData[0]=9
            self.Theta=self.Alpha11+self.Alpha12
        if self.IP1 == 1000:
            base_x = np.array(list(range(-1,2)))*self.r
            base_y = base_x*0
            Center_line_r = self.h/self.Theta
            if np.sign(self.Theta)>0:
                Left_line_r = Center_line_r-self.r
                
                Right_line_r = Center_line_r+self.r
                center = np.array([-Center_line_r,0])
                thetasteps= np.array(list(range(0,50)))*abs(self.Theta/49)
                Left_line_x = np.cos(thetasteps)*Left_line_r + center[0]
                Left_line_y = np.sin(thetasteps)*Left_line_r + center[1]
                Right_line_x = np.cos(thetasteps)*Right_line_r + center[0]
                Right_line_y = np.sin(thetasteps)*Right_line_r + center[1]
                Center_line_x = np.cos(thetasteps)*Center_line_r + center[0]
                Center_line_y = np.sin(thetasteps)*Center_line_r + center[1]

            else:

                Left_line_r = Center_line_r+self.r
                Right_line_r = Center_line_r-self.r
                center = np.array([Center_line_r,0])
                thetasteps= np.array(list(range(0,50)))*abs(self.Theta/49)-np.pi
                Left_line_x = -(np.cos(thetasteps)*Left_line_r + center[0])
                Left_line_y = (np.sin(thetasteps)*Left_line_r + center[1])
                Right_line_x = -(np.cos(thetasteps)*Right_line_r + center[0])
                Right_line_y = (np.sin(thetasteps)*Right_line_r + center[1])
                Center_line_x = -(np.cos(thetasteps)*Center_line_r + center[0])
                Center_line_y = (np.sin(thetasteps)*Center_line_r + center[1])
            
            translation_x = Center_line_x[-1]
            translation_y = Center_line_y[-1]

            end_line_x = base_x
            end_line_y = base_y
            end_line_x_rot_transl = (np.cos(self.Theta)*end_line_x-np.sin(self.Theta)*end_line_y)+translation_x
            end_line_y_rot_transl = (np.sin(self.Theta)*end_line_x+np.cos(self.Theta)*end_line_y)+translation_y
            # plt.plot(base_x,base_y,'k-')
            # plt.plot(Left_line_x,Left_line_y,'r-')
            # plt.plot(Right_line_x,Right_line_y,'b-')
            # plt.plot(end_line_x_rot_transl,end_line_y_rot_transl,'y-')
            # plt.show()
            #Reset all of drawing variables to be empty
            self.Left_Line_1.set_data([],[])
            self.Left_Line_2.set_data([],[])
            self.Right_Line_1.set_data([],[])
            self.Right_Line_2.set_data([],[])
            self.Center_Line_1.set_data([],[])
            self.Center_Line_2.set_data([],[])
            self.Base_Line_1.set_data([],[])
            self.Base_Line_2.set_data([],[])
            self.End_Line.set_data([],[])
            #Update just the ones we need to draw for this set
            self.Left_Line_1.set_data(Left_line_x,Left_line_y)
            self.Right_Line_1.set_data(Right_line_x,Right_line_y)
            self.Base_Line_1.set_data(base_x,base_y)
            self.End_Line.set_data(end_line_x_rot_transl,end_line_y_rot_transl)
            plt.pause(.01)

            return 'CC'

            
        elif self.IP1 == 0:
            base_x = np.array(list(range(-1,2)))*self.r
            base_y = base_x*0
            Left_line_x = np.array([-self.r,-self.r])
            Left_line_y = np.array([0,self.h])
            Right_line_x = np.array([self.r,self.r])
            Right_line_y = np.array([0,self.h])
            end_line_x = base_x
            end_line_y = base_y + self.h
            
            # plt.plot(base_x,base_y,'k-')
            # plt.plot(Left_line_x,Left_line_y,'r-')
            # plt.plot(Right_line_x,Right_line_y,'b-')
            # plt.plot(end_line_x,end_line_y,'y-')
            # plt.show()

            self.Left_Line_1.set_data([],[])
            self.Left_Line_2.set_data([],[])
            self.Right_Line_1.set_data([],[])
            self.Right_Line_2.set_data([],[])
            self.Center_Line_1.set_data([],[])
            self.Center_Line_2.set_data([],[])
            self.Base_Line_1.set_data([],[])
            self.Base_Line_2.set_data([],[])
            self.End_Line.set_data([],[])

            self.Left_Line_1.set_data(Left_line_x,Left_line_y)
            self.Right_Line_1.set_data(Right_line_x,Right_line_y)
            self.Base_Line_1.set_data(base_x,base_y)
            self.End_Line.set_data(end_line_x,end_line_y)
            plt.pause(.01)

            return 'Straight'
        else:
            if np.sign(self.Alpha11)>0:                          # Sign of Alpha11 determines on which side of the robot the center of rotation is.  Positive its towards strings, Negative its away from the strings.
                base_x= np.array(list(range(-1,2)))*self.r# For drawing the BaseLine
                base_y= base_x*0 # For drawing the BaseLine
                center_r_base= self.IP1*self.h/self.Alpha11 #r=S/theta. Negative makes it so positive rotation is in the direction of the strings (left in the graph)
                center_base = np.array([-center_r_base,0])  #First seg will be defined as flat so the center has only x.
                thetasteps= np.array(list(range(0,50)))*self.Alpha11/49 #Break the arcs into 50 steps

                centerline_x = center_r_base*np.cos(thetasteps)+center_base[0] #For drawing the centerline of the first seg
                centerline_y = center_r_base*np.sin(thetasteps)+center_base[1] #For drawing the centerline of the first seg

                Left_line_r = (center_r_base-self.r)               #Radius subtracts here because center of rotation is on the same side as the strings
                Left_line_x = Left_line_r*np.cos(thetasteps)+center_base[0]
                Left_line_y = Left_line_r*np.sin(thetasteps)+center_base[1]

                Right_line_r = (center_r_base+self.r)               #Radius subtracts here because center of rotation is on the opposite side as the strings
                Right_line_x = Right_line_r*np.cos(thetasteps)+center_base[0]
                Right_line_y = Right_line_r*np.sin(thetasteps)+center_base[1]
            else:
                base_x= np.array(list(range(-1,2)))*self.r# For drawing the BaseLine
                base_y= base_x*0 # For drawing the BaseLine
                center_r_base=-self.IP1*self.h/self.Alpha11 #r=S/theta.Positive means the first seg is bent away from the strings (right in the graph)
                center_base = np.array([center_r_base,0])  #First seg will be defined as flat so the center has only x.
                thetasteps= np.array(list(range(0,50)))*abs(self.Alpha11/49)+-np.pi #Break the arcs into 50 steps

                centerline_x = center_r_base*np.cos(thetasteps)+center_base[0] #For drawing the centerline of the first seg
                centerline_y = -center_r_base*np.sin(thetasteps)+center_base[1] # Y is negative for all of these because it makes it right...

                Left_line_r = (center_r_base+self.r)           #Radius adds here because center of rotation is on the side opposite the strings
                Left_line_x = Left_line_r*np.cos(thetasteps)+center_base[0]
                Left_line_y = - Left_line_r*np.sin(thetasteps)+center_base[1] #Negative sign flips the plot so it looks right...

                Right_line_r = (center_r_base-self.r)          #Radius subtracts here because center of rotation is on the same side as the strings
                Right_line_x = Right_line_r*np.cos(thetasteps)+center_base[0]
                Right_line_y = -Right_line_r*np.sin(thetasteps)+center_base[1] #Negative sign flips the plot so it looks right...

            if np.sign(self.Alpha12)>0: #Repeat again for the second segment, we calculate what all the points should be if it were located at the origin, and then rotate them by Alpha11 and translate them so the new origin is the end of the last curve.
                translation = np.array([centerline_x[-1], centerline_y[-1]])
                
                upper_x = np.array(list(range(-1,2)))*self.r# For drawing the UpperLine
                upper_y = upper_x*0 # For drawing the UpperLine        
                upper_x_rot_transl = (np.cos(self.Alpha11)*upper_x-np.sin(self.Alpha11)*upper_y)+translation[0]
                upper_y_rot_transl = (np.sin(self.Alpha11)*upper_x+np.cos(self.Alpha11)*upper_y)+translation[1]
                
                center_r_upper=(1-self.IP1)*self.h/self.Alpha12 #r=S/theta. Negative makes it so positive rotation is in the direction of the strings (left in the graph)
                center_upper= np.array([-center_r_upper,0])
                center_upper_rot_transl = np.array([[np.cos(self.Alpha11), -np.sin(self.Alpha11)],[np.sin(self.Alpha11),np.cos(self.Alpha11)]])@center_upper + translation  #First seg will be defined as flat so the center has only x.
                
                thetasteps = np.array(list(range(0,50)))*self.Alpha12/49 #Break the arcs into 50 steps

                centerline_x_2 = center_r_upper*np.cos(thetasteps)+center_upper[0] #For drawing the centerline of the first seg
                centerline_y_2 = center_r_upper*np.sin(thetasteps)+center_upper[1] #For drawing the centerline of the first seg

                centerline_x_rot_transl = (np.cos(self.Alpha11)*centerline_x_2-np.sin(self.Alpha11)*centerline_y_2)+translation[0]
                centerline_y_rot_transl = (np.sin(self.Alpha11)*centerline_x_2+np.cos(self.Alpha11)*centerline_y_2)+translation[1]

                Left_line_r_2 = (center_r_upper-self.r)               #Radius subtracts here because center of rotation is on the same side as the strings
                Left_line_x_2 = Left_line_r_2*np.cos(thetasteps)+center_upper[0]
                Left_line_y_2 = Left_line_r_2*np.sin(thetasteps)+center_upper[1]
                Left_line_x_rot_transl = (np.cos(self.Alpha11)*Left_line_x_2-np.sin(self.Alpha11)*Left_line_y_2)+translation[0]
                Left_line_y_rot_transl = (np.sin(self.Alpha11)*Left_line_x_2+np.cos(self.Alpha11)*Left_line_y_2)+translation[1]


                Right_line_r_2 = (center_r_upper+self.r)               #Radius subtracts here because center of rotation is on the opposite side as the strings
                Right_line_x_2 = Right_line_r_2*np.cos(thetasteps)+center_upper[0]
                Right_line_y_2 = Right_line_r_2*np.sin(thetasteps)+center_upper[1]
                Right_line_x_rot_transl = (np.cos(self.Alpha11)*Right_line_x_2-np.sin(self.Alpha11)*Right_line_y_2)+translation[0]
                Right_line_y_rot_transl = (np.sin(self.Alpha11)*Right_line_x_2+np.cos(self.Alpha11)*Right_line_y_2)+translation[1]



            else:
                translation = np.array([centerline_x[-1], centerline_y[-1]])
                
                upper_x = np.array(list(range(-1,2)))*self.r# For drawing the UpperLine
                upper_y = upper_x*0 # For drawing the UpperLine        
                upper_x_rot_transl = (np.cos(self.Alpha11)*upper_x-np.sin(self.Alpha11)*upper_y)+translation[0]
                upper_y_rot_transl = (np.sin(self.Alpha11)*upper_x+np.cos(self.Alpha11)*upper_y)+translation[1]
                
                center_r_upper=-(1-self.IP1)*self.h/self.Alpha12 #r=S/theta. Negative makes it so positive rotation is in the direction of the strings (left in the graph)
                center_upper= np.array([center_r_upper,0])
                center_upper_rot_transl = np.array([[np.cos(self.Alpha11), -np.sin(self.Alpha11)],[np.sin(self.Alpha11),np.cos(self.Alpha11)]])@center_upper + translation  #First seg will be defined as flat so the center has only x.
                
                thetasteps = np.array(list(range(0,50)))*abs(self.Alpha12/49)+-np.pi #Break the arcs into 50 steps

                centerline_x_2 = center_r_upper*np.cos(thetasteps)+center_upper[0] #For drawing the centerline of the first seg
                centerline_y_2 = -center_r_upper*np.sin(thetasteps)+center_upper[1] #For drawing the centerline of the first seg

                centerline_x_rot_transl = (np.cos(self.Alpha11)*centerline_x_2-np.sin(self.Alpha11)*centerline_y_2)+translation[0]
                centerline_y_rot_transl = (np.sin(self.Alpha11)*centerline_x_2+np.cos(self.Alpha11)*centerline_y_2)+translation[1]

                Left_line_r_2 = (center_r_upper+self.r)               #Radius subtracts here because center of rotation is on the same side as the strings
                Left_line_x_2 = Left_line_r_2*np.cos(thetasteps)+center_upper[0]
                Left_line_y_2 = -Left_line_r_2*np.sin(thetasteps)+center_upper[1]
                Left_line_x_rot_transl = (np.cos(self.Alpha11)*Left_line_x_2-np.sin(self.Alpha11)*Left_line_y_2)+translation[0]
                Left_line_y_rot_transl = (np.sin(self.Alpha11)*Left_line_x_2+np.cos(self.Alpha11)*Left_line_y_2)+translation[1]


                Right_line_r_2 = (center_r_upper-self.r)               #Radius subtracts here because center of rotation is on the opposite side as the strings
                Right_line_x_2 = Right_line_r_2*np.cos(thetasteps)+center_upper[0]
                Right_line_y_2 = -Right_line_r_2*np.sin(thetasteps)+center_upper[1]
                Right_line_x_rot_transl = (np.cos(self.Alpha11)*Right_line_x_2-np.sin(self.Alpha11)*Right_line_y_2)+translation[0]
                Right_line_y_rot_transl = (np.sin(self.Alpha11)*Right_line_x_2+np.cos(self.Alpha11)*Right_line_y_2)+translation[1]
            translation=np.array([centerline_x_rot_transl[-1],centerline_y_rot_transl[-1]])
            end_line_x=base_x
            end_line_y=base_y
            end_line_x_rot_transl = (np.cos(self.Theta)*end_line_x-np.sin(self.Theta)*end_line_y)+translation[0]
            end_line_y_rot_transl = (np.sin(self.Theta)*end_line_x+np.cos(self.Theta)*end_line_y)+translation[1]

            # plt.plot(base_x,base_y,'k')
            # plt.plot(upper_x_rot_transl,upper_y_rot_transl,'k')
            # plt.plot(Left_line_x,Left_line_y,'b')
            # plt.plot(Left_line_x_rot_transl,Left_line_y_rot_transl,'b')
            # plt.plot(Right_line_x,Right_line_y,'r')
            # plt.plot(Right_line_x_rot_transl,Right_line_y_rot_transl,'r')
            # plt.plot(end_line_x_rot_transl,end_line_y_rot_transl,'k')
            # plt.show()

            self.Left_Line_1.set_data([],[])
            self.Left_Line_2.set_data([],[])
            self.Right_Line_1.set_data([],[])
            self.Right_Line_2.set_data([],[])
            self.Center_Line_1.set_data([],[])
            self.Center_Line_2.set_data([],[])
            self.Base_Line_1.set_data([],[])
            self.Base_Line_2.set_data([],[])
            self.End_Line.set_data([],[])

            self.Left_Line_1.set_data(Left_line_x,Left_line_y)
            self.Right_Line_1.set_data(Right_line_x,Right_line_y)
            self.Base_Line_1.set_data(base_x,base_y)
            self.Left_Line_2.set_data(Left_line_x_rot_transl,Left_line_y_rot_transl)
            self.Right_Line_2.set_data(Right_line_x_rot_transl,Right_line_y_rot_transl)
            self.Base_Line_2.set_data(upper_x_rot_transl,upper_y_rot_transl)
            
            self.End_Line.set_data(end_line_x_rot_transl,end_line_y_rot_transl)
            
            if RECORD:
                now=datetime.datetime.now()
                stamp = "{}{}{}{}".format(now.day,now.hour,now.minute,now.second)
                out = {}
                out['Endpoint x, y'] = np.hstack((centerline_x_rot_transl[-1],centerline_y_rot_transl[-1]))
                CenterLine=np.hstack((np.vstack((centerline_x,centerline_x_rot_transl)),np.vstack((centerline_y,centerline_y_rot_transl))))
                lst = (np.linspace(1,100,50)).astype(int)-1
                fiftyCL=[]
                for i in lst:
                    fiftyCL.append(CenterLine[:,i])

                out['50 step centerline x,y'] = fiftyCL
                out['Theta'] = self.Theta
                
                Center_line_r = self.h/self.Theta
                if np.sign(self.Theta)>0:
                    center = np.array([-Center_line_r,0])
                    thetasteps = np.array(list(range(0,50)))*abs(self.Theta/49)
                    Center_line_x = np.cos(thetasteps)*Center_line_r + center[0]
                    Center_line_y = np.sin(thetasteps)*Center_line_r + center[1]
                else:
                    center = np.array([Center_line_r,0])
                    thetasteps = np.array(list(range(0,50)))*abs(self.Theta/49)-np.pi
                    Center_line_x = -(np.cos(thetasteps)*Center_line_r + center[0])
                    Center_line_y = (np.sin(thetasteps)*Center_line_r + center[1])
                Constant_curve = [Center_line_x[-1], Center_line_y[-1]]

                
                out['Constant Curve Endpoint'] = Constant_curve
                
                with open("SavedData_{}.txt".format(stamp),"w") as f:
                    print(out, file=f)
                    
            plt.pause(.01)



        

    
'''Class definition for simple timestamp handler given on Zed SDK github https://github.com/stereolabs/zed-examples/blob/master/tutorials/tutorial%207%20-%20sensor%20data/python/sensor_data.py'''
class TimestampHandler:
    def __init__(self):
        self.t_imu = sl.Timestamp()
        self.t_baro = sl.Timestamp()
        self.t_mag = sl.Timestamp()

    ##
    # check if the new timestamp is higher than the reference one, and if yes, save the current as reference
    def is_new(self, sensor):
        if (isinstance(sensor, sl.IMUData)):
            new_ = (sensor.timestamp.get_microseconds() > self.t_imu.get_microseconds())
            if new_:
                self.t_imu = sensor.timestamp
            return new_
        elif (isinstance(sensor, sl.MagnetometerData)):
            new_ = (sensor.timestamp.get_microseconds() > self.t_mag.get_microseconds())
            if new_:
                self.t_mag = sensor.timestamp
            return new_
        elif (isinstance(sensor, sl.BarometerData)):
            new_ = (sensor.timestamp.get_microseconds() > self.t_baro.get_microseconds())
            if new_:
                self.t_baro = sensor.timestamp
            return 
class EstimationPub:
    def __init__(self,rate = 0,LengthPubname = 'LengthPub'):
        self.Lengths = np.array([0.0,0.0,0.0,0.0])
        self.subnode = rospy.init_node('EstimationListener', anonymous=True)
        self.Lengthsub = rospy.Subscriber(LengthPubname,f32A,self.callback())
        self.pub = rospy.Publisher('')
        #TODO: ADD ALL of the parameters pertaining to the 
        pass   
    
    def callback(self,LengthData):
        self.Lengths = LengthData.data
    
def EstimationPublisher():
    publisher=EstimationPub.EstimationPublisher()
    rospy.spin()
if __name__=='__main__':
    try:
        LengthPublisher()
    except rospy.ROSInterruptException:
        pass