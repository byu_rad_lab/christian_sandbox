% ThreedPlot2

Data=load('70000Data852021.mat');
Data=Data.Data;
[m,n]=size(Data);
for i=1:m
    for j=1:n
        if isnan(Data(i,j))
            Data(i,j)=1;
        end
    end
end

DataTable=table(Data(:,1),Data(:,2),Data(:,3), Data(:,4),Data(:,5),Data(:,6),'VariableNames', {'Inflection', 'Alpha1', 'Theta', 'L1', 'L2', 'Ratio'});
lme = fitlme(DataTable,'Inflection ~ L2^3*L1^3*Theta*Alpha1*Ratio^2');%+ L1^2:Del^2 + L2^2*Del ')