%Model Discovery
clear
close all
major_iters=3000;
seed=12;


angle2s=[-5*pi/180:-5*pi/180:-60*pi/180];
overall_h = 2;
r = .125;
%angle2s=pi/4*ones(12);
overlap_start=.25;
overlap_end=.75;
stepsize = 5;
inflection_start = 10;
inflection_end = 90;
iterations=inflection_start:stepsize:inflection_end;
iters = length(angle2s); 
%iters = 4;
angle1 = 20*pi/180;
%angle2 = pi/4;
overlap_end_angle = zeros(iters,length(iterations));
overlap_start_angle = zeros(iters,length(iterations));
overlap_length = zeros(iters,length(iterations));
a = PCCSeg([0,angle1,0,0,20/100*overall_h,r;]);
overlap_end_seg = a;

for k=2:length([inflection_start:stepsize:inflection_end])
    overlap_end_seg = [overlap_end_seg,a];
end
overlap_end_seg1= overlap_end_seg;

for j=1:iters
    overlap_end_seg = [overlap_end_seg; overlap_end_seg1];
end
overlap_end_seg2= overlap_end_seg;
for l=1:major_iters
    overlap_end_seg = cat(3,overlap_end_seg,overlap_end_seg2);
    l
end

overlap_start_seg = overlap_end_seg;
%load('Learninginit.mat')
%load('Learninginit100pts.mat')

rng(seed)
for generate=1:major_iters
    overlap_start(generate) = (randi([-5,5])*2+25)/100;
    overlap_end(generate) = (randi([-5,5])*2+75)/100;
    overlap_pct(generate) =overlap_end(generate)-overlap_start(generate);
    angle2s=[-5*pi/180:-5*pi/180:-60*pi/180];
    angle1 = 20*pi/180;
    angle2sadjust = -1 + (2)*rand;
    angle1adjust = -3 + (6)*rand;
    r_adjust = 1+(4)*rand;
    h_adjust = 1+(2)*rand;
    angle2s=angle2sadjust.*angle2s;
    angle1=angle1*angle1adjust;
    
for j = 1:iters
    %r(j)=j^1.5*.125;
    %overall_h(j)=j*2;
    close all
    r(j,generate)=r_adjust*.125;
    
    overall_h(j,generate)=h_adjust;
    ratio(j,generate)=overall_h(j,generate)/r(j,generate);
    for i = inflection_start:stepsize:inflection_end
        counter=(i-(inflection_start-stepsize))/stepsize;
        a = PCCSeg([0,angle1,0,0,i/100*overall_h(j,generate),r(j,generate);]);
        b = PCCSeg([0,angle2s(j),0,0,((1-i/100)*overall_h(j,generate)),r(j,generate)]);
        
        a = a.plotseg;
        a = a.update([],a.Lhs_Length, a.Rhs_Length);
        b = b.update(a,b.Lhs_Length, b.Rhs_Length);
        b = b.plotseg;
        measurement_location=1;
        
        segments(:,counter)=[a;b];
        Pm(j,counter,generate)=measure([a,b],0,overlap_end(generate),measurement_location); %Checked the values for Pm(i/20)=measure([a,b],0,.75,1) with the inflection at .8*Overall_h
        Gm(j,counter,generate)=measure([a,b],overlap_start(generate),1,measurement_location);%Checked this value Gm(i/20)=measure([a,b],.25,1,1) same inflection point. h=2, r= .125


        if (overlap_start(generate)*overall_h(j,generate))<=segments(1,counter).height %Check to see where the overlap starts
            overlap_start_angle(j,counter,generate) = (overlap_start(generate)*overall_h(j,generate)/segments(1,counter).centerline_radius);
            overlap_start_seg(j,counter,generate) = segments(1,counter);%Overlap starts in the first segment
        else
            overlap_start_angle(j,counter,generate) = ((overlap_start(generate)*overall_h(j,generate)-segments(1,counter).height)/segments(2,counter).centerline_radius); %Overlap starts in the second segment
            overlap_start_seg(j,counter,generate) = segments(2,counter);
        end

        if (overall_h(j,generate)-overlap_end(generate)*overall_h(j,generate))<segments(2,counter).height %Check to see where the overlap ends
            overlap_end_angle(j,counter,generate) = (overlap_end(generate)*overall_h(j,generate)-segments(1,counter).height)/segments(2,counter).centerline_radius;%Overlap ends in the second segment
            overlap_end_seg(j,counter,generate) = segments(2,counter);
        else
            overlap_end_angle(j,counter,generate) = (overlap_end(generate)*overall_h(j,generate))/segments(1,counter).centerline_radius;%Overlap ends in the first segment
            overlap_end_seg(j,counter,generate) = segments(1,counter);
        end

        if overlap_end_seg(j,counter,generate).seg_position == overlap_start_seg(j,counter,generate).seg_position
            overlap_length(j,counter,generate) = (overlap_end_angle(j,counter,generate)-overlap_start_angle(j,counter,generate))*(overlap_start_seg(j,counter,generate).centerline_radius+measurement_location*r(j,generate));
        else
            overlap_length(j,counter,generate) = (overlap_end_angle(j,counter,generate)*(overlap_end_seg(j,counter,generate).centerline_radius+measurement_location*r(j,generate)))+(overlap_start_seg(j,counter,generate).centerline_radius+measurement_location*r(j,generate))*(overlap_start_seg(j,counter,generate).angle-overlap_start_angle(j,counter,generate));
        end

       %figure
       
    end
    
    
% overlap_length=overlap_length/overall_h;

end
% legendStrings = "h/r = " + string(ratio);
% legendStrings = "Angle 2 = " + string(angle2s*180/pi) + " deg";
% titlestring = "Angle 1 = " +string(angle1*180/pi) + " deg";
% figure
% for j=1:iters
%     
%     scatter([inflection_start:stepsize:inflection_end],[overlap_length(j,:)/overall_h(j)]);
%     hold on
%     title(titlestring)
%     xlabel("Inflection Point as pct of overall length")
%     ylabel("Overlap Length as percent of overall length")
%     legend(legendStrings)
% end
% figure
% for j=1:iters
% 
%     scatter([inflection_start:stepsize:inflection_end],Gm(j,:)/overall_h(j));
%     hold on
%     title(titlestring)
%     xlabel("Inflection Point as pct of overall length")
%     ylabel("Measured length of sensor attached to the start of the link Gm (% of overall length)")
%     
%     
%    legend(legendStrings)
% end
% figure
% for j=1:iters
%     
%     scatter([inflection_start:stepsize:inflection_end],Pm(j,:)/overall_h(j));
%     hold on
%     title(titlestring)
%     xlabel("Inflection Point as pct of overall length")
%     ylabel("Measured length of the sensor attached to the end of the link Pm (% of overall length)")
%     %ylabel("Overlap Length as percent of overall length")
%     legend(legendStrings)
% end
generate
end
outputs=inflection_start:stepsize:inflection_end;
new_outputs=[];
params2=[];
new_h=[];
Pmnew=[];
Gmnew=[];
newratio=[];
new_overlap_pct=[];
  
% overall_h=reshape(overall_h,[],1);
% Pmnew=reshape(Pm,[],17);
for a=1:size(Pm,3)
    Pmnew=[Pmnew;Pm(:,:,a)];
    Gmnew=[Gmnew;Gm(:,:,a)];
end
Pmnew=Pmnew';
Gmnew=Gmnew';
Pmnew2=[];
Gmnew2=[];
for a=1:size(Pmnew,2)
    Pmnew2=[Pmnew2;Pmnew(:,a)];
    Gmnew2=[Gmnew2;Gmnew(:,a)];
end

for c = 1:size(Pm,3)
    for b = 1:size(Pm,1)
        for a = 1:size(Pm,2)
            newratio=[newratio;ratio(b,c)];
            new_h = [new_h;overall_h(b,c)];
            new_outputs=[new_outputs;outputs(1,a)];
        end
    end
    c
end


    

for a=1:size(overlap_pct,2)
    for b=1:(size(Pm,1)*size(Pm,2))
        new_overlap_pct=[new_overlap_pct;overlap_pct(1,a)];
    end
end

params=[Pmnew2,Gmnew2,newratio,new_overlap_pct];
fit=polyfitn(params,new_outputs,3);

% for j = 1:size(overlap_start_angle,3)
%     
%     for i =1:size(ratio,1)
%         params(:,:,i) = [Gmnew2, Pmnew2, newratio, overlap_pct(1,1)*ones(1,length(overlap_end_seg1)) ]'; 
%         output(:,:,i) = [inflection_start:stepsize:inflection_end]';
%         params2=vertcat(params2, params(:,:,i));
%     end
%     
%     
%     output2=reshape(output,1,[]);
%     fit=polyfitn(params2,output2,3);
% end
% 

