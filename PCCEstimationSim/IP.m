syms IP h r a1 a2 t OL M2 M1
% solve(M2-h+r*a2==-IP*h+(OL-(1-IP))/(IP)*(IP*h-r*(t-a2)),IP)
%% The above returns the solution for IP as a function of a2, theta, Overlap, Measurement, radius and height
solve([M2-h+r*a2==-IP*h+(OL-(1-IP))/(IP)*(IP*h-r*(t-a2)),M1==IP*h-r*(t-a2)+(OL-IP)/(1-IP)*(h-IP*h-r*a2)],[IP,a2])
%%The above returns analytic solution for IP and a2 as a function of length
%%measurements (M1,M2), End angle measurement (t), segment length (h), and
%%radius of the segment (r)