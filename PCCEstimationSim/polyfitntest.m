%x^2=y
% xdata = 0:1:20;
% ydata = xdata.^2;
% fit = polyfitn(xdata,ydata)
%x^2+y^2 = z^2

xdata = 0 + (400-0).*rand(100,1);
ydata = 0 + (400-0).*rand(100,1);
zdata = xdata.^2+ydata.^2;
fit = polyfitn([xdata,ydata],zdata,2);
cs=fit.Coefficients;
zfit=cs(1)*xdata.^2+cs(2)*xdata.*ydata+cs(3)*xdata+cs(4)*ydata.^2+cs(5)*ydata+cs(6);
scatter3(xdata,ydata,zdata')
hold on
scatter3(xdata,ydata,zfit)
%Data is right on but the form is way wrong... when I create the vectors
%0:stepsize:upperbound, but when I create it randomly it is perfect.
%Inputs need to span the input space. Before I was creating the data with
%the two inputs correlated. (i.e. as xdata increased so did ydata)