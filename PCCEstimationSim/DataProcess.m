clear 
close all
load('3000Datapts62221.mat')
outputs=inflection_start:stepsize:inflection_end;
new_outputs=[];
params2=[];
new_h=[];
Pmnew=[];
Gmnew=[];
newratio=[];
new_overlap_pct=[];
  
% overall_h=reshape(overall_h,[],1);
% Pmnew=reshape(Pm,[],17);
for a=1:size(Pm,3)
    Pmnew=[Pmnew;Pm(:,:,a)];
    Gmnew=[Gmnew;Gm(:,:,a)];
end
Pmnew=Pmnew';
Gmnew=Gmnew';
Pmnew2=[];
Gmnew2=[];
for a=1:size(Pmnew,2)
    Pmnew2=[Pmnew2;Pmnew(:,a)];
    Gmnew2=[Gmnew2;Gmnew(:,a)];
end

for c = 1:size(Pm,3)
    for b = 1:size(Pm,1)
        for a = 1:size(Pm,2)
            newratio=[newratio;ratio(b,c)];
            new_h = [new_h;overall_h(b,c)];
            new_outputs=[new_outputs;outputs(1,a)];
        end
    end
    c
end


    

for a=1:size(overlap_pct,2)
    for b=1:(size(Pm,1)*size(Pm,2))
        new_overlap_pct=[new_overlap_pct;overlap_pct(1,a)];
    end
end

params=[Pmnew2,Gmnew2,newratio,new_overlap_pct];
fit=polyfitn(params,new_outputs,5);