classdef PCCSeg
    properties
        %Starting with 2D, eventually need to add front and back lengths
        seg_position      %segment number in the chain
        start_orientation % currently just a single angle representing rotation in the 2d plane with respect to base (0 right now)
        end_orientation   % currently just a single angle representing rotation in the 2d plane
        angle             % angle between start and end orientation
        start_position    % x-y coordinates of the centerline start point of the segment
        end_position      % x-y coordinates of the centerline end point of the segment
        height %height of the segment
        radius %radius of the segment
        Lhs_Length %Length of the left side of the segment
        Rhs_Length %Length of the right side of the segment
        arc_center %Centerpoint of all 3 arcs that define the segment
        centerline_radius %Radius defining the centerline
        maxROM=360*pi/180; %Makes it so the max deflection of a segment is 60 degrees
        
    end
    methods
        function obj = PCCSeg(init)
            obj.start_orientation = init(1);
            obj.end_orientation = init(2);
            obj.start_position = init(3:4);
            obj.height = (init(5));
            obj.radius = init(6);
            obj.angle = obj.end_orientation - obj.start_orientation; %angle between the start and end orientaitons
            if obj.angle == 0
                obj.Lhs_Length=obj.height;
                obj.Rhs_Length=obj.height;
            else
            virtual_radius = abs(obj.height/obj.angle); %radius of the circle being drawn by the centerline of the segment
            Lhs_virtual_radius = virtual_radius-sign(obj.angle)*obj.radius; %Radius of the circle being drawn by the lhs of the segment
            Rhs_vitrual_radius = virtual_radius+sign(obj.angle)*obj.radius; %Radius of the circle being drawn by the rhs of the segment
            obj.Lhs_Length = abs(Lhs_virtual_radius*obj.angle); % L=a*R
            obj.Rhs_Length = abs(Rhs_vitrual_radius*obj.angle); % L=a*R
            end
        end
        function obj = update(obj,prev,lhs_measurement, rhs_measurement)
            %%Could calculate angle and opposite side length from any 1
            %%measurement.  Should probably.  
            obj.Lhs_Length=lhs_measurement;
            obj.Rhs_Length=rhs_measurement;
            
            
            if isempty(prev)
                obj.start_orientation = 0;
                obj.seg_position = 1;
            else
            obj.start_orientation = prev.end_orientation; %Start orientation is the last segments end orientation
            obj.start_position = prev.end_position; %Start Position is the last segments end position
            obj.seg_position = prev.seg_position +1; %Make the segment 1 greater then the last segment
            end
            %if rhs_measurement>lhs_measurement
                obj.angle = (rhs_measurement-lhs_measurement)/2/obj.radius;
                obj.end_orientation = obj.start_orientation+obj.angle;%This comes from 2 circles centered at the same point whose radii are 2*obj.radius different.  
            %else
            %    obj.angle = (lhs_measurement-rhs_measurement)/2*obj.radius;
            %end
            
            if obj.angle > obj.maxROM
                %fprintf('Angle exceeds max range of motion')
            end
            if obj.angle == Inf
                obj.end_orientation=obj.start_orientation;
                obj.angle=0;
            else
            obj.end_orientation = obj.start_orientation+obj.angle;
            end
        end
        function obj=plotseg(obj)
            hold on;
            
            lhs_arc_radius = abs(obj.Lhs_Length/obj.angle);
            rhs_arc_radius = abs(obj.Rhs_Length/obj.angle);
            
            theta = linspace(obj.start_orientation,obj.angle+obj.start_orientation,20);
            
            if sign(obj.angle)>0
                xcenter = obj.start_position(1) - (lhs_arc_radius+obj.radius)*cos(obj.start_orientation); 
                ycenter = obj.start_position(2) - (lhs_arc_radius+obj.radius)*sin(obj.start_orientation);
                obj.centerline_radius = lhs_arc_radius + obj.radius;
%                 if (obj.centerline_radius+obj.radius) ~= obj.Rhs_Length/obj.angle
%                     fprintf("Impossible Configuration: Exceeds minimum bend radius")
%                 end
                Xlhs = (lhs_arc_radius*cos(theta)+xcenter);
            Xrhs = (rhs_arc_radius*cos(theta)+xcenter);
            Ylhs = (lhs_arc_radius*sin(theta)+ycenter);
            Yrhs = (rhs_arc_radius*sin(theta)+ycenter);
            Xcenter = obj.centerline_radius*cos(theta)+xcenter;
            Ycenter = obj.centerline_radius*sin(theta)+ycenter;
            else
                if sign(obj.angle)<0
                    xcenter = obj.start_position(1) + (lhs_arc_radius-obj.radius)*cos(obj.start_orientation); 
                    ycenter = obj.start_position(2) + (lhs_arc_radius-obj.radius)*sin(obj.start_orientation);
                    obj.centerline_radius=lhs_arc_radius - obj.radius;
                    Xlhs = (lhs_arc_radius*-cos(theta)+xcenter);
%                     if (obj.centerline_radius+obj.radius) ~= obj.Lhs_Length/obj.angle
%                         fprintf("Impossible Configuration: Exceeds minimum bend radius")
%                     end
            Xrhs = (rhs_arc_radius*-cos(theta)+xcenter);
            Ylhs = (lhs_arc_radius*-sin(theta)+ycenter);
            Yrhs = (rhs_arc_radius*-sin(theta)+ycenter);
            Xcenter = obj.centerline_radius*-cos(theta)+xcenter;
            Ycenter = obj.centerline_radius*-sin(theta)+ycenter;
            
                else 
                    xcenter = inf; %%If the angle is zero (i.e. the segment is straight) use inf.
                    ycenter = inf;
                    %fprintf("This Segment is Straight \n")
                    obj.centerline_radius=inf;
                    Xlhs=[(-obj.radius*cos(obj.start_orientation)+obj.start_position(1)),(-obj.radius*cos(obj.start_orientation)+obj.start_position(1))-obj.height*sin(obj.start_orientation)];
                    Xrhs=[(obj.radius*cos(obj.start_orientation)+obj.start_position(1)),(obj.radius*cos(obj.start_orientation)+obj.start_position(1))-obj.height*sin(obj.start_orientation)];
                
                    Ylhs=[-obj.radius*sin(obj.start_orientation)+obj.start_position(2),-obj.radius*sin(obj.start_orientation)+obj.start_position(2)+obj.height*cos(obj.start_orientation)];
                    Yrhs=[obj.radius*sin(obj.start_orientation)+obj.start_position(2),+obj.radius*sin(obj.start_orientation)+obj.start_position(2)+obj.height*cos(obj.start_orientation)];
                
                end
            end
            
            
            
            X1=[Xlhs(1); Xrhs(1)];
            X2=[Xlhs(end); Xrhs(end)];
            Y1 = [Ylhs(1); Yrhs(1)];
            Y2 = [Ylhs(end); Yrhs(end)];
%             plot(Xlhs,Ylhs,'m');
%             plot(Xrhs,Yrhs,'r');
%             plot(X1,Y1,'c');
%             plot(X2,Y2,'k');
%             plot(Xcenter,Ycenter,'--k')
%             axis 'equal'
%             legend('lhs','rhs','proximal','distal')
            obj.end_position = [(X2(1)+X2(2))/2 (Y2(1)+Y2(2))/2];
            obj.arc_center = [xcenter,ycenter];

        end
        function measurement = measure(segments, pct_h_start, pct_h_end, measurement_location)
            total_length = 0;
            for i=1:length(segments)
                total_length = total_length + segments(i).height;
            end
            
            %% Find the starting segment and calculate how much length that segment adds to the measurement
            LiL=0; %Length in Loop
            j=1;
            while (LiL<total_length*pct_h_start)
                LiL=LiL+segments(j).height;
                j=j+1;
            end
            if j==1
                j=2;
                LiL=segments(1).height;
            end
            
            if j>1
                start_seg=segments(j-1);
                
                if start_seg.height>=pct_h_end*total_length
                    angle_fraction = (pct_h_end*total_length-pct_h_start*total_length)/(start_seg.height);
                    measured_angle = start_seg.angle*angle_fraction;
                    start_length = abs(measured_angle)*(start_seg.centerline_radius-sign(start_seg.angle)*measurement_location*start_seg.radius);
                else
                    
                    length_fraction = (abs(start_seg.angle)-(total_length*pct_h_start-(LiL-start_seg.height))/start_seg.height*abs(start_seg.angle));
                    
                    start_length=length_fraction*(start_seg.centerline_radius-sign(start_seg.angle)*measurement_location*start_seg.radius);
                    
                end
                
            else
                start_seg=segments(j);
                start_length=(abs(start_seg.angle)-(total_length*pct_h_start-(LiL))/start_seg.height*abs(start_seg.angle))*(start_seg.centerline_radius-sign(start_seg.angle)*measurement_location*start_seg.radius);
                
            end
            
            %% Find the end segment and calculate how much of it is being measured.
            LiL=0;
            k=1;
            while (LiL<total_length*pct_h_end)
                LiL=LiL+segments(k).height;
                k=k+1;
            end
            if k~=j
                if k>1
                    end_seg=segments(k-1);
                    
                    end_length= (((total_length*pct_h_end-(LiL-segments(k-1).height))/end_seg.height*abs(end_seg.angle)))*(end_seg.centerline_radius-sign(end_seg.angle)*measurement_location*end_seg.radius);
                    
                    
                else
                    end_seg=segments(k);
                    end_length=(abs(end_seg.angle) -(total_length*pct_h_end-(LiL))/end_seg.height*abs(end_seg.angle))*(end_seg.centerline_radius-sign(end_seg.angle)*measurement_location*end_seg.radius);
                end
            else
                end_length=0;
            end
            measurement = 0;
            if j~=k
                for i=j-1:k-1
                    if i==j-1
                        measurement = measurement+start_length;
                    else
                        if i==k-1
                            measurement = measurement+end_length;
                        else
                            measurement = measurement + abs(segments(i).angle)*(segments(i).centerline_radius-sign(segments(i).angle)*measurement_location*end_seg.radius);
                        end
                    end
                end
            else
                for i=j:k
                    if i==j
                        measurement = measurement+start_length;
                    else
                        if i==k
                            measurement = measurement+end_length;
                        else
                            measurement = measurement + abs(segments(i).angle)*(segments(i).centerline_radius-sign(segments(i).angle)*measurement_location*end_seg.radius);
                        end
                    end
                end
            end
            
        end
    end
end