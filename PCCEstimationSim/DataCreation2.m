Data = [];
for g= 1:13
    ratio=(5+.5*(g-1)); % Vary this from 5-12 in steps of .5
    h = 10; % Constant (Makes Segment lengths easy)
    radius = h/ratio; % Set by the ratio
    for k =1:11
        FirstSegmentLength = (h/4+.5*(k-1)); % Range from 2.5 to 7.5
        SecondSegmentLength = h - FirstSegmentLength; % Second Segment's length is the total length (h) minus the first segs length
        
        inflectionPoint = FirstSegmentLength/h; % Ratio of the length of the first segment to the total length is the inflection pct
        for j = 1:22
            if j<12
                FirstSegmentAngle = -(80-5*j)*pi/180;
            else
                FirstSegmentAngle = (20+5*(j-11))*pi/180;
            end
            % range from -75 to -25, then from 25 to 75
            
            for i=1:22
                if i<12
                    Theta = -(80-5*i)*pi/180;
                else
                    Theta = (20+5*(i-11))*pi/180;
                end
                % The resultant angle across the base to tip of the 2 segments FirstSegmentAngle + SecondSegmentAngle range from -75 to -25 then from 25 to 75
                SecondSegmentAngle = Theta-FirstSegmentAngle;
                Segment1= PCCSeg([0,FirstSegmentAngle,0,0,FirstSegmentLength,radius;]);
                Segment1=Segment1.plotseg;
                Segment1=Segment1.update([],Segment1.Lhs_Length, Segment1.Rhs_Length);
                hold on
                Segment2= PCCSeg([0,SecondSegmentAngle,0,0,SecondSegmentLength,radius;]);
                Segment2=Segment2.update(Segment1,Segment2.Lhs_Length, Segment2.Rhs_Length);
                Segment2=Segment2.plotseg;
                L1=measure([Segment1,Segment2],0,.75,1)/h;
                L2=measure([Segment1,Segment2],.25,1,1)/h;
                Data=[Data; inflectionPoint, FirstSegmentAngle, Theta, L1, L2, ratio];
                [M,~]=size(Data)
            end
        end
    end
end

[m,n]=size(Data);
for i=1:m
    for j=1:n
        if isnan(Data(i,j))
            Data(i,j)=1;
        end
    end
end
Filename = sprintf('Data_%s.csv', datestr(now,'mm-dd-yyyy-MM-HH'))
writematrix(Data,Filename)
