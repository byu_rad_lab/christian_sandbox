import serial
import numpy
import keyboard
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation
#Bottom right port on my mac is /dev/cu.usbmodem144301
#Serial stuff
radius = 3

# This is for the visualization of the changes
fig, ax = plt.subplots()
line, = ax.plot([], [], lw = 2)
ax.set_xlim(0, 10)
ax.set_ylim(0, 20)
ax.set_xlabel("Time (s)")
ax.set_ylabel("H, U, and V")
ax.grid()

times = []
values = []
timestart = time.time()

def init():
    line.set_data([], [])
    return line,

def update(frame):
    timenow = time.time() - timestart
    h, u, v, = recieve() # You really are going to have to parse the data here
    times.append(timenow)
    values.append(h, u, v)
    line.set_data(times, values)
    ax.set_xlim(max(0, timenow - 10), timenow + 1)
    ax.set_ylim(min(values) - 1, max(values) + 1)

    ani = animation.FuncAnimation(fig, update, init_func=init, interval=100, blit = True)
    plt.show

# Here we actually start recieving data from the arduino
def recieve():
    ser = serial.Serial('/dev/cu.usbmodem144301', 9600)

    print ("Press x to end transmission")

    while True:
        if keyboard.is_pressed("x"): #This needs to change
            print("Ending Transmission")
            break
        elif ser.in_waiting() > 0:
            data = ser.readline()
            print(data) # Don't need this
            #Get data and parse it into h, u, and v

            time.sleep(0.1)
        else:
            print("Error with function")
            return
        
    
    

# Here are the calculations
def hpos(l1, l2, l3, l4):
    return 0.25*(l1+l2+l3+l4)

def upos(l2, l4):
    return (l2-l4)/radius

def vpos(l1, l3):
    return (l3-l1)/radius

def pos(l1, l2, l3, l4):
    h = hpos(l1, l2, l3, l4)
    u = upos(l2, l4)
    v = vpos(l1, l3)
    return h, u, v


# h = str(hpos())
# u = str(upos())
# v = str(vpos())

# print ("Position of u: " + u)
# print ("Position of h: " + h)
# print ("Position of v: " + v)
# If you use the readlines function on there serial port, 
# you have to make sure you use a end line character in your arduino code. 