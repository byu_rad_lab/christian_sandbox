"""This code will take a message of 4 numbers seperated with ', ' and return 
H, U, and V. You can update the 0 position by clicking '0' on your keyboard.
Terminate the program with 'x'."""

import serial
import time
from pynput import keyboard

zero = [0, 0, 0]

def get():
    data = ser.readline()
    return data

def parse(string):
    string = string.decode('utf-8').strip()
    numbers = [int(num) for num in string.split(", ")]
    return numbers

def lengths(value, minr, maxr, mind, maxd):
    volt = value * (5/1023) #The 1023 could change based on the ADC. 
    return (volt - minr) * (maxd - mind) / (maxr - minr) + mind #Maps voltage to distance
#Right now, the values I'm using are maxr = 4.66, minr = 0.205, maxd = 466.725mm, mind = 0

def lenlist(list): #Creates a list of all the newly calculated lengths
    new = []
    for resistance in list:
         val = lengths(resistance, 0.205, 4.66, 0, 466.725)
         new.append(val)
    return new

def calculate(list, radius): #Taking the lengths and calculating the H U and V Values
        HUV = []
        h = 0.25 * sum(list)
        u = (list[1] - list[3])/radius
        v = (list[0] - list[2])/radius
        HUV.append(h)
        HUV.append(u)
        HUV.append(v)
        return HUV

def change(zero, total):
     diff = []
     if len(zero) != 3:
          raise ValueError("Make sure you are using a list with H, U, and V!")
     else:
          for i in range(0, 3):
               diff.append(total[i] - zero[i])
     return diff

def on_press(key):
    global zero
    try:
        if key.char == '0':
            print("Setting zero point...")
            the0 = parse(get())
            result = lenlist(the0)
            zero = calculate(result, 30)
            print(f"Zero point set: {zero}")
        elif key.char == 'x':
            print("Terminating the program.")
            listener.stop()
            ser.close()
            exit(0)  # Terminate the program
    except AttributeError:
        pass

listener = keyboard.Listener(on_press=on_press) # This will call the on_press function whenever a key is pressed. 
listener.start()
     
ser = serial.Serial('/dev/cu.usbmodem144201', 9600)

try:
    while True:
        if ser.in_waiting > 0:
            mess = parse(get())
            dist = lenlist(mess)
            HUV = calculate(dist, 30) # The radius is 30 here. It should be how far to the bellows.
            difference = change(zero, HUV)
            print(f"HUV: {HUV}, Difference: {difference}")
            time.sleep(0.1)
        else:
            time.sleep(0.1)
except KeyboardInterrupt:
    print("Terminating the program.")
finally:
    ser.close()
    listener.stop()

# test = [936, 10, 760, 500]
# lengths = lenlist(test)
# print (lengths)
# H, U, V = calculate(lengths, 30)
# print("H is: " + str(H) + "U is: " + str(U) + "V is: " + str(V))
    