#include <Wire.h>
#include <Adafruit_ADS1X15.h>

Adafruit_ADS1015 ads1115;

void setup(void)
{
  Serial.begin(9600);
  ads1115.begin();
}

void loop(void)
{
  int16_t adc0, adc1, adc2, adc3;

  adc0 = ads1115.readADC_SingleEnded(0);
  adc1 = ads1115.readADC_SingleEnded(1);
  adc2 = ads1115.readADC_SingleEnded(2);
  adc3 = ads1115.readADC_SingleEnded(3);
  Serial.print(adc0);
  Serial.print(", "); Serial.print(adc1);
  Serial.print(", "); Serial.print(adc2);
  Serial.print(", "); Serial.print(adc3);
  Serial.println(" ");
  
  delay(100);
}