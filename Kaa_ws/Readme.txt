This ws should contain everything needed to run estimation for Kaa.  It was created Nov 2020 by Christian Sorensen to dettach our code from the pneubotics code.

The first step of setup will be to build the cur_devel workspaces in the following order: (I will add a script to do it, but as of Nov 30th its not done)
Catkin_make order cur_devel:
third_party
models
control
experiments
sensors
utils (making sure to source models)
estimation(making sure to source utils)
robots (making suret to source estimation)

Next go to the root of the workspace and build it.

i.e. cd Kaa_ws catkin_make

After take the python folder from ego-projects-byu and add it to the devel folder in Kaa_ws.

Finally add the following line to the end of the setup.bash file in the devel folder: (Best to do this in an alias whenever you want to source the workspace, I think every time you catkin_make this file gets overwritten)

export PYTHONPATH=$PYTHONPATH:<path_to_ws>/Kaa_ws/src/python on my system its export PYTHONPATH=$PYTHONPATH:~/christian_sandbox/Kaa_ws/src/python/ego_byu

This will make the python modules available.

I changed the names in the python modules so that they are importing kinematics out of cur_devel instead of looking for ego_core

Each of the swig modules needs to be built in the python library. 

sourcing Kaa_ws doesn't source everything, I need to --extend all of cur_devel




