%module armeval

%{
  #define SWIG_FILE_WITH_INIT
  #include "yaml-cpp/yaml.h"
  #include "kinematics/types.h"
  #include "kinematics/partials.h"
  #include "kinematics/joint_limits.h"
  #include "kinematics/joint_base.h"
  #include "kinematics/joint_ccc.h"
  #include "kinematics/joint_cc1d.h"
  #include "kinematics/joint_rot.h"
  #include "kinematics/link.h"
  #include "kinematics/sdls.h"
  #include "kinematics/arm.h"
  #include "kinematics/quadruped.h"
  #include "kinematics/arm_ik.h"
  #include "kinematics/yaml.h"
  #include "kinematics/gyro_bias_estimator.h"
  #include "primitives/smartenums.h"
  #include <time.h>
  #include <vector>


  // #include "armeval/arm_eval.h"
  #include "armeval/arm_eval_fk.h"
%}

%include <numpy.i>
%include <std_vector.i>

/* For Vectors */
%template(DoubleVector) std::vector<double>;

%include <eigen.i>
%ignore orientation_map_;
typedef long time_t;

%init %{
  import_array();  // required for Numpy
%}

// this has to come after import_array
%import "/home/cs574296/christian_sandbox/Kaa_ws/src/cur_devel/models/src/kinematics/swig/kinematics.i"

// %include "armeval/arm_eval.h"
%include "/home/cs574296/christian_sandbox/Kaa_ws/src/ego-projects-byu/src/armeval/include/armeval/arm_eval_fk.h"
