#!/usr/bin/env python

from ego_byu.madgwick import madgwick as md

FilterIterations = 10000
FRAME = md.NED
mfilter = md.ImuFilter()
mfilter.setWorldFrame(FRAME)
mfilter.setDriftBiasGain(0.0)
mfilter.setAlgorithmGain(0.1)
mfilter.setOrientation(.5, .5, .5, .5)
q0 = 0
q1 = 0
q2 = 0
q3 = 0

q = mfilter.returnOrientation()


Gx = 0
Gy = 0
Gz = 0

Ax = 0
Ay = 0
Az = 9.81
dt = .1

for i in range(FilterIterations):
    mfilter.madgwickAHRSupdateIMU(Gx, Gy, Gz, Ax, Ay, Az, dt)

q0 = 0
q1 = 0
q2 = 0
q3 = 0
q = mfilter.returnOrientation()

print q
