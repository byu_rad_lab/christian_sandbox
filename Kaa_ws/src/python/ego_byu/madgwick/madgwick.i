%module madgwick

%{
  #define SWIG_FILE_WITH_INIT
  #include "yaml-cpp/yaml.h"
  #include "imu_filter_madgwick/imu_filter.h"
  // #include "imu_filter_madgwick/imu_filter_nodelet.h"
  // #include "imu_filter_madgwick/imu_filter_ros.h"
  // #include "imu_filter_madgwick/stateless_orientation.h"
  #include "imu_filter_madgwick/world_frame.h"
  #include <time.h>
  #include <vector>
  #include <vector>


  // #include "armeval/arm_eval.h"
  // #include "armeval/arm_eval_fk.h"
%}

%include <numpy.i>
%include <std_vector.i>

/* For Vectors */
%template(DoubleVector) std::vector<double>;

%include <eigen.i>
typedef long time_t;

%init %{
  import_array();  // required for Numpy
%}

// this has to come after import_array

%include "imu_filter_madgwick/imu_filter.h"
%include "imu_filter_madgwick/world_frame.h"
