 # SWIG 1.3 is not enough because some unit tests failed with this version
 # Not sure this all of this is required, but it works.

CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

SET(CMAKE_BUILD_TYPE Release)  # sets -O3 optimization, removes debuggin
# SET(CMAKE_BUILD_TYPE Debug)

LIST(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR} )

FIND_PACKAGE(SWIG 3.0 REQUIRED)
FIND_PACKAGE(PythonLibs 2.7 REQUIRED)
FIND_PACKAGE(NumPy REQUIRED)
FIND_PACKAGE(Eigen3 REQUIRED)

INCLUDE(${SWIG_USE_FILE})

INCLUDE_DIRECTORIES(/usr/include)
# INCLUDE_DIRECTORIES(/usr/include/yaml-cpp)
INCLUDE_DIRECTORIES(${PYTHON_INCLUDE_PATH})
INCLUDE_DIRECTORIES(${NUMPY_INCLUDE_DIR})
INCLUDE_DIRECTORIES(${EIGEN3_INCLUDE_DIR})
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR})

INCLUDE_DIRECTORIES(
    ${CMAKE_CURRENT_SOURCE_DIR}/../../../ego-projects-byu/src/armeval/include
    ${CMAKE_CURRENT_SOURCE_DIR}/../../../ego-projects-byu/src/path_planner/include
    ${CMAKE_CURRENT_SOURCE_DIR}/../../../cur_devel/models/src/kinematics/include
    ${CMAKE_CURRENT_SOURCE_DIR}/../../../ego-projects-byu/src/cvxgen/include
    ${CMAKE_CURRENT_SOURCE_DIR}/../../../cur_devel/models/src/primitives/include
    $ENV{EGO_CORE})

# SET(CMAKE_CXX_FLAGS "-std=c++0x")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wpedantic -Wextra")

# --- SWIG MODULES ---
SET(CMAKE_SWIG_FLAGS "")

SET_SOURCE_FILES_PROPERTIES(path_planner.i PROPERTIES CPLUSPLUS ON)
SET_SOURCE_FILES_PROPERTIES(path_plannerPYTHON_wrap.cxx PROPERTIES COMPILE_FLAGS -Wno-pedantic)

link_directories(${CMAKE_CURRENT_SOURCE_DIR}/../../../ego-projects-byu/devel/lib) 

SWIG_ADD_MODULE(path_planner python path_planner.i)
SWIG_LINK_LIBRARIES(path_planner path_planner)

# dummy target to get copy command below to work
ADD_CUSTOM_TARGET(runpath_planner ALL DEPENDS demo_path_planner.py)

# copy file into build directory for workflow purposes
ADD_CUSTOM_COMMAND(
  OUTPUT demo_path_planner.py
  COMMAND ${CMAKE_COMMAND} -E copy
    ${CMAKE_CURRENT_SOURCE_DIR}/demo_path_planner.py demo_path_planner.py
  DEPENDS demo_path_planner.py
  )
