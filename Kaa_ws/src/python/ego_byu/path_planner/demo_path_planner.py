#/usr/bin/env python
from ego_core.kinematics import kinematics as kin
from ego_byu.path_planner import path_planner as pp
# from ego_byu.tools import arm_tools as at
import numpy as np

# height = 0.0
# lengths = np.array([1.0,1.0,1.0])
# limit = np.pi/2
# arm = at.simple_arm(height,lengths)
# for i in range(len(arm.links_)):
#     arm.links_[i].joint_.limits().setLimitsScalar(limit)
# arm.calcFK()
arm = kin.Arm()

mypathplanner = pp.PathPlanner(arm)
g_goal = np.eye(4)
g_goal[0,3] = 1
g_goal[1,3] = -1.5
print "Goal"
print g_goal

mypathplanner.qpik_.Solve(g_goal)
print
print mypathplanner.qpik_.status_
print "G_WORLD_TOOL"
print mypathplanner.arm_.getGWorldTool()
print "JOINT ANGS"
print mypathplanner.arm_.getJointAngs()


