#!/usr/bin/env python

from ego_byu.cvxgen import cvxgen
from ego_core.kinematics import kinematics as kin
import numpy as np

P = np.array([[1.0,0],[0,1.0]])
B = np.array([0,0.0]).T
G = np.array([[-1.0,0],[1,0],[0,-1],[0,1]])
limit = 1.0
h = np.ones(4).T*limit

mycvxgen = cvxgen.CVXGen(P,B,G,h)
mycvxgen.PrintParams()

mycvxgen.Solve()
print "Solution"
print mycvxgen.x_

