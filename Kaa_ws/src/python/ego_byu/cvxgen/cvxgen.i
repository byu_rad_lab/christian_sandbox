%module cvxgen

%{
  #define SWIG_FILE_WITH_INIT

  #include "yaml-cpp/yaml.h"
  #include "kinematics/types.h"
  #include "kinematics/partials.h"
  #include "kinematics/joint_base.h"
  #include "kinematics/joint_ccc.h"
  #include "kinematics/joint_cc1d.h"
  #include "kinematics/joint_rot.h"
  #include "kinematics/link.h"
  #include "kinematics/arm.h"
  #include "kinematics/sdls.h"
  #include "kinematics/arm_ik.h"
  #include "kinematics/yaml.h"
  #include "kinematics/gyro_bias_estimator.h"
  #include "primitives/smartenums.h"

  // #include "armeval/arm_eval.h"
  // #include "armeval/arm_eval_fk.h"
  #include "cvxgen/cvxgen.h"
%}

%include <std_vector.i>
%include <eigen.i>

%init %{
  import_array();  // required for Numpy
%}

// this has to come after import_array
%import "/home/cs574296/christian_sandbox/Kaa_ws/src/cur_devel/models/src/kinematics/swig/kinematics.i"

// %include "armeval/arm_eval.h"
%include "/home/cs574296/christian_sandbox/Kaa_ws/src/ego-projects-byu/src/cvxgen/include/cvxgen/cvxgen.h"
