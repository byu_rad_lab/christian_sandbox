#!/usr/bin/env python
import matplotlib.pyplot as plt
import kinematics as kin
import numpy as np
import rospy
import os
# print "PYTHONPATH IS (Constructors):"
# print os.environ["PYTHONPATH"]
# raw_input("break")
from ego_byu.tools import arm_plotter as ap
from ego_byu.tools import arm_tools as at
from ego_byu.tools import kin_tools as kt
# from ego_byu.tools import arm_plotter as ap
import tf.transformations as tft
import analysis
import rospy
from geometry_msgs.msg import PoseStamped
from copy import deepcopy


class Base(object):

    def __init__(self, JBase, constraints, g_world_base, g_base_arm):
        self.JBase = JBase
        self.constraints = constraints
        self.bdof = np.shape(JBase)[1]
        self.base_center = np.array([])
        self.dmax = .005  # Around .003 Depending on accuracy of Initial Guess
        for i in range(self.bdof):
            bound_lower = -constraints[i][0]
            bound_upper = constraints[i][1]
            self.base_center = np.append(self.base_center, (bound_lower+bound_upper)/2.0)

        self.g_world_base = g_world_base
        self.g_base_arm = g_base_arm

# 2D


def SimpleArms(narms, base):
    arms = []
    height = 0
    lengths = np.array([.2, .2, .2, .2])
    angs0 = np.ones(len(lengths))*.1
    g_world_arm0 = np.dot(base.g_world_base, base.g_base_arm)
    for i in range(narms):
        arm = at.simple_arm(height, lengths)
        g_world_arm = g_world_arm0
        arm.g_world_arm_ = g_world_arm
        arm.setJointAngsReal(angs0)
        arm.calcFK()
        arm.calcJac()
        arms.append(arm)
    return arms

# 3D
def NasaArms(narms, base, angs0=np.ones(6)*.1):
    arms = []
    height = .06
    lengths = np.array([.0, .467, .286, .164, .210, .00])
    g_world_arm0 = np.dot(base.g_world_base, base.g_base_arm)
    for i in range(narms):
        arm = at.nasa_arm(height, lengths, g_world_arm0)
        arm.setJointAngsReal(angs0)
        arm.calcJac()
        arms.append(arm)
    return arms

def CustomArms(narms, base, height, lengths, angs0=np.ones(6)*.1):
    arms = []
    g_world_arm0 = np.dot(base.g_world_base, base.g_base_arm)
    for i in range(narms):
        arm = at.nasa_arm(height, lengths, g_world_arm0)
        arm.setJointAngsReal(angs0)
        arm.calcJac()
        arms.append(arm)
    return arms

def YAMLArms():
    narms = rospy.get_param("state_commander/ArmsInPlanner")
    arms = []
    for i in range(narms):
        arm = at.ArmFromYAML()
        arms.append(arm)
    return arms

def BaxterArms(narms,base_hom,angs0=np.ones(7)*.001):
    arms = []
    for i in range(narms):
        arm = at.baxter_arm(base_hom)
        arm.setJointAngsReal(angs0)
        arm.calcJac()
        arms.append(arm)
    return arms


def WrapArms(narms, base, angs0=np.ones(6)*.1):
    arms = []
    height = .06
    lengths = np.array([.10,0,.10,0,.10,0,.10,0,.10,0])
    g_world_arm0 = np.dot(base.g_world_base, base.g_base_arm)
    for i in range(narms):
        arm = at.nasa_arm(height, lengths, g_world_arm0)
        arm.setJointAngsReal(angs0)
        arm.calcJac()
        arms.append(arm)
    return arms

#Sample model representing truth
def NasaArmsTrue(narms, base, angs0=np.ones(6)*.1):
    arms = []
    height = .058
    lengths = np.array([.02, .450, .270, .16, .220, .02])
    g_world_arm0 = np.dot(base.g_world_base, base.g_base_arm)
    for i in range(narms):
        arm = at.nasa_arm(height, lengths, g_world_arm0)
        arm.setJointAngsReal(angs0)
        arm.calcJac()
        arms.append(arm)
    return arms


def NasaArms4DOF(narms, base):
    arms = []
    height = .07
    lengths = np.array([.0, .467, .286, 0])
    angs0 = np.ones(len(lengths))*.1
    g_world_arm0 = np.dot(base.g_world_base, base.g_base_arm)
    for i in range(narms):
        arm = at.nasa_arm(height, lengths, g_world_arm0)
        arm.setJointAngsReal(angs0)
        arm.calcJac()
        arms.append(arm)
    return arms


def BlowMoldedArms(narms):
    case = 1
    arms = []
    arm = kin.Arm()
    angs = np.zeros(8)
    info_list = analysis.create_info_list(case, 4)
    # joint limit parameters
    center = np.zeros(2)
    nc = 16  # number of constraints
    phase = 0.0
    for info in info_list:
        link = kin.Link(kin.JointCCC(info.joint_length))
        link.setGTopEndXYZ(0.0, 0.0, info.link_length)
        link.joint().limits().setLimitsCircular(info.joint_limit, center, nc, phase)
        arm.addLink(link)
    g_end_tool = np.eye(4)
    # r = np.zeros(3)
    r = np.array([0, np.pi/4.0, 0])
    g_world_arm = kt.ax_vec2homog(r, np.zeros(3))
    arm.g_world_arm_ = g_world_arm
    arm.set_g_end_tool(g_end_tool)
    arm.setJointAngsReal(angs)
    arm.calcFK()
    arm.calcJac()
    for i in range(narms):
        arm = deepcopy(arm)
        arms.append(arm)
    return arms

# Base Options


def BaseXYZ():
    xlimits = np.array([-10, 10])  # Limits on region where base can be
    ylimits = np.array([-10, 10])
    zlimits = np.array([-10, 10])
    JBase = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1], [0, 0, 0], [0, 0, 0], [0, 0, 0]])
    constraints = [xlimits, ylimits, zlimits]

    # Base Rotation and Starting Position
    r = np.array([0, 0, 0])
    # t = np.array([.2, 1.297/2.0-.1, .6])
    t = np.array([0, 0, 0])
    g_world_base = kt.ax_vec2homog(r, t)

    # Optional Offset of Arm
    r = np.array([0, 0, 0])
    t = np.array([0, 0, 0])
    g_base_arm = kt.ax_vec2homog(r, t)
    return Base(JBase, constraints, g_world_base, g_base_arm)


def BaseXY():
    ylimits = np.array([-1, 1])
    xlimits = np.array([-1, 1])  # Limits from zero
    JBase = np.array([[1, 0], [0, 1], [0, 0], [0, 0], [0, 0], [0, 0]])
    constraints = [xlimits, ylimits]
    # Base Rotation and Starting Position
    r = np.array([0, 0, 0.0])
    # t = np.array([.2, 1.297/2.0-.1, .6])
    t = np.array([0, 0, .5])
    g_world_base = kt.ax_vec2homog(r, t)

    # Optional Offset of Arm
    r = np.array([0, 0, 0])
    t = np.array([0, 0, 0])
    g_base_arm = kt.ax_vec2homog(r, t)
    return Base(JBase, constraints, g_world_base, g_base_arm)


def BaseX():
    xlimits = np.array([0, 1])  # Limits from zero
    JBase = np.array([[1], [0], [0], [0], [0], [0]])
    constraints = [xlimits]
    # Base Rotation and Starting Position
    r = np.array([0, 0, 0])
    # t = np.array([.2, 1.297/2.0-.1, .6])
    t = np.array([0, 0, 0])
    g_world_base = kt.ax_vec2homog(r, t)

    # Optional Offset of Arm
    r = np.array([0, 0, 0])
    t = np.array([0, 0, 0])
    g_base_arm = kt.ax_vec2homog(r, t)
    return Base(JBase, constraints, g_world_base, g_base_arm)

def BaseKrex():
    JBase = np.empty([6, 0])
    constraints = []
    # Base Rotation and Starting Position
    r = np.array([0, 0, 0])
    t = np.array([0, 0, .8])
    g_world_base = kt.ax_vec2homog(r, t).dot(kt.ax_vec2homog(np.array([0, np.pi/2.0, 0]), np.zeros(3)))

    # Optional Offset of Arm
    r = np.array([0, 0, 0])
    t = np.array([0, 0, 0])
    # g_base_arm = kt.ax_vec2homog(r,t).dot(kt.ax_vec2homog(np.array([0,0,-np.pi/2.0]),np.zeros(3)))
    g_base_arm = kt.ax_vec2homog(r, t)
    return Base(JBase, constraints, g_world_base, g_base_arm)

def FixedKrex(g_world_base):
    JBase = np.empty([6, 0])
    constraints = []
    r = np.array([0, 0, 0])
    t = np.array([0, 0, 0])
    g_base_arm = kt.ax_vec2homog(r, t)
    return Base(JBase, constraints, g_world_base, g_base_arm)

def MobileKrex(g_world_base):
    ylimits = np.array([-1, 1])
    xlimits = np.array([-1, 1])  # Limits from zero
    thlimits = np.array([-np.pi, np.pi])  # Limits from zero
    JBase = np.array([[1,0,0], [0,1,0], [0,0,0], [0,0,0], [0,0,0], [0,0,1]])
    constraints = [xlimits, ylimits,thlimits]
    r = np.array([0, 0, 0])
    t = np.array([0, 0, 0])
    g_base_arm = kt.ax_vec2homog(r, t)
    return Base(JBase, constraints, g_world_base, g_base_arm)

def BaseFixed(g_world_base):
    JBase = np.empty([6, 0])
    constraints = []
    # Base Rotation and Starting Position
    r = np.array([0, 0, 0])
    t = np.array([.3, -.1, .6])
    g_world_base = kt.ax_vec2homog(r, t).dot(kt.ax_vec2homog(np.array([0, 0, 0]), np.zeros(3)))

    # Optional Offset of Arm
    r = np.array([0, 0, 0])
    t = np.array([0, 0, 0])
    # g_base_arm = kt.ax_vec2homog(r,t).dot(kt.ax_vec2homog(np.array([0,0,-np.pi/2.0]),np.zeros(3)))
    g_base_arm = kt.ax_vec2homog(r, t)
    return Base(JBase, constraints, g_world_base, g_base_arm)


class BaseCortex():

    def __init__(self):
        self.got_data = False
        self.g = []
        self.posBase = []
        self.quatBase = []

        rate = 10.0
        self.rate = rospy.Rate(rate)
        rospy.Subscriber('/base/base', PoseStamped, self.BaseCallback)

    def BaseCallback(self, msg):
        self.posBase = [msg.pose.position.x, msg.pose.position.y, msg.pose.position.z]
        self.quatBase = [
            msg.pose.orientation.x,
            msg.pose.orientation.y,
            msg.pose.orientation.z,
            msg.pose.orientation.w]

    def SetGWorldBase(self):
        while not rospy.is_shutdown():
            if self.quatBase != [] and self.posBase != []:
                self.got_data = True
                self.g = np.zeros([4, 4])
                self.g[0:3, 0:3] = tft.quaternion_matrix(self.quatBase)[0:3, 0:3]
                self.g[0:3, 3] = np.array(self.posBase)
                print "G_world_base obtained from cortex:"
                print self.g
                if np.any(self.g[0:3, 3] > 20):
                    print "Bad Cortex Reading"
                    assert(False)

            # print "check for data"
            if not self.got_data:
                print "Not recieveing g_world_base from cortex"
            else:
                break
            self.rate.sleep()


def BaseFixedCortex():
    JBase = np.empty([6, 0])
    constraints = []

    temp = BaseCortex()
    temp.SetGWorldBase()
    g_world_base = temp.g

    # Optional Offset of Arm: Used to find difference between Cortex rotation of base frame and Levi's
    r = np.array([0, 0, 0])
    t = np.array([0, 0, 0])
    g_base_arm = kt.ax_vec2homog(r, t)
    g_base_arm = g_base_arm.dot(kt.ax_vec2homog(np.array([0, np.pi/2.0, 0]), np.zeros(3)))
    g_base_arm = g_base_arm.dot(kt.ax_vec2homog(np.array([0, 0, np.pi]), np.zeros(3)))

    return Base(JBase, constraints, g_world_base, g_base_arm)


def TestBase(base):
    plt.figure(1)
    ax = plt.gca(projection="3d")
    ap.plot_gnomon(ax, base.g_world_base)
    ap.plot_gnomon(ax, np.eye(4), .1, 7, ['k', 'k', 'b'])  # WorldFrame
    ap.set_axes_equal(ax)
    plt.show()


def TestArmSetUp(arm):
    plt.figure(1)
    ax = plt.gca(projection="3d")
    plotter = ap.SimpleArmPlotter(arm)
    plotter.plot(ax)
    ap.plot_gnomon(ax, np.eye(4), .1, 7, ['k', 'k', 'b'])  # WorldFrame
    ap.set_axes_equal(ax)
    plt.show()



if __name__ == "__main__":
    base = BaseFixed()
    angs0 = np.array([0.1, 1, 0, 0, 0, 0])
    narms = 10
    arms = NasaArms(narms, base, angs0)
    TestArmSetUp(arms[0])
