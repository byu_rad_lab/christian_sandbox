from spliner import CtrlPt, Spliner

# import numpy as np

# class HighVelocityPlanner(object, g_world_start, g_world_target):

#     def __init__(self, order=3):
#         self.v_end = 0.02
#         self.t0 = 1.5
#         self.order = order
#         self.gpt1 = g_world_start
#         self.gpt2 = g_world_target
#         self.v_start = 0.02
#         self.v_contact = 0.5

#     def create_path(self):
#         self.path = []
#         self.path += CtrlPt.from_dict({
#             0: [gpt1[0,3], gpt1[1,3],gpt2[2,3]],
#             1: [ v

#         # Set starting and ending velocities to zero
#         self.path[0].goals[1].fill(0.0)
#         # self.path[-1].goals[1].fill(0.0)
#         self.path[-1].goals[1][1]=0

#     def plan(self):
#         self.spliner = Spliner(self.path, self.order)
#         self.spliner.t0 = self.t0
#         self.spliner.estimate_times()
#         self.spliner.solve()

#     def optimize_time(self, num_iter):
#         for i in range(num_iter):
#             self.spliner.optimize_time()

#     def create_row(self):
#         x1 = x0 + dx
#         x2 = xf - dx

#         y = self.y_bottom + row_num * self.y_delta

#         pts = []

#         # start of row
#         pt = CtrlPt.from_dict({
#             0: [x0, y, 0.0],
#             1: [v, 0.0, 0.0]
#         })
#         pt.fill_in_zero(self.order)
#         pts.append(pt)

#         # add some middle points to help control velocity
#         pts.append(CtrlPt.from_pos([x1, y, 0.0])),
#         pts.append(CtrlPt.from_pos([x2, y, 0.0])),

#         # end of row
#         pt = CtrlPt.from_dict({
#             0: [xf, y, 0.0],
#             1: [v, 0.0, 0.0]
#         })
#         pt.fill_in_zero(self.order)
#         pts.append(pt)
#         return pts

class CouponPlanner(object):

    def __init__(self, order=3,dx = .225,dy =.10 ):
        # self.x_left = -0.25
        # self.x_right = 0.25
        # self.y_bottom = 0.0
        # self.y_delta = 0.10

        self.x_left = -dx
        self.x_right = dx
        self.y_bottom = 0.0
        self.y_delta = dy

        self.v_end = 0.02
        self.t0 = 1.5
        self.order = order
        self.num_passes = 4

        self.x_offset = 0.1 * (self.x_right - self.x_left)

    def create_path(self):
        self.path = []
        for i in range(self.num_passes):
            self.path += self.create_row((i+1) % 2, i)

        # Set starting and ending velocities to zero
        self.path[0].goals[1].fill(0.0)
        self.path[-1].goals[1].fill(0.0)
        # self.path[-1].goals[1][1]=0

    def plan(self):
        self.spliner = Spliner(self.path, self.order)
        self.spliner.t0 = self.t0
        self.spliner.estimate_times()
        self.spliner.solve()

    def optimize_time(self, num_iter):
        for i in range(num_iter):
            self.spliner.optimize_time()

    def create_row(self, is_right, row_num):
        if is_right:
            v = self.v_end
            x0 = self.x_left
            xf = self.x_right
            dx = self.x_offset
        else:
            v = -self.v_end
            x0 = self.x_right
            xf = self.x_left
            dx = -self.x_offset

        x1 = x0 + dx
        x2 = xf - dx

        y = self.y_bottom + row_num * self.y_delta

        pts = []

        # start of row
        pt = CtrlPt.from_dict({
            0: [x0, y, 0.0],
            1: [v, 0.0, 0.0]
        })
        pt.fill_in_zero(self.order)
        pts.append(pt)

        # add some middle points to help control velocity
        pts.append(CtrlPt.from_pos([x1, y, 0.0])),
        pts.append(CtrlPt.from_pos([x2, y, 0.0])),

        # end of row
        pt = CtrlPt.from_dict({
            0: [xf, y, 0.0],
            1: [v, 0.0, 0.0]
        })
        pt.fill_in_zero(self.order)
        pts.append(pt)
        return pts

class AngleSmoothener(object):

    def __init__(self, order, pathconfigs):
        self.order = order
        self.pathconfigs = pathconfigs
        self.vmax = .02
        self.t0 = 1.5


    def plan(self):
        self.spliner = Spliner(self.path, self.order)
        self.spliner.t0 = self.t0
        self.spliner.estimate_times()
        self.spliner.solve()

    def optimize_time(self, num_iter):
        for i in range(num_iter):
            self.spliner.optimize_time()

    def create_path(self):
        self.path = []

        pts = []

        # start of row
        pt = CtrlPt.from_dict({
            0: self.pathconfigs[0].flatten(),
            1: [self.vmax for i in range(len(self.pathconfigs[0].flatten()))]
        })
        pt.fill_in_zero(self.order)
        pts.append(pt)

        # add some middle points to help control velocity
        for config in range(1,len(self.pathconfigs)-1):
            pts.append(CtrlPt.from_pos(self.pathconfigs[config].flatten()))

        # end of row
        pt = CtrlPt.from_dict({
            0: self.pathconfigs[-1].flatten(),
            1: [self.vmax for i in range(len(self.pathconfigs[0].flatten()))]
        })
        pt.fill_in_zero(self.order)
        pts.append(pt)

        # Set starting and ending velocities to zero
        pts[0].goals[1].fill(0.0)
        pts[-1].goals[1].fill(0.0)
        self.path = pts
