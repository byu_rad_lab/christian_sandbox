#!/usr/bin/env python
from __future__ import division
import matplotlib.pyplot as plt
import numpy as np
from ego_byu.tools import pickle_tools as pk
from ego_byu.tools import pickle_tools as pk
from path_planner import AngleSmoothener

np.set_printoptions(precision=3)
np.set_printoptions(suppress=True)
np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
np.set_printoptions(linewidth=190)

order = 3
# order = 4
# order = 7  # turns out very poorly

configs = pk.load_pickle("joint_configs")
planner = AngleSmoothener(order,configs)
planner.create_path()
planner.plan()
planner.optimize_time(4)

# # planner.optimize_time(20)

sp = planner.spliner
# # num_time_grid = 10
# # sp.calc_maxes(num_time_grid)  # to print last solution

# # Analyize numerically
num_grid = 50
# # num_grid = 500

time, state = sp.discretize(num_grid)

# print np.shape(time)

num_dof = sp.num_dof
colors = 'rgbymc'

plot_state = False
plot_state = True
if plot_state:
    plt.figure()
    # plt.title('States vs. Time')
    axes = []
    for i in range(num_dof):
        if i == 0:
            ax = plt.subplot(num_dof, 1, i+1)
            plt.title('States as a function of time.  States are in rgbymc order.')
        else:
            ax = plt.subplot(num_dof, 1, i+1, sharex=axes[0])
        axes.append(ax)

        for j in reversed(range(order+1)):  # plot position on top
            c = colors[j % len(colors)]
            # Plot state and derivatives
            options = {'color': c, 'lw': 3}
            plt.plot(time, state[j][:, i], **options)

            # Plot constraints
            options = {'color': c, 'marker': 's', 'ms': 6}
            for p in sp.points:
                if j in p.goals:
                    g = p.goals[j]
                    plt.plot(p.t, g[i], **options)

        ax.set_ylabel('state %d' % i)
        ax.set_xlim([time[0], time[-1]])
    ax.set_xlabel('Time [s]')  # bottom axis


plt.show()
