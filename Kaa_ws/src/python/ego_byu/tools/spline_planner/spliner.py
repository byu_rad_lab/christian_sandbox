import numpy as np

from spline_helpers import is_even, PathSeg, TimePt
from spline_helpers import CtrlPt  # NOQA


# Based on wikipedia and Farin:
# Wikipedia:  https://en.wikipedia.org/wiki/De_Boor%27s_algorithm
#   appears to have a mistake:
#     "set d_i^[0] = N_i^[0](x)" where N_i^[0] is either 1 or zero
#   I think it should be:
#     d_i^[0] = d_i - your control point."
class Spliner(object):

    # make sure order and k are set first
    def _initialize_arrays(self):
        # basis is a list of matrices for calculating basis functions
        self.basis = [np.zeros((self.order+1, self.order+1)) for _ in range(self.order+1)]
        self.basis[0][0, -1] = 1.0

        # Matrix for executing De Boor's algorithm
        self.dk = np.zeros((self.order+1, self.order+1, self.num_dof))

        # Constraint matrix:  p = A * d, where:
        #   p are goal points (pos, vel, acc, etc.)
        #   d are control points
        self.A = np.zeros((self.k, self.k))
        self.goals = np.zeros((self.k, self.num_dof))
        # self.time_segs = [TimeSeg(1.0) for _ in range(self.k - self.order)]
        self.time_pts = [TimePt(1.0) for _ in range(self.k)]
        self.path_segs = []

    def _setup_calcs(self):
        self.num_dof = self.points[0].get_goal_val(0).size
        self.points[0].fill_in_zero(self.order)
        self.points[-1].fill_in_zero(self.order)
        self.k = sum([p.num_goals() for p in self.points])  # number of DOF/constraints
        self._initialize_arrays()

    # rename to something about time?
    def _create_u(self):
        # made delta u vector for each time segment (except the last few)
        end = self.k - self.order  # ignore the last few points
        self.du = np.hstack((self.t0, [ts.dt for ts in self.time_pts[:end]]))
        self.u = np.cumsum(self.du)
        self.up = np.hstack((  # u, but padded
            np.arange(-self.order, 0, dtype=float) + self.u[0],
            self.u,
            np.arange(1, self.order + 2, dtype=float) + self.u[-1],
        ))

        for p in self.points:
            c0 = p.idx_start[0]
            c1 = p.idx_start[1]
            p.t = 0.5 * (self.u[c0] + self.u[c1])  # save for plotting

    def _calc_indices(self):
        row = 0
        for p in self.points:
            col = row + (p.num_goals() - self.order) // 2  # where do we start writing

            # sometimes p.t needs to be in the middle of two knots
            if is_even(p.num_goals()) == is_even(self.order):
                p.idx_start = [col, col]
            else:  # if t does not lie directly on a know
                p.idx_start = [col, col + 1]

            for der, val in p.goals.iteritems():
                tp = self.time_pts[row]
                tp.point = p  # save reference to point
                tp.row = row
                tp.der = der  # order of derivative

                # di is an offset of 0 or 1, used to avoid writing off the end of A
                tp.di = 0 if col+self.order == self.k else 1

                # goals are stored separately, because we need a matrix
                self.goals[row] = val
                row += 1

    def _calc_a(self):

        for tp in self.time_pts:
            n = self.order
            p = tp.point
            col = p.idx_start[0]
            basis = self.calc_basis(p.t, tp.der)
            self.A[tp.row, col:col+n+tp.di] = basis[:n+tp.di]

    def __init__(self, points, order=3):
        self.points = points
        self.order = order
        self.t0 = 0.0
        scale = 0.1
        self.deriv_max = {i: scale for i in range(1, order+1)}  # no max for position

        self.opt_time_grid = 10
        self.dt_min = 0.1

        self._setup_calcs()

        self._calc_indices()
        self._make_path_segs()  # half of this needed now

        self._create_u()

    def optimize_time(self):
        for tp in self.time_pts:
            tp.discretize(self, self.opt_time_grid)
            tp.calc_maxes()
            # tp.adjust_time(self.deriv_max, 0.1)
            tp.adjust_time(self.deriv_max, self.dt_min)
        self.solve()
        # self._calc_a()

    # self.point needs to have idx_start, idx_end set
    def _make_path_segs(self):
        pts = self.points
        for i in range(len(pts) - 1):
            self.path_segs.append(PathSeg(pts[i], pts[i+1]))

    def estimate_times(self):
        for tp in self.time_pts:
            tp.dt = 0.0  # there's a t_min in PathSeg, though it maybe should be elsewhere
        for ps in self.path_segs:
            idx0 = ps.idx_start
            idx1 = ps.idx_end
            for j in range(2):
                for i in range(idx0[j], idx1[j]):
                    self.time_pts[i].dt += 0.5 * ps.dt / ps.num_spl_seg

    def solve(self):
        self._create_u()
        self._calc_a()

        # solve for d, then pad last point for interpolating later
        self.d = np.linalg.solve(self.A, self.goals)
        self.d = np.vstack((self.d, self.d[-1]))

    # Returns index of which time interval is currently active
    def get_time_index(self, t):
        assert(t >= self.u[0])
        assert(t <= self.u[-1])
        return np.searchsorted(self.u, t, side='right') - 1

    def get_time_index_offset(self, t):
        return self.get_time_index(t) + self.order

    def calc_basis_all(self, t):
        self.calc_basis(t, self.order)

    def calc_basis(self, t, deriv=0):
        u = self.up
        n = self.order
        b = self.basis[0]
        self.idx_t = self.get_time_index_offset(t)

        for k in range(1, n+1):
            for i in range(n-k, n+1):
                r = self.idx_t+i-n  # l in Farin notation, where to index into u vector
                assert(t >= u[r])
                assert(t <= u[r+k+1])
                alpha = (t - u[r]) / (u[r+k] - u[r])
                beta = (u[r+k+1] - t) / (u[r+k+1] - u[r+1])

                b[k, i] = alpha * b[k-1, i]
                if i < n:
                    b[k, i] += beta * b[k-1, i+1]
        self._calc_basis_derivs(t, deriv)
        return self.basis[deriv][-1, :]

    # Call calc_basis (all) first
    def get_deriv(self, deriv):
        b = self.basis[deriv][-1, :]
        idx = self.idx_t - self.order  # hack
        state = b[0] * self.d[idx]
        for i in range(1, self.order + 1):
            state += b[i] * self.d[idx + i]
        return state

    def _calc_basis_derivs(self, t, deriv):
        n = self.order
        assert(deriv <= n)  # deriv n is piecewise constant
        u = self.up
        for m in range(1, deriv+1):
            bp = self.basis[m-1]  # previous (deriv order) basis
            bd = self.basis[m]    # this derivative
            for k in range(m, n+1):
                for i in range(n-k, n+1):
                    # r is l in Farin notation, where to index into u vector
                    r = self.idx_t+i-n
                    assert(t >= u[r])
                    assert(t <= u[r+k+1])
                    alpha = k / (u[r+k] - u[r])
                    beta = -k / (u[r+k+1] - u[r+1])

                    bd[k, i] = alpha * bp[k-1, i]
                    if i < n:
                        bd[k, i] += beta * bp[k-1, i+1]

    # Warning:  doesn't calculate basis or derivs
    # Returns order+1 basis coefficiencts at time = t
    # If der_order > 0, these are coefficients of that order of derivative
    def get_basis(self, t, der_order=0):
        return self.basis[der_order][-1, :]

    def calc_maxes(self, num_grid):
        for tp in self.time_pts:
            tp.discretize(self, num_grid)
            tp.calc_maxes()

    def adjust_times(self):
        pass

    def discretize(self, num_grid, t0=None, tf=None):
        if t0 is None:
            t0 = self.t0
        if tf is None:
            tf = self.u[-1]
        # Calculate deriatives using summation of N * d_i
        time = np.linspace(t0, tf, num_grid)
        state = [np.empty((num_grid, self.num_dof)) for _ in range(self.order+1)]
        for i, t in enumerate(time):
            self.calc_basis_all(t)
            for k in range(self.order+1):
                state[k][i] = self.get_deriv(k)
        return time, state

    def interp(self, t):
        u = self.up
        dk = self.dk
        n = self.order
        idx_d = self.get_time_index(t)
        idx_t = self.get_time_index_offset(t)
        dk[0, :, :] = self.d[idx_d:idx_d+n+1]

        for k in range(1, n+1):
            for i in range(n-k+1):
                r = idx_t+i  # l+i in Farin notation
                a = (t - u[r+1]) / (u[r-n+k] - u[r+1])  # alpha
                # from Farin, but a, (1 - a) switched.  Not clear why, but this works:
                dk[k, i] = a * dk[k-1, i] + (1.0 - a) * dk[k-1, i+1]
        return dk[-1, 0]
