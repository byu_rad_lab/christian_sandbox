#!/usr/bin/env python
from __future__ import division

import matplotlib.pyplot as plt

import numpy as np

# from spliner import Spliner, Point
from spliner import Spliner, CtrlPt


np.set_printoptions(precision=3)
np.set_printoptions(suppress=True)
np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
np.set_printoptions(linewidth=190)

# later:  plot v, a, along path
# figure out how to modify time to improve things

# much later:  add points in between
# use null space projector to optimize path

# TODO: deal with time along paths
# TODO: calculate derivatives along path

order = 3
# order = 4

x0 = 0.0
y0 = 0.0
width_x = 1.0
dx = 0.2
end_x = width_x + dx
step_y = 0.2

v = 0.1
# ve = 0.05

# positions
# p0 = CtrlPt(np.array([x0, y0]))
# p1 = CtrlPt(np.array([x0 + width_x, y0]))
# p2 = CtrlPt(np.array([x0 + width_x, y0 + step_y]))
# p3 = CtrlPt(np.array([x0, y0 + step_y]))
# p4 = CtrlPt(np.array([x0, y0 + 2 * step_y]))
# p5 = CtrlPt(np.array([x0 + width_x, y0 + 2 * step_y]))

p0 = CtrlPt.from_pos([x0, y0])
p1 = CtrlPt.from_pos([x0 + width_x, y0])
p2 = CtrlPt.from_pos([x0 + width_x, y0 + step_y])
p3 = CtrlPt.from_pos([x0, y0 + step_y])
p4 = CtrlPt.from_pos([x0, y0 + 2 * step_y])
p5 = CtrlPt.from_pos([x0 + width_x, y0 + 2 * step_y])

# velocity and acceleration
p1.set_point(np.array([v, 0.0]), 1)
p1.set_point(np.array([0.0, 0.0]), 2)
p2.set_point(np.array([-v, 0.0]), 1)
p2.set_point(np.array([0.0, 0.0]), 2)

p3.set_point(np.array([-v, 0.0]), 1)
p4.set_point(np.array([v, 0.0]), 1)


for p in [p1, p2, p3, p4]:
    for i in range(2, order):
        p3.set_point(np.array([0.0, 0.0]), i)
        p4.set_point(np.array([0.0, 0.0]), i)

points = [p0, p1, p2, p3, p4, p5]

# a = 0.2
# v_mid = 0.2
# x1 = width_x * a
# x2 = width_x * (1.0 - a)
# pv1 = CtrlPt(np.array([x1, 0.0]), 0)
# pv1.set_point(np.array([v_mid, 0.0]), 1)
# x = width_x * a
# pv2 = CtrlPt(np.array([x2, 0.0]), 0)
# pv2.set_point(np.array([v_mid, 0.0]), 1)
# points = [p0, pv1, pv2, p1, p2, p3, p4, p5]

# pe1 = CtrlPt(np.array([end_x, y0 + 0.33 * step_y]))
# pe1.set_point(np.array([0.0, ve]), 1)  # don't add acceleration without velocity
# pe2 = CtrlPt(np.array([end_x, y0 + 0.66 * step_y]))
# pe2.set_point(np.array([0.0, ve]), 1)  # don't add acceleration without velocity
# pe1.set_point(np.array([0.0, 0.0]), 2)
# points = [p0, p1, pe1, pe2, p2, p3, p4, p5]
# pem = CtrlPt(np.array([end_x, y0 + 0.5 * step_y]))
# pem.set_point(np.array([0.0, ve]), 1)  # don't add acceleration without velocity
# points = [p0, p1, pem, p2, p3, p4, p5]


# Create Spliner which solves for control points
sp = Spliner(points, order)

print "sp.A:"
print sp.A

sp.solve()
# sp.du[1] = 10
# sp.du[5] = 3
# sp.du[8] = 10
sp.t0 = 12.0
sp.resolve()

# Analyize numericaly
num_grid = 100
# num_grid = 500
# num_grid = 1500

time, state = sp.discretize(num_grid)

num_dof = sp.num_dof
colors = 'rgbymc'

plot_state = False
plot_state = True
if plot_state:
    plt.figure()
    plt.title('States vs. Time')
    axes = []
    for i in range(num_dof):
        if i == 0:
            ax = plt.subplot(num_dof, 1, i+1)
        else:
            ax = plt.subplot(num_dof, 1, i+1, sharex=axes[0])
        axes.append(ax)

        # plt.plot(sp.u, sp.d[:-order, i], 'rx')  # alignment issues
        for j in reversed(range(order+1)):  # plot position on top
            c = colors[j % len(colors)]
            # Plot state and derivatives
            options = {'color': c, 'lw': 3}
            plt.plot(time, state[j][:, i], **options)

            # Plot constraints
            options = {'color': c, 'marker': 's', 'ms': 6}
            for p in sp.points:
                if j in p.goals:
                    g = p.goals[j]
                    plt.plot(p.t, g[i], **options)

        ax.set_ylabel('state %d' % i)
        ax.set_xlim([time[0], time[-1]])
    ax.set_xlabel('Time [s]')  # bottom axis


plt.figure()
x = state[0]
plt.plot(x[:, 0], x[:, 1], 'bx-')
plt.plot(sp.d[:, 0], sp.d[:, 1], 'rx', ms=8)

vel_dt = 0.1
for p in sp.points:
    for der, pt in p.goals.iteritems():
        if i == 0:  # position
            plt.plot(p.goals[i][0], p.goals[i][1], 'yo')
        if i == 1:  # velocity
            pos0 = sp.interp(p.t)
            pos1 = pos0 + p.goals[1] * vel_dt
            plt.plot([pos0[0], pos1[0]], [pos0[1], pos1[1]], 'mx-', ms=10, lw=4)

plt.show()
