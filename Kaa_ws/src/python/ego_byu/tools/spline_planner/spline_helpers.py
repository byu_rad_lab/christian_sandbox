import numpy as np


def is_even(n):
    assert(isinstance(n, (int, long)))
    return n % 2 == 0


# corresponds to rows of A
# keeps goal, row, col
# perhaps combine with TimeSeg?
class TimePt(object):

    def __init__(self, dt):
        self.dt = dt
        # self.row
        # self.point
        # self.di
        self.maxes = None
        self.time = None
        self.state = None
        self.opt_time_scale = 0.5

    def discretize(self, spliner, num_grid):
        eps = 1e-9  # excelude end points, otherwise final derivative ends up off by one
        if spliner.u.size > self.row+1:
            t0 = spliner.u[self.row] + eps
            tf = spliner.u[self.row+1] - eps
            self.time, self.state = spliner.discretize(num_grid, t0, tf)

    def calc_maxes(self):
        if self.state is not None:
            self.maxes = [np.max(np.abs(s)) for s in self.state]

    def adjust_time(self, deriv_maxes, dt_min):
        if self.maxes is None:
            return
        scale = 0.0
        for order, val in deriv_maxes.iteritems():
            assert(order > 0)
            scale = max([self.maxes[order] / deriv_maxes[order], scale])
        scale2 = 1.0 + (scale - 1.0) * self.opt_time_scale  # modulate things
        self.dt *= scale2
        self.dt = max(self.dt, dt_min)


# Portion of a path between CtrlPts
class PathSeg(object):

    def __init__(self, pt1, pt2):
        self.pt1 = pt1
        self.pt2 = pt2
        self.process_pts()

        self.max_scale = 0.1
        # self.max_scale = 1.0  # causes jerk much larger than 1
        self.v_max = self.max_scale
        self.a_max = self.max_scale
        self.j_max = self.max_scale
        # self.t_min = 0.1
        self.t_min = 1.0

        self.calc_time()

    def process_pts(self):
        self.idx_start = self.pt1.idx_start
        self.idx_end = self.pt2.idx_start
        self.num_spl_seg = 0.5 * (
            (self.idx_end[0] + self.idx_end[1]) -
            (self.idx_start[0] + self.idx_start[1]))

    def calc_time(self):
        self.dx = 0.0
        self.dv = 0.0
        self.da = 0.0
        # If constraints on same derivative exist, calculate the delta
        if 0 in self.pt1.goals and 0 in self.pt2.goals:
            self.dx = self.pt2.goals[0] - self.pt1.goals[0]
        if 1 in self.pt1.goals and 1 in self.pt2.goals:
            self.dv = self.pt2.goals[1] - self.pt1.goals[1]
        if 2 in self.pt1.goals and 2 in self.pt2.goals:
            self.da = self.pt2.goals[2] - self.pt1.goals[2]
        # Add time for each delta that exists
        # Delta velocity causes excessive jerk:  not sure how to fix this
        # Perhaps use non-limited time optimal solutions for higher derivatives?
        self.dt = self.t_min
        self.dt += np.linalg.norm(self.dx) / self.v_max
        self.dt += np.linalg.norm(self.dv) / self.a_max
        self.dt += np.linalg.norm(self.da) / self.j_max


# Point specifying position or a derivative
# A series of these, along with time points (knots) defines a path
class CtrlPt(object):

    def __init__(self):
        self.goals = {}

    @classmethod
    def from_pos(cls, val):
        pt = cls()
        pt.goals = {0: np.array(val)}
        pt.num_dof = pt.goals[0].shape
        return pt

    @classmethod
    def from_dict(cls, d):
        pt = cls()
        for key, val in d.iteritems():
            pt.goals[key] = np.array(val)
        pt.num_dof = pt.goals[key].shape
        return pt

    def set_point(self, val, der):
        self.goals[der] = np.array(val)

    def num_goals(self):
        return len(self.goals)

    def get_goal_val(self, idx):
        key = self.goals.keys()[idx]
        return self.goals[key]

    def fill_in_zero(self, max_der):
        for i in range(max_der):
            if i not in self.goals:
                self.goals[i] = np.zeros(self.get_goal_val(0).shape)
