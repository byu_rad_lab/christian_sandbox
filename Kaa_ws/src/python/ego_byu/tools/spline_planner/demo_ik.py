#!/usr/bin/env python
from __future__ import division

import matplotlib.pyplot as plt
from ego_core.plotting import dyn_plotter, plot_gnomon

from atheris_gui.opengl import dynamics_qpik as qpik
from ego_atheris import dynamics as dyn
from ego_atheris import robot as rob

import numpy as np

from path_planner import Planner

np.set_printoptions(precision=3)
np.set_printoptions(suppress=True)
np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
np.set_printoptions(linewidth=190)

order = 3
# order = 4
# order = 7  # turns out very poorly

planner = Planner(order)
planner.create_path()
planner.plan()
planner.optimize_time(1)

sp = planner.spliner
num_grid = 50
# num_grid = 500
# num_grid = 50
# num_grid = 10
time, plan_state = sp.discretize(num_grid)

filepath = "./"
filename_arm = filepath + "sanding_arm.yaml"
# arm_node = rob.YamlLoadFile(filename_arm)
arm_node = dyn.YamlLoadFile(filename_arm)
arm = dyn.ArmFromYamlNode(arm_node)
num_dof = arm.num_dof()

arm.SetStateZero()
arm.CalcFK()

# vec = np.array([0.0, 0.90, 0.7])
# d_th = 15.0 * np.pi / 180.0

# vec = np.array([0.0, 0.85, 0.65])
vec = np.array([0.0, 0.8, 0.65])
d_th = 0.0
rvec = np.array([d_th + np.pi / 2.0, 0.0, 0.0])
g_world_task = dyn.HomFromRvecVec(rvec, vec)


qpik = qpik.QPIK(arm)
qpik.error_improv_tol = 1e-8

# pt_list = []
# for p in planner.path:
#     if 0 in p.goals:
#         pt_list.append(p.goals[0])
# TODO: make point list from numerics instead
pt_list = plan_state[0]

num_pts = len(pt_list)

print "num_pts:", num_pts
pts_task = np.empty((4, num_pts))
pts_task[3, :] = 1.0

for i, pt in enumerate(pt_list):
    pts_task[:3, i] = np.array(pt)

print "pts_task:"
print pts_task

pts_world = np.dot(g_world_task, pts_task)

g_world_arm = arm.GetLink(0).g_world_link()
g_arm_world = dyn.HomInv(g_world_arm)
rot_world_arm = dyn.RotFromHom(g_world_arm)
rot_arm_goal = dyn.RotFromRvec(np.array([d_th + -np.pi / 4.0, 0.0, 0.0]))
rot_goal = np.dot(rot_arm_goal, rot_world_arm)

goals = []
for pt in pts_world.T:
    goals.append(dyn.HomFromRotVec(rot_goal, pt[:3]))

state = []
g_last = []
# goals = goals[0:3]  # FIXME
err = np.empty(len(goals))
for i, goal in enumerate(goals):
    qpik.solve(goal)
    err[i] = qpik.error_tot
    state.append(arm.q())
    g_last.append(arm.GetLink(-1).g_world_link())

namef = "path_forward_%04d.yaml" % num_grid
nameb = "path_backward_%04d.yaml" % num_grid
with open(namef, 'w') as ff:
    for s in state:
        ff.write("- [")
        for e in s[:-1]:
            ff.write("%8.5f," % e)
        ff.write("%8.5f]\n" % s[-1])


with open(namef, 'r') as ff:
    with open(nameb, 'w') as fb:
        lines = ff.read().splitlines()
        for line in reversed(lines):
            fb.write(line + "\n")

gnomon_lw = 2
gnomon_len = 0.05

plt.figure()
plt.plot(time, err)
plt.title('Error')
plt.xlabel('Time [sec]')
plt.ylabel('Error [m or rad?]')

plot_robot = True
# plot_robot = False
if plot_robot:
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # i = 5
    i = -1
    arm.set_q(state[i])
    arm.CalcFK()

    # create plotter and plot
    arm_plotter = dyn_plotter.ArmPlotter(arm)
    arm_plotter.plot(ax)
    plot_gnomon(ax, goal, gnomon_len, gnomon_len)

    plt.plot(pts_world[0, :], pts_world[1, :], pts_world[2, :], 'kx-')
    # for goal in goals:
    #     plot_gnomon(ax, goal, gnomon_len, gnomon_lw)
    for g in g_last:
        plot_gnomon(ax, g, gnomon_len, gnomon_lw)

    # labels and such
    dyn_plotter.set_axes_equal(ax)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

plt.show()
