#!/usr/bin/env python

from ego_byu.madgwick import madgwick as md
from ego_byu.tools import pickle_tools as pk
import numpy as np
import os
import rospy
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Quaternion
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
from geometry_msgs.msg import Vector3Stamped
from geometry_msgs.msg import TransformStamped
from byu_robot.msg import robot_state
import message_filters
from threading import RLock
import tf
import tf2_ros
import time

import tf.transformations as tft

class IMU_Set(object):
    def __init__(self, IMU_Nums, world, gain, calib=False, pub=False):
        self.IMUs = []
        self.calib_time=40

        for i in IMU_Nums:
            IMU = IMU_Filter(i, world, gain, calib=calib, pub=pub)
            self.IMUs.append(IMU)

        #Collect Data in Subscribers
        if calib:
            for imu in self.IMUs:
                imu.StartSubscriber()

            print "Calibrating IMUs: Move IMUs Around for " + str(self.calib_time) + " seconds."
            t=0
            start_time = time.time()
            while t < self.calib_time+1:
                t=time.time()-start_time
                print "Collecting Data: Move IMUs around"
                time.sleep(1)

            for imu in self.IMUs:
                imu.set_calibration()

        else:
            for imu in self.IMUs:
                imu.load_calibration()
                imu.StartSubscriber()

class IMU_Filter(object):
    def __init__(self, IMU_Num, world, gain, calib=False, pub=False):
        self.IMU_Num=IMU_Num
        self.initialized=False
        self.lock=RLock()
        self.rate=100.0
        self.Rate=rospy.Rate(self.rate)
        self.dt=0.0
        self.mag_points=[] #Where calibration pts are collected

        self.Filter=md.ImuFilter()
        self.Filter.setWorldFrame(world)

        self.Filter.setAlgorithmGain(gain)
        self.reversed_=False  # For Rviz Convenience
        self.first_iter=0
        self.br=tf2_ros.TransformBroadcaster()
        self.cal_path = os.environ['EGO_BYU']+"/python/ego_byu/calibrations/"
        self.q_imuworld_imu = [0,0,0,1.0]
        self.q_imuworld_top = [0,0,0,1.0] #These may be set later
        self.q_imuworld_bot = [0,0,0,1.0]
        self.q_imuworld_base = [0,0,0,1.0]

        self.calibrate=calib

        # Publishers (For this Module Only)
        self.publish=pub
        if self.publish:
            self.mag_pub=rospy.Publisher('/IMU_mag_corrected'+str(self.IMU_Num), Vector3Stamped, queue_size=0)
            self.IMU_pub=rospy.Publisher('/Imu_Orientation'+str(self.IMU_Num), Imu, queue_size=0)

    def StartSubscriber(self):
        rospy.Subscriber("robot_state", robot_state, self.callback_from_robot)

    def load_calibration(self):
        parameters=pk.load_pickle(self.cal_path+'calibration_imu'+str(self.IMU_Num))
        self.magx_min=parameters['magx_min']
        self.magx_max=parameters['magx_max']
        self.magy_min=parameters['magy_min']
        self.magy_max=parameters['magy_max']
        self.magz_min=parameters['magz_min']
        self.magz_max=parameters['magz_max']
        self.q_imuworld_zero=parameters['q_imuworld_zero']
        self.magx_range=self.magx_max-self.magx_min
        self.magx_mid=self.magx_range/2.0+self.magx_min
        self.magy_range=self.magy_max-self.magy_min
        self.magy_mid=self.magy_range/2.0+self.magy_min
        self.magz_range=self.magz_max-self.magz_min
        self.magz_mid=self.magz_range/2.0+self.magz_min

    def set_calibration(self):
        self.mpts=np.array(self.mag_points)
        self.magx_max=np.max(self.mpts[:, 0])
        self.magx_min=np.min(self.mpts[:, 0])
        self.magy_max=np.max(self.mpts[:, 1])
        self.magy_min=np.min(self.mpts[:, 1])
        self.magz_max=np.max(self.mpts[:, 2])
        self.magz_min=np.min(self.mpts[:, 2])
        self.magx_range=self.magx_max-self.magx_min
        self.magx_mid=self.magx_range/2.0+self.magx_min
        self.magy_range=self.magy_max-self.magy_min
        self.magy_mid=self.magy_range/2.0+self.magy_min
        self.magz_range=self.magz_max-self.magz_min
        self.magz_mid=self.magz_range/2.0+self.magz_min
        self.mag_points=[] #Release Data

        self.calibrate = False

        raw_input("IMU Zero Frame Measurement: Set Arm at Zero State, Then Press Return")
        self.q_imuworld_zero =self.q_imuworld_imu

        parameters={'magx_min': self.magx_min,
                      'magx_max': self.magx_max,
                      'magy_min': self.magy_min,
                      'magy_max': self.magy_max,
                      'magz_min': self.magz_min,
                      'magz_max': self.magz_max,
                      'q_imuworld_zero': self.q_imuworld_zero}

        print "IMU " + str(self.IMU_Num) + " paramters:"
        print parameters
        pk.save_pickle(self.cal_path+'calibration_imu'+str(self.IMU_Num),parameters)
        # self.plot_mag_data()

    def plot_mag_data(self):
        answer=raw_input('Would you like to see magnetometer calibration plot? (y/n): ')
        if answer == 'y':

            x=[]
            y=[]
            z=[]
            for i in range(0, 1000):

                print 'Move the gyroscope around in circles until count reaches 0', 1000-i
                x.append(self.M_comp_x)
                y.append(self.M_comp_y)
                z.append(self.M_comp_z)
                time.sleep(.01)


            fig=plt.figure(1)
            ax=p3.Axes3D(fig)
            ax.scatter3D(x, y, z, c='r')
            ax.set_xlabel('X')
            ax.set_ylabel('Y')
            ax.set_zlabel('Z')
            fig.add_axes(ax)
            plt.show()
        else:
            return 1

    def callback_from_robot(self, msg):
        self.lock.acquire()
        M=msg.sensors[self.IMU_Num].mag_raw
        self.M=[M[1], M[0], -M[2]]

        if self.calibrate == True and len(self.mag_points) < 10000000:
            self.mag_points.append([self.M[0], self.M[1], self.M[2]])
        else:
            self.Ang_Vel=msg.sensors[self.IMU_Num].gyr_raw
            self.Lin_Vel=msg.sensors[self.IMU_Num].acc_raw
            self.time=msg.header.stamp;
            frame_id=msg.header.frame_id

            if self.dt == 0:
                self.dt=1.0/self.rate
            else:
                self.dt=(self.time - self.last_time).to_sec()
            self.last_time=msg.header.stamp
            # self.UpdateBounds()
            self.M_comp_x=(self.M[0] - self.magx_mid)*200.0/self.magx_range
            self.M_comp_y=(self.M[1] - self.magy_mid)*200.0/self.magy_range
            self.M_comp_z=(self.M[2] - self.magz_mid)*200.0/self.magz_range
            # print self.Ang_Vel, self.Lin_Vel, self.M_comp_x,self.dt
            # print self.Ang_Vel, self.Lin_Vel, self.M_comp_x, self.M_comp_y,self.M_comp_z,self.dt
            self.Filter.madgwickAHRSupdate(
            self.Ang_Vel[0], self.Ang_Vel[1], self.Ang_Vel[2],
            self.Lin_Vel[0], self.Lin_Vel[1], self.Lin_Vel[2],
            self.M_comp_x, self.M_comp_y, self.M_comp_z,
            self.dt);
            self.qs = self.Filter.returnOrientation()
            self.q_imuworld_imu = [self.qs[1],self.qs[2],self.qs[3],self.qs[0]]
            self.q_imuworld_top = tft.quaternion_multiply(self.q_imuworld_imu,self.q_imu_top)
            self.q_imuworld_bot = tft.quaternion_multiply(self.q_imuworld_imu,self.q_imu_bot)
            self.q_base_top = tft.quaternion_multiply(tft.quaternion_inverse(self.q_imuworld_base),self.q_imuworld_top)
            self.q_base_bot = tft.quaternion_multiply(tft.quaternion_inverse(self.q_imuworld_base),self.q_imuworld_bot)
            # if self.IMU_Num == 0:
            #     print "base_imuworld  ", tft.quaternion_inverse(self.q_imuworld_base)
            #     # print "imuworld_imu  ",self.q_imuworld_imu
            #     print "imuworld_top  ",self.q_imuworld_top
            #     # print "imuworld_bot  ",self.q_imuworld_bot
            #     # print "imu_top  ",self.q_imu_top
            #     # print "imu_bot  ",self.q_imu_bot
            #     print "base_top ", self.q_base_top
            #     print

            if self.publish:
                self.PublishFilteredMessage()
                self.PublishTransform()
                # self.PublishMagCorrected()
                # self.Rate.sleep()

        self.lock.release()

    def UpdateBounds(self):
        if self.M[0] > self.magx_max:
            self.magx_max=self.M[0]
        if self.M[0] < self.magx_min:
            self.magx_min=self.M[0]
        self.magx_range=self.magx_max-self.magx_min
        self.magx_mid=self.magx_min + self.magx_range/2.0
        if self.M[1] > self.magy_max:
            self.magy_max=self.M[1]
        if self.M[1] < self.magy_min:
            self.magy_min=self.M[1]
        self.magy_range=self.magy_max-self.magy_min
        self.magy_mid=self.magy_min + self.magy_range/2.0
        if self.M[2] > self.magz_max:
            self.magz_max=self.M[2]
        if self.M[2] < self.magz_min:
            self.magz_min=self.M[2]
        self.magz_range=self.magz_max-self.magz_min
        self.magz_mid=self.magz_min + self.magz_range/2.0


    def PublishFilteredMessage(self):
        imu_msg=Imu()
        self.stamp=imu_msg.header.stamp
        imu_msg.orientation.w=self.q_base_top[3]
        if self.reversed_:
            imu_msg.orientation.x=-self.q_imuworld_imu[0]
            imu_msg.orientation.y=-self.q_imuworld_imu[1]
            imu_msg.orientation.z=-self.q_imuworld_imu[2]
        else:
            imu_msg.orientation.x= self.q_imuworld_imu[0]
            imu_msg.orientation.y= self.q_imuworld_imu[1]
            imu_msg.orientation.z= self.q_imuworld_imu[2]

        imu_msg.orientation_covariance[0]=0  # Should be adjusted to be variance
        imu_msg.orientation_covariance[1]=0
        imu_msg.orientation_covariance[2]=0
        imu_msg.orientation_covariance[3]=0
        imu_msg.orientation_covariance[4]=0
        imu_msg.orientation_covariance[5]=0
        imu_msg.orientation_covariance[6]=0
        imu_msg.orientation_covariance[7]=0
        imu_msg.orientation_covariance[8]=0
        self.IMU_pub.publish(imu_msg)

    def PublishTransform(self):
        transform=TransformStamped()
        transform.header.stamp=self.stamp
        transform.header.frame_id="base"
        transform.child_frame_id="IMU"+str(self.IMU_Num)+"_base_top"
        transform.transform.rotation.w=self.q_base_top[3]
        if self.reversed_:
            transform.transform.rotation.x=-self.q_base_top[0]
            transform.transform.rotation.y=-self.q_base_top[1]
            transform.transform.rotation.z=-self.q_base_top[2]
        else:
            transform.transform.rotation.x= self.q_base_top[0]
            transform.transform.rotation.y= self.q_base_top[1]
            transform.transform.rotation.z= self.q_base_top[2]
        self.br.sendTransform(transform)

    def PublishMagCorrected(self):
        msg=Vector3Stamped()
        msg.vector.x=self.M_comp_x
        msg.vector.y=self.M_comp_y
        msg.vector.z=self.M_comp_z
        self.mag_pub.publish(msg)

    def Run(self):
        while not rospy.is_shutdown():
            if len(self.q) > 0:
                if self.first_iter == 0:
                    print "Filtering Data"
                    self.first_iter=1
                if self.publish:
                    # self.PublishFilteredMessage()
                    self.PublishTransform()
                    self.PublishMagCorrected()
                    self.Rate.sleep()
            else:
                rospy.sleep(1)

if __name__ == "__main__":
    # print "Starting IMU Filter"
    # rospy.init_node("My_Node")

    # gain=.5
    # world_frame=md.NWU
    # my_mode="ROBOT"
    # calibrate_state=False
    # imu_reader=IMU_Filter(
    # world_frame,
    # gain,
    # publish_option=True,
    # mode=my_mode,
    # calibrate=calibrate_state,
    #  IMU_Num=2)

    # imu_reader.Run()
    rospy.init_node("mynode")
    imus = [1]
    world= md.NWU
    set = IMU_Set(imus,world,.5,calib=False,pub=True)
    print "Publishing"
    rospy.spin()

