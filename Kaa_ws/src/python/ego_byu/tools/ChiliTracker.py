#!/usr/bin/env python

from __future__ import division
import cv2
import numpy as np
import chilipy as cp
from ego_byu.tools import kin_tools as kt
import Tag_Colors as tc
import preferences as pref

font = cv2.FONT_HERSHEY_PLAIN


class Tracker(object):

    def __init__(self, tags, camera):
        self.tags = tags
        self.world_detections = 0
        self.body_detections = {}

        self.cam = camera
        self.cw = cp.ChiliPy()
        # self.cw.set_dt_detect_periodically()

        self.reprojection_tol = pref.reprojection_tol

        self.bad_tags = None

        self.cam_pose_good = False

    def find_chilitags(self, img):

        if img is None:
            print "Warning - Image Not Found"
            assert(False)

        # reset tags information
        for tag in self.tags.itervalues():
            tag.is_found = False
            self.body_detections[tag.body] = 0
        self.world_detections = 0

        # run fiducial library in SWIG
        t = None
        t = self.cw.process(img)
        tag_dict = {}

        # Set corners
        for key in t:
            tag_dict[key] = self.cw.get_corners(key)

        # Processes tags in tracker object
        # Identify which tags are found, return incorrect tags
        self.bad_tags = self.process_tags(tag_dict)

        # Iterate through all tags in tracker and update self info
        for tag in self.tags.itervalues():
            if not tag.is_found:
                # If tag is still not found after processing, mark position as -1
                tag.corners_image = np.array([[-1, -1, ], [-1, -1], [-1, -1], [-1, -1]])
            else:
                self.body_detections[tag.body] += 1
                if tag.body == "world":
                    self.world_detections+=1

        # Returns tag dictionary
        return tag_dict

    def process_tags(self, detections):
        bad_tags = {}
        num_bad_tags = 0

        # go through tags, categorize, label, store data for known tags
        for key in detections:
            if key in self.tags:
                self.tags[key].key = key
                self.tags[key].corners_image = detections[key]
                self.tags[key].centerpt_image = find_centerpt(detections[key])
                retval, self.tags[key].rct, self.tags[key].tct = self.cam.est_pose(
                    self.tags[key].corners_body, self.tags[key].corners_image)
                self.tags[key].is_found = True
            else:
                tag = Tag(key, 0, "")
                tag.corners_image = detections[key]
                tag.centerpt_image = find_centerpt(detections[key])
                bad_tags[num_bad_tags] = tag
                num_bad_tags += 1

        return bad_tags

    #Find's gcw by PnP image and tag points of "world", then locates tags
    def estimate_cw_pose(self):
        # Orient Camera With Respect to World Frame Tags
        ret = self.cam.orient(self.tags, self.reprojection_tol)
        if ret == -1:
            print "### Warning: camera world pose not found"


        # Convert gcw to GL coordinates
        self.cam.pose = kt.homog_cv2gl(self.cam.gcw)

        # If camera position found, compute rot,trans_world
        self.cam.locate_tags(self.tags)

    def estimate_cb_poses(self, bodies):
        # self.cam.tracked_bodies = []
        for body in bodies:
            ret = self.cam.find_body(self.tags, self.reprojection_tol, body)
            if ret == -1:
                print "### Warning: ", body, " not found"
                return

            # Convert gcb to GL coordinates
            # self.cam.pose_bodies.append(kt.homog_cv2gl(self.cam.gcb))

    def label_image(self, img):
        # Draws Tag Axis in tags (from world):
        for tag in self.tags.itervalues():
            if tag.is_found:
                self.cam.draw_axis(img, tag.rwt, tag.twt, pref.length, pref.thickness)

        # Draws potracker.ints (image_points) and outlines tags
        label_tags(img, self.tags)
        label_bad_tags(img, self.bad_tags)

    # draws all tags in the world where it thinks they are based on camera to world transform (and world to body transform where applicable)
    # should overlap with actually found tag corners nicely
    def reproject_body(self, img, size, color, thickness, body):
        image_points, world_points = get_pts_in_world_frame(self.tags, body)

        if len(world_points) > 0:
            # Projecting from world frame to camera frame
            proj_p = self.cam.project_pts_from_world(world_points)
            plot_points(img, proj_p, size, color, thickness)
            error = np.max(np.abs(proj_p - image_points))

            if error > self.reprojection_tol:
                print "### Warning: reproj error > tol (%.1f > %.1f)" % (error, pref.reprojection_tol)

    # scales image before showing to keep it from taking up the whole screen
    def imshow_small(self, name, img, scale):
        img_small = cv2.resize(img, (int(self.cam.w/scale), int(self.cam.h/scale)))
        cv2.imshow(name, img_small)


class Tag(object):

    def __init__(self, key, width, body):
        self.key = key
        self.width = width
        self.half_width = width / 2
        self.body = body

        self.is_found = False

        # Orientation with respect to its attached body
        self.rbt = np.zeros((3, 1))
        self.tbt = np.zeros((3, 1))

        # Orientation with respect to world
        self.rwt = np.zeros((3, 1))
        self.twt = np.zeros((3, 1))

        # Orientation with respect to world
        self.rct = np.zeros((3, 1))
        self.tct = np.zeros((3, 1))

        # Corners in Tag Frame - This must match the order in which chilitags/apriltags reports points.
        self.corners_tag = self.half_width * np.array([
            [-1, 1, 0], [1, 1, 0], [1, -1, 0], [-1, -1, 0]
            ], np.float32)

        # Corners in Body Frame - Computed when the body is defined (e.g. addsheet)
        self.corners_body = np.zeros((4, 3))

        # Corners in World Frame - Set in orient (for world tags) or when projecting body pts
        self.corners_world = np.zeros((4, 3))

        # Corners in Image Frame (pi) - set by cv
        self.corners_image = np.zeros((4, 2))

        # Center Point in Image (ci)
        self.centerpt_image = np.zeros((1, 2))

    # Rotation of Tag relative to Body Frame
    def set_rbt(self, wx, wy, wz):
        self.rbt[0] = wx
        self.rbt[1] = wy
        self.rbt[2] = wz

        # hack
        if self.body == "world":
            self.rwt = self.rbt

    # Translation of Tag relative to Body Frame
    def set_tbt(self, x, y, z):
        self.tbt[0] = x
        self.tbt[1] = y
        self.tbt[2] = z
        # hack
        if self.body == "world":
            self.twt = self.tbt

    # converts corners of tag from tag frame to world frame (pw)
    def set_corners_world(self):
        R = cv2.Rodrigues(self.rwt)[0]
        self.corners_world = self.twt.T + np.dot(R, self.corners_tag.T).T

    # converts corners of tag from tag frame to object frame (po)
    def set_corners_body(self):
        # TODO: this should be in kin_tools(?)
        R = cv2.Rodrigues(self.rbt)[0]
        self.corners_body = self.tbt.T + np.dot(R, self.corners_tag.T).T


class Camera(object):

    def __init__(self, w, h, camera_matrix, dist_coefs,bodies):
        self.w = w
        self.h = h
        self.K = camera_matrix
        self.D = dist_coefs

        # Brings points from world to camera coordinate system
        self.rcw = np.zeros((3, 1))
        self.tcw = np.zeros((3, 1))

        # Brings points from body to camera coordinate system
        self.rcb = {}
        self.tcb = {}

        for body in bodies:
            self.rcb[body] = np.zeros((3,1))
            self.tcb[body] = np.zeros((3,1))

        # homogeneous transorms - world to camera and camera to world
        self.gwc = np.eye(4)
        self.gcw = np.eye(4)

        # homogeneous transorms - body to camera and camera to body
        self.gbc = {}
        self.gcb = {}
        for body in bodies:
            self.gbc[body] = np.eye(4)
            self.gcb[body] = np.eye(4)

    def orient(self, tags, tol):

        self.cam_pose_good = False  # < object of Camera, not Tracker?
        self.reproject_error = float("inf")

        # Finds world tag's points in the frame of world - This instance is just a matter of formatting
        corners_image, corners_world = get_pts_in_world_frame(tags, 'world')
        if len(corners_image) < 4:
            return -1
        retval, rcw, tcw = self.est_pose(corners_world, corners_image)
        if not retval:
            return -1

        # Set the camera pose
        self.rcw = rcw
        self.tcw = tcw
        self.gcw = kt.ax_vec2homog(rcw, tcw)
        self.gwc = kt.homog_inv(self.gcw)

        # Check the reprojection error on the world frame
        proj_p = self.project_pts_from_world(corners_world)
        err = np.max(np.abs(proj_p - corners_image))
        if err > tol:
            print "### Warning: reproj error > tol (%.1f > %.1f)" % (err, tol)
            return -1

        self.cam_pose_good = True
        self.reproject_error = err
        return 1

    def find_body(self, tags, tol, body):

        self.reproject_error = float("inf")

        # Finds world tag's points in the frame of world - This instance is just a matter of formatting
        corners_image, corners_body = get_pts_in_body_frame(tags, body)

        if len(corners_image) < 4:
            return -1
        retval, rcb, tcb = self.est_pose(corners_body, corners_image)
        if not retval:
            return -1

        # Set the camera pose
        self.rcb[body] = rcb
        self.tcb[body] = tcb
        self.gcb[body] = kt.ax_vec2homog(rcb, tcb)
        self.gbc[body] = kt.homog_inv(self.gcb[body])
        return 1

    # finds pose of object in camera frame
    def est_pose(self, object_points, image_points):
        retval, rcb, tcb = cv2.solvePnP(object_points, image_points, self.K, self.D)
        return retval, rcb, tcb

    # estimates body in the world space
    def est_pose_body(self, body, tags):
        # Abbreviated assumes est_cb and est_cw has been called
        gwb = np.dot(self.gwc, self.gcb[body])
        rwb, twb = kt.hom2ax_vec(gwb)

        # #Go through all tracker's tags and find bodies img. pts and obj. pts
        # image_points, object_points = get_pts_in_body_frame(tags, object_name)
        # rcb = np.zeros((3, 1))
        # tcb = np.zeros((3, 1))
        # error = float("inf")

        # #If object is no tags with body are found
        # if len(image_points) < 4:
        #    print "###Warning: %s Not Found" % object_name
        #    return rcb, tcb, error

        # #Find pose from camera to body
        # retval, rcb, tcb = self.est_pose(object_points, image_points)
        # #
        # #Check to see if the solution is behind camera by checking pose to each
        # #tag associated with body, if tag translation with respect to camera is
        # #negative then then the solution for body was found behind the camera
        # for i in tags.itervalues():
        #    if i.is_found == True and i.body == object_name:
        #        retval, rct, tct = self.est_pose(i.corners_tag, i.corners_image)

        #        #If this problem ever reoccurs, we label error as None and
        #        #take a picture
        #        if tct[2] < 0:   # behind camera
        #            print "### Warning: Object posed behind camera!"
        #            r = np.zeros(3)
        #            t = np.zeros(3)
        #            t[2] = 1.0
        #            retval, rcb, tcb = self.est_pose_guess(object_points, image_points, r, t)
        #            error = None

        # # TODO: add kin_tools stuff to do this right
        # gcb = kt.ax_vec2homog(rcb, tcb)

        # gwb = np.dot(self.gwc, gcb)
        # rwb, twb = kt.hom2ax_vec(gwb)

        # #Project points bodies points in world frame to screen and check error
        # image_points, object_points = get_pts_in_world_frame(tags, object_name)
        # # Project_points projects points from the world frame to the camera
        # proj_p = self.project_pts_from_world(object_points)

        # if error != None:
        #  error = np.max(np.abs(proj_p - image_points))

        return rwb, twb

    # hack to fix object behind camera problem
    def est_pose_guess(self, op, ip, r, t):
        retval, r, t = cv2.solvePnP(op, ip, self.K, self.D, r, t, True)
        return retval, r, t

    # projects points in world frame to image
    def project_pts_from_world(self, points):
        points, jacobian = cv2.projectPoints(points, self.rcw, self.tcw, self.K, self.D)
        return np.squeeze(points)

    # projects points in world frame to image (Repetitive)
    def project_pts_from_body(self, points):
        points, jacobian = cv2.projectPoints(points, self.rcb, self.tcb, self.K, self.D)
        return np.squeeze(points)

    # Finds tags in world frame
    def locate_tags(self, tags):
        # calculate position of non-fixed tags
        for tag in tags.itervalues():
            if tag.is_found and tag.body != "world":
                gwt = self.get_world_pose(tag.corners_tag, tag.corners_image)
                R, p = kt.homog2rot_vec(gwt)

                hpts = np.vstack([tag.corners_tag.T, np.ones(4)])
                tag.corners_world = np.dot(gwt, hpts).T[:, 0:3]
                rw, tw = kt.hom2ax_vec(gwt)

                tag.rwt = rw.reshape(3, 1)
                tag.twt = tw.reshape(3, 1)

    # finds world to object pose
    def get_world_pose(self, object_points, image_points):
        retval, rco, tco = self.est_pose(object_points, image_points)
        gco = kt.ax_vec2homog(rco, tco)
        return np.dot(self.gwc, gco)

    def draw_axis(self, img, rwb, twb, length, thickness, filter_option="colored", body="world"):
        p0 = length * np.vstack((np.zeros(3), np.eye(3)))  # Points in body frame

        if body == "world":
            pb = twb.T + np.dot(kt.ax2rot(rwb), p0.T).T  # Points in world frame
            pi = self.project_pts_from_world(pb)  # Points in image frame
        else:
            pb = twb.T + np.dot(kt.ax2rot(rwb), p0.T).T
            pi = self.project_pts_from_body(pb)

        if filter_option == "colored":
            color1 = tc.red
            color2 = tc.green
            color3 = tc.blue

        elif filter_option == "uncolored":
            color1 = tc.black
            color2 = tc.black
            color3 = tc.black
        try:
            cv2.line(img, tuple(pi[0].astype(int)), tuple(pi[1].astype(int)), color1, thickness)
            cv2.line(img, tuple(pi[0].astype(int)), tuple(pi[2].astype(int)), color2, thickness)
            cv2.line(img, tuple(pi[0].astype(int)), tuple(pi[3].astype(int)), color3, thickness)
        except:
            print "### Warning: Axis points off screen in draw axis function"


def load_tags(set_name):  # NOQA
    print "loading set: ", set_name
    if set_name == "all70":
        # set has no world frame
        size = 0.070
        tags = {}
        for i in range(0, 29):
            tags[i] = Tag(i, size, "")

    elif set_name == 'paper0-4-67':
        size = 0.067
        gap_x = 0.023
        gap_y = 0.025
        tags = {}
        add_sheet(tags, size, gap_x, gap_y, 0, "world")
        add_sheet(tags, size, gap_x, gap_y, 4, "paper")

    elif set_name == 'paper-dan':
        size = 0.0635
        gap_x = 0.0127
        gap_y = 0.0127
        tags = {}
        add_sheet(tags, size, gap_x, gap_y, 16, "world")
        add_sheet(tags, size, gap_x, gap_y, 20, "paper")

    elif set_name == 'paper-byu':
        size = 0.0785
        gap_x = 0.0170
        gap_y = 0.0170
        tags = {}
        add_sheet(tags, size, gap_x, gap_y, 1, "world")
        size = 0.057
        gap_x = 0.041
        gap_y = 0.027
        add_sheet(tags, size, gap_x, gap_y, 6, "body1")

        size = 0.0785
        gap_x = 0.0170
        gap_y = 0.0170
        add_sheet(tags, size, gap_x, gap_y, 10, "body2")
        # add_sheet(tags, 6, .057, "body")

    elif set_name == 'stumpy':
        size = 0.0635
        gap_x = .3683
        gap_y = .3683
        tags = {}
        add_sheet(tags, size, gap_x, gap_y, 9, "world")

        gap_z = .098425
        add_octagon(tags, size, gap_z, 1, "top_plate")
    else:
        print "Tag set '%s' not known." % set_name
        assert(False)
    return tags


def add_tag_body(tags, i, size, body):
    tags[i] = Tag(i, size, body)
    tags[i].set_corners_body()


def add_octagon(tags, size, gap_z, start_num, body):

    for i in range(start_num, start_num+8):
        tags[i] = Tag(i, size, body)

        tags[i].set_tbt
        hom1 = kt.ax_vec2homog(np.array([np.pi/2, 0, 0]), np.array([0, 0, 0]))
        hom2 = kt.ax_vec2homog(np.array([0, 0, -np.pi/4*(i-1)]), np.array([0, 0, 0]))
        rot_hom = np.dot(hom2, hom1)

        angle = 3*np.pi/2-(i-1)*np.pi/4
        hom3 = kt.ax_vec2homog(np.array([0, 0, 0]), np.array([np.cos(angle)*gap_z, np.sin(angle)*gap_z, 0]))
        rbt, tbt = kt.hom2ax_vec(np.dot(hom3, rot_hom))

        tags[i].rbt = rbt
        tags[i].tbt = tbt
        tags[i].set_corners_body()

# Printed Sheet of Four Tags (Sheet is a "body")


def add_sheet(tags, size, gap_x, gap_y, start_num, body):

    x = (gap_x + size)/2
    y = (gap_y + size)/2

    for i in range(start_num, start_num+4):
        tags[i] = Tag(i, size, body)

    tags[start_num+0].set_tbt(-x,  y, 0)
    tags[start_num+1].set_tbt(x,  y, 0)
    tags[start_num+2].set_tbt(-x, -y, 0)
    tags[start_num+3].set_tbt(x, -y, 0)

    for i in range(start_num, start_num+4):
        tags[i].set_corners_body()
        if body == "world":
            tags[i].corners_world = tags[i].corners_body


def load_camera(camera_name,bodies):
    calib_dir = "/home/danielbodily/git/ego/ego-projects-byu/python/scripts/vision/chilitags/calibration/"

    camera_matrix = np.load(calib_dir + camera_name + "_camera_matrix.npy")
    dist_coefs = np.load(calib_dir + camera_name + "_dist_coefs.npy")

    if camera_name == 'Kinect':
        width = 1920
        height = 1080
    if camera_name == 'FullHD':
        width = 1920
        height = 1080
    elif camera_name == 'thinkpad':
        width = 1280
        height = 720

    return Camera(width, height, camera_matrix, dist_coefs, bodies)


def find_centerpt(pts):
    return np.array([np.mean(pts[:, 0]), np.mean(pts[:, 1])])

# collects body's points in the frame of world and in the frame of Image


def get_pts_in_world_frame(tags, body_name):
    corners_image = np.ndarray((0, 2))
    corners_world = np.ndarray((0, 3))

    for tag in tags.itervalues():
        if tag.is_found and tag.body == body_name:
            corners_image = np.vstack((corners_image, tag.corners_image))
            corners_world = np.vstack((corners_world, tag.corners_world))
    return corners_image, corners_world

# collects body's points in the frame of body and in the frame of Image


def get_pts_in_body_frame(tags, body_name):
    corners_image = np.ndarray((0, 2))
    corners_body = np.ndarray((0, 3))

    for tag in tags.itervalues():
        if tag.is_found and tag.body == body_name:
            corners_image = np.vstack((corners_image, tag.corners_image))
            corners_body = np.vstack((corners_body, tag.corners_body))
    return corners_image, corners_body

# marks good or bad tags


def label_tags(img, tags):
    for tag in tags.itervalues():
        if tag.is_found:
            if tag.body == "world":
                color = tc.green
            else:
                color = tc.cyan
            mark_tag(img, tag.corners_image, color)
            label_tag(img, tag.centerpt_image, tag.key, color)


def label_bad_tags(img, tags):
    for tag in tags.itervalues():
        color = tc.red
        mark_tag(img, tag.corners_image, color)
        label_tag(img, tag.centerpt_image, tag.key, color)

# some helper functions for labelling tags


def mark_tag(img, corners_image, color):
    plot_points(img, corners_image, 4, color, 3)
    plot_polygon(img, corners_image, color, 1)


def label_tag(img, centerpt_image, key, color):
    cv2.putText(img, "%d" % key, tuple(centerpt_image.astype(int)),  font, 4, color, 3)
    cv2.circle(img, tuple(centerpt_image.astype(int)), 4, color, 3)


def plot_points(img, points, size, color, thickness):
    for p in points:
        try:
            cv2.circle(img, tuple(p.astype(int)), size, color, thickness)
        except:
            print "### Warning: Can't Draw Corner (428 in ChiliTracker)"


def plot_polygon(img, points, color, thickness):
    for i in range(len(points)):
        p1 = tuple(points[i].astype(int))
        p2 = tuple(points[(i+1) % 4].astype(int))
        cv2.line(img, p1, p2, color, thickness)


def get_chilitag_dict(t):
    tag_dict = {}
    for key in t:
        tag_dict[key] = self.cw.get_corners(key)

    return tag_dict

if __name__ == '__main__':
    #Key Selection
    world_keys = np.array([1,2,3,4])
    tracked_keys = [6,7,8,9,10,11,12]
    tracked_keys = np.append(world_keys,tracked_keys)
    tag_set = 'paper-byu'

    #Camera/Picture Settings
    camera_name = 'Kinect'

    #Tolerances
    reprojection_tol = 50

    #For Drawing Coordinate Frames
    length = 0.030
    thickness = 3

    #For Scaling Images
    scale = 1.3

    #Max Number of Found Tags
    tagmax = 30

    #Body to be tracked
    tracked_bodies = ['body1','body2']

    # Setup Tracker Object
    tags = load_tags(tag_set)
    camera = load_camera(camera_name,tracked_bodies)
    tracker = Tracker(tags, camera)

    # Reads Image to CV object
    img = cv2.imread("0000_color.jpg")

    if img is None:
        print "###Error: No Image"
        print "The file attempted to load %s" % img
        assert(False)

    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Gets image points and uses inherent knowledge about body points to compute world coordinates
    tracker.find_chilitags(img_gray)
    tracker.estimate_cw_pose()
    tracker.estimate_cb_poses(tracked_bodies)

    tracker.label_image(img)

    # Reprojects points from world frame to screen and compares with image
    tracker.reproject_body(img, 6, tc.magenta, 4, "world")
    tracker.reproject_body(img, 6, tc.yellow, 4, "body1")
    tracker.reproject_body(img, 6, tc.yellow, 4, "body2")

    # Draws World
    tracker.cam.draw_axis(img, np.array([0, 0, 0]), np.array([0, 0, 0]), length*1.5, 5)

    # Estimates pose and warns for reprojection error
    rwb, twb = tracker.cam.est_pose_body("body1", tracker.tags)
    tracker.cam.draw_axis(img, rwb, twb, pref.length, 5)
    print "rwb body1: ", rwb
    print "twb body1: ", twb

    rwb, twb = tracker.cam.est_pose_body("body2", tracker.tags)
    tracker.cam.draw_axis(img, rwb, twb, pref.length, 5)
    print "rwb body2: ", rwb
    print "twb body2: ", twb

    tracker.imshow_small('Tracker test', img, scale)
    k = cv2.waitKey(0)

    cv2.destroyAllWindows()
    print "Done!"
