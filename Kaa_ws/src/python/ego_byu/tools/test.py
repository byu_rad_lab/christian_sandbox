#!/usr/bin/env python
import kin_tools as kt
import numpy as np

r = np.array([np.pi/4,0,0])
t = np.array([0,2,1])

r2 = np.array([np.pi/4,0,0])
t2 = np.array([0,0,.5])

g_ab = kt.ax_vec2homog(r,t)
g_bc = kt.ax_vec2homog(r2,t2)

g_ac = np.dot(g_ab,g_bc)

adj_ab = kt.homog2adj(g_ab)
adj_bc = kt.homog2adj(g_bc)
adj_ac = kt.homog2adj(g_ac)

print np.dot(adj_ab,adj_bc)
print
print adj_ac


