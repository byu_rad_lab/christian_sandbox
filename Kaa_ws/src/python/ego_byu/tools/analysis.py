#!/usr/bin/env python

# import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # NOQA

# import sys
# import os
import numpy as np
import itertools as it
import operator

import kinematics as kin
# from ego_core.plotting import arm_plotter as ap
# from ego_pneubo.vis_tools import qpik
# from ego_core.pickle_tools import pickle_tools as pk

from time import time

np.set_printoptions(formatter={'float': '{: 8.3f}'.format})
np.set_printoptions(linewidth=150)


class LinkInfo(object):

    def __init__(self):
        # "normal" defaults
        self.name = "normal"
        self.joint_length = 0.197
        self.link_length = 0.207
        self.joint_limit = 90.0 * np.pi / 180.0


def create_info_list(case,joints=4):

    # joint types:
    # normal  # standard PilotArm joint
    # spaced  # pilot arm joint, spaced a bit
    # eight   # eight bellows joint

    link_ext_eight = 0.044
    joint_limit_eight = 56.0 * np.pi / 180.0
    # joint_limit_eight = 6.0 * np.pi / 180.0

    link_ext_spaced = link_ext_eight  # same
    joint_limit_spaced = 73.0 * np.pi / 180.0
    # joint_limit_spaced = 3.0 * np.pi / 180.0

    link_to_gripper = 0.119

    info_list = [LinkInfo() for _ in range(joints)]

    if case == 0:
        pass
    elif case == 1:
        info_list[0].name = "eight"
    elif case == 2:
        info_list[0].name = "eight"
        info_list[1].name = "spaced"
    elif case == 3:
        info_list[0].name = "eight"
        info_list[1].name = "eight"
    else:
        assert False, "Case %s not handled.  Aborting." % case

    for i, info in enumerate(info_list):
        if info.name == "normal":
            pass
        elif info.name == "eight":
            if i > 0:
                info_list[i-1].link_length += link_ext_eight
            info.joint_limit = joint_limit_eight
            info.link_length += link_ext_eight
        elif info.name == "spaced":
            if i > 0:
                info_list[i-1].link_length += link_ext_spaced
            info.joint_limit = joint_limit_spaced
            info.link_length += link_ext_spaced
        else:
            assert False, "Case %s not handled.  Aborting." % info.name

    info_list[-1].link_length = link_to_gripper
    return info_list


def create_robot(case):
    arm = kin.Arm()

    info_list = create_info_list(case)

    # joint limit parameters
    center = np.zeros(2)
    nc = 32  # number of constraints
    phase = 0.0

    for info in info_list:
        link = kin.Link(kin.JointCCC(info.joint_length))
        link.setGTopEndXYZ(0.0, 0.0, info.link_length)
        link.joint().limits().setLimitsCircular(info.joint_limit, center, nc, phase)
        arm.addLink(link)

    # tool_dz = 0.119
    # g_end_tool = kin.HomTranslate(0.0, 0.0, tool_dz)
    g_end_tool = np.eye(4)

    arm.set_g_end_tool(g_end_tool)
    arm.setJointAngsZero()
    arm.calcFK()
    return arm


def plot_poses(ax, ap, pose_dict):
    for pose in pose_dict.itervalues():
        # if pose.status == 'solved':
        if pose.is_solved:
            # ap.plot_gnomon(ax, pose.hom)
            # ap.plot_gnomon(ax, pose.hom, length=0.05, linewidth=3)
            vec = pose.hom[:3, 3]
            ax.plot([vec[0]], [vec[1]], [vec[2]], 'kx')
        # else:
        #     vec = pose.hom[:3, 3]
        #     ax.plot([vec[0]], [vec[1]], [vec[2]], 'kx')
        # print pose.status


def neighbors_six_connected(n):
    deltas = [
        (-1, 0, 0),
        (1, 0, 0),
        (0, 1, 0),
        (0, -1, 0),
        (0, 0, 1),
        (0, 0, -1)]
    return [tuple(map(operator.add, n, delta)) for delta in deltas]


def neighbors_twenty_six_connected(n):
    deltas = [x for x in it.product([-1, 0, 1], repeat=3) if x != (0, 0, 0)]
    return [tuple(map(operator.add, n, delta)) for delta in deltas]


class PoseIK(object):

    def __init__(self, hom):

        self.hom = hom  # homogeneous transform
        self.is_solved = False


class Solver(object):

    def __init__(self, qpik):
        self.qpik = qpik

        self.new_solns = []
        self.num_solved = 0
        self.num_calculated = 0
        self.time_last = -float('inf')
        self.time_delta = 2.0

    def create_pose_dict(self, x_vals, y_vals, z_vals):
        self.num_total = len(x_vals) * len(y_vals) * len(z_vals)
        self.max_idx = [len(x_vals), len(y_vals), len(z_vals)]

        self.x_vals = x_vals
        self.y_vals = y_vals
        self.z_vals = z_vals

        # set up the poses to solve
        self.pose_dict = {}
        ax = np.array([-np.pi / 2.0, 0.0, 0.0])
        for ix, x in enumerate(x_vals):
            for iy, y in enumerate(y_vals):
                for iz, z in enumerate(z_vals):
                    hom = kin.HomFromAxVec(ax, np.array([x, y, z]))
                    # hom = np.dot(hom_rot, hom_trans)
                    self.pose_dict[(ix, iy, iz)] = PoseIK(hom)

    def check_time(self):
        if time() > self.time_last + self.time_delta:
            self.time_last = time()
            dt = self.time_last - self.time_start
            string = "time: %5.1f: calculated %5d of %5d. " % \
                (dt, self.num_calculated, self.num_total)
            string += "%5d solns found. %3d solns to check" % \
                (self.num_solved, len(self.new_solns))
            print string

    def solve(self, x_vals, y_vals, z_vals):
        self.time_start = time()
        self.create_pose_dict(x_vals, y_vals, z_vals)

        for key, pose in self.pose_dict.iteritems():
            self.check_time()
            self.num_calculated += 1
            if self.num_calculated > 500:
                break
            if not pose.is_solved:
                self.solve_pose(key, pose)

    def solve_pose(self, key, pose):
        self.qpik.arm.setJointAngsZero()
        self.qpik.solve(pose.hom)
        if self.qpik.status == 'solved':
            self.num_solved += 1
            pose.is_solved = True
            pose.angles = self.qpik.arm.getJointAngs()
            pose.index = key
            self.new_solns.append(pose)
            self.solve_new_solns()

    def solve_new_solns(self):
        while self.new_solns:
            self.check_time()
            new_soln = self.new_solns.pop()

            for n in neighbors_six_connected(new_soln.index):
                # make sure index is valid
                if all([b >= 0 for b in n]) and \
                        all([b < a for b, a in zip(n, self.max_idx)]):
                    pose = self.pose_dict[n[0], n[1], n[2]]
                    if not pose.is_solved:
                        # print "solving (%d, %d)" % n
                        self.qpik.arm.setJointAngs(new_soln.angles)
                        self.qpik.solve(pose.hom)
                        if self.qpik.status == 'solved':
                            self.num_solved += 1
                            pose.is_solved = True
                            pose.angles = self.qpik.arm.getJointAngs()
                            pose.index = n
                            self.new_solns.append(pose)

# if __name__ == "__main__":

#     case = 0
#     if len(sys.argv) == 0:
#         print "Solving case %d.  I hope that's what you want." % case

#     case = int(sys.argv[1])
#     print "Solving case %d." % case

#     arm = create_robot(case)

#     qpik = qpik.QPIK(arm)
#     qpik.max_iter = 200
#     qpik.error_improv_tol = 5.0e-6

#     # qpik.error_term_tol = 5.0e-3
#     qpik.error_term_tol = 1.0e-3
#     qpik.max_step = 0.25  # no effect
#     qpik.step_scale = 0.8  # helps a lot, but maybe instabilities?

#     vec_weight = 1.00 * np.ones(3)
#     # rot_weight = 1.0 * np.ones(3)
#     rot_weight = 0.0 * np.ones(3)  # ignore rotation completely
#     # rot_weight = 0.10 * np.ones(3)
#     rot_weight[2] = 0.0  # don't solve for rotation about hand z-axis
#     qpik.Pf = np.diag(np.hstack((vec_weight, rot_weight)))

#     num_grid = 50

#     nx = num_grid
#     nz = num_grid

#     x_max = 2.0
#     x_min = 0.0  # symmetric, only need half

#     z_min = -1.2
#     z_max = 2.0

#     x_vals = np.linspace(x_min, x_max, nx)
#     y_vals = [0.0]
#     z_vals = np.linspace(z_min, z_max, nz)

#     solver = Solver(qpik)

#     time_start = time()
#     theta = 0.0
#     solver.solve(theta, x_vals, y_vals, z_vals)
#     time_end = time()
#     time_solve = time_end - time_start

#     print "Elapsed time is %.1f sec." % time_solve

#     name = "data_case_%02d" % case
#     path = "/home/thomas/code/workspace_analysis/"

#     if not os.path.exists(path):
#         print "Making path."
#         os.makedirs(path)

#     # filename = path + "theta=%0.1f" % theta_deg
#     filename = path + name
#     solver.qpik.arm = None  # otherwise pickle doesn't work
#     pk.save_pickle(filename, solver)

#     do_plot = False
#     # do_plot = True
#     if do_plot:
#         fig = plt.figure()
#         ax = fig.gca(projection='3d')

#         plot_poses(ax, ap, solver.pose_dict)

#         perc_solved = 100.0 * float(solver.num_solved) / float(solver.num_total)
#         print "%d solutions of %d poses:  %.1f%%" % \
#             (solver.num_solved, solver.num_total, perc_solved)

#         # create plotter and plot
#         arm_plotter = ap.ArmPlotter(arm)
#         arm.setJointAngsZero()
#         arm.calcFK()
#         arm_plotter.plot(ax)

#         # labels and such
#         ap.set_axes_equal(ax)
#         ax.set_xlabel('x')
#         ax.set_ylabel('y')
#         ax.set_zlabel('z')
#         plt.show()

#     plt.show()
