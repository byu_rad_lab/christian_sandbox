#!/usr/bin/env python
import numpy as np
import copy
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import arm_plotter as ap
import matplotlib.pyplot as plt
from scipy.optimize import minimize

def process_data(data_block,rem_indices):
    data_block= delete_duplicates(data_block)
    data_block = synchronize_data(data_block)
    data_block = remove_dropouts(data_block)
    data_block = remove_custom(data_block,rem_indices)
    return data_block

#Delete Duplicates
def delete_duplicates(dat):
    data_block = copy.copy(dat)
    np.set_printoptions(precision = 12)

    for i in range(len(data_block)):
        data = copy.copy(data_block[i])
        j = 0
        while (j != len(data)-2):
            if (data[j+1,1] == data[j,1]):
                data = np.delete(data,j+1,0)
            else:
                j = j+1
        data_block[i] = data

    return data_block


#Presumably no Duplicates
def synchronize_data(data_block):
    lengths = []
    #Cut first and last second from all data
    t0 = data_block[0][1,1]+1
    te = data_block[0][-1,1]-1
    for i in range(len(data_block)):
        data = copy.copy(data_block[i])
        j = 0
        while(j <= len(data)-1):
            if data[j,1] <= t0 or data[j,1] >= te:
                data = np.delete(data,j,0)
            else:
                j = j+1
        data_block[i] = data
        lengths.append(len(data_block[i]))

    #after processing, differing amount of measurements persist, redesign synchroniziation
    assert(all(x==lengths[0] for x in lengths))
    return data_block

def remove_dropouts(data_block):
    dropout_indices = []
    for i in range(len(data_block)):
        for row in range(len(data_block[i][:,0])):
            if data_block[i][row,2] >9999:
                dropout_indices.append(row)

    dropout_indices = np.array((sorted(set(dropout_indices))))
    dropout_indices = np.flipud(dropout_indices)

    for i in range(len(data_block)):
        data = copy.copy(data_block[i])
        for j in dropout_indices:
            data = np.delete(data,j,0)
        data_block[i] = data

    return data_block

def remove_custom(data_block,indices):
    for i in range(len(data_block)):
        data = copy.copy(data_block[i])
        for j in np.flipud(indices):
            data = np.delete(data,j,0)
        data_block[i] = data

    return data_block

