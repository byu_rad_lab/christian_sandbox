#!/usr/bin/env python

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # NOQA
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

import numpy as np

import kinematics as kin
from ego_byu.tools import arm_tools as at
from ego_byu.tools import kin_tools as kt
import copy
np.set_printoptions(precision=3)
np.set_printoptions(linewidth=200)
np.set_printoptions(suppress=True)


# hack to make all axes plot hte same width
# from: http://stackoverflow.com/questions/13685386/
#   matplotlib-equal-unit-length-with-equal-aspect-ratio-z-axis-is-not-equal-to
def set_axes_equal(ax):
    '''
    Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    '''

    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = x_limits[1] - x_limits[0]
    y_range = y_limits[1] - y_limits[0]
    z_range = z_limits[1] - z_limits[0]

    x_mean = np.mean(x_limits)
    y_mean = np.mean(y_limits)
    z_mean = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.4*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_mean - plot_radius, x_mean + plot_radius])
    ax.set_ylim3d([y_mean - plot_radius, y_mean + plot_radius])
    ax.set_zlim3d([z_mean - plot_radius, z_mean + plot_radius])
    # ax.set_ylim3d([0,2*plot_radius])
    # ax.set_zlim3d([0,2*plot_radius])


def fix_axes(ax, xlims, ylims, zlims):
    ax.set_xlim3d(xlims)
    ax.set_ylim3d(ylims)
    ax.set_zlim3d(zlims)


def plot_line(ax, a, b, color, linewidth_custom, alpha=1.0):  # Ax must be projection 3d
    ax.plot([a[0], b[0]], [a[1], b[1]], [a[2], b[2]], color=color, linewidth=linewidth_custom, alpha=alpha)
    # Error here may mean you forgot ax projection


def plot_line_2d(ax, a, b, color, linewidth_custom):  # Ax must be projection 3d
    # ax.plot([a[0], b[0]], [a[1], b[1]])
    ax.plot([a[0], b[0]], [a[1], b[1]], color=color, linewidth=linewidth_custom)


def plot_sphere(ax, center, radius, color='r'):
    # ax.set_aspect("equal")
    u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
    x = np.cos(u)*np.sin(v)*radius+center[0]
    y = np.sin(u)*np.sin(v)*radius+center[1]
    z = np.cos(v)*radius+center[2]
    ax.plot_wireframe(x, y, z, color=color, alpha=.2)

# def plot_cylinder(ax, center,direction,radius, color='r'):
#     # ax.set_aspect("equal")
#     u, v = np.mgrid[0:2*np.pi:20j, -radius*10,radius*10:20j]
#     x = np.cos(u)*np.sin(v)*radius+center[0]
#     y = np.sin(u)*np.sin(v)*radius+center[1]
#     z = np.cos(v)*radius+center[2]
#     ax.plot_wireframe(x, y, z, color=color,alpha=.2)


def plot_gnomon(ax, g, length=0.05, linewidth=5, color=['r', 'g', 'b']):
    x = g.dot(np.array([length, 0.0, 0.0, 1.0]))
    y = g.dot(np.array([0.0, length, 0.0, 1.0]))
    z = g.dot(np.array([0.0, 0.0, length, 1.0]))
    o = g.dot(np.array([0.0, 0.0, 0.0, 1.0]))

    plot_line(ax, o, x, color[0], linewidth)
    plot_line(ax, o, y, color[1], linewidth)
    plot_line(ax, o, z, color[2], linewidth)


def plot_gnomon_2d(ax, g, length=0.1, linewidth=5, color=['r', 'g', 'b']):
    x = g.dot(np.array([length, 0.0, 0.0, 1.0]))
    y = g.dot(np.array([0.0, length, 0.0, 1.0]))
    z = g.dot(np.array([0.0, 0.0, length, 1.0]))
    o = g.dot(np.array([0.0, 0.0, 0.0, 1.0]))

    plot_line_2d(ax, o, x, color[0], linewidth)
    plot_line_2d(ax, o, y, color[1], linewidth)
    plot_line_2d(ax, o, z, color[2], linewidth)


# not really a cylinder, just circles at g1 and g2
#   with lines connecting them
def plot_cylinder(ax, g1, g2, r=0.05, color='k', n=80):
    th_vals = [2 * np.pi * i / n for i in range(n+1)]
    circle = [[r * np.sin(th), r * np.cos(th), 0.0, 1.0] for th in th_vals]
    circle = np.array(circle).T

    circle1 = g1.dot(circle)
    g3 = copy.copy(g1)  # Gets rid of twist in Z
    g3[0:3, 3] = g3[0:3, 3]+(g2[0:3, 3]-g1[0:3, 3])
    circle2 = g3.dot(circle)

    for i in range(circle1.shape[1] - 1):
        plot_line(ax, circle1[:, i], circle1[:, i+1], color, 1.0)
        plot_line(ax, circle2[:, i], circle2[:, i+1], color, 1.0)
        plot_line(ax, circle1[:, i], circle2[:, i], color, 1.0)
        plot_line(ax, circle1[:, i], circle2[:, i+1], color, 1.0)


def plot_box(ax, length, width, height, cp, alpha=.02, color=[0, .4, .4]):
    xmin = -length/2.0+cp[0]
    ymin = -width/2.0+cp[1]
    zmin = -height/2.0+cp[2]
    xmax = length/2.0+cp[0]
    ymax = width/2.0+cp[1]
    zmax = height/2.0+cp[2]

    rx = [xmin, xmax]
    ry = [ymin, ymax]
    rz = [zmin, zmax]
    X, Y = np.meshgrid(rx, ry)
    ax.plot_surface(X, Y, zmin, alpha=alpha, color=color)
    ax.plot_surface(X, Y, zmax, alpha=alpha, color=color)
    X, Z = np.meshgrid(rx, rz)
    ax.plot_surface(X, ymin, Z, alpha=alpha, color=color)
    ax.plot_surface(X, ymax, Z, alpha=alpha, color=color)
    Y, Z = np.meshgrid(ry, rz)
    ax.plot_surface(xmin, Y, Z, alpha=alpha, color=color)
    ax.plot_surface(xmax, Y, Z, alpha=alpha, color=color)
    return


def plot_krex(ax):
    offset = [0, -1.29/2, 0.]
    # Arms
    arm_z = .2
    arm_x = 1.58496
    arm_y = .21592
    krex_x = .42626
    krex_y = 1.2972
    krex_z_arm = .581
    krex_z_max = 1.63526
    wheel_d = krex_z_arm-arm_z

    krex_z_body = krex_z_max-krex_z_arm+arm_z
    plot_box(ax, arm_x, arm_y, arm_z, np.array([0, krex_y/2.0-arm_y/2.0, krex_z_arm-arm_z/2.0])+offset)
    plot_box(ax, arm_x, arm_y, arm_z, np.array([0, -krex_y/2.0+arm_y/2.0, krex_z_arm-arm_z/2.0])+offset)

    # Body
    cp = np.array([0, 0, krex_z_max-(krex_z_body/2.0)])
    plot_box(ax, krex_x, krex_y-2*arm_y, krex_z_body, cp+offset, .05)

    # Wheels
    plot_box(ax, wheel_d, arm_y, wheel_d, np.array(
        [arm_x/2.0-wheel_d/2.0, krex_y/2.0-arm_y/2.0, wheel_d/2.0])+offset)
    plot_box(ax, wheel_d, arm_y, wheel_d, np.array(
        [arm_x/2.0-wheel_d/2.0, -(krex_y/2.0-arm_y/2.0), wheel_d/2.0])+offset)
    plot_box(ax, wheel_d, arm_y, wheel_d, np.array(
        [-(arm_x/2.0-wheel_d/2.0), krex_y/2.0-arm_y/2.0, wheel_d/2.0])+offset)
    plot_box(ax, wheel_d, arm_y, wheel_d, np.array(
        [-(arm_x/2.0-wheel_d/2.0), -(krex_y/2.0-arm_y/2.0), wheel_d/2.0])+offset)


def plot_floor(ax, width, n=5, color='k', lineweight=2.0):
    hw = width / 2.0
    x_vals = np.linspace(-hw, hw, n)

    for x in x_vals:
        ax.plot([x, x], [-hw, hw], color, lineweight)
        ax.plot([-hw, hw], [x, x], color, lineweight)


def plot_ws(ax, dimensions, center, n=2, color='k', lineweight=2.0):
    x_dim = dimensions[0]/2.0
    y_dim = dimensions[1]/2.0
    z_dim = dimensions[2]/2.0

    x_vals = np.linspace(-x_dim, x_dim, n)+center[0]
    y_vals = np.linspace(-y_dim, y_dim, n)+center[1]
    z_vals = np.linspace(-z_dim, z_dim, n)+center[2]

    # Plot Y planes
    for y in y_vals:
        for x in x_vals:
            ax.plot([x, x], [y, y], [z_vals[0], z_vals[-1]], color)
        for z in z_vals:
            ax.plot([x_vals[0], x_vals[-1]], [y, y], [z, z], color)

    for x in x_vals:
        for z in z_vals:
            ax.plot([x, x], [y_vals[0], y_vals[-1]], [z, z], color)


def plot_ellipse(ax, gwa, U, s, n=50):
    # Set of all spherical angles:
    u = np.linspace(0, 2 * np.pi, n)
    v = np.linspace(0, np.pi, n)
    # Cartesian coordinates that correspond to the spherical angles:
    # (this is the equation of an ellipsoid):
    x = s[0] * np.outer(np.cos(u), np.sin(v))
    y = s[1] * np.outer(np.sin(u), np.sin(v))
    z = s[2] * np.outer(np.ones_like(u), np.cos(v))
    gau = kt.rot_vec2homog(U, np.zeros(3))
    g = gwa.dot(gau)
    for i in range(n):
        for j in range(n):
            pt = np.array([x[i, j], y[i, j], z[i, j], 1.0])
            rpt = g.dot(pt)
            x[i, j] = rpt[0]
            y[i, j] = rpt[1]
            z[i, j] = rpt[2]
    # Plot:
    ax.plot_surface(x, y, z,  rstride=4, cstride=4, color='b')


def plot_force(ax, frame, force, color='k'):
    x = frame[0][3]
    y = frame[1][3]
    z = frame[2][3]
    if force.all() != 0:
        U_f = force/np.linalg.norm(force)/5
        u = x - U_f[0]
        v = y - U_f[1]
        w = z - U_f[2]
        a = Arrow3D([u, x], [v, y], [w, z], mutation_scale=40, lw=5, arrowstyle="-|>", color=color)
        ax.add_artist(a)


class Arrow3D(FancyArrowPatch):

    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0, 0), (0, 0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
        FancyArrowPatch.draw(self, renderer)


def plot_plane(ax, point, normal, color='r'):
    # print "plot_plane"
    # print point
    # print normal
    # raw_input("break")
    # a plane is a*x+b*y+c*z+d=0
    # [a,b,c] is the normal. Thus, we have to calculate
    point = point.reshape(3)
    normal = normal.reshape(3)
    d = -point.dot(normal)

    # Case XY Plane
    if normal[0] == 0 and normal[1] == 0:
        xx, yy = np.meshgrid(range(-2, 3), range(-2, 3))
        # calculate corresponding z
        zz = np.ones(np.shape(xx))*point[2]
    # Case XZ Plane
    elif normal[0] == 0 and normal[2] == 0:
        xx, zz = np.meshgrid(range(-2, 3), range(-2, 3))
        # calculate corresponding z
        yy = np.ones(np.shape(xx))*point[1]
    # Case YZ Plane
    elif normal[1] == 0 and normal[2] == 0:
        yy, zz = np.meshgrid(range(-2, 3), range(-2, 3))
        # calculate corresponding z
        xx = np.ones(np.shape(yy))*point[0]
    else:
        if normal[1] != 0:
            xx, zz = np.meshgrid(range(-2, 3), range(-2, 3))
            # calculate corresponding y
            yy = (-normal[0] * xx - normal[2] * zz - d) * 1. / normal[1]
        else:
            xx, yy = np.meshgrid(range(-2, 3), range(-2, 3))
            # calculate corresponding x
            zz = (-normal[0] * xx - normal[1] * yy - d) * 1. / normal[2]

    # plot the surface
    ax.plot_surface(xx, yy, zz, color=color, alpha=.1)


def plot_arm_with_jt_torques(ax, arm, jt_torques, jt_stiffness_model, force, color):
    arm_plotter = ArmPlotter(arm)
    # COMPUTE DEFLECTIONS AT JOINTS AND FK
    stiffness_mat = np.diag(jt_stiffness_model)
    jt_dths = np.zeros(len(arm.getJointAngs()))
    for i in range(len(arm.getRealJointIdx())):
        jt_torque = jt_torques[3*i:3*(i+1)]
        jt_dth = np.dot(np.linalg.inv(stiffness_mat), jt_torque)
        jt_dths[3*i:3*(i+1)] = jt_dth.T
    arm.setJointAngsDelta(jt_dths)
    arm.calcFK()

    # PLOT DEFLECTED ARM and HYBRID FRAME
    arm_plotter.link_color = color
    g_world_hybrid_ = np.eye(4)
    g_world_hybrid_[0:3, 3] = arm.getGWorldTool()[0:3, 3]
    plot_gnomon(ax, g_world_hybrid_, length=0.1, linewidth=2, color=['k', 'k', 'k'])
    plot_force(ax, g_world_hybrid_, force, 'r')
    arm_plotter.plot(ax)


class ArmPlotter(object):

    def __init__(self, arm, link_color='k'):
        self.arm = arm
        self.gnomon_length = 0.02
        self.r = 0.07
        self.link_color = link_color

    def plot(self, ax):
        # plot_floor(ax, 0.5)
        plot_gnomon(ax, np.eye(4), self.gnomon_length * 2)  # world frame

        # plot link frames and cylinders
        for link in self.arm.links_:
            bot = link.g_world_bot_
            top = link.g_world_top_
            end = link.g_world_end_
            plot_gnomon(ax, bot, self.gnomon_length, linewidth=3.0)
            plot_gnomon(ax, top, self.gnomon_length, linewidth=3.0)
            plot_cylinder(ax, top, end, r=self.r, color=self.link_color, n=10)
            # a = top[0:3,3]
            # b = end[0:3,3]
            # plot_line(ax,a,b,self.link_color,5)
        plot_gnomon(ax, self.arm.getGWorldTool(), self.gnomon_length * 2)  # EE frame

# 2D Arm


class SimpleArmPlotter(object):

    def __init__(self, arm, color='k'):
        self.arm = arm
        self.line_width = 5
        self.alpha = 1
        self.color = color
        self.ellipse_scale = .1
        self.gnomon_length = .10

    def plot(self, ax):
        # plot_floor(ax, 2,10)
        # plot link frames and cylinders
        for link in self.arm.links_:
            a = link.g_world_bot_[:3, 3].T
            b = link.g_world_top_[:3, 3].T
            c = link.g_world_end_[:3, 3].T
            # plot_line(ax, a, b, self.color, 1,self.alpha)
            plot_line(ax, a, b, self.color, .5*self.line_width, self.alpha)
            plot_line(ax, b, c, self.color, self.line_width, self.alpha)
        self.plot_link_ends(ax)

    def plot_link_ends(self, ax):
        # plot link frames and cylinders
        # for link in self.arm.links_:
            # ax.scatter(b[0], b[1],b[2], color=[.0,.4,.8],s=200)
        plot_gnomon(ax, self.arm.g_world_arm_, self.gnomon_length * 1)  # BaseFrame
        plot_gnomon(ax, np.eye(4), self.gnomon_length, 7, ['k', 'k', 'b'])  # WorldFrame

    # Assumes J is set
    def plot_manipulability_ellipse(self, ax):
        J = self.arm.getJacBodyReal()[:3, :]
        U, s, V = np.linalg.svd(J)
        g = self.arm.getGWorldTool()
        self.plot(ax)
        plot_ellipse(ax, g, U, s*self.ellipse_scale)
        set_axes_equal(ax)

    # # Assumes J is set
    # def plot_velocity_transformation_ellipse(self, ax):
    #     J = self.arm.getJacBodyReal()[:3, :]
    #     U, s, V = np.linalg.svd(J)
    #     g = self.arm.getGWorldTool()
    #     self.plot(ax)
    #     plot_ellipse(ax, g, U, s*self.ellipse_scale)
    #     set_axes_equal(ax)


class JabberPlotter(object):

    def __init__(self, arm):
        self.arm = arm
        self.gnomon_length = 0.03
        self.r = 0.05
        self.link_color = 'b'

    def plot(self, ax):
        plot_floor(ax, 0.5)
        dimensions = np.array([2, .5, 1])
        center = np.array([0, 1, 1])
        plot_ws(ax, dimensions, center)
        plot_gnomon(ax, np.eye(4), self.gnomon_length * 2)  # world frame

        # plot link frames and cylinders
        for i, link in enumerate(self.arm.links_):
            bot = link.g_world_bot_
            top = link.g_world_top_
            end = link.g_world_end_
            plot_gnomon(ax, bot, self.gnomon_length, linewidth=3.0)
            plot_gnomon(ax, top, self.gnomon_length, linewidth=3.0)
            if i >= 2:
                plot_cylinder(ax, top, end, r=self.r, color=self.link_color, n=10)

        # First two links
        for i in np.array([0, 1]):
            length1 = np.array([0.0, 0.0, self.arm.lengths[2*i]])
            length2 = np.array([0.0, 0.0, self.arm.lengths[2*i+1]])
            bend1 = np.array([self.arm.bend_angles[i], 0.0, 0.0])
            hom_trans1 = kin.HomFromAxVec(np.zeros(3), length1)
            hom_trans2 = kin.HomFromAxVec(np.zeros(3), length2)

            a_g_world_bot = self.arm.links_[i].g_world_top_
            a_g_bot_top = hom_trans1
            a_g_world_top = np.dot(a_g_world_bot, a_g_bot_top)
            plot_cylinder(ax, a_g_world_bot, a_g_world_top, r=self.r, color=self.link_color, n=10)

            b_g_world_top = self.arm.links_[i].g_world_end_
            b_g_bot_top = kt.homog_inv(hom_trans2)
            b_g_world_bot = np.dot(b_g_world_top, b_g_bot_top)
            plot_cylinder(ax, b_g_world_bot, b_g_world_top, r=self.r, color=self.link_color, n=10)
        # plot_cylinder
        plot_gnomon(ax, self.arm.getGWorldTool(), self.gnomon_length * 2)  # EE frame


class BaxterPlotter(object):

    def __init__(self, arm):
        self.arm = arm
        self.gnomon_length = 0.03
        self.r = 0.05
        self.link_color = 'b'

    def plot(self, ax):
        plot_floor(ax, 0.5)
        dimensions = np.array([2, .5, 1])
        center = np.array([0, 1, 1])
        plot_gnomon(ax, np.eye(4), self.gnomon_length * 2)  # world frame

        # plot link frames and cylinders
        for i, link in enumerate(self.arm.links_):
            bot = link.g_world_bot_
            top = link.g_world_top_
            end = link.g_world_end_
            plot_gnomon(ax, bot, self.gnomon_length, linewidth=3.0)
            plot_gnomon(ax, top, self.gnomon_length, linewidth=3.0)
            plot_cylinder(ax, top, end, r=self.r, color=self.link_color, n=10)

        # First two links
        # for i in np.array([0, 1]):
        #     length1 = np.array([0.0, 0.0, self.arm.lengths[2*i]])
        #     length2 = np.array([0.0, 0.0, self.arm.lengths[2*i+1]])
        #     bend1 = np.array([self.arm.bend_angles[i], 0.0, 0.0])
        #     hom_trans1 = kin.HomFromAxVec(np.zeros(3), length1)
        #     hom_trans2 = kin.HomFromAxVec(np.zeros(3), length2)

        #     a_g_world_bot = self.arm.links_[i].g_world_top_
        #     a_g_bot_top = hom_trans1
        #     a_g_world_top = np.dot(a_g_world_bot, a_g_bot_top)
        #     plot_cylinder(ax, a_g_world_bot, a_g_world_top, r=self.r, color=self.link_color, n=10)

        #     b_g_world_top = self.arm.links_[i].g_world_end_
        #     b_g_bot_top = kt.homog_inv(hom_trans2)
        #     b_g_world_bot = np.dot(b_g_world_top, b_g_bot_top)
        #     plot_cylinder(ax, b_g_world_bot, b_g_world_top, r=self.r, color=self.link_color, n=10)
        # # plot_cylinder
        # plot_gnomon(ax, self.arm.getGWorldTool(), self.gnomon_length * 2)  # EE frame

class LegPlotter(object):

    def __init__(self, arm, color='k'):
        self.arm = arm
        self.line_width = 5
        self.alpha = 1
        self.color = color
        self.ellipse_scale = .1
        self.gnomon_length = .10

    def plot(self, ax):
        # plot_floor(ax, 2,10)
        # plot link frames and cylinders
        i = 0
        for link in self.arm.links_:
            a = link.g_world_bot_[:3, 3].T
            b = link.g_world_top_[:3, 3].T
            d = link.g_world_end_[:3, 3].T
            hom_length1 = np.array(kin.HomFromAxVec(np.zeros(3), np.array([0.0, 0.0, self.arm.lengths[2*i]])))
            c = np.dot(np.array(link.g_world_top_), hom_length1)
            c = c[:3, 3].T
            # plot_line(ax, a, b, self.color, 1,self.alpha)
            plot_line(ax, a, b, 'g', self.line_width, self.alpha)
            plot_line(ax, b, c, self.color, self.line_width, self.alpha)
            plot_line(ax, c, d, self.color, self.line_width, self.alpha)
            i = i + 1
        self.plot_link_ends(ax)

    def plot_link_ends(self, ax):
        # plot link frames and cylinders
        # for link in self.arm.links_:
            # ax.scatter(b[0], b[1],b[2], color=[.0,.4,.8],s=200)
        plot_gnomon(ax, self.arm.g_world_arm_, self.gnomon_length * 1)  # BaseFrame
        plot_gnomon(ax, np.eye(4), self.gnomon_length, 7, ['b', 'y', 'r'])  # WorldFrame


if __name__ == "__main__":
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # # create an Nasa arm
    # lengths = np.ones(6)*.2
    # r = np.array([-np.pi/2,0,0])
    # t = np.array([0,0,1.0])

    # base_hom = kt.ax_vec2homog(r,t)
    # height = 0.06
    # arm = at.nasa_arm(height,lengths,base_hom)

    # # arm.setJointAngsReal(np.ones(6)*0)
    # arm.setJointAngsReal(np.array([0,0,.5,0,1,0]))
    # arm.calcFK()
    # arm_plotter = SimpleArmPlotter(arm)
    # # arm_plotter = ArmPlotter(arm)
    # # plot_krex(ax)
    # arm_plotter.plot(ax)
    # arm.calcJacReal()
    # arm_plotter.plot_manipulability_ellipse(ax)
    # set_axes_equal(ax)
    # # ax.xaxis.set_visible(False)
    # # ax.yaxis.set_visible(False)
    # ax.zaxis.set_visible(False)
    # ax.w_xaxis.line.set_lw(0.)
    # ax.set_xticks([])
    # # ax.w_yaxis.line.set_lw(0.)
    # # ax.set_yticks([])
    # # ax.axis("off")
    # # ax.grid("off")
    # # print self.GUI_fig.gca().axim = 0
    # # print self.GUI_fig.gca().ayim = 0
    # # print self.GUI_fig.gca().azim = 0
    # ax.view_init(elev = 20,azim=20)
    # # plot_cylinder(ax,pt,direction,'r')
    # # print self.GUI_fig.gca().elev
    # # print self.GUI_fig.gca().azim
    # plt.show()

    # create jabberwock
    # angles = np.array([-20*np.pi/180,-35*np.pi/180])
    # lengths = np.array([.26,.16,.18,.28,.025])
    # lengths = np.array([.2,.1,.2,.2,.2])

    # base_hom = kt.ax_vec2homog(np.array([-30*np.pi/180,0,0]),np.array([0,.2,.8]))
    # base_hom = kt.ax_vec2homog(np.array([0,0,0]),np.array([0,0,0]))
    # lengths = np.array([.33,.15,.025,.42,.025])
    # angles = np.array([-20*np.pi/180,-32*np.pi/180])
    # base_hom = kt.ax_vec2homog(np.array([-38*np.pi/180,0,0]),np.array([0,.2,.75]))
    # arm = at.jabberwock(lengths,angles,base_hom)
    # Plotting Jabberwock is a little deceptive, interpret with care.k
    # arm.setJointAngsZero()

    # arm.calcFK()
    # print arm.getGWorldTool()

    # create plotter and plot
    # arm_plotter = JabberPlotter(arm)
    # arm_plotter.plot(ax)

    # wx = np.linspace(-1.3-np.pi/2, 1.3-np.pi/2,5)
    # wx = np.linspace(0,-np.pi,5)
    # wy = np.linspace(-np.pi/2,np.pi/2,5)
    # wz = np.linspace(-1,1,5)
    # t = np.array([0,0,0])
    # for i in range(len(wx)):
    #     for j in range(len(wy)):
    #         for k in range(len(wz)):
    #             r = np.array([wx[i],wy[j],wz[k]])
    #             g = kt.ax_vec2homog(r,t)
    #             if g[1][2] > 0:
    #                 plot_gnomon(ax,g)

    # labels and such
    # set_axes_equal(ax)
    # ax.set_xlabel('x')
    # ax.set_ylabel('y')
    # ax.set_zlabel('z')
    # plt.title('Optimal Arm Oriented +Y in Box Workspace')
    # plt.show()
    # fig = plt.figure(figsize=plt.figaspect(1))  # Square figure
    # ax = fig.gca(projection='3d')
    # lengths = np.array([.2, .2, .2, .2, .2])
    # height = 0
    # base_hom = np.eye(4)
    # arm = at.nasa_arm(height, lengths, base_hom)
    # arm.setJointAngsRandom()
    # arm.setJointAngsZero()
    # angs = np.ones(len(arm.getRealJointIdx()))*np.pi/4.0
    # arm.setJointAngsReal(angs)
    # arm.calcFK()
    # arm.calcJacReal()
    # J = arm.getJacBodyReal()
    # plotter = SimpleArmPlotter(arm, "3D")
    # plotter.plot(ax)
    # plotter.plot_manipulability_ellipse(ax)

    # set_axes_equal(ax)
    # plt.xlabel('x')
    # plt.ylabel('y')
    # set_axes_equal(ax)
    # plt.show()

    # TEST PLOTBOX
    # create baxter
    base_hom = np.eye(4)
    arm = at.baxter_arm(base_hom)
    # angs = np.zeros([0.0,0.0,0.0,0,0,0,0])
    angs = np.array([0,0,0.0,0.0,0.0,0.0,0.0,0.0])
    arm.setJointAngsReal(angs)
    arm.calcFK()

    BaxterPlotter(arm)
    plotter = BaxterPlotter(arm)
    plotter.link_color = 'k'
    plotter.plot(ax)

    angs = np.array([0.0,0.0,0.0,0.0,np.pi/180*90,np.pi/180*-45,0.0])
    arm.setJointAngsReal(angs)
    arm.calcFK()
    plotter.link_color = 'b'
    plotter.plot(ax)

    set_axes_equal(ax)
    plt.xlabel('x')
    plt.ylabel('y')
    set_axes_equal(ax)
    plt.show()
