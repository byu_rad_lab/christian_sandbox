#!/usr/bin/env python
from __future__ import division
import pickle_tools as pk
import rospy
import copy
import time
import datetime
import os
import math
# print "PYTHONPATH IS (ARMTOOLS)"
# print os.environ["PYTHONPATH"]
# print
# raw_input("break")

import ego_byu.armeval as ae
import kinematics.kinematics as kin
import numpy as np
# from tools import kin_tools as kt
import kin_tools as kt
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from multiprocessing import Process, Queue

np.set_printoptions(threshold=50000)


def simple_arm(height, lengths, jt_limit=np.pi/2):
    arm = kin.Arm()

    # Link Type Initialization
    for i in range(len(lengths)):
        # ROTATIONAL JOINT BEFORE EACH LINK
        j = kin.JointRot(height)
        l = kin.Link(j)
        l.setGTopEnd(lengths[i], 0.0, 0)
        # inertia
        m = .5
        r = .07
        Ix = m * (3.0 * r * r + lengths[i]*lengths[i]) / 12.0
        Iy = m * (3.0 * r * r + lengths[i]*lengths[i]) / 12.0
        Iz = m * (r * r) / 2.0
        I = np.array([Ix, Iy, Iz])
        pos = np.array([0.0, 0, 0]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
        l.setInertiaSelf(m, pos, I)
        arm.addLink(l)

        # Rotation about Z for each Joint
        # g = kt.ax_vec2homog(np.array([0, 0, -np.pi/2]), np.array([0, 0, 0]))
        # g = kt.ax_vec2homog(np.array([0, 0, np.pi/2]), np.array([0, 0, 0]))
        # l.g_top_end_ = np.dot(g, l.g_top_end_)
        # l.g_top_com_ = np.eye(4)
        # l.g_top_com_[2][3] = lengths[i]/2.0

        # addLink
        for i in range(len(arm.links_)):
            arm.links_[i].joint_.limits().setLimitsScalar(jt_limit)

    return arm

def simple_leg(height, lengths, base_hom=np.identity(4), jt_limit=np.pi/2):
    arm = kin.Arm()

    # Link Type Initialization
    for i in range(len(lengths)):
        # ROTATIONAL JOINT BEFORE EACH LINK
        j = kin.JointRot(height)
        l = kin.Link(j)
        l.setGTopEnd(lengths[i], 0.0, 0)
        # inertia
        m = .051 #makes it have a weight of .5 N
        r = .07
        Ix = m * (3.0 * r * r + lengths[i]*lengths[i]) / 12.0
        Iy = m * (3.0 * r * r + lengths[i]*lengths[i]) / 12.0
        Iz = m * (r * r) / 2.0
        I = np.array([Ix, Iy, Iz])
        pos = np.array([lengths[i]/2, 0, 0]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
        l.setInertiaSelf(m, pos, I)
        arm.addLink(l)

        # Rotation about Z for each Joint
        # g = kt.ax_vec2homog(np.array([0, 0, -np.pi/2]), np.array([0, 0, 0]))
        # g = kt.ax_vec2homog(np.array([0, 0, np.pi/2]), np.array([0, 0, 0]))
        # l.g_top_end_ = np.dot(g, l.g_top_end_)
        # l.g_top_com_ = np.eye(4)
        # l.g_top_com_[2][3] = lengths[i]/2.0

        # addLink
        for i in range(len(arm.links_)):
            arm.links_[i].joint_.limits().setLimitsScalar(jt_limit)

    arm.setGravity(9.81)
    arm.g_world_arm_ = base_hom

    return arm
# Nasa arm constructor
# Stiffness based off of a diagonal stiffness matrix for each joint -
# torsional, active, passive.  High active stiffness makes assumption that
# we can completely control torque in active directions
def nasa_arm(jt_height, lengths, base_hom, stiffness=np.array([50, 100000000, 270])):
    arm = kin.Arm()
    valves_placed = 0
    arm.type_ = 0; #0 means it is a nasa arm (mainly used to know which objectives to evaluate in arm_eval_fk.cpp)

    # Link Type Initialization
    for i in range(len(lengths)):
        # ROTATIONAL JOINT BEFORE EACH LINK
        j = kin.JointRot(0)
        l = kin.Link(j)
        l.setGTopEnd(0, 0, 0)
        # inertia
        I = np.array([0, 0, 0])
        pos = np.array([0.0, 0, 0])
        l.setInertiaSelf(0, pos, I)
        arm.addLink(l)

        # JOINT CCC For each link
        j = kin.JointCCC(jt_height)
        j.setJointStiffnessScalar(0) #Set Later
        # print np.array(j.getJointStiffnessMatrix())
        l = kin.Link(j)
        l.setGTopEnd(0.0, 0.0, lengths[i])

        # Add Inertia for Valves this is taken from hardware specs but needs to be updated continually
        m = .4+.7*lengths[i]
        if i == 0 or i == len(lengths)-1:
            m = m-.2  # half joint on beginning and end
        # Valve Condition
        if lengths[i] > .182 and valves_placed != len(lengths)-1:
            m += .7
            valves_placed += 1
        if i != 0 and lengths[i-1] < .182 and valves_placed != len(lengths)-1:
            m += .7
            valves_placed += 1

        r = .07
        Ix = m * (3.0 * r * r + lengths[i]) / 12.0
        Iy = m * (3.0 * r * r + lengths[i]) / 12.0
        Iz = m * (r * r) / 2.0
        I = np.array([Ix, Iy, Iz])
        pos = np.array([0, 0, lengths[i]/2.0])
        l.setInertiaSelf(m, pos, I)
        l.InitInertia()

        # Rotation about Z for each Joint
        if i % 2 == 0:
            g = kt.ax_vec2homog(np.array([0, 0, np.pi/2]), np.array([0, 0, 0]))
        else:
            g = kt.ax_vec2homog(np.array([0, 0, -np.pi/2]), np.array([0, 0, 0]))

        l.g_top_end_ = np.dot(g, l.g_top_end_)

        # addLink
        arm.addLink(l)

    # Valves placed optimilly for joint
    if valves_placed != len(lengths)-1:
        print "ERROR: Not enough valves placed"
        assert(False)

    # stiffness - When passing to Vec signature calls, inputs must be floats, NOT INTS
    for i in range(len(lengths)):
        arm.links_[2*i].joint_.setJointStiffnessScalar(float(stiffness[0]))
        arm.links_[2*i+1].joint_.setJointStiffnessVector(np.array([float(stiffness[1]), float(stiffness[2])]))

    # Real Joint Initialization
    real_joint_idx = np.array([])
    for i in range(len(lengths)):
        real_joint_idx = np.append(real_joint_idx, 3*i+1)

    arm.setRealJointIDX(real_joint_idx)

    # Extra Variables for Evolution
    arm.lengths = lengths
    arm.jt_positions = get_jt_positions(lengths)
    arm.num_lengths = len(lengths)

    # Base variables
    arm.base_hom = base_hom
    arm.g_world_arm_ = base_hom

    # We re-evaluate every 5 generations (see evaluate_design function)
    arm.next_eval_gen = 0

    # Limits <- Problem here?
    limit = np.pi/2.0
    for i in range(len(arm.links_)):
        if np.mod(i, 2) == 0:  # Rotational Joint
            arm.links_[i].joint_.limits().setLimitsScalar(0)
        elif np.mod(i+1, 2) == 0:  # CCC Joint
            bounds_upper = np.array([limit, 0])
            bounds_lower = -bounds_upper
            arm.links_[i].joint_.limits().setLimitsVector(bounds_lower, bounds_upper)
    return arm

def ArmFromYAML():
    LinkTransforms = rospy.get_param("LinkTransforms")
    LinkNum = len(LinkTransforms)
    JointHeights = rospy.get_param("JointHeights")
    LinkMasses = rospy.get_param("LinkMasses")
    LinkRadii = rospy.get_param("LinkRadii")
    BaseHom = rospy.get_param("g_world_base")

    arm = kin.Arm()
    # Link Type Initialization
    for i in range(len(LinkTransforms)):
        # NO ROTATIONAL JOINT BEFORE EACH LINK

        # JOINT CCC For each link
        j = kin.JointCCC(JointHeights[i])
        j.setJointStiffnessScalar(0) #Set Later
        l = kin.Link(j)
        gtopend = kt.ax_vec2homog(np.array(LinkTransforms[i][:3]),np.array(LinkTransforms[i][3:]))
        l.g_top_end_ = gtopend

        #Inertia Taken form Z offset only for now
        r = LinkRadii[i]
        m = LinkMasses[i]
        length = LinkTransforms[i][-1]
        Ix = m * (3.0 * r * r + length) / 12.0
        Iy = m * (3.0 * r * r + length) / 12.0
        Iz = m * (r * r) / 2.0
        I = np.array([Ix, Iy, Iz])
        pos = np.array([LinkTransforms[i][3]/2.0, LinkTransforms[i][4]/2.0, LinkTransforms[i][5]/2.0])
        l.setInertiaSelf(m, pos, I)
        l.InitInertia()

        # addLink
        arm.addLink(l)

    # Real Joint Initialization for CCC Joints
    # CCC Joints have 2 DOFS natively, Bending aboud +X axis (u) and Bending about +Y (v)
    # real_joint_idx specifies which axes motion is occuring about
    jts = rospy.get_param("ControllableJointAxes")
    real_joint_idx =[]
    for i in range(len(jts)):
        if jts[i] == "X":
            real_joint_idx.append(2.0*i)
        if jts[i] == "Y":
            real_joint_idx.append(2.0*i+1.0)
    arm.setRealJointIDX(np.array(real_joint_idx))


    # Base variables
    g = kt.ax_veclist2homog(BaseHom)
    arm.g_world_arm_ = np.array(g)

    # Limits
    l = rospy.get_param("JointLimitsLower")
    u = rospy.get_param("JointLimitsUpper")
    for i in range(LinkNum):
        bounds_upper = np.array([u[i],u[i]])
        bounds_lower = np.array([l[i],l[i]])
        arm.links_[i].joint_.limits().setLimitsVector(bounds_lower, bounds_upper)

    arm.setJointAngsZero()
    arm.calcJacReal()
    return arm

def baxter_arm(base_hom):
    #Parameters obtained from http://sdk.rethinkrobotics.com/wiki/Hardware_Specifications
    arm = kin.Arm()
    arm.base_hom = base_hom

    #Joint1: S0
    j = kin.JointRot(0)
    l = kin.Link(j)
    l.joint_.limits().setLimitsScalar(97*np.pi/180)
    l.g_top_end_ = kt.ax_vec2homog(np.array([-np.pi/2.0,0,0]),np.array([0, .069, .27035]))
    I = np.array([0.0, 0, 0])
    pos = np.array([0.0, 0, 0])
    l.setInertiaSelf(0, pos, I)
    arm.addLink(l)

    #Joint2: S1
    j = kin.JointCC1D(0)
    l = kin.Link(j)
    l.joint_.limits().setLimitsVector(np.array([-60.0*np.pi/180.0]),np.array([123*np.pi/180.0]))
    l.g_top_end_ = kt.ax_vec2homog(np.array([0.0,0.0,0.0]),np.array([0.0, 0.0, .102]))
    I = np.array([0.0, 0, 0])
    pos = np.array([0.0, 0, 0])
    l.setInertiaSelf(0, pos, I)
    arm.addLink(l)

    #Joint3: E0
    j = kin.JointRot(0)
    l = kin.Link(j)
    l.joint_.limits().setLimitsScalar(174.25*np.pi/180)
    l.g_top_end_=kt.ax_vec2homog(np.array([0.0,0.0,0.0]),np.array([0, .069, .262]))
    I = np.array([0.0, 0, 0])
    pos = np.array([0.0, 0, 0])
    l.setInertiaSelf(0, pos, I)
    arm.addLink(l)

    #Joint4: E1
    j = kin.JointCC1D(0)
    l = kin.Link(j)
    l.joint_.limits().setLimitsVector(np.array([-150.0*np.pi/180.0]),np.array([2.864*np.pi/180.0]))
    l.g_top_end_=kt.ax_vec2homog(np.array([0.0,0.0,0.0]),np.array([0.0, 0.0, .104]))
    I = np.array([0.0, 0, 0])
    pos = np.array([0.0, 0, 0])
    l.setInertiaSelf(0, pos, I)
    arm.addLink(l)

    #Joint5: W0
    j = kin.JointRot(0)
    l = kin.Link(j)
    l.joint_.limits().setLimitsScalar(175.25*np.pi/180)
    l.g_top_end_=kt.ax_vec2homog(np.array([0.0,0.0,0.0]),np.array([0, .01, .271]))
    I = np.array([0.0, 0, 0])
    pos = np.array([0.0, 0, 0])
    l.setInertiaSelf(0, pos, I)
    arm.addLink(l)

    #Joint6: W1 --- TO THE END OF THE BAXTER-TOOL INTERFACE
    j = kin.JointCC1D(0)
    l = kin.Link(j)
    l.joint_.limits().setLimitsVector(np.array([-120.0*np.pi/180.0]),np.array([90*np.pi/180.0]))
    l.g_top_end_=kt.ax_vec2homog(np.array([0.0,0.0,0.229]),np.array([0.0, 0.0, .104]))
    I = np.array([0.0, 0, 0])
    pos = np.array([0.0, 0, 0])
    l.setInertiaSelf(0, pos, I)
    arm.addLink(l)

    #Joint7: W2
    j = kin.JointRot(0)
    l = kin.Link(j)
    l.joint_.limits().setLimitsScalar(175.25*np.pi/180)
    l.g_top_end_=kt.ax_vec2homog(np.array([0.0,0.0,0.0]),np.array([0.0, 0.0, 0.0]))
    I = np.array([0.0, 0, 0])
    pos = np.array([0.0, 0, 0])
    l.setInertiaSelf(0, pos, I)
    arm.addLink(l)
    return arm


# Jabberwock Constructor
def jabberwock(lengths, angles, base_hom):
    height = 0.182

    # Link1
    length1 = np.array([0.0, 0.0, lengths[0]])
    length2 = np.array([0.0, 0.0, lengths[1]])
    bend1 = np.array([angles[0], 0.0, 0.0])
    hom_trans = kin.HomFromAxVec(np.zeros(3), length2)
    hom_bend = kin.HomFromAxVec(bend1, length1)
    hom_offset1 = np.dot(hom_bend, hom_trans)

    # Link1
    length1 = np.array([0.0, 0.0, lengths[2]])
    length2 = np.array([0.0, 0.0, lengths[3]])
    bend1 = np.array([angles[1], 0.0, 0.0])
    hom_trans = kin.HomFromAxVec(np.zeros(3), length2)
    hom_bend = kin.HomFromAxVec(bend1, length1)
    hom_offset2 = np.dot(hom_bend, hom_trans)

    # Link3
    hom_offset3 = kin.HomTranslate(0.0, 0.0, lengths[4])

    # Offsets
    link_offsets = []
    link_offsets.append(hom_offset1)
    link_offsets.append(hom_offset2)
    link_offsets.append(hom_offset3)

    # Limits
    limits = np.array([56.0, 90.0, 90.0]) * np.pi / 180.0

    arm = kin.Arm()
    for i in range(len(limits)):
        joint = kin.JointCCC(height)
        link = kin.Link(joint)

        # should be a method of link, but only allows x, y, z setting
        link.g_top_end_ = link_offsets[i]
        arm.addLink(link)

    for i in range(len(limits)):
        bounds_upper = np.array([limits[i], limits[i]])
        bounds_lower = -bounds_upper
        arm.links_[i].joint_.limits().setLimitsVector(bounds_lower, bounds_upper)

    # Add A Rot on the End
    j = kin.JointRot(0)
    l = kin.Link(j)
    l.setGTopEnd(0, 0, 0)
    arm.addLink(l)
    arm.links_[-1].joint_.limits().setLimitsScalar(np.pi)

    arm.g_world_arm_ = base_hom

    # For Evolution
    arm.bend_angles = angles
    arm.lengths = lengths
    arm.num_lengths = len(lengths)
    arm.base_hom = base_hom
    arm.next_eval_gen = 0
    arm.type_ = 1; #1 means it is a Jabberwock arm (mainly used so can tell which optimization to run in arm_eval_fk.cpp)

    return arm

def leg(lengths, angles, base_hom, height, density = 0.5):
    angles[1] = 0.0
    angles[3] = 0.0

    hom_offset = []

    limits = np.array([90.0, 90.0]) * np.pi / 180.0
    arm = kin.Arm()

    #Build links
    for i in range(int(len(lengths)/2)):
        #define length and rotation vectors to make the link
        length1 = np.array([0.0, 0.0, lengths[2*i]])
        length2 = np.array([0.0, 0.0, lengths[2*i+1]])
        bend1 = np.array([angles[2*i],0.0,0.0])
        bend2 = np.array([0.0, angles[2*i+1],0.0])

        #make the homogeneous transform from the base of the link to the end of the link
        hom_length1 = np.array(kin.HomFromAxVec(np.zeros(3), length1))
        hom_bend1 = np.array(kin.HomFromAxVec(bend1, np.array([0.0,0.0,0.0])))
        hom_bend2 = np.array(kin.HomFromAxVec(bend2, np.array([0.0,0.0,0.0])))
        hom_length2 = np.array(kin.HomFromAxVec(np.zeros(3), length2))
        hom_offset = np.dot(hom_length1, np.dot(hom_bend1, np.dot(hom_bend2,hom_length2)))

        # create the joint and link
        joint = kin.JointCCC(height)
        link = kin.Link(joint)

        # should be a method of link, but only allows x, y, z setting
        link.g_top_end_ = hom_offset

        # inertia
        # m_joint = link.joint_.mass_ #get mass of joint
        m1 = density*lengths[2*i] #mass of first link
        m2 = density*lengths[2*i+1] #mass of second link
        mass_board_mount = .2994 #estimated board mount mass (1000 kg/m^3 density disk)
        m = m1 + m2 + mass_board_mount #total mass

        #find the COMs of each length
        hom_COM1 = np.array(kin.HomFromAxVec(np.zeros(3), length1/2.0))
        hom_COM2_int = np.array(kin.HomFromAxVec(np.zeros(3), length2/2.0))
        hom_COM2 = np.dot(hom_length1, np.dot(hom_bend1, np.dot(hom_bend2,hom_COM2_int)))

        #find COM of joint
        # print "g_world_top_: ", link.g_world_top_
        # print "g_world_bot_: ", link.g_world_bot_
        # g_top_bot = np.dot(np.transpose(link.g_world_top_), link.g_world_bot_)
        # x_COM_joint = g_top_bot[0][3]/2.0
        # y_COM_joint = g_top_bot[1][3]/2.0
        # z_COM_joint = g_top_bot[2][3]/2.0
        # print "x joint: ", x_COM_joint
        # print "y joint: ", y_COM_joint
        # print "z joint: ", z_COM_joint

        #find the center of mass of the whole link (center of mass of joint is assumed to be half of the joint length behind the top of it (accurate in 0 configuration, but quite off at pi/2, need to see if want to change this)
        x_COM = (m1*hom_COM1[0][3] + m2*hom_COM2[0][3])/m
        y_COM = (m1*hom_COM1[1][3] + m2*hom_COM2[1][3])/m
        z_COM = (m1*hom_COM1[2][3] + m2*hom_COM2[2][3])/m

        # print "x, y, and z"
        # print x_COM
        # print y_COM
        # print z_COM

        Ix = 1.0 #need to look into Is
        Iy = 1.0
        Iz = 1.0
        I = np.array([Ix, Iy, Iz])
        pos = np.array([x_COM,y_COM,z_COM])
        link.setInertiaSelf(m, pos, I)
        # add the link to the arm
        arm.addLink(link)

        #set the joint limits for each joint
        bounds_upper = np.array([limits[i], limits[i]])
        bounds_lower = -bounds_upper
        arm.links_[i].joint_.limits().setLimitsVector(bounds_lower, bounds_upper)

    arm.g_world_arm_ = base_hom

    # For Evolution
    arm.bend_angles = angles
    arm.lengths = lengths
    arm.num_lengths = len(lengths)
    arm.base_hom = base_hom
    arm.next_eval_gen = 0
    arm.type_ = 2; #2 means it is a leg (mainly used so can tell which optimization to run in arm_eval_fk.cpp)
    arm.setGravity(9.81)

    return arm




def plot_arm(arm, ax, fig):
    x = np.array([])
    y = np.array([])
    z = np.array([])
    for i in range(len(arm.links_)):
        x = np.append(x, arm.links_[i].g_world_end_[0][3])
        y = np.append(y, arm.links_[i].g_world_end_[1][3])
        z = np.append(z, arm.links_[i].g_world_end_[2][3])
    ax = fig.gca(projection='3d')
    ax.plot(x, y, z)
    # ax.scatter(x,y,z)
    ax.set_xlim([-1.5, 1.5])
    ax.set_ylim([0, 3])
    ax.set_zlim([0, 3])
    fig.canvas.set_window_title('Forward Kinematic Sampling')
    plt.draw()
    plt.show()


def plot_fks(arm):
    fig, ax = plt.subplots()
    ax = fig.gca(projection='3d')
    ax.view_init(elev=25, azim=20)
    Evaluator = ae.ArmEvalFK(arm)
    Evaluator.Initialize()
    plt.ion()
    plt.show()
    fig.canvas.draw()
    raw_input("break")
    # Take of the cla in plot_arm
    xs = []
    ys = []
    zs = []
    for i in range(600):
        angs = np.array([])
        for j in range(len(arm.lengths)):
            angs = np.append(angs, 0)
            angs = np.append(angs, np.random.rand()*np.pi-np.pi/2)
            angs = np.append(angs, 0)
        arm.setJointAngs(angs)
        arm.calcFK()
        # if (arm.links_[-1].g_world_end_[2][3] > 0 and arm.links_[-1].g_world_end_[2][3]<.10 and arm.links_[-1].g_world_end_[1][3] > 1.95 and arm.links_[-1].g_world_end_[1][3]<2.05 and arm.links_[-1].g_world_end_[0][3] > -.05 and arm.links_[-1].g_world_end_[0][3]<.05):
        # if (arm.links_[-1].g_world_end_[2][3] > 0 and
        # arm.links_[-1].g_world_end_[2][3] < .1 and
        # arm.links_[-1].g_world_end_[1][3] > 0):
        if (arm.links_[-1].g_world_end_[2][3] > 0):
            # Evaluator.arm_ = arm
            plot = True
            for link in arm.links_:
                if link.g_world_end_[2][3] < 0 and link.g_world_end_[1][3] > 0:
                    plot = False

            if plot:
                xs.append(arm.links_[-1].g_world_end_[0][3])
                ys.append(arm.links_[-1].g_world_end_[1][3])
                zs.append(arm.links_[-1].g_world_end_[2][3])
                plot_arm(arm, ax, fig)

        ax.set_xlim3d([-1.0,1.0])
        ax.set_ylim3d([-1.0,1.0])
        ax.set_zlim3d([0,2.0])

        fig.canvas.draw()

        # if (np.mod(i, 1000) == 0):
        #     print i

    plt.xlabel("X")
    plt.ylabel("Y")
    plt.title("Forward Kinematic Sampling")
    plt.ioff()
    plt.cla()
    ax.scatter(xs,ys,zs)

    plt.title("Explored Areas of Workspace")
    plt.xlabel("X")
    plt.ylabel("Y")
    ax.set_xlim3d([-1.0,1.0])
    ax.set_ylim3d([-1.0,1.0])
    ax.set_zlim3d([0,2.0])
    plt.show()


# Optional function that can be called
def check_variability(arm):
    # This function checks the variability in the FK evaluator, the arm should be evaluated +50x
    # The delay needs to stay for random seeding purposes
    scores = np.array([])
    cutoffs = np.array([])
    variance = np.array([])

    plt.ion()
    fig, ax = plt.subplots()
    # for rep in np.array([.9,.91,.92,.93,.94,.95,.96,.97,.98,.99,.999]):
    for cutoff in np.array([.1, .2, .3, .4, .5, .6, .7, .8, .9, .95, .96, .97, .98, .99]):
        for x in range(50):
            time.sleep(1)
            score = evaluate_design(arm, 0, cutoff, True, 'single_objective')
            scores = np.append(scores, score)

        variance = np.append(variance, np.std(scores))
        cutoffs = np.append(cutoffs, cutoff)
        scores = np.array([])
        ax.scatter(cutoffs, variance)
        plt.xlabel("Redundancy Cutoff")
        plt.ylabel("Std. of 50 Evaluations")
        plt.title("Variability of Arm Evaluation")
        plt.draw()

    plt.ioff()

    plt.show()
    raw_input("Check Variability")


def pickle_arms(arms, robot_type="nasa_arm"):
    today = datetime.date.today()
    todaystr = today.isoformat()
    filepath = os.path.expanduser(
        "~/git/ego/ego-projects-byu/python/scripts/optimization/optimal_designs/"+todaystr)
    try: #This will make a new file path if there already isn't one.  If there is, it will ask if it should replace it.
        os.mkdir(filepath)
        for i, arm in enumerate(arms):
            print "Saving Pickle of Best Arm"
            pk.save_pickle(os.path.join(filepath, 'best_arm_lengths_%s' % str(i)), arm.lengths)
            pk.save_pickle(os.path.join(filepath, 'best_arm_hom_%s' % str(i)), arm.base_hom)
            if robot_type == "leg" or robot_type == "jabberwock":
                pk.save_pickle(os.path.join(filepath, 'best_arm_bend_angles_%s' % str(i)), arm.bend_angles)

    except:
        key = raw_input("Replace Already Existing Optimal Design? y/n")
        while(key != 'y' and key != 'n'):
            key = raw_input("Replace Already Existing Optimal Design? y/n")

        if key == 'y':
            print filepath
            for i, arm in enumerate(arms):
                print "Optimal Design replaced"
                pk.save_pickle(os.path.join(filepath, 'best_arm_lengths_%s' % str(i)), arm.lengths)
                pk.save_pickle(os.path.join(filepath, 'best_arm_hom_%s' % str(i)), arm.base_hom)
                if robot_type == "leg" or robot_type == "jabberwock":
                    pk.save_pickle(os.path.join(filepath, 'best_arm_bend_angles_%s' % str(i)), arm.bend_angles)

        if key == 'n':
            print "Optimal Design Not Saved"

def load_leg_pickle(lengths_filename, hom_filename, bend_angles_filename=""):
    lengths = pk.load_pickle(lengths_filename)
    hom = pk.load_pickle(hom_filename)
    bend_angles = pk.load_pickle(bend_angles_filename)
    arm = leg(lengths, bend_angles, hom, .2286)
    return arm

def get_jt_positions(link_lengths):
    pos = []
    for i in range(len(link_lengths)):
        pos.append(sum(link_lengths[0:i]))
    pos.append(sum(link_lengths))
    return pos


def get_link_lengths(jt_positions):
    lengths = []
    for i in range(len(jt_positions)-1):
        lengths.append(jt_positions[i+1]-jt_positions[i])
    return lengths


if __name__ == '__main__':
    # Plots arm in pck
    # lengths = pk.load_pickle('invalid_arm_lengths')
    # hom = pk.load_pickle('invalid_arm_hom')
    # r,t = kt.hom2ax_vec(hom)
    # print r
    # print t
    # print lengths
    lengths = np.array([.0,.27,.27,.17,.20])
    r = np.array([-np.pi/2.0,0,0])
    t = np.array([0,0,.5])
    hom = kt.ax_vec2homog(r,t)
    jt_height = 0
    arm = nasa_arm(jt_height,lengths,hom)

    # arm.setJointAngsRandom()
    # arm.calcFK()

    # pizza = kin.Arm()

    # # print np.identity(4)
    # pizza = leg(np.array([.2,.5,.7,.1]),np.array([0,0,0,0]),np.array([[1.0, 0.0, 0.0, 50.0],[0.0,1.0,0.0,1.0],[0.0,0.0,1.0,1.0],[0.0,0.0,0.0,1.0]]),.20)
    # # print np.array([[1.0, 0.0, 0.0, 0.0],[0.0,1.0,0.0,0.0],[0.0,0.0,1.0,0.0],[0.0,0.0,0.0,1.0]])

    # pizza.setJointAngs(np.array([np.pi/2, 0.0, np.pi/2, 0.0]))
    # # # pizza.setJointAngsRandom()
    # print pizza.joint_angs_
    # pizza.calcFK()


    # print pizza.g_world_tool()
    # '''angles = np.array([-np.pi/4, -np.pi/4])
    # # angles = np.array([0,0])
    # lengths = np.array([.2, .2, .2, .2, .2])
    # angs = np.array([
    #     0.789074,
    #     0.404205,
    #     0.0513736,
    #     -1.52079,
    #     1.10384,
    #     0.321514])
    # base_hom = np.identity(4)
    # arm = jabberwock(lengths, angles, base_hom)

    # pickle_arms([arm])'''
    # arm.setJointAngs(angs)
    # arm.calcFK()

    # Eval = ae.ArmEvalFK(arm)
    # Eval.print_on = False
    # Eval.XYZRes_ = 20
    # Eval.SO3Res_ = 3
    # Eval.Initialize()

    # #Scoring
    # Eval.redundancy_cutoff_ = .9
    # Eval.Run()
    # scores = unwrap_scores(Eval.scores_)
    # print scores
    plot_fks(arm)
    # print "Success"

    # base_hom= np.eye(4)
    # my_arm = baxter_arm(base_hom)
