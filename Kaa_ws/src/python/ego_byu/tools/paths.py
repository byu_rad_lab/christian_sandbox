#!/usr/bin/env python
import kinematics as kin
from mpl_toolkits.mplot3d import Axes3D  # NOQA
import matplotlib.pyplot as plt
import numpy as np
from ego_byu.tools.spline_planner.path_planner import CouponPlanner
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import arm_plotter as ap
from ego_byu.tools import constructors
from ego_byu.tools import pickle_tools as pk
import rospy
from copy import deepcopy


def testpath(path_func):
    if callable(path_func):
        plt.figure(1)
        ax = plt.gca(projection="3d")
        for i in np.linspace(0, 1, 10):
            g, wv, wr = path_func(i)
            ap.plot_gnomon(ax, g)
        ap.set_axes_equal(ax)
        plt.show()

    else:
        plt.figure(1)
        ax = plt.gca(projection="3d")
        for i in path_func["targets"]:
            ap.plot_gnomon(ax, i)
        ap.plot_gnomon(ax, np.eye(4))
        ap.set_axes_equal(ax)
        plt.show()

def LineX(t):
    fx = t*1
    fy = .2
    fz = 1.0
    wx = 0
    wy = 0
    wz = 0
    # if t==1:
    vw = np.array([1.0, 1.0, 1.0])
    # else:
    #     vw = np.array([.0, .0, 0.0])
    rw = np.array([.0, .0, 0.0])
    g = kt.ax_vec2homog(np.array([wx, wy, wz]), np.array([fx, fy, fz]))
    return g, vw, rw

def LineY(t):
    fx = .8
    fy = t*1.5-1
    fz = 1.0
    wx = 0
    wy = 0
    wz = 0
    if t == 0:
        vw = np.array([1.0, 1.0, 1.0])
    else:
        vw = np.array([.0, .0, 0.0])
    rw = np.array([.0, .0, 0.0])
    g = kt.ax_vec2homog(np.array([wx, wy, wz]), np.array([fx, fy, fz]))
    return g, vw, rw

def NoPath(t):
    fx = 0
    fy = 0
    fz = 0
    wx = 0
    wy = 0
    wz = 0
    vw = np.array([.0, .0, 0.0])
    rw = np.array([.0, .0, 0.0])
    g = np.eye(4)
    return g, vw, rw


def LineYZ(t):
    fx = 1
    fy = 2*t-1
    fz = -(2*t-1)**2+1.5
    wx = 0
    wy = 0
    wz = 0
    vw = np.array([1.0, 1.0, 1.0])
    rw = np.array([.2, .2, 0.0])
    g = kt.ax_vec2homog(np.array([wx, wy, wz]), np.array([fx, fy, fz]))
    return g, vw, rw

# Sine Wave


def Sine(t):
    fx = 1.0
    fy = -.5+t*1
    fz = np.sin(t*2*np.pi)*.2
    wx = 0
    wy = np.pi/2.0
    wz = 0
    vw = np.array([1.0, 1.0, 1.0])
    rw = np.array([.2, .2, 0.0])
    g = kt.ax_vec2homog(np.array([wx, wy, wz]), np.array([fx, fy, fz]))
    return g, vw, rw

# Coupon


def Coupon(t):
    width = 1
    height = 1.0

    # Path Snakes like a lawnmower starting with top left and going down
    nh = 6.0  # Number of paths down or up
    nw = nh-1.0  # Number of paths connecting vertical passes
    assert(nh-nw == 1.0)

    xe = width/nw
    ye = height

    xp = 1.0/(nw+nw*nh*height/width)
    yp = (1-nw*xp)/nh

    cycles = int(t/(2*xp+2*yp))
    t_ext = t-cycles*(2*xp+2*yp)

    if t_ext <= yp:
        xoff = 0
        yoff = -ye*t_ext/yp
    elif yp < t_ext <= yp+xp:
        xoff = (t_ext-yp)/xp*xe
        yoff = -ye
    elif yp+xp < t_ext <= 2*yp+xp:
        xoff = xe
        yoff = -ye+(t_ext-xp-yp)/yp*ye
    elif 2*yp+xp < t_ext <= 2*yp+2*xp:
        xoff = xe+(t_ext-2*yp-xp)/xp*xe
        yoff = 0

    fx = 1.0
    fy = cycles*xe*2+xoff-width/2.0
    fz = ye+yoff

    wx = 0
    wy = np.pi/2.0
    wz = 0
    vw = np.array([1.0, 1.0, 1.0])
    rw = np.array([.2, .2, 0.0])
    g = kt.ax_vec2homog(np.array([wx, wy, wz]), np.array([fx, fy, fz]))
    return g, vw, rw


def Krex_arc(t):
    fx = -(2*t-1)**2*1.5+1.5-.1+.2
    fy = 1.60*(1-t)-.30*(t)
    # fz = -(1-2*t)**2*.4+.45
    fz = 0
    wx = 0
    # wy=np.pi/2.0
    wy = 0
    wz = 0
    vw = np.array([1.0, 1.0, 1.0])
    rw = np.array([.2, .2, 0.0])
    g = kt.ax_vec2homog(np.array([wx, wy, wz]), np.array([fx, fy, fz]))
    return g, vw, rw


def Krex_arc4DOF(t):
    fx = .50
    fy = 1*(1-t)
    fz = -(1-2*t)**2*.5+2
    wx = 0
    wy = 0
    wz = 0

    vw = np.array([1.0, 1.0, 1.0])
    rw = np.array([.2, .2, 0.0])
    g = kt.ax_vec2homog(np.array([wx, wy, wz]), np.array([fx, fy, fz]))
    return g, vw, rw


def Circle(t):
    c = np.array([.7, .5, 1.1])
    rad = .5
    fx = c[0]+np.cos(1*np.pi*(t*.8)-np.pi/3)*rad
    fy = c[1]+np.cos(1*np.pi*(t*1)-np.pi/3)*rad
    fz = c[2]+np.sin(1*np.pi*(t*1)-np.pi/3)*rad
    wx = 0
    wy = 0
    wz = 0
    vw = np.array([1.0, 1.0, 1.0])
    rw = np.array([.2, .2, 0.0])
    g = kt.ax_vec2homog(np.array([wx, wy, wz]), np.array([fx, fy, fz]))
    return g, vw, rw


def LineBetween(h1, h2,fvw=np.array([1.0, 1.0, 1.0]),frw=np.array([0.0, 0.0, 0.0])):
    def Line(t):
        # fx = (1-t)*h1[0][3]+t*h2[0][3]
        # fy = (1-t)*h1[1][3]+t*h2[1][3]
        # fz = (1-t)*h1[2][3]+t*h2[2][3]
        # wx = 0
        # wy = 0
        # wz = 0
        if t == 0:
            vw = np.array([1.0, 1.0, 1.0])
            rw = np.array([0.0, 0.0, 0.0])
        elif t == 1:
            vw = fvw
            rw = frw
        else:
            vw = np.array([0.0, 0.0, 0.0])
            rw = np.array([.0, .0, 0.0])
        g = kt.hom_interp2(h1,h2,t)
        return g, vw, rw
    return Line

def PNP(g2,g4):
    r2,t2 = kt.hom2ax_vec(g2)
    g2 = kt.ax_vec2homog(np.array([0,np.pi,0]),t2)
    g1 = deepcopy(g2)
    g1[2,3]+=.20
    r4,t4 = kt.hom2ax_vec(g4)
    g4 = kt.ax_vec2homog(np.array([0,np.pi,0]),t4)
    g3 = deepcopy(g4)
    g3[2,3]+=.20

    #Add Empty targets (no weight) to allow arm  stretch
    g0  = np.eye(4)
    g = [g1,g2,g1,g0,g0,g0,g0,g0,g0,g3,g4,g3]
    pos_wts = np.zeros([0,3])
    rot_wts = np.zeros([0,3])
    for i in range(3):
        pos_wts = np.vstack([pos_wts,np.array([1.0,1.0,1.0])])
        rot_wts = np.vstack([rot_wts,np.array([.2,.2,0.0])])
    for i in range(6):
        pos_wts = np.vstack([pos_wts,np.array([0.0,0.0,0.0])])
        rot_wts = np.vstack([rot_wts,np.array([0.0,0.0,0.0])])
    for i in range(3):
        pos_wts = np.vstack([pos_wts,np.array([1.0,1.0,1.0])])
        rot_wts = np.vstack([rot_wts,np.array([.2,.2,0.0])])

    return {"targets" : g, "pos_wts" : pos_wts, "rot_wts": rot_wts}

def BraceGround(g_ob):
    r,t = kt.hom2ax_vec(g_ob)
    g_ob = kt.ax_vec2homog(np.array([0,np.pi/2.0,0]),t)

    g0  = np.eye(4)
    g = [g_ob]

    #Add Empty targets (no weight) to allow arm stretch
    for i in range(3):
        pos_wts = np.vstack([pos_wts,np.array([1.0,1.0,1.0])])
        rot_wts = np.vstack([rot_wts,np.array([.2,.2,0.0])])
    for i in range(6):
        pos_wts = np.vstack([pos_wts,np.array([0.0,0.0,0.0])])
        rot_wts = np.vstack([rot_wts,np.array([0.0,0.0,0.0])])
    for i in range(3):
        pos_wts = np.vstack([pos_wts,np.array([1.0,1.0,1.0])])
        rot_wts = np.vstack([rot_wts,np.array([.2,.2,0.0])])

    return {"targets" : g, "pos_wts" : pos_wts, "rot_wts": rot_wts}


def CombinePaths(path1, path2):
    def Combined(t):
        if t < .5:
            return path1(2*t)
        else:
            return path2((t-.5)*2)
    return Combined

# This Function should not be passed into Planner or QPIK, instead, the output should be passed into Planner

def SplineCoupon(num_grid,g_world_blcorner_=np.eye(4), passes = 6, dx=.2,dy =.2 ):
    order = 3
    planner = CouponPlanner(order,dx,dy)
    planner.num_passes = passes
    planner.create_path()
    planner.plan()
    planner.optimize_time(4)

    sp = planner.spliner

    # Analyize numerically
    time, state = sp.discretize(num_grid)
    targets = []
    pos_wts = np.zeros([0, 3])
    rot_wts = np.zeros([0, 3])
    vw=np.array([1.0, 1.0, 1.0])
    rw=np.array([.1,.1,.1])
    for pos in state[0]:
        r = np.zeros(3)
        t = np.array(pos)
        g = kt.ax_vec2homog(r, t)
        target = g_world_blcorner_.dot(g)
        targets.append(target)
        pos_wts = np.vstack([pos_wts, vw])
        rot_wts = np.vstack([rot_wts, rw])

    path = {"targets": targets, "pos_wts": pos_wts, "rot_wts": rot_wts}
    return path

def Surface(num_grid,g_world_blcorner_=np.eye(4), passes = 6,dx = .2,dy = .2):
    order = 3
    planner = CouponPlanner(order,dx,dy)
    planner.num_passes = passes
    planner.create_path()
    planner.plan()
    planner.optimize_time(4)

    sp = planner.spliner

    # Analyize numerically
    time, state = sp.discretize(num_grid)

    targets = []
    pos_wts = np.zeros([0, 3])
    rot_wts = np.zeros([0, 3])
    vw=np.array([1., 1., 1.])
    rw=np.array([.1,.1,0])
    for pos in state[0]:
        r = np.zeros(3)
        r[0] = np.pi/2.0
        t = np.array(pos)
        g = kt.ax_vec2homog(r, t)
        target = g_world_blcorner_.dot(g)
        targets.append(target)
        pos_wts = np.vstack([pos_wts, vw])
        rot_wts = np.vstack([rot_wts, rw])

    path = {"targets": targets, "pos_wts": pos_wts, "rot_wts": rot_wts}
    return path

if __name__ == "__main__":
# ===================================FOR TESTING TARGETS=====================
#     rospy.init_node('PathsTester', anonymous=False)
# base = constructors.BaseFixed()
#     base = constructors.BaseFixedCortex()
#     narms = 10
#     arm = constructors.NasaArms(narms,base)[0]
#     arm.setJointAngsReal(np.array([0,-np.pi/2.0,0,0,0,0]))
#     arm.calcFK()
#     h1 = arm.getGWorldTool()

#     plt.figure(1)
#     ax = plt.gca(projection="3d")
#     armplotter = ap.SimpleArmPlotter(arm)
#     armplotter.plot(ax)
#     ap.set_axes_equal(ax)

#     h2,wv,wr = LineY(0)
# testpath(LineBetween(h1,h2))
#     testpath(CombinePaths(LineBetween(h1,h2),LineY))
# ===========================================FOR TESTING SPLINECOUPON====
    # r = np.array([0, 0, np.pi/2.0])
    # t = np.array([0, 0, 0])
    # g1 = kt.ax_vec2homog(r, t)
    # r = np.array([0, np.pi/2.0, 0])
    # t = np.array([1, 0, 1])
    # g2 = kt.ax_vec2homog(r, t)
    # offset = g2.dot(g1)
    # path = SplineCoupon(20, offset)
    # path = SplineCoupon(20, offset)
    # testpath(path)
# ===========================================FOR TESTING PNP====
    r = np.array([0, 0, np.pi,0])
    t = np.array([.5, -.5, 0])
    g1 = kt.ax_vec2homog(r, t)
    r = np.array([0, np.pi, 0])
    t = np.array([0,-.50, .5])
    g2 = kt.ax_vec2homog(r, t)
    path = PNP(g1,g2)
    testpath(path)
