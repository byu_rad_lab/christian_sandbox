#!/usr/bin/env python
import numpy as np

# Collision constraints


def Collision_Weight_Func(distance, dist_tol, geom_weight):
    if distance < dist_tol:
        weight = geom_weight*((distance-dist_tol)/dist_tol)**2
        return weight
    else:
        return 0


def Touch_Weight_Func(distance, squeeze_tol, geom_weight):
    if distance > -squeeze_tol:
        weight = geom_weight*(distance+squeeze_tol)**2
    return weight


def Ground(type="collision"):
    point = np.zeros([3, 1])
    normal = np.zeros([3, 1])
    normal[2] += 1
    weight = .02
    return {'shape': "Plane", 'point': point, 'normal': normal, 'weight': weight, 'type': type}


def CustomPlane(offset, link_nums = [], type="collision",weight = .2):
    if link_nums != []:
        return {'shape': "Plane", 'point': offset[:3, 3].reshape(3, 1)+offset[:3, 2].reshape(3, 1)*-.05, "normal": offset[:3, 2].reshape(3, 1), "weight": weight, "type": "collision", "link_nums": link_nums}
    else:
        return {'shape': "Plane", 'point': offset[:3, 3].reshape(3, 1)+offset[:3, 2].reshape(3, 1)*-.05, "normal": offset[:3, 2].reshape(3, 1), "weight": weight, "type": "collision"}


def KrexProximalArmSphere(weight_func=Collision_Weight_Func):
    point = np.array([1.584/2.0-.1, (1.2972/2.0-.21592/2.0), .4]).reshape(3, 1)
    radius = .25
    weight = .2
    return {'shape': "Sphere", 'point': point, 'radius': radius, 'weight': weight, 'type': type}


def KrexBodyPlane(type="collision", weight_func=Collision_Weight_Func):
    point = np.array([.22, 0, 0]).reshape(3, 1)
    normal = np.array([1, 0, 0]).reshape(3, 1)
    weight = .2
    return {'shape': "Plane", 'point': point, 'normal': normal, 'weight': weight, 'type': type}


def KrexDistalArmPlane(weight_func=Collision_Weight_Func):
    point = np.array([0, -(1.2972/2.0-.21592)+.05, 0]).reshape(3, 1)
    normal = np.array([0, 1, 0]).reshape(3, 1)
    weight = .2
    return {'shape': "Plane", 'point': point, 'normal': normal, 'weight': weight, 'type': type}


def KrexDistalBodySphere(weight_func=Collision_Weight_Func):
    point = np.array([.2, -(1.2972/2.0-.21592/2.0)+.3, .5]).reshape(3, 1)
    radius = .2
    weight = .2
    return {'shape': "Sphere", 'point': point, 'radius': radius, "weight": weight, 'type': type}


def KrexProximalBodySphere(weight_func=Collision_Weight_Func):
    point = np.array([.1, (1.2972/2.0-.21592/2.0), .6]).reshape(3, 1)
    radius = .25
    weight = .2
    return {'shape': "Sphere", 'point': point, 'radius': radius, "weight": weight, 'type': type}


# def KrexBodyCylinder(weight_func=Collision_Weight_Func):
#     point = np.array([.2, -(1.2972/2.0-.21592/2.0), .3]).reshape(3, 1)
    # direction = np.array([0, 1, 0]).reshape(3, 1)
    # weight = .2
    # radius = 0
    # return {
    #     'shape': "Cylinder",
    #     'radius': radius
    #     'point': point,
    #     'direction': direction,
    #     "weight": weight,
    #  'type': type}

# For 2d Example


def SphereConstraint(weight_func=Collision_Weight_Func):
    point = np.array([.8, -.4, 0]).reshape(3, 1)
    radius = .15
    weight = 1
    return {'shape': "Sphere", 'point': point, 'radius': radius, "weight": weight, 'type': type}


def WallConstraint(weight_func=Collision_Weight_Func):
    point = np.array([1, 0, 0]).reshape(3, 1)
    normal = np.array([-1, 0, 0]).reshape(3, 1)
    weight = 1
    return {'shape': "Plane", 'point': point, 'normal': normal, 'weight': weight, 'type': type}

