%module chilipy

%{
  #define SWIG_FILE_WITH_INIT
  #include "chilipy.h"
%}

%include "std_vector.i"
%include "typemaps.i"
%include "numpy.i"
%include "eigen.i"
%include "opencv.i"

// needed for numpy
%init
%{
  import_array();
%}

%template(IntVector) std::vector<int>;

%eigen_typemaps(corners_t);

%include "chilipy.h";

