// file chilipy.h

#include "chilitags/chilitags.hpp"

#include <eigen3/Eigen/Dense>

typedef Eigen::Matrix<double, 4, 2> corners_t;
typedef std::vector<int> int_vec_t;

class ChiliPy {
    public:
        // processes img, returns vector of keys
        int_vec_t process(cv::Mat &img);

        // returns corner locations given tag key
        corners_t get_corners(int key);

        // setters for the various DetectionTrigger
        void set_dt_track_and_detect();
        void set_dt_detect_only();
        void set_dt_track_only();
        void set_dt_detect_periodically();
        void set_dt_async_detect_periodically();
        void set_dt_async_detect_always();

    private:
        // hide these from SWIG
        chilitags::Chilitags ct_;
        chilitags::TagCornerMap corner_map_;
        chilitags::Chilitags::DetectionTrigger dt_;
};