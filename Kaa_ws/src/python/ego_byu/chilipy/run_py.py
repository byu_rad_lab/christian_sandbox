#!/usr/bin/env python

import numpy as np  # NOQA import numpy before chilipy
import chilipy as cp
import cv2

cw = cp.ChiliPy()

# test passing image from python
img = cv2.imread("test.jpg", 0)
t = cw.process(img)

tag_dict = {}
for key in t:
    # print "key = ", key
    tag_dict[key] = cw.get_corners(key)

print "\ntag_dict = "
for key, corners in tag_dict.iteritems():
    print "key = ", key
    print "corners = "
    print corners
    print

# NOTE:  using key, as id is a Python keyword

# EOF
