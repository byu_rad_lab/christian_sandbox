// file chilipy.cpp

#include "chilipy.h"

#include <stdio.h>
#include <iostream>

#include <opencv2/highgui/highgui.hpp>  // for imread

using namespace cv;
using namespace chilitags;

int_vec_t ChiliPy::process(cv::Mat& img) {

  // store corners in ChiliPy
  ct_.setFilter(0,0.0f);
  corner_map_ = ct_.find(img, dt_);

  // Put
  int_vec_t key_vector;
  for (auto const& corner : corner_map_) {
    // std::cout << "first = " << corner.first << std::endl;
    key_vector.push_back(corner.first);
  }
  return key_vector;
}

corners_t ChiliPy::get_corners(int key) {
  // std::cout << "corner_map_[key] = " << corner_map_[key] << std::endl;

  corners_t corners;
  // check if key exists
  if (corner_map_.find(key) == corner_map_.end()) {
    // If not found, return zero matrix.
    std::cout << "Warning:  key not found." << std::endl;
    corners.setZero();
  } else {
    // If found, convert to Eigen 4x2
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 2; j++) {
        corners(i, j) = (double)corner_map_[key](i, j);
      }
    }
  }
  // std::cout << "Eigen corners = " << std::endl << corners << std::endl;
  return corners;
}

void ChiliPy::set_dt_track_and_detect() {
  dt_ = Chilitags::DetectionTrigger::TRACK_AND_DETECT;
}

void ChiliPy::set_dt_detect_only() {
  dt_ = Chilitags::DetectionTrigger::DETECT_ONLY;
}

void ChiliPy::set_dt_track_only() {
  dt_ = Chilitags::DetectionTrigger::TRACK_ONLY;
}

void ChiliPy::set_dt_detect_periodically() {
  dt_ = Chilitags::DetectionTrigger::DETECT_PERIODICALLY;
}

void ChiliPy::set_dt_async_detect_periodically() {
  dt_ = Chilitags::DetectionTrigger::ASYNC_DETECT_PERIODICALLY;
}

void ChiliPy::set_dt_async_detect_always() {
  dt_ = Chilitags::DetectionTrigger::ASYNC_DETECT_ALWAYS;
}

// Partially copied from chilitags
int main(int argc, char* argv[]) {
  if (argc != 2) {
    std::cout << "Usage: chilitags-detect <image>\n\n"
              << "Returns the list of detected tag id's in the image, one per line.\n";
    return 1;
  }

  ChiliPy cw;

  cv::Mat image = cv::imread(argv[1]);
  if (image.data) {
    std::cout << "Processing data." << std::endl;
    int_vec_t w = cw.process(image);
    corners_t corner;
    for (auto const& key : w) {
      std::cout << "key[i] = " << key << std::endl;
      corner = cw.get_corners(key);
      std::cout << "Eigen corner = " << std::endl << corner << std::endl;
    }
    std::cout << "\nChecking key -1, should warn:" << std::endl;
    corner = cw.get_corners(-1);
    std::cout << "Eigen corner = " << std::endl << corner << std::endl;
    return 0;
  }
  return 1;
}
