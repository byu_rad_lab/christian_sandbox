%{
#define SWIG_FILE_WITH_INIT
%}

%include "numpy.i"
%init %{
import_array();
%}

%fragment("OpenCV_Fragments", "header", fragment="NumPy_Fragments") {

  int convert_type_cv_mat_to_numpy(int cv_type) {
    int np_type;
    switch (CV_MAT_DEPTH(cv_type)) {
      case CV_8U:
        np_type = NPY_UBYTE;
        break;
      case CV_8S:
        np_type = NPY_BYTE;
        break;
      case CV_16U:
        np_type = NPY_USHORT;
        break;
      case CV_16S:
        np_type = NPY_SHORT;
        break;
      case CV_32F:
        np_type = NPY_FLOAT;
        break;
      case CV_32S:
        np_type = NPY_INT;
        break;
      case CV_64F:
        np_type = NPY_DOUBLE;
        break;
      default:
        PyErr_SetString(PyExc_TypeError, "Unsupported OpenCV data type.");
        np_type = -1;
    }
    return np_type;
  }

  // convert from opencv Mat to numpy array
  PyObject* mat_to_array(const cv::Mat& mat)
  {
    // get NumPy type from OpenCV type
    int typenum = convert_type_cv_mat_to_numpy(mat.type());

    int nd = 2;
    if (mat.channels() > 1) {
      nd = 3;
    }

    npy_intp dims[3];
    dims[0] = mat.rows;
    dims[1] = mat.cols;
    dims[2] = mat.channels();

    if (!mat.isContinuous()) {
      PyErr_SetString(PyExc_TypeError, "OpenCV Mat must be contiguous.");
    }
    PyObject* array = PyArray_SimpleNewFromData(nd, dims, typenum, mat.data);
    return array;
  }

  // convert from numpy array to opencv Mat
  cv::Mat* convert_numpy_array_to_cv_mat(PyObject* obj) {
    PyArrayObject* array = obj_to_array_no_conversion(obj, NPY_NOTYPE);
    if (array == NULL) {
      return NULL;
    }

    int nd = array_numdims(array);
    int channels;
    if (nd == 2)
      channels = 1;
    else if (nd == 3) {
      channels = array_size(array, 2);
      //TODO: check if number of channels is appropriate
    }
    else
    {
      PyErr_Format(PyExc_TypeError, "Array must have 2 or 3 dims.  Has %d dims.", nd);
    }

    // get appropriate opencv type
    int type;
    switch (array_type(array)) {
      case NPY_BYTE:
        type = CV_MAKETYPE(CV_8S, channels);
        break;
      case NPY_UBYTE:
        type = CV_MAKETYPE(CV_8U, channels);
        break;
      case NPY_SHORT:
        type = CV_MAKETYPE(CV_16S, channels);
        break;
      case NPY_USHORT:
        type = CV_MAKETYPE(CV_16U, channels);
        break;
      case NPY_INT:
        type = CV_MAKETYPE(CV_32S, channels);
        break;
      case NPY_FLOAT:
        type = CV_MAKETYPE(CV_32F, channels);
        break;
      case NPY_DOUBLE:
        type = CV_MAKETYPE(CV_64F, channels);
        break;
      default:
        PyErr_SetString(PyExc_TypeError, "Unsupported Numpy data type.");
        type = -1;
    }
    // Note:  NPY_UINT, NPY_LONG, NPY_ULONG aren't supported

    if (type == -1) {
      return NULL;
    }
    int rows = array_size(array, 0);
    int cols = array_size(array, 1);

    return new cv::Mat(rows, cols, type, array_data(array));
  }

}  // end OpenCV_Fragments


// cv::Mat&
%typemap(in, fragment="OpenCV_Fragments") cv::Mat& {
  $1 = convert_numpy_array_to_cv_mat($input);
  if ($1 == NULL)
      SWIG_fail;
}

// cv::Mat
%typemap(out) cv::Mat {
  PyObject* array = mat_to_array($1);
  if (array == NULL) {
      SWIG_fail;
  }
  cv::Mat* mat = new cv::Mat($1);  // otherwise data gets freed
  $result = array;
}

// EOF
