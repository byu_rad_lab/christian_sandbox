#!/usr/bin/env python
import numpy as np

# Robot Type - Note - The FK simulation counts all poses within the
# discretization with links above the ground as valid poses.  To set a
# desired pose to optimize for or adjust other evaluation settings, edit
# ~/git/src/armeval/src/arm_eval_fk.cpp - "validpose" func. definition

# robot_type = "jabberwock"
# robot_type = "nasa_arm"
robot_type = "leg"

# Optimization Type
# optimization_type = "single_objective"
optimization_type = "multi_objective"

# Optimization Options
Planned_Generations = 101
Number_Of_Designs = 40
FK_Redundancy_Cutoff = .90

if robot_type == "nasa_arm" or robot_type =="jabberwock":
    # Forward Kinematic Routine Options
    XYZ_Resolution = 30
    SO3_Resolution = 5
    Gridsize = 3.0  # Cube bounding box - e.g 3.0 -> 3m x 3m x 3m
    Gridcenter = np.array([0, 1.5, 1.5])

    # Armspan Limits
    Armspan_Limits = [0, 1.5]
    Number_of_Links = [6, 7] # rounds down, second number is never reached nasa arm

    # Constraints For Link Lengths
    Link_Lower_Limits = [0, 0, 0, 0, 0, 0]
    Link_Upper_Limits = [.5, .5, .5, .5, .5, .5]

    # Constraints For Base
    Base_X_Range = [0.0, 0.0]
    Base_Y_Range = [0.0, 0.0]
    Base_Z_Range = [.3, 1.5]

if robot_type == "leg":
    # Forward Kinematic Routine Options
    XYZ_Resolution = 30
    SO3_Resolution = 5
    Gridsize = 3.0 # Cube bounding box was 6.0 before
    Gridcenter = np.array([0.0, 0.0, 0.0])

    # Armspan Limits
    Armspan_Limits = [0, 1.2192] #before was 2.0 need to find a good value for this (if it needs to be constrained at all)
    Number_of_Links = [4, 5]  # rounds down, second number is never reached leg

    # Constraints For Link Lengths
    Link_Lower_Limits = [0, 0.0, 0, 0.0] #[0, 0, 0, 0]
    Link_Upper_Limits = [0.5, 0.5, 0.5, 0.5]

    # Constraints For Base
    Base_X_Range = [0.0, 0.0]
    Base_Y_Range = [0.0, 0.0]
    Base_Z_Range = [0.0, 0.0]

    # Link Density for Weight purposes
    Link_Density = .0003461*7850 #.0003461 is the surface area of the 4 hex shaped rods and 7850 is the density of steel

# Mutation & Perturbation Parameters
Mutation_Rate = .3
Initial_Max_Perterbation_Lengths = .05
Initial_Max_Perterbation_Base_Trans = 0.10  # Defines pertation bounding box
Initial_Max_Perturbation_Base_Angle = np.pi/20.0  # Defines pertation bounding box
Initial_Max_Perterbation_Jabberwock_Angles = np.pi/20.0  # Defines pertation bounding box

# JabberWock and Leg Constraints
Link_Bend_Constraints = [-np.pi/2.0, np.pi/2.0] #[0.0, 0.0] #[-np.pi/2.0, np.pi/2.0]  #[-np.pi/2.0, 0] for Jabberwock max angles it can bend around x and y axis
Leg_Base_Angle_Constraint = [-np.pi/2.0, np.pi] # max angles base can bend around the world x (and soon hopefully y) axes
Jabber_Base_Angle_Constraint = [-np.pi/4.0, np.pi/4.0] # max angles base can bend around the world x (and soon hopefully y) axes

# NasaArm Constraints
Nasa_Valve_Constraint = .182  # There cannot be two consecutive links under this limit for valve
Nasa_Joint_Height = .06
Nasa_Base_Angle_Constraint = [-np.pi/3.0-np.pi/2.0, np.pi/3.0-np.pi/2.0]  # NASA ARM

# Plotting Options
Plotted_Generations = [1, 5, 10, 50, 75, 100]

# FilePath For Saving Designs
