#!/usr/bin/env python

from scripts.optimization import optimization as opt

designs = 40
generations = 21
robot_type = 'nasa_arm'
mode = "multi_objective"
evolution_solver = opt.Optimization(designs, generations, robot_type, mode)
evolution_solver.run()
