#!/usr/bin/env python
# from __future__ import division
from ego_byu.armeval import armeval as ae
from ego_core.kinematics import kinematics as kin
import time, os,datetime
import numpy as np
from ego_byu.tools import arm_tools as at
from ego_byu.tools import arm_plotter as ap
from ego_byu.tools import pickle_tools as pk
from ego_byu.tools import kin_tools as kt
import matplotlib.pyplot as plt
import subprocess
import pref as pref
import copy

image_number = 1
# today=datetime.date.today()
# todaystr = today.isoformat()
# filepath =os.path.join("/home/radlab/Desktop/Visualization/")
# if not os.path.exists(filepath):
#     os.makedirs(filepath)
filepath = "/home/vsherrod/Desktop/Visualization/"

def EvaluateArm(arm,XYZRes = pref.XYZRes,SO3Res=pref.SO3Res, mode = pref.mode, dth_num_=pref.dth_num_,FKCutoff=pref.FKCutoff):
    print "Evaluator set up"
    Evaluator = ae.ArmEvalFK(arm)
    Evaluator.setXYZRes(XYZRes)
    Evaluator.setSO3Res(SO3Res)
    Evaluator.setBounds(pref.ws_dimension)
    Evaluator.setCenterDiscretization(pref.ws_center)
    Evaluator.dth_num_ = dth_num_ #only when using SYSTEMATIC
    Evaluator.print_on = True
    print Evaluator.dth_num_

    #Mode: Enum{0 = PERCENT REDUNDANT, 1 = USER INPUT, 2 = SYSTEMATIC, 3 = FIXED_TIME}
    Evaluator.mode_ = mode
    Evaluator.redundancy_cutoff_ = FKCutoff
    Evaluator.Initialize()
    Evaluator.Run()
    print Evaluator.scores_.orientations_
    print Evaluator.scores_.payload_score_
    return Evaluator

def make_heat_map(metrics,x_vals,y_vals,labels,xval):
    x, y = np.meshgrid(x_vals,y_vals)

    plt.figure(figsize=(10,10))
    # plt.subplot(231)
    plt.title(labels[0],fontsize=30)
    plt.xlabel(labels[1],fontsize=20)
    plt.ylabel(labels[2],fontsize=20)
    plt.pcolormesh(x, y, metrics, vmin=0, vmax=1)
    # plt.imshow(metrics, cmap = 'RdBu', vmin=0, vmax=1,extent= [x_vals[0],x_vals[-1],y_vals[0],y_vals[-1]])
    plt.axis([x_vals[0],x_vals[-1],y_vals[0],y_vals[-1]])
    cb = plt.colorbar()
    cb.set_label(labels[3],fontsize = 20)

    #Make directory if doesn't exist

    #Save File
    if pref.save == True:
        global image_number
        global filepath
        file = filepath+'image_%03d.png'% image_number
        image_number +=1
        print file
        plt.savefig(file)
    if image_number == 31:
        for i in range(10):
            global image_number
            global filepath
            file = filepath+'image_%03d.png'% image_number
            image_number +=1
            print file
            plt.savefig(file)

    if pref.save == False:
        plt.show()

def ws_visualization(Evaluator):

    # xisup = np.array([])
    # xisdown = np.array([])
    # for i in range(100000):
    #     xisup = np.append(xisup,Evaluator.getValFromVecDouble(i,Evaluator.wxis))
    #     xisdown = np.append(xisdown,Evaluator.getValFromVecDouble(i,Evaluator.wyis))

    # plt.figure(1)
    # plt.suptitle("Distributions of Axial Components from RotToAx (above and below centerline)")
    # plt.subplot(211)
    # plt.hist(xisup,bins=50,align='mid', range=[-3.5,3.5])
    # plt.title("ax(0) distribution above")
    # plt.subplot(212)
    # plt.hist(xisdown,bins=50,align='mid',range=[-3.5,3.5])
    # plt.title("ax(0) distribution below")
    # plt.show()
    # print "#of up", len(xisup)
    # print "#of down", len(xisdown)
    # assert(False)


    # #Collect Metrics
    # #3D Grids
    n = pref.XYZRes+1
    # bounds = Evaluator.bounds_

    # # Get all 3D Data
    x_vals = np.array([])
    y_vals = np.array([])
    z_vals = np.array([])
    for i in range(n):
        x_vals = np.append(x_vals,Evaluator.getValFromVecDouble(i,Evaluator.getx_values_()))
        y_vals = np.append(y_vals,Evaluator.getValFromVecDouble(i,Evaluator.gety_values_()))
        z_vals = np.append(z_vals,Evaluator.getValFromVecDouble(i,Evaluator.getz_values_()))


    plotted_metric = np.zeros((len(x_vals),len(y_vals),len(z_vals)),dtype=float)
    for i in range(len(x_vals)):
        for j in range(len(y_vals)):
            for k in range(len(z_vals)):
                metric = Evaluator.get_metric(i,j,k)
                plotted_metric[i][j][k] = metric.orientations_ #finds number of orientations at each point in the discretization

    planes = np.arange((pref.XYZRes)/2-1,(pref.XYZRes)/2+2)

    # for i,x in enumerate(x_vals):
    for i in planes:
        labels = np.array(["Dexterity Visualization (X = %s)" % x_vals[i],"Y(m)","Z(m)","% Orientations Found"])
        make_heat_map(np.transpose(plotted_metric[i][:][:]/Evaluator.getnum_orientation_vecs_()),y_vals,z_vals,labels,x_vals[i])
        np.set_printoptions(precision=3)

    if pref.save == True:
        os.chdir(filepath)
        subprocess.call("ffmpeg -f image2 -r 10 -i image_%03d.png -vcodec mpeg4 -y 3dvisualization.mp4", shell=True)
        print " Video Generated "

def EvalStats(Evaluator):
    if Evaluator.mode_ !=3:
        print "evaluator must be in hybrid-test mode to work"
        assert(False)

    # plt.figure()
    # plt.gca().grid(True)
    # plt.title("Example of Hybrid Algorithm With Exhaustive IK")
    # plt.xlabel("Algorithm Time (s)")
    # plt.ylabel("Number of Poses in Workspace Found")
    # labels = []


    # t = Evaluators[0].time_tracker_;
    # tik = Evaluators[0].time_tracker_ik_;
    # p = Evaluators[0].poses_found_tracker_;
    # pik = Evaluators[0].poses_found_tracker_ik_;
    # r = Evaluators[0].redundancy_percent_tracker_;
    # plot = plt.plot(t,p)
    # plot = plt.plot(tik,pik)

    # # t = Evaluators[1].time_tracker_;
    # # p = Evaluators[1].poses_found_tracker_;
    # # plot = plt.plot(t,p)

    # labels=["FK-Hybrid","IK-Hybrid","Pure FK"]
    # plt.legend(labels,loc=4)

    plt.figure()
    plt.gca().grid(True)
    plt.title("FK vs IK: Average New Sample Times (50 Samples)")
    plt.xlabel("Algorithm Time (s)")
    plt.ylabel("Avg. Time To Find New Sample (s)")

    t = Evaluator.avg_sample_time_tracker;
    fk = Evaluator.avg_sample_time_FK;
    pik = Evaluator.avg_sample_time_pik;
    piknu = Evaluator.avg_sample_time_pik_no_update;
    qpik = Evaluator.avg_sample_time_qpik;

    plot = plt.plot(t,fk)
    plot = plt.plot(t,pik)
    plot = plt.plot(t,piknu)
    plot = plt.plot(t,qpik)

    plt.legend(["FK","Pinv. IK", "Simple Pinv. IK", "QPIK"],loc=4)
    plt.gca().set_yscale('log')


    # plt.subplot(212)
    # plt.title("Redundancy Percentage per 50,000 Configurations")
    # plt.plot(t,r)
    # plt.gca().grid(True)
    # plt.ylim([0,100])
    plt.show()


def max_value(data):
    max_val = max(data[0])
    for i in range(len(data)):
        max_line = max(data[i])
        if max_line > max_val:
            max_val = max_line
    return max_val

def sum_matrix(data):
    sum_mat = 0;
    for i in range(len(data)):
        sum_mat = sum_mat + sum(data[i])
    return sum_mat

if __name__ == '__main__':

    # Initialize Arm from file
    # today=datetime.date.today()
    # todaystr = today.isoformat()
    # fp =os.path.join("/home/radlab/git/ego/ego-projects-byu/python/scripts/optimization/optimal_designs/2016-09-15/best_arm_lengths_10")
    # lengths = pk.load_pickle(fp)
    # fp =os.path.join("/home/radlab/git/ego/ego-projects-byu/python/scripts/optimization/optimal_designs/2016-09-15/best_arm_hom_10")
    # base_hom = pk.load_pickle(fp)

    #NASA ARM
    lengths = [.0,.270,.280,.390,.19,.01]
    # lengths = [.23,.27,.13,.18,.19,.04]
    # base_hom = kt.ax_vec2homog(np.array([-90*np.pi/180,0,0]),np.array([0,0,1.50]))
    # base_hom = kt.ax_vec2homog(np.array([-150*np.pi/180,0,0]),np.array([0,0,1.19]))
    base_hom = kt.ax_vec2homog(np.array([0.0,0.0,0.0]),np.array([0,0,1.50]))
    # print base_hom
    # raw_input('wait')

    jt_height = .06
    arm = at.nasa_arm(jt_height, lengths, base_hom)


    #JABBERWOCK
    # lengths = np.array([.33,.15,.025,.42,.025])
    # angles = np.array([-20*np.pi/180,-32*np.pi/180])
    # base_hom = kt.ax_vec2homog(np.array([-38*np.pi/180,0,0]),np.array([0,0,0]))
    # base_hom = np.identity(4)
    # base_hom[1][3] = .5
    # base_hom[2][3] = .5
    # arm = at.jabberwock(lengths,angles,base_hom)
    # print arm.getJointAngs()

    arm.setJointAngsZero()
    arm.calcFK()

    # Custom Arm
    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # arm_plotter = ap.SimpleArmPlotter(arm)
    # arm_plotter.plot(ax)
    # ap.set_axes_equal(ax)
    # plt.show()
    dth_num_ = pref.dth_num_

    Evaluators = []
    # Evaluator1 = EvaluateArm(arm)
    # Evaluator1 = EvaluateArm(arm,40,3,0,pref.dth_num_,.9)
    # Evaluators.append(Evaluator1)
    # Evaluator2 = EvaluateArm(arm,40,3,0,dth_num_,.999)
    # Evaluators.append(Evaluator2)
    # Evaluators.append(Evaluator2)
    # Evaluator3 = EvaluateArm(arm,50,7,3,dth_num_,.995)
    # Evaluators.append(Evaluator3)
    # Evaluator4 = EvaluateArm(arm,30,5,0,dth_num_,.95)
    # Evaluators.append(Evaluator4)
    # Evaluator5 = EvaluateArm(arm,30,5,0,dth_num_,.95)
    # Evaluators.append(Evaluator5)

    # Evaluator4 = EvaluateArm(arm,20,3,2,20,.95)
    # Evaluators.append(Evaluator4)
    # Evaluator5 = EvaluateArm(arm,35,5,2,15,.95)
    # Evaluators.append(Evaluator5)

    # ws_visualization(Evaluator1)
    # EvalStats(Evaluator3)

