#!/usr/bin/env python
import visualization as vis
from ego_byu.armeval import armeval as ae
from ego_core.kinematics import kinematics as kin
import time, os,datetime
import numpy as np
from ego_byu.tools import arm_tools as at
from ego_byu.tools import arm_plotter as ap
from ego_byu.tools import pickle_tools as pk
from ego_byu.tools import kin_tools as kt
import matplotlib.pyplot as plt
import subprocess
import pref as pref
import copy

image_number = 1

def plot_ws(arm):
    theta = np.linspace(-np.pi/2.0, np.pi/2.0, 5)
    x = []
    y = []
    z = []
    print arm.getJointAngs()
    for i in range(len(theta)):
        for j in range(len(theta)):
            for k in range(len(theta)):
                for l in range(len(theta)):
                    arm.setJointAngs(np.array([theta[i],theta[j],theta[k],theta[l]]))
                    arm.calcFK()

                    x.append(arm.g_world_tool()[0][3])
                    y.append(arm.g_world_tool()[1][3])
                    z.append(arm.g_world_tool()[2][3])

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter(x,y,z)
    plt.show()

def plot_z_planes(Evaluator):
    # #Collect Metrics
    # #3D Grids
    n = pref.XYZRes+1
    # bounds = Evaluator.bounds_

    k = 18

    # # Get all 3D Data
    x_vals = np.array([])
    y_vals = np.array([])
    z_vals = np.array([])
    for i in range(n):
        x_vals = np.append(x_vals,Evaluator.getValFromVecDouble(i,Evaluator.getx_values_()))
        y_vals = np.append(y_vals,Evaluator.getValFromVecDouble(i,Evaluator.gety_values_()))
        z_vals = np.append(z_vals,Evaluator.getValFromVecDouble(i,Evaluator.getz_values_()))
        # x_vals = np.append(x_vals,[1])
        # y_vals = np.append(y_vals,[2])
        # z_vals = np.append(z_vals,[1])

    print x_vals

    num_orientations = np.zeros((len(x_vals),len(y_vals)))
    num_orientations_list = []

    for k in range(len(z_vals)):
        orientations_at_z = 0
        for i in range(len(x_vals)):
            for j in range(len(y_vals)):
                metric = Evaluator.get_metric(i,j,k)
                num_orientations[j][i] = metric.orientations_
                # num_orientations[i][j] = 0.0
                orientations_at_z = orientations_at_z + metric.orientations_
        print "Orientations at", z_vals[k], " : ", orientations_at_z
        # num_orientations[len(x_vals)/2.0+5][len(y_vals)/2.0] = 1000
        labels = ["Dexterity Visualization (Z = %s)" % z_vals[k], "X(m)", "Y(m)", "Num Orientations"]
        plot_heat_map(x_vals,y_vals,num_orientations,labels)
        # print num_orientations
        num_orientations_list.append(num_orientations)

    # print num_orientations_list
def plot_heat_map(x_vals,y_vals,num_orientations, labels):

    x, y = np.meshgrid(x_vals,y_vals)


    plt.figure(figsize=(10,10))
    plt.title(labels[0],fontsize=30)
    plt.xlabel(labels[1],fontsize=20)
    plt.ylabel(labels[2],fontsize=20)
    plt.pcolormesh(x, y, num_orientations, vmin=0, vmax=10.0)
    plt.axis([x_vals[0],x_vals[-1],y_vals[0],y_vals[-1]])
    cb = plt.colorbar()
    cb.set_label(labels[3],fontsize = 20)

    #Save File
    if pref.save == True:
        global image_number
        # global vis.filepath
        file = vis.filepath+'image_%03d.png'% image_number
        image_number +=1
        print file
        plt.savefig(file)

    if pref.save == False:
        plt.show()

def leg_plotter(arm, joint_angs=np.array([0.0,0.0,0.0,0.0]), title="Leg"):

    arm.setJointAngs(joint_angs)
    arm.calcFK()

    fig = plt.figure()
    plt.clf()
    plt.suptitle(title, fontsize=20)
    ax = fig.gca(projection='3d')
    arm_plotter = ap.LegPlotter(arm)
    arm_plotter.plot(ax)
    ap.set_axes_equal(ax)
    ax.zaxis.set_visible(False)
    ax.w_xaxis.line.set_lw(0.)
    ax.set_xticks([])
    ax.view_init(elev = 20,azim=20)
    plt.draw()
    ax.set_xlabel('X Axis')
    ax.set_ylabel('Y Axis')
    ax.set_zlabel('Z Axis')

    #Save File
    if pref.save == True:
        # global vis.filepath
        file = vis.filepath+'leg.png'
        print file
        plt.savefig(file)

    if pref.save == False:
        plt.show()

if __name__ == '__main__':
    print "I'm a cheez-it!"
    # lengths = [.0,.270,.280,.390,.19,.01]
    # lengths = [.5,.5,.5,.5]
    lengths = [ 0.,    0.0,  0.09,  0.0]
    jt_h = .2286
    angles = np.array([0.0, 0.0, 0.0, 0.0])
    # angles = [0.56, 1.01, -1.2, 0.52]
    base_hom = kt.ax_vec2homog(np.array([51.2*np.pi/180.0,0.0,0.0]),np.array([0,0,0]))
    directory = "../optimal_designs/2017-04-19/"
    arm_num = "6"
    # arm = at.load_leg_pickle("best_arm_lengths_0", "best_arm_hom_0", "best_arm_bend_angles_0")
    arm = at.load_leg_pickle(directory + "best_arm_lengths_" + arm_num, directory +"best_arm_hom_" + arm_num, directory +"best_arm_bend_angles_" + arm_num)
    # arm = at.leg(lengths,angles,base_hom,jt_h)
    arm.setJointAngs(angles)
    # arm.setJointVelsZero()
    arm.calcFK()
    print arm.g_world_tool()

    print "lengths: ", arm.lengths
    print "bend angles: ", arm.bend_angles
    r, t = kt.hom2ax_vec(arm.base_hom)
    print "base angle: ", r[0]*180.0/np.pi
    # plot_ws(arm)
    # new_base_hom = kt.ax_vec2homog(np.array([-r[0],0.0,0.0]),np.array([0,0,0]))
    # arm.base_hom = new_base_hom
    # r, t = kt.hom2ax_vec(arm.base_hom)
    # print "base angle: ", r[0]*180.0/np.pi
    # arm = at.leg([ 0.5, 0.5, 0.5, 0.5],[0.053,  0.367,  0.015,  0.157,], base_hom, .18)
    # arm = at.leg([ 0.0, 0.5, 0.5, 0.5],[0.113,  -0.035,  0.01,  0.005,], base_hom, .18)
    # arm = at.leg([ 0.5, 0.5, 0.5, 0.5],[1.571,  -1.57,  0.12,  0.118], base_hom, .18)
    # arm = at.leg([ 0.5, 0.5, 0.5, 0.5],angles, base_hom, .18)
    # arm = at.leg([ 0.0, 0.331, 1.499, 1.5],[-0.06,-1.569, 0.024, -0.61], base_hom, .18)
    # leg_plotter(arm)

    #[-0.     0.5    0.5    0.5    0.113 -0.035  0.01   0.005  0.   ]
    # print "cow: ",  arm.lengths
    # arm = at.nasa_arm(jt_height, lengths, base_hom)
    # arm.calcFK()
    # print arm.g_world_tool()
    # print "bend_angles: ", arm.bend_angles
    # print "lengths: ", arm.lengths
    # print "hom: ", arm.base_hom
    evaluator = vis.EvaluateArm(arm,30,5, 4, 46,.90)
    # evaluator = vis.EvaluateArm(arm,30,5, 0, 30,.90)

    # vis.ws_visualization(evaluator)
    # plot_z_planes(evaluator)
    # plot_ws(arm)
    leg_plotter(arm, angles, "Balanced")
    # plot_ws(arm)
    # print arm.g_world_arm()
    # print arm.g_world_tool()

    # for link in arm.links_:
    #     a = link.g_world_bot_[:3, 3].T
    #     b = link.g_world_top_[:3, 3].T
    #     c = link.g_world_end_[:3, 3].T
    #     print "a: ", a
    #     print "b: ", b
    #     print "c: ", c
    # arm.setJointAngsZero()
    # arm.calcFK()

    # fig = plt.figure()
    # plt.clf()
    # plt.suptitle("Dexterous Robot", fontsize=20)
    # ax = fig.gca(projection='3d')
    # arm_plotter = ap.LegPlotter(arm)
    # arm_plotter.plot(ax)
    # ap.set_axes_equal(ax)
    # ax.zaxis.set_visible(False)
    # ax.w_xaxis.line.set_lw(0.)
    # ax.set_xticks([])
    # ax.view_init(elev = 20,azim=20)
    # plt.draw()
    # plt.show()
    # plt.savefig("cow.png")
