#!/usr/bin/env python
from ego_byu.tools import arm_tools as at
import numpy as np
import leg_vis as lv
from ego_byu.tools import pickle_tools as pk
from ego_byu.tools import kin_tools as kt
from ego_core.kinematics import kinematics as kin
import os
from ego_byu.armeval import armeval as ae


def screw(d):
    return np.array([[0, -d[2], d[1]], [d[2], 0, -d[0]], [-d[1], d[0], 0]])

def adj_from_trans(d):
    return np.array([[1, 0, 0, 0, d[2], -d[1]],[0, 1, 0, -d[2], 0, d[0]],[0, 0, 1, d[1], -d[0], 0],[0, 0, 0, 1, 0, 0],[0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 1]])

def jac_t_from_hom(hom,arm,i):
        t = []
        t.append(hom[0][3])
        t.append(hom[1][3])
        t.append(hom[2][3])

        adj = adj_from_trans(t)

        jac_spat = arm.getJacSpatial()
        jac = np.dot(adj,jac_spat)

        for j in range(0,i+1):
            dof = arm.links_[j].joint_.num_dof_
            dof_prox = arm.links_[j].num_dof_prox_
            for l in range(dof):
                for k in range(len(jac)):
                    jac[k][dof_prox+l] = 0.0

        jac_t = np.transpose(jac)


        return jac_t

def calc_support_torques(arm):
    torques = np.zeros((arm.num_dof_,1))
    arm.calcJac()
    # jac_spat = arm.getJacSpatial()

    arm.setTorqueExternalZero()

    #NEED TO ADD MASS AT JOINTS
    for i in range(len(arm.links_)-2,-1,-1):
        g_world_com = arm.links_[i].g_world_com_
        g_world_com_joint = np.dot(arm.links_[i].g_world_top_,arm.links_[i].joint_.g_top_com_)
        print "g_world_com_joint: ", g_world_com_joint
        print "g_top_com_Joint: ", arm.links_[i].joint_.g_top_com_
        print "g_world_top_: ", arm.links_[i].g_world_top_
        # t = []
        # t.append(g_world_com[0][3])
        # t.append(g_world_com[1][3])
        # t.append(g_world_com[2][3])

        # adj = adj_from_trans(t)

        # jac = np.dot(adj,jac_spat)

        # for j in range(0,i+1):
        #     dof = arm.links_[j].joint_.num_dof_
        #     dof_prox = arm.links_[j].num_dof_prox_
        #     for l in range(dof):
        #         for k in range(len(jac)):
        #             jac[k][dof_prox+l] = 0.0

        # jac_t = np.transpose(jac)

        wrench = np.array([[0],[0],[-arm.getGravity()*arm.links_[i].inertia_self_.mass],[0],[0],[0]])
        wrench_joint = np.array([[0],[0],[-arm.getGravity()*arm.links_[i].joint_.mass_],[0],[0],[0]])

        jac_t = jac_t_from_hom(g_world_com,arm,i)
        jac_t_joint = jac_t_from_hom(g_world_com_joint,arm,i)

        #seeing if their old code already works for this
        arm.addTorqueExternal(i, 0, wrench, g_world_com)

        torques = torques + np.dot(jac_t,wrench) + np.dot(jac_t_joint,wrench_joint)
        print "mine: ", torques
        print "before: ", arm.getTorqueExternal()

    return torques

if __name__ == '__main__':

    #create the leg (just a rotary one)
    # base_hom = np.array([[0, 0, 1.0, 0], [-1, 0, 0, 0], [0,-1, 0, 0], [0, 0, 0, 1]])
    base_hom = kt.ax_vec2homog(np.array([0.0, 0.0, 0.0]), np.array([0.0,0.0,0.0]))
    # arm = at.simple_leg(0.0, [1.0,1.0,1.0], base_hom)
    arm = at.leg([0.5,0.5,0.5,0.5],[0.0,0.0,0.0,0.0],base_hom, .2286, 2.716855)

    #set the joint angles and calculate the FK
    # jangles = np.array([0.0,-np.pi/4.0,-np.pi/4.0])
    jangles = np.array([0.0, 0.0, 0.0, 0.0])
    arm.setJointAngs(jangles)
    arm.calcFK()
    print arm.getGWorldTool()

    #wrenches in the base frame that will be applied on the leg
    forces = np.array([[0],[0],[-1.0],[0],[0],[0]])
    w1 = np.array([[0],[0],[-0.5],[0],[0],[0]])

    #calculate the jacobians
    arm.calcJac()
    jac_spat = arm.getJacSpatial()
    jac_spat_t = np.transpose(jac_spat)

    #find the torque just from the weight of the body on the end
    torques = np.dot(jac_spat_t,forces)

    print "Torques: ", torques
    arm.setTorqueExternal(1, 0, forces, np.array([[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 1.0]]))
    print "Calculating: ", arm.getTorqueExternal()
    #screw symmetric matrix of the vector putting the w1 force at the base
    print screw([0, -.5, 0])

    #put the w1 force at the base by adjusting the Jacobian
    adj = np.array([[1, 0, 0, 0, 0, .5],[0, 1, 0, 0, 0, 0],[0, 0, 1, -.5, 0, 0],[0, 0, 0, 1, 0, 0],[0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 1]])
    jac_w1 = np.dot(adj, jac_spat)

    #the first joint will not be able to help with the loading so make that column all zeros in the jacobian
    for i in range(len(jac_w1)):
        jac_w1[i][0] = 0.0

    #find the torques due to w1 and then add it to the torques from the body weight to find the total torque
    jac_w1_t = np.transpose(jac_w1)
    torques_weight = np.dot(jac_w1_t, w1)
    print "Torque Weight: ", torques_weight
    print "Total Torque: ", torques_weight + torques


    print arm.links_[0].g_world_com_

    fun_torques = calc_support_torques(arm)
    print "python fun_torques: ", fun_torques
    even_funner_torques = arm.calcTorqueGravityLeg()
    print "even_funner_torques", arm.getTorqueGravityLeg()

    Evaluator = ae.ArmEvalFK(arm)
    Evaluator.Initialize()
    Evaluator.arm_.setJointAngs(jangles)
    Evaluator.arm_.calcFK()
    print "In Desired XY Region? ", Evaluator.EEInDesiredXYRegion()
    if Evaluator.CanSupportWeight():
        Evaluator.CalcBodyPayloadScore()
        # print Evaluator.scores_.payload_score_
    print Evaluator.CanSupportWeight()

    print "Not Slipping" , Evaluator.NotSlipping(even_funner_torques)
    Evaluator.TempDumbFunc()

    # leg_inert = arm.links_[0].inertia_self_
    # print "mass: ", leg_inert.mass
    # print "vec: ", leg_inert.vec
    lv.leg_plotter(arm, jangles)
