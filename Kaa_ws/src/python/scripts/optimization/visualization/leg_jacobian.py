#!/usr/bin/env python
from ego_byu.tools import arm_tools as at
import numpy as np
import leg_vis as lv
from ego_byu.tools import pickle_tools as pk
from ego_core.kinematics import kinematics as kin
import os
from ego_byu.armeval import armeval as ae

def get_torque_limits(q):

    max_press = 586054 #Pa

    t_to_p = 7400.0 #Pa/Nm
    gamma = 1/t_to_p

    # print "I am gamma!", gamma

    k = 27.0 #Nm/rad

    tau_max = []
    tau_min = []

    for i in range(0,len(q)):
        tau_max.append(gamma*(max_press) - k*q[i])
        tau_min.append(-1.0*gamma*(max_press) - k*q[i])

    return tau_max, tau_min

def arm_can_support_weight(torques, tau_max, tau_min):
    for i in range(len(torques)):
        if torques[i] > tau_max[i] or torques[i] < tau_min[i]:
            print "InValid Pose"
            return 0
    return 1

if __name__ == '__main__':
    base_hom = np.array([[0, 0, 1.0, 0], [1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 1]])
    # print base_hom
    arm = at.leg([0.5,0.5,0.5,0.5],[0.0,0.0,0.0,0.0],np.identity(4), .28)
    arm.links_[0].setInertiaSelf(1,[0.0,0.0,0.0],[0.0,0.0,0.0])
    arm.links_[0].InitInertia
    arm.links_[1].setInertiaSelf(1,[0.0,0.0,0.0],[0.0,0.0,0.0])
    arm.links_[1].InitInertia
    # arm.setJointAngsZero();
    jangles =np.array([np.pi/2.0,0.0,0.0,0.0])
    arm.setJointAngs(jangles)
    arm.calcFK()
    # print arm.g_world_arm()
    # print arm.g_world_tool()
    arm.calcJac()
    # print arm.getJacBody()
    # print arm.getJacHybrid()
    # print arm.getJacHybridInverse()
    # print arm.getJacSpatial()
    jac = arm.getJacHybridInverse()
    jac_spat = arm.getJacSpatial()

    print "jac: ", jac
    print "jac_spat: ", jac_spat

    adj = np.array([[1, 0, 0, 0, 0, -1],[0, 1, 0, 0, 0, 0],[0, 0, 1, 1, 0, 0],[0, 0, 0, 1, 0, 0],[0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 1]])

    print "adjoint", adj

    print "jac_shift: ", np.dot(adj,jac_spat)
    jac_shift = np.dot(adj,jac_spat)
    jac_shift_t = np.transpose(jac_shift)

    jac = np.array(jac_spat)
    jac_t = np.transpose(jac)
    # print jac_t

    m = 10;
    g = 9.81;
    # torque_upper = 15
    # torque_lower = -15

    print "I'm a cow", arm.getTorqueLimitUpper()


    forces = np.array([[0],[0],[-m*g/2.0],[0],[0],[0]])
    forces = np.array([[0],[0],[-1.0],[0],[0],[0]])
    print "Forces:  ", forces

    torques = np.dot(jac_t, forces)

    arm.calcTorqueGravity()
    torques_gravity = arm.getTorqueGravity()
    print "Torques:   ", torques
    print "Torques Shift:    ", np.dot(jac_shift_t,forces)
    print "gravity torque",  torques_gravity

    torques = torques + torques_gravity

    print "Torques:   ", torques

    tau_max, tau_min = get_torque_limits(jangles)


    if(arm_can_support_weight(torques, tau_max, tau_min)):


        diff = []

        for i in range(len(torques)):
            # if torques[i] > tau_max[i] or torques[i] < tau_min[i]:
            #     print "InValid Pose"

            if torques[i] < 0:
                diff.append(abs(tau_min[i] - torques[i]))

            elif torques[i] >= 0:
                diff.append(abs(tau_max[i] - torques[i]))

        print "diff", diff

        limiter = diff.index(min(diff))
        print min(diff)

        if torques[limiter] < 0:
            new_forces = tau_min[limiter]/torques[limiter]*forces
        elif torques[limiter] >= 0:
            new_forces = tau_max[limiter]/torques[limiter]*forces

        score = min(abs(new_forces[2] - forces[2]), 10*m*g) #do min of the 2 values to try to curbe singularities

        # new_torques = torque_upper/torques[2]*torques
        # new_forces = torque_upper/torques[2]*forces


        # print "New Torques: ", new_torques
        print "New Forces: ", new_forces
        new_torques = np.dot(jac_t, new_forces)
        print "New Torques: ", new_torques

        print "score:   ", score

    # print np.dot(jac,new_torques)
    # print np.dot(jac,torques)


    # tau_max, tau_min = get_torque_limits([-np.pi/4,np.pi/4,np.pi/2.0,np.pi/100.0])
    # tau_max, tau_min = get_torque_limits([0.0,0.0,0.0,0.0])

    print "tau_max: ", tau_max
    print "tau_min: ", tau_min

    arm.calcTorqueLimits()

    for i in range(0, arm.num_dof_):
        print "c++ tau_max: ", arm.max_torques_[i]
        print "c++ tau_min: ", arm.min_torques_[i]

    bob = arm.links_[0].joint()
    bob.calcTorqueLimits()
    print bob.torque_max_
    print bob.torque_min_
    arm.links_[0].SetInertiaDiag(1,1,1,1)
    arm.links_[1].SetInertiaDiag(0,0,0,0)
    larry = arm.links_[0].inertia_mat_full()
    # print larry
    arm.links_[0].inertia_self_.vec = np.array([0.5,0.5,0.5])
    arm.links_[0].InitInertia()
    print arm.links_[0].inertia_self_.vec
    print arm.links_[0].g_top_com_
    print "gravity: ", arm.getGravity()
    arm.setGravity(-9.81)
    print arm.getJacHybrid()
    # print jac_t
    bob = arm.calcTorqueGravity()
    print "gravity torque",  arm.getTorqueGravity()

    print arm.links_[0].jac_hybrid_
    print arm.links_[1].jac_hybrid_

    lv.leg_plotter(arm,jangles)
    print arm.calcTorqueJointStatic(forces)
