#!/usr/bin/env python
import numpy as np

save = False
XYZRes = 30
SO3Res = 3
ws_dimension = 3.0
# ws_center = np.array([0,ws_dimension/2.0,ws_dimension/2.0]) #Must be float
ws_center = np.array([0,0,0])

#Mode: Enum{0 = PERCENT REDUNDANT, 1 = USER INPUT, 2 = SYSTEMATIC, 3 = FIXED_TIME,4 = HYBRID}
dth_num_ = 10 #Systematic
mode = 5
FKCutoff = .9
