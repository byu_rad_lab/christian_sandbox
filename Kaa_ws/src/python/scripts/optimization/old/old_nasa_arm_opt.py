#!/usr/bin/env python
from __future__ import division
import numpy as np
import copy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math

from ego_byu.tools import arm_tools as at
from ego_byu.tools import pickle_tools as pk
from ego_byu.tools import kin_tools as kt

#This version of the evolutionary algorithm attempts to incorporate more variables:
# - num_lengths #
# - Mounting position
# - Arm Length

class Evolution_NASA_Arm(object):
    def __init__(self,num_design,max_generations):
        if np.mod(num_design,2) != 0:
            print "####ERROR: number of designs must be even,and a multiple of processors/2"
            assert(False)

        #Constraints on Arm
        self.arm_length_constraints = [.8,1.5]
        self.num_lengths_constraints = [6,7] #rounds down, max is never reached
        self.link_constraints = [0,.5]
        self.base_angle_constraint = [-np.pi/4.0-np.pi/2.0, np.pi/4.0-np.pi/2.0]
        self.base_z_constraint = [0,1]
        self.valve_constraint = .18 #There cannot be two consecutive links under this limit for valve

        #Arm Parametersj
        self.jt_height = .06

        #Generation Parameters
        self.curr_generation = 0
        self.num_design = num_design
        self.num_pairs = int(num_design/2)
        self.max_generations = max_generations

        #Gene Pools and Score/Design Tracking Variables
        self.best_designs = []
        self.gene_pool_parent = []
        self.gene_pool_children = []
        self.max_scores = []
        self.mean_scores = []
        self.std_scores = []
        self.link_variances = [[] for i in range(self.num_lengths_constraints[0])]
        self.base_hom_variance_Z = []

        #Mutation and Crossover Parameters
        self.mutation_rate = .3
        self.pet = self.link_constraints[1]/5.0
        self.plot_on = True

    def run(self):
        #Set Up Visualization
        if self.plot_on == True:
            plt.ion()
            self.fig_lengths, self.ax_lengths = plt.subplots(2,figsize=(10,18))
            self.fig_base, self.ax_base = plt.subplots(2,figsize=(10,18),sharex=True)
            self.fig_score, self.ax_score = plt.subplots()
            self.fig_pareto, self.ax_pareto = plt.subplots()

        #Initialize Evolution
        self.initialize_first_generation()

        #Start Evolution, Kill on Control-C
        try:
            while self.curr_generation < self.max_generations:
                self.evolve()
        except KeyboardInterrupt:
            raw_input("Press Return")
            pass

        #Save Best Design
        if len(self.gene_pool_parent[0].scores) == 1:
            at.pickle_arm(self.best_designs[-1])

            #Report Best Design
            print "Best Robot Arm Found:"
            print "Lengths:", self.best_designs[-1].lengths
            r,t = kt.hom2ax_vec(self.best_designs[-1].base_hom)
            print "Ax Vec: ", r*180/np.pi
            print "Vertical Mounting Position: ", t

        #Plot a simple FKS Scheme
        if self.plot_on == True:
        #     at.plot_fks(self.gene_pool_children[index_best])#,self.ax_best,self.fig_best)
            plt.ioff()
            plt.show()

    def initialize_first_generation(self):
        print "Generating Initial Set of Robots"
        for i in range(self.num_design):
            arm = self.generate_design()
            self.gene_pool_children.append(arm)
        self.curr_generation+=1

    def evolve(self):
        print
        print "--------------------GENERATION ", self.curr_generation,"-------------------------"
        print

        #Set up/update generation variables
        self.gene_pool_parent = self.gene_pool_children
        self.gene_pool_children = []
        self.children_scores = []

        children = []

        scores = np.array([]) # Only Children Scores (Not Perturbed Version Scores)
        gen_lengths = [[] for i in range(self.num_lengths_constraints[0])]
        gen_base_hom = []

        #Crossover, Generate Children
        indices = np.arange(self.num_design)
        np.random.shuffle(indices)
        for i in range(int(len(indices)/2)):
            children.append(self.perform_crossover(self.gene_pool_parent[indices[2*i]],self.gene_pool_parent[indices[2*i+1]]))

        #Perform Evaluations of Children and Parents (Internal score member assigned)
        print "Evaluating Parents"
        at.eval_arm_set(self.gene_pool_parent, self.curr_generation,.95,'single_objective')
        print "Evaluating Children"
        at.eval_arm_set(children, self.curr_generation,.95,'single_objective')

        # For Multiple Objectives: Maximin
        if len(self.gene_pool_parent[0].scores) == 1:
        #For Single Objective: Tournamet
            for i in range(len(children)):
                arm, score = at.tournament(self.gene_pool_parent[indices[2*i]],self.gene_pool_parent[indices[2*i+1]],children[i])
                scores = np.append(scores,score)
                self.gene_pool_children.append(arm)
                self.children_scores.append(score)

        # Maximin Optimization
        else:
            arms = np.append(self.gene_pool_parent,children)
            self.gene_pool_children = at.maximin(arms)

        for i in range(len(self.gene_pool_children)):
            #Create Slightly Modified arm of Winner: Allow for local refinement
            arm_perturbed = self.perturbate(self.gene_pool_children[i])
            self.gene_pool_children.append(arm_perturbed)
            self.children_scores.append(0) #scoring is 0 for perturbed

        #For Single Objective: Tournamet
        if len(self.gene_pool_children[0].scores) == 1:
            #     #Collecting link lengths and base hom in structure for variance computation
            for i in range(len(self.gene_pool_children)):
                for j in range(self.num_lengths_constraints[0]):
                    gen_lengths[j].append(self.gene_pool_children[i].lengths[j])
                gen_base_hom.append(self.gene_pool_children[i].base_hom[2,3])

            #Record statistics on scores of gene pool
            self.mean_scores = np.append(self.mean_scores,np.mean(scores))
            self.std_scores = np.append(self.std_scores,np.std(scores))

            #Record Link Variances in generation
            for i in range(self.num_lengths_constraints[0]):
                self.link_variances[i].append(np.var(gen_lengths[i]))
            self.base_hom_variance_Z.append(np.var(gen_base_hom))

            #Record Best Design and score lengths
            index_best = self.children_scores.index(max(self.children_scores))
            self.max_scores.append(max(self.children_scores))
            self.best_designs.append(self.gene_pool_children[index_best])
            print "BEST DESIGN: ", self.best_designs[-1].lengths
            print "SCORE: ", self.max_scores[-1]

        #Plot Progress
        if self.plot_on == True:
            if len(self.gene_pool_children[0].scores) ==1:
                self.plot_lengths_evol()
                self.plot_base_evol()
                self.plot_score_evol()
                # at.plot_arm(self.gene_pool_children[index_best],self.ax4,self.fig4)

            else:
                self.plot_pareto(self.gene_pool_children[1:int(self.num_design/2.0)])

        self.curr_generation+=1

    #Purpose: Find a child from 2 arms
    def perform_crossover(self,arm1,arm2):
        lengths = self.crossover_lengths(arm1.lengths,arm2.lengths)

        count = 0
        while (self.total_length_violated(lengths) or self.valve_constraint_violated(lengths)):
            count = count+1
            lengths = self.crossover_lengths(arm1.lengths,arm2.lengths)
            if np.mod(count,100)==0:
                print "+100 iters to find length in crossover"

        #Crossover base mounts
        base_hom = self.crossover_base(arm1.base_hom,arm2.base_hom)

        #Allow Possibility of Mutation: Gives new lengths order, and new base mount
        if np.random.rand() < self.mutation_rate:
            lengths = self.mutate_lengths(lengths)

            base_hom = self.choose_base_hom()

        arm_child = at.nasa_arm(self.jt_height,lengths,base_hom)
        self.check_constraints(arm_child)
        return arm_child

    #Mix lengths of parents
    def crossover_lengths(self,lengths1,lengths2):
        lengths = []
        min_num_lengths = min(len(lengths1),len(lengths2))
        diff_num_lengths = abs(len(lengths1)-len(lengths2))
        for i in range(min_num_lengths):
            dice = np.random.rand()
            if (dice < 1.0/3.0):
                lengths.append(lengths1[i])
            elif (dice >2.0/3.0):
                lengths.append(lengths2[i])
            else:
                lengths.append(np.mean([lengths1[i],lengths2[i]]))

        #Add random number of extra links to child
        for i in range(min_num_lengths,min_num_lengths+diff_num_lengths):
            dice = np.random.rand()
            if dice < 1.0/2.0:
                if len(lengths1) > len(lengths2):
                    lengths.append(lengths1[i])
                else:
                    lengths.append(lengths2[i])

        return lengths

    #Randomly select either of the basemounts or avg.
    def crossover_base(self,hom1,hom2):
        dice = np.random.rand()
        if (dice < 1.0/3.0):
            return hom1
        elif (dice >2.0/3.0):
            return hom2
        else:
            return kt.hom_avg(hom1,hom2)

    #Switches two random links
    def mutate_lengths(self,lengths):
        new_lengths = copy.copy(lengths)
        ind1 = math.trunc(np.random.rand()*len(lengths))
        ind2 = math.trunc(np.random.rand()*len(lengths))
        while ind2 == ind1:
            ind2 = math.trunc(np.random.rand()*len(lengths))
        new_lengths[ind2] = lengths[ind1]
        new_lengths[ind1] = lengths[ind2]

        while self.valve_constraint_violated(new_lengths):
            new_lengths = copy.copy(lengths)
            ind1 = math.trunc(np.random.rand()*len(lengths))
            ind2 = math.trunc(np.random.rand()*len(lengths))
            while ind2 == ind1:
                ind2 = math.trunc(np.random.rand()*len(lengths))
            new_lengths[ind2] = lengths[ind1]
            new_lengths[ind1] = lengths[ind2]
        return new_lengths

    #Generate Random Design
    def generate_design(self):
        #Generate (num_lengths) links within link constraints, check sum within arm constraints, try again if failed
        num_lengths,lengths = self.choose_links()
        count =0
        while (self.total_length_violated(lengths) or self.valve_constraint_violated(lengths)):
            count+=1
            num_lengths,lengths = self.choose_links() #Repick
            if (np.mod(count,100)==0):
                print "100+ iterations in generating designs"

        #Base Mount generation
        base_hom = self.choose_base_hom()

        #Construct and return arm
        arm = at.nasa_arm(self.jt_height,lengths,base_hom)
        self.check_constraints(arm)
        return arm

    #Stacks up a random number (within constraints) of links (link lengths in constraint)
    def choose_links(self):
        num_lengths = int(np.random.uniform(self.num_lengths_constraints[0],self.num_lengths_constraints[1]))
        lengths = []
        for i in range(num_lengths):
            lengths.append(np.random.uniform(self.link_constraints[0],self.link_constraints[1]))
        return num_lengths, lengths

    #Picks random z and angle (Neutral angle with arm pointed horizontally from KREX. Arm allowed to be mounted tilted upwards or downwards from horizontal)
    def choose_base_hom(self):
        base_angle_x = np.random.uniform(self.base_angle_constraint[0],self.base_angle_constraint[1]);
        base_z = np.random.uniform(self.base_z_constraint[0],self.base_z_constraint[1])
        return kt.ax_vec2homog(np.array([base_angle_x,0,0]),np.array([0,0,base_z]))

    #Generate slightly modified version of arm, while keeping within constraints
    def perturbate(self,arm):
        #Perturb Lengths
        jt_positions, lengths = self.find_perturbed_config(arm)

        count = 0
        while (self.link_length_violated(lengths) or self.valve_constraint_violated(lengths)):
            jt_positions, lengths = self.find_perturbed_config(arm)
            count = count+1
            if np.mod(count,100)==0:
                print "link length constraint in perturbation taking a lot of time"

        #perturb base
        r , t = kt.hom2ax_vec(arm.base_hom)
        r_p = copy.copy(r)
        t_p = copy.copy(t)

        r_p[0] = r_p[0] + np.random.uniform(-np.pi/20,np.pi/20)
        count =0
        while (r_p[0]>self.base_angle_constraint[1] or r_p[0]<self.base_angle_constraint[0]):
            r_p[0] = r[0] + np.random.uniform(-np.pi/20,np.pi/20)
            count = count+1
            if np.mod(count,10)==0:
                print "rotation", r
                print "constraint", self.base_angle_constraint
                print "base angle taking time"

        t_p[2] = t[2] + np.random.uniform(-.1,.1)
        count = 0
        while (t_p[2]>self.base_z_constraint[1] or t_p[2]<self.base_z_constraint[0]):
            t_p[2] = t[2] + np.random.uniform(-.1,.1)
            count = count+1
            if np.mod(count,10)==0:
                print "base translation taking time"

        base_hom = kt.ax_vec2homog(r_p,t_p)
        arm_p = at.nasa_arm(self.jt_height,lengths, base_hom)
        self.check_constraints(arm_p)
        return arm_p

    #Moves joint positions slightly by a uniform variable in +/- self.pet
    def find_perturbed_config(self,arm):
        lengths = copy.copy(arm.lengths)
        jt_positions = copy.copy(arm.jt_positions)
        pet = self.pet*(self.max_generations-self.curr_generation/2.0)/self.max_generations
        for i in range(len(jt_positions)-2):
            new_jt_pos = jt_positions[i+1]+np.random.uniform(-pet,pet)
            if new_jt_pos > 0 and new_jt_pos < self.arm_length_constraints[1]-self.jt_height*arm.num_lengths:
                jt_positions[i+1] = new_jt_pos
        jt_positions = sorted(jt_positions)
        lengths = at.get_link_lengths(jt_positions)
        return jt_positions, lengths

    #Plotting Functions
    #Plots lengths and length variances
    def plot_lengths_evol(self):
        plt.figure(1)
        self.fig_lengths.canvas.set_window_title('Link Optimization')
        width = 1.0/(2*self.max_generations)
        col= 1.0/self.max_generations

        #Lengths
        i = len(self.best_designs)-1
        ind = np.arange(len(self.best_designs[i].lengths))
        links = self.ax_lengths[0].bar(ind+width*i, self.best_designs[i].lengths, width, color=np.array([1-i*col,i*col,0]),label="Gen. %d" % (i+1))
        self.ax_lengths[0].set_title("Evolution of Link Length", fontsize=20)
        self.ax_lengths[0].set_ylabel("Link Length (m)",fontsize=15)
        self.ax_lengths[0].set_xlabel("Link # (Start Link 0)",fontsize=15)
        self.ax_lengths[0].grid(True)

        #Variances
        col = 1.0/self.num_lengths_constraints[0]
        for i in range(len(self.link_variances)):
            self.ax_lengths[1].scatter(self.curr_generation,self.link_variances[i][-1], s=60,color=np.array([1-i*col,i*col,0]),label="Link %d" % (i+1))
        if self.curr_generation == 1:
            self.ax_lengths[1].legend()

        self.ax_lengths[1].set_title("Pool Variance: Link Lengths", fontsize=20)
        self.ax_lengths[1].set_ylabel("Variance",fontsize=15)
        self.ax_lengths[1].set_xlabel("Generation",fontsize=15)
        self.ax_lengths[1].grid(True)
        plt.draw()

    #Visualization of Base Mount
    def plot_base_evol(self):
        plt.figure(2)
        self.fig_base.canvas.set_window_title('Base Optimization')
        self.ax_base[0].scatter(self.curr_generation,self.best_designs[-1].base_hom[2,3],s=60,color='blue',label="Best Design Found")
        self.ax_base[0].set_title("Evolution of Base Mount Vertical", fontsize=20)
        self.ax_base[0].set_ylabel("Vertical (m) from ground",fontsize=15)
        self.ax_base[0].set_xlabel("Generation",fontsize=15)
        self.ax_base[0].grid(True)

        #Variances
        self.ax_base[1].scatter(self.curr_generation,self.base_hom_variance_Z[-1],s=60,color='red',label="Variance in Z (m)")
        self.ax_base[1].set_title("Pool Variance: Base Mount Vertical", fontsize=20)
        self.ax_base[1].set_ylabel("Variance",fontsize=15)
        self.ax_base[1].set_xlabel("Generation",fontsize=15)
        self.ax_base[1].grid(True)
        plt.draw()

    #Plot mean and max score of each generation
    def plot_score_evol(self):
        plt.figure(3)
        self.fig_score.canvas.set_window_title('Generation Mean Score')
        self.ax_score.scatter(self.curr_generation,self.mean_scores[-1], s=60,color='blue',label='mean')
        self.ax_score.scatter(self.curr_generation,self.max_scores[-1], s=60,color='green',label='max.')
        if self.curr_generation ==1:
            self.ax_score.legend(loc=2)

        plt.title("Progression Statistics", fontsize=30)
        plt.ylabel("Stat of Scores",fontsize=20)
        plt.xlabel("Generation",fontsize=20)
        plt.grid(True)
        plt.draw()

    def plot_pareto(self,arms):
        x = []
        y = []
        for i in range(len(arms)):
            x.append(arms[i].scores[0])
            y.append(arms[i].scores[1])

        plt.figure(4)
        col = self.curr_generation/self.max_generations
        self.fig_pareto.canvas.set_window_title('Pareto Front')
        self.ax_pareto.scatter(x,y, s=60,color=np.array([1-col,col,0]),label='max.')

        plt.title("Pareto Front", fontsize=30)
        plt.ylabel("Lift Force at Ground Metric",fontsize=20)
        plt.xlabel("Manipulability Metric",fontsize=20)
        plt.grid(True)
        plt.draw()

    #Constraints
    def total_length_violated(self,lengths):
        num_lengths = len(lengths)
        if (sum(lengths) > self.arm_length_constraints[1]-self.jt_height*num_lengths or sum(lengths) < self.arm_length_constraints[0]-self.jt_height*num_lengths):
            return True
        return False

    def link_length_violated(self,lengths):
        if max(lengths) > self.link_constraints[1] or min(lengths) < self.link_constraints[0]:
            return True
        return False

    def valve_constraint_violated(self,lengths):
        #three consecutive links can't be under tol
        for i in range(len(lengths)-1):
            if (lengths[i] < self.valve_constraint and lengths[i+1] < self.valve_constraint):
                return True
        return False

    #Overall check to ensure constraints aren't being violated in evolutionary algorithm
    def check_constraints(self,arm):
        if self.link_length_violated(arm.lengths):
            print "Link Length Constraint Violated"
            print "Links child:", arm.lengths
            raw_input("Error")

        if (self.total_length_violated(arm.lengths)):
            print "Arm Length Violated"
            print "Arm Length:", sum(arm.lengths)+self.jt_height*arm.num_lengths
            print "Lengths:", arm.lengths
            raw_input("Error")

        if (self.valve_constraint_violated(arm.lengths)):
            print "Three Consecutive Links Under Valve Constraint"
            print "Links: ", arm.lengths
            raw_input("Error")

        if (arm.num_lengths >= self.num_lengths_constraints[1] or arm.num_lengths <self.num_lengths_constraints[0]):
            print "num_lengths Constraint Violated"
            print "num_lengths of arm:", arm.num_lengths
            raw_input("Error")

        r,t = kt.hom2ax_vec(arm.base_hom)
        if (t[2] < self.base_z_constraint[0] or t[2] > self.base_z_constraint[1]):
            print "Base Vertical Position Constraint Violated"
            print "Base Position Vector", t
            raw_input("Error")

        if (r[0] < self.base_angle_constraint[0] or r[0]> self.base_angle_constraint[1]):
            print "Base Angle Constraint Violated"
            print "Base Angle:", r
            raw_input("Error")

if __name__ == '__main__':
    designs = 20
    generations = 10
    evolution_solver = Evolution_NASA_Arm(designs,generations)
    evolution_solver.run()


