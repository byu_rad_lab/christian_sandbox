#!/usr/bin/env python
from __future__ import division
import copy
import time,datetime,os
import math
from ego_byu.tools import arm_tools as at
from ego_byu.tools import pickle_tools as pk
from ego_byu.tools import kin_tools as kt
from ego_core.kinematics import kinematics as kin
from ego_byu.armeval import armeval as ae
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from multiprocessing import Process, Queue

jt_height = .06
lengths = np.ones(5)*.2
base_hom = np.eye(4)

arm = at.nasa_arm(jt_height,lengths,base_hom)

Eval = ae.ArmEvalFK(arm)
Eval.print_on = False
Eval.setXYZRes(10)
Eval.setSO3Res(3)
Eval.setBounds(2)
Eval.setCenterDiscretization(np.array([0,0,0]))
Eval.initialize()
Eval.run()
print 'FK iterations', Eval.FKnum_
print 'Orientations Found', Eval.scores_.orientations_
