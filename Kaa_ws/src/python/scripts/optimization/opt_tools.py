#!/usr/bin/env python
from __future__ import division
import copy
from ego_core.kinematics import kinematics as kin
from ego_byu.armeval import armeval as ae
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from multiprocessing import Process, Queue
import preferences as pref

#Passes arm to C++ for evaluation
def evaluate_design(arm, current_gen, cutoff,idx,q, type ,std_scoring=False):
    #Std_scoring evaluations are used when we want to compare an designs accross generations by a std value.
    #Std_scoring evaluations are different than regular evaluations in the way they track when an arm should be re-evaluated
    #Each arm is only evaluated every 5 generations (if it survives).  This speeds up computation time significantly
    if (current_gen!=0 and arm.next_eval_gen>current_gen and std_scoring==False):
        scores = copy.copy(arm.scores)
        q.put([scores,idx]) # return arm.score
    else:
        Eval = ae.ArmEvalFK(arm)
        Eval.print_on = False

        XYZRes = pref.XYZ_Resolution
        SO3Res = pref.SO3_Resolution

        Eval.setXYZRes(XYZRes) #Controls discretization resolution
        Eval.setSO3Res(SO3Res) #Controls orientational resolution
        Eval.setBounds(pref.Gridsize) #How large grid is in one dimension ex. 2 means 2x2x2
        Eval.setCenterDiscretization(pref.Gridcenter) #Center Point of Discretization Grid
        Eval.Initialize()
        # Eval.showDiscretization()
        # assert(False)
        if pref.robot_type == "leg":
            Eval.mode_ = 4 #systematic
            Eval.dth_num_ = 46
        else:
            Eval.mode_ = 0 #percent_redendant

        #Scoring
        Eval.redundancy_cutoff_ = cutoff
        Eval.Run()
        # max_force = unwrap_discretization(Eval,XYZRes)
        # print np.around(max_force[:,:,0],decimals=1)
        print ("Arm: {0:2f}, #FK Iter: {1:7f}, Poses Skipped: {2:3f}, Unsupportable Poses: {3:3f}, Time: {4:2f}").format(arm.index,Eval.FKnum_,Eval.poses_skipped_,Eval.unsupportable_poses_,Eval.total_time_)


        if (std_scoring==False):
            arm.next_eval_gen = copy.copy(current_gen)+10
        scores = unwrap_scores(Eval.scores_,type)
        q.put([scores,idx])

def unwrap_discretization(Eval,XYZRes):
    disc = np.zeros([XYZRes+1,XYZRes+1,XYZRes+1])
    for i in range(XYZRes+1):
        for j in range(XYZRes+1):
            for k in range(XYZRes+1):
                disc[i,j,k] = Eval.get_metric(i,j,k).max_force_
    return disc

def unwrap_scores(sc,type):
    if type == 'single_objective':
        scores = np.array([sc.orientations_]) # Add more values here
    elif type == 'multi_objective':
        if pref.robot_type == "leg":
            scores = np.array([sc.orientations_,sc.payload_score_]) #sc.payload_score_]) #max_force_]) # Add more values here add sc.payload_score_
        if pref.robot_type == "nasa_arm" or pref.robot_type == "jabberwock":
            scores = np.array([sc.orientations_,sc.max_force_]) # Add more values here add sc.payload_score_
    else:
        print "Optimization Setting Not Handled"
        # print "setting given: ", type
        assert(False)
    return scores

def eval_arm_set(arms,current_gen,cutoff,type='multi_objective'):
    N = len(arms)
    q = Queue()
    results = []
    pnum = 10
    if np.mod(len(arms),pnum)!=0:
        print "ERROR:Design Number Must be a multiple of 2*processor number"
        assert(False)

    for i in range(int(N/pnum)):
        #create 4 sub-processes to do the work
        arm1 = arms[0+pnum*i]
        arm2 = arms[1+pnum*i]
        arm3 = arms[2+pnum*i]
        arm4 = arms[3+pnum*i]
        arm5 = arms[4+pnum*i]
        arm6 = arms[5+pnum*i]
        arm7 = arms[6+pnum*i]
        arm8 = arms[7+pnum*i]
        arm9 = arms[8+pnum*i]
        arm10 = arms[9+pnum*i]

        p1 = Process(target=evaluate_design, args=(arm1,current_gen,cutoff,0+pnum*i,q,type,False))
        p1.start()
        p2 = Process(target=evaluate_design, args=(arm2,current_gen,cutoff,1+pnum*i,q,type,False))
        p2.start()
        p3 = Process(target=evaluate_design, args=(arm3,current_gen,cutoff,2+pnum*i,q,type,False))
        p3.start()
        p4 = Process(target=evaluate_design, args=(arm4,current_gen,cutoff,3+pnum*i,q,type,False))
        p4.start()
        p5 = Process(target=evaluate_design, args=(arm5,current_gen,cutoff,4+pnum*i,q,type,False))
        p5.start()
        p6 = Process(target=evaluate_design, args=(arm6,current_gen,cutoff,5+pnum*i,q,type,False))
        p6.start()
        p7 = Process(target=evaluate_design, args=(arm7,current_gen,cutoff,6+pnum*i,q,type,False))
        p7.start()
        p8 = Process(target=evaluate_design, args=(arm8,current_gen,cutoff,7+pnum*i,q,type,False))
        p8.start()
        p9 = Process(target=evaluate_design, args=(arm9,current_gen,cutoff,8+pnum*i,q,type,False))
        p9.start()
        p10 = Process(target=evaluate_design, args=(arm10,current_gen,cutoff,9+pnum*i,q,type,False))
        p10.start()

        for i in range(pnum):
            #set block=True to block until we get a result
            results.append(q.get(True))

        p1.join()
        p2.join()
        p3.join()
        p4.join()
        p5.join()
        p6.join()
        p7.join()
        p8.join()
        p9.join()
        p10.join()

    for i in range(len(results)):
        arm_scores = results[i][0]
        idx = results[i][1]
        arms[idx].scores = arm_scores

#Pick the best design
def tournament(arm1,arm2,arm3):
    #Tournament
    if max(arm1.scores,arm2.scores,arm3.scores) == arm1.scores:
        return arm1, arm1.scores
    if max(arm1.scores,arm2.scores,arm3.scores) == arm2.scores:
        return arm2, arm2.scores
    if max(arm1.scores,arm2.scores,arm3.scores) == arm3.scores:
        return arm3, arm3.scores

#For Evolutionary algorithm
