#!/usr/bin/env python
from ego_byu.tools import kin_tools as kt
#Add Custom Constraints

#NASA Constraints
# def check_constraints(arm)

def base_hom_violated(opt,r,t):
    if (t[0] < opt.base_x_CT[0] or t[0] > opt.base_x_CT[1]):
        return True
    if (t[1] < opt.base_y_CT[0] or t[1] > opt.base_y_CT[1]):
        return True
    if (t[2] < opt.base_z_CT[0] or t[2] > opt.base_z_CT[1]):
        return True
    if (r[0] < opt.base_angle_CT[0] or r[0]> opt.base_angle_CT[1]):
        return True
    return False

#Valid Link Length Constraints Violated?
def lengths_CT_violated(opt,lengths):
    if opt.robot_type == "nasa_arm":
        return (link_CT_violated(opt,lengths) or span_CT_violated(opt,lengths) or valve_CT_violated(opt,lengths))

    if opt.robot_type == "jabberwock" or opt.robot_type == "leg":
        return (link_CT_violated(opt,lengths) or span_CT_violated(opt,lengths))


def link_CT_violated(opt,lengths):
    for i in range(len(lengths)):
        if lengths[i] > opt.link_ct_high[i] or lengths[i] < opt.link_ct_low[i]:
            return True
    return False


def span_CT_violated(opt,lengths):
    if opt.robot_type == "nasa_arm":
        num_lengths = len(lengths)
        if (sum(lengths) > opt.armspan[1]-opt.jt_h*num_lengths or sum(lengths)+opt.jt_h*num_lengths < opt.armspan[0]):
            return True

    if opt.robot_type == "jabberwock" or opt.robot_type == "leg":
        num_lengths = len(lengths)
        if (sum(lengths) > opt.armspan[1]-opt.jt_h*(num_lengths-2) or sum(lengths) < opt.armspan[0]-opt.jt_h*(num_lengths-2)):
            return True
    return False

#Robot Specific Constraints
#NASA ARM CONSTRAINT ONLY
def valve_CT_violated(opt,lengths):
    #two consecutive links can't be under .18
    for i in range(len(lengths)-1):
        if (lengths[i] < opt.valve_CT and lengths[i+1] < opt.valve_CT):
            return True
    return False

#JABBERWOCK CONSTRAINT
def bend_angle_violated(opt,angles):
    if max(angles) > opt.bend_angles_CT[1] or min(angles) < opt.bend_angles_CT[0]:
        return True
    return False

#Overall check to ensure constraint aren't being violated in evolutionary algorithm
def check_CTs(opt,arm):
    if (arm.num_lengths >= opt.num_lengths_CT[1] or arm.num_lengths <opt.num_lengths_CT[0]):
        print "num_lengths Constraint Violated"
        print "num_lengths of arm:", arm.num_lengths
        raw_input("Error")

    r,t = kt.hom2ax_vec(arm.base_hom)
    if base_hom_violated(opt,r,t):
        print "Base Angle Constraint Violated"
        print "Base_hom:", arm.base_hom
        print r, t
        raw_input("Error")

    if lengths_CT_violated(opt,arm.lengths):
        print "Span/Length/Valve Constraint Violated"
        print "Lengths:", arm.lengths
        raw_input("Error")

    if opt.robot_type == "jabberwock" or opt.robot_type == "leg":
        if bend_angle_violated(opt,arm.bend_angles):
            print "Bend Angle Constraint Violated"
            print "Angles: ", arm.angles
            raw_input("Error")
