#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import arm_plotter as ap
import sys
sys.path.insert(0,'./visualization')
import leg_vis as ls


def initialize(opt):
    plt.ion()
    if opt.optimization_type == 'single_objective':
        opt.fig_lengths, opt.ax_lengths = plt.subplots(2,figsize=(10,18))
        opt.fig_base, opt.ax_base = plt.subplots(2,2,figsize=(18,18))

        if opt.robot_type == "jabberwock":
            opt.fig_angles, opt.ax_angles = plt.subplots(2,figsize=(10,18))
            opt.fig_angles.canvas.set_window_title('Optimal Robot')

        opt.fig_score, opt.ax_score = plt.subplots()
        opt.fig_optimal, opt.ax_optimal = plt.subplots()

    if opt.optimization_type == 'multi_objective':
        opt.fig_pareto, opt.ax_pareto = plt.subplots()

def shutdown(opt):
    # at.plot_fks(self.children_pool[index_best])#,self.ax_best,self.fig_best)
    plt.ioff()
    plt.show()

def plot(opt):
    #Plot Progress
    if opt.plot_on == True:
        if opt.optimization_type == "single_objective":
            if np.mod(opt.curr_generation,opt.plot_lengths_freq) == 0 or opt.curr_generation == 1:
                plot_lengths_evol(opt)

            plot_base_evol(opt)
            plot_score_evol(opt)
            plot_optimal(opt)
            # at.plot_arm(opt.children_pool[index_best],opt.ax4,opt.fig4)

            if opt.robot_type == "jabberwock":
                plot_bend_angles_evol(opt)

        if opt.optimization_type == "multi_objective":
            if opt.curr_generation in opt.plot_generations:
                plot_pareto(opt,opt.children_pool[1:int(opt.num_design/2.0)])

            if opt.curr_generation == opt.max_generations-1:
                plot_final_solutions(opt)

#Plotting Functions
#Plots lengths and length variances
def plot_lengths_evol(opt):
    plt.figure(opt.fig_lengths.number)
    opt.fig_lengths.canvas.set_window_title('Link Optimization')
    plt.suptitle("Link Length Optimization", fontsize=20)
    bar_width = opt.plot_lengths_freq/opt.max_generations*(4.0/5.0)
    col= 1.0/opt.max_generations

    #Lengths
    plt.subplot(211)
    i = opt.curr_generation-1
    ind = np.arange(1,len(opt.best_designs[i].lengths)+1)
    offset = bar_width*i/opt.plot_lengths_freq
    links = plt.bar(ind+offset, opt.best_designs[i].lengths, bar_width, color=np.array([1-i*col,i*col,0]),label="Gen. %d" % (i+1))
    plt.title("Evolution of Link Length, Plotted Every %d Generations" % opt.plot_lengths_freq, fontsize=17)
    plt.ylabel("Link Length (m)",fontsize=15)
    plt.xlabel("Link #",fontsize=15)
    plt.xticks(ind)
    plt.grid(True)

    #Variances
    plt.subplot(212)
    col = 1.0/opt.num_lengths_CT[0]
    for i in range(len(opt.link_variances)):
        plt.scatter(opt.curr_generation,opt.link_variances[i][-1], s=60,color=np.array([1-i*col,i*col,0]),label="Link %d" % (i+1))
    plt.title("Pool Variance: Link Lengths", fontsize=17)
    plt.ylabel("Variance",fontsize=15)
    plt.xlabel("Generation",fontsize=15)
    plt.grid(True)
    plt.draw()

    if opt.curr_generation == 1:
        plt.legend()

#Visualization of Base Mount
def plot_base_evol(opt):
    plt.figure(opt.fig_base.number)
    opt.fig_base.canvas.set_window_title('Base Optimization')
    plt.suptitle('Base Optimization',fontsize=20)
    r,t = kt.hom2ax_vec(opt.best_designs[-1].base_hom)

    #IN Y
    plt.subplot(221)
    plt.scatter(opt.curr_generation,t[1],s=60,color='red',label="Y Position")
    plt.scatter(opt.curr_generation,t[2],s=60,color='blue',label="Z Position")
    plt.title("Base Position of Optimal Design",fontsize=17)
    plt.xlabel('Generation',fontsize=15)
    plt.ylabel('Position (m)',fontsize=15)
    plt.grid(True)

    if opt.curr_generation == 1:
        plt.legend()

    plt.subplot(222)
    plt.scatter(opt.curr_generation,r[0]*180/np.pi,s=60,color='green',label="Base Angle")
    plt.title("Angle about +X of Optimal Design",fontsize=17)
    plt.xlabel('Generation',fontsize=15)
    plt.ylabel('Angle (degrees)',fontsize=15)
    plt.grid(True)

    plt.subplot(223)
    plt.scatter(opt.curr_generation,opt.base_hom_variance_Y[-1],s=60,color='red',label="Variance in Y")
    plt.scatter(opt.curr_generation,opt.base_hom_variance_Z[-1],s=60,color='blue',label="Variance in Z")
    plt.title('Positional Variance in Gene Pool',fontsize=17)
    plt.xlabel('Generation',fontsize=15)
    plt.ylabel('Variance',fontsize=15)
    plt.grid(True)

    if opt.curr_generation == 1:
        plt.legend()

    plt.subplot(224)
    plt.scatter(opt.curr_generation,opt.base_hom_variance_a[-1],s=60,color='green',label="Base Angle Variance")
    plt.title("Anglular Variance in Gene Pool",fontsize=17)
    plt.xlabel('Generation',fontsize=15)
    plt.ylabel('Angle (degrees)',fontsize=15)
    plt.grid(True)

    plt.draw()

#Plot mean and max score of each generation
def plot_score_evol(opt):
    plt.figure(opt.fig_score.number)
    opt.fig_score.canvas.set_window_title('Generation Mean Score')
    plt.scatter(opt.curr_generation,opt.mean_scores[-1], s=60,color='blue',label='mean')
    plt.scatter(opt.curr_generation,opt.max_scores[-1], s=60,color='green',label='max.')
    if opt.curr_generation ==1:
        plt.legend(loc=2)

    plt.title("Progression Statistics", fontsize=30)
    plt.ylabel("Stat of Scores",fontsize=20)
    plt.xlabel("Generation",fontsize=20)
    plt.grid(True)
    plt.draw()

#Visualization of Base Mount
def plot_bend_angles_evol(opt):
    plt.figure(opt.fig_angles.number)
    opt.fig_angles.canvas.set_window_title('Bend Angles Optimization')
    plt.suptitle("Bend Angles Optimization", fontsize=20)
    bend_angles = opt.best_designs[-1].bend_angles

    plt.subplot(211)
    plt.scatter(opt.curr_generation,bend_angles[0]*180/np.pi,s=60,color='red',label="Theta1")
    plt.scatter(opt.curr_generation,bend_angles[1]*180/np.pi,s=60,color='blue',label="Theta2")
    plt.title("Bend Angles of Optimal Jabberwock",fontsize=17)
    plt.xlabel('Generation',fontsize=15)
    plt.ylabel('Angles (degrees)',fontsize=15)
    plt.grid(True)

    if opt.curr_generation == 1:
        plt.legend()

    #Variances
    plt.subplot(212)
    plt.scatter(opt.curr_generation,opt.angle_variances[0][-1], s=60,color='red',label="Theta1")
    plt.scatter(opt.curr_generation,opt.angle_variances[1][-1], s=60,color='blue',label="Theta2")
    plt.title("Pool Variance: Link Lengths", fontsize=17)
    plt.ylabel("Variance",fontsize=15)
    plt.xlabel("Generation",fontsize=15)
    plt.grid(True)
    plt.draw()

    if opt.curr_generation == 1:
        plt.legend()

def plot_optimal(opt):
    fig = plt.figure(opt.fig_optimal.number)
    plt.clf()
    plt.suptitle("Optimal Robot", fontsize=20)
    ax = fig.gca(projection='3d')

    opt.best_designs[-1].setJointAngsZero()
    opt.best_designs[-1].calcFK()
    if opt.robot_type == "jabberwock":
        arm_plotter = ap.JabberPlotter(opt.best_designs[-1])
    if opt.robot_type == "nasa_arm":
        arm_plotter = ap.ArmPlotter(opt.best_designs[-1])
    arm_plotter.plot(ax)
    ap.set_axes_equal(ax)
    plt.draw()

def plot_final_solutions(opt):
    arm1 = min(opt.children_pool, key=lambda(item) : item.scores_adj[0])
    arm2 = min(opt.children_pool, key=lambda(item) : item.scores_adj[1])
    arm3 = min(opt.children_pool, key=lambda(item) : abs(item.scores_adj[1]-item.scores_adj[0]))
    arms = [arm1,arm2,arm3]
    for arm in arms:
        arm.setJointAngsReal(np.array([0,0,-.3,0,.3,0]))
        arm.calcFK()

    if opt.robot_type == "leg":
        arm.setJointAngsZero()
        arm.calcFK()
        ls.leg_plotter(arm1,np.array([0.0,0.0,0.0,0.0]), "Dexterous Robot")
        ls.leg_plotter(arm2,np.array([0.0,0.0,0.0,0.0]), "Best Payload Robot")
        ls.leg_plotter(arm3,np.array([0.0,0.0,0.0,0.0]), "Optimal Robot In Both Metrics")


    else:
        fig = plt.figure()
        plt.clf()
        plt.suptitle("Dexterous Robot", fontsize=20)
        ax = fig.gca(projection='3d')
        arm_plotter = ap.SimpleArmPlotter(arm1)
        arm_plotter.plot(ax)
        ap.set_axes_equal(ax)
        ax.zaxis.set_visible(False)
        ax.w_xaxis.line.set_lw(0.)
        ax.set_xticks([])
        ax.view_init(elev = 20,azim=20)
        plt.draw()

        fig = plt.figure()
        plt.clf()
        plt.suptitle("Stiff Robot", fontsize=20)
        ax = fig.gca(projection='3d')
        arm_plotter = ap.SimpleArmPlotter(arm2)
        arm_plotter.plot(ax)
        ap.set_axes_equal(ax)
        ax.zaxis.set_visible(False)
        ax.w_xaxis.line.set_lw(0.)
        ax.set_xticks([])
        ax.view_init(elev = 20,azim=20)
        plt.draw()

        fig = plt.figure()
        plt.clf()
        plt.suptitle("Optimal Robot In Both Metrics", fontsize=20)
        ax = fig.gca(projection='3d')
        arm_plotter = ap.SimpleArmPlotter(arm3)
        arm_plotter.plot(ax)
        ap.set_axes_equal(ax)
        ax.zaxis.set_visible(False)
        ax.w_xaxis.line.set_lw(0.)
        ax.set_xticks([])
        ax.view_init(elev = 20,azim=20)
        plt.draw()

def plot_pareto(opt,arms):
    x = []
    y = []
    for i in range(len(arms)):
        x.append(arms[i].scores[0])
        y.append(arms[i].scores[1])

    plt.figure(1)
    col = opt.curr_generation/(opt.max_generations*1.0)
    opt.fig_pareto.canvas.set_window_title('Pareto Front')
    if opt.curr_generation == opt.plot_generations[0]:
        col = np.array([1,0,0])
    if opt.curr_generation == opt.plot_generations[1]:
        col = np.array([1,.5,0])
    # if opt.curr_generation == opt.plot_generations[2]:
    #     col = np.array([1,1,0])
    if opt.curr_generation == opt.plot_generations[2]:
        col = np.array([.7,1,0])
    if opt.curr_generation == opt.plot_generations[3]:
        col = np.array([0,.7,0])
    if opt.curr_generation == opt.plot_generations[4]:
        col = np.array([0,1,.5])
    if opt.curr_generation == opt.plot_generations[5]:
        col = np.array([0,0,1])
    # opt.ax_pareto.scatter(x,y, s=60,color=np.array([1-col,col,0]),label='Gen. %s' % str(opt.curr_generation))
    opt.ax_pareto.scatter(x,y, s=60,color=col,label='Gen. %s' % str(opt.curr_generation))
    plt.title("Pareto Front", fontsize=30)
    plt.ylabel(r"Load Bearing Capacity $\alpha_{sum}$",fontsize=20)
    plt.xlabel(r"Dexterity $\gamma_{sum}$",fontsize=20)
    # plt.xlim(-1000,6000)
    # plt.ylim(-50, 200)
    plt.legend()
    plt.grid(True)
    plt.draw()
