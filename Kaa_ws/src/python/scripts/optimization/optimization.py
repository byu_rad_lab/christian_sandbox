#!/usr/bin/env python
from __future__ import division
import numpy as np
import copy
from matplotlib.ticker import FuncFormatter
from mpl_toolkits.mplot3d import Axes3D
import math
import datetime as dt

from ego_byu.tools import arm_tools as at
from ego_byu.tools import arm_plotter as ap
from ego_byu.tools import pickle_tools as pk
from ego_byu.tools import kin_tools as kt

import opt_tools as ot
import preferences as pref

# FilesInThisFolder
import constraints as cts
import plotting as pt

# This version of the evolutionary algorithm attempts to incorporate more variables:
# - num_lengths #
# - Mounting position
# - Arm Length
np.set_printoptions(threshold=50000)

class Optimization(object):

    def __init__(self):
        if np.mod(pref.Number_Of_Designs, 2) != 0 or pref.Planned_Generations < 10:
            print "Number of Designs/Generations not compatible with processing setup"
            assert(False)

        #Jabberwock or NASA or Leg
        self.robot_type = pref.robot_type

        # Mounting Constraints
        self.base_x_CT = pref.Base_X_Range
        self.base_y_CT = pref.Base_Y_Range
        self.base_z_CT = pref.Base_Z_Range

        # Arm and Link Constraint
        self.link_ct_low = pref.Link_Lower_Limits
        self.link_ct_high =pref.Link_Upper_Limits
        self.armspan = pref.Armspan_Limits
        self.num_lengths_CT = pref.Number_of_Links

        # JabberWock
        ##################### JABBERWOCK ################################
        if self.robot_type == 'jabberwock':
            # Constraints on Arm
            self.bend_angles_CT = pref.Link_Bend_Constraints

            # Arm Parameters
            self.jt_h = .182 #FIXED - To change this adjust jabberwock() in arm_tools
            self.angle_variances = [[] for i in range(len(self.bend_angles_CT))]

            # BASE
            self.base_angle_CT = pref.Jabber_Base_Angle_Constraint

       # Leg
        ##################### LEG ################################
        if self.robot_type == 'leg':
            # Constraints on Arm
            self.bend_angles_CT = pref.Link_Bend_Constraints

            # Arm Parameters
            self.jt_h = .2286 #was .182 # pass this into the leg function
            self.link_density = pref.Link_Density
            self.angle_variances = [[] for i in range(len(self.bend_angles_CT))]

            # BASE
            self.base_angle_CT = pref.Leg_Base_Angle_Constraint #may want to make it so can change around y as well.

        # Nasa Arm
        ##################### NASA ARM ################################
        if self.robot_type == 'nasa_arm':
            # Constraints on Arm
            self.valve_CT = pref.Nasa_Valve_Constraint

            # Arm Parameters
            self.jt_h = pref.Nasa_Joint_Height

            # BASE
            self.base_angle_CT = pref.Nasa_Base_Angle_Constraint

        ##################### EVOLUTIONARY PARAMETERS ################################
        # Gene Pools and Score/Design Tracking Variables
        self.curr_generation = 0
        self.num_design = pref.Number_Of_Designs
        self.num_pairs = int(self.num_design/2)
        self.max_generations = pref.Planned_Generations
        self.plot_generations = pref.Plotted_Generations

        self.best_designs = []
        self.parent_pool = []
        self.children_pool = []
        self.max_scores = []
        self.mean_scores = []
        self.std_scores = []
        self.link_variances = [[] for i in range(self.num_lengths_CT[0])]
        self.base_hom_variance_a = []
        self.base_hom_variance_X = []
        self.base_hom_variance_Y = []
        self.base_hom_variance_Z = []

        # Mutation and Perturbation Parameters
        self.mutation_rate = pref.Mutation_Rate
        self.p_lengths = pref.Initial_Max_Perterbation_Lengths
        self.p_hom_trans = pref.Initial_Max_Perterbation_Base_Trans
        self.p_hom_angle = pref.Initial_Max_Perturbation_Base_Angle
        self.p_angles = pref.Initial_Max_Perterbation_Jabberwock_Angles

        # Optimization Parameters
        self.optimization_type = pref.optimization_type
        self.FK_cutoff = pref.FK_Redundancy_Cutoff

        # Plotting Options
        self.plot_on = True
        self.plot_lengths_freq = round(self.max_generations/10)

    def run(self):

        # Set Up Visualization
        if self.plot_on:
            pt.initialize(self)

        # Initialize Evolution
        self.initialize_first_generation()

        # Start Evolution, Kill on Control-C
        self.start_time = dt.datetime.now()
        try:
            while self.curr_generation < self.max_generations:
                self.evolve()
        except KeyboardInterrupt:
            raw_input("Evolution Interrupted: Press Return")
        self.end_time = dt.datetime.now()
        print "Total Time: ", self.end_time - self.start_time

        # Report Solution
        print "------------------FINAL OPTIMIZATION RESULTS--------------------"
        self.Report_Solution()

        # Save Best Design
        if self.optimization_type == "single_objective":
            at.pickle_arms([self.best_designs[-1]],self.robot_type)
        elif self.optimization_type == "multi_objective":
            at.pickle_arms(self.children_pool,self.robot_type)

        # Shut Down Plotter
        if self.plot_on:
            pt.shutdown(self)

    def initialize_first_generation(self):
        print "Generating Initial Set of Robots"
        for i in range(self.num_design):
            arm = self.generate_design()
            self.children_pool.append(arm)
        self.curr_generation += 1

    def evolve(self):
        print "--------------------GENERATION ", self.curr_generation, "-------------------------"
        # Set up/update generation variables
        self.parent_pool = self.children_pool
        self.children_pool = []
        self.children_scores = []

        # Crossover, Generate Children and return List of Parent Indices in Order
        print "Performing Crossover"
        children, parent_indices = self.perform_crossover()

        # Perform Evaluations of Children and Parents (Internal score member assigned)
        self.AssignIndices(self.parent_pool, children)
        print "Evaluating Parents"
        ot.eval_arm_set(self.parent_pool, self.curr_generation, self.FK_cutoff, self.optimization_type)
        print "Evaluating Children"
        ot.eval_arm_set(children, self.curr_generation, self.FK_cutoff, self.optimization_type)

        # Tournament or Maximin Function
        print "Choosing Designs"
        self.ChooseDesigns(children, parent_indices)

        # Report Solution
        print "Best Design(s)"
        self.Report_Solution()

        # Perturbate Children Slightly to New Design
        print "Perturbating Child Designs"
        if self.curr_generation != self.max_generations-1:
            self.perform_perturbation()

        print "Plotting"
        pt.plot(self)
        self.curr_generation += 1

    #---------------GENERATING DESIGN RANDOMLY ROUTINES----------------------
    def generate_design(self):
        # Generate (num_lengths) links within link CT, check sum within arm CT, try again if failed
        num_lengths, lengths = self.choose_links()
        base_hom = self.choose_base_hom()

        # Construct and return arm
        if self.robot_type == "nasa_arm":
            arm = at.nasa_arm(self.jt_h, lengths, base_hom)
        elif self.robot_type == "jabberwock":
            angles = self.choose_bend_angles(num_lengths)
            arm = at.jabberwock(lengths, angles, base_hom)
        elif self.robot_type == "leg":
            angles = self.choose_bend_angles(num_lengths)
            arm = at.leg(lengths, angles, base_hom, self.jt_h, self.link_density)
        cts.check_CTs(self, arm)
        return arm

    # Builds a valid sequence of lengths
    def choose_links(self):
        num_lengths = int(np.random.uniform(self.num_lengths_CT[0], self.num_lengths_CT[1]))
        lengths = []
        for i in range(num_lengths):
            lengths.append(np.random.uniform(self.link_ct_low[i], self.link_ct_high[i]))

        while(cts.lengths_CT_violated(self, lengths)):  # If any link length CTs are broken, pick new. This sees if the links violate there length constraints individual and as a arm as a whole.
            # print "Lengths CT Violated", lengths,", Repicking"
            for i in range(num_lengths):
                lengths[i] = np.random.uniform(self.link_ct_low[i], self.link_ct_high[i])

        return num_lengths, lengths

    def choose_bend_angles(self, num_lengths):  # JABBERWOCK AND LEGS ONLY
        angles = []
        if self.robot_type == "jabberwock":
            angles_num = 2
        elif self.robot_type == "leg":
            angles_num = num_lengths
        for i in range(angles_num):
            angles.append(np.random.uniform(self.bend_angles_CT[0], self.bend_angles_CT[1]))
        return angles

    # Picks random z and angle (Neutral angle with arm pointed horizontally
    # from KREX. Arm allowed to be mounted tilted upwards or downwards from
    # horizontal)
    def choose_base_hom(self):
        base_a = np.random.uniform(self.base_angle_CT[0], self.base_angle_CT[1])
        base_x = np.random.uniform(self.base_x_CT[0], self.base_x_CT[1])
        base_y = np.random.uniform(self.base_y_CT[0], self.base_y_CT[1])
        base_z = np.random.uniform(self.base_z_CT[0], self.base_z_CT[1])
        return kt.ax_vec2homog(np.array([base_a, 0, 0]), np.array([base_x, base_y, base_z]))

    #-----------------------------CROSSOVER ROUTINES---------------------------------
    def perform_crossover(self):
        children = []
        indices = np.arange(self.num_design)
        np.random.shuffle(indices)
        for i in range(int(len(indices)/2)):
            children.append(self.crossover(self.parent_pool[indices[2*i]], self.parent_pool[indices[2*i+1]]))
        return children, indices

    # Purpose: Find a child from 2 arms
    def crossover(self, arm1, arm2):
        lengths = self.crossover_lengths(arm1.lengths, arm2.lengths)
        while cts.lengths_CT_violated(self, lengths):
            lengths = self.crossover_lengths(arm1.lengths, arm2.lengths)

        # Crossover base mounts
        base_hom = self.crossover_base(arm1.base_hom, arm2.base_hom)

        if self.robot_type == 'jabberwock' or self.robot_type == 'leg':
            angles = self.crossover_angles(arm1.bend_angles, arm2.bend_angles)

        # mutation
        if np.random.rand() < self.mutation_rate:
            lengths = self.mutate_lengths(lengths)
            base_hom = self.choose_base_hom()
            if self.robot_type == 'jabberwock' or self.robot_type == 'leg':
                angles = angles[::-1]

        # ARM
        if self.robot_type == 'nasa_arm':
            arm_child = at.nasa_arm(self.jt_h, lengths, base_hom)
        elif self.robot_type == 'jabberwock':
            arm_child = at.jabberwock(lengths, angles, base_hom)
        elif self.robot_type == 'leg':
            arm_child = at.leg(lengths,angles,base_hom, self.jt_h, self.link_density)

        cts.check_CTs(self, arm_child)
        return arm_child

    # Mix lengths of parents
    def crossover_lengths(self, lengths1, lengths2):
        lengths = []
        min_num_lengths = min(len(lengths1), len(lengths2))
        diff_num_lengths = abs(len(lengths1)-len(lengths2))
        for i in range(min_num_lengths):
            dice = np.random.rand()
            if (dice < 1.0/3.0):
                lengths.append(lengths1[i])
            elif (dice > 2.0/3.0):
                lengths.append(lengths2[i])
            else:
                lengths.append(np.mean([lengths1[i], lengths2[i]]))

        # Add random number of extra links to child if links don't match up
        for i in range(min_num_lengths, min_num_lengths+diff_num_lengths):
            dice = np.random.rand()
            if dice < 1.0/2.0:
                if len(lengths1) > len(lengths2):
                    lengths.append(lengths1[i])
                else:
                    lengths.append(lengths2[i])

        return lengths

    def crossover_angles(self, angles1, angles2):
        angles = []
        for i in range(len(angles1)):
            dice = np.random.rand()
            if (dice < 1.0/3.0):
                angles.append(angles1[i])
            elif (dice > 2.0/3.0):
                angles.append(angles2[i])
            else:
                angles.append(np.mean([angles1[i], angles2[i]]))
        return angles

    # Randomly select either of the basemounts or avg.
    def crossover_base(self, hom1, hom2):
        dice = np.random.rand()
        if (dice < 1.0/3.0):
            return hom1
        elif (dice > 2.0/3.0):
            return hom2
        else:
            return kt.hom_avg(hom1, hom2)

    #------------------MUTATION ROUTINES-----------------------
    def mutate_lengths(self, lengths):
        new_lengths = self.switch_lengths(lengths)

        while cts.lengths_CT_violated(self, new_lengths):
            new_lengths = self.switch_lengths(lengths)

        return new_lengths

    def switch_lengths(self, lengths):
        new_lengths = copy.copy(lengths)
        ind1 = math.trunc(np.random.rand()*len(lengths))
        ind2 = math.trunc(np.random.rand()*len(lengths))
        while ind2 == ind1:
            ind2 = math.trunc(np.random.rand()*len(lengths))
        new_lengths[ind2] = lengths[ind1]
        new_lengths[ind1] = lengths[ind2]
        return new_lengths

    #-----------------PERTURBATION ROUTINES------------------------
    def perform_perturbation(self):
        for i in range(len(self.children_pool)):
            # Create Slightly Modified arm of Winner: Allow for local refinement
            arm_perturbed = self.perturbate(self.children_pool[i])
            self.children_pool.append(arm_perturbed)
            self.children_scores.append(0)  # scoring is 0 for perturbed, must be re-evaluated

    # Generate slightly modified version of arm, while keeping within CT. Helps in later stages of evolution
    def perturbate(self, arm):
        lengths = self.find_perturbed_lengths(arm.lengths)
        base_hom = self.find_perturbed_base_hom(arm.base_hom)
        if self.robot_type == 'jabberwock':
            angles = self.find_perturbed_angles(arm.bend_angles)
            arm_p = at.jabberwock(lengths, angles, base_hom)
        if self.robot_type == 'nasa_arm':
            arm_p = at.nasa_arm(self.jt_h, lengths, base_hom)
        if self.robot_type == 'leg':
            angles = self.find_perturbed_angles(arm.bend_angles)
            arm_p = at.leg(lengths, angles, base_hom, self.jt_h, self.link_density)
        cts.check_CTs(self, arm_p)
        return arm_p

    # Moves joint positions slightly by a uniform variable in +/- self.pet
    def find_perturbed_lengths(self, lengths_arm):
        lengths = copy.copy(lengths_arm)
        p = self.p_lengths*(self.max_generations-self.curr_generation/2.0)/self.max_generations # this slowly decreases over of the generations

        for i in range(len(lengths)):
            lengths[i] = lengths_arm[i]+np.random.uniform(-p, p)
            lengths[i] = self.sat_param(lengths[i], self.link_ct_low[i], self.link_ct_high[i])
        while cts.lengths_CT_violated(self, lengths):
            for i in range(len(lengths)):
                lengths[i] = lengths_arm[i]+np.random.uniform(-p, p)
                lengths[i] = self.sat_param(lengths[i], self.link_ct_low[i], self.link_ct_high[i])
        return lengths

    # Perturbs Base Hom
    def find_perturbed_base_hom(self, base_hom_arm):
        p_angle = self.p_hom_angle*(self.max_generations-self.curr_generation/2.0)/self.max_generations
        p_trans = self.p_hom_trans*(self.max_generations-self.curr_generation/2.0)/self.max_generations
        # perturb base
        r, t = kt.hom2ax_vec(base_hom_arm)
        r_p = copy.copy(r)
        t_p = copy.copy(t)

        r_p[0] = r[0] + np.random.uniform(-p_angle, p_angle)
        r_p[0] = self.sat_param(r_p[0], self.base_angle_CT[0], self.base_angle_CT[1])
        t_p[2] = t[2] + np.random.uniform(-p_trans, p_trans)
        t_p[2] = self.sat_param(t_p[2], self.base_z_CT[0], self.base_z_CT[1])
        if self.robot_type == 'jabberwock':
            t_p[1] = t[1] + np.random.uniform(-p_trans, p_trans)
            t_p[1] = self.sat_param(t_p[1], self.base_y_CT[0], self.base_y_CT[1])
        if (cts.base_hom_violated(self, r_p, t_p)):
            print "BaseHom Violated During Perturbation"
            assert(False)

        base_hom = kt.ax_vec2homog(r_p, t_p)
        return base_hom

    # Perturbs Bend Angles in Jabberwock
    def find_perturbed_angles(self, angles_arm):
        angles = copy.copy(angles_arm)
        p = self.p_angles*(self.max_generations-self.curr_generation/2.0)/self.max_generations
        for i in range(len(angles)):
            angles[i] = angles_arm[i]+np.random.uniform(-p, p)
            angles[i] = self.sat_param(angles[i], self.bend_angles_CT[0], self.bend_angles_CT[1])
        if (cts.bend_angle_violated(self, angles)):
            print "Bend Angles Violated During Perturbation"
            assert(False)
        return angles

    #---------------------SELECTION ROUTINES-------------------
    def ChooseDesigns(self, children, indices):
        if self.optimization_type == "single_objective":  # (Tournament)
            print "Tournament Evaluation"
            scores = self.perform_tournament(children, indices)
            index_best = self.children_scores.index(max(self.children_scores))
            self.max_scores.append(max(self.children_scores))
            self.best_designs.append(self.children_pool[index_best])
            self.Process_Generation_Stats(scores)

        elif self.optimization_type == "multi_objective":  # Maximin Optimization
            print "Maximin Fitness Evaluation"
            arms = np.append(self.parent_pool, children)
            self.children_pool = self.Maximin(arms)

    def perform_tournament(self, children, indices):
        scores = np.array([])  # Only Children Scores (Not Perturbed Version Scores)
        for i in range(len(children)):
            arm, score = ot.tournament(
                self.parent_pool[indices[2 * i]],
                self.parent_pool[indices[2 * i + 1]],
                children[i])
            scores = np.append(scores, score)
            self.children_pool.append(arm)
            self.children_scores.append(score)
        return scores

    def Maximin(self, arms):
        # Normalize by Largest Score
        self.inv_normalize_scores(arms)

        # Maximin Function
        temp = []
        scores = []
        for i in range(len(arms)):
            for j in range(len(arms)):
                if j != i:
                    temp.append(min(arms[i].scores_adj - arms[j].scores_adj))
            arms[i].fitness = 1-max(temp)
            temp = []
            # r,t =  kt.hom2ax_vec(arms[i].base_hom)
            # print 'arm:  {0:2d}, fitness: {1:1.2f}, scores: [{2:6.0f},{3:6.0f}],
            # adjusted: [{4:1.3f},{5:1.3f}], base_h: {6:1.2f}, base_wx: {7:4.1f},
            # lengths:'.format(i,arms[i].fitness,round(arms[i].scores[0]),round(arms[i].scores[1]),np.around(arms[i].scores_adj[0],decimals=2),arms[i].scores_adj[1],t[2],r[0]*180/np.pi),
            # np.around(arms[i].lengths,decimals=2)
            scores.append(arms[i].fitness)

        best_designs = []
        temp_arms = copy.copy(arms)

        for i in range(int(1/3.0*len(arms))):
            best_fitness = 0
            best_index = 0

            for j in range(len(temp_arms)):
                if temp_arms[j].fitness > best_fitness:
                    best_fitness = temp_arms[j].fitness
                    best_index = j

            best_designs.append(temp_arms[best_index])
            temp_arms = np.delete(temp_arms, best_index)

        return best_designs

    def inv_normalize_scores(self, arms):
        # First Normalize from 0 to 1
        for i in range(len(arms)):
            arms[i].scores_adj = (arms[i].scores)

        # Find Max Scores for Normalization
        max_scores = copy.copy(arms[0].scores_adj)
        min_scores = copy.copy(arms[0].scores_adj)
        for i in range(len(arms[0].scores_adj)):
            for j in range(len(arms)):
                if arms[j].scores_adj[i] > max_scores[i]:
                    max_scores[i] = arms[j].scores_adj[i]
                if arms[j].scores_adj[i] < min_scores[i]:
                    min_scores[i] = arms[j].scores_adj[i]

        # Make into Minimization by adding 1- to normalized scores.
        for i in range(len(arms)):
            arms[i].scores_adj = 1-(arms[i].scores_adj-min_scores)/(max_scores-min_scores)

        return arms

    #-----------------------HELPER ROUTINES------------------------
    def sat_param(self, p, pmin, pmax):
        if p < pmin:
            p = pmin
        elif p > pmax:
            p = pmax
        return p

    # Single Objective Processing Only
    def Process_Generation_Stats(self, scores):
        gen_lengths = [[] for i in range(self.num_lengths_CT[0])]  # Keeps track of links in entire gen.
        if self.robot_type == "jabberwock":
            gen_angles = [[] for i in range(len(self.bend_angles_CT))]
        gen_base_hom_a = []
        gen_base_hom_X = []
        gen_base_hom_Y = []
        gen_base_hom_Z = []

        for i in range(len(self.children_pool)):
            for j in range(self.num_lengths_CT[0]):
                gen_lengths[j].append(self.children_pool[i].lengths[j])
            if self.robot_type == "jabberwock":
                for j in range(len(self.bend_angles_CT)):
                    gen_angles[j].append(self.children_pool[i].bend_angles[j])
            r, t = kt.hom2ax_vec(self.children_pool[i].base_hom)
            gen_base_hom_a.append(r[0])
            gen_base_hom_X.append(t[0])
            gen_base_hom_Y.append(t[1])
            gen_base_hom_Z.append(t[2])

        # Record statistics on scores of gene pool
        self.mean_scores = np.append(self.mean_scores, np.mean(scores))
        self.std_scores = np.append(self.std_scores, np.std(scores))

        # Record Link Variances in generation
        for i in range(self.num_lengths_CT[0]):
            self.link_variances[i].append(np.var(gen_lengths[i]))

        if self.robot_type == "jabberwock":
            for i in range(len(self.bend_angles_CT)):
                self.angle_variances[i].append(np.var(gen_angles[i]))

        self.base_hom_variance_a.append(np.var(gen_base_hom_a))
        self.base_hom_variance_X.append(np.var(gen_base_hom_X))
        self.base_hom_variance_Y.append(np.var(gen_base_hom_Y))
        self.base_hom_variance_Z.append(np.var(gen_base_hom_Z))

    def AssignIndices(self, parents, children):
        i = 0
        for j in range(len(parents)):
            parents[j].index = i
            i += 1
        for k in range(len(children)):
            children[k].index = i
            i += 1

    def Report_Solution(self):
        if self.optimization_type == "single_objective":
            # Report Best Design
            print "Best Robot Arm Found: Arm", self.best_designs[-1].index
            print "Lengths:", self.best_designs[-1].lengths
            if self.robot_type == 'jabberwock':
                print "Angles:", self.best_designs[-1].bend_angles
            r, t = kt.hom2ax_vec(self.best_designs[-1].base_hom)
            print "Ax Vec: ", r*180/np.pi
            print "Vertical Mounting Position: ", t
            print "SCORE: ", self.max_scores[-1]

        if self.optimization_type == "multi_objective":
            arms = self.children_pool
            for i in range(len(arms)):
                r, t = kt.hom2ax_vec(arms[i].base_hom)
                r, t = kt.hom2ax_vec(arms[i].base_hom)
                if self.robot_type == "nasa_arm":
                    print 'Arm:  {0:2d}, fitness: {1:1.2f}, scores: [{2:6.0f},{3:6.0f}], adjusted: [{4:1.3f},{5:1.3f}], base_h: {6:1.2f}, base_wx: {7:4.1f}, lengths:'.format(arms[i].index, arms[i].fitness, round(arms[i].scores[0]), round(arms[i].scores[1]), np.around(arms[i].scores_adj[0], decimals=2), arms[i].scores_adj[1], t[2], r[0]*180/np.pi), np.around(arms[i].lengths, decimals=2)
                if self.robot_type == "jabberwock" or self.robot_type == "leg":
                    print 'Arm:  {0:2d}, fitness: {1:1.2f}, scores: [{2:6.0f},{3:6.0f}], adjusted: [{4:1.3f},{5:1.3f}], base_h: {6:1.2f}, base_wx: {7:4.1f}, lengths: {8:s} , angles: {9:s}'.format(arms[i].index, arms[i].fitness, round(arms[i].scores[0]), round(arms[i].scores[1]), np.around(arms[i].scores_adj[0], decimals=2), arms[i].scores_adj[1], t[2], r[0]*180/np.pi, np.around(arms[i].lengths, decimals=2), np.around(arms[i].bend_angles, decimals=2))


if __name__ == '__main__':
    evolution_solver = Optimization()
    evolution_solver.run()
