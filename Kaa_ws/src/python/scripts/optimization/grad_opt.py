import sys
sys.path.insert(0,'./visualization')
from ego_byu.tools import arm_tools as at
import numpy as np
from ego_byu.tools import pickle_tools as pk
from ego_core.kinematics import kinematics as kin
import os
from ego_byu.armeval import armeval as ae
import pyOpt
import visualization as vis
import leg_vis as ls
from ego_byu.tools import kin_tools as kt


def objfunc(x):
    overall_length = 0.762 #0.4572 #combined with the joint size this would make the max length around 3 feet
    g = [0.0]*4

    base_hom = kt.ax_vec2homog(np.array([x[8], 0, 0]), np.array([0, 0, 0]))
    arm = at.leg([x[0],x[1],x[2],x[3]],[x[4],x[5],x[6],x[7]], base_hom, .2286)
    evaluator = vis.EvaluateArm(arm, 30, 5, 4, 30,.90)
    print "reach_score_: ", evaluator.scores_.reach_score_

    f = -1.0*evaluator.scores_.reach_score_/10000.0
    # print "score:   ", f
    g[0] = sum(x[0:4]) - overall_length
    # print "g:   ", g
    arm.setJointAngsZero()
    arm.calcFK()
    x_zero = arm.g_world_tool()[0][3]
    y_zero = arm.g_world_tool()[1][3]
    z_zero = arm.g_world_tool()[2][3]
    g[1] = -x_zero + .1
    g[2] =  y_zero + .1
    g[3] = -z_zero + .25

    print "x: ", x_zero
    print "y: ", y_zero
    print "z: ", z_zero

    print g

    fail = 0
    return f, g, fail


if __name__ == '__main__':

    opt_prob = pyOpt.Optimization('Leg_Rad',objfunc)

    opt_prob.addObj('f')


    opt_prob.addVarGroup('links',4,type='c',value=[0.25, 0.25, 0.25, 0.25],lower=[0.0,0.0,0.0,0.0],upper=[1.5,1.5,1.5,1.5])

    opt_prob.addVarGroup('angles',4,type='c',value=[0.0, 0.0, 0.00, 0.0],lower=[-np.pi/2.0,-np.pi/2.0,-np.pi/2.0,-np.pi/2.0],upper=[np.pi/2.0,np.pi/2.0,np.pi/2.0,np.pi/2.0])

    opt_prob.addVar('base_angle',type='c',lower=-np.pi/2.0,upper=np.pi,value=0.0)

    opt_prob.addCon('tot_length',type='i',lower=-1000,upper=1000,equal=0.0)

    opt_prob.addConGroup('zero_configuration',3,type='i',lower=[-1000,-1000,-1000],upper=[1000,1000,1000],equal=[0.0,0.0,0.0])

    print opt_prob

    raw_input()

    slsqp = pyOpt.SLSQP(options={'MAXIT':1000,'IPRINT':0})

    [fstr, xstr, inform] = slsqp(opt_prob, sens_type='FD')

    print fstr
    print xstr
    # print inform.text
    # print inform.value
    print inform

    base_hom = kt.ax_vec2homog(np.array([xstr[8],0.0,0.0]),np.array([0,0,0]))
    arm = at.leg([xstr[0],xstr[1],xstr[2],xstr[3]],[xstr[4],xstr[5],xstr[6],xstr[7]], base_hom, .2286)
    ls.leg_plotter(arm, np.array([0.0, 0.0, 0.0, 0.0]))
    #[-0.     0.5    0.5    0.5    0.113 -0.035  0.01   0.005  0.   ]
    #[ 0.25   0.25   0.5    0.5    0.016  0.013 -0.003 -0.003  0.   ]
    #[ 0.229  0.229  0.     0.     0.     0.     0.     0.     0.   ]
    #[ 0.704  0.     0.     0.     0.179 -1.571  1.571  1.571  0.   ]

    ls.plot_ws(arm)
    evaluator = vis.EvaluateArm(arm, 30, 5, 4, 30,.90)
    ls.plot_z_planes(evaluator)

