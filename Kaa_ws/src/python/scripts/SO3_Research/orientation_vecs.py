#!/usr/bin/env python
from __future__ import division
from ego_core.kinematics import kinematics as kin
from ego_byu.tools import arm_tools as at
from ego_byu.tools import kin_tools as kt

import numpy as np  # probably best to import this before kinematics
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from itertools import product, combinations
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d
import math
#This code grids rotation vectors gridded over a 3d wx, wy, wz space
#Magnitude of rot vec is constrained under

def one_toward_center(d,center):
    if d>center:
        d-=1
    if d<center:
        d+=1
    return d

class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

#n = 3 is 19
#n = 5 is 81
#n = 7 is 179
#n = 9 is 389
groups = 389
colors = []
for z in range(groups+1):
    colors.append([np.random.rand(),np.random.rand(),np.random.rand()])

n = 4
dx = np.pi*(n-1)/n
wx = np.linspace(-dx, dx,n)
wy = np.linspace(-dx, dx,n)
wz = np.linspace(-dx, dx,n)
dth = wx[-1]-wx[-2]

#draw sphere
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.set_aspect("equal")
u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
dx=np.cos(u)*np.sin(v)
dy=np.sin(u)*np.sin(v)
dz=np.cos(v)
ax.plot_wireframe(dx*np.pi, dy*np.pi, dz*np.pi, color="r")

#draw origin
ax.scatter([0],[0],[0],color="g",s=100)

# #draw arros
# print wx,wy,wz

# #Plots standard Vectors
for x in wx:
    for y  in wy:
        for z in wz:
            d = np.linalg.norm([x,y,z])
            if d < np.pi:
                xx= x
                yy= y
                zz= z
                # if d<0.75 * np.pi:

                color = 'k'
                a = Arrow3D([0,xx],[0,yy],[0,zz], mutation_scale=20, lw=1, arrowstyle="-|>", color=color)
                ax.add_artist(a)

distribution = np.zeros(groups)

#Picks Random orientations and plots them by the color of their group number
#Define an arm
np.set_printoptions(precision=3)
lengths = np.array([3,3.5,.5,.5,.5,.5,.5,.5,.5,.5,.5,0.5, 0.5,.5,.5,.5,.5,.5,.5,.5,3])
height = 0
#New Arm Implemented
a = kin.Arm()
for i in range(len(lengths)):
    j = kin.JointCC1D(height)
    l = kin.Link(j)
    l.setGTopEnd(0.0, 0.0, lengths[i])

    # Rotation about Z for each Joint
    g = kt.ax_vec2homog(np.array([0, 0, -np.pi/2]), np.array([0, 0, 0]))
    l.g_top_end_ = np.dot(g, l.g_top_end_)
    l.g_top_com_ = np.eye(4)
    l.g_top_com_[2][3] = lengths[i]/2.0

    # addLink
    a.addLink(l)

a.setJointAngsZero()

#Set up mapping between rotations
orientation_vectors = []
orientation_map = np.zeros((len(wx),len(wy),len(wz)),dtype=int)
index = 1
for i,x in enumerate(wx):
    for j,y in enumerate(wy):
        for k,z in enumerate(wz):
            d = np.linalg.norm([x,y,z])
            if d < np.pi/2:
                orientation_map[i,j,k] = index
                orientation_vectors.append([x,y,z])
                # print "group:", index, " VeC:", orientation_vectors[-1]
                index+=1
# print orientation_map
# assert(False)
#Fix Colors

#Simulate 100 fksamples
rounded =0
xs = []
ys = []
zs = []
norms = []
for i in range(10000):
    exp = True
    angs = np.random.rand(len(lengths))*np.pi*2 - np.pi
    a.setJointAngs(angs)
    a.calcFK()
    [r1,t1] =kt.hom2ax_vec(a.links_[-1].g_world_end_)
    [r2,t2] =kt.hom2ax_vec(a.links_[-2].g_world_end_)

    xs.append(r1[0])
    ys.append(r1[1])
    zs.append(r1[2])
    norms.append(np.linalg.norm(r1))

    t = t1-t2

    wx_ind = round((r1[0]-wx[0])/dth)
    wy_ind = round((r1[1]-wy[0])/dth)
    wz_ind = round((r1[2]-wz[0])/dth)

    # while np.linalg.norm([dth*wx_ind+wx[0],dth*wy_ind+wy[0],dth*wz_ind+wz[0]])>np.pi:
    #     center = (n-1)/2
    #     wx_ind = one_toward_center(wx_ind,center)
    #     wy_ind = one_toward_center(wy_ind,center)
    #     wz_ind = one_toward_center(wz_ind,center)
    #     exp = False

    if orientation_map[wx_ind,wy_ind,wz_ind] != 0:
        # print "ERROR: Gridded angle rounded to invalid angle"
        # assert(False)
        group_num = orientation_map[wx_ind][wy_ind][wz_ind]
    # groupsinterest = [1,4,7,10,13,18,23,28,31,34,39,44,49,52,55,60,65,70,73,76,79]
    # groupsinterest = [23,28,31,34,39,44]
    # if group_num in groupsinterest:# and exp == True:
        distribution[group_num-1] += 1
        color = colors[group_num]
    # arrow = Arrow3D([0,t[0]],[0,t[1]],[0,t[2]], mutation_scale=20, lw=1, arrowstyle="-|>", color=color)
    # ax.add_artist(arrow)


plt.figure(2)
groups = np.arange(len(distribution))
bins = plt.bar(groups,distribution)
# raw_input("wait")
# width = 0.7 * (bins[1] - bins[0])
# raw_input("wait")
# center = (bins[:-1] + bins[1:]) / 2
plt.bar(groups, distribution)

# plt.title('Plotting Axial Vectors From Random Poses')
# # plt.title('Standard Orientation Vector: (%sx%sx%s) Grid'% (n,n,n) )
# # print orientation_map
# fig = plt.figure(3)
# # ax.scatter(xs,ys,zs,c ='b')
# ax2 = fig.add_subplot(111)
# ax2.hist(norms,bins = 50)
plt.title('Distribution of Poses Rounded To Groups Within Pi/2 Radius')
plt.xlabel('Points in the Axial Discretization')
plt.ylabel('# of Poses at that Point')
plt.show()

