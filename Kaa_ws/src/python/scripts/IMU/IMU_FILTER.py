#!/usr/bin/env python

import os
from ego_byu.tools import pickle_tools as pk
from ego_byu.madgwick import madgwick as md
import numpy as np
import rospy
from sensor_msgs.msg import Imu
import scipy.io as sio
from geometry_msgs.msg import Quaternion
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
from geometry_msgs.msg import Vector3Stamped
from geometry_msgs.msg import TransformStamped
from falkor.msg import robot_state
import message_filters
from threading import RLock
import tf.transformations as tft
import tf2_ros
import time

class IMU_Filter(object):
    def __init__(self,world_frame,gain,mode='USB',publish_option=False,calibrate = True,IMU_Num=0):
        FilterIterations = 10000
        self.IMU_Num = IMU_Num
        self.initialized = False
        self.lock = RLock()
        self.rate = 100.0
        self.Rate = rospy.Rate(self.rate)
        self.dt  = 0.0
        self.cal_path = os.environ['EGO_BYU']+"/python/ego_byu/calibrations/"

        self.Filter = md.ImuFilter()
        self.Filter.setWorldFrame(world_frame)

        self.Filter.setAlgorithmGain(gain)
        self.reversed_ = False #For Rviz Convenience
        self.first_iter = 0
        self.br = tf2_ros.TransformBroadcaster()
        self.q_imuworld_imu = [] #q is x, y z, w (the madgick uses w, x, y, z)

        self.input_mode = mode
        self.calibrate = calibrate

        # #Calibration Option
        if self.calibrate:
            self.StartSubscribers()
            self.start_time = time.time()
            self.cal_time = 30
            self.mag_points = []
            self.calibrate_mag()
        else:

            parameters=pk.load_pickle(self.cal_path+'calibration_imu'+str(self.IMU_Num))
            self.magx_min=parameters['magx_min']
            self.magx_max=parameters['magx_max']
            self.magy_min=parameters['magy_min']
            self.magy_max=parameters['magy_max']
            self.magz_min=parameters['magz_min']
            self.magz_max=parameters['magz_max']
            self.q_imuworld_zero=parameters['q_imuworld_zero']
            self.q_zero_imuworld=tft.quaternion_inverse(self.q_imuworld_zero)

            print "Taking Zero State from 0th IMU Calibration"
            p0 =pk.load_pickle(self.cal_path+'calibration_imu'+str(0))

            self.magx_range=self.magx_max-self.magx_min
            self.magx_mid=self.magx_range/2.0+self.magx_min
            self.magy_range=self.magy_max-self.magy_min
            self.magy_mid=self.magy_range/2.0+self.magy_min
            self.magz_range=self.magz_max-self.magz_min
            self.magz_mid=self.magz_range/2.0+self.magz_min
            self.StartSubscribers()

        #Publishers (For this Module Only)
        self.publish = publish_option
        if self.publish:
            self.mag_pub = rospy.Publisher('/IMU_mag_corrected',Vector3Stamped,queue_size=0)
            self.IMU_pub = rospy.Publisher('/Imu_Orientation',Imu,queue_size=0)

    def StartSubscribers(self):
        # #Subscribers
        if self.input_mode == 'USB':
            pass
            # s1 = message_filters.Subscriber("IMU0/data_raw", Imu)
            # s2 = message_filters.Subscriber("IMU0/mag_raw", Vector3Stamped)
            # ts = message_filters.ApproximateTimeSynchronizer([s1, s2],10,.001)
            # ts.registerCallback(self.callback)
        elif self.input_mode == "ROBOT":
            rospy.Subscriber("robot_state",robot_state,self.callback_from_robot)

    def calibrate_mag(self):
        print "Shake Magnetometer Around for " + str(self.cal_time) + " seconds."
        t = 0
        while t < self.cal_time+1:
            try:
                t = time.time()-self.start_time
                print "Collecting Data: Move IMU(s) around"
                time.sleep(1)

            except KeyboardInterrupt:
                break

        self.mpts=np.array(self.mag_points)
        self.magx_max=np.max(self.mpts[:, 0])
        self.magx_min=np.min(self.mpts[:, 0])
        self.magy_max=np.max(self.mpts[:, 1])
        self.magy_min=np.min(self.mpts[:, 1])
        self.magz_max=np.max(self.mpts[:, 2])
        self.magz_min=np.min(self.mpts[:, 2])
        self.magx_range=self.magx_max-self.magx_min
        self.magx_mid=self.magx_range/2.0+self.magx_min
        self.magy_range=self.magy_max-self.magy_min
        self.magy_mid=self.magy_range/2.0+self.magy_min
        self.magz_range=self.magz_max-self.magz_min
        self.magz_mid=self.magz_range/2.0+self.magz_min
        self.mag_points=[] #Release Data

        fig=plt.figure(1)
        ax = p3.Axes3D(fig)
        ax.scatter3D(self.mpts[:,0],self.mpts[:,1],self.mpts[:,2],c='r')
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        fig.add_axes(ax)
        plt.show()

        self.calibrate = False
        rospy.sleep(.5)

        raw_input("Compute Offsets: Set Arm at Zero State, Then Press Return")
        self.q_imuworld_zero =self.q_imuworld_imu
        self.q_zero_imuworld=tft.quaternion_inverse(self.q_imuworld_zero)

        parameters={'magx_min': self.magx_min,
                      'magx_max': self.magx_max,
                      'magy_min': self.magy_min,
                      'magy_max': self.magy_max,
                      'magz_min': self.magz_min,
                      'magz_max': self.magz_max,
                      'q_imuworld_zero': self.q_imuworld_zero}

        print "IMU " + str(self.IMU_Num) + " paramters:"
        print parameters
        pk.save_pickle(self.cal_path+'calibration_imu'+str(self.IMU_Num),parameters)
        # self.plot_mag_data()

    def plot_mag_data(self):
        answer = raw_input('Would you like to see magnetometer calibration plot? (y/n): ')
        if answer == 'y':
            x = []
            y = []
            z = []
            for i in range(0, 1000):

                print 'Move the gyroscope around in circles until count reaches 0',1000-i
                x.append(self.M_comp_x)
                y.append(self.M_comp_y)
                z.append(self.M_comp_z)
                time.sleep(.01)

            fig=plt.figure(1)
            ax = p3.Axes3D(fig)
            ax.scatter3D(x,y,z,c='r')
            ax.set_xlabel('X')
            ax.set_ylabel('Y')
            ax.set_zlabel('Z')
            fig.add_axes(ax)
            plt.show()
        else:
            return 1

    # def callback(self,msg1,msg2):
    #     self.lock.acquire()

    #     self.A = msg1.angular_velocity
    #     self.Lin_Vel = msg1.linear_acceleration
    #     self.M = msg2.vector

    #     self.time = msg1.header.stamp;
    #     frame_id = msg1.header.frame_id

    #     if self.dt == 0:
    #         self.dt = 1.0/self.rate
    #     else:
    #         self.dt = (self.time - self.last_time).to_sec()
    #     self.last_time = msg1.header.stamp

    #     if self.calibrate == True and len(self.mag_points) < 1000000:
    #         self.mag_points.append([self.M.x,self.M.y,self.M.z])
    #     else:
    #         self.M_comp_x = (self.M.x - self.magx_mid)*200/self.magx_range
    #         self.M_comp_y = (self.M.y - self.magy_mid)*200/self.magy_range
    #         self.M_comp_z = (self.M.z - self.magz_mid)*200/self.magz_range
    #         self.Filter.madgwickAHRSupdate(
    #         self.A.x, self.A.y, self.A.z,
    #         self.Lin_Vel.x, self.Lin_Vel.y, self.Lin_Vel.z,
    #         self.M_comp_x, self.M_comp_y, self.M_comp_z,
    #         self.dt);
    #         self.qs = self.Filter.returnOrientation()
    #         self.q_world_imu = [self.qs[1],self.qs[2],self.qs[3],self.qs[0]]

    #     self.lock.release()

    def callback_from_robot(self,msg):
        self.lock.acquire()

        self.Ang_Vel = msg.sensors[self.IMU_Num].gyr_raw
        self.Lin_Vel = msg.sensors[self.IMU_Num].acc_raw

        #Convention Between Magnetometer and ACC-GYRO Different
        M = msg.sensors[self.IMU_Num].mag_raw
        self.M = [M[1],M[0],-M[2]]

        self.time = msg.header.stamp;
        frame_id = msg.header.frame_id

        if self.dt == 0:
            self.dt = 1.0/self.rate
        else:
            self.dt = (self.time - self.last_time).to_sec()
        self.last_time = msg.header.stamp

        if self.calibrate == True and len(self.mag_points) < 1000000:
            self.mag_points.append([self.M[0],self.M[1],self.M[2]])
        else:
            # self.UpdateBounds()
            self.M_comp_x = (self.M[0] - self.magx_mid)*200.0/self.magx_range
            self.M_comp_y = (self.M[1] - self.magy_mid)*200.0/self.magy_range
            self.M_comp_z = (self.M[2] - self.magz_mid)*200.0/self.magz_range
            # print self.Ang_Vel, self.Lin_Vel, self.M_comp_x, self.dt
            # print self.Ang_Vel, self.Lin_Vel, self.M_comp_x, self.M_comp_y,self.M_comp_z,self.dt
            self.Filter.madgwickAHRSupdate(
            self.Ang_Vel[0], self.Ang_Vel[1], self.Ang_Vel[2],
            self.Lin_Vel[0], self.Lin_Vel[1], self.Lin_Vel[2],
            self.M_comp_x, self.M_comp_y, self.M_comp_z,
            self.dt);
            self.qs = self.Filter.returnOrientation()
            self.q_imuworld_imu = [self.qs[1],self.qs[2],self.qs[3],self.qs[0]]

        self.lock.release()

    def UpdateBounds(self):
        if self.M[0] > self.magx_max:
            self.magx_max = self.M[0]
        if self.M[0] < self.magx_min:
            self.magx_min = self.M[0]
        self.magx_range = self.magx_max-self.magx_min
        self.magx_mid = self.magx_min + self.magx_range/2.0
        if self.M[1] > self.magy_max:
            self.magy_max = self.M[1]
        if self.M[1] < self.magy_min:
            self.magy_min = self.M[1]
        self.magy_range = self.magy_max-self.magy_min
        self.magy_mid = self.magy_min + self.magy_range/2.0
        if self.M[2] > self.magz_max:
            self.magz_max = self.M[2]
        if self.M[2] < self.magz_min:
            self.magz_min = self.M[2]
        self.magz_range = self.magz_max-self.magz_min
        self.magz_mid = self.magz_min + self.magz_range/2.0


    def PublishFilteredMessage(self):
        imu_msg = Imu()
        self.stamp = imu_msg.header.stamp

        q = tft.quaternion_multiply(self.q_zero_imuworld,self.q_imuworld_imu,)
        imu_msg.orientation.w = q[3]

        if self.reversed_:
            imu_msg.orientation.x = -q[0]
            imu_msg.orientation.y = -q[1]
            imu_msg.orientation.z = -q[2]
        else:
            imu_msg.orientation.x = q[0]
            imu_msg.orientation.y = q[1]
            imu_msg.orientation.z = q[2]

        imu_msg.orientation_covariance[0] = 0 #Should be adjusted to be variance
        imu_msg.orientation_covariance[1] = 0
        imu_msg.orientation_covariance[2] = 0
        imu_msg.orientation_covariance[3] = 0
        imu_msg.orientation_covariance[4] = 0
        imu_msg.orientation_covariance[5] = 0
        imu_msg.orientation_covariance[6] = 0
        imu_msg.orientation_covariance[7] = 0
        imu_msg.orientation_covariance[8] = 0
        self.IMU_pub.publish(imu_msg)

    def PublishTransform(self):
        transform = TransformStamped()
        transform.header.stamp = self.stamp
        transform.header.frame_id = "zero_frame"
        transform.child_frame_id = "q_zero_imu"
        q = tft.quaternion_multiply(self.q_zero_imuworld,self.q_imuworld_imu,)
        transform.transform.rotation.w = q[3]
        if self.reversed_:
            transform.transform.rotation.x = -q[0]
            transform.transform.rotation.y = -q[1]
            transform.transform.rotation.z = -q[2]
        else:
            transform.transform.rotation.x = q[0]
            transform.transform.rotation.y = q[1]
            transform.transform.rotation.z = q[2]
        self.br.sendTransform(transform)

        transform = TransformStamped()
        transform.header.stamp = self.stamp
        transform.header.frame_id = "imuworld"
        transform.child_frame_id = "q_imuworld_imu"
        q = self.q_imuworld_imu
        transform.transform.rotation.w = q[3]
        if self.reversed_:
            transform.transform.rotation.x = -q[0]
            transform.transform.rotation.y = -q[1]
            transform.transform.rotation.z = -q[2]
        else:
            transform.transform.rotation.x = q[0]
            transform.transform.rotation.y = q[1]
            transform.transform.rotation.z = q[2]
        self.br.sendTransform(transform)

    def PublishMagCorrected(self):
        msg = Vector3Stamped()
        msg.vector.x = self.M_comp_x
        msg.vector.y = self.M_comp_y
        msg.vector.z = self.M_comp_z
        self.mag_pub.publish(msg)

    def Run(self):
        while not rospy.is_shutdown():
            if len(self.q_imuworld_imu)>0:
                if self.first_iter == 0:
                    print "Filtering Data"
                    self.first_iter = 1
                if self.publish:
                    self.PublishFilteredMessage()
                    self.PublishTransform()
                    self.PublishMagCorrected()
                    self.Rate.sleep()
            else:
                rospy.sleep(1)

if __name__ == "__main__":
    print "Starting IMU Filter"
    rospy.init_node("My_Node")

    gain = .5
    world_frame = md.NWU
    my_mode = "ROBOT"
    calibrate_state = False
    IMUNUM = 2
    imu_reader = IMU_Filter(world_frame,gain,publish_option=True,mode=my_mode,calibrate=calibrate_state,IMU_Num=IMUNUM)
    imu_reader.Run()

