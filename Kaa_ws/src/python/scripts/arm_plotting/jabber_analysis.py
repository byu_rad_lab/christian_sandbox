#!/usr/bin/env python

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # NOQA

import numpy as np

from ego_core.kinematics import kinematics as kin
from ego_byu.tools import arm_plotter as ap
from ego_pneubo.vis_tools import qpik

def create_jabberwock():
    height = 0.185
    joint_half_length = 0.20
    ax45 = np.array([-np.pi / 4.0, 0.0, 0.0])
    vec = np.array([0.0, 0.0, joint_half_length])
    hom_trans = kin.HomFromAxVec(np.zeros(3), vec)
    hom_45 = kin.HomFromAxVec(ax45, vec)
    hom_offset = np.dot(hom_45, hom_trans)

    link_offsets = []
    link_offsets.append(hom_offset)
    link_offsets.append(hom_offset)
    link_offsets.append(kin.HomTranslate(0.0, 0.0, 0.05))  # link 2
    limits = np.array([56.0, 90.0, 90.0]) * np.pi / 180.0

    center = np.zeros(2)
    phase = 0.0
    num_segments = 8

    arm = kin.Arm()
    for i in range(len(limits)):
        joint = kin.JointCCC(height)
        joint.limits().setLimitsCircular(limits[i], center, num_segments, phase)
        print "LIMITS"
        print joint.limits_lower_
        print joint.limits_upper_
        link = kin.Link(joint)
        # should be a method of link, but only allows x, y, z setting
        link.g_top_end_ = link_offsets[i]
        arm.addLink(link)
    return arm


# create an arm
arm = create_jabberwock()
arm.setJointAngsZero()
arm.calcFK()

qpik = qpik.QPIK(arm)
qpik.max_iter = 200
qpik.error_improv_tol = 5.0e-6
qpik.error_term_tol = 1.0e-3
qpik.max_step = 0.25  # no effect
qpik.step_scale = 0.8  # helps a lot, but maybe instabilities?

ax = np.array([-np.pi / 2.0, 0.0, 0.0])
vec = np.array([0.4, 0.8, 0.5])
goal = kin.HomFromAxVec(ax, vec)
# goal = kin.HomTranslate(0.0, 0.0, 1.0)

fig = plt.figure()
ax = fig.gca(projection='3d')

ap.plot_gnomon(ax, goal)

# create plotter and plot
arm_plotter = ap.ArmPlotter(arm)
arm_plotter.plot(ax)

# solve and plot again
qpik.solve(goal)
print "Error Total:", qpik.error_tot
# print "Num Steps:", qpik.steps_taken
arm_plotter.plot(ax)

# labels and such
ap.set_axes_equal(ax)
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
plt.show()
