#!/usr/bin/env python
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import constructors
from ego_byu.tools import arm_plotter as ap
from ego_byu.tools import qpik
from tf2_msgs.msg import TFMessage as tf
import tf.transformations as tft
from nasa_mpc.msg import angles_velocity as angles
import rospy
import numpy as np
import matplotlib.pyplot as plt
import time

class Servo(object):

    def __init__(self, model, true_arm=[]):
        n = len(arm.getJointAngsReal())
        self.q = np.zeros([n,1])
        self.qdot = np.zeros([n,1])

        self.qpik = qpik.QPIK_SINGLE_ARM(model,True)
        self.max_ang_vel = np.pi/180*10
        self.g_tool_vive1 = np.eye(4)

        self.g_cam_vive1 = []
        self.g_cam_vive2 = []

        self.distance = float("inf")
        self.figure = plt.figure(1)
        self.ax = self.figure.gca(projection="3d")
        plt.ion()


        rospy.Subscriber("/tf", tf, self.vive_callback)

        #If no true arm is passed, we look for joint angles from joint_estimation
        #If an arm is passed, we use this to determine truth
        self.true_arm = true_arm
        if self.true_arm == []:
            rospy.Subscriber("/robot_angles", angles, self.angles_callback)

    def vive_callback(self, msg):
        if msg.transforms[0].child_frame_id == "controller1":
            self.g_cam_vive1 = self.assign(msg.transforms[0].transform)

        if msg.transforms[0].child_frame_id == "controller2":
            self.g_cam_vive2 = self.assign(msg.transforms[0].transform)

    def angles_callback(self, msg):
        self.q[0,0] = msg.q[0]
        self.q[1,0] = msg.q[1]
        self.q[2,0] = msg.q[2]
        self.q[3,0] = 0  # Need to be reassigned when we're ready
        self.q[4,0] = 0
        self.q[5,0] = 0

        self.qdot[0,0] = msg.qdot[0]
        self.qdot[1,0] = msg.qdot[1]
        self.qdot[2,0] = msg.qdot[2]
        self.qdot[3,0] = 0  # see above
        self.qdot[4,0] = 0
        self.qdot[5,0] = 0
        self.got_estimates = True


    # Step towards desired twist
    def step(self):
        self.CheckVive()

        # Find Desired Step and New Angles
        self.qpik.angs = self.q
        g_tool_vive2 = self.g_tool_vive1.dot(kt.homog_inv(self.g_cam_vive1).dot(self.g_cam_vive2))
        self.distance = np.linalg.norm(g_tool_vive2[:3,3])
        self.qpik.step(g_tool_vive2)
        self.q_goal = self.qpik.angs

        # Limit Velocity
        if np.amax(self.qpik.angs_delta) > self.max_ang_vel:
            self.qdot_goal = self.qpik.angs_delta/(np.amax(self.qpik.angs_delta))*self.max_ang_vel
        else:
            self.qdot_goal = self.qpik.angs_delta

        #Command and Step
        self.true_arm.setJointAngsReal(self.q_goal)
        self.true_arm.calcFK()

        #Plot Stuff
        self.ax.cla()
        plotter1 = ap.SimpleArmPlotter(model)
        plotter2 = ap.SimpleArmPlotter(model)
        plotter1.plot(self.ax)
        plotter2.plot(self.ax)
        ap.plot_gnomon(self.ax,np.eye(4))

        g_world_vive1 = self.true_arm.getGWorldTool().dot(self.g_tool_vive1)
        g_world_vive2 = g_world_vive1.dot(kt.homog_inv(self.g_cam_vive1).dot(self.g_cam_vive2))
        ap.plot_gnomon(self.ax,np.eye(4))
        ap.plot_gnomon(self.ax,g_world_vive1,color=['r','r','r'])
        ap.plot_gnomon(self.ax,g_world_vive2,color=['b','b','b'])

        ap.set_axes_equal(self.ax)
        ap.fix_axes(self.ax,[-.5,1.5],[-.5,1.5],[0,2.0])

        self.figure.gca().view_init(elev=35, azim=-60)
        self.figure.canvas.draw()

        # raw_input("")

    def continuously_step(self):
        plt.figure(1)
        plt.show()
        while not rospy.is_shutdown() and self.distance > .10:
            self.step()

    def CheckVive(self):
        Count = 0
        while( self.g_cam_vive1 == [] or self.g_cam_vive2 == []):
            print "Waiting for estimates"
            time.sleep(1)
            Count+=1
            if Count > 2:
                print "Failed to get controller data"
                Assert(False)


    def assign(self, t):
        quat = [t.rotation.x,t.rotation.y,t.rotation.z,t.rotation.w]
        trans = [t.translation.x,t.translation.y,t.translation.z]
        R0 = tft.quaternion_matrix(quat)[0:3, 0:3]
        g = np.eye(4)
        g[:3, :3] = R0
        g[:3, 3] = trans
        return g

if __name__ == "__main__":
    rospy.init_node("Servoer")

    #Model Robot
    base = constructors.BaseFixed()
    arm = constructors.NasaArms(1,base)[0]

    #Real Robot
    base = constructors.BaseFixed()
    model = constructors.NasaArmsTrue(1,base)[0]

    servo = Servo(model,arm)
    servo.continuously_step()

