#!/usr/bin/env python
import numpy as np
from ego_byu.tools import constructors
from ego_byu.tools import paths

import Servo
import rospy
from nasa_mpc.msg import angles_velocity as angles
from nasa_mpc.msg import commands
# from nasa_mpc.msg import robot_ready
import matplotlib.pyplot as plt
# from PlannerGUI import PlannerGUI
from scripts.path_planner.QPIK import PlanQPIK
import time
from copy import deepcopy


class PlanPublisher(object):

    def __init__(self):
        self.total_time = 10
        # self.distance_to_path_tol = 20
        # print "robot_callback"
        self.robot_type = "NightCrawler"
        self.Ready = []
        self.path_started = False
        self.flag = True  # look for interruptions
        self.start_time = 0
        self.time_stalled = 0
        self.waiting_message = True

        # begin plans to first configuraiton from current estimate.
        self.PlannerToFirstPose = []

        # Estimate Subscriber
        jt_num = 6
        self.q_ = [0.]*jt_num
        self.qdot_ = [0.]*jt_num
        self.got_estimates = False
        rospy.Subscriber("/robot_angles", angles, self.angles_callback)

        # Command Publisher
        self.rate = rospy.Rate(100)
        self.state_pub = rospy.Publisher('/jt_commands', commands, queue_size=100)

        # Finds Initial Configuration
        self.des_path = paths.LineY
        self.InitializePlanner()

        self.g_camera_vive1 = []
        self.g_camera_vive2 = []

    def angles_callback(self, msg):
        self.q_[0] = msg.q[0]
        self.q_[1] = msg.q[1]
        self.q_[2] = msg.q[2]
        # self.q_[0] = 0
        # self.q_[1] = -np.pi/2.0
        # self.q_[2] = 0
        self.q_[3] = 0  # Need to be reassigned when we're ready
        self.q_[4] = 0
        self.q_[5] = 0

        self.qdot_[0] = msg.qdot[0]
        self.qdot_[1] = msg.qdot[1]
        self.qdot_[2] = msg.qdot[2]
        self.qdot_[3] = 0  # see above
        self.qdot_[4] = 0
        self.qdot_[5] = 0
        self.got_estimates = True

    def InitializePlanner(self):
        # Get Joint Angles
        while (self.got_estimates is False):
            try:
                print "Waiting For Estimates"
                rospy.sleep(.1)
            except rospy.ROSInterruptException:
                assert(False)

        # Check Joint Angles
        if np.any(np.array(map(abs, self.q_)) > 2.0):
            print self.q_
            print "Bad Joint Angles Obtained. Check Cortex"
            assert(False)

        # Input initial condition into Planner
        if self.robot_type == "NightCrawler":
            self.base = constructors.BaseFixedCortex()
            self.narms = 20  # Number and type of arms in high level planner
            self.arms = constructors.NasaArms(self.narms, self.base, np.array(self.q_))

            # Path Chosen
            arm = constructors.NasaArms(1, self.base)[0]
            arm.setJointAngsReal(np.array(self.q_))
            arm.calcFK()

            h1 = arm.getGWorldTool()
            h2, wv, wr = self.des_path(0)

            path_from_current_pose = paths.CombinePaths(paths.LineBetween(h1, h2), self.des_path)
            self.constraints = []
            self.planner = PlanQPIK.Planner(
                path_from_current_pose,
                self.arms,
                self.base,
                self.constraints,
                True,
                self.q_)
            self.planner.smoothpts = 100
            self.planner.ref_tol = 100  # (off)

        else:
            print "Robot Type Not Recognized"
            assert(False)
        print "Planner Initialized"

    def PlanFromCurrentPosition(self):
        self.InitializePlanner()
        self.planner.Plan()
        self.planner.ScaleFinalPathTimes(self.total_time)

    def SamplePath(self):
        self.PlanFromCurrentPosition()
        self.planner.LaunchGUI()
        plt.show()

    def ManipulatorIsOffTrack(self, q_command):
        # Check if Commands and angles are far, if so, replan
        dist = np.linalg.norm(np.array(self.q_)-np.array(q_command))
        if dist < self.distance_to_path_tol:
            return False
        else:
            return True

    def run(self):
        self.PlanFromCurrentPosition()

        raw_input("Press Return To Begin Sending Commmands To Controller")
        print "Path Started"
        self.start_time = time.time()

        # Start Sending Commands
        times = self.planner.FinalPathTimes

        while not rospy.is_shutdown():

            # Find where time is in path
            t = time.time()-self.start_time
            i = np.argmax(times > t)
            if t > times[-1]:
                i = len(times)-1

            # Safety Checks
            if i < 0 or i > len(times)-1:
                print "index found out of range of valid indices"
                assert(False)

            # Find Command
            q_command = self.planner.FinalPathConfigs[i].tolist()
            qdot_command = self.planner.FinalPathVels[i].tolist()

            msg = commands()
            msg.q_goal = q_command
            msg.qdot_goal = qdot_command
            self.state_pub.publish(msg)
            print "time on path: ", t, "index of command: ", i, " QCommands: ", q_command
            # if self.ManipulatorIsOffTrack(q_command):
            #     print "Manipulator failed to follow trajectory, give more time to the planner"
            #     break
            if not self.ManipulatorIsOffTrack(q_command) and t > times[-1]:
                print "Planned Path Completed"
                break
            self.rate.sleep()

        print "Beginning Servo From Vive1 to Vive2"
        servo = Servo(deepcopy(self.planner.arms[0]))
        while not rospy.is_shutdown():
            servo.q = self.q_
            servo.step()

            msg = commands()
            msg.q_goal = servo.q_goal
            msg.qdot_goal = servo.qdot_goal
            self.state_pub.publish(msg)

            if np.linalg.norm(servo.angs_delta) < .05:
                print "Objective Achieved"
                break
            self.rate.sleep()


if __name__ == "__main__":
    rospy.init_node('planner', anonymous=False)
    planner = PlanPublisher()
    # planner.SamplePath()
    planner.run()
