#!/usr/bin/env python
import numpy as np
import copy
# from ego_byu.tools import kin_tools as kt
# from ego_byu.tools import arm_plotter as ap
import matplotlib.pyplot as plt

#Read in File
ft_data_path = './Data/TestData/Fy/data_ftnode1.txt'

ft_data = np.genfromtxt(ft_data_path,delimiter = ",",skip_footer = 1,skip_header = 1)

Fx = ft_data[:, 2]
Fy = ft_data[:, 3]
Fz = ft_data[:, 4]
Tx = ft_data[:, 5]
Ty = ft_data[:, 6]
Tz = ft_data[:, 7]
bias = np.mean(Fy[:200])
meas = np.mean(Fy[900:1000])
print 'bias', bias
print 'meas-bias',meas-bias

plt.figure()
plt.plot(Fy)
plt.show()

