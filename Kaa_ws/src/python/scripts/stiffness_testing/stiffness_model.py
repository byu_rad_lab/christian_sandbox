#!/usr/bin/env python
import numpy as np
import copy
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import arm_plotter as ap
from ego_byu.tools import data_processor as dp
import matplotlib.pyplot as plt
from scipy.optimize import minimize

#Psuedo Inverse
def linear_regression(thetas,torques):
    K = np.zeros([3,3])
    temp = np.dot(torques,np.transpose(thetas))
    K = np.dot(temp ,np.linalg.inv(np.dot(thetas,np.transpose(thetas))))
    return K

def bfgs(thetas,torques,constrained = False):
    K_bfgs = np.zeros([3,3])
    if constrained == True:
        x =np.array([20,235,0])
        res = minimize(objective_function_constrained_diag,x,args=(thetas,torques,),method='BFGS',options={'disp':True})
        K_bfgs[0,0] = res.x[0]
        K_bfgs[1,1] = res.x[1]
        K_bfgs[2,2] = res.x[2]
    else:
        x =np.array([20,0,0,0,235,0,0,0,40])
        res = minimize(objective_function,x,args=(thetas,torques,),method='BFGS',options={'disp':True})
        K_bfgs = np.resize(res.x,[3,3])
    return K_bfgs

def rms_error(K,thetas,torques):
    n = len(torques[0][:])
    #Compute error
    error = 0
    avg_torque = 0
    torques_p = np.zeros([3,n])
    for i in range(n):
        torques_p[:,i] = np.dot(K,thetas[:,i])
    diff = torques-torques_p
    rms = np.linalg.norm(np.ndarray.flatten(diff))/np.linalg.norm(np.ndarray.flatten(torques))
    return rms

def plot_link_motion(data,indices,ax):
    #Plot
    for i in indices:
        g_world_end = kt.ax_vec2homog(kt.axfromquat(data[i,5:]),data[i,2:5])
        trans_hom = kt.homog_translate(0,0,-.34)
        g_world_top = np.dot(g_world_end,trans_hom)
        ap.plot_gnomon(ax, g_world_end)  # world frame
        ap.plot_gnomon(ax, g_world_top)  # world frame
        # ap.plot_cylinder(ax,g_world_end, g_world_top, r=0.05, color='k', n=20)

def objective_function(x,thetas,torques):
    error = 0
    K = np.zeros([3,3])
    K[0,:] = [x[0],x[1],x[2]]
    K[1,:] = [x[3],x[4],x[5]]
    K[2,:] = [x[6],x[7],x[8]]
    torques_p = np.zeros([3,len(thetas[0,:])])

    for i in range(len(thetas[0,:])):
        th = thetas[:,i]
        tau = torques[:,i]
        torques_p[:,i] = np.dot(K,th)
        error = error+np.linalg.norm(torques[:,i]-torques_p[:,i])

    error = np.sqrt(error/len(torques[0,:]))
    return error

def objective_function_constrained_diag(x,thetas,torques):
    error = 0
    K = np.zeros([3,3])
    K[0,0] = x[0]
    K[1,1] = x[1]
    K[2,2] = x[2]

    torques_p = np.zeros([3,len(thetas[0,:])])

    for i in range(len(thetas[0,:])):
        th = thetas[:,i]
        tau = torques[:,i]
        torques_p[:,i] = np.dot(K,th)
        error = error+np.linalg.norm(torques[:,i]-torques_p[:,i])

    error = np.sqrt(error/len(torques[0,:]))
    return error

if __name__ == "__main__":
    np.set_printoptions(precision = 3)
    #Read in File
    base_data_path = './Data/TestData/SampleData/data_blackGrub_base.txt'
    link_data_path = './Data/TestData/SampleData/data_blackGrub_link.txt'
    ft_data_path = './Data/TestData/SampleData/data_ftnode1.txt'
    # base_data_path = './Data/0_Degrees/60000kpa/data_blackGrub_base.txt'
    # link_data_path = './Data/0_Degrees/60000kpa/data_blackGrub_link.txt'
    # ft_data_path = './Data/0_Degrees/60000kpa/data_ftnode1.txt'

    base_data = np.genfromtxt(base_data_path,delimiter = ",",skip_footer = 1,skip_header = 1)
    link_data = np.genfromtxt(link_data_path,delimiter = ",",skip_footer = 1,skip_header = 1)
    ft_data = np.genfromtxt(ft_data_path,delimiter = ",",skip_footer = 1,skip_header = 1)

    data = [base_data,link_data,ft_data]

    #PROCESS DATA
    #Delete duplicates and synchronize data by time_steps
    # rem_indices = np.hstack([np.arange(525,575),np.arange(900,1000),np.arange(1100,1200),np.arange(1450,1500),np.arange(1600,1750),np.arange(1950,2050),np.arange(2100,2125),np.arange(2270,2290),np.arange(2375,2425),np.arange(2575,2620)])
    rem_indices = np.hstack([0])
    [base_data,link_data,ft_data] = dp.process_data(data,rem_indices)

    #PLOT LINK
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    # ap.plot_floor(ax,.5)
    link_length1 = .36

    #BASE LINK
    base_hom1 = kt.ax_vec2homog(kt.axfromquat(base_data[0,5:]),base_data[0,2:5])
    trans_hom1 = kt.homog_translate(0,0,link_length1)
    link_base_hom1 = np.dot(base_hom1,trans_hom1)
    r1,t1 = kt.hom2ax_vec(link_base_hom1)
    # ap.plot_gnomon(ax, np.eye(4))  # world frame
    ap.plot_gnomon(ax, base_hom1)  # world frame
    ap.plot_gnomon(ax, link_base_hom1)  # world frame
    ap.plot_cylinder(ax,base_hom1, link_base_hom1, r=0.05, color='k', n=80)

    #Adjusted Joint Center that Matches Preliminary Data
    r = np.array([0,.05,.02])
    t = np.array([.02,0,.01])
    g_adj = kt.ax_vec2homog(r,t)
    g_world_jc = np.dot(link_base_hom1,g_adj)
    ap.plot_gnomon(ax, g_world_jc,.1,5,['k','k','k'])

    #2nd LINK
    indices = np.arange(0,len(link_data),50)
    # indices = np.array([1])
    plot_link_motion(link_data,indices,ax)

    #Move Wrench, Collect Data for Regression
    n = len(ft_data)
    thetas = np.zeros([3,n])
    torques = np.zeros([3,n])
    torques_untransformed = np.zeros([3,n])
    torques_end = np.zeros([3,n])
    torques_p = np.zeros([3,n])
    torques_bfgs = np.zeros([3,n])

    wrench_ft = np.zeros([6,n])
    wrench_end = np.zeros([6,n])
    wrench_jc = np.zeros([6,n])
    bias = np.mean(ft_data[0:10,2:],axis=0)

    for i in range(n):
        #Rotate Wrench Into End Frame
        wrench_ft[:,i] = ft_data[i,2:]-bias
        g_world_end = kt.ax_vec2homog(kt.axfromquat(link_data[i,5:]),link_data[i,2:5])
        r = np.array([0,0,np.pi/1.7])
        t = np.zeros(3)
        g_end_ft = kt.ax_vec2homog(r,t)
        wrench_end[:,i] = np.dot(kt.homog2adj(kt.homog_inv(g_end_ft)).T,wrench_ft[:,i])

        #Transform into JC frame
        g_world_ft = np.dot(g_world_end,g_end_ft)
        g_jc_ft = np.dot(kt.homog_inv(g_world_jc),g_world_ft)
        g_jc_end = np.dot(kt.homog_inv(g_world_jc),g_world_end)
        wrench_jc[:,i] = np.dot(kt.homog2adj(kt.homog_inv(g_jc_end)).T,wrench_end[:,i])

        #Collect torques and thetas
        g_jc_end = np.dot(kt.homog_inv(g_world_jc),g_world_end)
        r,t = kt.hom2ax_vec(g_jc_end)
        thetas[:,i] = r
        torques[:,i] = wrench_jc[3:,i]

    #Remove bias
    bias_th = np.mean(thetas[:,0:50],axis=1)
    for i in range(n):
        thetas[:,i]=thetas[:,i]-bias_th

    ######################### LINEAR MODEL FITTING #####################################
    #Regression
    K = linear_regression(thetas,torques)
    print "PSEUDO INVERSE MODEL"
    print K

    rms = rms_error(K,thetas,torques)
    print "RMS Normalized"
    print rms

    # #BFGFS Algorithm
    # K_bfgs = bfgs(thetas,torques,constrained = False)
    # print "BFGS FIT MODEL"
    # print K_bfgs

    # rms = rms_error(K_bfgs,thetas,torques)
    # print "RMS Normalized"
    # print rms

    #BFGFS Algorithm
    # K_bfgs_c = bfgs(thetas,torques,constrained = True)
    # print "Constrained BFGS FIT MODEL"
    # print K_bfgs_c

    # rms = rms_error(K_bfgs_c,thetas,torques)
    # print "RMS Normalized"
    # print rms

    #Compute Predicted Torques for Plotting
    for i in range(len(torques_p[0,:])):
        torques_p[:,i] = np.dot(K,thetas[:,i])

    #Compute Predicted Torques for Plotting
    # for i in range(len(torques_bfgs[0,:])):
    #     torques_bfgs[:,i] = np.dot(K_bfgs,thetas[:,i])


    ############################### PLOTTING ###################################
    #Axis start and finish
    b = 0
    e = 10000

    ap.set_axes_equal(ax)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.title("Joint Center")

    plt.figure()
    plt.subplot(411)
    plt.plot(torques[0,:],'-r')
    plt.plot(torques[1,:],'-b')
    plt.plot(torques[2,:],'-g')
    plt.legend(['tau_x','tau_y','tau_z'])
    plt.axis([b,e,-100,100])
    plt.title('Measured Torques About Joint Center')

    plt.subplot(412)
    plt.plot(torques_p[0,:],'-r')
    plt.plot(torques_p[1,:],'-b')
    plt.plot(torques_p[2,:],'-g')
    plt.axis([b,e,-100,100])
    # plt.plot(torques_bfgs[0,:],'--r')
    # plt.plot(torques_bfgs[1,:],'--b')
    # plt.plot(torques_bfgs[2,:],'--g')
    # plt.legend(['tau_x','tau_y','tau_z'])
    plt.title('Predicted Torques About Joint Center (Linear Regression)')

    plt.subplot(413)
    plt.plot(torques[0,:]-torques_p[0,:],'-r')
    plt.plot(torques[1,:]-torques_p[1,:],'-b')
    plt.plot(torques[2,:]-torques_p[2,:],'-g')
    plt.axis([b,e,-100,100])
    # plt.legend(['tau_x','tau_y','tau_z'])
    plt.title('Error')

    plt.subplot(414)
    plt.plot(thetas[0,:],'-r')
    plt.plot(thetas[1,:],'-b')
    plt.plot(thetas[2,:],'-g')
    plt.axis([b,e,-1,1])
    # plt.legend(['wx','wy','wz'])
    plt.title('Axial Angle Components of Transformation g_jc_end')


    def plot_wrench(wrench):
        plt.plot(wrench[0,:],'-r')
        plt.plot(wrench[1,:],'-b')
        plt.plot(wrench[2,:],'-g')
        plt.plot(wrench[3,:],'-m')
        plt.plot(wrench[4,:],'-k')
        plt.plot(wrench[5,:],'-y')
        # plt.legend(['Fx','Fy','Fz','T_x','T_y','T_z'])
    plt.figure()
    plt.subplot(311)
    plt.title("wrench ft")
    plot_wrench(wrench_ft)
    plt.subplot(312)
    plt.title("wrench end")
    plot_wrench(wrench_end)
    plt.subplot(313)
    plt.title("wrench jc")
    plot_wrench(wrench_jc)

    plt.show()
