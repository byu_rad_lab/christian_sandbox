#!/usr/bin/env python

# This File Creates an arm, applies a wrench and predicts deflection using a user-input stiffness matrix.
# Noise is then simulated in the pure data and the linear model is reconstructed from the noisy data.
import numpy as np
import copy
from ego_byu.tools import arm_tools as at
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import arm_plotter as ap
import matplotlib.pyplot as plt
import stiffness_model as sm
import random


#BUILD ARM
jt_height = .02
lengths = np.array([.35])
angles = np.array([0])
base_hom = np.identity(4)
stiffness=np.array([40,20,235])

arm = at.nasa_arm(jt_height,lengths,base_hom,stiffness)

np.set_printoptions(precision=5)
np.set_printoptions(suppress=True)
arm.calcJac()
arm.calcStiffness()

#Print Preliminaries
print "Joint Angs"
print arm.joint_angs_

print "Joint Stiffness"
print np.diag(stiffness)

print "Hybrid Jacobian"
print arm.getJacHybrid()

print "Hybrid Flexibility"
print arm.getFlexibilityHybrid()

#Plot Data
base_hom = np.eye(4)
g_world_hybrid_ = np.eye(4)
g_world_hybrid_[0:3,3] = arm.getGArmTool()[0:3,3]
fig = plt.figure()
ax = fig.gca(projection='3d')
ap.plot_gnomon(ax, arm.g_world_arm_)  # world frame
ap.plot_gnomon(ax, g_world_hybrid_)  # world frame
# ap.plot_cylinder(ax,arm.g_world_arm_, g_world_hybrid_, r=0.05, color='k', n=80)
ax.scatter(0,0,0,s=1000,c='k')
ap.set_axes_equal(ax)

n = 10
Fx = np.arange(0,81,20)
# Fx = np.array([0])
Fy = np.arange(0,28,2)
# Fy = np.array([0])
Tz = np.arange(0,5,.5)
# Tz = np.array([0])

twist = np.zeros([6,len(Fx)*len(Fy)*len(Tz)])
thetas = np.zeros([3,len(Fx)*len(Fy)*len(Tz)])
thetas_noisy = np.zeros([3,len(Fx)*len(Fy)*len(Tz)])
torques = np.zeros([3,len(Fx)*len(Fy)*len(Tz)])
torques_noisy = np.zeros([3,len(Fx)*len(Fy)*len(Tz)])


ind = 0
thn = np.pi/36
tht = 3
for i in Fx:
    for j in Fy:
        for k in Tz:
            wrench_hybrid = np.array([i,j,0,0,0,k])
            wrench_jc = np.dot(kt.homog2adj(kt.homog_inv(g_world_hybrid_)).T,wrench_hybrid)

            twist[:,ind] = np.dot(arm.getFlexibilityHybrid(),wrench_hybrid)
            g_world_DisplacedArm = np.dot(g_world_hybrid_,kt.twist2homog(twist[:,ind]))
            r,t = kt.hom2ax_vec(g_world_DisplacedArm)
            thetas[:,ind] = r
            thetas_noisy[:,ind] = r+np.array([random.uniform(-thn,thn),random.uniform(-thn,thn),random.uniform(-thn,thn)])
            torques[:,ind] = wrench_jc[3:]
            torques_noisy[:,ind] = wrench_jc[3:]+np.array([random.uniform(-tht,tht),random.uniform(-tht,tht),random.uniform(-tht,tht)])
            ap.plot_gnomon(ax, g_world_DisplacedArm)  # world frame
            ap.plot_line(ax,[0,0,0],g_world_DisplacedArm[0:3,3],'k',2)
            ind+=1
plt.show()


#RECOVER STIFFNESS MATRICES
#No Noise
K = sm.linear_regression(thetas,torques)
print "Psuedo Inverse Stiffness:No Noise"
print K
rms = sm.rms_error(K,thetas,torques)
print "RMS Normalized"
print rms

#Noise
K_noisy = sm.linear_regression(thetas_noisy,torques_noisy)
print "Psuedo Inverse Stiffness:No Noise"
print K_noisy
rms = sm.rms_error(K_noisy,thetas,torques)
print "RMS Normalized"
print rms

K_bfgs = sm.bfgs(thetas,torques)
print "BFGS Stiffness"
print K_bfgs
rms = sm.rms_error(K_bfgs,thetas,torques)
print "RMS Normalized"
print rms

plt.figure()
plt.title("Computed Thetas")
plt.subplot(311)
plt.title('torques')
plt.plot(torques[0,:],'-r')
plt.plot(torques[1,:],'-g')
plt.plot(torques[2,:],'-b')
plt.legend(['tau_x','tau_y','tau_z'])

plt.subplot(312)
plt.title('thetas')
plt.plot(thetas[0,:],'-r')
plt.plot(thetas[1,:],'-g')
plt.plot(thetas[2,:],'-b')
plt.legend(['w_x','w_y','w_z'])

plt.subplot(313)
plt.title('thetas_noisy')
plt.plot(thetas_noisy[0,:],'-r')
plt.plot(thetas_noisy[1,:],'-g')
plt.plot(thetas_noisy[2,:],'-b')
plt.show()








