#!/usr/bin/env python

# This File Creates an arm, applies a wrench and predicts deflection using a user-input stiffness matrix.
# Noise is then simulated in the pure data and the linear model is reconstructed from the noisy data.
import numpy as np
import copy
from ego_byu.tools import arm_tools as at
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import arm_plotter as ap
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import stiffness_model as sm
import matplotlib.patches as mpatches
import random


#BUILD ARM
jt_height = .02
lengths = np.array([.23,.23,.23,.23,.23,.23])
# lengths = np.array([.20,.20])
base_hom = kt.ax_vec2homog(np.array([np.pi/2,0,0]),np.array([0,0,0]))
stiffness=np.array([80,1000,235])
arm = at.nasa_arm(jt_height,lengths,base_hom,stiffness)

np.set_printoptions(precision=5)
np.set_printoptions(suppress=True)

#SET JOINT ANGLES
jt_angs = np.zeros([len(lengths)*3,1])
limit = np.pi/2
for i in arm.getRealJointIdx():
    jt_angs[int(i)] = np.random.random()*2*limit-limit


arm.setJointAngs(jt_angs)
arm.calcJac()
arm.calcStiffness()

#PREDICT MAX FORCE LIMIT DISPLACEMENT
direction = np.array([0,0,-1.0])
delta_max = .2
limit = arm.calcMaxForceLimitDisplacement(direction,delta_max)

#Print Preliminaries
# print "Joint Angs"
# print arm.joint_angs_

# print "Joint Stiffness"
# print np.diag(stiffness)

# print "Hybrid Jacobian"
# print arm.getJacHybrid()

# print "Hybrid Flexibility"
# print arm.getFlexibilityHybrid()
# for i in range(len(arm.links_)):
# raw_input("break")

#APPLY EXTERNAL WRENCH IN HYBRID FRAME
force = np.array([0.0,0,-limit])
torque = np.array([0,0,0])
wrench = np.hstack([force,torque])

#Set External Force and Torque from Gravity
jt_torques = arm.calcTorqueJointStatic(wrench)

#Plot (Not Displaced)
fig = plt.figure()
ax = fig.gca(projection='3d')

arm.setJointAngs(jt_angs)

ap.plot_arm_with_jt_torques(ax,arm,np.zeros([3*len(lengths),1]),stiffness,force,color = 'k')
center = arm.getGWorldTool()[:3,3]
ap.plot_sphere(ax,center,delta_max,'g')

arm.setJointAngs(jt_angs)
ap.plot_arm_with_jt_torques(ax,arm,arm.getTorqueGravity(),stiffness,force,color = 'b')
center2 = arm.getGWorldTool()[:3,3]

arm.setJointAngs(jt_angs)
ap.plot_arm_with_jt_torques(ax,arm,jt_torques,stiffness,force,color = 'r')
center3 = arm.getGWorldTool()[:3,3]

# print "MAX ALLOWABLE FORCE TO STAY UNDER DEFLECTION TOLERANCE"
# print "DIRECTION: ", direction
# print "LIMIT: ", limit

# delta_ext = np.dot(arm.getFlexibilityHybrid(),wrench)
# delta_grav = np.dot(np.dot(arm.getJacHybrid(),arm.getFlexibilityGlobal()),arm.getTorqueGravity())
# print "GRAVITY ONLY"
# print "Kdth Method Displacement: ", np.linalg.norm(center2-center)
# print "Jac Method: Displacement ", np.linalg.norm(delta_grav[0:3])

# print "LOAD and GRAVITY"
# print "Kdth Method Displacement: ", np.linalg.norm(center3-center)
# print "Jac Method: Displacement ", np.linalg.norm(delta_ext[0:3]+delta_grav[0:3].T)

ap.set_axes_equal(ax)
plt.xlabel('x')
plt.ylabel('y')
plt.title('Deflection of Joint With Infinite Stiffness in Active Directions \n(Black = Nominal Position, Blue = Gravity, Red = Gravity + Critical Load')
plt.show()







