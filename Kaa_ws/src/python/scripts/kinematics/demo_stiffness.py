#!/usr/bin/env python

import matplotlib.pyplot as plt
from ego_core.kinematics import kinematics as kin
from ego_byu.armeval import armeval as ae
from ego_byu.tools import arm_tools as at
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import arm_plotter as ap
import numpy as np

lengths = np.empty(6)
lengths.fill(.2)
lengths = np.array([.070,.2,.242,.012,.216,.115])

# r = np.array([-np.pi/2,0,0])
r = np.array([-np.pi*2/5,0,0])
# t = np.array([0,0,1.5])
t = np.array([0,0,1])

base_hom = kt.ax_vec2homog(r,t)
base_hom[1][1] = -.651
base_hom[1][2] = .758
base_hom[2][1] = -.758
base_hom[2][2] = -.65
jt_height = .06

stiffness = np.array([300.0,300.0,300.0])
arm = at.nasa_arm(jt_height,lengths,base_hom,stiffness)

angs = np.array([])
for j in range(len(arm.lengths)):
    angs = np.append(angs,0)
    # angs = np.append(angs,np.random.rand()*np.pi-np.pi/2)
    # angs = np.append(angs,-np.pi/2)
    angs = np.append(angs,0)
    angs = np.append(angs,0)

# #High Tolerance Load Pose Sample
# angs[1] = -1.36011
# angs[4] = .317925
# angs[7] = .558372
# angs[10] = .770438
# angs[13] = .483851
# angs[16] = .790993

#This set of angles was found to be particularly good at carrying loads
angs[1] = -.29
angs[4] = .109
angs[7] = 1.444
angs[10] = 1.35
angs[13] = 1.53
angs[16] = 1.254

print angs
arm.setJointAngs(angs)
arm.calcJacReal()

np.set_printoptions(precision=3)
np.set_printoptions(suppress=True)

evaluator = ae.ArmEvalFK(arm)
print "Joint Angs Set At:"
print arm.getJointAngs()

evaluator.arm_.calcStiffness()
direction = np.array([0,0,-1.0,0,0,0])
dx = .05
dw = 5*np.pi/180.0
delta = np.array([dx,dx,dx,dw,dw,dw])
max_load = evaluator.arm_.calcMaxStaticLiftForce(.15)

# limitDispForce = evaluator.arm_.CalcMaxForceLimitDisplacement(direction,delta)
# passive_load = np.array([0,0,-limitDispForce,0,0,0])
# limitTorqueForce = evaluator.arm_.calcMaxForceLimitTorque(direction)
# limitActiveForce = evaluator.arm_.calcMaxForceActiveWithExternalLoad(passive_load)

print
print "g-world-tool"
print arm.getGWorldTool()
print
print "g-world-arm"
print arm.g_world_arm_
print
print "jac_body"
print arm.getJacBody()
print
print "jac_body_real"
print arm.jac_body_real_
print
print "jac_body_real Condition Number"
print np.linalg.cond(arm.jac_body_real_)
print
print "flexibility"
print arm.getFlexibilityBody()
# print
# print "Max acceptable Force Limiting Total Displacement at End Effector: ", limitDispForce
# print "Max acceptable Force Limiting Torque At Each Joint: ", limitTorqueForce
# print "Max active wrench producible with left over joint torque: ", limitActiveForce
# print "Torqus from passive displacement only"
# print evaluator.arm_.getTorquePassive()

fig = plt.figure()
ax = fig.gca(projection='3d')
arm_plotter = ap.ArmPlotter(evaluator.arm_)
arm_plotter.plot(ax)

# # labels and such
ap.set_axes_equal(ax)
ax.set_title('Pose With Unusally High Load Tolerance')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
plt.show()
