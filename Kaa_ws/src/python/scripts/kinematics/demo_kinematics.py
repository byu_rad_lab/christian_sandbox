#!/usr/bin/env python

import matplotlib.pyplot as plt
from ego_core.kinematics import kinematics as kin
from ego_byu.armeval import armeval as ae
from ego_byu.tools import arm_tools as at
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import arm_plotter as ap
import numpy as np

lengths = np.array([.5,.5,.5,.5,.5,.5])
r = np.array([0,0,0])
t = np.array([0,0,1.0])

base_hom = kt.ax_vec2homog(r,t)
jt_height = .01

stiffness = np.array([300.0,300.0,300.0])
arm = at.nasa_arm(jt_height,lengths,base_hom,stiffness)

angs = np.array([])
for i in range(len(arm.lengths)):
    angs = np.append(angs,0)
    # angs = np.append(angs,np.random.rand()*np.pi-np.pi/2)
    angs = np.append(angs,-np.pi/2)
    angs = np.append(angs,0)

arm.setJointAngs(angs)
arm.calcJacReal()

np.set_printoptions(precision=3)
np.set_printoptions(suppress=True)
print "Arm Angles"
print arm.getJointAngs()
print

for i in range(len(arm.links_)):
    #Real Links Only
    if np.mod(i,2)==1:
        print "Link ",i," g_world_bot"
        print arm.links_[i].g_world_bot_

        print "Link ",i," g_world_top"
        print arm.links_[i].g_world_top_

        print "Link ",i," g_world_end"
        print arm.links_[i].g_world_end_

print "jac_spatial_"
print arm.getJacSpatial()
print

print "jac_spatial_real_"
print arm.jac_spatial_real_
print

print "jac_hybrid_"
print arm.getJacHybrid()
print

print "jac_hybrid_real_"
print arm.jac_hybrid_real_
print

print "jac_body_ from EE"
print arm.getJacBody()
print

print "jac_body_EE_real_"
print arm.jac_body_real_
print

fig = plt.figure()
ax = fig.gca(projection='3d')
arm_plotter = ap.ArmPlotter(arm)
arm_plotter.plot(ax)

# labels and such
ap.set_axes_equal(ax)
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
plt.show()
