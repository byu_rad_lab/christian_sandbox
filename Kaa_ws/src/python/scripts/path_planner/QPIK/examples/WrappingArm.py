#!/usr/bin/env python
from ego_byu.tools import geometries
from ego_byu.tools import constructors
from ego_byu.tools import paths
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import pickle_tools as pk
from ego_byu.tools.spline_planner.path_planner import AngleSmoothener
from copy import deepcopy  # from ego_byu.tools import constraints as cts
from scripts.path_planner.QPIK import PlanQPIK
import numpy as np
import matplotlib.pyplot as plt

# CONSTRUCT ARM
base = constructors.BaseXYZ()
xlimits = np.array([-3, 3])  # Limits on region where base can be
ylimits = np.array([-3, 3])  # FIXED
zlimits = np.array([-3, 3])  # FIXED
constraints = [xlimits, ylimits, zlimits]
base.constraints = constraints
# base = constructors.BaseFixed()

# Number and type of arms in high level planner
narms = 10
angs0 = np.array([0, .10, 0, 0, 0, 0,0,0,0,0])
arms = constructors.WrapArms(narms, base, angs0)
# angs0 = np.array([0, 1.5, 0, 0, 0, 0])
# arms = constructors.NasaArms(narms, base, angs0)

# Initialize Path
path = paths.NoPath
# paths.testpath(path)

# Constraints
## GRABBING A SPHERE
geoms = []
point = np.array([.4, -.3, .2]).reshape(3, 1)
r = .2
links = np.array(range(0,len(arms[0].links_)))

ct = {
    'shape': "Sphere",
    'point': point,
    'radius': r,
    "weight": 10,
     "type": "touch",
    "link_nums" : links}
geoms.append(ct)
ct = {
    'shape': "Sphere",
    'point': point,
    'radius': .8*r,
    "weight": 10,
     "type": "collision"}
geoms.append(ct)
# ct = {
#     'shape': "Sphere",
#     'point': np.array([0,-.3,.4]).reshape(3, 1),
#     'radius': 1.2*r,
#     "weight": 10,
#      "type": "collision"}
# geoms.append(ct)

## GRABBING A CYLINDER
# geoms = []
# point = np.array([.4, -.3, .2]).reshape(3, 1)
# r = .2
# ct = {
#     'shape': "Line",
#     'point': np.array([0, -.3, .2]).reshape(3, 1),
#     'radius': r,
#     "weight": 10,
#      "type": "touch"}
# geoms.append(ct)
# ct = {
#     'shape': "Sphere",
#     'point': np.array([0, -.3, .2]).reshape(3, 1),
#     'radius': .8*r,
#     "weight": 10,
#      "type": "collision"}
# geoms.append(ct)
# ct = {
#     'shape': "Sphere",
#     'point': np.array([0,-.3,.4]).reshape(3, 1),
#     'radius': 1.2*r,
#     "weight": 10,
#      "type": "collision"}
# geoms.append(ct)

# Plan! Smooth Feature Turned On
HLP = PlanQPIK.Planner(path, arms, base, geoms, True, angs0)
# HLP = PlanQPIK.Planner(path, arms, base, geoms, True)
HLP.smoothpts = 100
HLP.Plan()
HLP.ScaleFinalPathTimes(5)

HLP.LaunchGUI()
plt.show()

# Plot Resulting End Effector Path
# arm = deepcopy(HLP.toplevel.arms[0])
# plt.figure()
# y = []
# z = []
# for i in range(len(state[0])):
#     angs = np.array(state[0][i])
#     arm.setJointAngsReal(angs)
#     arm.calcFK()
#     g = arm.getGWorldTool()
#     y.append(arm.getGWorldTool()[1,3])
#     z.append(arm.getGWorldTool()[2,3])
# plt.plot(y, z, 'b*-', ms=10)
# plt.plot(y, z, 'b-')
# plt.xlabel('y [m]')
# plt.ylabel('z [m]')
# plt.title('Path in Space.  Equal time intervals marked with *.')
# plt.margins(0.05)
# plt.axis('equal')
# plt.show()

# for ang in state[0]
