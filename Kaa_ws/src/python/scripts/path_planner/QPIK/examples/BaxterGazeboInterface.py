#/usr/bin/env python
import rospy
import time
from ego_byu.tools import pickle_tools as pk

#source extend rad_demos rad_baxter_limb from byu git repo
from rad_baxter_limb.rad_baxter_limb import RadBaxterLimb
import numpy as np
import time

#MUST ENABLE ROBOT
# rosrun baxter_tools enable_robot.py -e

plan = pk.load_pickle('baxter_plan')
times= plan[0]
angs = plan[1]

#Change Convention on odd joints (+x is up for me, down for them)
angs[:,1] = -angs[:,1]
angs[:,3] = -angs[:,3]
angs[:,5] = -angs[:,5]

rospy.init_node("My_Node")
limb = RadBaxterLimb("right")
limb.set_command_timeout(50.0)
rate = rospy.Rate(500)

raw_input("Press Return to Stream Commands")

start_time = time.time()
# Start Sending Commands

while not rospy.is_shutdown():
    # Find where time is in path
    t = time.time()-start_time
    i = np.argmax(times > t)
    if t > times[-1]:
        i = len(times)-1

    # Safety Checks
    if i < 0 or i > len(times)-1:
        print "index found out of range of valid indices"
        assert(False)

    limb.set_joint_positions_mod(angs[i])
    print "time on path: ", t, "index of command: ", i
    rate.sleep()






