#!/usr/bin/env python
from ego_byu.tools import geometries
from ego_byu.tools import constructors
from ego_byu.tools import paths
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import pickle_tools as pk
from ego_byu.tools.spline_planner.path_planner import AngleSmoothener
from copy import deepcopy  # from ego_byu.tools import constraints as cts
from scripts.path_planner.QPIK import PlanQPIK
import numpy as np
import matplotlib.pyplot as plt

# CONSTRUCT ARM
base = constructors.BaseKrex()
xlimits = np.array([-3, 3])  # Limits on region where base can be
ylimits = np.array([-3, 3])  # FIXED
zlimits = np.array([-3, 3])  # FIXED
constraints = [xlimits, ylimits, zlimits]
base.constraints = constraints
# base = constructors.BaseFixed()

# Number and type of arms in high level planner
narms = 20
angs0 = np.array([0.0, np.pi/2, 0, 0, 0, 0])
arms = constructors.NasaArms(narms, base,angs0)

# Initialize Path
r = np.array([0,np.pi/4, 0])
t = np.array([.8, -.7, 1.])
g1 = kt.ax_vec2homog(r,t)
r = np.array([0,0.0,0])
t = np.array([0,0,0.0])
g2 = kt.ax_vec2homog(r,t)
offset =  g2.dot(g1)
path = paths.Surface(narms,offset,4)
# paths.testpath(path)

## Wiping A Panel
#Assign links for
links = np.array(range((len(arms[0].links_)-5),len(arms[0].links_)))
geoms = []
geoms.append(geometries.KrexBodyPlane())
geoms.append(geometries.CustomPlane(offset,links))

# Plan! Smooth Feature Turned On
HLP = PlanQPIK.Planner(path, arms, base, geoms, True)
# HLP = PlanQPIK.Planner(path, arms, base, geoms, True)
HLP.smoothpts = 100
HLP.Toplevel.max_iter = 100
HLP.Toplevel.jacobian_used = 3
# HLP.Toplevel.jacobian_used = 3
HLP.Plan()
HLP.ScaleFinalPathTimes(15)

HLP.LaunchGUI()
plt.show()

# Plot Resulting End Effector Path
# arm = deepcopy(HLP.toplevel.arms[0])
# plt.figure()
# y = []
# z = []
# for i in range(len(state[0])):
#     angs = np.array(state[0][i])
#     arm.setJointAngsReal(angs)
#     arm.calcFK()
#     g = arm.getGWorldTool()
#     y.append(arm.getGWorldTool()[1,3])
#     z.append(arm.getGWorldTool()[2,3])
# plt.plot(y, z, 'b*-', ms=10)
# plt.plot(y, z, 'b-')
# plt.xlabel('y [m]')
# plt.ylabel('z [m]')
# plt.title('Path in Space.  Equal time intervals marked with *.')
# plt.margins(0.05)
# plt.axis('equal')
# plt.show()

# for ang in state[0]
