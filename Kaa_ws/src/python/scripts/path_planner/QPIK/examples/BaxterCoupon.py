#!/usr/bin/env python
from ego_byu.tools import constructors
from ego_byu.tools import paths
from ego_byu.tools import arm_plotter as ap
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import pickle_tools as pk
from ego_byu.tools.spline_planner.path_planner import AngleSmoothener
from copy import deepcopy  # from ego_byu.tools import constraints as cts
from scripts.path_planner.QPIK import PlanQPIK
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# CONSTRUCT ARM
base = constructors.BaseXY()
xlimits = np.array([-1.0, 1.0])  # Limits on region where base can be
ylimits = np.array([-1.0, 1.0])  # FIXED
zlimits = np.array([-0, 0])  # FIXED
constraints = [xlimits, ylimits, zlimits]
base.constraints = constraints

# Number and type of arms in high level planner
narms = 50
base_hom = np.eye(4)
base_hom[2][3] = .5
arms = constructors.BaxterArms(narms, base_hom)
arms[0].setJointAngsZero()
arms[0].calcFK()

# fig = plt.figure()
# ax = fig.gca(projection='3d')
# plotter = ap.BaxterPlotter(arms[0])
# plotter.plot(ax)
# ap.set_axes_equal(ax)
# plt.xlabel('x')
# plt.ylabel('y')
# plt.show()

# PATH COUPON
# path = paths.Coupon
r = np.array([-np.pi/2.0, 0, 0])
t = np.array([0.0, 0.5, 1.00])
offset = kt.ax_vec2homog(r, t)
passes = 6
dx = .2
path = paths.SplineCoupon(narms, offset, passes, dx)
# paths.testpath(path)

# Constraints
constraints = []

# Plan! Smooth Feature Turned On
HLP = PlanQPIK.Planner(path, arms, base, constraints, True)
HLP.Toplevel.max_iter = 400
HLP.smoothpts = 10000
HLP.Plan()
HLP.ScaleFinalPathTimes(15)

switchkey = raw_input("Do you want to launch python GUI (p), save to pckl for Gazebo (g), or quit (q)?")

while (not switchkey in ['p', 'g', 'q']):
    switchkey = raw_input("Do you want to launch python GUI (p), save to pckl for Gazebo (g), or quit (q)?")

if switchkey == 'p':
    HLP.LaunchGUI()
    plt.show()
elif (switchkey == 'g'):
    pk.save_pickle("baxter_plan",[HLP.FinalPathTimes,HLP.state[0]])
    print "baxter_plan.pckl file written.  Simulate in BaxterGazeboInterface.py"



# Plot Resulting End Effector Path
# arm = deepcopy(HLP.Toplevel.arms[0])
# plt.figure()
# ax = plt.gca(projection="3d")

# x = []
# y = []
# z = []
# for i in range(len(HLP.state[0])):
#     angs = np.array(HLP.state[0][i])
#     arm.setJointAngsReal(angs)
#     arm.calcFK()
#     g = arm.getGWorldTool()
#     x.append(arm.getGWorldTool()[0,3])
#     y.append(arm.getGWorldTool()[1,3])
#     z.append(arm.getGWorldTool()[2,3])
# ax.scatter(x[0::10],y[0::10], z[0::10], 'b*-')
# ax.scatter(x,y, z, 'b-')
# plt.xlabel('x [m]')
# plt.ylabel('z [m]')
# # plt.zlabel('z [m]')
# plt.title('Path in Space.  Equal time intervals marked with *.')
# plt.margins(0.05)
# ax.set_xlim([-.5,.5])
# ax.set_ylim([0,1.0])
# ax.set_zlim([0,1.0])
# plt.show()

# for ang in state[0]
