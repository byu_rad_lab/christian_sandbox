#!/usr/bin/env python
import matplotlib.pyplot as plt
from ego_byu.tools import constructors
# from ego_core.kinematics import kinematics as kin
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import paths
from ego_byu.tools import arm_plotter as ap
# from ego_byu.tools import constraints as cts
import numpy as np
from scripts.path_planner.QPIK import PlanQPIK
import time
import rospy

import matplotlib.pyplot as plt


base = constructors.BaseFixed()
print base
raw_input("break")
# rospy.init_node('Base_getter',anonymous=False)
# base = constructors.BaseFixedCortex()
# base = constructors.BaseFixed()

#Rotation to Correct rotation on Cortex
# base.g_world_base = base.g_world_base.dot(kt.ax_vec2homog(np.array([0,np.pi/2.0,0]),np.zeros(3)))
# base.g_world_base = base.g_world_base.dot(kt.ax_vec2homog(np.array([0,0,np.pi]),np.zeros(3)))
# print base.g_world_basr
# assert(False)

# Number and type of arms in high level planner
narms = 40
# arms = constructors.SimpleArms(narms,base)
angs0 = np.array([0,-np.pi/2.0,0,0,0,0])
arms = constructors.NasaArms(narms, base,angs0)
# arms = constructors.BlowMoldedArms(narms)

# Needs to get h1 from a subscriber eventually
# fig = plt.figure()
# ax = fig.gca(projection='3d')
# arm = constructors.NasaArms(narms, base)[0]
# arm.setJointAngsReal(np.array([np.pi/4.0, np.pi/4.0, 0, 0, 0, 0]))
# arm.calcFK()
# aplot = ap.SimpleArmPlotter(arm)
# aplot.plot(ax)
# ap.set_axes_equal(ax)
# plt.show()

# arm.setJointAngsReal(np.array([0, -np.pi/2.0, 0, 0, 0, 0]))
arm = constructors.NasaArms(narms, base)[0]
arm.setJointAngsReal(angs0)
arm.calcFK()
h1 = arm.getGWorldTool()
h2, wv, wr = paths.LineY(0)

path = paths.CombinePaths(paths.LineBetween(h1, h2), paths.LineY)

# Constraints for Collision avoidance
constraints = []
# point = np.array([.4, -.6, .4]).reshape(3, 1)
# radius = .4
# weight = 1
# ct = {'type': "Sphere", 'point': point, 'radius': radius, "weight": weight}
# constraints.append(ct)
# point = np.array([.4, -.3, .4]).reshape(3, 1)
# radius = .4
# weight = 1
# ct = {'type': "Sphere", 'point': point, 'radius': radius, "weight": weight}
# constraints.append(ct)

# Plan!
HLP = PlanQPIK.Planner(path, arms, base, constraints,True,angs0)
HLP.refinement_tol = 10
HLP.smoothpts = 1000
s = time.time()
HLP.Plan()
HLP.ScaleFinalPathTimes(10)
f = time.time()

print "Total Number of Refined Poses:"
print len(HLP.FinalPathConfigs)
print "Total Refinement Time:"
print HLP.RefineTime

print "Total Time: ", f-s
r, t = kt.hom2ax_vec(HLP.base.g_world_base)
print "Base Position: "
print r
print t

HLP.LaunchGUI()
plt.show()
