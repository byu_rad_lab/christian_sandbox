#!/usr/bin/env python
import kinematics as kin
import numpy as np
import scipy.linalg
import ego_byu.cvxgen 
from cvxopt import matrix, solvers, spmatrix
from collections import defaultdict
from ego_byu.tools import geometries
from ego_byu.tools import arm_plotter as ap

# from ego_byu.tools import paths

import matplotlib.pyplot as plt
from copy import copy
from copy import deepcopy
import time
from scipy import sparse

solvers.options['show_progress'] = False
np.set_printoptions(linewidth=200)
np.set_printoptions(suppress=True)
np.set_printoptions(precision=5)


class PathIK(object):

    def __init__(self, arms, base, path_function, geometries, initial_angs, refine=False):
        # Arms
        self.arms = arms
        if initial_angs != [] and not callable(path_function):
            self.empty_arm_num = 3
            for i in range(self.empty_arm_num):
                self.arms = np.append(self.arms,deepcopy(arms[-1]))
                self.arms[0].setJointAngsReal(np.array(initial_angs))
        self.narms = len(self.arms)
        self.temp_arms = deepcopy(self.arms)
        self.adof = len(arms[0].getJointAngsReal())
        self.asdof = self.adof*self.narms
        self.angs_arms = self.UnpackAngsArm()

        # BASE
        self.base = base
        self.bdof = base.bdof
        self.angs_base = self.UnpackAngsBase()
        self.base.dmax = .003

        # Initial Configuration Option: Added as a Constraint in the QP
        self.init_config = initial_angs
        self.tdof = self.asdof+self.bdof
        self.angs_delta_full = np.zeros([self.tdof, 1])  # stacked angs (dth1 dth2 dth3 ... dthn dth_base)
        self.angs_avg = np.zeros([self.adof, 1])  # Average of each angle  (th_avg1 .. th_avgm)
        self.dth_max = 10*np.pi/180  # - Keep above 5 deg - Constraint overridden if init_config exists
        self.angs = np.vstack([self.angs_arms, self.angs_base])

        #Default end_effector - for wiping, assign this to link number from end
        self.jacobian_used = "end_effector"

        # Solver Options : cvxopt generally does well with HiDof problems.  Cvxgen handles no more than 24dof
        self.solver = "cvxopt"
        self.path_function = path_function
        self.max_iter = 1000
        self.error_tol = 1e-4  # Error between arms and targets, under this and we quit
        self.error_improv_tol = -100.0  # if we don't improve by this much, quit (negative allows worse steps)
        self.max_step = 1.0  # capping the maximum step we take in ClampAngsDelta
        self.refine = refine  # Generally don't refine, use many arms and solve once

        # -------------------------Clamping/LineSearch---------------------------------
        # ratio of rotational to translational 1.0 means 1 meter and 90 degrees equivalent
        self.rot_clamp_max = 30.0 * np.pi/180.0  # radians
        self.vec_clamp_max = 1.5

        self.use_line_search = True  # True: Linesearch,False: Clamp Angs
        self.step_scale = .3  # Reinitialized at 1.0 for line search, default at .3 for clamping
        self.min_step_scale = 2e-1  # Keep around 1e-2, theoretically the line search should stay away from
        self.step_scale_factor = .90

        # -------------------WEIGHTING---------------------------------
        self.update_wts_below_improvement = .10  # Percent of improvement at which objectives is rescaled

        # Weighting Alpha: Penalty on Joints being away from joint averages (betwen arms)
        #||th_i - th_avgs_i|| expanded so th_dot = th_i - th_i_past(or self.angs) => solving for th_dot in QP
        self.alpha = 0  # Keep in range between .1 and 10
        self.alpha_scale_step = .9  # Generally between .88 and .95
        self.alpha_min = 0  # Good to keep minimum, but very small to allow convergence

        # Weighting Beta: Penalty on Joints being away from joint centers
        #||th_i - th_jc|| expanded so th_dot = th_i - th_i_past(or self.angs) => solving for th_dot in QP
        self.beta = 0
        self.beta_scale_step = .9
        self.beta_min = 0  # Good to keep minimum, but very small to allow convergence

        # Weighting Kappa: Penalty on Joints being away from neighboring joints
        #||th_(i,j) - th_(i,j+1)|| ith joint, jth arm, - minimizing squared distance between configurations
        self.kappa = 1.0  # Keep in range between .1 and 10
        self.kappa_scale_step = .9  # Generally between .88 and .95
        self.kappa_min = .02  # Good to keep minimum, but very small to allow convergence

        # Weighting Gamma: Penalty on Decreasing Manipulability OR Decreasing Velocity Trans Ratio.
        # - gamma * DM/dth_i where M is manipulability determinant given by Yokishiwa (found numerically)
        # self.gamma_type = "VelocityTransformationRatio"
        # self.gamma_u = np.array([1,0,0,0,0,0])
        self.gamma_type = "ManipulabilityEllipsoidVolume"
        self.gamma = 0  # Keep in range between .1 and 10
        self.gamma_scale_step = .9  # Generally between .88 and .95
        self.gamma_min = 0  # Good to keep minimum, but very small to allow convergence

        # Weighting on Movement in general
        rho = 5.0e-2  # Good to keep very small - alpha already constrains movement
        self.Pth = rho * np.eye(self.tdof)

        # Path Targets -- Weights are len(targets) x 3 numpy arrays each representing target weighting in Pf
        self.targets = []  # Assigned in "AssignTargets"
        self.vec_weight = np.zeros([self.narms, 3])
        self.rot_weight = np.zeros([self.narms, 3])

        # Error and cost terms
        self.pose_error = float('inf')
        self.Acost = float('inf')
        self.Bcost = float('inf')
        self.Kcost = float('inf')
        self.Gcost = float('inf')
        self.total_cost = float('Inf')

        # Recording
        self.step_dict = defaultdict(list)
        self.step_dict["angs"].append(copy(self.angs))
        self.steps_taken = 0
        self.improvement_cutoff = 1e-5
        self.TrackEvolution = False

        # Constraints
        self.SetConstraints()

        # Collision OR Touch:
        self.geometries= geometries

        # collision tolerances start at () cm, and regress to () cm after () iterations
        # If a manipulator falls within this tolerance, a heavy weight pushes it away.
        self.distance_tolerances = np.linspace(.05, .02, 100)
        self.squeeze_tol = .50

        # Final Evaluation Criterion
        self.path_cost = 0
        self.status = []
        self.alpha_final = self.alpha
        self.beta_final = self.beta
        self.kappa_final = self.kappa
        self.gamma_final = self.gamma

        self.KillSwitch = False

    # p1 and p2 used in refining, not important otherwise (from p1 = 0, p2 = 1 otherwise)
    def Solve(self, p1, p2):
        start_time = time.time()
        self.p1 = p1
        self.p2 = p2
        self.AssignTargets()  # From Parameterized Path Function OR from targets provided by the spliner
        self.SetWeightingMats()  # Sets inital weights for QP  (alphas, kappas, betas)

        self.UpdateArms()
        for i in range(self.max_iter):
            try:
                self.Step()
                self.steps_taken = i + 1
                self.PrintProgress()
                # constructors.TestArmSetUp(self.arms[1])

                if self.pose_error < self.error_tol and self.improvement < self.improvement_cutoff and np.linalg.norm(self.angs_delta) < .01:
                    self.status = 'Solved'
                    break
                if self.improvement < self.error_improv_tol:
                    self.status = 'Stuck'
                    break
                if i == self.max_iter-1:
                    self.status = 'Max Iterations'
                    break
                if self.KillSwitch:
                    break
            except KeyboardInterrupt:
                print "Breaking Out of Planning Seqence"
                break

        print "Status: ", self.status, "(%s)" % self.solver, "Time: ", time.time()-start_time
        self.EvaluatePath()
        return

    def Step(self):
        # Setup QP: 1/2 X.T*P*X + X.T*B
        # Quadratic Terms: 2 cancels 1/2
        self.CalcPrimaryTargetPathTerms()  # ||Jth_dot - V_des||_Pf terms
        self.CalcGeomTerms()  # Same as above only V_des are desired twists away or towards close collisions
        self.P = 2*(self.TargQTerm + self.LinkQTerm + self.alphas + self.betas + self.kappas + self.Pth)

        # Linear Terms: See init for explanations, these are expanded from normed expressions
        self.AAL = -2*self.alphas.T.dot(self.angs_avg_stacked)+2*self.alphas.T.dot(self.angs)
        self.BBL = -2*self.betas.T.dot(self.c)+2*self.betas.T.dot(self.angs)
        self.KKL = self.GetKKL()  # nontrivial expansion
        if np.mod(self.steps_taken, 5) == 0:  # Expensive, only updated every 5 steps
                #Scale gammas so later arms are more important to have ellipsoid than beginning
                self.GGL = -self.GetManipDerivs()*self.gamma  # Want to Increase M -> (penalize negative)

        self.B = self.TargLTerm + self.LinkLTerm + self.AAL + self.BBL + self.KKL + self.GGL

        # Constraints Gx < h
        self.G = self.A

        # Limited by Joint Stops and Dth_maxes
        self.h = self.b + self.A.dot(-self.angs)  # Joint Stops
        for i in range(len(self.h)-2*self.bdof):
            self.h[i] = min(self.dth_max, self.h[i])  # Constrain closer of two: joint stop, and dth max
        for i in range(self.bdof*2):
            self.h[-i-1] = min(self.base.dmax, self.h[-i-1])

        # Forces First Arm To Go to Initial Condition (potentially overrides other constraint)
        if self.init_config != []:
            for i in range(self.adof):
                self.h[2*i] = -(self.init_config[i]-self.angs[i])
                self.h[2*i+1] = (self.init_config[i]-self.angs[i])

        # RefineMode: Fixes first and last arm by reducing dimensionality in QP: generally unneeded
        if self.refine:
            self.Pr = self.P[(self.adof):(self.asdof-self.adof), (self.adof):(self.asdof-self.adof)]
            self.Br = self.B[(+self.adof):(self.asdof-self.adof)]
            self.Gr = self.A[:, (self.adof):(self.asdof-self.adof)]
            self.hr = self.h

            # Cut Rows with only zeros after reduction in constraint matrices
            for i in reversed(range(len(self.Gr[:, 0]))):
                if np.all(self.Gr[i, :] == 0):
                    self.Gr = np.delete(self.Gr, i, 0)
                    self.hr = np.delete(self.hr, i, 0)

        # solve the QP to obtain search direction, limit by linesearch or clamping
        self.SolveQP()
        self.LimitAngsDelta()

        # Updates
        self.angs += self.angs_delta
        # if self.init_config != []:
        #     self.angs[0:self.adof,0] = self.init_config
        self.UpdateArms()

        # Evaluate and Record
        self.Evaluate()
        if self.improvement < self.update_wts_below_improvement:
            self.UpdateWeights()
        self.Record()

    def SolveQP(self):
        if self.solver == "cvxgen":
            if (np.shape(self.P)[0] > 25 and not self.refine) or (self.refine and np.shape(self.Pr)[0] > 25):
                print "Cvxgen handles only about 24 DOF"
                assert(False)
            if self.refine:
                fixed_angs = np.zeros([self.adof, 1])
                S = cvxgen.CVXGen(np.array(self.Pr), np.array(self.Br), np.array(self.Gr), np.array(self.hr))
                S.Solve()
                self.angs_delta_full = np.vstack([fixed_angs, S.x_, fixed_angs])
            else:
                S = cvxgen.CVXGen(np.array(self.P), np.array(self.B), np.array(self.G), np.array(self.h))
                S.Solve()
                self.angs_delta_full = S.x_
        elif self.solver == "cvxopt":
            if self.refine:
                fixed_angs = np.zeros([self.adof, 1])
                PSparse = scipy_sparse_to_spmatrix(scipy.sparse.coo_matrix(self.Pr))
                GSparse = scipy_sparse_to_spmatrix(scipy.sparse.coo_matrix(self.Gr))
                S = solvers.qp(PSparse, matrix(self.Br), GSparse, matrix(self.hr))
                self.angs_delta_full = np.vstack([fixed_angs, np.array(S['x']), fixed_angs])
            else:

                PSparse = scipy_sparse_to_spmatrix(scipy.sparse.coo_matrix(self.P))
                GSparse = scipy_sparse_to_spmatrix(scipy.sparse.coo_matrix(self.G))
                S = solvers.qp(PSparse, matrix(self.B), GSparse, matrix(self.h))
                # S = solvers.qp(matrix(self.P), matrix(self.B), matrix(self.G), matrix(self.h))
                self.angs_delta_full = np.array(S['x'])
        else:
            print "Solver Not Recognized"
            assert(False)

    def LimitAngsDelta(self):
        if self.use_line_search is True:
            self.LineSearch()
        else:
            self.ClampAngsDelta()

    #This can drive the end effector, or a link body to a given target
    def CalcPrimaryTargetPathTerms(self):
        J = []  # Stack J in Diagonal Structure
        Jb = []  # Stack Jb on Left to Contribute

        # Define Targets
        v_deltas = np.zeros([6*self.narms, 1])
        self.v_deltas_full = np.zeros([6*self.narms, 1])
        for i, arm in enumerate(self.arms):

            if self.jacobian_used == "end_effector":
                g_world_tool = arm.getGWorldTool()
                J.append(arm.getJacBodyReal())
                Jb.append(kin.AdjFromHom(kin.HomInv(g_world_tool)).dot(self.base.JBase))  # Body Jacobian

                # Figure out delta position, clamp, turn into twist
                g_delta_full = kin.HomInv(g_world_tool).dot(self.targets[i])
                g_delta = self.ClampGoal(g_delta_full)
                v_deltas[6*i:6*i+6] = kin.VeeHom(g_delta)  # not right, but works well
                self.v_deltas_full[6*i:6*i+6] = kin.VeeHom(g_delta)

            else:
                g_world_link_com = arm.links_[-self.jacobian_used].g_world_com_
                J_link = np.array(arm.getJacBodyRealLink(len(arm.links_)-self.jacobian_used))
                if np.shape(J_link)[1] < len(arm.getRealJointIdx()):
                    J_link = np.hstack([J_link,np.zeros([6,len(arm.getRealJointIdx())-np.shape(J_link)[1]])])

                # arm.setJointAngsZero()
                # angs = np.array([np.pi/2.0,0,0,0,0,0])
                # arm.setJointAngsReal(angs)

                J.append(J_link)
                # print J
                # raw_input("break")
                Jb.append(kin.AdjFromHom(kin.HomInv(g_world_link_com)).dot(self.base.JBase))  # Body Jacobian

                g_delta_full = kin.HomInv(g_world_link_com).dot(self.targets[i])
                g_delta = self.ClampGoal(g_delta_full)
                v_deltas[6*i:6*i+6] = kin.VeeHom(g_delta)  # not right, but works well
                self.v_deltas_full[6*i:6*i+6] = kin.VeeHom(g_delta)
                # Figure out delta position, clamp, turn into twist
                # if self.steps_taken > 100:
                #     plt.figure()
                #     ax = plt.gca(projection="3d")
                #     plotter = ap.SimpleArmPlotter(arm)
                #     plotter.plot(ax)
                #     ap.plot_gnomon(ax,arm.links_[-3].g_world_com_)
                #     ap.plot_gnomon(ax,self.targets[i],color=['k','k','m'])
                #     ap.set_axes_equal(plt.gca())
                #     print J_link
                #     print arm.getJacBodyRealLink(len(arm.links_)-self.jacobian_used)

                #     print g_delta_full
                #     print self.v_deltas_full

                # plt.show()

        Jb = np.vstack(Jb)
        J = np.hstack([scipy.linalg.block_diag(*J), Jb])

        # Terms for QP
        self.TargQTerm = J.T.dot(self.Pf).dot(J)
        self.TargLTerm = -2*np.dot(J.T.dot(self.Pf), v_deltas)

    def CalcGeomTerms(self):
        self.LinkQTerm = np.zeros(np.shape(self.TargQTerm))
        self.LinkLTerm = np.zeros(np.shape(self.TargLTerm))

        #For Collision Terms
        self.dist_tol = self.distance_tolerances[min(self.steps_taken, len(self.distance_tolerances)-1)]

        for geometry in self.geometries:
            self.AddGeomTerm(geometry)

    def AddGeomTerm(self, geometry):
        Pf = np.diag(np.hstack((np.ones(3), np.zeros(3))))
        for i, arm in enumerate(self.arms):
            arm.calcFK()
            for j, link in enumerate(arm.links_):

                #Find Target and weighting for link
                weight, g_world_target = self.AssignLinkTarget(geometry,link,i,j)

                if weight != 0:
                    adof = self.adof
                    bdof = self.bdof
                    Jarm = np.zeros([6, adof+bdof])
                    g_delta = kin.HomInv(link.g_world_com_).dot(g_world_target)
                    v_delta = kin.VeeHom(g_delta)  # not right, but works wel

                    if bdof > 0:  # Breaks if bdof == 0
                        Jarm[:, -bdof:] = kin.AdjFromHom(kin.HomInv(link.g_world_com_)).dot(self.base.JBase)
                    jac_body_link = arm.getJacBodyRealLink(j)
                    Jarm[:, :len(jac_body_link[0])] = jac_body_link
                    Qterm = weight*Jarm.T.dot(Pf).dot(Jarm)
                    Lterm = weight*-2*Jarm.T.dot(Pf).dot(v_delta)

                    self.LinkQTerm[adof*i:adof*(i+1), adof*i:adof*(i+1)] += Qterm[:adof, :adof]
                    self.LinkLTerm[adof*i:adof*(i+1), 0] += Lterm[:adof, 0]
                    if bdof > 0:
                        self.LinkQTerm[-bdof:, adof*i:adof*(i+1)] += Qterm[-bdof:, :adof]
                        self.LinkQTerm[adof*i:adof*(i+1), -bdof:] += Qterm[:adof, -bdof:]
                        self.LinkQTerm[-bdof:, -bdof:] += Qterm[-bdof:, -bdof:]
                        self.LinkLTerm[-bdof:, 0] += Lterm[-bdof:, 0]

    def AssignLinkTarget(self,geometry,link,ith_arm,jth_link):
        link_p = link.g_world_com_[:3, 3].reshape(3, 1)
        g_world_target = link.g_world_com_  # body is defined at g_world_com_ for links

        #Define targets normal to geometry for collisions, on object for touch objects
        if geometry["shape"] == "Plane":
            distance = (link_p-geometry['point']).T.dot(geometry['normal'])[0, 0]
            if geometry["type"] == "collision":
                g_world_target[:3, 3] += geometry['normal'].reshape(3) #Pull Link Away from Plane
            elif geometry["type"] == "touch":
                g_world_target[:3, 3] -= geometry['normal'].reshape(3)*distance #Push Link Towards Plane

        elif geometry["shape"] == "Cylinder":
            v_pt_link = link.g_world_com_[:3, 3].reshape(3, 1)-geometry['point']
            v_cpt_link = v_pt_link-((geometry['direction'].T.dot(v_pt_link))*geometry['direction'])
            v_cyl_link = v_cpt_link-geometry['radius']*v_cpt_link/np.linalg.norm(v_cpt_link)
            distance = np.linalg.norm(v_cyl_link)
            if geometry["type"] == "collision":
                g_world_target[:3, 3] += v_cyl_link.reshape(3)/distance
            if geometry["type"] == "touch":
                g_world_target[:3, 3] -= v_cyl_link.reshape(3)

        elif geometry["shape"] == "Sphere":
            v_point_link = link.g_world_com_[:3, 3].reshape(3, 1)-geometry['point']
            v_sphere_link = v_point_link-geometry['radius']*v_point_link/np.linalg.norm(v_point_link)
            distance = np.linalg.norm(v_sphere_link)
            if geometry["type"] == "collision":
                g_world_target[:3, 3] += v_point_link.reshape(3)/distance
            if geometry["type"] == "touch":
                g_world_target[:3, 3] -= v_sphere_link.reshape(3)

        #Assign Weight: Only last arm weighted for touching applications
        if geometry['type'] == "collision":
            if not 'link_nums' in geometry:
                weight = geometries.Collision_Weight_Func(distance,self.dist_tol,geometry['weight'])
            elif sum(geometry['link_nums']==jth_link)>0:
                weight = geometries.Collision_Weight_Func(distance,self.dist_tol,geometry['weight'])
                if distance < 0 and self.steps_taken >= self.max_iter-1:
                    print "LINK TOUCHING CONSTRAINT AT END OF OPTIMIZATION"
            else:
                weight = 0
        elif geometry['type'] == "touch":
            if ith_arm == len(self.arms)-1 and sum(geometry['link_nums']==jth_link)>0:
                weight = geometries.Touch_Weight_Func(distance,self.squeeze_tol,geometry['weight'])
            else:
                weight = 0
        else:
            print "Geometry Weight Function Not Recognized"
            assert(False)
        return weight, g_world_target

    def GetKKL(self):
        KKL = np.zeros([self.asdof, 1])
        for j in range(self.adof):
            for i in range(self.narms-1):
                index1 = j+i*self.adof
                index2 = j+(i+1)*self.adof
                KKL[index1][0] += self.angs[index1]-self.angs[index2]
                KKL[index2][0] += -self.angs[index1]+self.angs[index2]
        KKL = np.vstack([KKL, np.zeros([self.bdof, 1])])*self.kappa
        return KKL

    def GetManipDerivs(self):
        dth = 1e-6
        dMdth = np.zeros([self.tdof, 1])
        k = 0
        for i, arm in enumerate(self.temp_arms):
            thetas = self.arms[i].getJointAngsReal()
            arm.setJointAngsReal(thetas)
            arm.calcJacReal()
            if self.gamma_type == "ManipulabilityEllipsoidVolume":
                J = arm.getJacBodyReal()
                inner = np.linalg.det((J.dot(J.T)))
            elif self.gamma_type == "VelocityTransformationRatio":
                J = arm.getJacHybridReal()
                inner = 1.0/(self.gamma_u.dot(np.linalg.inv(J.dot(J.T))).dot(self.gamma_u.T))
            if inner < 0:
                inner = 0
            M0 = np.sqrt(inner)
            for j in range(len(thetas)):
                th_d = copy(thetas)
                th_d[j] += dth
                arm.setJointAngsReal(th_d)
                arm.calcJacReal()
                if self.gamma_type == "ManipulabilityEllipsoidVolume":
                    J = arm.getJacBodyReal()
                    inner = np.linalg.det((J.dot(J.T)))
                elif self.gamma_type == "VelocityTransformationRatio":
                    J = arm.getJacHybridReal()
                    inner = 1.0/(self.gamma_u.dot(np.linalg.inv(J.dot(J.T))).dot(self.gamma_u.T))
                if inner < 0:
                    inner = 0
                M1 = np.sqrt(inner)

                dMdth[k] = (M1-M0)/dth
                k += 1
            dMdth[0:self.tdof-self.bdof-self.adof] = 0
        return dMdth

    def AssignTargets(self):
        # If Path is a parameterized function
        if callable(self.path_function):
            p = np.linspace(self.p1, self.p2, self.narms, endpoint=True)
            targets = []
            for i, t in enumerate(p):
                g, vw, rw = self.path_function(t)
                self.targets.append(g)
                self.vec_weight[i, :] = vw
                self.rot_weight[i, :] = rw
        # If path variable is a list of targets
        else:
            self.targets = self.path_function['targets']
            self.vec_weight = self.path_function['pos_wts']
            self.rot_weight = self.path_function['rot_wts']

            #Add erroneous arms/targets to allow arm flexibility in moving to beginning configuration
            if self.init_config != []:
                g = self.arms[0].getGWorldTool()
                for i in range(self.empty_arm_num):
                    self.targets.insert(0,g)
                    self.vec_weight = np.vstack([np.array([.0,.0,.0]),self.vec_weight])
                    self.rot_weight = np.vstack([np.array([.0,.0,.0]),self.rot_weight])

        #Add Target if initial condition is specified for first arm.

    def UpdateArms(self):
        self.angs_avg = np.zeros([self.adof, 1])
        for i, arm in enumerate(self.arms):
            if not (self.refine and (i == 0 or i == self.narms-1)):
                angs = self.angs[self.adof*i:self.adof*(i+1)]
                self.angs_avg += angs
                arm.setJointAngsReal(angs)
                for j in range(self.bdof):
                    self.base.g_world_base[j][3] = self.angs[-self.bdof+j][0]
                arm.g_world_arm_ = np.dot(self.base.g_world_base, self.base.g_base_arm)
                arm.calcJacReal()
        self.angs_avg = self.angs_avg/self.narms
        self.angs_avg_stacked = np.zeros([self.tdof, 1])  # Alphas Base = 0
        for i in range(self.narms):
            self.angs_avg_stacked[self.adof*i:self.adof*(i+1)] = self.angs_avg

    def SetConstraints(self):
        A_list = []  # Limits dth in either direction
        b_list = []  # Limits from zero in either direction (abs value)
        c_list = []  # Preferred position (Joint Center)

        for arm in self.arms:
            for link in arm.links_:
                A = link.joint().limits().a()
                b = link.joint().limits().b().flatten()
                c = link.joint().limits().c().flatten()

                A_list.append(A.copy())
                b_list.append(b.copy())
                c_list.append(c.copy())

        for i in range(self.bdof):
            A = np.array([[-1.0, 1.0]]).T
            b = np.array([-self.base.constraints[i][0], self.base.constraints[i][1]])
            c = np.array([self.base.base_center[i]])

            A_list.append(A.copy())
            b_list.append(b.copy())
            c_list.append(c.copy())

        # make a big matrix for each
        self.A = scipy.linalg.block_diag(*A_list)
        self.b = np.array([np.hstack(b_list)]).T  # Defined as column Vecs
        self.c = np.array([np.hstack(c_list)]).T
        self.RemoveFalseJoints()

    def RemoveFalseJoints(self):
        real_indices = []
        for i in range(len(self.arms)):
            for j in self.arms[0].getRealJointIdx():
                real_indices.append((j+(i*self.arms[0].num_dof_))[0])
        for i in range(self.bdof):
            real_indices.append(self.arms[0].num_dof_*self.narms+i)
        for i in reversed(range(len(self.A[0][:]))):
            if i not in real_indices:
                self.A = np.delete(self.A, i, 1)
        for i in reversed(range(len(self.A[:, 0]))):
            if np.all(self.A[i, :] == 0):
                self.A = np.delete(self.A, i, 0)
                self.b = np.delete(self.b, i, 0)
        for i in reversed(range(len(self.c))):
            if i not in real_indices:
                self.c = np.delete(self.c, i, 0)

    def ClampGoal(self, g_delta):
        rot_mag = np.linalg.norm(kin.RotToAx(g_delta[:3, :3]))
        vec_mag = np.linalg.norm(g_delta[:3, 3])

        rot_ratio = rot_mag / self.rot_clamp_max
        vec_ratio = vec_mag / self.vec_clamp_max
        max_ratio = max(rot_ratio, vec_ratio)

        if max_ratio > 1.0:
            return kin.HomScale(g_delta, 1.0 / max_ratio)
        else:
            return g_delta

    def ClampAngsDelta(self):
        angs_delta_scale = self.angs_delta_full * self.step_scale
        if self.refine:
            mag = np.linalg.norm(angs_delta_scale[:], 2)
        else:
            mag = np.linalg.norm(angs_delta_scale[:self.bdof], 2)
        if mag > self.max_step:
            self.angs_delta = angs_delta_scale / mag * self.max_step
        else:
            self.angs_delta = angs_delta_scale

    def LineSearch(self):
        self.step_scale = 1.0
        objective = float("inf")
        while(True):
            angs_delta_scale = self.angs_delta_full*self.step_scale
            angs = self.angs+angs_delta_scale
            obj = self.ObjNonlinear(angs)

            # step until it gets worse or below minimum step allowable
            if obj > objective or self.step_scale < self.min_step_scale:
                self.step_scale *= 1.0/self.step_scale_factor
                break
            else:
                self.step_scale *= self.step_scale_factor
                objective = obj
        self.angs_delta = self.angs_delta_full*self.step_scale
        return

    def ObjNonlinear(self, angs):
        # Objective Function: Minimize: (V_delta.T*Pf*V_delta)
        #              + (angs-avgs).T*alphas*(angs-avgs)
        #              + (angs-jc).T*betas*(angs-jc) - Sum of differences between angle and jc
        #              + .5*kappa*sum((th_i-th_(i+1))^2) - Sum of squared differences of neighboring terms
        # angs_avg_stacked = np.zeros([self.bdof+self.asdof, 1])
        # angs_avg = np.zeros([self.adof, 1])
        v_deltas = np.zeros([6*self.narms, 1])
        for i, arm in enumerate(self.temp_arms):
            arm_angles = angs[i*self.adof:(i+1)*self.adof]
            arm.setJointAngsReal(arm_angles)
            for j in range(self.bdof):
                self.base.g_world_base[j][3] = angs[-self.bdof+j][0]
            arm.g_world_arm_ = np.dot(self.base.g_world_base, self.base.g_base_arm)
            arm.calcFK()
            if self.jacobian_used == "end_effector":
                g_start = arm.getGWorldTool()  # Figure out delta position, clamp, turn into twist
            else:
                g_start = arm.links_[-self.jacobian_used].g_world_com_
            g_delta = kin.HomInv(g_start).dot(self.targets[i])
            v_deltas[6*i:6*i+6] = kin.VeeHom(g_delta)  # not right, but works well
            # angs_avg += arm_angles

        # Compute error on Twist
        pose_error = v_deltas.T.dot(self.Pf).dot(v_deltas)[0, 0]

        # Compute Difference Between Joint Angles and Their Avgs
        # angs_avg = angs_avg/self.narms #
        # for i in range(self.narms):
        #     angs_avg_stacked[self.adof*i:self.adof*(i+1)] = angs_avg

        # ang_hat_avg = (angs-angs_avg_stacked)
        # ang_hat_ctr = (angs-self.c)
        # alpha_penalty = ang_hat_avg.T.dot(self.alphas).dot(ang_hat_avg)[0, 0]
        # beta_penalty = ang_hat_ctr.T.dot(self.betas).dot(ang_hat_ctr)[0, 0]
        # kappa_penalty = self.KappaPenalty(angs)
        # return pose_error+alpha_penalty+beta_penalty+kappa_penalty
        # Just about as good as with all terms, and much faster
        return pose_error

    def Evaluate(self, FinalEvaluation=False):
        ang_hat_avg = (self.angs-self.angs_avg_stacked)
        ang_hat_ctr = (self.angs-self.c)

        self.pose_error = self.v_deltas_full.T.dot(self.Pf).dot(self.v_deltas_full)[0, 0]
        self.Acost = (ang_hat_avg).T.dot(self.alphas).dot(ang_hat_avg)[0, 0]
        self.Bcost = (ang_hat_ctr).T.dot(self.betas).dot(ang_hat_ctr)[0, 0]
        self.Kcost = self.KappaPenalty(self.angs)
        self.ManipScore = -self.GetManipScore()

        # In QP
        self.EEterms = (self.angs_delta).T.dot(self.TargQTerm).dot(self.angs_delta)[
                      0, 0]+self.TargLTerm.T.dot(self.angs_delta)[0, 0]
        self.Aterms = (self.angs_delta).T.dot(self.alphas).dot(
            self.angs_delta)[0, 0]+self.AAL.T.dot(self.angs_delta)[0, 0]
        self.Bterms = (self.angs_delta).T.dot(self.betas).dot(
            self.angs_delta)[0, 0]+self.BBL.T.dot(self.angs_delta)[0, 0]
        self.Kterms = (self.angs_delta).T.dot(self.kappas).dot(
            self.angs_delta)[0, 0]+self.KKL.T.dot(self.angs_delta)[0, 0]
        self.Gterms = self.GGL.T.dot(self.angs_delta)[0, 0]*self.gamma

        self.improvement = self.total_cost
        self.total_cost = self.pose_error + self.Acost + self.Bcost + self.Kcost + self.ManipScore
        self.improvement = self.improvement-self.total_cost

    def EvaluatePath(self):
        self.alpha = self.alpha_final
        self.beta = self.beta_final
        self.kappa = self.kappa_final
        self.gamma_final = self.gamma_final
        self.SetWeightingMats()  # Reset weights
        ang_hat_avg = (self.angs-self.angs_avg_stacked)
        ang_hat_ctr = (self.angs-self.c)

        self.pose_error = self.v_deltas_full.T.dot(self.Pf).dot(self.v_deltas_full)[0, 0]
        self.Acost = (ang_hat_avg).T.dot(self.alphas).dot(ang_hat_avg)[0, 0]
        self.Bcost = (ang_hat_ctr).T.dot(self.betas).dot(ang_hat_ctr)[0, 0]
        self.Kcost = self.KappaPenalty(self.angs)
        self.ManipScore = -self.GetManipScore()

        print "Pose Error: ", self.pose_error, "(", self.status, ")"
        print "Weight Alpha: ", self.Acost
        print "Weight Beta: ", self.Bcost
        print "Weight Kappa: ", self.Kcost
        print "Weight Gamma: ", self.ManipScore
        self.total_cost = self.Acost+self.Bcost+self.Kcost+self.ManipScore

    def KappaPenalty(self, angs):
        kappa_penalty = 0
        for j in range(self.adof):
            for i in range(self.narms-1):
                index1 = j+i*self.adof
                index2 = j+(i+1)*self.adof
                kappa_penalty += .5*(angs[index1][0]-angs[index2][0])**2*self.kappa
        return kappa_penalty

    def GetManipScore(self):
        Manip = 0
        for i, arm in enumerate(self.arms):
            J = arm.getJacBodyReal()
            inner = np.linalg.det(J.dot(J.T))
            if inner < 0:
                inner = 0
            Manip += np.sqrt(inner)
        return Manip/self.narms

    def Record(self):
        self.step_dict["EEterms"].append(copy(self.EEterms))
        self.step_dict["Aterms"].append(copy(self.Aterms))
        self.step_dict["Bterms"].append(copy(self.Bterms))
        self.step_dict["Kterms"].append(copy(self.Kterms))
        self.step_dict["Gterms"].append(copy(self.Gterms))

        self.step_dict["pose_error"].append(self.pose_error)
        self.step_dict["Acost"].append(copy(self.Acost))
        self.step_dict["Bcost"].append(copy(self.Bcost))
        self.step_dict["Kcost"].append(copy(self.Kcost))
        self.step_dict["ManipScore"].append(copy(self.ManipScore))
        if self.TrackEvolution and np.mod(self.steps_taken, self.plot_interval) == 0:
            self.step_dict["angs"].append(copy(self.angs))

    def PrintProgress(self):
        printstring = 'Iter: %02d  ' % self.steps_taken
        printstring += 'E_Pose: %02.5f ' % self.pose_error
        printstring += 'Alpha: %2.5f ' % self.Acost
        printstring += 'Beta: %2.5f ' % self.Bcost
        printstring += 'Kappa: %2.5f ' % self.Kcost
        printstring += 'ManipScore: %2.5f ' % self.ManipScore
        printstring += 'Step: %1.2f ' % self.step_scale
        printstring += 'A: %1.5f ' % self.alpha
        printstring += 'B: %1.5f ' % self.beta
        printstring += 'K: %1.5f ' % self.kappa
        printstring += 'G: %1.5f ' % self.gamma
        printstring += 'Improv: %1.5f ' % self.improvement
        print printstring

    def UpdateWeights(self):
        # A lot of play with this option
        if self.alpha > self.alpha_min:
            alpha_scale_step = self.alpha_scale_step
        else:
            alpha_scale_step = 1.0
        self.alpha = self.alpha * alpha_scale_step
        self.alphas = self.alphas * alpha_scale_step

        # Kappa Weight Update
        if self.kappa > self.kappa_min:
            kappa_scale_step = self.kappa_scale_step
        else:
            kappa_scale_step = 1.0
        self.kappa = self.kappa * kappa_scale_step
        self.kappas = self.kappas * kappa_scale_step

        # Beta Weight Update
        if self.beta > self.beta_min:
            beta_scale_step = self.beta_scale_step
        else:
            beta_scale_step = 1.0
        self.beta = self.beta * beta_scale_step
        self.betas = self.betas * beta_scale_step

        # Gamma Weight Update
        if self.gamma > self.gamma_min:
            gamma_scale_step = self.gamma_scale_step
        else:
            gamma_scale_step = 1.0
        self.gamma = self.gamma * gamma_scale_step

    def SetWeightingMats(self):
        # Create Single Arm
        single_arm_alphas = []
        single_arm_betas = []
        for i in range(self.adof):
            single_arm_alphas.append(self.alpha)
            single_arm_betas.append(self.beta)
        single_arm_alphas = scipy.linalg.block_diag(*single_arm_alphas)
        single_arm_betas = scipy.linalg.block_diag(*single_arm_betas)

        # Stack Alpha Matrices up in diag
        alphas = []
        betas = []
        for i in range(self.narms):
            alphas.append(single_arm_alphas)
            betas.append(single_arm_betas)

        alphas.append(np.zeros([self.bdof, self.bdof]))
        betas.append(np.zeros([self.bdof, self.bdof]))
        self.alphas = scipy.linalg.block_diag(*(alphas))
        self.betas = scipy.linalg.block_diag(*(betas))

        # Format Kappa Matrix: (Nontrivial)
        self.kappas = np.zeros([self.asdof, self.asdof])
        for j in range(self.adof):
            for i in range(self.narms-1):
                self.kappas[j+i*self.adof][j+i*self.adof] += .5
                self.kappas[j+(i+1)*self.adof][j+i*self.adof] += -.5
                self.kappas[j+i*self.adof][j+(i+1)*self.adof] += -.5
                self.kappas[j+(i+1)*self.adof][j+(i+1)*self.adof] += .5

        # Pad with zeros for base dof
        self.kappas = np.hstack([self.kappas, np.zeros([self.asdof, self.bdof])])
        self.kappas = np.vstack([self.kappas, np.zeros([self.bdof, self.tdof])])
        self.kappas *= self.kappa

        # Weighting On Dimensions in Problem
        weights = np.empty([1, 0])
        for i in range(self.narms):
            weights = np.append(weights, np.hstack([self.vec_weight[i, :], self.rot_weight[i, :]]))
        self.Pf = np.diag(weights)

    def SetWeightsZero(self):
        self.alpha *= 0
        self.alphas *= 0
        self.beta *= 0
        self.betas *= 0
        self.kappas *= 0
        self.kappa *= 0
        self.gamma *= 0

    def UnpackAngsArm(self):
        angs = np.zeros([self.asdof, 1])
        for i, arm in enumerate(self.arms):
            angs[self.adof*i:self.adof*(i+1)] = arm.getJointAngsReal()
        return angs

    def UnpackAngsBase(self):
        base_angs = np.zeros([self.bdof, 1])
        for i in range(self.bdof):
            base_angs[i] = self.base.g_world_base[i][3]
        return base_angs


def scipy_sparse_to_spmatrix(A):
    coo = A.tocoo()
    SP = spmatrix(coo.data.tolist(), coo.row.tolist(), coo.col.tolist(), size=A.shape)
    return SP
