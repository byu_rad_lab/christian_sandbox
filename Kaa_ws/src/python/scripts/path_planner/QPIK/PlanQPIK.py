#!/usr/bin/env python
import numpy as np
from ego_byu.tools import constructors
from ego_byu.tools import paths
# from ego_byu.tools import constraints as cts
from ego_byu.tools.spline_planner.path_planner import AngleSmoothener

from PathIK import PathIK
import matplotlib.pyplot as plt
from PlannerGUI import PlannerGUI
from copy import deepcopy
import time


class Planner(object):

    def __init__(self, path_function, arms, base, constraints, smoothen=False, init_config=[]):
        self.path_function = path_function

        # path function can be a parameterized path OR a list of targets
        if callable(path_function):
            self.narms = len(arms)
            self.arms = arms
        else:
            self.narms = len(path_function)
            self.arms = np.array([])
            for i in range(len(path_function["targets"])):
                self.arms = np.append(self.arms, deepcopy(arms[0]))

        self.base = base
        self.constraints = constraints
        self.smoothen = smoothen
        self.smoothpts = 50

        self.narms_ref = 3  # arms between each pose pair for refinement
        self.FinalPathConfigs = []
        self.FinalPathVels = []
        self.FinalPathAccs = []
        self.FinalPathTimes = []
        self.Targets = []
        self.Toplevel = PathIK(self.arms, self.base, self.path_function, self.constraints, init_config, False)

        # Allows for Recursive Adjustment between arms, if distance between two homs on path is greater than
        # this, the path is refined. With cvxopt it becomes increasingly harder to
        # tune this parameter.  It's better just to use a lot of arms and branch in one PathIK. with CVXGen
        # we must use this refinement operation because cvxgen is limited in dimensionality (24 DOFs max or
        # about 4 6DOF arms max). In general CVXOpt is linear in the number of arms in high level
        self.refinement_tol = 100  # distance between points, 100 meters essentially turns this off

        # Commands sent to conttroller
        self.commands = []

    # Plan High Level (current config option added)
    def Plan(self, current_configuration=[]):
        # self.Toplevel.solver = "cvxgen"
        self.Toplevel.solver = "cvxopt"
        self.Toplevel.TrackEvolution = True
        self.Toplevel.plot_interval = 10
        self.Toplevel.Solve(0.0, 1.0)  # Sets Arms to Desired Positions
        refinestarttime = time.time()
        self.Refine(self.Toplevel.arms, self.Toplevel.targets, 0.0, 1.0)

        # Record Last Arm
        self.RecordArm(self.Toplevel.arms[-1], self.Toplevel.targets[-1])
        self.RefineTime = time.time()-refinestarttime

        # Smooths Joint Trajectories Using Spliner
        if self.smoothen:
            self.SplinePath()

    def LaunchGUI(self):
        self.GUI = PlannerGUI(self.Toplevel, self.Targets, self.FinalPathConfigs, self.FinalPathTimes)
        self.GUI.Show()
        plt.show()

    # Recursive refinement is only slightly faster than just planning with many arms
    # It's simpler and about as efficient to plan with many, and spline configurations together
    # Refines target path - Must have a parameterized path for this to work
    def Refine(self, arms, targets, p1, p2):
        pvals = np.linspace(p1, p2, len(arms))
        for i in range(len(arms)-1):
            self.Refine_Between(arms[i], arms[i+1], pvals[i], pvals[i+1], targets[i])

    # Fixes boarder configurations and then refines recursively between arms
    def Refine_Between(self, arm1, arm2, p1, p2, target1):
        # If path function is not parameterized func, don't refine
        if p2-p1 > self.refinement_tol and callable(self.path_function):
            arms = [deepcopy(arm1)]+self.AddArmsInterpolated(arm1, arm2, p1, p2)+[deepcopy(arm2)]
            base = constructors.BaseFixed()
            base.g_world_base = self.Toplevel.base.g_world_base
            mpik = PathIK(arms, base, self.path_function, self.constraints, [], True)
            mpik.solver = "cvxopt"  # Low Dimensionality Should Handle Well
            mpik.max_iter = 100  # Refinement Max Iterations, may be worth adjusting
            mpik.refine = True  # Fixes Base and First and Last Arms
            mpik.alpha = self.Toplevel.alpha
            mpik.beta = self.Toplevel.beta
            mpik.kappa = self.Toplevel.kappa
            mpik.gamma = self.Toplevel.gamma
            mpik.update_wts_below_improvement = -float("inf")  # Don't Update Weights in Refinement
            mpik.SetWeightingMats()
            mpik.Solve(p1, p2)
            self.Refine(mpik.arms, mpik.targets, p1, p2)  # Recursive Refinement
        else:
            # Tolerance achieved Record Arm
            self.RecordArm(arm1, target1)

    def AddArmsInterpolated(self, arm1, arm2, p1, p2):
        arms = []
        gammas = np.linspace(1.0/(self.narms_ref+1), 1.0, self.narms_ref, endpoint=False)
        for i in range(self.narms_ref):
            angles = (1-gammas[i])*arm1.getJointAngsReal()+(gammas[i])*arm2.getJointAngsReal()
            arm = deepcopy(arm1)
            arm.setJointAngsReal(angles)
            arm.calcJacReal()
            arms.append(arm)
        return arms

    def RecordArm(self, arm, target):
        self.FinalPathConfigs.append(arm.getJointAngsReal())
        self.Targets.append(target)

    # Linear Interpolation of Path provided by Tom's Spline Function
    def ScaleFinalPathTimes(self, total_time):
        start = self.FinalPathTimes[0]
        self.FinalPathTimes = self.FinalPathTimes-start
        scale_factor = total_time/self.FinalPathTimes[-1]
        self.FinalPathTimes = (self.FinalPathTimes)*scale_factor+start
        self.FinalPathVels = (self.FinalPathVels)/scale_factor
        self.FinalPathAccs = (self.FinalPathVels)/(scale_factor**2)

    # For Interpolation between Configs, now interpolation is done by addin more pts to spline discret.
    # def CreatePath(self,total_time):
        # Linear Interpolation between configurations
        # Points between path angles
        # t = np.linspace(0, 1, 101)
        # npts = len(self.FinalPathConfigs)
        # pts = len(t)*(npts-1)-(npts-2)
        # num_angs = len(self.FinalPathConfigs[0])
        # path = np.zeros([pts, num_angs*2+1])
        # for pt in range(len(self.FinalPathConfigs)-1):
        #     pt1 = self.FinalPathConfigs[pt]
        #     pt2 = self.FinalPathConfigs[pt+1]
        #     for ind, j in enumerate(t):
        #         if j != t[-1]:
        #             thetas = (1-j)*pt1+j*pt2
        #             index = pt*(len(t)-1)+ind
        #             path[index, 1:num_angs+1] = thetas.flatten()
        # path[-1, 1:num_angs+1] = self.FinalPathConfigs[-1].flatten()
        # path[:, 0] = np.linspace(0, total_time, pts)

        # # Differentiation
        # dt = path[1, 0]-path[0, 0]
        # path[0, num_angs+1:] = ((path[1, 1:(num_angs+1)]-path[0, 1:(num_angs+1)])/dt).flatten()
        # for i in range(1, pts-1):
        #     path[i, num_angs+1:] = ((path[i+1, 1:(num_angs+1)]-path[i-1, 1:(num_angs+1)])/(2*dt)).flatten()
        # path[-1, num_angs+1:] = ((path[-1, 1:(num_angs+1)]-path[-2, 1:(num_angs+1)])/dt).flatten()

    def SplinePath(self):
        order = 3
        planner = AngleSmoothener(order, self.FinalPathConfigs)
        planner.create_path()
        planner.plan()
        planner.optimize_time(4)

        sp = planner.spliner
        self.FinalPathTimes, self.state = sp.discretize(self.smoothpts)
        self.FinalPathConfigs = [np.array(angs) for angs in self.state[0]]
        self.FinalPathVels = [np.array(angs) for angs in self.state[1]]
        self.FinalPathAccs = [np.array(angs) for angs in self.state[2]]

if __name__ == "__main__":
    # Base Degrees of Freedom and limits on where the base can be put
    # base = constructors.BaseXYZ()
    # xlimits = np.array([-3, 3])  # Limits on region where base can be
    # ylimits = np.array([-3, 3]) #FIXED
    # zlimits = np.array([0,0]) #FIXED
    # constraints = [xlimits, ylimits, zlimits]
    # base.constraints = constraints

    base = constructors.BaseFixed()
    # xlimits = np.array([-3, 3])  # Limits on region where base can be
    # ylimits = np.array([-3, 3]) #FIXED
    # zlimits = np.array([0,0]) #FIXED
    # constraints = [xlimits, ylimits, zlimits]
    # base.constraints = constraints

    # Number and type of arms in high level planner
    narms = 20
    # arms = constructors.SimpleArms(narms,base)
    seed_angs = np.array([.1, .1, .1, .1, .1, .1])*3
    arms = constructors.NasaArms(narms, base, seed_angs)
    # arms = constructors.NasaArms(narms, base,)
    # arms = constructors.BlowMoldedArms(narms)

    # Path Chosen
    # path = paths.Krex_arc
    # path = paths.Sine
    # path = paths.Coupon
    # path = paths.LineY

    # Needs to get h1 from a subscriber eventually
    angs0 = np.array([0, -np.pi/2.0, 0, 0, 0, 0])
    arm = constructors.NasaArms(narms, base, angs0)[0]
    h1 = arm.getGWorldTool()
    h2, wv, wr = paths.LineY(0)

    path = paths.CombinePaths(paths.LineBetween(h1, h2), paths.LineY)
    # path = paths.SplineCoupon(50)

    # Constraints for Collision avoidance
    # point = np.array([.7, -.2, .5]).reshape(3, 1)
    # radius = .30
    # weight = 1
    # const =  {'type': "Sphere", 'point': point, 'radius': radius, "weight": weight}
    # constraints = [const]
    constraints = []

    # Plan!
    # HLP = Planner(path, arms, base, constraints, True)
    HLP = Planner(path, arms, base, constraints, True, angs0)
    s = time.time()
    HLP.Plan()
    HLP.ScaleFinalPathTimes(20)
    f = time.time()

    # print "Total Time: ", f-s
    # r, t = kt.hom2ax_vec(HLP.base.g_world_base)
    # print "Base Position: "
    # print r
    # print t

    HLP.LaunchGUI()
    plt.show()
