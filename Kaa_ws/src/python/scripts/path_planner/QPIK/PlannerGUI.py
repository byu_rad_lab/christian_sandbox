#!/usr/bin/env python
import numpy as np
from ego_byu.tools import arm_plotter as ap
import matplotlib.pyplot as plt
from copy import copy
from matplotlib.widgets import Slider, Button
import os
import datetime
import time
import subprocess
import glob
import yaml

today = datetime.date.today()
todaystr = today.isoformat()
np.set_printoptions(precision=5)


class PlannerGUI(object):

    def __init__(self, toplevel, PathTargets, FinalPathConfigs, FinalPathTimes):
        self.GUI_fig = plt.figure(1)
        self.toplevel = toplevel
        self.targets = PathTargets
        self.angs = FinalPathConfigs
        self.times = FinalPathTimes

        self.arm = toplevel.arms[0]
        self.arm_plotter = ap.SimpleArmPlotter(self.arm)
        self.plot_interval = 10
        self.show_krex = False
        self.show_geometries = False
        self.axesoff = False

        self.image_number = 1
        self.fpath = os.path.join("/home/danielbodily/Desktop/GeneratedFigures/", todaystr, "PathPlanning/")

    def Show(self):
        plt.figure(self.GUI_fig.number)
        ax = plt.gca(projection='3d')
        ax.set_xlim3d([-.5,1])
        ax.set_ylim3d([-1,.5])
        ax.set_zlim3d([0,1.5])
        self.GUI_fig.show()
        self.GUIax = ax

        # Set Up GUI
        plt.subplots_adjust(bottom=.25)
        col = 'lightgoldenrodyellow'
        ax_clear = plt.axes([.82, 0.15, .15, .05], axisbg=col)
        ax_anim_evol = plt.axes([.13, 0.15, .25, .04], axisbg=col)
        ax_slider_evol = plt.axes([.4, 0.15, 0.2, 0.03], axisbg=col)

        ax_anim_path = plt.axes([.13, 0.1, .25, .04], axisbg=col)
        ax_slider_path = plt.axes([.4, 0.1, 0.2, 0.03], axisbg=col)
        ax_button_show_krex = plt.axes([.65, 0.10, .1, .04], axisbg=col)
        ax_button_show_constr = plt.axes([.77, 0.10, .1, .04], axisbg=col)

        ax_button_show_terms = plt.axes([.15, 0.05, .17, .04], axisbg=col)
        ax_button_eepath = plt.axes([.35, 0.05, .17, .04], axisbg=col)
        ax_button_show_angs = plt.axes([.55, 0.05, .17, .04], axisbg=col)
        ax_button_save_all = plt.axes([.75, .05, .15, .04], axisbg=col)
        ax_button_show_first_ang = plt.axes([.65, .15, .15, .04], axisbg=col)

        # Buttons/Sliders
        self.config_slider = Slider(ax_slider_path, '', 0, len(self.angs)-1, valinit=10)
        self.evol_slider = Slider(ax_slider_evol, '', 0, len(self.toplevel.step_dict["angs"])-1, valinit=10)

        self.button_clear = Button(ax_clear, 'Clear Figure', color=col, hovercolor='0.97')
        self.button_anim_path = Button(ax_anim_path, 'Animate Path', color=col, hovercolor='0.97')
        self.button_anim_evol = Button(ax_anim_evol, 'Animate Optimization', color=col, hovercolor='0.97')
        self.button_draw_EEPath = Button(ax_button_eepath, 'Draw Refined Path', color=col, hovercolor='0.97')
        self.button_show_angs = Button(ax_button_show_angs, 'Jt. Trajectories', color=col, hovercolor='0.97')
        self.button_show_terms = Button(ax_button_show_terms, 'Show QP Terms', color=col, hovercolor='0.97')
        self.button_save_all = Button(ax_button_save_all, 'Save all', color=col, hovercolor='0.97')
        self.button_show_krex = Button(ax_button_show_krex, 'Krex', color=col, hovercolor='0.97')
        self.button_show_constr = Button(ax_button_show_constr, 'Geometries', color=col, hovercolor='0.97')
        self.button_show_first_ang = Button(
            ax_button_show_first_ang,
            'First Ang.',
            color=col,
            hovercolor='0.97')

        self.config_slider.on_changed(self.UpdatePath)
        self.evol_slider.on_changed(self.UpdateEvolution)

        self.button_clear.on_clicked(self.ClearFig)
        self.button_anim_path.on_clicked(self.AnimatePathButton)
        self.button_anim_evol.on_clicked(self.AnimateEvolButton)
        self.button_draw_EEPath.on_clicked(self.DrawRefinedPath)
        self.button_show_angs.on_clicked(self.PlotFinalPathAngs)
        self.button_show_terms.on_clicked(self.PlotQPTerms)
        self.button_save_all.on_clicked(self.SaveAll)
        self.button_show_krex.on_clicked(self.ShowKrex)
        self.button_show_constr.on_clicked(self.ShowGeometries)
        self.button_show_first_ang.on_clicked(self.ShowFirstAng)

        plt.sca(ax)
        plt.figure(self.GUI_fig.number).show()

    def UpdatePath(self, val):
        config_idx = int(self.config_slider.val)
        config = self.angs[config_idx]
        self.DrawConfig(config)
        self.ShowGeometries("Redraw")
        if self.axesoff:
            self.TurnOffAxes()
        self.GUI_fig.show()
        if plt.fignum_exists(2):
            self.PlotFinalPathAngs()


    def ClearFig(self, event):
        plt.figure(self.GUI_fig.number)
        self.GUIax.cla()
        self.GUIax.set_xlim3d([-1,1])
        self.GUIax.set_ylim3d([-1,1])
        self.GUIax.set_zlim3d([-1,1])

    def ShowFirstAng(self, event, save=False):
        config = self.angs[0]*0
        for i in np.linspace(-np.pi/2.0, np.pi/2.0, 10):
            config[0] = i
            self.DrawConfig(config)
            if save:
                name = 'image_%03d.png' % self.image_number
                self.SaveImg(self.GUI_fig.number, name)
        if save:
            for i in range(10):
                name = 'image_%03d.png' % self.image_number
                self.SaveImg(self.GUI_fig.number, name)
            self.CompileMovie("FirstAng")

    def ShowGeometries(self, event):
        if event != "Redraw":
            self.show_geometries = not self.show_geometries
        if self.show_geometries:
            for i, geom in enumerate(self.toplevel.geometries):
                self.GetAxis()
                if geom['type'] == "collision":
                    color = 'r'
                elif geom['type'] == "touch":
                    color = 'b'
                if geom['shape'] == 'Cylinder':
                    pt = (np.array(geom['point']-geom['direction']*2)).reshape(3)
                    pt2 = (np.array(geom['point']+geom['direction']*2)).reshape(3)
                    ap.plot_line(plt.gca(), pt, pt2, color, 10, .2)
                if geom['shape'] == 'Plane':
                    ap.plot_plane(plt.gca(), geom['point'], geom['normal'],color)
                if geom['shape'] == 'Sphere':
                    ap.plot_sphere(plt.gca(), geom['point'], geom['radius'],color)
                self.SetAxis()
            if self.show_krex:
                self.ShowKrex("Redraw")

    def UpdateEvolution(self, val):
        configs = self.toplevel.step_dict["angs"][int(self.evol_slider.val)]
        self.DrawConfigs(configs)

        if self.axesoff:
            self.TurnOffAxes()
        self.GUI_fig.show()
        # for arm in self.toplevel.arms:
        #     self.arm_plotter.arm = arm
        #     self.arm_plotter.plot_manipulability_ellipse(self.GUI_fig.gca())

        if plt.fignum_exists(3):
            self.PlotWeightedTerms()

    def SaveAll(self, event):
        if not os.path.exists(self.fpath):
            os.makedirs(self.fpath)
        self.AnimateEvol(self.toplevel.step_dict['angs'], True)
        self.AnimatePath(self.angs, True)
        self.DrawRefinedPath("clicked", True)
        self.ShowFirstAng("clicked", True)
        self.PlotFinalPathAngs("clicked", True)
        self.PlotQPTerms("clicked", True)
        self.SaveYAMLfile()

    def AnimatePathButton(self, event):
        self.AnimatePath(self.angs, False)

    def AnimateEvolButton(self, event):
        self.AnimateEvol(self.toplevel.step_dict['angs'])

    def AnimatePath(self, angs, save=False):
        plt.figure(self.GUI_fig.number)
        self.GetAxis()
        self.GUIax.cla()

        self.DrawTargets()
        self.show_geometries = True
        self.ShowGeometries("Redraw")
        # self.show_krex = True
        # self.ShowKrex("Redraw")
        self.PlotBase()

        start = time.time()
        t = time.time()-start
        while (t < self.times[-1]):
            i = np.argmax(self.times > t)
            self.DrawConfig(angs[i])
            self.DrawTargets()
            # self.DrawRefinedPath("Redraw")
            # self.ShowKrex("Redraw")
            self.ShowGeometries("Redraw")
            self.SetAxis()
            self.GUI_fig.canvas.draw()
            if save:
                name = 'image_%03d.png' % self.image_number
                self.SaveImg(self.GUI_fig.number, name)
            t = time.time()-start
        if save:
            print "SAVING"
            # Add extra images to prevent film cutting
            for i in range(10):
                name = 'image_%03d.png' % self.image_number
                self.SaveImg(self.GUI_fig.number, name)
            self.CompileMovie("Path")

    def AnimateEvol(self, angs, save=False):
        plt.figure(self.GUI_fig.number)
        self.GetAxis()
        for i in range(len(angs)):
            self.DrawConfigs(angs[i])
            self.SetAxis()
            if save:
                name = 'image_%03d.png' % self.image_number
                self.SaveImg(self.GUI_fig.number, name)
        if save:
            for i in range(10):
                name = 'image_%03d.png' % self.image_number
                self.SaveImg(self.GUI_fig.number, name)
            self.CompileMovie("Optimization")

    def DrawConfig(self, angs):
        plt.figure(self.GUI_fig.number)
        self.GetAxis()
        self.GUIax.cla()

        self.arm_plotter.color = 'k'
        self.arm_plotter.alpha = .3
        self.arm.setJointAngsReal(angs)
        self.arm.calcFK()
        self.arm_plotter.plot(self.GUIax)
        ap.plot_gnomon(plt.gca(),self.arm_plotter.arm.getGWorldTool())
        if self.toplevel.jacobian_used!="end_effector":
            ap.plot_gnomon(plt.gca(),self.arm_plotter.arm.links_[-self.toplevel.jacobian_used].g_world_com_)
        self.SetAxis()
        # self.DrawTargets()

    def DrawConfigs(self, angs):
        plt.figure(self.GUI_fig.number)
        self.GetAxis()
        self.GUIax.cla()

        self.arm_plotter.color = 'k'
        self.arm_plotter.alpha = 1
        if self.show_geometries:
            self.ShowGeometries("Redraw")
        self.PlotBase()

        for i in range(len(self.toplevel.arms)):
            angs_arm = angs[self.toplevel.adof*i:self.toplevel.adof*(i+1)]
            self.arm.setJointAngsReal(angs_arm)
            for j in range(self.toplevel.bdof):
                self.toplevel.base.g_world_base[j][3] = angs[-self.toplevel.bdof+j][0]
            self.arm.g_world_arm_ = np.dot(self.toplevel.base.g_world_base, self.toplevel.base.g_base_arm)
            self.arm.calcFK()
            self.arm_plotter.plot(self.GUI_fig.gca())
            # self.arm_plotter.plot_manipulability_ellipse(self.GUI_fig.gca())
        self.DrawTargets()
        self.SetAxis()
        self.GUI_fig.canvas.draw()


    def PlotBase(self):
        self.GetAxis()
        a = self.toplevel.base.g_world_base[:3, 3].reshape(3)
        b = np.dot(self.toplevel.base.g_world_base, self.toplevel.base.g_base_arm)[:3, 3].reshape(3)
        # ap.plot_cylinder(plt.gca(),a,b,'k',20)
        ap.plot_line(plt.gca(), a, b, 'k', 20)
        self.SetAxis()

    def DrawRefinedPath(self, event, save=False):
        self.GetAxis()
        time = 0
        EEpt = []
        for i, config in enumerate(self.angs):
            self.arm.setJointAngsReal(config)
            self.arm.calcFK()
            if i != 0:
                if self.toplevel.jacobian_used == "end_effector":
                    ap.plot_line(self.GUI_fig.gca(), EEpt, self.arm.getGWorldTool()[0:3, 3], 'c', 5,alpha = .2)
                    EEpt = self.arm.getGWorldTool()[0:3, 3]
                else:
                    ap.plot_line(self.GUI_fig.gca(), EEpt, self.arm.links_[-self.toplevel.jacobian_used].g_world_com_[0:3, 3], 'c', 5,alpha = .2)
                    EEpt = self.arm.links_[-self.toplevel.jacobian_used].g_world_com_[0:3, 3]
                if self.times != []:
                    time_p = time
                    time = self.times[i]
                    # if int(time) > int(time_p):
                    #     plt.gca().scatter(EEpt[0], EEpt[1], EEpt[2], s=70)
                elif np.mod(i, 4) == 0:
                    plt.gca().scatter(EEpt[0], EEpt[1], EEpt[2], s=20)
            EEpt = self.arm.getGWorldTool()[0:3, 3]
        self.SetAxis()

        if event != "Redraw":
            self.DrawTargetsRefined()

        if save:
            self.SaveImg(self.GUI_fig.number, "refined_path")

    def DrawTargetsRefined(self):
        self.GetAxis()
        for j in range(len(self.targets)):
            ap.plot_gnomon(self.GUIax, self.targets[j],length=.025)
        self.SetAxis()

    def DrawTargets(self):
        self.GetAxis()
        for j in range(len(self.toplevel.targets)):
            ap.plot_gnomon(self.GUIax, self.toplevel.targets[j])
        self.SetAxis()

    def PlotFinalPathAngs(self, event="clicked", save=False):
        self.path_angles_fig = plt.figure(2)
        plt.figure(self.path_angles_fig.number)
        plt.clf()
        ax = plt.gca()
        dof = self.toplevel.adof
        for i in range(self.toplevel.adof):
            angles = []
            color = [float(i) / dof, (dof - float(i)) / dof, 0]
            for j in range(len(self.angs)):
                angles.append(self.angs[j][i])

            if self.times != []:
                ax.plot(self.times, angles, c=color, label=r"$\theta_{i%s}$" % str(i + 1))
            else:
                ax.plot(range(len(angles)), angles, c=color, label=r"$\theta_{i%s}$" % str(i + 1))
        xlim = copy(plt.gca().get_xlim())
        ylim = copy(plt.gca().get_ylim())
        if self.times == []:
            plt.plot(self.config_slider.val*np.ones(2), [-10, 10], color='r')
        plt.gca().set_xlim(xlim)
        plt.gca().set_ylim(ylim)
        if self.times != []:
            plt.xlabel("time (s)")
        else:
            plt.xlabel("ith Configuration")
        plt.ylabel("Joint Angle (rad)")
        plt.legend()
        plt.title("Final Path Configurations")
        self.path_angles_fig.show()
        if save:
            self.SaveImg(2, "final_angs")

    def PlotQPTerms(self, event="clicked", save=False):
        self.weighted_terms_fig = plt.figure(3)
        plt.clf()
        EEterms = self.toplevel.step_dict['EEterms']
        # Aterms = self.toplevel.step_dict['Aterms']
        Bterms = self.toplevel.step_dict['Bterms']
        Kterms = self.toplevel.step_dict['Kterms']
        Gterms = self.toplevel.step_dict['Gterms']

        pose_error = self.toplevel.step_dict['pose_error']
        # Acost = self.toplevel.step_dict['Acost']
        Bcost = self.toplevel.step_dict['Bcost']
        Kcost = self.toplevel.step_dict['Kcost']
        ManipScore = self.toplevel.step_dict['ManipScore']

        plt.subplot(211)
        plt.plot(range(len(pose_error)), pose_error, 'r')
        # plt.plot(range(len(Acost)), Acost, 'b')
        plt.plot(range(len(Bcost)), Bcost, 'k')
        plt.plot(range(len(Kcost)), Kcost, 'm')
        plt.plot(range(len(ManipScore)), ManipScore, 'c')
        xlim = copy(plt.gca().get_xlim())
        ylim = copy(plt.gca().get_ylim())
        plt.plot(self.evol_slider.val*np.ones(2)*self.plot_interval, [-100, 100], color='r')
        plt.gca().set_xlim(xlim)
        plt.gca().set_ylim(ylim)
        plt.legend(
            [r'$\vert\vert J\delta\theta-\delta X_{des})\vert\vert_{Pf}^2$',
             # r'$\alpha\vert\vert \theta_i-\theta_{i.avg} \vert\vert$ + ',
             r'$\beta\vert\vert \theta_{ij}-\theta_{ij.ctr} \vert\vert^2$ ',
             r'$\kappa\vert\vert \theta_{ij}-\theta_{(i+1)j} \vert\vert^2$',
             r'$-\frac{\gamma}{k}\sum_{i=0}^k\sum_{j=0}^n\frac{d(\sqrt{det(J_iJ_i^T)})}{d\theta_{ij}}\delta\theta_{ij}$'])
        plt.xlabel('Step In Path Planning Algorithm')
        plt.ylabel('Weighted Objective Terms')
        plt.title('Objectives To Minimize')

        plt.subplot(212)
        plt.plot(range(len(EEterms)), EEterms, 'r')
        # plt.plot(range(len(Aterms)), Aterms, 'b')
        plt.plot(range(len(Bterms)), Bterms, 'k')
        plt.plot(range(len(Kterms)), Kterms, 'm')
        plt.plot(range(len(Gterms)), Gterms, 'c')
        xlim = copy(plt.gca().get_xlim())
        ylim = copy(plt.gca().get_ylim())
        plt.plot(self.evol_slider.val*np.ones(2)*self.plot_interval, [-100, 100], color='r')
        plt.gca().set_xlim(xlim)
        plt.gca().set_ylim(ylim)
        plt.xlabel('Step In Path Planning Algorithm')
        plt.ylabel('Weighted Terms (Negative Implies Reward)')
        plt.title('Terms Effect in QP (Goal is to Minimize Sum)')
        plt.figure(3).set_size_inches(9, 13, forward=True)
        self.weighted_terms_fig.show()
        if save:
            self.SaveImg(3, "QP_terms")

    def SaveYAMLfile(self):
        name = "configs.yaml"
        file = self.fpath+name
        with open(file, 'w') as outfile:
            for config in self.angs:
                angs = config.T
                for j in range(len(angs)):
                    angs[j] = round(1000*angs[j])/1000
                outfile.write(yaml.dump(angs.tolist(), default_flow_style=True))

    def SaveImg(self, fignum, name):
        # For Generating Video
        plt.figure(fignum)
        file = self.fpath+name
        self.image_number += 1
        plt.savefig(file)
        print file

    def CompileMovie(self, name):
        os.chdir(self.fpath)
        subprocess.call(
            "ffmpeg -f image2 -r 10 -i image_%03d.png -vcodec mpeg4 -y " + name + ".mp4",
            shell=True)
        print " Video Generated "

        images = self.fpath + "/image_***.png"
        for file in glob.glob(images):
            os.remove(file)
        self.image_number = 1

    def ShowKrex(self, event):
        if event != "Redraw":
            self.show_krex = not self.show_krex
        if self.show_krex:
            self.GetAxis()
            ap.plot_krex(plt.gca())
            plt.xlabel('x')
            plt.ylabel('y')
            self.SetAxis()


    def TurnOffAxes(self):
        # self.GUI_fig.gca().xaxis.set_visible(False)
        # self.GUI_fig.gca().yaxis.set_visible(False)
        # self.GUI_fig.gca().zaxis.set_visible(False)
        # self.GUI_fig.gca().w_xaxis.line.set_lw(0.)
        # self.GUI_fig.gca().set_xticks([])
        # self.GUI_fig.gca().w_yaxis.line.set_lw(0.)
        # self.GUI_fig.gca().set_yticks([])
        # self.GUI_fig.gca().axis("off")
        # self.GUI_fig.gca().grid("off")
        # print self.GUI_fig.gca().axim = 0
        # print self.GUI_fig.gca().ayim = 0
        # print self.GUI_fig.gca().azim = 0
        self.GUI_fig.gca().view_init(elev=10, azim=-80)
        # print self.GUI_fig.gca().elev
        # print self.GUI_fig.gca().azim

    def GetAxis(self):
        self.xlim = copy(self.GUI_fig.gca().get_xlim3d())
        self.ylim = copy(self.GUI_fig.gca().get_ylim3d())
        self.zlim = copy(self.GUI_fig.gca().get_zlim3d())

    def SetAxis(self):
        self.GUI_fig.gca().set_xlim3d(self.xlim)
        self.GUI_fig.gca().set_ylim3d(self.ylim)
        self.GUI_fig.gca().set_zlim3d(self.zlim)
        plt.xlabel("x(m)")
        plt.ylabel("y(m)")
