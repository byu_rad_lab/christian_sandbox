#!/usr/bin/env python
from ego_core.kinematics import kinematics as kin
import numpy as np
import scipy.linalg
from scipy import sparse
from ego_byu.cvxgen import cvxgen
from cvxopt import matrix, solvers, spmatrix
from collections import defaultdict
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import arm_plotter as ap
import matplotlib.pyplot as plt
import qpik
from copy import copy
from copy import deepcopy
from matplotlib.widgets import Slider, Button
import os
import datetime
import subprocess
import des_paths
import constructors

solvers.options['show_progress'] = False
np.set_printoptions(linewidth=200)
np.set_printoptions(suppress=True)
np.set_printoptions(precision=15)

# For Generating Video
image_number = 1
today = datetime.date.today()
todaystr = today.isoformat()
filepath = os.path.join("/home/rtpc-devel/Desktop/GeneratedFigures/", todaystr, "PathPlanning/")

# class Planner(object):


class PATH_QPIK(object):
    def __init__(self, arms, base, mode="3D",boundary_configs = []):  # ARM ARCHITECTURE
        self.refine = False
        self.mode = mode
        # ------------------------------ARM ARCHITECTURE---------------------------------
        # ARMS
        self.arms = arms
        self.narms = len(arms)
        self.temp_arms = deepcopy(arms)

        self.adof = len(arms[0].getJointAngsReal())
        self.asdof = self.adof*self.narms

        if len(boundary_configs)!=0:
            self.refine = True
            self.SetArmsInterpolated(boundary_configs)

        # BASE
        self.base = base
        self.bdof = base.bdof
        self.tdof = self.asdof+self.bdof
        self.base.dmax = .01

        # Structures for Angle Measurements
        self.angs_delta_full = np.zeros([self.tdof,1])
        self.dth_max = 20*np.pi/180  # - Keep above 5 deg - Constraint Generally Hinders Optimization
        self.angs = self.UnpackAngs()
        self.angs_avg = np.zeros([self.adof, 1])
        self.angs_avg_stacked = np.zeros([self.narms*self.adof, 1])

        # Format Matrices
        self.SetConstraints()
        # -------------------WEIGHTING---------------------------------
        self.update_wts_below_improvement = .01
        # Weighting Alpha: Penalty on Joints being away from joint averages (all Joints)
        self.alpha = 10  # Keep in range between .1 and 10
        self.alpha_scale_step = .95  # Generally between .88 and .95
        self.alpha_min = 1e-7  # Good to keep minimum, but very small to allow convergence

        # Weighting Beta: Penalty on Joints being away from joint centers
        self.beta = 10
        self.beta_scale_step = .9
        self.beta_min = 1e-7  # Good to keep minimum, but very small to allow convergence

        # Weighting Kappa: Penalty on Joints being away from neighboring joints
        self.kappa = 10  # Keep in range between .1 and 10
        self.kappa_scale_step = .90  # Generally between .88 and .95
        self.kappa_min = 1e-7  # Good to keep minimum, but very small to allow convergence

        # Weighting Gamma: Penalty on Decreasing Manipulability
        self.gamma = 0  # Keep in range between .1 and 10
        self.gamma_scale_step = .95  # Generally between .88 and .95
        self.gamma_min = 1e-7  # Good to keep minimum, but very small to allow convergence

        # Weighting on Movement In Optimization
        rho = 5.0e-2  # Good to keep very small - alpha already constrains movement
        self.Pth = rho * np.eye(self.asdof+self.bdof)

        # Jacobian Wighting Matrix
        self.vec_weight = 1.00 * np.ones(3)
        self.rot_weight = 0.1 * np.ones(3)
        if self.mode == '3D':
            self.rot_weight[2] = 0.0  # don't solve for rotation about hand z-axis
        self.weights_arm = np.hstack((self.vec_weight, self.rot_weight))
        weights = np.empty([1, 0])
        for i in range(self.narms):
            weights = np.append(weights, self.weights_arm)
        self.Pf = np.diag(weights)

        # Format Matrices
        self.SetWeightingMats()
        # -------------------------Clamping/LineSearch---------------------------------
        # ratio of rotational to translational 1.0 means 1 meter and 90 degrees equivalent
        self.clamp_goals = True
        self.rot_clamp_max = 30.0 * np.pi/180.0  # radians
        self.vec_clamp_max = 1.5

        self.use_line_search = True  # True: Linesearch,False: Clamp Angs
        self.step_scale = .3  # Initialized at 1.0 for line search, default for clamping
        self.min_step_scale = 1e-2  # Keep around 1e-2, theoretically the line search should stay away from
        self.step_scale_factor = .95
        # -----------------------------Algorithm---------------------------------
        # self.cvxgen_on = True #Can't handle more than 26 DOf!
        self.cvxgen_on = False  # Can't handle more than 26 DOf!
        self.steps_taken = 0
        self.improvement_cutoff = 1e-5
        self.max_iter = 1000
        self.error_tol = 1e-6
        self.error_improv_tol = -100.0  # turn off improv_tol, more or less
        self.max_step = 1.0  # capping the maximum step we take
        self.allowable_error = .02  # Definitely needs to be tuned to application/Dimension

        # Null Space Projection Option
        self.zero_step_weight = 0.05

        # Error and cost terms
        self.pose_error = float('inf')
        self.Acost = float('inf')
        self.Bcost = float('inf')
        self.Kcost = float('inf')
        self.Gcost = float('inf')
        self.total_cost = float('Inf')

        # Plotting
        self.plot = True
        self.arm_plotter = ap.SimpleArmPlotter(arms[0], self.mode)

        self.path_evolution_fig = plt.figure(1)
        self.animation_fig = plt.figure(2)
        self.path_evolution_fig.gca(projection='3d')
        if self.plot:
            self.path_angles_fig = plt.figure(3)
            self.error_fig = plt.figure(4)
            self.line_search_fig = plt.figure(5)

        if mode == "3D":
            self.arm_plotter.mode = "3D"
            self.animation_fig.gca(projection='3d')
            if self.plot:
                self.path_evolution_fig.gca(projection='3d')


        # Interpolation
        self.angs_interp = []
        self.targets_interp = []
        self.EEHoms_interp = []

        # FinalPaths
        self.angs_IK = []
        self.EEHoms_IK = []

        # Recording
        self.step_dict = defaultdict(list)

    # Repeatedly step til stuck or solved
    def Solve(self,path_function,start,end):
        self.g_world_goals = self.CreatePath(path_function,start,end)
        self.UpdateArms()
        for i in range(self.max_iter):
            self.Step()
            self.steps_taken = i + 1
            self.PrintProgress()
            if self.pose_error < self.error_tol and self.improvement < self.improvement_cutoff:
                self.status = 'Solved'
                break
            if self.improvement < self.error_improv_tol:
                self.status = 'Stuck'
                break
            if i == self.max_iter-1:
                self.status = 'Max Iterations'
                break
        print "Status: ", self.status
        print "Generating Interpolated Path and Simple IK Path"
        self.PlotEvolution()
        self.InterpolatePath()
        self.IKAlongPath()
        # self.DrawTargets()
        print "Paths Generated"

    def CreatePath(self,pf,start,end):
        t = np.linspace(start,end,self.narms,endpoint=True)
        goals = []
        for i in t:
            x,y,z = pf(i)
            goals.append(kt.ax_vec2homog(np.zeros(3),np.array([x,y,z])))
        return goals

    def Step(self):
        J = []  # Stack J in Diagonal Structure
        Jb = []  # Stack Jb on Left to Contribute

        v_delta = np.empty([0, 1])
        self.v_deltas = np.empty([0, 1])
        for i, arm in enumerate(self.arms):
            g_world_tool = arm.getGWorldTool()
            J.append(arm.getJacBodyReal())
            Jb.append(kin.AdjFromHom(kin.HomInv(g_world_tool)).dot(self.base.JBase))  # Body Jacobian

            # Figure out delta position, clamp, turn into twist
            g_delta_full = kin.HomInv(g_world_tool).dot(self.g_world_goals[i])
            if self.clamp_goals is True:
                g_delta = self.ClampGoal(g_delta_full)
            else:
                g_delta = g_delta_full
            # v_delta = np.vstack([v_delta,kin.TwistFromHom(g_delta)])  # right, but produces instabilities
            # self.v_deltas =
            # np.vstack([self.v_deltas,kin.TwistFromHom(g_delta_full)])  # save to
            # calculate error
            v_delta = np.vstack([v_delta, kin.VeeHom(g_delta)])  # not right, but works well
            self.v_deltas = np.vstack([self.v_deltas,
                                       kin.VeeHom(g_delta_full)])  # save to calculate error

        Jb = np.vstack(Jb)
        J = np.hstack([Jb, scipy.linalg.block_diag(*J)])

        # Set up the QP
        # Quadratic Terms
        JPfJ = J.T.dot(self.Pf).dot(J)
        AA = self.alphas
        BB = self.betas
        KK = self.kappas

        # Linear Terms
        AAL = -2*self.alphas.T.dot(self.angs_avg_stacked)+2*self.alphas.T.dot(self.angs)
        BBL = -2*self.betas.T.dot(self.c)+2*self.betas.T.dot(self.angs)
        KKL = self.GetKKL(self.angs)
        self.MML = self.GetManipDerivs()

        JPfJL = -2*np.dot(J.T.dot(self.Pf), v_delta)
        # self.P = 2 * matrix(JPfJ + AA + BB + KK + self.Pth)  # 2 Cancels CVXOpts (1/2) term
        self.P = JPfJ + AA + BB + KK + self.Pth  # 2 Cancels CVXOpts (1/2) term
        self.B = JPfJL+KKL+AAL+BBL+self.MML

        G = self.A
        dth_to_jt_stop = self.b + self.A.dot(self.c - self.angs)
        h = np.zeros(np.shape(dth_to_jt_stop))
        for i in range(len(dth_to_jt_stop)):
            h[i, 0] = min(self.d[i], dth_to_jt_stop[i])

        if self.refine:
            self.P = self.P[(self.bdof+self.adof):(self.tdof-self.adof),(self.bdof+self.adof):(self.tdof-self.adof)]
            self.B = self.B[(self.bdof+self.adof):(self.tdof-self.adof)]
            G = self.A[:,(self.bdof+self.adof):(self.tdof-self.adof)]
            for i in reversed(range(len(G[:,0]))):
                if np.all(G[i, :] == 0):
                    G = np.delete(G, i, 0)
                    h = np.delete(h, i, 0)

        # Constraint Formats: Constrains by min(dth, joint_stop position for each dof direction)
        PSparse = scipy_sparse_to_spmatrix(scipy.sparse.coo_matrix(self.P))
        GSparse = scipy_sparse_to_spmatrix(scipy.sparse.coo_matrix(G))
        # For setting max motion
        self.B = matrix(self.B)
        self.h = matrix(h)

        # solve the QP
        if self.cvxgen_on:
            if np.shape(self.P)[0] > 25:
                print "Swig not implemented to wrap above Dimension 32"
                print "Cvxgen handles only about 24 DOF"
                assert(False)
            solver = cvxgen.CVXGen(np.array(self.P), np.array(self.B), np.array(self.G), np.array(self.h))
            solver.Solve()
            self.angs_delta_full = solver.x_
        else:
            # QP cvxopt solver
            # self.sol = solvers.qp(self.P, self.B, self.G, self.h)
            # if self.refine:
            self.sol = solvers.qp(PSparse, self.B, GSparse, self.h)
            self.angs_delta_full = np.array(self.sol['x'])

        # Plotting Objective Function And Search Direction and Evolution
        if self.plot and np.mod(self.steps_taken, 10) == 0:
            # self.PlotSearch()
            self.PlotEvolution()

        if self.use_line_search is True:
            self.LineSearch()
        else:
            self.ClampAngsDelta()

        # update the angles
        self.angs += self.angs_delta
        self.UpdateArms()

        self.CalcError()
        if self.improvement < self.update_wts_below_improvement:
            self.UpdateWeights()
        self.Record()

    # retrieves linear terms from
    def GetKKL(self, angs):
        KKL = np.zeros([self.asdof, 1])
        for j in range(self.adof):
            for i in range(self.narms-1):
                index1 = j+i*self.adof
                index2 = j+(i+1)*self.adof
                KKL[index1][0] += angs[index1+self.bdof]-angs[index2+self.bdof]
                KKL[index2][0] += -angs[index1+self.bdof]+angs[index2+self.bdof]
        KKL = np.vstack([np.zeros([self.bdof, 1]), KKL])*self.kappa
        return KKL

    def GetManipScore(self):
        Manip = 0
        for i, arm in enumerate(self.arms):
            J = arm.getJacBodyReal()
            Manip += np.sqrt(np.linalg.det(J.dot(J.T)))
        return Manip/self.narms

    def GetManipDerivs(self):
        dth = 1e-9
        dMdth = np.zeros([self.tdof, 1])
        k = self.bdof
        for i, arm in enumerate(self.temp_arms):
            thetas = self.arms[i].getJointAngsReal()
            arm.setJointAngsReal(thetas)
            arm.calcJacReal()
            J = arm.getJacBodyReal()
            M0 = np.sqrt(np.linalg.det((J.dot(J.T))))
            for j in range(len(thetas)):
                th_d = copy(thetas)
                th_d[j] += dth
                arm.setJointAngsReal(th_d)
                arm.calcJacReal()
                J = arm.getJacBodyReal()
                M1 = np.sqrt(np.linalg.det((J.dot(J.T))))
                dMdth[k] = (M1-M0)/dth*-1  # Decrease Manipulability -> Cost
                k += 1

        return dMdth*self.gamma

    def UpdateArms(self):
        self.angs_avg = np.zeros([self.adof, 1])
        self.angs_avg_stacked = np.zeros([self.bdof+self.asdof, 1])  # Alphas Base = 0
        for i, arm in enumerate(self.arms):
            if not (self.refine and (i!=0 or i!=self.narms-1)):
                angs = self.angs[self.adof*i+self.bdof:self.adof*(i+1)+self.bdof]
                self.angs_avg += angs
                arm.setJointAngsReal(angs)
                g_world_arm = arm.g_world_arm_
                for i in range(self.bdof):
                    g_world_arm[i][3] = self.angs[i][0]
                arm.g_world_arm_ = g_world_arm
                arm.calcJacReal()

        self.angs_avg = self.angs_avg/self.narms
        for i in range(self.narms):
            self.angs_avg_stacked[self.adof*i+self.bdof:self.adof*(i+1)+self.bdof] = self.angs_avg

    def UpdateWeights(self):
        # A lot of play with this option
        # and self.alphas[3,3] >self.alpha_min:
        if self.alphas[self.bdof+1, self.bdof+1] > self.alpha_min:
            alpha_scale_step = self.alpha_scale_step
        else:
            alpha_scale_step = 1.0
        self.alpha = self.alpha * alpha_scale_step
        self.alphas = self.alphas * alpha_scale_step

        # Kappa Weight Update
        if self.kappas[self.bdof+1, self.bdof+1] > self.kappa_min:
            kappa_scale_step = self.kappa_scale_step
        else:
            kappa_scale_step = 1.0
        self.kappa = self.kappa * kappa_scale_step
        self.kappas = self.kappas * kappa_scale_step

        # Beta Weight Update
        if self.betas[self.bdof+1, self.bdof+1] > self.beta_min:
            beta_scale_step = self.beta_scale_step
        else:
            beta_scale_step = 1.0
        self.beta = self.beta * beta_scale_step
        self.betas = self.betas * beta_scale_step

        # Gamma Weight Update
        if self.gamma > self.gamma_min:
            gamma_scale_step = self.gamma_scale_step
        else:
            gamma_scale_step = 1.0
        self.gamma = self.gamma * gamma_scale_step


    # # Clamp Goal to be Nearer
    def ClampGoal(self, g_delta):
        rot_mag = np.linalg.norm(kin.RotToAx(g_delta[:3, :3]))
        vec_mag = np.linalg.norm(g_delta[:3, 3])

        rot_ratio = rot_mag / self.rot_clamp_max
        vec_ratio = vec_mag / self.vec_clamp_max
        max_ratio = max(rot_ratio, vec_ratio)

        if max_ratio > 1.0:
            return kin.HomScale(g_delta, 1.0 / max_ratio)
        else:
            return g_delta

    # Clamp angs to be closer if too much twist
    def ClampAngsDelta(self):
        angs_delta_scale = self.angs_delta_full * self.step_scale
        mag = np.linalg.norm(angs_delta_scale[self.bdof:], 2)
        if mag > self.max_step:
            self.angs_delta = angs_delta_scale / mag * self.max_step
        else:
            self.angs_delta = angs_delta_scale

    def LineSearch(self):
        self.step_scale = 1.0
        objective = float("inf")
        while(True):
            angs_delta_scale = self.angs_delta_full*self.step_scale
            angs = self.angs+angs_delta_scale
            obj = self.ObjNonlinear(angs)
            if obj > objective or self.step_scale < self.min_step_scale:
                self.step_scale *= 1.0/self.step_scale_factor
                break
            else:
                self.step_scale *= self.step_scale_factor
                objective = obj
        self.angs_delta = self.angs_delta_full*self.step_scale
        return

    def ObjNonlinear(self, angs):
        # Objective Function: Minimize: (V_delta.T*Pf*V_delta)
    #              + (angs-avgs).T*alphas*(angs-avgs)
    #              + (angs-jc).T*betas*(angs-jc) - Sum of differences between angle and jc
        #              + .5*kappa*sum((th_i-th_(i+1))^2) - Sum of squared differences of neighboring terms
        angs_avg = np.zeros([self.adof, 1])
        # angs_avg_stacked = np.zeros([self.bdof+self.asdof, 1])
        v_deltas = np.zeros([6*self.narms, 1])
        for i, arm in enumerate(self.temp_arms):
            arm_angles = angs[self.bdof+i*self.adof:self.bdof+(i+1)*self.adof]
            arm.setJointAngsReal(arm_angles)
            g_world_arm = arm.g_world_arm_
            for j in range(self.bdof):
                g_world_arm[j][3] = angs[j][0]
            arm.g_world_arm_ = g_world_arm
            arm.calcFK()
            g_start = arm.getGWorldTool()  # Figure out delta position, clamp, turn into twist
            g_delta = kin.HomInv(g_start).dot(self.g_world_goals[i])
            # v_deltas = np.vstack([v_deltas,kin.TwistFromHom(g_delta)])  # right, but produces instabilities
            v_deltas[6*i:6*i+6] = kin.VeeHom(g_delta)  # not right, but works well
            angs_avg += arm_angles

        # Compute error on Twist
        pose_error = v_deltas.T.dot(self.Pf).dot(v_deltas)[0, 0]

        # Compute Difference Between Joint Angles and Their Avgs
        # angs_avg = angs_avg/self.narms
        # for i in range(self.narms):
        #     angs_avg_stacked[self.adof*i+self.bdof:self.adof*(i+1)+self.bdof] = angs_avg

        # ang_hat_avg = (angs-angs_avg_stacked)
        # ang_hat_ctr = (angs-self.c)
        # alpha_penalty = ang_hat_avg.T.dot(self.alphas).dot(ang_hat_avg)[0, 0]
        # beta_penalty = ang_hat_ctr.T.dot(self.betas).dot(ang_hat_ctr)[0, 0]
        # kappa_penalty = self.KappaPenalty(angs)
        # return pose_error+alpha_penalty+beta_penalty+kappa_penalty
        return pose_error  # +alpha_penalty+beta_penalty+kappa_penalty

    def ObjLinear(self, delta_angs):
        return (.5*delta_angs.T.dot(self.P).dot(delta_angs)+delta_angs.T.dot(self.B))[0, 0]

    def KappaPenalty(self, angs):
        kappa_penalty = 0
        for j in range(self.adof):
            for i in range(self.narms-1):
                index1 = j+i*self.adof
                index2 = j+(i+1)*self.adof
                kappa_penalty += .5*(angs[self.bdof+index1][0]-angs[self.bdof+index2][0])**2*self.kappa

        return kappa_penalty

    def CalcError(self):
        self.pose_error = self.v_deltas.T.dot(self.Pf).dot(self.v_deltas)[0, 0]
        ang_hat_avg = (self.angs-self.angs_avg_stacked)
        ang_hat_ctr = (self.angs-self.c)

        self.Acost = (ang_hat_avg).T.dot(self.alphas).dot(ang_hat_avg)[0, 0]
        self.Bcost = (ang_hat_ctr).T.dot(self.betas).dot(ang_hat_ctr)[0, 0]
        self.Kcost = self.KappaPenalty(self.angs)
        self.Gcost = self.GetManipScore()

        self.improvement = self.total_cost
        self.total_cost = self.pose_error + self.Acost + self.Bcost+self.Kcost
        self.improvement = self.improvement-self.total_cost

    def Record(self):
        self.step_dict["pose_error"].append(self.pose_error)
        self.step_dict["Acost"].append(copy(self.Acost))
        self.step_dict["Bcost"].append(copy(self.Bcost))
        self.step_dict["Kcost"].append(copy(self.Kcost))
        self.step_dict["Gcost"].append(copy(self.Gcost))

    def UnpackAngs(self):
        angs = np.zeros([self.bdof+self.asdof, 1])
        angs[0: self.bdof] = self.UnpackBaseVars()
        for i, arm in enumerate(self.arms):
            angs[self.adof*i+self.bdof: self.adof*(i+1)+self.bdof] = arm.getJointAngsReal()
        return angs

    def UnpackBaseVars(self):
        base_angs = np.zeros([self.bdof, 1])
        for i in range(self.bdof):
            base_angs[i] = self.arms[0].g_world_arm_[i][3]
        return base_angs

    def SetConstraints(self):
        A_list = []  # Limits dth in either direction
        b_list = []  # Limits from zero in either direction (abs value)
        c_list = []  # Preferred position (Joint Center)
        d_list = []  # Constraints on movement from joint limits or dth max
        for i in range(self.bdof):
            A = np.array([[-1.0, 1.0]]).T
            b = np.array([self.base.constraints[i][0], self.base.constraints[i][1]])
            c = np.array([self.base.base_center[i]])
            d = np.array([self.base.dmax, self.base.dmax])

            A_list.append(A.copy())
            b_list.append(b.copy())
            c_list.append(c.copy())
            d_list.append(d.copy())

        for arm in self.arms:
            for link in arm.links_:
                A = link.joint().limits().a()
                b = link.joint().limits().b().flatten()
                c = link.joint().limits().c().flatten()

                A_list.append(A.copy())
                b_list.append(b.copy())
                c_list.append(c.copy())

        # make a big matrix for each
        self.A = scipy.linalg.block_diag(*A_list)
        self.b = np.array([np.hstack(b_list)]).T  # Defined as column Vecs
        self.c = np.array([np.hstack(c_list)]).T

        # Remove False Joints
        real_indices = []
        for i in range(self.bdof):
            real_indices.append(i)
        for i in range(len(self.arms)):
            for j in self.arms[0].getRealJointIdx():
                real_indices.append((j+(i*self.arms[0].num_dof_))[0]+self.bdof)
        for i in reversed(range(len(self.A[0][:]))):
            if i not in real_indices:
                self.A = np.delete(self.A, i, 1)
        for i in reversed(range(len(self.A[:, 0]))):
            if np.all(self.A[i, :] == 0):
                self.A = np.delete(self.A, i, 0)
                self.b = np.delete(self.b, i, 0)
        for i in reversed(range(len(self.c))):
            if i not in real_indices:
                self.c = np.delete(self.c, i, 0)
        for i in range(self.bdof, len(real_indices)):
            d_list.append(np.array([self.dth_max, self.dth_max]))
        self.d = np.array([np.hstack(d_list)]).T

    def SetWeightingMats(self):
        # Create Single Arm
        single_arm_alphas = []
        single_arm_betas = []
        for i in range(self.adof):
            single_arm_alphas.append(self.alpha)
            single_arm_betas.append(self.beta)
        single_arm_alphas = scipy.linalg.block_diag(*single_arm_alphas)
        single_arm_betas = scipy.linalg.block_diag(*single_arm_betas)

        # Stack Alpha Matrices up in diag
        alphas = []
        betas = []
        for i in range(self.narms):
            alphas.append(single_arm_alphas)
            betas.append(single_arm_betas)

        self.alphas = scipy.linalg.block_diag(np.zeros([self.bdof, self.bdof]), *alphas)
        self.betas = scipy.linalg.block_diag(np.zeros([self.bdof, self.bdof]), *betas)

        # Format Kappa Matrix: (Nontrivial)
        self.kappas = np.zeros([self.asdof, self.asdof])
        for j in range(self.adof):
            for i in range(self.narms-1):
                self.kappas[j+i*self.adof][j+i*self.adof] += .5
                self.kappas[j+(i+1)*self.adof][j+i*self.adof] += -.5
                self.kappas[j+i*self.adof][j+(i+1)*self.adof] += -.5
                self.kappas[j+(i+1)*self.adof][j+(i+1)*self.adof] += .5

        # Pad with zeros for base dof
        self.kappas = np.hstack([np.zeros([self.asdof, self.bdof]), self.kappas])
        self.kappas = np.vstack([np.zeros([self.bdof, self.bdof+self.asdof]), self.kappas])
        self.kappas *= self.kappa

    def SetWeightsZero(self):
        self.alpha = 0
        self.alphas*=0
        self.beta*=0
        self.betas*=0
        self.kappas*=0
        self.kappa*=0
        self.gamma*=0

    def IKAlongPath(self):
        arm = deepcopy(self.arms[0])
        arm.calcFK()
        self.arm_plotter.arm = arm
        qpik_arm = qpik.QPIK_SINGLE_ARM(arm)
        self.angs_IK = [arm.getJointAngsReal()]

        # QPIk to find ang combination
        for i in range(self.narms-1):
            qpik_arm.zero_step_weight = .05
            qpik_arm.error_term_tol = 1e-4
            qpik_arm.max_iter = 100
            qpik_arm.max_step = .2
            qpik_arm.Pf = np.diag(self.weights_arm)
            qpik_arm.step_scale = 0.3
            qpik_arm.error_improv_tol = -10
            qpik_arm.step_dict.clear()
            qpik_arm.des_angs = self.arms[i+1].getJointAngsReal()
            qpik_arm.solve(self.arms[i+1].getGWorldTool())
            self.angs_IK = self.angs_IK + qpik_arm.step_dict["angs"]
            if qpik_arm.status is not "Solved":
                print "WARNING - QPIK failed to solve between paths"
            # qpik_arm.arm.setJointAngsReal(self.arms[i+1].getJointAngsReal())

        for i in range(len(self.angs_IK)):
            arm.setJointAngsReal(self.angs_IK[i])
            arm.calcFK()
            self.EEHoms_IK.append(arm.getGWorldTool())

    # Animates,Interpolates,And Checks Error
    def InterpolatePath(self):
        my_arm = deepcopy(self.arms[0])
        for i in range(self.narms-1):
            angs1 = self.arms[i].getJointAngsReal()
            angs2 = self.arms[i+1].getJointAngsReal()
            for gamma in np.linspace(0, 1, 10):
                angles = (1-gamma)*angs1+(gamma)*angs2
                self.angs_interp.append(angles)
                path_target = kt.hom_interp2(self.g_world_goals[i], self.g_world_goals[i+1], gamma)
                self.targets_interp.append(path_target)
                my_arm.setJointAngsReal(angles)
                my_arm.calcFK()
                self.EEHoms_interp.append(my_arm.getGWorldTool())

    def SetArmsInterpolated(self,bconfigs):
        angs = []
        for gamma in np.linspace(0,1,self.narms,endpoint = True):
            angs.append((1-gamma)*bconfigs[0]+(gamma)*bconfigs[-1])
        for i,config in enumerate(angs):
            self.arms[i].setJointAngsReal(config)
            self.arms[i].calcFK()

    def AnimateGUI(self):
        self.arm_plotter.arm = self.temp_arms[0]
        self.arm_plotter.color = 'k'
        self.arm_plotter.line_width = 4
        plt.figure(self.animation_fig.number)
        self.animation_fig.show()
        ax = plt.gca()
        if self.mode == '2D':
            ax.set_xlim(-.5, 1.5)
            ax.set_ylim(-1, 1)
        if self.mode == '3D':
            ax.set_xlim3d(-.5, 1.5)
            ax.set_ylim3d(-1, 1)
            ax.set_zlim3d(-.5, 1.5)
            # ap.set_axes_equal(plt.gca())
        self.anim_ax = ax
        plt.subplots_adjust(bottom=.25)
        axcolor = 'lightgoldenrodyellow'
        axconfiginterp = plt.axes([.52, 0.05, 0.25, 0.03], axisbg=axcolor)
        axconfigik = plt.axes([.2, 0.05, 0.25, 0.03], axisbg=axcolor)
        axinterp = plt.axes([.5, 0.15, .30, .04], axisbg=axcolor)
        axpathinterp = plt.axes([.57, 0.1, .15, .04], axisbg=axcolor)
        axIK = plt.axes([.2, 0.15, .25, .04], axisbg=axcolor)
        axpathIK = plt.axes([.25, 0.1, .15, .04], axisbg=axcolor)
        self.sconfiginterp = Slider(axconfiginterp, '', 0, len(self.angs_interp)-1, valinit=10)
        self.sconfigik = Slider(axconfigik, '', 0, len(self.angs_IK)-1, valinit=10)
        self.sinterp = Button(axinterp, 'Animate Interpolated Path', color=axcolor, hovercolor='0.975')
        self.sIK = Button(axIK, 'Animate Simple IK', color=axcolor, hovercolor='0.975')
        self.spathinterp = Button(axpathinterp, 'Draw EE Path', color=axcolor, hovercolor='0.975')
        self.spathik = Button(axpathIK, 'Draw EE Path', color=axcolor, hovercolor='0.975')
        self.sconfiginterp.on_changed(self.UpdateAnimationInterp)
        self.sconfigik.on_changed(self.UpdateAnimationIK)
        self.sinterp.on_clicked(self.AnimateInterp)
        self.sIK.on_clicked(self.AnimateIK)
        self.spathinterp.on_clicked(self.DrawPathInterp)
        self.spathik.on_clicked(self.DrawPathIK)
        plt.sca(ax)
        plt.figure(self.animation_fig.number).show()

    def AnimatePath(self, angs):
        for i in range(len(angs)):
            self.DrawConfig(angs[i])
        plt.xlabel("x(m)")
        plt.ylabel("y(m)")
        plt.title("Animated Path")

    def DrawConfig(self, angs):
        plt.figure(self.animation_fig.number)

        if self.mode == "2D":
            self.xlim = copy(plt.gca().get_xlim())
            self.ylim = copy(plt.gca().get_ylim())
        if self.mode == "3D":
            self.xlim = copy(plt.gca().get_xlim3d())
            self.ylim = copy(plt.gca().get_ylim3d())
            self.zlim = copy(plt.gca().get_zlim3d())
        plt.cla()
        self.temp_arms[0].setJointAngsReal(angs)
        self.temp_arms[0].calcFK()
        self.DrawTargets()
        self.arm_plotter.plot(self.animation_fig.gca())
        if self.mode == "2D":
            plt.gca().set_xlim(self.xlim)
            plt.gca().set_ylim(self.ylim)
        if self.mode == "3D":
            plt.gca().set_xlim3d(self.xlim)
            plt.gca().set_ylim3d(self.ylim)
            plt.gca().set_zlim3d(self.zlim)
        self.animation_fig.canvas.draw()

    def UpdateAnimationInterp(self, val):
        config = self.angs_interp[int(self.sconfiginterp.val)]
        self.DrawConfig(config)
        self.animation_fig.show()

    def UpdateAnimationIK(self, val):
        config = self.angs_IK[int(self.sconfigik.val)]
        self.DrawConfig(config)
        self.animation_fig.show()

    def AnimateInterp(self, event):
        self.AnimatePath(self.angs_interp)
        self.DrawEEpts(self.EEHoms_interp, 'c')

    def AnimateIK(self, event):
        self.AnimatePath(self.angs_IK)
        self.DrawEEpts(self.EEHoms_IK, 'm')

    def DrawPathInterp(self, event):
        self.DrawEEpts(self.EEHoms_interp, 'c')
        self.DrawTargets()

    def DrawPathIK(self, event):
        if len(self.angs_IK) == 0:
            print "Generating IK Path"
            self.IKAlongPath()
            print "Path Generated"
        self.DrawEEpts(self.EEHoms_IK, 'm')
        self.DrawTargets()

    def DrawTargets(self):
        ax = plt.gca()
        for j in range(len(self.g_world_goals)):
            if self.mode == '2D':
                ap.plot_gnomon_2d(ax, self.g_world_goals[j])
            if self.mode == '3D':
                ap.plot_gnomon(ax, self.g_world_goals[j])

    def DrawArms(self, color='r'):
        ax = plt.gca()
        for k in range(self.narms):
            self.arm_plotter.arm = self.arms[k]
            self.arm_plotter.color = color
            self.arm_plotter.plot(ax)

    def DrawEEpts(self, EEHoms, color='c'):
        ax = plt.gca()
        for i in range(len(EEHoms)-1):
            if self.mode == '2D':
                a = EEHoms[i][0: 2, 3]
                b = EEHoms[i+1][0: 2, 3]
                ap.plot_line_2d(ax, [a[0], a[1]], [b[0], b[1]], color, 4)
            if self.mode == '3D':
                a = EEHoms[i][0: 3, 3]
                b = EEHoms[i+1][0: 3, 3]
                ap.plot_line(ax, [a[0], a[1], a[2]], [b[0], b[1], b[2]], color, 4)
        self.animation_fig.show()

    def CheckError(self):
        temp = 0
        for i in range(len(self.EEHoms_interp)):
            et, ep, er = kt.homog_error(self.EEHoms_interp[i], self.targets_interp[i], .03)
            temp = temp+et
            if et > self.allowable_error:
                print "Significant Deviation In Euclidean Path"
        print "Total Error: ", temp

    def ShowAlgorithmEvolution(self):
        plt.figure(self.path_evolution_fig.number)
        self.DrawTargets()
        self.DrawArms('k')
        plt.xlabel("x")
        plt.ylabel("y")
        plt.title("Evolution Progress")
        plt.figure(self.path_evolution_fig.number).show()

    def PlotFinalPathAngs(self):
        plt.figure(self.path_angles_fig.number)
        ax = self.path_angles_fig.gca()
        for i in range(self.adof):
            angles = []
            for j in range(self.narms):
                angles.append(self.arms[j].getJointAngsReal()[i][0])
            ax.plot(
                range(len(angles)),
                angles, c=[float(i) / self.adof, (self.adof - float(i)) / self.adof, 0],
                label="Jt.Angle %s" % str(i + 1))
        plt.xlabel("Path Point Reached")
        plt.ylabel("Joint Angle (rad)")
        plt.legend()
        plt.title("Final Path Angles")
        self.path_angles_fig.show()

    def PlotScaledError(self):
        plt.figure(self.error_fig.number)
        pose_error = self.step_dict['pose_error']
        Acost = self.step_dict['Acost']
        Bcost = self.step_dict['Bcost']
        Kcost = self.step_dict['Kcost']
        Gcost = self.step_dict['Gcost']
        plt.plot(range(len(pose_error)), pose_error, 'r')
        plt.plot(range(len(Acost)), Acost, 'b')
        plt.plot(range(len(Bcost)), Bcost, 'k')
        plt.plot(range(len(Kcost)), Kcost, 'm')
        plt.plot(range(len(Gcost)), Gcost, 'c')
        plt.legend(['Pose Error',
                    'Joint Avg. Dev. Cost',
                    'Joint Ctr. Dev. Cost',
                    'Angs Dev. From Neighbor Cost', 'Manipulability'])
        plt.xlabel('Step In Path Planning Algorithm')
        plt.ylabel('Weighted Error')
        plt.title('Error Terms in QP')
        self.error_fig.show()

    def Report(self):
        print "Total Weighted Cost:", self.total_cost
        print "Avg. Manipulability Volume:", self.GetManipScore()
        print "Path Planning  Steps Taken: ", self.steps_taken
        print "Base Position"
        print self.arms[0].g_world_arm_

    def PlotSearch(self):
        objnl = []
        objl = []
        for i in np.linspace(0, 1, 10):
            # angs_delta_scale = deepcopy(self.angs_delta_full)*i
            angs_delta_scale = (self.angs_delta_full)*i
            objnl.append(self.ObjNonlinear(angs_delta_scale+self.angs))
            objl.append(self.ObjLinear(angs_delta_scale))

        plt.figure(self.line_search_fig.number)
        plt.cla()
        plt.plot(np.linspace(0, 1, 10), objnl, 'b', label="nonlinear")
        plt.plot(np.linspace(0, 1, 10), objl, 'r', label="linear")
        plt.legend()
        plt.title("Objective Function At Step Length")
        plt.xlabel("Step Length")
        plt.ylabel("Objective Function")
        plt.figure(self.line_search_fig.number).canvas.draw()
        plt.figure(self.line_search_fig.number).show()

    def PlotEvolution(self, flag=False):
        plt.figure(self.path_evolution_fig.number)
        for arm in self.arms:
            color = [(self.max_iter-float(self.steps_taken))/self.max_iter,
                     float(self.steps_taken)/self.max_iter, 0.0]
            self.arm_plotter.arm = arm
            self.arm_plotter.color = color
            self.arm_plotter.plot(self.path_evolution_fig.gca())
            if flag:
                self.arm_plotter.ellipse_scale = .1
                self.arm_plotter.plot_manipulability_ellipse(plt.gca())

    def PrintProgress(self):
        printstring = 'Iter: %02d  ' % self.steps_taken
        printstring += 'E_Pose: %02.5f ' % self.pose_error
        printstring += 'E_Alpha: %2.5f ' % self.Acost
        printstring += 'Beta: %2.5f ' % self.Bcost
        printstring += 'Kappa: %2.5f ' % self.Kcost
        printstring += 'Gamma: %2.5f ' % self.Gcost
        printstring += 'Step: %1.2f ' % self.step_scale
        printstring += 'A: %1.5f ' % self.alphas[3, 3]
        printstring += 'B: %1.5f ' % self.betas[3, 3]
        printstring += 'K: %1.5f ' % self.kappas[3, 3]
        printstring += 'G: %1.5f ' % self.gamma
        printstring += 'Improv: %1.5f ' % self.improvement
        print printstring

    def SaveVideoInterp(self):
        global filepath
        if not os.path.exists(filepath):
            os.makedirs(filepath)
        plt.figure(self.animation_fig.number)
        ax = plt.gca()
        ax.set_xlim3d(-.5, 1.5)
        ax.set_ylim3d(-1, 1)
        ax.set_zlim3d(-.5, 1.5)
        self.animation_fig.show()
        arm = self.temp_arms[0]
        self.arm_plotter.arm = arm
        for angs in self.angs_interp:
            self.DrawConfig(angs)
            global image_number
            file = filepath+'image_%03d.png' % image_number
            image_number += 1
            print file
            plt.savefig(file)

        os.chdir(filepath)
        subprocess.call(
            "ffmpeg -f image2 -r 10 -i image_%03d.png -vcodec mpeg4 -y pathplanning.mp4",
            shell=True)
        print " Video Generated "
        plt.show()

def scipy_sparse_to_spmatrix(A):
    coo = A.tocoo()
    SP = spmatrix(coo.data.tolist(), coo.row.tolist(), coo.col.tolist(), size=A.shape)
    return SP


def Example2D():
    # Arms SetUp
    narms = 6
    lengths = np.ones(narms)*.25
    jt_height = 0
    angs0 = np.ones(narms)*0
    arms = constructors.SimpleArms(jt_height, lengths, angs0, narms)
    base = constructors.BaseXY()

    PathTop = PATH_QPIK(arms, base, "3D")
    def path_func(t):
        fx = np.cos(t*np.pi/2-np.pi/4)
        fy = np.sin(t*np.pi/2-np.pi/4)
        fz = 0
        return fx,fy,fz
    start = 0.0
    end = 1.0

    # Targets Setup
    Path2d = PATH_QPIK(arms, base)
    Path2d.Solve(path_func,start,end)
    Path2d.Report()
    Path2d.CheckError()
    if Path2d.plot:
        Path2d.ShowAlgorithmEvolution()
        Path2d.PlotFinalPathAngs()
        Path2d.PlotScaledError()
    Path2d.AnimateGUI()

    plt.show()

def Example3D():
    # Arms SetUpj
    narms = 10
    arms = constructors.NasaArms(narms)

    # angs0 = np.ones(8)*0
    # arms = constructors.BlowMoldedArms(narms,angs0)
    base = constructors.BaseXYZ()
    # Targets Setup
    # g_world_goals = des_paths.InvArcRock(narms, .6, 0, np.pi)
    # g_world_goals = des_paths.InvArcXY(narms,.6,-np.pi/4.0,np.pi/4.0)
    # center = np.array([1.0, 0, 1.0])
    # g_world_goals = des_paths.Coupon(narms, nheight, nwidth, .80, 1.6, center)
    # g_world_goals = des_paths.Sine(narms,.5,0*np.pi,2*np.pi,1,180*np.pi/180)

    PathTop = PATH_QPIK(arms, base, "3D")
    def path_func(t):
        fx = 1.0
        fy = np.cos(t*np.pi)
        fz = np.sin(t*np.pi)
        return fx,fy,fz
    start = 0.0
    end = 1.0

    PathTop.Solve(path_func,start,end)
    boundary_configs = [PathTop.arms[0].getJointAngsReal(),PathTop.arms[1].getJointAngsReal()]
    # arm = deepcopy(PathTop.arms[0])
    # arm_plotter = ap.SimpleArmPlotter(arm,"3D")
    # arm.setJointAngsReal(boundary_configs[0])
    # arm.calcFK()
    # arm_plotter.color = 'm'
    # arm_plotter.plot(plt.gca())
    # arm.setJointAngsReal(boundary_configs[1])
    # arm.calcFK()
    # arm_plotter.plot(plt.gca())
    # plt.show()

    # PathRefine1 = PATH_QPIK(arms[0:3], base, "3D",boundary_configs)
    # PathRefine1.SetWeightsZero()
    # PathRefine1.max_iter = 100
    # PathRefine1.Solve(path_func,0,.25)

    # PathRefine2 = PATH_QPIK(arms[0:3], base, "3D",boundary_configs)
    # boundary_configs = [PathRefine1.arms[0].getJointAngsReal(),PathRefine1.arms[1].getJointAngsReal()]
    # PathRefine2.SetWeightsZero()
    # PathRefine2.max_iter = 100
    # PathRefine2.Solve(path_func,0,.25/2.0)
    # PathRefine2.DrawTargets()
    # plt.show()

    # Path3d.Report()
    # Path3d.CheckError()

    if PathTop.plot:
        PathTop.ShowAlgorithmEvolution()
        PathTop.PlotFinalPathAngs()
        PathTop.PlotScaledError()
    PathTop.AnimateGUI()
    # Path3d.SaveVideoInterp()

    plt.show()

if __name__ == "__main__":
    # Example2D()
    Example3D()
