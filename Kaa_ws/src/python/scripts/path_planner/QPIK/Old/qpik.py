#!/usr/bin/env python

from ego_core.kinematics import kinematics as kin
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import arm_tools as at
import numpy as np
import scipy.linalg
from ego_byu.cvxgen import cvxgen
from ego_byu.tools import arm_plotter as ap
from ego_byu.tools import constructors
from cvxopt import matrix, solvers
from collections import defaultdict
from copy import copy
import matplotlib.pyplot as plt
import time
from copy import deepcopy

solvers.options['show_progress'] = False


# pulls out A, b, c from joint limits
# TODO: this should be implement in Arm


class QPIK_SINGLE_ARM(object):
    def __init__(self, arm):
        self.arm = arm
        self.temp_arm = deepcopy(arm)
        self.num_dof = len(arm.getRealJointIdx())
        self.angs = np.zeros([self.num_dof,1])

        # joint limit stuff
        # should be of the form A (x - c) < b
        #   where:
        #       x is the joint angles, c is the center point
        #       rows of A are unit normal vectors for half plane constraints
        #       r is the minimum distance from c to half plane constraints
        # r = 1.5  # roughly at pi/2 circle
        # num_th = 8  # approximated by 8 half-planes
        # self.A, self.b, self.c = create_joint_limits_arm(arm, r, num_th)
        self.SetConstraints()

        # weight on joint movement
        rho = 1.0e-1
        self.Pth = rho * np.eye(self.num_dof)

        # set up Pf, weighting on the Jacobian matrix
        vec_weight = 1.00 * np.ones(3)
        rot_weight = 0.05 * np.ones(3)*0
        # rot_weight[2] = 0.0  # don't solve for rotation about hand z-axis
        self.Pf = np.diag(np.hstack((vec_weight, rot_weight)))

        # ratio of rotational to translational 1.0 means 1 meter and 90 degrees equivalent
        self.error_ratio = 1.0

        # capping goal state based on rotational or vector component
        # appears to work, but not sure it's helpful
        scale = 1.0
        self.rot_clamp_max = scale * 30.0 * 180.0 / np.pi  # radians
        self.vec_clamp_max = scale * 1.5  # meters

        self.steps_taken = 0
        # termination tolerance stuff
        self.max_iter = 50
        self.error_term_tol = 1e-4  # ~0.1 mm
        # self.error_improv_tol = 1e-5
        self.error_improv_tol = -5

        # capping the maximum step we take
        self.max_step = 0.50

        # only go a portion of the step discovered
        # there's a better way to do this:  line search?
        self.step_scale = 0.3 #default for clamping, overloaded for linesearch
        self.step_scale_factor = .90 #What factor to change in line search

        # move towards zero through null space projector to keep us from joint stops
        self.zero_step_weight = 0.05
        self.des_angs = self.c #Desired angle to project towards
        self.g_goal = np.eye(4)

        # for recording
        self.step_dict = defaultdict(list)
        self.plot = False
        self.figure = plt.figure(1)

    def SetConstraints(self):
        A_list = []  # Limits dth in either direction
        b_list = []  # Limits from zero in either direction (abs value)
        c_list = []  # Preferred position (Joint Center)

        for link in self.arm.links_:
            A = link.joint().limits().a()
            b = link.joint().limits().b().flatten()
            c = link.joint().limits().c().flatten()

            A_list.append(A.copy())
            b_list.append(b.copy())
            c_list.append(c.copy())

        # make a big matrix for each
        self.A = scipy.linalg.block_diag(*A_list)
        self.b = np.array([np.hstack(b_list)]).T  # Defined as column Vecs
        self.c = np.array([np.hstack(c_list)]).T

        # Remove False Joints
        real_indices = self.arm.getRealJointIdx()
        for i in reversed(range(len(self.A[0][:]))):
            if i not in real_indices:
                self.A = np.delete(self.A, i, 1)
        for i in reversed(range(len(self.A[:, 0]))):
            if np.all(self.A[i, :] == 0):
                self.A = np.delete(self.A, i, 0)
                self.b = np.delete(self.b, i, 0)
        for i in reversed(range(len(self.c))):
            if i not in real_indices:
                self.c = np.delete(self.c, i, 0)

    # maybe clamp hom should be in Kinematics
    def clamp_goal(self, g_delta):
        rot_mag = np.linalg.norm(kin.RotToAx(g_delta[:3, :3]))
        vec_mag = np.linalg.norm(g_delta[:3, 3])

        rot_ratio = rot_mag / self.rot_clamp_max
        vec_ratio = vec_mag / self.vec_clamp_max
        max_ratio = max(rot_ratio, vec_ratio)

        if max_ratio > 1.0:
            return kin.HomScale(g_delta, 1.0 / max_ratio)
        else:
            return g_delta

    def set_angs(self, angs):
        self.angs = angs
        self.update_arm()

    def update_arm(self):
        self.arm.setJointAngsReal(self.angs)
        self.arm.calcJacReal()

    def step(self):
        self.update_arm()  # duplicate calls, think about this
        J = self.arm.getJacBodyReal()

        # figure out delta position, clamp, turn into twist
        g_start = self.arm.getGWorldTool()
        g_delta_full = kin.HomInv(g_start).dot(self.g_goal)
        g_delta = self.clamp_goal(g_delta_full)
        # v_delta = kin.TwistFromHom(g_delta)  # right, but produces instabilities
        v_delta = kin.VeeHom(g_delta)  # not right, but works well

        self.v_delta_full = kin.VeeHom(g_delta_full)  # save to calculate error

        # save v_delta, terminate on v.T Pf v?

        # Set up the QP
        self.JPfJ = J.T.dot(self.Pf).dot(J)
        self.JPfJL = np.dot(J.T.dot(self.Pf), v_delta)

        self.CalcCollisionTerms()

        # two needed to cancel 1/2 in cvxopt cost function
        self.P = 2 * matrix(self.JPfJ + self.JPfJC + self.Pth)
        self.B = -2 * matrix(self.JPfJL+ self.JPfJCL)

        # Joint limits of form A (x - c) <= r
        # for deltas, we have:
        #   A (angs + delta_angs - c) <= r
        #   optimizing in delta_angs
        self.G = matrix(self.A)
        self.h = matrix(self.b + self.A.dot(self.c - self.angs))

        # solve the QP
        # solver = cvxgen.CVXGen(np.array(self.P), np.array(self.B), np.array(self.G), np.array(self.h))
        # solver.Solve()
        # self.angs_delta_full = solver.x_;

        #QP cvxopt solver
        self.sol = solvers.qp(self.P, self.B, self.G, self.h)
        self.angs_delta_full = np.array(self.sol['x'])

        # treat angs_delta_full appropriately
        # self.clamp_angs_delta()
        self.line_search()
        self.null_space_project(J)

        # update the angles
        self.angs += self.angs_delta
        self.update_arm()

        self.calc_error()
        self.record()

    # set up the arm, the call step until termination tolerance reached
    def solve(self, g_goal):
        self.g_goal = g_goal.copy()
        self.angs = self.arm.getJointAngsReal()
        self.update_arm()

        self.error_last = float('Inf')
        for i in range(self.max_iter):
            self.step()
            if self.plot:
                self.Plot()
            if self.error_tot < self.error_term_tol:
                print "Close enough"
                self.status = 'Solved'
                return
            if self.error_tot - self.error_last > -self.error_improv_tol:
                print "Not improving enough"
                self.status = 'improvement'
                print self.status
                return
            self.steps_taken = i + 1
            self.error_last = self.error_tot
        self.status = 'iterations'
        self.steps_taken = i + 1

    def Plot(self):
        ax = plt.gca(projection="3d")
        plt.cla()
        arm_plotter = ap.SimpleArmPlotter(self.arm)
        arm_plotter.plot(ax)
        ax.set_xlim3d(-1, 2)
        ax.set_ylim3d(-1, 2)
        ax.set_zlim3d(-1, 2)
        ap.plot_gnomon(ax,self.g_goal,.1,5,['m','m','m'])
        ap.plot_gnomon(ax,self.arm.getGWorldTool())
        ap.plot_gnomon(ax,self.arm.g_world_arm_)
        plt.figure(1).canvas.draw()
        # ap.plot_floor(ax, 5)
        plt.figure(1).show()

    # find joint motion towards center of joint angles (usually zero)
    # pushes this though null space projector, so it doesn't move end effector
    # helps keep us away from joint stops
    def null_space_project(self, J):
        # calculate the pseudo-inverse and project a motion towards des joint angles
        #   through the null space projector
        rho = 1e-9  # damp slightly for numerics
        j_inv = np.dot(J.T, np.linalg.inv(J.dot(J.T) + rho * np.eye(6)))  # pesudo-inverse
        null_projector = np.eye(self.num_dof) - j_inv.dot(J)
        self.angs_delta += self.zero_step_weight * null_projector.dot(self.des_angs - self.angs)

    # only go a portion of the way
    # if that portion is too big, cap it as well
    # haven't implemented single element cap, not sure it would help
    def clamp_angs_delta(self):
        angs_delta_scale = self.angs_delta_full * self.step_scale
        mag = np.linalg.norm(angs_delta_scale, 2)
        if mag > self.max_step:
            self.angs_delta = angs_delta_scale / mag * self.max_step
        else:
            self.angs_delta = angs_delta_scale

    def calc_error(self):
        # h0 = self.arm.getGWorldTool()
        # hg = self.g_goal
        # error_twist = kin.VeeHom(np.dot(hg, kin.HomInv(h0)))
        # self.error_twist = self.v_delta_full

        # self.error_tot = np.sqrt(error_twist.T.dot(self.Pf).dot(error_twist))
        self.error_tot = self.v_delta_full.T.dot(self.Pf).dot(self.v_delta_full)[0, 0]
        # self.error_tot = kin.HomError(h0, hg, self.error_ratio)
        # self.error_rot = kin.RotError(h0[:3, :3], hg[:3, :3])
        # self.error_vec = np.linalg.norm(h0[:3, 3] - hg[:3, 3], 2)

    def record(self):
        self.step_dict["error_tot"].append(self.error_tot)
        self.step_dict["angs_delta"].append(copy(self.angs_delta))
        self.step_dict["angs"].append(copy(self.angs))

    def consolidate_step(self):
        self.step_array = {}
        for key, value in self.step_dict.iteritems():
            self.step_array[key] = np.asarray(value)

    def line_search(self):
        self.step_scale= 1.0
        objective = float("inf")
        while(True):
            angs_delta_scale = self.angs_delta_full*self.step_scale
            angs= self.angs+angs_delta_scale
            obj = self.objective_function(angs)
            if obj > objective or self.step_scale < 1e-2:
                self.step_scale*=1/self.step_scale_factor
                break
            else:
                self.step_scale*=self.step_scale_factor
                objective = obj

        self.step_scale = min(self.step_scale, self.max_step)
        self.angs_delta = self.angs_delta_full*self.step_scale
        return

    def objective_function(self,angs):
        arm = self.temp_arm
        v_delta = np.empty([0,1])
        arm.setJointAngsReal(angs.flatten())
        arm.calcFK()
        # figure out delta position, clamp, turn into twist
        g_start = arm.getGWorldTool()
        g_delta = kin.HomInv(g_start).dot(self.g_goal)
        # g_delta= self.clamp_goal(g_delta_full)
        # v_delta = kin.TwistFromHom(g_delta)  # right, but produces instabilities
        v_delta = kin.VeeHom(g_delta)# not right, but works well

        #Compute error on Twist
        pose_error = v_delta.T.dot(self.Pf).dot(v_delta)[0,0]
        return pose_error

    def CalcCollisionTerms(self):
        self.Plot()
        Pf = np.diag(np.hstack((np.ones(3), np.zeros(3))))
        self.JPfJC = np.zeros(np.shape(self.JPfJ))
        self.JPfJCL = np.zeros(np.shape(self.JPfJL))
        # g_goal = kt.ax_vec2homog(r,t)
        dist = max(.4/(self.steps_taken/20.0+1),.1)
        for link in self.arm.links_:
            z = link.g_world_com_[2][3]
            if z < 0:
                print "LINK TOUCHED GROUND"
            if z < dist:
                J = np.zeros(np.shape(self.arm.getJacBodyReal()))
                g_world_target = link.g_world_com_ #body is defined at g_world_com_ for links
                g_world_target[2][3]+=.5
                # ap.plot_gnomon(plt.gca(),g_world_target)
                self.figure.canvas.draw()
                self.figure.show()
                # raw_input("break")
                g_delta = kin.HomInv(link.g_world_com_).dot(g_world_target)
                v_delta = kin.VeeHom(g_delta)# not right, but works well
                # print J[:,:2
                # print link.num_dof_total()
                J[:,:link.num_dof_total()] += link.jac_body_
                weight = -10*(z-dist)
                self.JPfJC = weight*J.T.dot(Pf).dot(J)
                self.JPfJCL += weight*J.T.dot(self.Pf).dot(v_delta)

                # ap.plot_gnomon(plt.gca(projection="3d"),g_world_target)
        # plt.show()
        # raw_input("break")


if __name__ == "__main__":
    # Arms SetUp
    height=.06
    lengths=np.array([.3,.3,.3,.3])
    limit=np.pi/2
    r=np.array([0, np.pi/2.0, 0])
    t=np.array([0, 0.0, 0.1])
    g_world_arm=kt.ax_vec2homog(r, t)

    # arm=at.nasa_arm(height, lengths, g_world_arm)
    arm=constructors.BlowMoldedArms(1)[0]
    arm.g_world_arm_ = g_world_arm
    angs = [.5,-.5,0,0,0,0,0,0]
    arm.setJointAngsReal(angs)
    arm.calcFK()
    arm.calcJacReal()

    # Targets Setup
    radius=.8
    xt=1
    yt=0
    zt= .2
    t=np.array([xt, yt, zt])
    r=np.array([0, np.pi, 0])
    target=kt.ax_vec2homog(r, t)

    myqp=QPIK_SINGLE_ARM(arm)
    myqp.plot=True
    myqp.solve(target)
    plt.show()

    # for angs in myqp.step_dict["angs"]:
    #     print angs
    #     raw_input("break")
    # plt.show()



