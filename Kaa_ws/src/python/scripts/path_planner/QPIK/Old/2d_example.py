#!/usr/bin/env python
from __future__ import division
from ego_core.kinematics import kinematics as kin
import numpy as np
from ego_byu.tools import arm_tools as at
from ego_byu.tools import arm_plotter as ap
from ego_byu.tools import pickle_tools as pk
from ego_byu.tools import kin_tools as kt

from ego_pneubo.vis_tools import qpik
from ego_byu.path_planner import path_planner as pp
import matplotlib.pyplot as plt
import random
import time

#(IK's along a 2 dimensional wall)
height = 0.0
lengths = np.array([1.0,1.0,1.0])
limit = 2*np.pi/4
arm = at.simple_arm(height,lengths)
for i in range(len(arm.links_)):
    arm.links_[i].joint_.limits().setLimitsScalar(limit)

arm.setJointAngsZero()
arm.calcFK()

#Setup QPIK and Plotting
mypathplanner = pp.PathPlanner(arm)

fig = plt.figure()
ax = fig.gca(projection='3d')
arm_plotter = ap.SimpleArmPlotter(arm)

#Targets
mt = time.time()
n = 10
yt = np.linspace(-1.5,1.5,n+1,endpoint=True)
xt = np.ones([len(yt)])*1
for i in range(len(yt)):
    r = np.zeros([3])
    t = np.array([xt[i],yt[i],0])
    g_goal = kt.ax_vec2homog(r,t)
    mypathplanner.qpik_.Solve(g_goal)
    arm_plotter.plot(ax)

print "TOTAL IK TIME"
print time.time()-mt
# catter
ap.plot_line_2d(ax,[xt[0],yt[0]],[xt[-1],yt[-1]],'r',3 )
plt.scatter(xt,yt)

# lot arm
plt.axis([-2,2,-2,2])
plt.xlabel('x')
plt.ylabel('y')
plt.title('QP IK solver for 2DOF "Painting" Robot, Joint Limits: 3pi/4 rad')
print "done"
plt.show()

