#!/usr/bin/env python
import numpy as np
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import arm_plotter as ap
import matplotlib.pyplot as plt

# 2D Paths


def InvArc2D(n, radius, ang1, ang2):
    g_world_goals = []
    thetas = np.linspace(ang1, ang2, n, endpoint=True)
    for i in range(n):
        xt = -radius*np.cos(thetas[i])+1
        yt = radius*np.sin(thetas[i])+.1
        t = np.array([xt, yt, 0])
        r = np.array([0, 0, np.arctan2(xt, yt)-np.pi/2.0])
        # r = np.array([0,0,0])
        target = kt.ax_vec2homog(r, t)
        g_world_goals.append(target)
    return g_world_goals

# 3D Paths


def InvArcRock(n, radius, ang1, ang2):
    g_world_goals = []
    thetas = np.linspace(ang1, ang2, n, endpoint=True)
    for i in range(n):
        xt = .80
        yt = radius*np.cos(thetas[i])
        zt = radius*np.sin(thetas[i])
        t = np.array([xt, yt, zt])
        r = np.array([0, np.pi, 0])
        # r = np.array([0,0,0])
        target = kt.ax_vec2homog(r, t)
        # target = np.dot(target,kt.ax_vec2homog(np.array([0,np.pi/2.0,0]),np.zeros(3)))
        g_world_goals.append(target)

    return g_world_goals


def InvArcXY(n, radius, ang1, ang2):
    g_world_goals = []
    thetas = np.linspace(ang1, ang2, n, endpoint=True)
    for i in range(n):
        xt = -radius*np.cos(thetas[i])+1
        yt = radius*np.sin(thetas[i])+.1
        zt = 0
        t = np.array([xt, yt, zt])
        r = np.array([0, 0, np.arctan2(xt, yt)-np.pi/2.0])
        # r = np.array([0,0,0])
        target = kt.ax_vec2homog(r, t)
        # target = np.dot(target,kt.ax_vec2homog(np.array([0,np.pi/2.0,0]),np.zeros(3)))
        g_world_goals.append(target)
    return g_world_goals


def Coupon(n, nheight, nwidth, height, width, center):
    if nheight*nwidth != n:
        print "Targeted Grid Space must be dimensioned to # arms"
        assert(False)

    ys = np.linspace(center[1]-width/2.0, center[1]+width/2.0, nwidth, endpoint=True)
    zs = np.linspace(center[2]-height/2.0, center[2]+height/2.0, nheight, endpoint=True)

    # Snake Through Coupon
    g_world_goals = []
    for i in range(len(ys)):
        if np.mod(i, 2) == 0:
            for j in range(len(zs)):
                x = center[0]
                y = ys[i]
                z = zs[j]
                g_world_goals.append(kt.ax_vec2homog(np.array([0, np.pi/2.0, 0]), np.array([x, y, z])))
        else:
            for j in reversed(range(len(zs))):
                x = center[0]
                y = ys[i]
                z = zs[j]
                g_world_goals.append(kt.ax_vec2homog(np.array([0, np.pi/2.0, 0]), np.array([x, y, z])))

    return g_world_goals


def Sine(n, amp, ang1, ang2, freq, tilt):
    g_world_goals = []
    ang = np.linspace(ang1, ang2, n)
    for i in range(n):
        x = 1.0
        y = ang[i]/(ang2-ang1)/freq
        z = np.sin(ang[i])*amp+amp
        r = np.array([0, np.pi/2.0+(float(i)/n)*tilt-tilt/2.0, 0])
        g_world_goals.append(kt.ax_vec2homog(r, np.array([x, y, z])))

    return g_world_goals


if __name__ == "__main__":
    plt.figure
    plt.gca(projection="3d")
    ax = plt.gca()
    center = np.array([1.0, 0, 1.0])
    # g_world_goals = Coupon(15.0,3.0,5.0,1.0,2.0,center)
    g_world_goals = Sine(20, 1, 0*np.pi, 2*np.pi, .5)

    for i, g in enumerate(g_world_goals):
        ap.plot_gnomon(ax, g)
    ap.set_axes_equal(ax)
    plt.show()
