#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np

# Describes Weighting Function For Collision Avoidance

np.set_printoptions(precision=2)

dist_tols = np.linspace(.3, .1, 20)
d = np.linspace(-1, 1, 200)
f = []
w = 1.0

plt.figure()
for j, dist_tol in enumerate(dist_tols):
    plt.cla()
    f = []
    for i in d:
        if i <= dist_tol:
            fx = ((i-dist_tol)/dist_tol)**2*w
            f.append(fx)

        else:
            f.append(0)

    plt.plot(d, f)
    plt.plot(np.linspace(dist_tol, 5, 100), np.zeros(len(np.linspace(dist_tol, 5, 100))))
    plt.plot([0, 0], [-10000, 10000], 'k')
    plt.legend()
    plt.gca().set_xlim(0, .5)
    plt.gca().set_ylim(0, 1)
    plt.gcf().canvas.draw()
    plt.gcf().show()
    plt.title("Changing Collision Costs in QP by Step #")
    plt.xlabel("Distance from collision element")
    plt.ylabel("Cost")

plt.show()
