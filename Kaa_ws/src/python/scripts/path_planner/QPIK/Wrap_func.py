#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np

# Describes Weighting Function For Wrapping Function

np.set_printoptions(precision=2)
squeeze_tol = .10

d = np.linspace(-1, 3, 200)
f = []

plt.figure()
plt.cla()
f = []
for x in d:
    if x < -squeeze_tol:
        fx = 0
        f.append(fx)
    else:
        fx = 10*(x+squeeze_tol)**2
        f.append(fx)

plt.plot(d, f)
# plt.plot(np.linspace(dist_tol, 5, 100), np.zeros(len(np.linspace(dist_tol, 5, 100))))
plt.plot([0, 0], [-10000, 10000], 'k')
plt.legend()
plt.gca().set_xlim(-2.0, 2.0)
plt.gca().set_ylim(-.5, 3)
plt.gcf().canvas.draw()
plt.gcf().show()
plt.title("Weights of Wrap")
plt.xlabel("Distance from element")
plt.ylabel("Cost")

plt.show()
