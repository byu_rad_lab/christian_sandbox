#!/usr/bin/env python
from __future__ import division
from ego_core.kinematics import kinematics as kin
import numpy as np
from ego_byu.path_planner import path_planner as ppc
from ego_byu.armeval import armeval as ae
from ego_byu.tools import arm_tools as at
from ego_byu.tools import arm_plotter as ap
from ego_byu.tools import pickle_tools as pk
from ego_byu.tools import kin_tools as kt
import qpik
from itertools import count
import dijkstra as dk
import time
import matplotlib.pyplot as plt
import random
import copy

class PathPoint:
    def __init__(self,target):
        self.g_world_desired = target
        self.configs_found = []
        self.configs_found_reduced = []

class Configuration:
    _ids = count(0)
    def __init__(self,jt_angs):
        self._ids = self._ids.next()
        self.jt_angs = jt_angs

class PathPlanner:
    def __init__(self,path_pts,mypathplanner):
        self.path_pts = path_pts
        self.pathplanner = mypathplanner #This is the PathPlanner implemented in C++
        self.optimal_path = []
        self.initial_jt_angs = mypathplanner.arm_.getJointAngs()

    #IKs from random position to path
    def RandomIKToPath(self,max_num_tries, acceptable_num_samples_at_pt):
        IKTime = time.time()
        for i in range(len(self.path_pts)):
            for j in range(max_num_tries):
                if (len(self.path_pts[i].configs_found) < acceptable_num_samples_at_pt):
                    self.pathplanner.arm_.setJointAngsRandom()
                    self.pathplanner.qpik_.Solve(self.path_pts[i].g_world_desired)
                    # print self.pathplanner.qpik_.error_improv_tol_
                    # raw_input("break")
                    # print "STATUS ", self.pathplanner.qpik_.status_, "Iterations:", self.pathplanner.qpik_.steps_taken_, "Total Error", self.pathplanner.qpik_.error_tot_, "Improvement", -(self.pathplanner.qpik_.error_tot_ - self.pathplanner.qpik_.error_last_)
                    if self.pathplanner.qpik_.status_=='Solved':
                        config = Configuration(self.pathplanner.arm_.getJointAngs())
                        self.path_pts[i].configs_found.append(config)
            print "Num Samples Found at Pt", i
            print len(self.path_pts[i].configs_found)

        IKTime = time.time()-IKTime
        print "TOTAL IK TIME ", IKTime

    #Sets up Eval Object for Seeding
    def SetupEvalFK(self):
        Eval = ae.ArmEvalFK(self.pathplanner.arm_)
        Eval.print_on = False
        Eval.setXYZRes(30)
        Eval.setSO3Res(5)
        Eval.setBounds(2)
        center = np.array([1.0,0,0]) #Should be centered at path center
        Eval.setCenterDiscretization(center)
        Eval.redundancy_cutoff_ = .999
        Eval.storage_num_ = max_num_tries
        Eval.path_plan_ = True
        for i in range(len(self.path_pts)):
            Eval.add_path_pt(self.path_pts[i].g_world_desired)
        Eval.Initialize()
        return Eval

    #Seeds IK
    def SeededIKToPath(self,max_num_tries):
        #Set up FK Simulator
        Eval = self.SetupEvalFK()
        FKTime = time.time()
        Eval.Run()
        FKTime = time.time()-FKTime
        print "TOTAL FK TIME ", FKTime

        #IK From Seeded Poses
        IKTime = time.time()
        for i in range(len(self.path_pts)):
            for pose in range(Eval.getNumConfigs(Eval.getPathPt(i))):
                angs = Eval.getConfig(pose,Eval.getPathPt(i))
                self.pathplanner.arm_.setJointAngs(angs)
                self.pathplanner.qpik_.Solve(self.path_pts[i].g_world_desired)
                # print "STATUS ", self.pathplanner.qpik_.status_, " ITERATIONS ", self.pathplanner.qpik_.steps_taken_
                if self.pathplanner.qpik_.status_=='Solved':
                    config = Configuration(self.pathplanner.arm_.getJointAngs())
                    self.path_pts[i].configs_found.append(config)

            print "Num Samples Found at Pt", i
            print len(self.path_pts[i].configs_found)
        IKTime = time.time()-IKTime
        print "TOTAL IK TIME ", IKTime

    #Reduces Sample to Representative Sample For Optimization
    def GetRepSample(self,cutoff):
        for pt in range(len(self.path_pts)):
            poses = copy.copy(self.path_pts[pt].configs_found)
            i = 0
            while(i<len(poses)):
                j = i+1
                while(j<len(poses)):
                    error = poses[i].jt_angs-poses[j].jt_angs
                    if np.linalg.norm(error) < cutoff:
                        poses.pop(j)
                    else:
                        j = j+1
                i = i+1
            self.path_pts[pt].configs_found_reduced = poses

    def CountPoses(self):
        count = 0
        countRepSample = 0
        for i in range(len(self.path_pts)):
            count+= len(self.path_pts[i].configs_found)
            countRepSample += len(self.path_pts[i].configs_found_reduced)

        print "Total Poses Found: ", count
        print "Sample Reduced To: ", countRepSample

    def Plan(self):
        #Add Current Configuration as Path Point
        self.pathplanner.arm_.setJointAngsRandom()
        target = np.eye(4)
        current_pt = PathPoint(target)
        config = Configuration(self.initial_jt_angs)
        current_pt.configs_found_reduced.append(config)
        self.path_pts.insert(0,current_pt)

        #Set Up and Solve Dijkstra Graph Search Problem
        PlanTime = time.time()
        edges = dk.FormatGraph(self.path_pts)
        target_indices = []
        for i in range(len(self.path_pts[-1].configs_found_reduced)):
            target_indices.append(self.path_pts[-1].configs_found_reduced[i]._ids)
        optimal_path =  dk.dijkstra(edges,self.path_pts[0].configs_found_reduced[0]._ids,target_indices)

        if optimal_path == float("inf"):
            print "Path Finder Failed!"
            return

        #Retrieve Sequence of Joint Angles From Graph
        for i in range(len(optimal_path[1])):
            for j in range(len(self.path_pts)):
                for k in range(len(self.path_pts[j].configs_found_reduced)):
                    if self.path_pts[j].configs_found_reduced[k]._ids == optimal_path[1][i]:
                        self.optimal_path.append(self.path_pts[j].configs_found_reduced[k].jt_angs)
        PlanTime = time.time()-PlanTime
        print "Total Planning Time ", PlanTime

if __name__ == "__main__":
    #Set up Arm
    height = 0
    lengths = np.array([1.0,1.0,1.0,1.0,1.0,1.0])*.2
    limit = np.pi/2
    arm = at.simple_arm(height,lengths)
    for i in range(len(arm.links_)):
        arm.links_[i].joint_.limits().setLimitsScalar(limit)
    arm.setJointAngsReal(np.ones(len(lengths))*-np.pi/8)
    arm.calcFK()

    #Setup QPIK and Plotting
    mypathplanner = ppc.PathPlanner(arm)
    mypathplanner.qpik_.max_iter=100
    mypathplanner.qpik_.max_step=.5
    mypathplanner.qpik_.error_term_tol = 1e-5
    mypathplanner.qpik_.error_improv_tol = 1e-5
    vec_weight = np.ones(3)
    rot_weight = np.ones(3)*.05
    # rot_weight[2] = 0
    mypathplanner.qpik_.Pf_ = np.diag(np.hstack((vec_weight,rot_weight)))

    #Define Path Targets (For now two pts)
    xt = np.array([])
    yt = np.array([])
    thetas = np.linspace(-np.pi/4,0,10)
    for theta in thetas:
        xt = np.append(xt,np.cos(theta))
        yt = np.append(yt,np.sin(theta))
    ys = np.linspace(0,.5,5)
    for y in ys:
        xt = np.append(xt,1)
        yt = np.append(yt,y)
        thetas = np.append(thetas,0)
    path = []

    for i in range(len(xt)):
        r = np.zeros([3])
        r[2] = thetas[i]
        t = np.array([xt[i],yt[i],0])
        target = kt.ax_vec2homog(r,t)
        path_pt = PathPoint(target)
        path.append(path_pt)
    max_num_tries = 50 #Num poses per path point sought by FK and IK routines
    acceptable = 50
    cutoff = 0 #If Path Planning is taking a long time, bump this up to reduce sample number

    planner = PathPlanner(path,mypathplanner)
    #IK
    planner.RandomIKToPath(max_num_tries,acceptable)
    # planner.SeededIKToPath(max_num_tries)
    planner.GetRepSample(cutoff)
    planner.CountPoses()
    planner.Plan()

    #PLOTTING
    plot_on = True
    if plot_on == True:
        fig = plt.figure()
        ax = fig.gca(projection="3d")
        arm_plotter = ap.SimpleArmPlotter(arm)
        arm_plotter.line_width=10
        pts= len(planner.optimal_path)

        for i in range(pts-1):
                arm_plotter.color = [i/pts,(1)-i/pts,0]
                arm.setJointAngs(planner.optimal_path[i+1])
                arm.calcFK()
                arm_plotter.plot_link_ends(ax)
                arm_plotter.plot(ax)

        #Scatter
        # ap.plot_line_2d(ax,[1,-1],[1,1],'r',3 )
        ap.set_axes_equal(ax)
        plt.scatter(xt,yt,s=1000,c='k')

        #Plot arm
        plt.axis([-.5,1.5,-1,1])
        plt.xlabel('x')
        plt.ylabel('y')
        # plt.title('Null Space Sampling for a 4-Link "Painting" Robot \n BLUE = Complete Sample, RED = Reduced Representative Sample')
        plt.title('Path Planning: FK Seeded IK Sampling along Trajectory,\n Dijkstra Graph Search Used To Find Optimal Sequence of Configs.')
        plt.show()

