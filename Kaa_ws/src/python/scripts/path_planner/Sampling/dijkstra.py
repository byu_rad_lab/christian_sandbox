#!/usr/bin/env python
from collections import defaultdict
from heapq import *
from itertools import count
import numpy as np

def dijkstra(edges, start, target_list):
    graph = defaultdict(list)
    for node1,node2,c in edges:
        graph[node1].append((c,node2))

    q = [(0,start,(),np.array([]))]
    seen = set()

    while q:
        (cost,v1,path,pathnp) = heappop(q)
        if v1 not in seen:
            seen.add(v1)
            path = (v1, path)
            pathnp = np.hstack([v1,pathnp])
            if v1 in target_list:
                return (cost, pathnp[::-1])

            for c, v2 in graph.get(v1, ()):
                if v2 not in seen:
                    heappush(q, (cost+c, v2, path,pathnp))

    return float("inf")

def FormatGraph(path_pts):
    edges = []
    #Connect sequences with edges
    for i in range(len(path_pts)-1):
        for j in range(len(path_pts[i].configs_found_reduced)):
            for k in range(len(path_pts[i+1].configs_found_reduced)):
                config1 = path_pts[i].configs_found_reduced[j]
                config2 = path_pts[i+1].configs_found_reduced[k]
                cost = np.linalg.norm(config1.jt_angs-config2.jt_angs)**4
                edges.append([config1._ids,config2._ids,cost])

    return edges

if __name__ == "__main__":
    edges = [
        (1, 2, 7),
        (1, 4, 5),
        (2, 3, 8),
        (2, 4, 9),
        (2, 5, 7),
        (3, 5, 5),
        (4, 5, 15),
        (4, 6, 6),
        (5, 6, 8),
        (5, 7, 9),
        (6, 7, 11)
    ]

    print "=== Dijkstra ==="
    print edges
    print "1 -> 5:"
    path =  dijkstra(edges, 1, [5,6])
    print path
    # print path.flatten()
    # print path[1][1][1][0]
    # print "6 -> 7:"
    # print dijkstra(edges, 6, 7)

