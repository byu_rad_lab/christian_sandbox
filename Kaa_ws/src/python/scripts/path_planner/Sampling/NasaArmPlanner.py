#!/usr/bin/env python
from __future__ import division
import numpy as np
from ego_core.kinematics import kinematics as kin
from ego_byu.armeval import armeval as ae
from ego_byu.path_planner import path_planner as ppc #Core Pathplan functionality in c
import SamplingPlanner as pp #PathPlanner in Python
from ego_byu.tools import arm_tools as at
from ego_byu.tools import arm_plotter as ap
from ego_byu.tools import kin_tools as kt
import matplotlib.pyplot as plt

#BUILD ARM
jt_height = .02
lengths = np.array([.23,.23,.23,.23,.23,.23])
base_hom = kt.ax_vec2homog(np.array([np.pi/2,0,0]),np.array([0,0,0]))
stiffness=np.array([80,1000,235])
arm = at.nasa_arm(jt_height,lengths,base_hom,stiffness)
arm.setJointAngsZero()
arm.calcFK()

#Setup QPIK and Plotting
mypathplanner = ppc.PathPlanner(arm)
mypathplanner.qpik_.max_iter_=50
mypathplanner.qpik_.max_step_=.3
mypathplanner.qpik_.error_term_tol_ = 1e-5
mypathplanner.qpik_.error_improv_tol_ = 1e-7
mypathplanner.qpik_.scale_ = 1
vec_weight = np.ones(3)
rot_weight = np.ones(3)*.05
rot_weight[2] = 0
mypathplanner.qpik_.Pf_ = np.diag(np.hstack((vec_weight,rot_weight)))

#Define Path Targets (For now two pts)
xt = np.array([])
yt = np.array([])
thetas = np.linspace(-np.pi/4,0,10)
dist = .8
for theta in thetas:
    xt = np.append(xt,dist*np.cos(theta))
    yt = np.append(yt,dist*np.sin(theta))
ys = np.linspace(0,.5,5)
for y in ys:
    xt = np.append(xt,dist)
    yt = np.append(yt,y)
    thetas = np.append(thetas,0)

path = []
d = 0
for i in range(len(xt)):
    r = np.zeros([3])
    r[2] = thetas[i]
    if xt[i] == dist:
        d = d+.2
        r[1] = d
    t = np.array([xt[i],yt[i],0])
    target = kt.ax_vec2homog(r,t)
    path_pt = pp.PathPoint(target)
    path.append(path_pt)
max_num_tries = 100 #Num poses per path point sought by FK and IK routines
acceptable = 150 #Num poses per point at which we have "enough" samples
cutoff = 0 #If Path Planning is taking a long time, bump this up to reduce sample number

planner = pp.PathPlanner(path,mypathplanner)
#IK FROM RANDOM POSITION
planner.RandomIKToPath(max_num_tries, acceptable)
planner.GetRepSample(cutoff)
planner.CountPoses()
# planner.Plan()
print "Planning"
planner.Plan()
print "Planned"

arm_plotter = ap.ArmPlotter(arm)

#PLOTTING
plot_on = True
if plot_on == True:
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    for i in range(len(path)):
        ap.plot_gnomon(ax, path[i].g_world_desired, .04)

    arm_plotter.line_width=10
    pts= len(planner.optimal_path)

    for i in range(pts-1):
            arm_plotter.link_color = [0,(1)-i/pts,0]
            print arm_plotter.link_color
            arm.setJointAngs(planner.optimal_path[i+1])
            arm.calcFK()
            arm_plotter.plot(ax)

    #Scatter
    # ap.plot_line_2d(ax,[1,-1],[1,1],'r',3 )
    # plt.scatter(xt,yt,s=1000,c='k')

    #Plot arm
    ap.set_axes_equal(ax)
    # plt.axis([-.5,1.5,-1,1])

    plt.xlabel('x')
    plt.ylabel('y')
    # plt.title('Null Space Sampling for a 4-Link "Painting" Robot \n BLUE = Complete Sample, RED = Reduced Representative Sample')
    plt.title('Path Planning: IK Sampling along Trajectory,\n Dijkstra Graph Search Used To Find Optimal Sequence of Configs.')
    plt.show()

