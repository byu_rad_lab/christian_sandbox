#!/usr/bin/env python
import numpy as np

#Key Selection
world_keys = np.array([1,2,3,4])
tracked_keys = [6,7,8,9,10,11,12,13]
tracked_keys = np.append(world_keys,tracked_keys)
# tag_set = 'stumpy'
tag_set = 'paper-byu'

#Camera/Picture Settings
camera_name = 'Kinect'
device_number = 0

#default_image = 'default_image.jpg'
default_image = '0000_color.jpg'

#Tolerances
reprojection_tol = 50

#For Drawing Coordinate Frames
length = 0.030
thickness = 3

#For Scaling Images
scale = 1.3

#Max Number of Found Tags
tagmax = 30

#Body to be tracked
tracked_bodies = ['body1','body2']


