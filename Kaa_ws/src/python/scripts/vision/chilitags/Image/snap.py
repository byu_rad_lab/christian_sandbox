#!/usr/bin/env python
#FOR IMAGES FROM THE KINECT
#(run with sudo -s to get around access issues)
#lsusb -v to see if you can see the Kinect from your user

#1. source /git/kinect/devel/setup.bash
#2. roslaunch kinect2_bridge kinect2_bridge
#3. (new terminal repeat source) roslaunch kinect2_viewer kinect2_viewer

from __future__ import division

import argparse
import cv2

parser = argparse.ArgumentParser(description='minimal example of argparse',
                                 version='%(prog)s 1.0')

parser.add_argument('--name', type=str, default='image', help='saved image prefix')
parser.add_argument('--num', type=int, default=0, help='saved image start num')
parser.add_argument('--dev', type=int, default=0, help='camera number')

args = vars(parser.parse_args())
for key, value in args.iteritems():
    print key, value

print args
print type(args)
print args['name']

image_num = args['num']

device_number = args['dev']  # default
device_number = 10
vc = cv2.VideoCapture(device_number)

width = 1920
height = 1080

scale = 1

vc.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, width)
vc.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, height)


while(True):
    ret, frame = vc.read()
    if not ret:
        print "### Warning:  bad frame read!  ###"
    else:
        frame_small = cv2.resize(frame, (width//scale, height//scale))
        # frame_small = cv2.flip(frame_small, 1)  # flips for camera aimed at self
        cv2.imshow("Camera capture:  space to save, ESC to quit", frame_small)

        k = cv2.waitKey(10)
    if k == 27:  # escape key
        break
    elif k == 113:  # q
        break
    elif k == 32:  # spacebar
        img_name = args['name'] + '_%02d.jpg' % image_num
        image_num += 1
        cv2.imwrite(img_name, frame)
        print 'Save %s.' % img_name

    elif k == -1:
        continue
    else:
        print "key %d pressed" % k


vc.release()
cv2.destroyAllWindows()
