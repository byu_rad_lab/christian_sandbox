#!/usr/bin/env python
from __future__ import division
import cv2
import numpy as np
# import time
import Tag_Colors as tc
import preferences as pref
from ego_byu.tools import ChiliTracker as CT

if __name__ == '__main__':

    # Setup Tracker Object
    tags = CT.load_tags(pref.tag_set)
    camera = CT.load_camera(pref.camera_name)
    tracker = CT.Tracker(tags, camera)

    # Reads Image to CV object
    img = cv2.imread(pref.default_image)

    if img is None:
        print "###Error: No Image"
        print "The file attempted to load %s" % pref.default_image
        assert(False)

    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Gets image points and uses inherent knowledge about body points to compute world coordinates
    tracker.find_chilitags(img_gray)
    tracker.estimate_cw_pose()
    tracker.estimate_cb_poses(pref.tracked_bodies)

    tracker.label_image(img)

    # Reprojects points from world frame to screen and compares with image
    tracker.reproject_body(img, 6, tc.magenta, 4, "world")
    tracker.reproject_body(img, 6, tc.yellow, 4, 'body1')
    tracker.reproject_body(img, 6, tc.yellow, 4, 'body2')

    # Draws World
    tracker.cam.draw_axis(img, np.array([0, 0, 0]), np.array([0, 0, 0]), pref.length*1.5, 5)

    # Estimates pose and warns for reprojection error
    rwb, twb = tracker.cam.est_pose_body('body1', tracker.tags)
    tracker.cam.draw_axis(img, rwb, twb, pref.length, 5)
    print "rwb: ", rwb
    print "twb: ", twb

    rwb, twb = tracker.cam.est_pose_body('body2', tracker.tags)
    tracker.cam.draw_axis(img, rwb, twb, pref.length, 5)
    print "rwb: ", rwb
    print "twb: ", twb

    tracker.imshow_small('Tracker test', img, pref.scale)
    k = cv2.waitKey(0)

    cv2.destroyAllWindows()
    print "Done!"
