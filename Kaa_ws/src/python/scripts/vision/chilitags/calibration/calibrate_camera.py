#!/usr/bin/env python

'''
USAGE: camera_calib.py [--save <filename>] [--debug <output path>] [--square_size] [<image mask>]
./calibrate_camera.py --debug=debug --save=my_camera_coeffs "./FullHD/image_*.bmp"
Loads images from FullHD folder, saves debug images to debug directory, saves camera
  coefficients to my_camera_coeff_camera_matrix, my_camera_coeff_dist_coeffs

'''

import numpy as np
import cv2
import os


def splitfn(fn):
    path, fn = os.path.split(fn)
    name, ext = os.path.splitext(fn)
    return path, name, ext


if __name__ == '__main__':
    import sys
    import getopt
    from glob import glob

    # camera_name = "FullHD"
    camera_name = "Kinect"
    # camera_name = "IDS_ML"
    extension = '/*color.jpg'

    args, img_mask = getopt.getopt(sys.argv[1:], '', ['save=', 'debug=', 'square_size='])
    args = dict(args)
    try:
        img_mask = img_mask[0]
    except:
        img_mask = camera_name + extension
    img_names = glob(img_mask)
    debug_dir = args.get('--debug')
    square_size = float(args.get('--square_size', 1.0))
    save_name = args.get('--save', camera_name)

    print "img_mask = \n", img_mask
    print "img_name = \n", img_names
    print img_names

    pattern_size = (7, 5)
    pattern_points = np.zeros((np.prod(pattern_size), 3), np.float32)
    pattern_points[:, :2] = np.indices(pattern_size).T.reshape(-1, 2)
    pattern_points *= square_size

    obj_points = []
    img_points = []
    h, w = 0, 0
    for fn in img_names:
        print 'processing %s...' % fn,
        img = cv2.imread(fn, 0)
        h, w = img.shape[:2]
        found, corners = cv2.findChessboardCorners(img, pattern_size)
        if found:
            term = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1)
            cv2.cornerSubPix(img, corners, (5, 5), (-1, -1), term)
        if debug_dir:
            vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
            cv2.drawChessboardCorners(vis, pattern_size, corners, found)
            path, name, ext = splitfn(fn)
            cv2.imwrite('%s/%s_chess.bmp' % (debug_dir, name), vis)
        if not found:
            print 'chessboard not found'
            continue
        img_points.append(corners.reshape(-1, 2))
        obj_points.append(pattern_points)

        print 'ok'

    flags = 0
    flags |= cv2.CALIB_FIX_PRINCIPAL_POINT
    # flags |= cv2.CALIB_ZERO_TANGENT_DIST
    flags |= cv2.CALIB_FIX_ASPECT_RATIO

    rms, camera_matrix, dist_coefs, rvecs, tvecs \
        = cv2.calibrateCamera(obj_points, img_points, (w, h), flags=flags)

    np.save(save_name + "_camera_matrix", camera_matrix)
    np.save(save_name + "_dist_coefs", dist_coefs)

    print "RMS:", rms
    print "camera matrix:\n", camera_matrix
    print "distortion coefficients:\n", dist_coefs.ravel()
    cv2.destroyAllWindows()
