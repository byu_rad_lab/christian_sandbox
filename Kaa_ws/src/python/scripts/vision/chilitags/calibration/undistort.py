#!/usr/bin/env python

from __future__ import division
import cv2
import numpy as np
from atheris_gui.utils       import Util

def imshow_scaled(name, img, scale):
    shape = img.shape
    img_small = cv2.resize(img, (shape[1]//scale, shape[0]//scale))
    cv2.imshow(name, img_small)

util            = Util()
parser          = util.build_arg_parser_from_yaml("./undistort.yaml")
util.args       = vars(parser.parse_args())

test_img_path = util.args["img"]
camera_name = util.args["camera"]
scale = int(util.args["scale"])

img = cv2.imread(test_img_path)
camera_matrix = np.load(camera_name + "_camera_matrix.npy")
dist_coefs = np.load(camera_name + "_dist_coefs.npy")
img_undst = cv2.undistort(img, camera_matrix, dist_coefs)
imshow_scaled('original', img, scale)
imshow_scaled('undistorted', img_undst, scale)
cv2.waitKey(0)
cv2.destroyAllWindows()