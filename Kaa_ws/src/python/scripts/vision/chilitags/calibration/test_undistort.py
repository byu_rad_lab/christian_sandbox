#!/usr/bin/env python

from __future__ import division

import cv2
import numpy as np

# camera_name = "IDSML_CM"
# camera_name = "logitech"
camera_name = "Kinect"
# camera_name = "FullHD"
# camera_name = "iphone_normal"
# camera_name = "iphone_fisheye"
# camera_name = "iphone_wide"
# camera_name = "thinkpad"

if camera_name == 'logitech':
    img_num = 18
    img_path = 'logitech/image_%d.jpg' % img_num
    scale = 2
if camera_name == 'Kinect':
    img_path = 'Kinect/0001_color.jpg'
    scale = 1

elif camera_name =="IDSML_CM":
    img_num = 1
    img_path = 'IDSML_CM/image%d.bmp' % img_num
    scale = 2

elif camera_name == "IDS_ML":
    img_num = 1
    img_path = 'IDS_ML/image%d.bmp' % img_num
    scale = 2

elif camera_name == "iphone_normal":
    img_num = 3351
    img_path = 'iphone_normal/IMG_%d.JPG' % img_num
    scale = 3
elif camera_name == "iphone_fisheye":
    img_num = 3367
    img_path = 'iphone_fisheye/IMG_%d.JPG' % img_num
    scale = 3
elif camera_name == "iphone_wide":
    # img_num = 3367
    img_num = 3390
    img_path = 'iphone_wide/IMG_%d.JPG' % img_num
    scale = 3
elif camera_name == 'thinkpad':
    img_num = 0
    img_path = 'thinkpad/image_%02d.jpg' % img_num
    scale = 1
elif camera_name == 'FullHD':
    img_num = 9
    img_path = 'FullHD/image_%02d.bmp' % img_num
    scale = 2
else:
    assert(False)


# img_num = 3390
# img_path = 'iphone_wide/IMG_%d.JPG' % img_num

print img_path

print "### Loading image %s" % img_path
camera_name = "FullHD"

img = cv2.imread(img_path)

# load camera matrix
camera_matrix = np.load(camera_name + "_camera_matrix.npy")
dist_coefs = np.load(camera_name + "_dist_coefs.npy")


print camera_matrix
print dist_coefs


def imshow_scaled(name, img, scale):
    shape = img.shape
    img_small = cv2.resize(img, (shape[1]//scale, shape[0]//scale))
    cv2.imshow(name, img_small)

img_undst = cv2.undistort(img, camera_matrix, dist_coefs)

imshow_scaled('original', img, scale)
imshow_scaled('undistorted', img_undst, scale)
cv2.imwrite("./original.bmp",img)
cv2.imwrite("./undistorted.bmp",img_undst)
cv2.waitKey(0)

cv2.destroyAllWindows()
