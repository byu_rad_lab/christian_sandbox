#!/usr/bin/env python

# A script to save images from a webcam.
# Usage:
#     ./save_images.py --dev=0 --name="image_" --num=15
#     --dev=0 is default device, plugged in webcam may be 1
#     --name is the prefix added to the images
#     --num is a number to start with, if you don't want to start with 0
#     Each press of space saves an image
#     Esc quits

from __future__ import division

import argparse
import cv2

parser = argparse.ArgumentParser(description='minimal example of argparse',
                                 version='%(prog)s 1.0')

parser.add_argument('--name', type=str, default='image', help='saved image prefix')
parser.add_argument('--num', type=int, default=0, help='saved image start num')
parser.add_argument('--dev', type=int, default=0, help='camera number')

args = vars(parser.parse_args())
for key, value in args.iteritems():
    print key, value

print args
print type(args)
print args['name']

image_num = args['num']

device_number = args['dev']  # default
vc = cv2.VideoCapture(device_number)

width = 1920
height = 1080

scale = 1

vc.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, width)
vc.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, height)


while(True):
    ret, frame = vc.read()
    if not ret:
        print "### Warning:  bad frame read!  ###"
    else:
        frame_small = cv2.resize(frame, (width//scale, height//scale))
        # frame_small = cv2.flip(frame_small, 1)  # flips for camera aimed at self
        cv2.imshow("Camera capture:  space to save, ESC to quit", frame_small)

        k = cv2.waitKey(10)
    if k == 27:  # escape key
        break
    elif k == 113:  # q
        break
    elif k == 32:  # spacebar
        # img_name = args['name'] + '_%02d.jpg' % image_num
        img_name = args['name'] + '_%02d.bmp' % image_num
        image_num += 1
        cv2.imwrite(img_name, frame)
        print 'Save %s.' % img_name

    elif k == -1:
        continue
    else:
        print "key %d pressed" % k


vc.release()
cv2.destroyAllWindows()
