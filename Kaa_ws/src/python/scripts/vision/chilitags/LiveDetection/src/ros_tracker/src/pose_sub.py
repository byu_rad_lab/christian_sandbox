#!/usr/bin/env python
import rospy
import cv2
import preferences as pref
import numpy as np
import Tag_Colors as tc
from ego_byu.tools import ChiliTracker as CT
from ego_byu.tools import kin_tools as kt

from ros_tracker.msg import pose_msg
from cv_bridge import CvBridge, CvBridgeError


class image_converter:

    def __init__(self):
        # Initialize Setup
        tags = CT.load_tags(pref.tag_set)
        camera = CT.load_camera(pref.camera_name)
        self.tracker = CT.Tracker(tags, camera)
        self.tracker.reprojection_tol = pref.reprojection_tol

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("pose_data", pose_msg, self.callback)
        self.image_num = 0

    def callback(self, msg):  # rospy.loginfo(data.data)
        # formats found image points in to tracker
        # Assume tag not found
        for tag in self.tracker.tags:
            self.tracker.tags[tag].is_found = False

        # Find tags in message, mark found in self.tracker
        self.format_pixel_data(msg.tags)

        # estimate camera pose
        self.tracker.estimate_cw_pose()

        try:
            cv_image = self.bridge.imgmsg_to_cv2(msg.image, "bgr8")
        except CvBridgeError as e:
            print e

        # Wait for user input
        k = cv2.waitKey(1)
        if k == 1048603 or k == 27:  # escape key
            cv2.destroyAllWindows()
            rospy.signal_shutdown("Escape Key Pressed")

        elif k == 113:  # q
            cv2.destroyAllWindows()
            rospy.signal_shutdown("Q Key Pressed")

        elif k == 1048608:  # space
            image_name = "image_" + str(self.image_num) + ".jpg"
            print "Image ", image_name, " saved in saved_pics."
            cv2.imwrite("saved_pics/" + image_name, cv_image)
            self.image_num += 1

        # label image points on image using image points
        CT.label_tags(cv_image, self.tracker.tags)

        # label image points on image using camera to world transform
        self.tracker.reproject_body(cv_image, 6, tc.magenta, 4, "world")

        # Find and draw body frame from passed data
        rwb, twb = kt.hom2ax_vec(np.array(msg.gwb).reshape(4, 4))
        self.tracker.cam.draw_axis(cv_image, rwb, twb, pref.length*1.5, 5)

        self.tracker.imshow_small("SubScriber: ESC to exit, Space to take a picture.", cv_image, pref.scale)

    def format_pixel_data(self, tags):
        tag_dict = {}

        for tag in tags:
            key = tag.key
            # print key
            pixels = np.reshape(np.array(tag.corner_pixels), (-1, 2))

            if key != -1:
                tag_dict[key] = np.array(pixels)
        self.tracker.bad_tags = self.tracker.process_tags(tag_dict)


def listener():
    ic = image_converter()
    rospy.init_node('listener', anonymous=True)

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "Shutting down"
    cv2.destroyAllWindows()


if __name__ == '__main__':
    listener()
