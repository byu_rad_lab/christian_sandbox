#!/usr/bin/env python
import numpy as np

#Key Selection
world_keys = np.array([1,2,3,4])
tracked_keys = [6,7,8,9,10,11,12,13]
tracked_keys = np.append(world_keys,tracked_keys)
# tag_set = 'stumpy'
tag_set = 'paper-byu'

#Camera/Picture Settings
# camera_name = 'FullHD'
camera_name = 'Kinect'
camera_name2 = 'FullHD'

#Tolerances
reprojection_tol = 50

#For Drawing Coordinate Frames
length = 0.030
thickness = 3

#For Scaling Images
scale = 2

#Max Number of Found Tags
tagmax = 30

#Body to be tracked
tracked_bodies = ['body1','body2']

#Save Images
save = False

# show_marked = False
show_marked = True

