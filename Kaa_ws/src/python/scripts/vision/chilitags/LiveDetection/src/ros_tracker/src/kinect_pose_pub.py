#!/usr/bin/env python
# The purpose of this file is to process images and return gwc using

from __future__ import division
import cv2
import rospy
import time
import numpy as np
import preferences as pref
from ego_byu.tools import Tag_Colors as tc
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import ChiliTracker as CT
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from geometry_msgs.msg import Twist
from ros_tracker.msg import pose_msg

width = 1920
height = 1080
scale = 2

# For Tag Frames
length = 0.030
thickness = 3

# previous gwc stored as global variable for filtering
# Proportion of previous element to include
# p = .1

class kinect_pose_publisher(object):
    def __init__(self,tracker,keys,topic_name = "camera1"):
        self.img = []
        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("kinect2/hd/image_color", Image, self.callback)
        self.image_sub = rospy.Subscriber("body1", Twist, self.callback_estimator)
        self.pub = rospy.Publisher(topic_name, pose_msg, queue_size=10)
        self.tracker = tracker
        self.keys = keys

        self.gwc_prev = None
        self.gwc_log = []
        self.world_detections = []

        self.gcb_prev = {}
        self.gcb_log = {}
        self.body_detections = {}

        for body in pref.tracked_bodies:
            self.gcb_prev[body] = None
            self.gcb_log[body] = []
            self.body_detections[body] = []

        self.image_num = 0
        self.rate = rospy.Rate(30)  # Runs about 15 HZ
        self.estimated_gwb = []


    def callback(self,msg):
        try:
            img = self.bridge.imgmsg_to_cv2(msg, "bgr8")
            # print "got image"
        except CvBridgeError as e:
            print e

        if img is None or []:
            print "###Warning: Can't read image"
            assert(False)

        # Process Image and assign image points to tags: return dictionary  with detections
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        print "frame %d" % self.image_num

        for tag in self.tracker.tags.itervalues():
            tag.is_found = False
        detections = self.tracker.find_chilitags(img_gray)

        # Fill Message Data
        message = pose_msg()
        message.tags_found = len(detections)

        # Set all tag data to -1 (Not found)
        for i in range(pref.tagmax):
            message.tags[i].key = -1
            message.tags[i].corner_pixels = [-1,-1,-1,-1,-1,-1,-1,-1]

        # To Publish Pixels of Tags Found: Fill in found tags
        # i = 0
        # for key in detections:
        #     message.tags[i].key = key
        #     message.tags[i].corner_pixels = np.array(detections[key]).reshape(8).tolist()
        #     i += 1

        #     if i == pref.tagmax:
        #         print "Warning: More than %d tags found, some tags are unpublished" % pref.tagmax
        #         break

        # Estimate gwc and locate tags in world
        self.tracker.estimate_cw_pose()

        order = 5

        # Complementary Filter Implementation: First 5 poses taking an average of axis angle representations
        rwc, twc = kt.hom2ax_vec(self.tracker.cam.gwc)
        self.gwc_log.append(np.hstack([rwc, twc]))
        self.world_detections.append(self.tracker.world_detections)
        if len(self.gwc_log) < order:
            gwc = [x/len(self.gwc_log) for x in sum(self.gwc_log)]
            gwc = kt.ax_vec2homog(np.array(gwc[:3]), np.array(gwc[3:]))

        else:
            # World should be stationary
            max_read = max(self.world_detections)
            gwc = kt.ax_vec2homog(np.array(self.gwc_log[-1][:3]),np.array(self.gwc_log[-1][3:]))
            if self.world_detections[-1] < 3:
                gwc = self.gwc_prev
                if max_read < 3:
                    print "###Warning: Less than 3 world frame tags detected in window, camera not oriented"

            # Update Logs
            self.gwc_log.pop(0)
            self.world_detections.pop(0)

        message.gwc = gwc.reshape(16).tolist()

        # Keep track of most recent read, as well as most recent reported read
        self.gwc_prev = gwc

        # Draw Axis before updated gwc
        # self.tracker.cam.draw_axis(img, np.zeros(3), np.zeros(3), pref.length*1.5, 5, "unfiltered")

        # Update gwc,rcw,tcw, in self.tracker
        self.tracker.cam.gwc = gwc
        self.tracker.cam.rcw, self.tracker.cam.tcw = kt.hom2ax_vec(kt.homog_inv(gwc))

        # Draw World
        self.tracker.cam.draw_axis(img, np.zeros(3), np.zeros(3), pref.length*1.5, 5)

        # Estimate gbc
        self.tracker.estimate_cb_poses(pref.tracked_bodies)
        gwb_message = np.array([])

        # Complementary Filter Implementation: First 10 poses taking an average of entries in list
        # Tags disappearing between frames causes flickering. It will be important to implement
        # a better system of filtering body poses. setFilter option in chilipy is important
        #self.tracker.cam.draw_axis(img,np.zeros(3),np.zeros(3),pref.length*1.5, 5,"filtered","top_plate")
        for body in pref.tracked_bodies:
            rcb, tcb = kt.hom2ax_vec(self.tracker.cam.gcb[body])
            self.gcb_log[body].append(np.hstack([rcb, tcb]))
            self.body_detections[body] = self.tracker.body_detections[body]
            if len(self.gcb_log[body]) < order:  # First 5 pose Case scenario
                gcb = [x/len(self.gcb_log[body]) for x in sum(self.gcb_log[body])]
                gcb = kt.ax_vec2homog(np.array(gcb[:3]), np.array(gcb[3:]))

            else:  # Complementary Filter: After initial horizon
                # If recent read is bad, take last
                # error, ep,eo = kt.homog_error(self.tracker.cam.gcb,self.gcb_prev,.2)
                if self.body_detections[body] < 2:
                    gcb = self.gcb_prev[body]

                # Else use a complementary filter, trusting new values as opposed to old
                else:
                    h = .8
                    r = self.gcb_log[body][-1][:3]
                    t = self.gcb_log[body][-1][3:]
                    rp = self.gcb_log[body][-2][:3]
                    tp = self.gcb_log[body][-2][3:]
                    r = h*rp+(1-h)*r
                    t = h*tp+(1-h)*t
                    gcb = kt.ax_vec2homog(r,t)

                # Update Logs
                self.gcb_log[body].pop(0)
            self.gcb_prev[body] = gcb

            gwb = np.dot(gwc, gcb)
            rwb, twb = kt.hom2ax_vec(gwb)
            gwb_message = np.append(gwb_message,gwb)

            rcb, tcb = kt.hom2ax_vec(gcb)
            self.tracker.cam.rcb[body] = rcb
            self.tracker.cam.tcb[body] = tcb

            # Draw Axis (from top plate frame) -- The filtered version of the body is not yet tuned.
            # Tags disappearing between frames causes flickering. It will be important to implement
            # a better system of filtering body poses. setFilter option in chilipy is important
            if pref.show_marked:
                self.tracker.cam.draw_axis(img,rwb,twb,pref.length*1.5, 5,) #Project from world
                if self.estimated_gwb != []:
                    r,t = kt.hom2ax_vec(self.estimated_gwb)
                    self.tracker.cam.draw_axis(img,r,t,pref.length*1, 5,"uncolored") #Project from world
            # self.tracker.cam.draw_axis(img,np.zeros(3),np.zeros(3),pref.length*1.5, 5,"filtered","body")

        message.gwb= gwb_message.reshape(-1).tolist()
        message.body_reads = self.body_detections.values()
        message.world_reads = self.world_detections[-1]

        if pref.save or pref.show_marked:
            self.tracker.label_image(img)
            self.tracker.reproject_body(img, 6, tc.magenta, 4, "world")
            for body in pref.tracked_bodies:
                self.tracker.reproject_body(img, 6, tc.yellow, 4, body)

        if pref.show_marked:
            self.tracker.imshow_small('Marked', img, pref.scale)
            cv2.waitKey(3)

        if pref.save:
            image_name = "saved_pics/image_%03d.jpg" % self.image_num
            cv2.imwrite(image_name, img)
            print "Saved: ", image_name
        self.image_num += 1


        # Publish Data
        self.pub.publish(message)
        self.rate.sleep()

    def callback_estimator(self,msg):
        r = np.zeros([3,1])
        t = np.zeros([3,1])
        t[0] = msg.linear.x
        t[1] = msg.linear.y
        t[2] = msg.linear.z
        r[0] = msg.angular.x
        r[1] = msg.angular.y
        r[2] = msg.angular.z
        gwb = kt.ax_vec2homog(r,t)
        self.estimated_gwb = gwb


# def get_frame():
#     # Captures a frame every 1 milliseconds (not quite that fast)
#     while(True):
#         frame =
#         if not ret:
#             print "### Warning:  bad frame read!  ###"
#         else:
#             frame_small = cv2.resize(frame, (width//scale, height//scale))
#             cv2.imshow("Camera Publisher, ESC to quit", frame_small)
#             k = cv2.waitKey(1)

#         if k == 27 or k == 1048603:  # escape key
#             vc.release()
#             cv2.destroyAllWindows()
#             rospy.signal_shutdown("Escape Key Hit")
#             break

#         elif k == 113:  # q
#             vc.release()
#             cv2.destroyAllWindows()
#             rospy.signal_shutdown("Escape Key Hit")
#             break

#         elif k == -1:
#             return frame
#             continue
#         else:
#             print "key %d pressed" % k


# if __name__ == '__main__':
#     try:
        # # Setup Tracker Object
        # tag_set = pref.tag_set
        # tracked_keys = pref.tracked_keys  # Keys must be defined in self.tracker sheet!
        # camera_name = pref.camera_name
        # device_number = pref.device_number

        # # Initialize Setup
        # tags = CT.load_tags(tag_set)
        # camera = CT.load_camera(camera_name)
        # self.tracker = CT.Tracker(tags, camera)
        # self.tracker.reprojection_tol = pref.reprojection_tol

        # vc = cv2.VideoCapture(device_number)
        # vc.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, width)
        # vc.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, height)

        # # Begin Publishing
        # pos_publisher(self.tracker, tracked_keys)

    # except rospy.ROSInterruptException:
    #     pass



if __name__ == '__main__':
    try:
        while not rospy.is_shutdown():
            rospy.init_node('pos_publisher_kinect', anonymous=True)
            # Setup Tracker Object
            tag_set = pref.tag_set
            tracked_keys = pref.tracked_keys  # Keys must be defined in self.tracker sheet!
            camera_name = pref.camera_name

            # Initialize Setup
            tags = CT.load_tags(tag_set)
            camera = CT.load_camera(camera_name,pref.tracked_bodies)
            tracker = CT.Tracker(tags, camera)
            tracker.reprojection_tol = pref.reprojection_tol

            # Begin Publishing
            pub = kinect_pose_publisher(tracker, tracked_keys)
            rospy.spin()

    except rospy.ROSInterruptException:
        pass
