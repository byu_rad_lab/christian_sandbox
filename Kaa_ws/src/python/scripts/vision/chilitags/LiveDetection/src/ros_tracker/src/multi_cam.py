#!/usr/bin/env python
# The purpose of this file is to process images and return gwc using

from __future__ import division
import cv2
import rospy
import time
import numpy as np
import preferences as pref
from ego_byu.tools import Tag_Colors as tc
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import ChiliTracker as CT
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from ros_tracker.msg import pose_msg
import kinect_pose_pub as kpp
import fullHD_pose_pub as fpp
from geometry_msgs.msg import Twist

width = 1920
height = 1080
scale = 2

# For Tag Frames
length = 0.030
thickness = 3

# previous gwc stored as global variable for filtering
# Proportion of previous element to include
# p = .1



class frame_estimator(object):
    def __init__(self):
        self.cam1 = rospy.Subscriber("camera1", pose_msg, self.camera1_callback)
        self.cam2 = rospy.Subscriber("camera2", pose_msg, self.camera2_callback)
        self.pub = rospy.Publisher("body1", Twist, queue_size=10)
        self.rate = rospy.Rate(30)  # Runs about 15 HZ

        #Frames estimated from lowest number camera by default
        self.body_estimate_camera = {}
        self.gwb = {}

        for body in pref.tracked_bodies:
            self.gwb[body] = np.eye(4)
            self.body_estimate_camera[body] = float("inf")


    def camera1_callback(self,msg):
        gwbs = np.array(msg.gwb).reshape(-1,4,4)
        body_detections = msg.body_reads
        world_detections = msg.world_reads
        self.estimate_frame(gwbs,world_detections,body_detections,1)
        self.publish()

    def camera2_callback(self,msg):
        gwbs = np.array(msg.gwb).reshape(-1,4,4)
        body_detections = msg.body_reads
        world_detections = msg.world_reads
        self.estimate_frame(gwbs,world_detections,body_detections,2)
        self.publish()

    def estimate_frame(self,gwbs,world_detections,body_detections,camnum):
        for i,body in enumerate(pref.tracked_bodies):
            if self.body_estimate_camera[body] >= camnum:
                if body_detections[i] > 2 and world_detections > 2:
                    self.gwb[body] = gwbs[i]
                    self.body_estimate_camera[body] = camnum
                else:
                    self.body_estimate_camera[body] = camnum+1

    def publish(self):
        message  = Twist()
        r, t = kt.hom2ax_vec(self.gwb["body1"])
        message.linear.x = t[0]
        message.linear.y = t[1]
        message.linear.z = t[2]
        message.angular.x = r[0]
        message.angular.y = r[1]
        message.angular.z = r[2]
        self.pub.publish(message)
        print "published: t:", message.linear, " r: ", message.angular


if __name__ == '__main__':

    try:
        while not rospy.is_shutdown():
            rospy.init_node('pose_estimator', anonymous=True)
            estimator = frame_estimator()
            rospy.spin()

    except rospy.ROSInterruptException:
        pass
