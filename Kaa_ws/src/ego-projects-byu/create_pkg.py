#! /usr/bin/env python

"""Create Package

This script creates a new catkin package in the ego-project's
src directory.

Example Usage:
    $ ./create_pkg.py <new_package_name> <dependency> <dependency> ...


"""

import subprocess
import argparse

def create_catkin_package(package_name, dependencies=None):
    """Create a catkin package in the ego-project catkin workspace
    named <package_name> and with the dependencies, <dependencies>.  The package
    will additionally have the dependencies required for projects based on the
    orthotics core.

    Args:
        package_name (str): the new package's name
        dependencies ([str]): a list of dependencies for the new catkin package

    """

    if not dependencies:
        dependencies = []

    core_project_dependencies = ["core",
                                 "dynamic_reconfigure",
                                 "ethercat_master",
                                 "message_generation",
                                 "message_runtime",
                                 "primitives",
                                 "ros_utils",
                                 "roscpp",
                                 "std_msgs"]

    create_package_command = ["catkin_create_pkg"]

    package_name_list = [package_name]

    catkin_creation_args = create_package_command + package_name_list \
                                + core_project_dependencies + dependencies

    subprocess.call(catkin_creation_args)

    subprocess.call(["mv", package_name, "src/"])


def _find_files_directories(path_to_contents):
    """Find all files and directories stored at the path location.

    Args:
        path_to_contents (str): location to search

    Returns:
        (files, directories): files is a list of files, directories is a list
            of directories in the current location

    """

    files_directories = subprocess.check_output(["find", path_to_contents])

    example_files_list = files_directories.splitlines()

    files = []
    directories = []

    for each_file in example_files_list:

        file_is_not_a_directory = subprocess.call(["test", "-d", each_file])

        if file_is_not_a_directory:
            files.append(each_file)
        else:
            directories.append(each_file)

    return (files, directories)


def create_package_files(path_to_core_example_project, package_name):
    """Create the package files for the new package.

    Args:
        path_to_core_example_project (str): the location of the core example
            project.
        package_name (str): the name of the new package

    """

    new_project_location = "./src"

    files, directories = _find_files_directories(path_to_core_example_project)

    for directory in directories:

        new_directory_location = directory.replace(
            path_to_core_example_project, "/"+package_name)

        new_directory_location = new_directory_location.replace(
            "core_example_project", package_name)

        directory_destination = new_project_location + new_directory_location

        subprocess.call(["mkdir", "-p", directory_destination])

    for each_file in files:

        new_file_location = each_file.replace(
            path_to_core_example_project, "/"+package_name)

        new_file_location = new_file_location.replace(
            "core_example_project", package_name)

        file_destination = new_project_location + new_file_location

        subprocess.call(["cp", each_file, file_destination])

        lines = []

        with open(file_destination, "r") as copied_file_original:
            for line in copied_file_original:
                line = line.replace("core_example_project", package_name)

                line = line.replace("CORE_EXAMPLE_PROJECT",
                                    package_name.upper())

                lines.append(line)

        with open(file_destination, "w") as copied_file_edited:
            for line in lines:
                copied_file_edited.write(line)


def meld_cmake(path_to_core_example_project, package_name):
    """Opens 'meld' to edit the new package's CMakeLists.txt file via merging
    the core example project's CMakeLists.txt file with the newly created
    package's CMakeLists.txt file.

    Args:
        path_to_core_example_project (str): path to core_example_project
        package_name (str): the name of the new package

    """

    core_cmake_file = "%s/CMakeLists.txt" % path_to_core_example_project

    copy_of_core_cmake_file = "./src/%s/Core_CMakeLists.txt" % package_name

    subprocess.call(["cp", core_cmake_file, copy_of_core_cmake_file])

    lines = []
    with open(copy_of_core_cmake_file, "r") as core_cmake_orig:
        for line in core_cmake_orig:
            line = line.replace("core_example_project", package_name)
            line = line.replace("CORE_EXAMPLE_PROJECT", package_name.upper())
            lines.append(line)
    with open(copy_of_core_cmake_file, "w") as core_cmake_edited:
        for line in lines:
            core_cmake_edited.write(line)

    cmake_file = "./src/%s/CMakeLists.txt" % package_name

    subprocess.call(["meld", cmake_file, copy_of_core_cmake_file])

    subprocess.call(["rm", copy_of_core_cmake_file])


def main():
    """Creates a new package in the src directory of the catkin
    workspace.

    """

    parser = argparse.ArgumentParser(
        description="Creates a new project")

    parser.add_argument(
        "package_name", nargs=1, help="The name for the project")

    parser.add_argument(
        "dependencies", nargs="*", help="Catkin package dependencies")

    args = parser.parse_args()

    package_name = args.package_name[0]

    dependencies = args.dependencies

    path_to_core_example_project = \
        "../ego-core/src/core_example_project"

    create_catkin_package(package_name, dependencies)

    create_package_files(path_to_core_example_project, package_name)

    meld_cmake(path_to_core_example_project, package_name)


if __name__ == "__main__":
    main()
