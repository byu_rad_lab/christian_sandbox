#!/bin/bash

catkin_make --force-cmake -G"Eclipse CDT4 - Unix Makefiles"

echo "Done with catkin_make and generating .project file for Eclipse."
echo ""
echo "Passing the current shell environment into the make process in Eclipse."

awk -f $(rospack find mk)/eclipse.awk build/.project > build/.project_with_env
mv build/.project_with_env build/.project

cd build 
cmake ../src -DCMAKE_BUILD_TYPE=Debug

cd ../

echo "Update Eclipse is Done."
