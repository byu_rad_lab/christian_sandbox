#!/usr/bin/env python

from ego_byu.madgwick import madgwick as md
import numpy as np
import rospy
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Quaternion
from geometry_msgs.msg import Vector3Stamped
from geometry_msgs.msg import TransformStamped
import message_filters
from threading import RLock
import tf
import tf2_ros
from falkor.msg import robot_state
import tf.transformations as tft

#Based off the Madgick Filter
class IMU_Madgwick_Filter(object):
    def __init__(self,topic, sensor_num, world_frame,gain,publish_option=False):
        FilterIterations = 10000
        self.initialized = False
        self.lock = RLock()
        self.rate = 100
        self.Rate = rospy.Rate(self.rate)
        self.dt  = 0.0
        self.sensor_num = sensor_num

        self.Mbias_x = 0
        self.Mbias_y = 0
        self.Mbias_z = 0

        self.Filter = md.ImuFilter()
        self.Filter.setWorldFrame(world_frame)

        self.Filter.setAlgorithmGain(gain)
        self.reversed_ = True
        self.first_iter = 0
        self.br = tf2_ros.TransformBroadcaster()
        self.q = []

        # #Subscribers
        s1 = rospy.Subscriber(topic, robot_state, self.callback)
        # ts = message_filters.ApproximateTimeSynchronizer([s1, s2],10,.01)
        # ts.registerCallback(self.callback)

        #Publishers (For this Module Only)
        self.publish = publish_option
        if self.publish:
            self.IMU_pub = rospy.Publisher('/Imu_Orientation',Imu,queue_size=5)

    def callback(self,msg):
        self.lock.acquire()

        A = msg.sensors[self.sensor_num].acc_cal
        G = msg.sensors[self.sensor_num].gyr_cal
        M = msg.sensors[self.sensor_num].mag_cal

        self.time = msg.header.stamp;
        frame_id = msg.header.frame_id

        if self.dt == 0:
            self.dt = 1.0/self.rate
        else:
            self.dt = (self.time - self.last_time).to_sec()

        M_comp = Vector3Stamped
        M_comp_x = M[0] - self.Mbias_x; #Need to Update Mbias
        M_comp_y = M[1] - self.Mbias_y;
        M_comp_z = M[2] - self.Mbias_z;

        self.Filter.madgwickAHRSupdate(
        A[0], A[1], A[2],
        G[0], G[1], G[2],
        M_comp_x, M_comp_y, M_comp_z,
        self.dt);
        self.q = self.Filter.returnOrientation()

        self.last_time = msg.header.stamp
        self.lock.release()

    def PublishFilteredMessage(self):
        imu_msg = Imu()
        self.stamp = imu_msg.header.stamp
        imu_msg.orientation.w = self.q[0]
        if self.reversed_:
            imu_msg.orientation.x = -self.q[1]
            imu_msg.orientation.y = -self.q[2]
            imu_msg.orientation.z = -self.q[3]
        else:
            imu_msg.orientation.x = self.q[1]
            imu_msg.orientation.y = self.q[2]
            imu_msg.orientation.z = self.q[3]

        imu_msg.orientation_covariance[0] = 0 #Should be adjusted to be variance
        imu_msg.orientation_covariance[1] = 0
        imu_msg.orientation_covariance[2] = 0
        imu_msg.orientation_covariance[3] = 0
        imu_msg.orientation_covariance[4] = 0
        imu_msg.orientation_covariance[5] = 0
        imu_msg.orientation_covariance[6] = 0
        imu_msg.orientation_covariance[7] = 0
        imu_msg.orientation_covariance[8] = 0
        self.IMU_pub.publish(imu_msg)

    def PublishTransform(self):
        transform = TransformStamped()
        transform.header.stamp = self.stamp
        transform.header.frame_id = "my_IMU"
        transform.child_frame_id = "odom"
        transform.transform.rotation.w = self.q[0]
        if self.reversed_:
            transform.transform.rotation.x = -self.q[1]
            transform.transform.rotation.y = -self.q[2]
            transform.transform.rotation.z = -self.q[3]
        else:
            transform.transform.rotation.x = self.q[1]
            transform.transform.rotation.y = self.q[2]
            transform.transform.rotation.z = self.q[3]
        self.br.sendTransform(transform)


    def Run(self):
        while not rospy.is_shutdown():
            if len(self.q)>0:
                if self.first_iter == 0:
                    print "Filtering Data"
                    self.first_iter = 1
                if self.publish:
                    self.PublishFilteredMessage()
                    self.PublishTransform()
                    self.Rate.sleep()
            else:
                rospy.sleep(1)

if __name__ == "__main__":
    print "Starting IMU Filter"
    rospy.init_node("My_Node")

    gain = .1
    world_frame = md.NWU
    topic = '/IMU0'
    sensor_num = 0
    imu_reader = IMU_Reader(topic, sensor_num,world_frame,gain)
    imu_reader.Run()

