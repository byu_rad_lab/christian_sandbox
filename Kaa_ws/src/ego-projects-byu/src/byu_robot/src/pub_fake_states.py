#!/usr/bin/env python

import os
import rospy
import numpy as np
from math import *
from geometry_msgs.msg import PoseStamped
from byu_robot.msg import commands
from byu_robot.msg import sim_robot
from byu_robot.msg import states
from falkor.msg import robot_state
import tf.transformations as tft
from tf2_msgs.msg import TFMessage
from threading import RLock
from copy import copy
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import pickle_tools as pk
from ego_byu.tools import arm_tools as at
from ego_byu.madgwick import madgwick as md
from ego_byu.tools import IMU_Filter as imu
from dynamic_reconfigure.server import Server as DynamicReconfigureServer
from byu_robot.cfg import estimatorConfig as ConfigType



def main():
    rospy.init_node('Fake_States_Publisher',anonymous=False)
    DOF = 4;
    q = [0.]*DOF
    qdot = [0.]*DOF
    pPlus = [0.]*DOF
    pMinus = [0.]*DOF
    pubStates = rospy.Publisher('/states', states, queue_size=1000)    
    while not rospy.is_shutdown():
        msg = states()
        msg.q = q
        msg.qdot = qdot
        msg.pPlus = pPlus
        msg.pMinus = pMinus
        pubStates.publish(msg)

    print 'Shutting Down Fake State Publisher'

if __name__=='__main__':
    main()
    
    
