#!/usr/bin/env python

from byu_robot.msg import commands

import rospy
import time
from math import pi
rospy.init_node('jointCommands', anonymous =False)
start = time.time()
pub = rospy.Publisher('/commands', commands, queue_size = 1)

while not rospy.is_shutdown():

    while time.time() - start < 65.:
        if time.time() - start < 20.:
            qCmd = [-30.0*pi/180., 60.*pi/180. , 20.*pi/180., 20.*pi/180.];
            # qCmd = [0.0, 0.0, 0.0, 0.0];
        # elif time.time() - start < 35.:
        #     qCmd = [-20.0*pi/180., -20.*pi/180. , -20.*pi/180., -20.*pi/180.];
        # elif time.time() - start < 50.:
        #     qCmd = [-30.0*pi/180., 60.*pi/180. , 20.*pi/180., 20.*pi/180.];
        else:
            qCmd = [-30.0*pi/180., 60.*pi/180. , 20.*pi/180., 20.*pi/180.];
            # qCmd = [0.0*pi/180., 0.*pi/180. , 0.*pi/180., 0.*pi/180.];

            
        msg = commands()
        msg.q = qCmd
        msg.qdot = [0,0,0,0]
        pub.publish(msg)
        rospy.sleep(0.002)


    


