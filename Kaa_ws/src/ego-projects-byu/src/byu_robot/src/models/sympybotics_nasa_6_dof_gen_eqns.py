
import sympy
import sympybotics
import math

pi = sympy.pi
q = sympybotics.robotdef.q

alpha = [pi/2, -pi/2, pi/2, -pi/2, pi/2, 0]
a = [0.0600, 0.5270, 0.3460, 0.2240, 0.2700, 0.0300]
d = [0, 0, 0, 0, 0, 0]
theta = [q, q, q, q, q, q]

#            
dh_params = [(alpha[0], a[0], d[0], theta[0]),
             (alpha[1], a[1], d[1], theta[1]),
             (alpha[2], a[2], d[2], theta[2]),
             (alpha[3], a[3], d[3], theta[3]),
             (alpha[4], a[4], d[4], theta[4]),
             (alpha[5], a[5], d[5], theta[5])]

rbtdef = sympybotics.RobotDef('NASA Dev Arm', dh_params, 'standard')
rbtdef.gravityacc = sympy.Matrix([9.81,0.0,0.0])
rbtdef.frictionmodel = {'viscous'} #'simple' # options are 'simple' and None, defaults to None

print(rbtdef.dynparms())

rbt = sympybotics.RobotDynCode(rbtdef)

arm = 'nasa_6_dof'

f_handle = open('./' + arm + '_dynamics.py', 'w+')

print >> f_handle, "from math import sin, cos"
print >> f_handle, "import numpy as np\n\n\n"

print >> f_handle, sympybotics.robotcodegen.robot_code_to_func( 'python', rbt.invdyn_code, 'tau_out', 'tau', rbtdef)
print >> f_handle, '\n\n\n'
print >> f_handle, sympybotics.robotcodegen.robot_code_to_func( 'python', rbt.H_code, 'regressor_out', 'regressor', rbtdef )
print >> f_handle, '\n\n\n'
print >> f_handle, sympybotics.robotcodegen.robot_code_to_func( 'python', rbt.M_code, 'M_out', 'M',  rbtdef)
print >> f_handle, '\n\n\n'
print >> f_handle, sympybotics.robotcodegen.robot_code_to_func( 'python', rbt.c_code, 'c_out', 'c', rbtdef)
print >> f_handle, '\n\n\n'
print >> f_handle, sympybotics.robotcodegen.robot_code_to_func('python', rbt.C_code, 'C_out', 'C', rbtdef)
print >> f_handle, '\n\n\n'
print >> f_handle, sympybotics.robotcodegen.robot_code_to_func( 'python', rbt.f_code, 'f_out', 'f', rbtdef)
print >> f_handle, '\n\n\n'
print >> f_handle, sympybotics.robotcodegen.robot_code_to_func( 'python', rbt.g_code, 'g_out', 'g', rbtdef)
print >> f_handle, '\n\n\n'


print >> f_handle, '\n\n\n'
#print >> f_handle, 'from coriolis import *'
print >> f_handle, '\n\n\n' 
print >> f_handle, '#dynparms = '+str(rbtdef.dynparms())
 

#f_dMdq_handle.close()
f_handle.close()

f_kin = open('./'+arm+'_kinematics.py', 'w+')
print >> f_kin, "from math import sin, cos"
#print >> f_kin, "from offset_util import offset_and_reshape"
print >> f_kin, "import numpy as np"
print >> f_kin, "pi = np.pi\n\n\n"
fk_dict = "FK = {"
for i in range(len(rbt.geo.T)):
    joint_fk_code = sympy.cse(rbt.geo.T[i])
    fk_string = sympybotics.robotcodegen.robot_code_to_func('python', joint_fk_code, 'pose', 'joint_fk' + str(i).zfill(2), rbtdef)
    fk_list_string = fk_string.split('\n')
    #fk_list_string.insert(-1, '    pose = offset_and_reshape(pose,'+str(x_offset)+','
#                           +str(y_offset)+','
#                           +str(z_offset)+')')
    fk_final = "\n".join(fk_list_string)
    print >> f_kin, fk_final + '\n\n\n'
    fk_dict = fk_dict+str(i)+":joint_fk"+str(i).zfill(2)+", "

print >> f_kin, fk_dict+"}\n\n\n"

jac_dict = "J = {"
for i in xrange(len(rbt.kin.J)):
    joint_jac_code = sympy.cse(rbt.kin.J[i])
    jac_string = sympybotics.robotcodegen.robot_code_to_func('python', joint_jac_code, 'jacobian', 'jacobian' + str(i).zfill(2), rbtdef)

    jac_list_string = jac_string.split('\n')
    jac_list_string.insert(-1, '    jacobian = np.array(jacobian).reshape(6,'+str(rbt.dof)+')')
    jac_final = "\n".join(jac_list_string)
    print >> f_kin, jac_final + '\n\n\n'
    jac_dict = jac_dict + str(i)+":jacobian"+str(i).zfill(2)+", "

print >> f_kin, jac_dict+"}"

f_kin.close()


