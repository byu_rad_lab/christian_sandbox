from math import sin, cos
import numpy as np
pi = np.pi



def joint_fk00(q):
#
    pose = [0]*16
#
    x0 = cos(q[0])
    x1 = sin(q[0])
#
    pose[0] = x0
    pose[1] = 0
    pose[2] = x1
    pose[3] = 0.06*x0
    pose[4] = x1
    pose[5] = 0
    pose[6] = -x0
    pose[7] = 0.06*x1
    pose[8] = 0
    pose[9] = 1
    pose[10] = 0
    pose[11] = 0
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    return pose



def joint_fk01(q):
#
    pose = [0]*16
#
    x0 = cos(q[0])
    x1 = cos(q[1])
    x2 = x0*x1
    x3 = sin(q[0])
    x4 = sin(q[1])
    x5 = x1*x3
#
    pose[0] = x2
    pose[1] = -x3
    pose[2] = -x0*x4
    pose[3] = 0.06*x0 + 0.527*x2
    pose[4] = x5
    pose[5] = x0
    pose[6] = -x3*x4
    pose[7] = 0.06*x3 + 0.527*x5
    pose[8] = x4
    pose[9] = 0
    pose[10] = x1
    pose[11] = 0.527*x4
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    return pose



FK = {0:joint_fk00, 1:joint_fk01, }



def jacobian00(q):
#
    jacobian = [0]*12
#

    jacobian[0] = -0.06*sin(q[0])
    jacobian[1] = 0
    jacobian[2] = 0.06*cos(q[0])
    jacobian[3] = 0
    jacobian[4] = 0
    jacobian[5] = 0
    jacobian[6] = 0
    jacobian[7] = 0
    jacobian[8] = 0
    jacobian[9] = 0
    jacobian[10] = 1
    jacobian[11] = 0
#
    jacobian = np.array(jacobian).reshape(6,2)
    return jacobian



def jacobian01(q):
#
    jacobian = [0]*12
#
    x0 = sin(q[0])
    x1 = cos(q[1])
    x2 = 0.527*x0
    x3 = sin(q[1])
    x4 = cos(q[0])
    x5 = 0.527*x4
    x6 = 0.527*x1
#
    jacobian[0] = -0.06*x0 - x1*x2
    jacobian[1] = -x3*x5
    jacobian[2] = x1*x5 + 0.06*x4
    jacobian[3] = -x2*x3
    jacobian[4] = 0
    jacobian[5] = x0**2*x6 + x4**2*x6
    jacobian[6] = 0
    jacobian[7] = x0
    jacobian[8] = 0
    jacobian[9] = -x4
    jacobian[10] = 1
    jacobian[11] = 0
#
    jacobian = np.array(jacobian).reshape(6,2)
    return jacobian



J = {0:jacobian00, 1:jacobian01, }
