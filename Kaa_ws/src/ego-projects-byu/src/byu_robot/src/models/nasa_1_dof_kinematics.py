from math import sin, cos
import numpy as np
pi = np.pi



def joint_fk00(q):
#
    pose = [0]*16
#
    x0 = cos(q[0])
    x1 = sin(q[0])
#
    pose[0] = x0
    pose[1] = 0
    pose[2] = x1
    pose[3] = 0.06*x0
    pose[4] = x1
    pose[5] = 0
    pose[6] = -x0
    pose[7] = 0.06*x1
    pose[8] = 0
    pose[9] = 1
    pose[10] = 0
    pose[11] = 0
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    return pose



FK = {0:joint_fk00, }



def jacobian00(q):
#
    jacobian = [0]*6
#

    jacobian[0] = -0.06*sin(q[0])
    jacobian[1] = 0.06*cos(q[0])
    jacobian[2] = 0
    jacobian[3] = 0
    jacobian[4] = 0
    jacobian[5] = 1
#
    jacobian = np.array(jacobian).reshape(6,1)
    return jacobian



J = {0:jacobian00, }
