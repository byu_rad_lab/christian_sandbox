import rospy
import numpy as np
from dynamics_utils import *
from math import pi, pow
import sys
sys.path.append('./..')


# Calculate the link center of gavity given the center of gravity of the bodies and the valves

def calc_cog(m_cyl, m_valve_1, m_valve_2, r_cyl, r_valve_1, r_valve_2):
    r_cyl = np.array(r_cyl)
    r_valve_1 = np.array(r_valve_1)
    r_valve_2 = np.array(r_valve_2)

    inter1 = r_cyl*m_cyl
    inter2 = r_valve_1*m_valve_1
    inter3 = r_valve_2*m_valve_2
    inter4 = m_cyl + m_valve_1 + m_valve_2

    inter5 = inter1 + inter2 + inter3

    r_cog = np.zeros(3)

    r_cog[0] = inter5[0] / inter4
    r_cog[1] = inter5[1] / inter4
    r_cog[2] = inter5[2] / inter4

    return r_cog.tolist()

# Calculate the Inertia of the link in the CoG frame with the bodies and
# the valves combined. This assumes the body is a cylider and the valve
# blocks are rectangular prisms


def calc_I(m_cyl, rad_in, rad_out, h_cyl, r_cyl, r_valve_1, r_valve_2):

    case = 0
    if r_valve_1 is None:
        case = 0
        r_cyl = np.array(r_cyl)
    elif r_valve_2 is None:
        case = 1
        r_cyl = np.array(r_cyl)
        r_valve_1 = np.array(r_valve_1)
    else:
        case = 2
        r_cyl = np.array(r_cyl)
        r_valve_1 = np.array(r_valve_1)
        r_valve_2 = np.array(r_valve_2)

    I = np.matrix(np.zeros([3, 3]))

    Ixx_cyl = (m_cyl/2.0) * (pow(rad_in, 2) + pow(rad_out, 2))
    Iyy_cyl = (m_cyl/12.0) * (3 * (pow(rad_in, 2) + pow(rad_out, 2)) + pow(h_cyl, 2))
    Izz_cyl = Iyy_cyl

    I_cyl = np.matrix([[Ixx_cyl, 0., 0.], [0., Iyy_cyl, 0.], [0., 0., Izz_cyl]])

    # These are measured values from the actual hardware
    h_valve = 0.40
    w_valve = 0.150
    l_valve = 0.180
    m_valve = 0.702

    Ixx_valve = (m_valve/12.) * (pow(h_valve, 2) + pow(l_valve, 2))
    Iyy_valve = (m_valve/12.) * (pow(h_valve, 2) + pow(w_valve, 2))
    Izz_valve = (m_valve/12.) * (pow(l_valve, 2) + pow(w_valve, 2))

    I_valve = np.matrix([[Ixx_valve, 0., 0.], [0., Iyy_valve, 0.], [0., 0., Izz_valve]])

    if case == 0:
        #        print 'case 1'
        I_cyl_new = I_cyl + m_cyl*(np.inner(r_cyl, r_cyl)*np.eye(3) - np.outer(r_cyl, r_cyl))
        I = I_cyl_new
    elif case == 1:
     #       print 'case 2'
        I_cyl_new = I_cyl + m_cyl*(np.inner(r_cyl, r_cyl)*np.eye(3) - np.outer(r_cyl, r_cyl))
        I_valve_1_new = I_valve + m_valve*(np.inner(r_valve_1, r_valve_1)
                                           * np.eye(3) - np.outer(r_valve_1, r_valve_1))
        I = I_valve_1_new + I_cyl_new
    else:
      #      print 'case 3'
        I_cyl_new = I_cyl + m_cyl*(np.inner(r_cyl, r_cyl)*np.eye(3) - np.outer(r_cyl, r_cyl))
        I_valve_1_new = I_valve + m_valve*(np.inner(r_valve_1, r_valve_1)
                                           * np.eye(3) - np.outer(r_valve_1, r_valve_1))
        I_valve_2_new = I_valve + m_valve*(np.inner(r_valve_2, r_valve_2)
                                           * np.eye(3) - np.outer(r_valve_2, r_valve_2))
        I = I_valve_1_new + I_valve_2_new + I_cyl_new

    return I

def compute(numJts):

  a = [0.0600, 0.5270, 0.3460, 0.2240, 0.2700, 0.0300]

  fv1 = 0.7
  fv2 = 0.7
  fv3 = 0.7
  fv4 = 0.7
  fv5 = 0.7
  fv6 = 0.7

# constants measured from the actual arm
  m_valve = 0.702
  m_joint = 0.4
  rad_in = 0.080/2.0
  rad_out = 0.160/2.0
  h_valve = 0.40

  m_cyl = [m_joint,
         m_joint + 0.2,
         0.902 - m_joint,
         1.082/2.0 - m_joint,
         1.082/2.0 - m_joint,
         m_joint/2.0]

  r_cyl = np.zeros([6, 3])

  r_cyl[0] = [-a[0]/2., 0., 0.]
  r_cyl[1] = [-a[1]/2., 0., 0.]
  r_cyl[2] = [-a[2]/2., 0., 0.]
  r_cyl[3] = [-a[3]/2., 0., 0.]
  r_cyl[4] = [-a[4]/2., 0., 0.]
  r_cyl[5] = [-a[5]/2., 0., 0.]

  r_valve = np.zeros([6, 3])

  r_valve[0] = [0., 0., 0.]  # This valve is on the base link and does not contribute inertia
  r_valve[1] = [-a[1] + 0.2, -(rad_out+(h_valve/2.)), 0.]  # This valve is on link 2
  r_valve[2] = [-0.2, 0., rad_out + (h_valve/2.)]  # This valve is on link 2
  r_valve[3] = [-0.2, 0., rad_out + (h_valve/2.)]  # This valve is on link 3
  r_valve[4] = [-a[4] + 0.2, -(rad_out + (h_valve/2.)), 0.]  # This valve is on link 5
  r_valve[5] = [-0.2, 0., rad_out + (h_valve/2.)]  # This valve is on link 5


  m1 = m_cyl[0]
  m2 = m_cyl[1] + 2.0*m_valve
  m3 = m_cyl[2] + m_valve
  m4 = m_cyl[3]
  m5 = m_cyl[4] + 2.0*m_valve
  m6 = m_cyl[5]

  r1 = calc_cog(m_cyl[0], 0, 0, r_cyl[0], r_valve[0], np.zeros(3))
#r2 = calc_cog(m_cyl[1], m_valve, m_valve, r_cyl[1], r_valve[1],r_valve[2])
  r2 = calc_cog(m_cyl[1], m_valve, m_valve, r_cyl[1], r_valve[1], np.zeros(3))  # r_valve[2])
  r3 = calc_cog(m_cyl[2], m_valve, 0, r_cyl[2], r_valve[3], np.zeros(3))
  r4 = calc_cog(m_cyl[3], 0, 0, r_cyl[3], np.zeros(3), np.zeros(3))
  r5 = calc_cog(m_cyl[4], m_valve, m_valve, r_cyl[4], r_valve[4], r_valve[5])
  r6 = calc_cog(m_cyl[5], 0, 0, r_cyl[5], np.zeros(3), np.zeros(3))

  #print "r1 : ", r1
  #print "r2 : ", r2
  #print "r3 : ", r3
  # print "r4 : ", r4
# print "r5 : ", r5
# print "r6 : ", r6

# print "r_cyl : " , r_cyl
# print "m_cyl : " , m_cyl
# print "r_valve : ", r_valve
# print "m_valve : ", m_valve

# print "rad_out : ", rad_out
# print "rad_in : ", rad_in
# print "a : ", a


  r_cyl_cog = np.zeros([6, 3])
  r_valve_cog = np.zeros([6, 3])

  r_cyl_cog[0] = -np.array(r1) + r_cyl[0]
  #r_cyl_cog[1] = -np.array(r2) + r_cyl[1] + np.array([.4,0,0])
  r_cyl_cog[1] = -np.array(r2) + r_cyl[1]  # + np.array([.4,0,0])
  r_cyl_cog[2] = -np.array(r3) + r_cyl[2]
  r_cyl_cog[3] = -np.array(r4) + r_cyl[3]
  r_cyl_cog[4] = -np.array(r5) + r_cyl[4]
  r_cyl_cog[5] = -np.array(r6) + r_cyl[5]

  r_valve_cog[0] = -np.array(r1) + r_valve[0]
  r_valve_cog[1] = -np.array(r2) + r_valve[1]
  r_valve_cog[2] = -np.array(r2) + r_valve[2]
  r_valve_cog[3] = -np.array(r3) + r_valve[3]
  r_valve_cog[4] = -np.array(r5) + r_valve[4]
  r_valve_cog[5] = -np.array(r5) + r_valve[5]

  I1_inter = calc_I(m_cyl[0], rad_in, rad_out, a[0], r_cyl_cog[0], None, None)
  #I2_inter = calc_I((m_cyl[1]+2.), rad_in, rad_out, a[1], r_cyl_cog[1],r_valve_cog[1],r_valve_cog[2])
  #I2_inter = calc_I((m_cyl[1]), rad_in, rad_out, a[1], r_cyl_cog[1],r_valve_cog[1],r_valve_cog[2])
  I2_inter = calc_I((m_cyl[1]), rad_in, rad_out, a[1], r_cyl_cog[1], r_valve_cog[1], None)
  I3_inter = calc_I(m_cyl[2], rad_in, rad_out, a[2], r_cyl_cog[2], r_valve_cog[3], None)
  I4_inter = calc_I(m_cyl[3], rad_in, rad_out, a[3], r_cyl_cog[3], None, None)
  I5_inter = calc_I(m_cyl[4], rad_in, rad_out, a[4], r_cyl_cog[4], r_valve_cog[4], r_valve_cog[5])
  I6_inter = calc_I(m_cyl[5], rad_in, rad_out, a[5], r_cyl_cog[5], None, None)

  #print I1_inter, '\n'
  #print I2_inter, '\n'
  #print I3_inter, '\n'
  # print I4_inter, '\n'
  # print I5_inter, '\n'
  # print I6_inter, '\n'


  # These values are from the Inerta Matrix so the cross terms are negated
  Ixx1 = I1_inter[0, 0]
  Iyy1 = I1_inter[1, 1]
  Izz1 = I1_inter[2, 2]
  Ixy1 = I1_inter[0, 1]
  Iyz1 = I1_inter[1, 2]
  Ixz1 = I1_inter[0, 2]

  Ixx2 = I2_inter[0, 0]
  Iyy2 = I2_inter[1, 1]
  Izz2 = I2_inter[2, 2]
  Ixy2 = I2_inter[0, 1]
  Iyz2 = I2_inter[1, 2]
  Ixz2 = I2_inter[0, 2]

  Ixx3 = I3_inter[0, 0]
  Iyy3 = I3_inter[1, 1]
  Izz3 = I3_inter[2, 2]
  Ixy3 = I3_inter[0, 1]
  Iyz3 = I3_inter[1, 2]
  Ixz3 = I3_inter[0, 2]

  Ixx4 = I4_inter[0, 0]
  Iyy4 = I4_inter[1, 1]
  Izz4 = I4_inter[2, 2]
  Ixy4 = I4_inter[0, 1]
  Iyz4 = I4_inter[1, 2]
  Ixz4 = I4_inter[0, 2]

  Ixx5 = I5_inter[0, 0]
  Iyy5 = I5_inter[1, 1]
  Izz5 = I5_inter[2, 2]
  Ixy5 = I5_inter[0, 1]
  Iyz5 = I5_inter[1, 2]
  Ixz5 = I5_inter[0, 2]

  Ixx6 = I6_inter[0, 0]
  Iyy6 = I6_inter[1, 1]
  Izz6 = I6_inter[2, 2]
  Ixy6 = I6_inter[0, 1]
  Iyz6 = I6_inter[1, 2]
  Ixz6 = I6_inter[0, 2]


  I1 = [Ixx1, Iyy1, Izz1, -1*Ixy1, -1*Iyz1, -1*Ixz1]
  I2 = [Ixx2, Iyy2, Izz2, -1*Ixy2, -1*Iyz2, -1*Ixz2]
  I3 = [Ixx3, Iyy3, Izz3, -1*Ixy3, -1*Iyz3, -1*Ixz3]
  I4 = [Ixx4, Iyy4, Izz4, -1*Ixy4, -1*Iyz4, -1*Ixz4]
  I5 = [Ixx5, Iyy5, Izz5, -1*Ixy5, -1*Iyz5, -1*Ixz5]
  I6 = [Ixx6, Iyy6, Izz6, -1*Ixy6, -1*Iyz6, -1*Ixz6]


  #I1 = [ Ixx1, Iyy1, Izz1, Ixy1, Iyz1, Ixz1]
  #I2 = [ Ixx2, Iyy2, Izz2, Ixy2, Iyz2, Ixz2]
  #I3 = [ Ixx3, Iyy3, Izz3, Ixy3, Iyz3, Ixz3]
  #I4 = [ Ixx4, Iyy4, Izz4, Ixy4, Iyz4, Ixz4]
  #I5 = [ Ixx5, Iyy5, Izz5, Ixy5, Iyz5, Ixz5]
  #I6 = [ Ixx6, Iyy6, Izz6, Ixy6, Iyz6, Ixz6]


  L1 = I_to_matrix(I1)-m1*skew(r1)*skew(r1)
  L2 = I_to_matrix(I2)-m2*skew(r2)*skew(r2)
  L3 = I_to_matrix(I3)-m3*skew(r3)*skew(r3)
  L4 = I_to_matrix(I4)-m4*skew(r4)*skew(r4)
  L5 = I_to_matrix(I5)-m5*skew(r5)*skew(r5)
  L6 = I_to_matrix(I6)-m6*skew(r6)*skew(r6)

  params = [L1[0, 0], L1[0, 1], L1[0, 2], L1[1, 1], L1[1, 2], L1[2, 2], r1[0]*m1, r1[1]*m1, r1[2]*m1, m1, fv1,
          L2[0, 0], L2[0, 1], L2[0, 2], L2[1, 1], L2[1, 2], L2[2, 2], r2[0]*m2, r2[1]*m2, r2[2]*m2, m2, fv2,
          L3[0, 0], -L3[0, 1], -L3[0, 2], L3[1, 1], -L3[1, 2], L3[2, 2], r3[0]*m3, r3[1]*m3, r3[2]*m3, m3, fv3,
          L4[0, 0], -L4[0, 1], -L4[0, 2], L4[1, 1], -L4[1, 2], L4[2, 2], r4[0]*m4, r4[1]*m4, r4[2]*m4, m4, fv4,
         L5[0,0], -L5[0,1], -L5[0,2], L5[1,1], -L5[1,2], L5[2,2], r5[0]*m5, r5[1]*m5, r5[2]*m5, m5, fv5,
         L6[0,0], -L6[0,1], -L6[0,2], L6[1,1], -L6[1,2], L6[2,2], r6[0]*m6, r6[1]*m6, r6[2]*m6, m6, fv6]

  print params

  params = params[0:11*numJts]
  return params
