from math import sin, cos
import numpy as np



def tau(parms, q, dq, ddq):
#
    tau_out = [0]*1
#
    x0 = -0.06*ddq[0]
    x1 = -parms[6]
#
    tau_out[0] = ddq[0]*parms[3] - 0.06*ddq[0]*x1 + dq[0]*parms[10] - 0.06*parms[9]*x0 + x0*x1
#
    return tau_out




def regressor(q, dq, ddq):
#
    regressor_out = [0]*11
#
    x0 = -0.06*ddq[0]
#
    regressor_out[0] = 0
    regressor_out[1] = 0
    regressor_out[2] = 0
    regressor_out[3] = ddq[0]
    regressor_out[4] = 0
    regressor_out[5] = 0
    regressor_out[6] = 0.06*ddq[0] - x0
    regressor_out[7] = 0
    regressor_out[8] = 0
    regressor_out[9] = -0.06*x0
    regressor_out[10] = dq[0]
#
    return regressor_out




def M(parms):
#
    M_out = [0]*1
#

    M_out[0] = parms[3] + 0.12*parms[6] + 0.0036*parms[9]
#
    return M_out




def c():
#
    c_out = [0]*1
#

    c_out[0] = 0
#
    return c_out




def C():
#
    C_out = [0]*1
#

    C_out[0] = 0
#
    return C_out




def f(parms, q, dq):
#
    f_out = [0]*1
#

    f_out[0] = dq[0]*parms[10]
#
    return f_out




def g():
#
    g_out = [0]*1
#

    g_out[0] = 0
#
    return g_out












#dynparms = [L_1xx, L_1xy, L_1xz, L_1yy, L_1yz, L_1zz, l_1x, l_1y, l_1z, m_1, fv_1]
