import rospy
import numpy as np
from dynamics_utils import *
import math
from math import pi, pow
import sys
sys.path.append('./..')


# Calculate the link center of gavity given the center of gravity of the bodies and the valves

def calc_cog(m_cyl, m_valve_1, m_valve_2, r_cyl, r_valve_1, r_valve_2):
    r_cyl = np.array(r_cyl)
    r_valve_1 = np.array(r_valve_1)
    r_valve_2 = np.array(r_valve_2)

    inter1 = r_cyl*m_cyl
    inter2 = r_valve_1*m_valve_1
    inter3 = r_valve_2*m_valve_2
    inter4 = m_cyl + m_valve_1 + m_valve_2

    inter5 = inter1 + inter2 + inter3

    r_cog = np.zeros(3)

    r_cog[0] = inter5[0] / inter4
    r_cog[1] = inter5[1] / inter4
    r_cog[2] = inter5[2] / inter4

    return r_cog.tolist()

# Calculate the Inertia of the link in the CoG frame with the bodies and
# the valves combined. This assumes the body is a cylider and the valve
# blocks are rectangular prisms


def calcI(m,r,radIn, radOut,hLink, hValve):

  # m lumped mass
  # r center of gravity of valve block
  # rad1 inner radius of passive
  # rad2 outter radius of passive
  # h length of link
  m1 = 0.25*m
  m2 = 0.75*m
  # print m1
  # print m2
  # print radIn
  # print radOut
  # print hLink
  # print hValve
  Ixx1 = m1/2.0*(pow(radIn,2.) + pow(radOut,2.))
  # print radIn
  # print radOut
  Iyy1 = m1/12.0*(3.*(pow(radIn,2.) + pow(radOut,2.)) + pow(hLink,2.))
  Izz1 = m1/12.0*(3.*(pow(radIn,2.) + pow(radOut,2.)) + pow(hLink,2.))

  Ixx2 = m2*pow(radIn,2.)/2.0
  Iyy2 = m2/12.0*(3.*pow(radIn,2.) + hValve)
  Izz2 = m2/12.0*(3.*pow(radIn,2.) + hValve)

  Ixx1Link = Ixx1
  Iyy1Link = Iyy1 + m1*(pow(hLink/2.0,2.))
  Izz1Link = Izz1 + m1*(pow(hLink/2.0,2.))

  Ixx2Link = Ixx2
  Iyy2Link = Iyy2 + m2*(pow(r,2.))
  Izz2Link = Izz2 + m2*(pow(r,2.))

  Ixx = Ixx1Link + Ixx2Link
  Iyy = Iyy1Link + Iyy2Link
  Izz = Izz1Link + Izz2Link
 
  return Ixx, Iyy, Izz
    

def compute(jointNums):

  # Approximated center of gravity of each link. I measuerd 
  # to the middle of the valve manifold from the center of the
  # next joint.
  r = [0.05, 0.5270, 0.15, 0.15, 0.14, 0.015]

  # Masses of everything in kilograms if it is .100 even then it is not
  # available
  massJoint = [ 0.600, 0.600, 0.505, 0.4496, 0.4522, 0.7302]
  massLink = [0.000, 1.642, 1.2242, 0.9759, 0.9911,0.0] #w/o vive
  massLinkVive = [0.000, 1.7361, 1.2242, 1.070, 0.9911, 0.0941] #w vive
  massLinkViveandEE = [0.000, 1.7361, 1.2242, 1.070, 0.9911, 0.5788] #w vive & ee
  m = [0.0]*len(massJoint)  
  mVive = [0.0]*len(massJoint)  
  mViveEE = [0.0]*len(massJoint)  
  for i in xrange(len(massLink)-1):
    m[i] = massJoint[i]/2. + massLink[i] + massJoint[i+1]/2.
    mVive[i] = massJoint[i]/2. + massLinkVive[i] + massJoint[i+1]/2.
    mViveEE[i] = massJoint[i]/2. + massLinkViveandEE[i] + massJoint[i+1]/2.
  m[5] = massJoint[5]/2. + massLink[5]
  mVive[5] = massJoint[5]/2. + massLinkVive[5]
  mViveEE[5] = massJoint[5]/2. + massLinkViveandEE[5]

  fv1 = 0.7
  fv2 = 0.7
  fv3 = 0.7
  fv4 = 0.7
  fv5 = 0.7
  fv6 = 0.7

  Ixx = []
  Iyy = []
  Izz = []
 
  IxxVive = []
  IyyVive = []
  IzzVive = []

  IxxViveEE = []
  IyyViveEE = []
  IzzViveEE = []



  hLink = [0.100, 0.460, 0.370, 0.230, 0.300, 0.0300]

  for i in xrange(len(massJoint)):

    if i == 0 or i == 1:
      radIn = (0.200-0.08)/2.
      radOut = 0.200/2.0
    elif i == 2:
      radIn = (0.180-0.035*2.)/2.
      radOut = 0.180/2.0
    else:
      radIn = (0.160-0.035*2.0)/2.0
      radOut = 0.160/2.0

    hValve = 0.140
    IxxBuff, IyyBuff, IzzBuff = calcI(m[i], r[i], radIn, radOut, hLink[i], hValve)
    IxxBuffVive, IyyBuffVive, IzzBuffVive = calcI(mVive[i], r[i], radIn, radOut, hLink[i], hValve)
    IxxBuffViveEE, IyyBuffViveEE, IzzBuffViveEE = calcI(mViveEE[i], r[i], radIn, radOut, hLink[i], hValve)


    Ixx.append(IxxBuff)
    Iyy.append(IyyBuff)
    Izz.append(IzzBuff)

    IxxVive.append(IxxBuffVive)
    IyyVive.append(IyyBuffVive)
    IzzVive.append(IzzBuffVive)

    IxxViveEE.append(IxxBuffViveEE)
    IyyViveEE.append(IyyBuffViveEE)
    IzzViveEE.append(IzzBuffViveEE)
    

  I1 = [Ixx[0], Iyy[0], Izz[0], 0.0, 0.0, 0.0]
  I2 = [Ixx[1], Iyy[1], Izz[1], 0.0, 0.0, 0.0]
  I3 = [Ixx[2], Iyy[2], Izz[2], 0.0, 0.0, 0.0]
  I4 = [Ixx[3], Iyy[3], Izz[3], 0.0, 0.0, 0.0]
  I5 = [Ixx[4], Iyy[4], Izz[4], 0.0, 0.0, 0.0]
  I6 = [Ixx[5], Iyy[5], Izz[5], 0.0, 0.0, 0.0]

  I1Vive = [IxxVive[0], IyyVive[0], IzzVive[0], 0.0, 0.0, 0.0]
  I2Vive = [IxxVive[1], IyyVive[1], IzzVive[1], 0.0, 0.0, 0.0]
  I3Vive = [IxxVive[2], IyyVive[2], IzzVive[2], 0.0, 0.0, 0.0]
  I4Vive = [IxxVive[3], IyyVive[3], IzzVive[3], 0.0, 0.0, 0.0]
  I5Vive = [IxxVive[4], IyyVive[4], IzzVive[4], 0.0, 0.0, 0.0]
  I6Vive = [IxxVive[5], IyyVive[5], IzzVive[5], 0.0, 0.0, 0.0]

  I1ViveEE = [IxxViveEE[0], IyyViveEE[0], IzzViveEE[0], 0.0, 0.0, 0.0]
  I2ViveEE = [IxxViveEE[1], IyyViveEE[1], IzzViveEE[1], 0.0, 0.0, 0.0]
  I3ViveEE = [IxxViveEE[2], IyyViveEE[2], IzzViveEE[2], 0.0, 0.0, 0.0]
  I4ViveEE = [IxxViveEE[3], IyyViveEE[3], IzzViveEE[3], 0.0, 0.0, 0.0]
  I5ViveEE = [IxxViveEE[4], IyyViveEE[4], IzzViveEE[4], 0.0, 0.0, 0.0]
  I6ViveEE = [IxxViveEE[5], IyyViveEE[5], IzzViveEE[5], 0.0, 0.0, 0.0]



  #I1 = [ Ixx1, Iyy1, Izz1, Ixy1, Iyz1, Ixz1]
  #I2 = [ Ixx2, Iyy2, Izz2, Ixy2, Iyz2, Ixz2]
  #I3 = [ Ixx3, Iyy3, Izz3, Ixy3, Iyz3, Ixz3]
  #I4 = [ Ixx4, Iyy4, Izz4, Ixy4, Iyz4, Ixz4]
  #I5 = [ Ixx5, Iyy5, Izz5, Ixy5, Iyz5, Ixz5]
  #I6 = [ Ixx6, Iyy6, Izz6, Ixy6, Iyz6, Ixz6]
  r1  = [-0.5*hLink[0], 0, 0]
  r2  = [- 0.5*hLink[1], 0, 0]
  r3  = [-0.75*r[2] - 0.25*hLink[2], 0, 0]
  r4  = [-0.75*r[3] - 0.25*hLink[3], 0, 0]
  r5  = [-0.75*r[4] - 0.25*hLink[4], 0, 0]
  r6  = [-0.75*r[5] - 0.25*hLink[5], 0, 0]

  type = "ViveEE"

  if type == "No_Vive":
      L1 = I_to_matrix(I1)-m[0]*skew(r1)*skew(r1)
      L2 = I_to_matrix(I2)-m[1]*skew(r2)*skew(r2)
      L3 = I_to_matrix(I3)-m[2]*skew(r3)*skew(r3)
      L4 = I_to_matrix(I4)-m[3]*skew(r4)*skew(r4)
      L5 = I_to_matrix(I5)-m[4]*skew(r5)*skew(r5)
      L6 = I_to_matrix(I6)-m[5]*skew(r6)*skew(r6)
  elif type == "Vive":
      L1 = I_to_matrix(I1Vive)-mVive[0]*skew(r1)*skew(r1)
      L2 = I_to_matrix(I2Vive)-mVive[1]*skew(r2)*skew(r2)
      L3 = I_to_matrix(I3Vive)-mVive[2]*skew(r3)*skew(r3)
      L4 = I_to_matrix(I4Vive)-mVive[3]*skew(r4)*skew(r4)
      L5 = I_to_matrix(I5Vive)-mVive[4]*skew(r5)*skew(r5)
      L6 = I_to_matrix(I6Vive)-mVive[5]*skew(r6)*skew(r6)
  elif type == "ViveEE":
      L1 = I_to_matrix(I1ViveEE)-mViveEE[0]*skew(r1)*skew(r1)
      L2 = I_to_matrix(I2ViveEE)-mViveEE[1]*skew(r2)*skew(r2)
      L3 = I_to_matrix(I3ViveEE)-mViveEE[2]*skew(r3)*skew(r3)
      L4 = I_to_matrix(I4ViveEE)-mViveEE[3]*skew(r4)*skew(r4)
      L5 = I_to_matrix(I5ViveEE)-mViveEE[4]*skew(r5)*skew(r5)
      L6 = I_to_matrix(I6ViveEE)-mViveEE[5]*skew(r6)*skew(r6)
  
  


  params = []

  params.append([L1[0, 0], L1[0, 1], L1[0, 2], L1[1, 1], L1[1, 2], L1[2, 2], r1[0]*m[0], r1[1]*m[0], r1[2]*m[0], m[0], fv1])
  params.append([L2[0, 0], L2[0, 1], L2[0, 2], L2[1, 1], L2[1, 2], L2[2, 2], r2[0]*m[1], r2[1]*m[1], r2[2]*m[1], m[1], fv2])
  params.append([L3[0, 0], -L3[0, 1], -L3[0, 2], L3[1, 1], -L3[1, 2], L3[2, 2], r3[0]*m[2], r3[1]*m[2], r3[2]*m[2], m[2], fv3])
  params.append([L4[0, 0], -L4[0, 1], -L4[0, 2], L4[1, 1], -L4[1, 2], L4[2, 2], r4[0]*m[3], r4[1]*m[3], r4[2]*m[3], m[3], fv4])
  params.append([L5[0,0], -L5[0,1], -L5[0,2], L5[1,1], -L5[1,2], L5[2,2], r5[0]*m[4], r5[1]*m[4], r5[2]*m[4], m[4], fv5])
  params.append([L6[0,0], -L6[0,1], -L6[0,2], L6[1,1], -L6[1,2], L6[2,2], r6[0]*m[5], r6[1]*m[5], r6[2]*m[5], m[5], fv6])

  print params
  passParams = []
  for i in xrange(len(jointNums)):
      passParams = passParams + params[jointNums[i]-1]
  #params = params[(startJoint-1)*11:11*endJoint]
  return passParams
