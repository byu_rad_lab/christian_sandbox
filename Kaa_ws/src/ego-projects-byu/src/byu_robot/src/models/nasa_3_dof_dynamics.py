from math import sin, cos
import numpy as np



def tau(parms, q, dq, ddq):
#
    tau_out = [0]*3
#
    x0 = sin(q[1])
    x1 = -dq[1]
    x2 = -x1
    x3 = cos(q[1])
    x4 = dq[0]*x3
    x5 = ddq[0]*x0 + x2*x4
    x6 = -ddq[1]
    x7 = ddq[0]*x3
    x8 = dq[0]*x0
    x9 = x1*x8 + x7
    x10 = 0.527*ddq[1] - 9.81*x3
    x11 = sin(q[2])
    x12 = cos(q[2])
    x13 = x1*x11 + x12*x8
    x14 = dq[2] + x4
    x15 = x11*x8
    x16 = x12*x2
    x17 = x15 + x16
    x18 = 0.527*dq[1]
    x19 = x12**2
    x20 = x11**2
    x21 = 0.346*x19 + 0.346*x20
    x22 = x15*x21 + x16*x21 + x18
    x23 = -parms[29]
    x24 = 0.527*x0**2 + 0.527*x3**2
    x25 = -0.06*dq[0]
    x26 = x24*x4 - x25
    x27 = x11*x26
    x28 = parms[24]*x13 + parms[26]*x14 + parms[27]*x17 + parms[28]*x22 + x23*x27
    x29 = -parms[30]
    x30 = x11*x5
    x31 = -x12
    x32 = x31*x6
    x33 = -x13
    x34 = -0.346*dq[2]
    x35 = x10 + x21*x30 + x21*x32 + x33*x34
    x36 = -parms[28]
    x37 = -0.346*x19 - 0.346*x20
    x38 = x26*x31 + x37*x4
    x39 = x34 + x38
    x40 = parms[23]*x13 + parms[25]*x14 + parms[26]*x17 + parms[30]*x27 + x36*x39
    x41 = -x17
    x42 = parms[28]*x17 + parms[30]*x33 + parms[31]*x22
    x43 = -dq[2]
    x44 = x11*x6 + x12*x5 + x17*x43
    x45 = ddq[2] + x9
    x46 = dq[2]*x13 + x30 + x32
    x47 = -9.81*x0
    x48 = -0.06*ddq[0]
    x49 = -x48
    x50 = -x8
    x51 = x18*x50 + x24*x7 + x49
    x52 = -0.346*ddq[2] + dq[2]*x27 + x11*x47 + x31*x51 + x37*x9
    x53 = parms[29]*x13 + parms[31]*x39 + x14*x36
    x54 = parms[22]*x44 + parms[23]*x45 + parms[24]*x46 + parms[29]*x52 + x14*x28 + x22*x53 + x29*x35 - x39*x42 + x40*x41
    x55 = parms[17]*x2 + parms[18]*x8 + parms[20]*x18
    x56 = parms[13]*x8 + parms[15]*x1 + parms[16]*x4 + parms[17]*x26
    x57 = x11*x51 + x12*x47 + x34*x4 + x38*x43
    x58 = parms[30]*x14 + parms[31]*x27 + x17*x23
    x59 = -x58
    x60 = parms[22]*x13 + parms[23]*x14 + parms[24]*x17 + parms[29]*x39 + x22*x29
    x61 = parms[24]*x44 + parms[26]*x45 + parms[27]*x46 + parms[28]*x35 + x13*x40 - x14*x60 + x22*x59 + x23*x57 + x27*x42
    x62 = -parms[19]
    x63 = parms[17]*x4 + parms[19]*x50 + parms[20]*x26
    x64 = -x18
    x65 = parms[12]*x8 + parms[14]*x1 + parms[15]*x4 + parms[17]*x64
    x66 = -x4
    x67 = parms[28]*x46 + parms[31]*x35 + x17*x58 + x29*x44 + x33*x53
    x68 = x21*x67
    x69 = parms[23]*x44 + parms[25]*x45 + parms[26]*x46 + parms[30]*x57 + x17*x60 - x27*x53 + x28*x33 + x36*x52 + x39*x58
    x70 = parms[29]*x44 + parms[31]*x52 + x13*x42 + x14*x59 + x36*x45
    x71 = parms[18]*x66 + parms[19]*x1
    x72 = parms[11]*x8 + parms[12]*x1 + parms[13]*x4 + parms[18]*x18 + x26*x62
    x73 = parms[17]*x9 + parms[20]*x51 + x11*(parms[30]*x45 + parms[31]*x57 + x14*x53 + x23*x46 + x41*x42) + x31*x70 + x4*x71 + x5*x62 + x50*x55
    x74 = -parms[17]
#
    tau_out[0] = ddq[0]*parms[3] + 0.06*ddq[0]*parms[6] + 0.06*dq[0]**2*parms[8] + dq[0]*parms[10] + dq[0]*parms[8]*x25 + parms[6]*x49 - 0.06*parms[9]*x48 + x0*(parms[11]*x5 + parms[12]*x6 + parms[13]*x9 + parms[18]*x10 + x1*x56 + x11*x61 + x11*x68 + x12*x54 + x26*x55 + x51*x62 + x63*x64 + x65*x66) + x24*x3*x73 + x3*(parms[13]*x5 + parms[15]*x6 + parms[16]*x9 + parms[17]*x51 - parms[18]*x47 + x2*x72 - x26*x71 + x37*x70 + x65*x8 + x69) + 0.06*x73
    tau_out[1] = dq[1]*parms[21] - parms[12]*x5 - parms[14]*x6 - parms[15]*x9 + 0.527*parms[18]*x5 - parms[19]*x47 + 0.527*parms[20]*x10 - x10*x74 - x11*x54 - x18*x71 + 0.527*x2*x71 - x31*x61 - x31*x68 - x4*x72 - x50*x56 + 0.527*x6*x74 + 0.527*x63*x8 + 0.527*x67
    tau_out[2] = dq[2]*parms[32] + x69 - 0.346*x70
#
    return tau_out




def regressor(q, dq, ddq):
#
    regressor_out = [0]*99
#
    x0 = -0.06*ddq[0]
    x1 = -x0
    x2 = -0.06*dq[0]
    x3 = -dq[1]
    x4 = sin(q[1])
    x5 = dq[0]*x4
    x6 = x3*x5
    x7 = -x6
    x8 = cos(q[1])
    x9 = -x3
    x10 = dq[0]*x8
    x11 = x10*x9
    x12 = ddq[0]*x4 + x11
    x13 = x10*x5
    x14 = -x13
    x15 = -ddq[1]
    x16 = x5**2
    x17 = x3**2
    x18 = -x17
    x19 = ddq[0]*x8
    x20 = x19 + x6
    x21 = x20 + x6
    x22 = x10**2
    x23 = -x22
    x24 = x10*x3
    x25 = 0.527*dq[1]
    x26 = -x5
    x27 = x25*x26
    x28 = 0.527*x4**2 + 0.527*x8**2
    x29 = x1 + x19*x28 + x27
    x30 = x27 + x29
    x31 = x30*x8
    x32 = x10*x28 - x2
    x33 = 0.527*ddq[1] - 9.81*x8
    x34 = x32*x5 + x33
    x35 = -9.81*x4
    x36 = x10*x32
    x37 = -x16
    x38 = x23 + x37
    x39 = x28*x8
    x40 = -x12 + x24
    x41 = sin(q[2])
    x42 = cos(q[2])
    x43 = x3*x41 + x42*x5
    x44 = x41*x5
    x45 = x42*x9
    x46 = x44 + x45
    x47 = x43*x46
    x48 = -x43
    x49 = dq[2] + x10
    x50 = x48*x49
    x51 = -dq[2]
    x52 = x12*x42 + x15*x41 + x46*x51
    x53 = x46*x49
    x54 = x52 + x53
    x55 = -x47
    x56 = ddq[2] + x20
    x57 = x55 + x56
    x58 = x43**2
    x59 = x49**2
    x60 = -x59
    x61 = x58 + x60
    x62 = x46**2
    x63 = -x58
    x64 = x62 + x63
    x65 = x43*x49
    x66 = x12*x41
    x67 = -x42
    x68 = x15*x67
    x69 = dq[2]*x43 + x66 + x68
    x70 = x65 + x69
    x71 = -x53
    x72 = x52 + x71
    x73 = x50 + x69
    x74 = -x62
    x75 = x59 + x74
    x76 = x47 + x56
    x77 = x41*x76
    x78 = x32*x41
    x79 = x46*x78
    x80 = x42**2
    x81 = x41**2
    x82 = -0.346*x80 - 0.346*x81
    x83 = x10*x82 + x32*x67
    x84 = -0.346*dq[2]
    x85 = x83 + x84
    x86 = 0.346*x80 + 0.346*x81
    x87 = x33 + x48*x84 + x66*x86 + x68*x86
    x88 = x48*x85 + x79 + x87
    x89 = x41*x88
    x90 = x41*x70
    x91 = -0.346*ddq[2] + dq[2]*x78 + x20*x82 + x29*x67 + x35*x41
    x92 = x49*x78
    x93 = x25 + x44*x86 + x45*x86
    x94 = x48*x93 - x91 + x92
    x95 = x47 - x56
    x96 = x41*(x60 + x74) + x67*x95
    x97 = x43*x93 + x91 - x92
    x98 = x10*x84 + x29*x41 + x35*x42 + x51*x83
    x99 = x46*x93
    x100 = x49*x85
    x101 = -x100 - x98 + x99
    x102 = x63 + x74
    x103 = x41*x86
    x104 = x41*(x65 - x69) + x54*x67
    x105 = x43*x85 - x79 - x87
    x106 = -x52 + x53
    x107 = x100 + x98 - x99
    x108 = x60 + x63
    x109 = x108*x67 + x77
    x110 = x107*x41 + x67*x97
    x111 = x12 + x24
    x112 = -x15
    x113 = x67*x88
    x114 = x67*x86
#
    regressor_out[0] = 0
    regressor_out[1] = 0
    regressor_out[2] = 0
    regressor_out[3] = ddq[0]
    regressor_out[4] = 0
    regressor_out[5] = 0
    regressor_out[6] = 0.06*ddq[0] + x1
    regressor_out[7] = 0
    regressor_out[8] = 0.06*dq[0]**2 + dq[0]*x2
    regressor_out[9] = -0.06*x0
    regressor_out[10] = dq[0]
    regressor_out[11] = x12*x4 + x7*x8
    regressor_out[12] = x4*(x14 + x15) + x8*(x16 + x18)
    regressor_out[13] = x21*x4 + x8*(x11 + x12)
    regressor_out[14] = x11*x4 + x6*x8
    regressor_out[15] = x4*(x17 + x23) + x8*(x13 + x15)
    regressor_out[16] = x20*x8 + x24*x4
    regressor_out[17] = x21*x28*x8 + 0.06*x21 + x31
    regressor_out[18] = x34*x4 + x38*x39 + 0.06*x38 + x8*(x25*x9 - x35 + x36)
    regressor_out[19] = x39*x40 + x4*(x25*x5 - x29) + 0.06*x40
    regressor_out[20] = x28*x31 + 0.06*x30
    regressor_out[21] = 0
    regressor_out[22] = x4*(x41*x50 + x42*x52) + x47*x8
    regressor_out[23] = x4*(x41*x61 + x42*x57) + x54*x8
    regressor_out[24] = x4*(x41*x72 + x42*x70) + x64*x8
    regressor_out[25] = x4*(x41*x65 + x42*x71) + x56*x8
    regressor_out[26] = x4*(x42*x75 + x77) + x73*x8
    regressor_out[27] = x4*(x41*x69 + x42*x53) + x55*x8
    regressor_out[28] = x39*x96 + x4*(x86*x90 + x89) + x8*(x82*x95 + x94) + 0.06*x96
    regressor_out[29] = x104*x39 + 0.06*x104 + x4*(x101*x41 + x102*x103 + x42*x97) + x54*x8*x82
    regressor_out[30] = x109*x39 + 0.06*x109 + x4*(x103*x106 + x105*x42) + x8*(x107 + x108*x82)
    regressor_out[31] = x110*x39 + 0.06*x110 + x4*x86*x89 + x8*x82*x97
    regressor_out[32] = 0
    regressor_out[33] = 0
    regressor_out[34] = 0
    regressor_out[35] = 0
    regressor_out[36] = 0
    regressor_out[37] = 0
    regressor_out[38] = 0
    regressor_out[39] = 0
    regressor_out[40] = 0
    regressor_out[41] = 0
    regressor_out[42] = 0
    regressor_out[43] = 0
    regressor_out[44] = x14
    regressor_out[45] = -x111
    regressor_out[46] = -x22 - x37
    regressor_out[47] = x112
    regressor_out[48] = -x20 - x7
    regressor_out[49] = -x14
    regressor_out[50] = 0.527*x112 + 0.527*x13 - x26*x32 + x33
    regressor_out[51] = 0.527*x111
    regressor_out[52] = 0.527*x18 - x25*x3 - x35 + x36 + 0.527*x37
    regressor_out[53] = 0.527*x34
    regressor_out[54] = dq[1]
    regressor_out[55] = -x41*x52 - x50*x67
    regressor_out[56] = -x41*x57 - x61*x67
    regressor_out[57] = -x67*x72 - x90
    regressor_out[58] = -x41*x71 - x65*x67
    regressor_out[59] = -x41*x75 - x67*x76
    regressor_out[60] = -x41*x53 - x67*x69
    regressor_out[61] = -x113 - x114*x70 + 0.527*x70
    regressor_out[62] = -x101*x67 - x102*x114 + 0.527*x102 - x41*x97
    regressor_out[63] = -x105*x41 - x106*x114 + 0.527*x106
    regressor_out[64] = -x113*x86 + 0.527*x88
    regressor_out[65] = 0
    regressor_out[66] = 0
    regressor_out[67] = 0
    regressor_out[68] = 0
    regressor_out[69] = 0
    regressor_out[70] = 0
    regressor_out[71] = 0
    regressor_out[72] = 0
    regressor_out[73] = 0
    regressor_out[74] = 0
    regressor_out[75] = 0
    regressor_out[76] = 0
    regressor_out[77] = 0
    regressor_out[78] = 0
    regressor_out[79] = 0
    regressor_out[80] = 0
    regressor_out[81] = 0
    regressor_out[82] = 0
    regressor_out[83] = 0
    regressor_out[84] = 0
    regressor_out[85] = 0
    regressor_out[86] = 0
    regressor_out[87] = 0
    regressor_out[88] = x47
    regressor_out[89] = x54
    regressor_out[90] = x64
    regressor_out[91] = x56
    regressor_out[92] = x73
    regressor_out[93] = x55
    regressor_out[94] = x94 - 0.346*x95
    regressor_out[95] = -0.346*x54
    regressor_out[96] = x107 - 0.346*x108
    regressor_out[97] = -0.346*x97
    regressor_out[98] = dq[2]
#
    return regressor_out




def M(parms, q):
#
    M_out = [0]*9
#
    x0 = sin(q[1])
    x1 = cos(q[1])
    x2 = cos(q[2])
    x3 = x0*x2
    x4 = sin(q[2])
    x5 = x0*x4
    x6 = x2**2
    x7 = x4**2
    x8 = -0.346*x6 - 0.346*x7
    x9 = x1*(0.527*x0**2 + 0.527*x1**2)
    x10 = x9 + 0.06
    x11 = -x10
    x12 = x1*x8 + x11*x2
    x13 = 0.346*x6 + 0.346*x7
    x14 = x13*x5
    x15 = -parms[30]
    x16 = parms[22]*x3 + parms[23]*x1 + parms[24]*x5 + parms[29]*x12 + x14*x15
    x17 = x10*x4
    x18 = -parms[29]
    x19 = parms[24]*x3 + parms[26]*x1 + parms[27]*x5 + parms[28]*x14 + x17*x18
    x20 = parms[28]*x5 + parms[31]*x14 + x15*x3
    x21 = x13*x20
    x22 = -parms[28]
    x23 = parms[23]*x3 + parms[25]*x1 + parms[26]*x5 + parms[30]*x17 + x12*x22
    x24 = parms[29]*x3 + parms[31]*x12 + x1*x22
    x25 = -x2
    x26 = parms[17]*x1 - parms[19]*x0 + parms[20]*x10 + x24*x25 + x4*(parms[30]*x1 + parms[31]*x17 + x18*x5)
    x27 = -parms[12]*x0 - parms[15]*x1 + 0.527*parms[18]*x0 - x16*x4 - x19*x25 + 0.527*x20 - x21*x25
    x28 = x23 - 0.346*x24
    x29 = -x4
    x30 = x13*x2 + 0.527
    x31 = parms[28]*x2 + parms[31]*x30 + x15*x29
    x32 = parms[23]*x29 + parms[26]*x2 - 0.346*parms[29]*x29
#
    M_out[0] = parms[3] + 0.12*parms[6] + 0.0036*parms[9] + x0*(parms[11]*x0 + parms[13]*x1 + parms[19]*x11 + x16*x2 + x19*x4 + x21*x4) + x1*(parms[13]*x0 + parms[16]*x1 + parms[17]*x10 + x23 + x24*x8) + x26*x9 + 0.06*x26
    M_out[1] = x27
    M_out[2] = x28
    M_out[3] = x27
    M_out[4] = parms[14] + 1.054*parms[17] + 0.277729*parms[20] - x13*x25*x31 - x25*(parms[24]*x29 + parms[27]*x2 + parms[28]*x30) + 0.527*x31 - x4*(parms[22]*x29 + parms[24]*x2 + x15*x30)
    M_out[5] = x32
    M_out[6] = x28
    M_out[7] = x32
    M_out[8] = parms[25] + 0.346*parms[28] + 0.119716*parms[31] - 0.346*x22
#
    return M_out




def c(parms, q, dq):
#
    c_out = [0]*3
#
    x0 = sin(q[1])
    x1 = -dq[1]
    x2 = -x1
    x3 = cos(q[1])
    x4 = dq[0]*x3
    x5 = x2*x4
    x6 = dq[0]*x0
    x7 = x1*x6
    x8 = 0.527*x0**2 + 0.527*x3**2
    x9 = -0.06*dq[0]
    x10 = x4*x8 - x9
    x11 = 0.527*dq[1]
    x12 = parms[17]*x2 + parms[18]*x6 + parms[20]*x11
    x13 = cos(q[2])
    x14 = sin(q[2])
    x15 = x14*x6
    x16 = x13*x2
    x17 = x15 + x16
    x18 = -dq[2]
    x19 = x13*x5 + x17*x18
    x20 = x14*x5
    x21 = x1*x14 + x13*x6
    x22 = dq[2]*x21 + x20
    x23 = x13**2
    x24 = x14**2
    x25 = 0.346*x23 + 0.346*x24
    x26 = -0.346*dq[2]
    x27 = -x21
    x28 = x20*x25 + x26*x27
    x29 = dq[2] + x4
    x30 = x10*x14
    x31 = -0.346*x23 - 0.346*x24
    x32 = -x13
    x33 = x10*x32 + x31*x4
    x34 = x26 + x33
    x35 = -parms[28]
    x36 = parms[23]*x21 + parms[25]*x29 + parms[26]*x17 + parms[30]*x30 + x34*x35
    x37 = x11 + x15*x25 + x16*x25
    x38 = parms[28]*x17 + parms[30]*x27 + parms[31]*x37
    x39 = -parms[29]
    x40 = -x6
    x41 = x11*x40
    x42 = x14*x41 + x18*x33 + x26*x4
    x43 = parms[30]*x29 + parms[31]*x30 + x17*x39
    x44 = -x43
    x45 = -parms[30]
    x46 = parms[22]*x21 + parms[23]*x29 + parms[24]*x17 + parms[29]*x34 + x37*x45
    x47 = parms[24]*x19 + parms[26]*x7 + parms[27]*x22 + parms[28]*x28 + x21*x36 - x29*x46 + x30*x38 + x37*x44 + x39*x42
    x48 = parms[13]*x6 + parms[15]*x1 + parms[16]*x4 + parms[17]*x10
    x49 = dq[2]*x30 + x31*x7 + x32*x41
    x50 = parms[29]*x21 + parms[31]*x34 + x29*x35
    x51 = parms[24]*x21 + parms[26]*x29 + parms[27]*x17 + parms[28]*x37 + x30*x39
    x52 = -x17
    x53 = parms[22]*x19 + parms[23]*x7 + parms[24]*x22 + parms[29]*x49 + x28*x45 + x29*x51 - x34*x38 + x36*x52 + x37*x50
    x54 = -parms[19]
    x55 = parms[17]*x4 + parms[19]*x40 + parms[20]*x10
    x56 = -x11
    x57 = parms[12]*x6 + parms[14]*x1 + parms[15]*x4 + parms[17]*x56
    x58 = -x4
    x59 = parms[28]*x22 + parms[31]*x28 + x17*x43 + x19*x45 + x27*x50
    x60 = x25*x59
    x61 = parms[23]*x19 + parms[25]*x7 + parms[26]*x22 + parms[30]*x42 + x17*x46 + x27*x51 - x30*x50 + x34*x43 + x35*x49
    x62 = parms[29]*x19 + parms[31]*x49 + x21*x38 + x29*x44 + x35*x7
    x63 = parms[18]*x58 + parms[19]*x1
    x64 = parms[11]*x6 + parms[12]*x1 + parms[13]*x4 + parms[18]*x11 + x10*x54
    x65 = parms[17]*x7 + parms[20]*x41 + x12*x40 + x14*(parms[30]*x7 + parms[31]*x42 + x22*x39 + x29*x50 + x38*x52) + x32*x62 + x4*x63 + x5*x54
#
    c_out[0] = 0.06*dq[0]**2*parms[8] + dq[0]*parms[8]*x9 + x0*(parms[11]*x5 + parms[13]*x7 + x1*x48 + x10*x12 + x13*x53 + x14*x47 + x14*x60 + x41*x54 + x55*x56 + x57*x58) + x3*x65*x8 + x3*(parms[13]*x5 + parms[16]*x7 + parms[17]*x41 - x10*x63 + x2*x64 + x31*x62 + x57*x6 + x61) + 0.06*x65
    c_out[1] = -parms[12]*x5 - parms[15]*x7 + 0.527*parms[18]*x5 - x11*x63 - x14*x53 + 0.527*x2*x63 - x32*x47 - x32*x60 - x4*x64 - x40*x48 + 0.527*x55*x6 + 0.527*x59
    c_out[2] = x61 - 0.346*x62
#
    return c_out




def C(parms, q, dq):
#
    C_out = [0]*9
#
    x0 = -0.06*parms[8]
    x1 = cos(q[1])
    x2 = cos(q[2])
    x3 = x2**2
    x4 = sin(q[2])
    x5 = x4**2
    x6 = -0.346*x3 - 0.346*x5
    x7 = sin(q[1])
    x8 = x2*x7
    x9 = x4*x7
    x10 = 0.346*x3 + 0.346*x5
    x11 = x10*x9
    x12 = -parms[30]
    x13 = parms[28]*x9 + parms[31]*x11 + x12*x8
    x14 = x13*x8
    x15 = x1**2
    x16 = x7**2
    x17 = x1*(0.527*x15 + 0.527*x16)
    x18 = x17 + 0.06
    x19 = x18*x4
    x20 = parms[31]*x19
    x21 = parms[30]*x1 + x20
    x22 = -parms[29]
    x23 = x22*x9
    x24 = x21 + x23
    x25 = -x1
    x26 = x14 + x24*x25
    x27 = parms[12]*x7 + parms[15]*x1
    x28 = parms[18]*x18
    x29 = x1*x28 + x27*x7
    x30 = -x18
    x31 = x1*x6 + x2*x30
    x32 = parms[23]*x1 + parms[29]*x31
    x33 = parms[22]*x8 + parms[24]*x9 + x11*x12
    x34 = x32 + x33
    x35 = x19*x22
    x36 = parms[26]*x1 + x35
    x37 = parms[24]*x8
    x38 = parms[27]*x9 + parms[28]*x11 + x37
    x39 = x36 + x38
    x40 = -x8
    x41 = -x19
    x42 = -parms[28]
    x43 = parms[31]*x31 + x1*x42
    x44 = parms[29]*x8
    x45 = x43 + x44
    x46 = x24*x31 + x34*x9 + x39*x40 + x41*x45
    x47 = x25*x27 + x28*x7
    x48 = parms[30]*x19
    x49 = parms[25]*x1 + x31*x42 + x48
    x50 = parms[23]*x8 + parms[26]*x9
    x51 = x49 + x50
    x52 = -x9
    x53 = -x31
    x54 = x1*x39 + x11*x45 + x13*x53 + x51*x52
    x55 = x13*x19
    x56 = -x11
    x57 = x24*x56 + x25*x34 + x51*x8 + x55
    x58 = x24*x9 + x40*x45
    x59 = x10*x58
    x60 = x13*x52
    x61 = -x2
    x62 = -parms[18]
    x63 = x15*x62 + x16*x62
    x64 = x26*x61 + x4*(x1*x45 + x60) + x63
    x65 = -parms[8]
    x66 = x0 + x1*(x26*x6 + x29 + x46) + x17*x64 + 0.06*x64 - 0.06*x65 + x7*(x2*x54 + x4*x57 + x4*x59 + x47)
    x67 = -x7
    x68 = x1*x2
    x69 = x1*x4
    x70 = -0.527*x7
    x71 = x6*x67 + x61*x70
    x72 = -x4
    x73 = x72 + x8
    x74 = x2 + x9
    x75 = x10*x2 + 0.527
    x76 = x11 + x75
    x77 = parms[24]*x73 + parms[27]*x74 + parms[28]*x76 + x36
    x78 = parms[29]*x73 + x43
    x79 = x10*x69
    x80 = parms[28]*x74 + parms[31]*x76 + x12*x73
    x81 = parms[23]*x73 + parms[26]*x74 + x49
    x82 = -x74
    x83 = parms[22]*x68 + parms[23]*x67 + parms[24]*x69 + parms[29]*x71 + x1*x77 + x12*x79 + x53*x80 + x76*x78 + x81*x82
    x84 = x4*x70
    x85 = parms[22]*x73 + parms[24]*x74 + x12*x76 + x32
    x86 = x21 + x22*x74
    x87 = parms[24]*x68 + parms[26]*x67 + parms[27]*x69 + parms[28]*x79 + x19*x80 + x22*x84 + x25*x85 + x73*x81 - x76*x86
    x88 = parms[17] + parms[18]*x7 + 0.527*parms[20]
    x89 = parms[13]*x7
    x90 = -parms[19]
    x91 = -parms[14] - 0.527*parms[17] + x27
    x92 = parms[17]*x1
    x93 = parms[20]*x18
    x94 = -x73
    x95 = parms[28]*x69 + parms[31]*x79 + x12*x68 + x74*x86 + x78*x94
    x96 = x10*x95
    x97 = parms[23]*x68 + parms[25]*x67 + parms[26]*x69 + parms[30]*x84 + x31*x86 + x41*x78 + x42*x71 + x74*x85 + x77*x94
    x98 = -parms[12] + 0.527*parms[18]
    x99 = parms[29]*x68 + parms[31]*x71 + x25*x86 + x42*x67 + x73*x80
    x100 = parms[18]*x25 + x90
    x101 = parms[13]*x1
    x102 = parms[11]*x7 + parms[19]*x30
    x103 = parms[17]*x67 + parms[19]*x25 + parms[20]*x70 + x1*x100 + x4*(parms[30]*x67 + parms[31]*x84 + x1*x78 + x22*x69 + x80*x82) + x61*x99 + x67*x88
    x104 = -x66
    x105 = parms[26]*x2
    x106 = parms[23]*x72 + x105
    x107 = parms[29]*x75
    x108 = x106*x61 + x107*x72
    x109 = x106*x72 + x107*x2
    x110 = x22*x3 + x22*x72**2
    x111 = x10*x110
    x112 = parms[22]*x72
    x113 = parms[24]*x2 + x112 + x12*x75
    x114 = parms[24]*x72
    x115 = parms[27]*x2 + parms[28]*x75
    x116 = x114 + x115
    x117 = -x72
    x118 = x113*x2 + x116*x117
    x119 = parms[28]*x2
    x120 = parms[31]*x75
    x121 = x119 + x12*x72 + x120
    x122 = x121*x72
    x123 = x121*x61
    x124 = x122*x61 + x123*x4
    x125 = x1*(x118 + x122*x6 + x98) + x124*x17 + 0.06*x124 + x7*(parms[15] + x108*x2 + x109*x4 + x111*x4)
    x126 = -x125
    x127 = x31 - 0.346
    x128 = x1 + 1
    x129 = parms[31]*x127 + x128*x42 + x44
    x130 = parms[26]*x128 + x35 + x38
    x131 = 0.346*x8
    x132 = parms[25]*x128 + x127*x42 + x48 + x50
    x133 = parms[22]*x52 + parms[29]*x19 + x11*x129 + x12*x131 - x127*x13 + x128*x130 + x132*x52 + x37
    x134 = -0.346*x1 + x53
    x135 = parms[30]*x128 + x20 + x23
    x136 = parms[23]*x128 + parms[29]*x127 + x33
    x137 = -x128
    x138 = parms[24]*x52 + parms[27]*x8 + parms[28]*x131 + x132*x8 + x134*x22 + x135*x56 + x136*x137 + x55
    x139 = parms[28]*x8 + parms[31]*x131 + x12*x52 + x129*x40 + x135*x9
    x140 = x10*x139
    x141 = parms[23]*x52 + parms[26]*x8 + parms[30]*x134 + x127*x135 + x129*x41 + x130*x40 + x136*x9 + x19*x42
    x142 = parms[29]*x52 + x135*x137 + x14 + x20
    x143 = x142*x61 + x4*(parms[31]*x134 + x128*x129 - x44 + x60) + x63
    x144 = 0.346*parms[29]
    x145 = -parms[23] + x144
    x146 = -0.346*parms[30]
    x147 = -0.346*parms[31] + x42
    x148 = x12*x61
    x149 = x147*x4 + x148
    x150 = x1*(x12*x6 + x146) + x149*x17 + 0.06*x149 + x7*(x105 + x145*x4)
    x151 = -x150
    x152 = parms[29]*x72 + x147
    x153 = 0.346*x72
    x154 = parms[25] + 0.346*parms[28] + x106
    x155 = parms[22]*x61 + parms[26] + 2*x114 + x115 + 0.346*x119 + x12*x153 + 0.346*x120 + x146*x72 + x152*x75 + x154*x61
    x156 = parms[30] + x2*x22
    x157 = 2*parms[24]*x61 + parms[27]*x72 + parms[28]*x153 + parms[30]*x75 - x112 + x145 + x154*x72 - x156*x75
    x158 = parms[28]*x72 + parms[31]*x153 + x117*x152 + x148 + x156*x2
    x159 = x10*x158
    x160 = parms[23]*x61 + parms[26]*x72 + x117*(parms[26] + x116) + x144*x2 + x146 + x2*(parms[23] - 0.346*parms[29] + x113)
    x161 = parms[29]*x2 + parms[29]*x61 + x12 + x122
    x162 = x161*x61 + x4*(x123 + x147)
    x163 = x101 + x102
    x164 = parms[16]*x1 + parms[17]*x18 + x89
    x165 = x1*x163 + x164*x67
    x166 = x7*(parms[19]*x67 + x92 + x93)
    x167 = -x165 + 0.527*x166 - x4*x54 - x57*x61 + 0.527*x58 - x59*x61
    x168 = -0.527*parms[19]
    x169 = -x167
    x170 = -x108*x4 - x109*x61 + 0.527*x110 - x111*x61 - x168 + 0.527*x90
    x171 = -x170
    x172 = -parms[26]*x4 - x145*x61
    x173 = -x172
    x174 = -0.346*x26 + x46
    x175 = x118 - 0.346*x122
    x176 = -x175
    x177 = -x174
    x178 = -0.346*x12 + x146
    x179 = -x178
#
    C_out[0] = dq[0]*x66
    C_out[1] = dq[0]*(x0 + x1*(parms[16]*x67 + parms[17]*x70 + x100*x30 + 2*x101 + x102 + x6*x99 + x7*x91 + x97 + x98) + x103*x17 + 0.06*x103 + x104 + x126 - 0.06*x65 + x7*(parms[11]*x1 + parms[13]*x67 + parms[15] + parms[16]*x25 + parms[17]*x30 + 0.527*parms[19]*x7 + x18*x88 + x2*x83 + x25*x91 + x4*x87 + x4*x96 + x70*x90 - x89 - 0.527*x92 - 0.527*x93)) + dq[1]*x125
    C_out[2] = dq[0]*(x0 + x1*(x141 + x142*x6 + x29) + x104 + x143*x17 + 0.06*x143 + x151 - 0.06*x65 + x7*(x133*x2 + x138*x4 + x140*x4 + x47)) + dq[1]*(x1*(x160 + x161*x6 + x98) + x126 + x151 + x162*x17 + 0.06*x162 + x7*(parms[15] + x155*x2 + x157*x4 + x159*x4)) + dq[2]*x150
    C_out[3] = dq[0]*x167
    C_out[4] = dq[0]*(-parms[12]*x1 - parms[15]*x67 + 0.527*parms[18]*x1 - x1*(x163 + x98) + 0.527*x166 - x168 + x169 + x171 - x4*x83 - x61*x87 - x61*x96 - x67*(-parms[15] + x164) + 0.527*x90 + 0.527*x95) + dq[1]*x170
    C_out[5] = dq[0]*(-x133*x4 - x138*x61 + 0.527*x139 - x140*x61 - x165 + 0.527*x166 + x169 + x173) + dq[1]*(-x155*x4 - x157*x61 + 0.527*x158 - x159*x61 - x168 + x171 + x173 + 0.527*x90) + dq[2]*x172
    C_out[6] = dq[0]*x174
    C_out[7] = dq[0]*(x176 + x177 + x97 - 0.346*x99) + dq[1]*x175
    C_out[8] = dq[0]*(x141 - 0.346*x142 + x177 + x179) + dq[1]*(x160 - 0.346*x161 + x176 + x179) + dq[2]*x178
#
    return C_out




def f(parms, q, dq):
#
    f_out = [0]*3
#

    f_out[0] = dq[0]*parms[10]
    f_out[1] = dq[1]*parms[21]
    f_out[2] = dq[2]*parms[32]
#
    return f_out




def g(parms, q):
#
    g_out = [0]*3
#
    x0 = sin(q[1])
    x1 = cos(q[1])
    x2 = -9.81*x1
    x3 = -9.81*x0
    x4 = sin(q[2])
    x5 = x3*x4
    x6 = -x2
    x7 = parms[29]*x5 + parms[30]*x6
    x8 = cos(q[2])
    x9 = x3*x8
    x10 = parms[28]*x2 - parms[29]*x9
    x11 = parms[31]*x2
    x12 = x8**2
    x13 = x4**2
    x14 = x11*(0.346*x12 + 0.346*x13)
    x15 = -parms[28]*x5 + parms[30]*x9
    x16 = parms[31]*x5
    x17 = -x8
    x18 = parms[31]*x4*x9 + x16*x17
#
    g_out[0] = x0*(parms[18]*x2 + x10*x4 + x14*x4 + x7*x8) + x1*x18*(0.527*x0**2 + 0.527*x1**2) + x1*(-parms[18]*x3 + x15 + x16*(-0.346*x12 - 0.346*x13)) + 0.06*x18
    g_out[1] = -parms[17]*x6 - parms[19]*x3 + 0.527*parms[20]*x2 - x10*x17 + 0.527*x11 - x14*x17 - x4*x7
    g_out[2] = x15 - 0.346*x16
#
    return g_out












#dynparms = [L_1xx, L_1xy, L_1xz, L_1yy, L_1yz, L_1zz, l_1x, l_1y, l_1z, m_1, fv_1, L_2xx, L_2xy, L_2xz, L_2yy, L_2yz, L_2zz, l_2x, l_2y, l_2z, m_2, fv_2, L_3xx, L_3xy, L_3xz, L_3yy, L_3yz, L_3zz, l_3x, l_3y, l_3z, m_3, fv_3]
