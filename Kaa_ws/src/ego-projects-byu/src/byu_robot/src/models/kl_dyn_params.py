from dynamics_utils import *
from math import pi, pow

fv1 = 0.0
fv2 = 0.0
fv3 = 0.0
fv4 = 0.0

grub_mass = 0.85
m1 = grub_mass
m2 = grub_mass
m3 = grub_mass
m4 = grub_mass


# center of mass (wrt each DH frame)
r1 = [0.09, 0.0,  0.0]
r2 = [0.19, 0.0,  0.0]
r3 = [0.06, 0.0,  0.0] 
r4 = [0.12, 0.0,  0.0]


#               Ixx    Iyy    Izz   Ixy Iyz Ixz
grub_inertia = [0.006, 0.013, 0.013, 0, 0, 0]
I1 = grub_inertia
I2 = grub_inertia
I3 = grub_inertia
I4 = grub_inertia


L1 = I_to_matrix(I1)-m1*skew(r1)*skew(r1)
L2 = I_to_matrix(I2)-m2*skew(r2)*skew(r2)
L3 = I_to_matrix(I3)-m3*skew(r3)*skew(r3)
L4 = I_to_matrix(I4)-m4*skew(r4)*skew(r4)

params = [L1[0,0], -L1[0,1], -L1[0,2], L1[1,1], -L1[1,2], L1[2,2], r1[0]*m1, r1[1]*m1, r1[2]*m1, m1, fv1,
          L2[0,0], -L2[0,1], -L2[0,2], L2[1,1], -L2[1,2], L2[2,2], r2[0]*m2, r2[1]*m2, r2[2]*m2, m2, fv2,
          L3[0,0], -L3[0,1], -L3[0,2], L3[1,1], -L3[1,2], L3[2,2], r3[0]*m3, r3[1]*m3, r3[2]*m3, m3, fv3,
          L4[0,0], -L4[0,1], -L4[0,2], L4[1,1], -L4[1,2], L4[2,2], r4[0]*m4, r4[1]*m4, r4[2]*m4, m4, fv4]
