from math import sin, cos
import numpy as np



def tau(parms, q, dq, ddq):
#
    tau_out = [0]*2
#
    x0 = sin(q[1])
    x1 = -dq[1]
    x2 = -x1
    x3 = cos(q[1])
    x4 = dq[0]*x3
    x5 = ddq[0]*x0 + x2*x4
    x6 = -ddq[1]
    x7 = ddq[0]*x3
    x8 = dq[0]*x0
    x9 = x1*x8 + x7
    x10 = 0.527*ddq[1] - 9.81*x3
    x11 = 0.527*x0**2 + 0.527*x3**2
    x12 = -0.06*dq[0]
    x13 = x11*x4 - x12
    x14 = 0.527*dq[1]
    x15 = parms[17]*x2 + parms[18]*x8 + parms[20]*x14
    x16 = parms[13]*x8 + parms[15]*x1 + parms[16]*x4 + parms[17]*x13
    x17 = -0.06*ddq[0]
    x18 = -x17
    x19 = -x8
    x20 = x11*x7 + x14*x19 + x18
    x21 = -parms[19]
    x22 = parms[17]*x4 + parms[19]*x19 + parms[20]*x13
    x23 = -x14
    x24 = parms[12]*x8 + parms[14]*x1 + parms[15]*x4 + parms[17]*x23
    x25 = -x4
    x26 = -9.81*x0
    x27 = parms[18]*x25 + parms[19]*x1
    x28 = parms[11]*x8 + parms[12]*x1 + parms[13]*x4 + parms[18]*x14 + x13*x21
    x29 = parms[17]*x9 + parms[20]*x20 + x15*x19 + x21*x5 + x27*x4
    x30 = -parms[17]
#
    tau_out[0] = ddq[0]*parms[3] + 0.06*ddq[0]*parms[6] + 0.06*dq[0]**2*parms[8] + dq[0]*parms[10] + dq[0]*parms[8]*x12 + parms[6]*x18 - 0.06*parms[9]*x17 + x0*(parms[11]*x5 + parms[12]*x6 + parms[13]*x9 + parms[18]*x10 + x1*x16 + x13*x15 + x20*x21 + x22*x23 + x24*x25) + x11*x29*x3 + 0.06*x29 + x3*(parms[13]*x5 + parms[15]*x6 + parms[16]*x9 + parms[17]*x20 - parms[18]*x26 - x13*x27 + x2*x28 + x24*x8)
    tau_out[1] = dq[1]*parms[21] - parms[12]*x5 - parms[14]*x6 - parms[15]*x9 + 0.527*parms[18]*x5 - parms[19]*x26 + 0.527*parms[20]*x10 - x10*x30 - x14*x27 - x16*x19 + 0.527*x2*x27 + 0.527*x22*x8 - x28*x4 + 0.527*x30*x6
#
    return tau_out




def regressor(q, dq, ddq):
#
    regressor_out = [0]*44
#
    x0 = -0.06*ddq[0]
    x1 = -x0
    x2 = -0.06*dq[0]
    x3 = sin(q[1])
    x4 = -dq[1]
    x5 = cos(q[1])
    x6 = dq[0]*x5
    x7 = x4*x6
    x8 = -x7
    x9 = ddq[0]*x3 + x8
    x10 = dq[0]*x3
    x11 = x10*x4
    x12 = -x11
    x13 = -ddq[1]
    x14 = x10*x6
    x15 = -x14
    x16 = x10**2
    x17 = x4**2
    x18 = -x17
    x19 = ddq[0]*x5
    x20 = x11 + x19
    x21 = x11 + x20
    x22 = x6**2
    x23 = -x22
    x24 = -x10
    x25 = 0.527*dq[1]
    x26 = x24*x25
    x27 = 0.527*x3**2 + 0.527*x5**2
    x28 = x1 + x19*x27 + x26
    x29 = x26 + x28
    x30 = x29*x5
    x31 = -x2 + x27*x6
    x32 = 0.527*ddq[1] - 9.81*x5
    x33 = x10*x31 + x32
    x34 = -9.81*x3
    x35 = x31*x6
    x36 = x25*x4
    x37 = -x16
    x38 = x23 + x37
    x39 = x27*x5
    x40 = x7 - x9
    x41 = x7 + x9
    x42 = -x13
#
    regressor_out[0] = 0
    regressor_out[1] = 0
    regressor_out[2] = 0
    regressor_out[3] = ddq[0]
    regressor_out[4] = 0
    regressor_out[5] = 0
    regressor_out[6] = 0.06*ddq[0] + x1
    regressor_out[7] = 0
    regressor_out[8] = 0.06*dq[0]**2 + dq[0]*x2
    regressor_out[9] = -0.06*x0
    regressor_out[10] = dq[0]
    regressor_out[11] = x12*x5 + x3*x9
    regressor_out[12] = x3*(x13 + x15) + x5*(x16 + x18)
    regressor_out[13] = x21*x3 + x5*(x8 + x9)
    regressor_out[14] = x11*x5 + x3*x8
    regressor_out[15] = x3*(x17 + x23) + x5*(x13 + x14)
    regressor_out[16] = x20*x5 + x3*x7
    regressor_out[17] = x21*x27*x5 + 0.06*x21 + x30
    regressor_out[18] = x3*x33 + x38*x39 + 0.06*x38 + x5*(-x34 + x35 - x36)
    regressor_out[19] = x3*(x10*x25 - x28) + x39*x40 + 0.06*x40
    regressor_out[20] = x27*x30 + 0.06*x29
    regressor_out[21] = 0
    regressor_out[22] = 0
    regressor_out[23] = 0
    regressor_out[24] = 0
    regressor_out[25] = 0
    regressor_out[26] = 0
    regressor_out[27] = 0
    regressor_out[28] = 0
    regressor_out[29] = 0
    regressor_out[30] = 0
    regressor_out[31] = 0
    regressor_out[32] = 0
    regressor_out[33] = x15
    regressor_out[34] = -x41
    regressor_out[35] = -x22 - x37
    regressor_out[36] = x42
    regressor_out[37] = -x12 - x20
    regressor_out[38] = -x15
    regressor_out[39] = 0.527*x14 - x24*x31 + x32 + 0.527*x42
    regressor_out[40] = 0.527*x41
    regressor_out[41] = 0.527*x18 - x34 + x35 - x36 + 0.527*x37
    regressor_out[42] = 0.527*x33
    regressor_out[43] = dq[1]
#
    return regressor_out




def M(parms, q):
#
    M_out = [0]*4
#
    x0 = sin(q[1])
    x1 = cos(q[1])
    x2 = -parms[19]
    x3 = x1*(0.527*x0**2 + 0.527*x1**2)
    x4 = x3 + 0.06
    x5 = parms[17]*x1 + parms[20]*x4 + x0*x2
    x6 = -parms[12]*x0 - parms[15]*x1 + 0.527*parms[18]*x0
#
    M_out[0] = parms[3] + 0.12*parms[6] + 0.0036*parms[9] + x0*(parms[11]*x0 + parms[13]*x1 + x2*x4) + x1*(parms[13]*x0 + parms[16]*x1 + parms[17]*x4) + x3*x5 + 0.06*x5
    M_out[1] = x6
    M_out[2] = x6
    M_out[3] = parms[14] + 1.054*parms[17] + 0.277729*parms[20]
#
    return M_out




def c(parms, q, dq):
#
    c_out = [0]*2
#
    x0 = sin(q[1])
    x1 = -dq[1]
    x2 = -x1
    x3 = cos(q[1])
    x4 = dq[0]*x3
    x5 = x2*x4
    x6 = dq[0]*x0
    x7 = x1*x6
    x8 = 0.527*x0**2 + 0.527*x3**2
    x9 = -0.06*dq[0]
    x10 = x4*x8 - x9
    x11 = 0.527*dq[1]
    x12 = parms[17]*x2 + parms[18]*x6 + parms[20]*x11
    x13 = parms[13]*x6 + parms[15]*x1 + parms[16]*x4 + parms[17]*x10
    x14 = -x6
    x15 = x11*x14
    x16 = -parms[19]
    x17 = parms[17]*x4 + parms[19]*x14 + parms[20]*x10
    x18 = -x11
    x19 = parms[12]*x6 + parms[14]*x1 + parms[15]*x4 + parms[17]*x18
    x20 = -x4
    x21 = parms[18]*x20 + parms[19]*x1
    x22 = parms[11]*x6 + parms[12]*x1 + parms[13]*x4 + parms[18]*x11 + x10*x16
    x23 = parms[17]*x7 + parms[20]*x15 + x12*x14 + x16*x5 + x21*x4
#
    c_out[0] = 0.06*dq[0]**2*parms[8] + dq[0]*parms[8]*x9 + x0*(parms[11]*x5 + parms[13]*x7 + x1*x13 + x10*x12 + x15*x16 + x17*x18 + x19*x20) + x23*x3*x8 + 0.06*x23 + x3*(parms[13]*x5 + parms[16]*x7 + parms[17]*x15 - x10*x21 + x19*x6 + x2*x22)
    c_out[1] = -parms[12]*x5 - parms[15]*x7 + 0.527*parms[18]*x5 - x11*x21 - x13*x14 + 0.527*x17*x6 + 0.527*x2*x21 - x22*x4
#
    return c_out




def C(parms, q, dq):
#
    C_out = [0]*4
#
    x0 = -0.06*parms[8]
    x1 = sin(q[1])
    x2 = cos(q[1])
    x3 = parms[12]*x1 + parms[15]*x2
    x4 = -x2
    x5 = x2**2
    x6 = x1**2
    x7 = x2*(0.527*x5 + 0.527*x6)
    x8 = x7 + 0.06
    x9 = parms[18]*x8
    x10 = -parms[18]
    x11 = x10*x5 + x10*x6
    x12 = -parms[8]
    x13 = x0 + x1*(x1*x9 + x3*x4) + x11*x7 + 0.06*x11 - 0.06*x12 + x2*(x1*x3 + x2*x9)
    x14 = parms[17] + parms[18]*x1 + 0.527*parms[20]
    x15 = parms[13]*x1
    x16 = parms[16]*x2
    x17 = parms[17]*x8
    x18 = -parms[19]
    x19 = -0.527*x1
    x20 = -parms[14] - 0.527*parms[17] + x3
    x21 = parms[17]*x2
    x22 = parms[20]*x8
    x23 = -x1
    x24 = parms[18]*x4 + x18
    x25 = parms[13]*x2
    x26 = parms[11]*x1 + x18*x8
    x27 = -parms[12] + 0.527*parms[18]
    x28 = parms[17]*x23 + parms[20]*x19 + x14*x23 + x18*x2 + x2*x24
    x29 = parms[15]*x1 + x2*x27
    x30 = x25 + x26
    x31 = x15 + x16 + x17
    x32 = x1*(x1*x18 + x21 + x22)
    x33 = -x2*x30 - x23*x31 + 0.527*x32
    x34 = -0.527*parms[19]
    x35 = 0.527*x18 - x34
#
    C_out[0] = dq[0]*x13
    C_out[1] = dq[0]*(x0 + x1*(parms[11]*x2 + parms[13]*x23 + parms[15] + 0.527*parms[19]*x1 + x14*x8 - x15 - x16 - x17 + x18*x19 + x20*x4 - 0.527*x21 - 0.527*x22) - 0.06*x12 - x13 + x2*(parms[16]*x23 + parms[17]*x19 + x1*x20 - x24*x8 + 2*x25 + x26 + x27) + x28*x7 + 0.06*x28 - x29) + dq[1]*x29
    C_out[2] = dq[0]*x33
    C_out[3] = dq[0]*(-parms[12]*x2 - parms[15]*x23 + 0.527*parms[18]*x2 + 0.527*x18 - x2*(x27 + x30) - x23*(-parms[15] + x31) + 0.527*x32 - x33 - x34 - x35) + dq[1]*x35
#
    return C_out




def f(parms, q, dq):
#
    f_out = [0]*2
#

    f_out[0] = dq[0]*parms[10]
    f_out[1] = dq[1]*parms[21]
#
    return f_out




def g(parms, q):
#
    g_out = [0]*2
#
    x0 = sin(q[1])
    x1 = cos(q[1])
    x2 = -9.81*x1
    x3 = -9.81*x0
#
    g_out[0] = parms[18]*x0*x2 - parms[18]*x1*x3
    g_out[1] = parms[17]*x2 - parms[19]*x3 + 0.527*parms[20]*x2
#
    return g_out












#dynparms = [L_1xx, L_1xy, L_1xz, L_1yy, L_1yz, L_1zz, l_1x, l_1y, l_1z, m_1, fv_1, L_2xx, L_2xy, L_2xz, L_2yy, L_2yz, L_2zz, l_2x, l_2y, l_2z, m_2, fv_2]
