from math import sin, cos
import numpy as np
pi = np.pi



def joint_fk00(q):
#
    pose = [0]*16
#
    x0 = cos(q[0])
    x1 = sin(q[0])
#
    pose[0] = x0
    pose[1] = 0
    pose[2] = x1
    pose[3] = 0.06*x0
    pose[4] = x1
    pose[5] = 0
    pose[6] = -x0
    pose[7] = 0.06*x1
    pose[8] = 0
    pose[9] = 1
    pose[10] = 0
    pose[11] = 0
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    return pose



def joint_fk01(q):
#
    pose = [0]*16
#
    x0 = cos(q[0])
    x1 = cos(q[1])
    x2 = x0*x1
    x3 = sin(q[0])
    x4 = sin(q[1])
    x5 = x1*x3
#
    pose[0] = x2
    pose[1] = -x3
    pose[2] = -x0*x4
    pose[3] = 0.06*x0 + 0.527*x2
    pose[4] = x5
    pose[5] = x0
    pose[6] = -x3*x4
    pose[7] = 0.06*x3 + 0.527*x5
    pose[8] = x4
    pose[9] = 0
    pose[10] = x1
    pose[11] = 0.527*x4
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    return pose



def joint_fk02(q):
#
    pose = [0]*16
#
    x0 = sin(q[0])
    x1 = sin(q[2])
    x2 = x0*x1
    x3 = cos(q[1])
    x4 = cos(q[0])
    x5 = cos(q[2])
    x6 = x4*x5
    x7 = x3*x6
    x8 = sin(q[1])
    x9 = x0*x5
    x10 = x1*x4
    x11 = 0.527*x3
    x12 = x3*x9
    x13 = x5*x8
#
    pose[0] = -x2 + x7
    pose[1] = -x4*x8
    pose[2] = x10*x3 + x9
    pose[3] = x11*x4 - 0.346*x2 + 0.06*x4 + 0.346*x7
    pose[4] = x10 + x12
    pose[5] = -x0*x8
    pose[6] = x2*x3 - x6
    pose[7] = x0*x11 + 0.06*x0 + 0.346*x10 + 0.346*x12
    pose[8] = x13
    pose[9] = x3
    pose[10] = x1*x8
    pose[11] = 0.346*x13 + 0.527*x8
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    return pose



def joint_fk03(q):
#
    pose = [0]*16
#
    x0 = sin(q[3])
    x1 = sin(q[1])
    x2 = cos(q[0])
    x3 = x1*x2
    x4 = x0*x3
    x5 = cos(q[3])
    x6 = sin(q[0])
    x7 = sin(q[2])
    x8 = x6*x7
    x9 = cos(q[1])
    x10 = cos(q[2])
    x11 = x10*x2
    x12 = x11*x9
    x13 = x12 - x8
    x14 = x13*x5
    x15 = x10*x6
    x16 = x2*x7
    x17 = 0.527*x9
    x18 = x1*x6
    x19 = x0*x18
    x20 = x15*x9
    x21 = x16 + x20
    x22 = x21*x5
    x23 = x0*x9
    x24 = x1*x10
    x25 = x24*x5
#
    pose[0] = x14 - x4
    pose[1] = -x15 - x16*x9
    pose[2] = -x0*x13 - x3*x5
    pose[3] = 0.346*x12 + 0.224*x14 + x17*x2 + 0.06*x2 - 0.224*x4 - 0.346*x8
    pose[4] = -x19 + x22
    pose[5] = x11 - x8*x9
    pose[6] = -x0*x21 - x18*x5
    pose[7] = 0.346*x16 + x17*x6 - 0.224*x19 + 0.346*x20 + 0.224*x22 + 0.06*x6
    pose[8] = x23 + x25
    pose[9] = -x1*x7
    pose[10] = -x0*x24 + x5*x9
    pose[11] = 0.527*x1 + 0.224*x23 + 0.346*x24 + 0.224*x25
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    return pose



def joint_fk04(q):
#
    pose = [0]*16
#
    x0 = sin(q[4])
    x1 = sin(q[0])
    x2 = cos(q[2])
    x3 = x1*x2
    x4 = cos(q[1])
    x5 = sin(q[2])
    x6 = cos(q[0])
    x7 = x5*x6
    x8 = -x3 - x4*x7
    x9 = x0*x8
    x10 = cos(q[4])
    x11 = sin(q[3])
    x12 = sin(q[1])
    x13 = x12*x6
    x14 = x11*x13
    x15 = cos(q[3])
    x16 = x1*x5
    x17 = x2*x6
    x18 = x17*x4
    x19 = -x16 + x18
    x20 = x15*x19
    x21 = -x14 + x20
    x22 = x10*x21
    x23 = 0.527*x4
    x24 = -x16*x4 + x17
    x25 = x0*x24
    x26 = x1*x12
    x27 = x11*x26
    x28 = x3*x4
    x29 = x28 + x7
    x30 = x15*x29
    x31 = -x27 + x30
    x32 = x10*x31
    x33 = x12*x5
    x34 = x0*x33
    x35 = x11*x4
    x36 = x12*x2
    x37 = x15*x36
    x38 = x35 + x37
    x39 = x10*x38
#
    pose[0] = x22 + x9
    pose[1] = -x11*x19 - x13*x15
    pose[2] = x0*x21 - x10*x8
    pose[3] = -0.224*x14 - 0.346*x16 + 0.346*x18 + 0.224*x20 + 0.27*x22 + x23*x6 + 0.06*x6 + 0.27*x9
    pose[4] = x25 + x32
    pose[5] = -x11*x29 - x15*x26
    pose[6] = x0*x31 - x10*x24
    pose[7] = x1*x23 + 0.06*x1 + 0.27*x25 - 0.224*x27 + 0.346*x28 + 0.224*x30 + 0.27*x32 + 0.346*x7
    pose[8] = -x34 + x39
    pose[9] = -x11*x36 + x15*x4
    pose[10] = x0*x38 + x10*x33
    pose[11] = 0.527*x12 - 0.27*x34 + 0.224*x35 + 0.346*x36 + 0.224*x37 + 0.27*x39
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    return pose



FK = {0:joint_fk00, 1:joint_fk01, 2:joint_fk02, 3:joint_fk03, 4:joint_fk04, }



def jacobian00(q):
#
    jacobian = [0]*30
#

    jacobian[0] = -0.06*sin(q[0])
    jacobian[1] = 0
    jacobian[2] = 0
    jacobian[3] = 0
    jacobian[4] = 0
    jacobian[5] = 0.06*cos(q[0])
    jacobian[6] = 0
    jacobian[7] = 0
    jacobian[8] = 0
    jacobian[9] = 0
    jacobian[10] = 0
    jacobian[11] = 0
    jacobian[12] = 0
    jacobian[13] = 0
    jacobian[14] = 0
    jacobian[15] = 0
    jacobian[16] = 0
    jacobian[17] = 0
    jacobian[18] = 0
    jacobian[19] = 0
    jacobian[20] = 0
    jacobian[21] = 0
    jacobian[22] = 0
    jacobian[23] = 0
    jacobian[24] = 0
    jacobian[25] = 1
    jacobian[26] = 0
    jacobian[27] = 0
    jacobian[28] = 0
    jacobian[29] = 0
#
    jacobian = np.array(jacobian).reshape(6,5)
    return jacobian



def jacobian01(q):
#
    jacobian = [0]*30
#
    x0 = sin(q[0])
    x1 = cos(q[1])
    x2 = 0.527*x0
    x3 = sin(q[1])
    x4 = cos(q[0])
    x5 = 0.527*x4
    x6 = 0.527*x1
#
    jacobian[0] = -0.06*x0 - x1*x2
    jacobian[1] = -x3*x5
    jacobian[2] = 0
    jacobian[3] = 0
    jacobian[4] = 0
    jacobian[5] = x1*x5 + 0.06*x4
    jacobian[6] = -x2*x3
    jacobian[7] = 0
    jacobian[8] = 0
    jacobian[9] = 0
    jacobian[10] = 0
    jacobian[11] = x0**2*x6 + x4**2*x6
    jacobian[12] = 0
    jacobian[13] = 0
    jacobian[14] = 0
    jacobian[15] = 0
    jacobian[16] = x0
    jacobian[17] = 0
    jacobian[18] = 0
    jacobian[19] = 0
    jacobian[20] = 0
    jacobian[21] = -x4
    jacobian[22] = 0
    jacobian[23] = 0
    jacobian[24] = 0
    jacobian[25] = 1
    jacobian[26] = 0
    jacobian[27] = 0
    jacobian[28] = 0
    jacobian[29] = 0
#
    jacobian = np.array(jacobian).reshape(6,5)
    return jacobian



def jacobian02(q):
#
    jacobian = [0]*30
#
    x0 = sin(q[0])
    x1 = cos(q[1])
    x2 = 0.527*x1
    x3 = x0*x2
    x4 = cos(q[0])
    x5 = 0.346*sin(q[2])
    x6 = x4*x5
    x7 = cos(q[2])
    x8 = 0.346*x1*x7
    x9 = x0*x8
    x10 = sin(q[1])
    x11 = 0.346*x10*x7 + 0.527*x10
    x12 = 0.346*x10**2*x7
    x13 = x6 + x9
    x14 = -x0*x5 + x4*x8
    x15 = x14 + x2*x4
    x16 = x0*x10
    x17 = x10*x4
#
    jacobian[0] = -0.06*x0 - x3 - x6 - x9
    jacobian[1] = -x11*x4
    jacobian[2] = -x0*x12 - x1*x13
    jacobian[3] = 0
    jacobian[4] = 0
    jacobian[5] = x15 + 0.06*x4
    jacobian[6] = -x0*x11
    jacobian[7] = x1*x14 + x12*x4
    jacobian[8] = 0
    jacobian[9] = 0
    jacobian[10] = 0
    jacobian[11] = x0*(x13 + x3) + x15*x4
    jacobian[12] = -x13*x17 + x14*x16
    jacobian[13] = 0
    jacobian[14] = 0
    jacobian[15] = 0
    jacobian[16] = x0
    jacobian[17] = -x17
    jacobian[18] = 0
    jacobian[19] = 0
    jacobian[20] = 0
    jacobian[21] = -x4
    jacobian[22] = -x16
    jacobian[23] = 0
    jacobian[24] = 0
    jacobian[25] = 1
    jacobian[26] = 0
    jacobian[27] = x1
    jacobian[28] = 0
    jacobian[29] = 0
#
    jacobian = np.array(jacobian).reshape(6,5)
    return jacobian



def jacobian03(q):
#
    jacobian = [0]*30
#
    x0 = sin(q[0])
    x1 = cos(q[1])
    x2 = 0.527*x1
    x3 = x0*x2
    x4 = sin(q[2])
    x5 = cos(q[0])
    x6 = x4*x5
    x7 = 0.346*x6
    x8 = 0.224*sin(q[3])
    x9 = sin(q[1])
    x10 = x0*x9
    x11 = x10*x8
    x12 = cos(q[2])
    x13 = x0*x12
    x14 = x1*x13
    x15 = 0.346*x14
    x16 = 0.224*cos(q[3])
    x17 = x16*(x14 + x6)
    x18 = x12*x9
    x19 = x1*x8 + x16*x18
    x20 = 0.346*x18 + x19
    x21 = x20 + 0.527*x9
    x22 = -x11 + x17
    x23 = x15 + x22 + x7
    x24 = x12*x5
    x25 = x0*x4
    x26 = x1*x25 - x24
    x27 = x4*x9
    x28 = x1*x24
    x29 = x5*x9
    x30 = x16*(-x25 + x28) - x29*x8
    x31 = -0.346*x25 + 0.346*x28 + x30
    x32 = x2*x5 + x31
    x33 = x1*x6 + x13
#
    jacobian[0] = -0.06*x0 + x11 - x15 - x17 - x3 - x7
    jacobian[1] = -x21*x5
    jacobian[2] = -x1*x23 - x10*x20
    jacobian[3] = x19*x26 - x22*x27
    jacobian[4] = 0
    jacobian[5] = x32 + 0.06*x5
    jacobian[6] = -x0*x21
    jacobian[7] = x1*x31 + x20*x29
    jacobian[8] = -x19*x33 + x27*x30
    jacobian[9] = 0
    jacobian[10] = 0
    jacobian[11] = x0*(x23 + x3) + x32*x5
    jacobian[12] = x10*x31 - x23*x29
    jacobian[13] = x22*x33 - x26*x30
    jacobian[14] = 0
    jacobian[15] = 0
    jacobian[16] = x0
    jacobian[17] = -x29
    jacobian[18] = x33
    jacobian[19] = 0
    jacobian[20] = 0
    jacobian[21] = -x5
    jacobian[22] = -x10
    jacobian[23] = x26
    jacobian[24] = 0
    jacobian[25] = 1
    jacobian[26] = 0
    jacobian[27] = x1
    jacobian[28] = x27
    jacobian[29] = 0
#
    jacobian = np.array(jacobian).reshape(6,5)
    return jacobian



def jacobian04(q):
#
    jacobian = [0]*30
#
    x0 = sin(q[0])
    x1 = cos(q[1])
    x2 = 0.527*x1
    x3 = x0*x2
    x4 = sin(q[2])
    x5 = cos(q[0])
    x6 = x4*x5
    x7 = 0.346*x6
    x8 = sin(q[3])
    x9 = sin(q[1])
    x10 = x0*x9
    x11 = x10*x8
    x12 = 0.224*x11
    x13 = cos(q[2])
    x14 = x0*x13
    x15 = x1*x14
    x16 = 0.346*x15
    x17 = cos(q[3])
    x18 = x15 + x6
    x19 = x17*x18
    x20 = 0.224*x19
    x21 = 0.27*sin(q[4])
    x22 = x13*x5
    x23 = x0*x4
    x24 = x1*x23
    x25 = x21*(x22 - x24)
    x26 = 0.27*cos(q[4])
    x27 = x26*(-x11 + x19)
    x28 = x13*x9
    x29 = x1*x8
    x30 = x17*x28
    x31 = x4*x9
    x32 = -x21*x31 + x26*(x29 + x30)
    x33 = 0.224*x29 + 0.224*x30 + x32
    x34 = 0.346*x28 + x33
    x35 = x34 + 0.527*x9
    x36 = x25 + x27
    x37 = -x12 + x20 + x36
    x38 = x16 + x37 + x7
    x39 = -x22 + x24
    x40 = -x10*x17 - x18*x8
    x41 = x1*x17 - x28*x8
    x42 = x1*x22
    x43 = x5*x9
    x44 = x43*x8
    x45 = -x23 + x42
    x46 = x17*x45
    x47 = x1*x6
    x48 = x21*(-x14 - x47) + x26*(-x44 + x46)
    x49 = -0.224*x44 + 0.224*x46 + x48
    x50 = -0.346*x23 + 0.346*x42 + x49
    x51 = x2*x5 + x50
    x52 = x14 + x47
    x53 = -x17*x43 - x45*x8
#
    jacobian[0] = -0.06*x0 + x12 - x16 - x20 - x25 - x27 - x3 - x7
    jacobian[1] = -x35*x5
    jacobian[2] = -x1*x38 - x10*x34
    jacobian[3] = -x31*x37 + x33*x39
    jacobian[4] = x32*x40 - x36*x41
    jacobian[5] = 0.06*x5 + x51
    jacobian[6] = -x0*x35
    jacobian[7] = x1*x50 + x34*x43
    jacobian[8] = x31*x49 - x33*x52
    jacobian[9] = -x32*x53 + x41*x48
    jacobian[10] = 0
    jacobian[11] = x0*(x3 + x38) + x5*x51
    jacobian[12] = x10*x50 - x38*x43
    jacobian[13] = x37*x52 - x39*x49
    jacobian[14] = x36*x53 - x40*x48
    jacobian[15] = 0
    jacobian[16] = x0
    jacobian[17] = -x43
    jacobian[18] = x52
    jacobian[19] = x53
    jacobian[20] = 0
    jacobian[21] = -x5
    jacobian[22] = -x10
    jacobian[23] = x39
    jacobian[24] = x40
    jacobian[25] = 1
    jacobian[26] = 0
    jacobian[27] = x1
    jacobian[28] = x31
    jacobian[29] = x41
#
    jacobian = np.array(jacobian).reshape(6,5)
    return jacobian



J = {0:jacobian00, 1:jacobian01, 2:jacobian02, 3:jacobian03, 4:jacobian04, }
