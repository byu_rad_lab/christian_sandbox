from math import sin, cos
import numpy as np
pi = np.pi



def joint_fk00(q):
#
    pose = [0]*16
#
    x0 = cos(q[0])
    x1 = sin(q[0])
#
    pose[0] = x0
    pose[1] = 0
    pose[2] = x1
    pose[3] = 0.346*x0
    pose[4] = x1
    pose[5] = 0
    pose[6] = -x0
    pose[7] = 0.346*x1
    pose[8] = 0
    pose[9] = 1
    pose[10] = 0
    pose[11] = 0
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    return pose



def joint_fk01(q):
#
    pose = [0]*16
#
    x0 = cos(q[0])
    x1 = cos(q[1])
    x2 = x0*x1
    x3 = sin(q[0])
    x4 = sin(q[1])
    x5 = x1*x3
#
    pose[0] = x2
    pose[1] = -x3
    pose[2] = -x0*x4
    pose[3] = 0.346*x0 + 0.224*x2
    pose[4] = x5
    pose[5] = x0
    pose[6] = -x3*x4
    pose[7] = 0.346*x3 + 0.224*x5
    pose[8] = x4
    pose[9] = 0
    pose[10] = x1
    pose[11] = 0.224*x4
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    return pose



def joint_fk02(q):
#
    pose = [0]*16
#
    x0 = sin(q[0])
    x1 = sin(q[2])
    x2 = x0*x1
    x3 = cos(q[1])
    x4 = cos(q[0])
    x5 = cos(q[2])
    x6 = x4*x5
    x7 = x3*x6
    x8 = sin(q[1])
    x9 = x0*x5
    x10 = x1*x4
    x11 = 0.224*x3
    x12 = x3*x9
    x13 = x5*x8
#
    pose[0] = -x2 + x7
    pose[1] = -x4*x8
    pose[2] = x10*x3 + x9
    pose[3] = x11*x4 - 0.27*x2 + 0.346*x4 + 0.27*x7
    pose[4] = x10 + x12
    pose[5] = -x0*x8
    pose[6] = x2*x3 - x6
    pose[7] = x0*x11 + 0.346*x0 + 0.27*x10 + 0.27*x12
    pose[8] = x13
    pose[9] = x3
    pose[10] = x1*x8
    pose[11] = 0.27*x13 + 0.224*x8
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    return pose



def joint_fk03(q):
#
    pose = [0]*16
#
    x0 = sin(q[3])
    x1 = sin(q[1])
    x2 = cos(q[0])
    x3 = x1*x2
    x4 = x0*x3
    x5 = cos(q[3])
    x6 = sin(q[0])
    x7 = sin(q[2])
    x8 = x6*x7
    x9 = cos(q[1])
    x10 = cos(q[2])
    x11 = x10*x2
    x12 = x11*x9
    x13 = x12 - x8
    x14 = x13*x5
    x15 = x10*x6
    x16 = x2*x7
    x17 = 0.224*x9
    x18 = x1*x6
    x19 = x0*x18
    x20 = x15*x9
    x21 = x16 + x20
    x22 = x21*x5
    x23 = x0*x9
    x24 = x1*x10
    x25 = x24*x5
#
    pose[0] = x14 - x4
    pose[1] = -x0*x13 - x3*x5
    pose[2] = x15 + x16*x9
    pose[3] = 0.27*x12 + 0.03*x14 + x17*x2 + 0.346*x2 - 0.03*x4 - 0.27*x8
    pose[4] = -x19 + x22
    pose[5] = -x0*x21 - x18*x5
    pose[6] = -x11 + x8*x9
    pose[7] = 0.27*x16 + x17*x6 - 0.03*x19 + 0.27*x20 + 0.03*x22 + 0.346*x6
    pose[8] = x23 + x25
    pose[9] = -x0*x24 + x5*x9
    pose[10] = x1*x7
    pose[11] = 0.224*x1 + 0.03*x23 + 0.27*x24 + 0.03*x25
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    return pose



FK = {0:joint_fk00, 1:joint_fk01, 2:joint_fk02, 3:joint_fk03, }



def jacobian00(q):
#
    jacobian = [0]*24
#

    jacobian[0] = -0.346*sin(q[0])
    jacobian[1] = 0
    jacobian[2] = 0
    jacobian[3] = 0
    jacobian[4] = 0.346*cos(q[0])
    jacobian[5] = 0
    jacobian[6] = 0
    jacobian[7] = 0
    jacobian[8] = 0
    jacobian[9] = 0
    jacobian[10] = 0
    jacobian[11] = 0
    jacobian[12] = 0
    jacobian[13] = 0
    jacobian[14] = 0
    jacobian[15] = 0
    jacobian[16] = 0
    jacobian[17] = 0
    jacobian[18] = 0
    jacobian[19] = 0
    jacobian[20] = 1
    jacobian[21] = 0
    jacobian[22] = 0
    jacobian[23] = 0
#
    jacobian = np.array(jacobian).reshape(6,4)
    return jacobian



def jacobian01(q):
#
    jacobian = [0]*24
#
    x0 = sin(q[0])
    x1 = cos(q[1])
    x2 = 0.224*x0
    x3 = sin(q[1])
    x4 = cos(q[0])
    x5 = 0.224*x4
    x6 = 0.224*x1
#
    jacobian[0] = -0.346*x0 - x1*x2
    jacobian[1] = -x3*x5
    jacobian[2] = 0
    jacobian[3] = 0
    jacobian[4] = x1*x5 + 0.346*x4
    jacobian[5] = -x2*x3
    jacobian[6] = 0
    jacobian[7] = 0
    jacobian[8] = 0
    jacobian[9] = x0**2*x6 + x4**2*x6
    jacobian[10] = 0
    jacobian[11] = 0
    jacobian[12] = 0
    jacobian[13] = x0
    jacobian[14] = 0
    jacobian[15] = 0
    jacobian[16] = 0
    jacobian[17] = -x4
    jacobian[18] = 0
    jacobian[19] = 0
    jacobian[20] = 1
    jacobian[21] = 0
    jacobian[22] = 0
    jacobian[23] = 0
#
    jacobian = np.array(jacobian).reshape(6,4)
    return jacobian



def jacobian02(q):
#
    jacobian = [0]*24
#
    x0 = sin(q[0])
    x1 = cos(q[1])
    x2 = 0.224*x1
    x3 = x0*x2
    x4 = cos(q[0])
    x5 = 0.27*sin(q[2])
    x6 = x4*x5
    x7 = cos(q[2])
    x8 = 0.27*x1*x7
    x9 = x0*x8
    x10 = sin(q[1])
    x11 = 0.27*x10*x7 + 0.224*x10
    x12 = 0.27*x10**2*x7
    x13 = x6 + x9
    x14 = -x0*x5 + x4*x8
    x15 = x14 + x2*x4
    x16 = x0*x10
    x17 = x10*x4
#
    jacobian[0] = -0.346*x0 - x3 - x6 - x9
    jacobian[1] = -x11*x4
    jacobian[2] = -x0*x12 - x1*x13
    jacobian[3] = 0
    jacobian[4] = x15 + 0.346*x4
    jacobian[5] = -x0*x11
    jacobian[6] = x1*x14 + x12*x4
    jacobian[7] = 0
    jacobian[8] = 0
    jacobian[9] = x0*(x13 + x3) + x15*x4
    jacobian[10] = -x13*x17 + x14*x16
    jacobian[11] = 0
    jacobian[12] = 0
    jacobian[13] = x0
    jacobian[14] = -x17
    jacobian[15] = 0
    jacobian[16] = 0
    jacobian[17] = -x4
    jacobian[18] = -x16
    jacobian[19] = 0
    jacobian[20] = 1
    jacobian[21] = 0
    jacobian[22] = x1
    jacobian[23] = 0
#
    jacobian = np.array(jacobian).reshape(6,4)
    return jacobian



def jacobian03(q):
#
    jacobian = [0]*24
#
    x0 = sin(q[0])
    x1 = cos(q[1])
    x2 = 0.224*x1
    x3 = x0*x2
    x4 = sin(q[2])
    x5 = cos(q[0])
    x6 = x4*x5
    x7 = 0.27*x6
    x8 = 0.03*sin(q[3])
    x9 = sin(q[1])
    x10 = x0*x9
    x11 = x10*x8
    x12 = cos(q[2])
    x13 = x0*x12
    x14 = x1*x13
    x15 = 0.27*x14
    x16 = 0.03*cos(q[3])
    x17 = x16*(x14 + x6)
    x18 = x12*x9
    x19 = x1*x8 + x16*x18
    x20 = 0.27*x18 + x19
    x21 = x20 + 0.224*x9
    x22 = -x11 + x17
    x23 = x15 + x22 + x7
    x24 = x12*x5
    x25 = x0*x4
    x26 = x1*x25 - x24
    x27 = x4*x9
    x28 = x1*x24
    x29 = x5*x9
    x30 = x16*(-x25 + x28) - x29*x8
    x31 = -0.27*x25 + 0.27*x28 + x30
    x32 = x2*x5 + x31
    x33 = x1*x6 + x13
#
    jacobian[0] = -0.346*x0 + x11 - x15 - x17 - x3 - x7
    jacobian[1] = -x21*x5
    jacobian[2] = -x1*x23 - x10*x20
    jacobian[3] = x19*x26 - x22*x27
    jacobian[4] = x32 + 0.346*x5
    jacobian[5] = -x0*x21
    jacobian[6] = x1*x31 + x20*x29
    jacobian[7] = -x19*x33 + x27*x30
    jacobian[8] = 0
    jacobian[9] = x0*(x23 + x3) + x32*x5
    jacobian[10] = x10*x31 - x23*x29
    jacobian[11] = x22*x33 - x26*x30
    jacobian[12] = 0
    jacobian[13] = x0
    jacobian[14] = -x29
    jacobian[15] = x33
    jacobian[16] = 0
    jacobian[17] = -x5
    jacobian[18] = -x10
    jacobian[19] = x26
    jacobian[20] = 1
    jacobian[21] = 0
    jacobian[22] = x1
    jacobian[23] = x27
#
    jacobian = np.array(jacobian).reshape(6,4)
    return jacobian



J = {0:jacobian00, 1:jacobian01, 2:jacobian02, 3:jacobian03, }
