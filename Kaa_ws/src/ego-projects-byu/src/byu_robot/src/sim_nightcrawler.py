#!/usr/bin/env python

import rospy
import numpy as np
import math
import time
from copy import deepcopy
import scipy.io as sio
from scipy.linalg import expm
from scipy import linalg
from scipy import signal
from std_msgs.msg import Float64
from threading import RLock, Timer
from collections import deque
#import nasa_1_dof_dist
from math import pi, cos, sin
import argparse

import falkor.msg as robot_msgs
from nasa_mpc.msg import send_pressures
from byu_robot.msg import sim_robot
from falkor.srv   import set_robot_ctrl_mode
from falkor.srv   import move_joint_prs
from falkor.srv   import move_joint_valve

from threading import RLock, Timer
from collections import deque

#from nasa_dev_arm_dyn_params import params
#import nasa_dev_arm_dynamics as nasaDyn
import importlib
from models import dyn_params


class NightCrawlerSim():
    
    def __init__(self,numJoints, nasaDyn, params):
        
        self.numOfJts = numJoints
        self.nasaDyn = nasaDyn
        self.params = params
        self.rate = 500.
        self.dt = 1./self.rate


        self.qSave = deque()
        self.qdotSave = deque()
        self.pPlusSave = deque()
        self.pMinusSave = deque()

        self.q_ = [0.]*self.numOfJts
        self.qdot_ = [0.]*self.numOfJts
        self.pPlus_ = [0.]*self.numOfJts
        self.pMinus_ = [0.]*self.numOfJts

        self.q = [0.]*self.numOfJts
        self.qdot = [0.]*self.numOfJts
        self.pPlus = [0.]*self.numOfJts
        self.pMinus = [0.]*self.numOfJts
        self.pPlusDesLast = [0.]*self.numOfJts
        self.pMinusDesLast = [0.]*self.numOfJts

        #self.pub_state = rospy.Publisher('robot_state', robot_msgs.robot_state, queue_size = 10)
        self.setRobotCtrlMode = rospy.Service('/SetRobotCtrlMode', set_robot_ctrl_mode, self.setRobotCtrlModeCallback)
        self.setPrsCmd = rospy.Service('MoveJointPrs', move_joint_prs, self.setJointPrsCallback)

        self.robot_state_pub = rospy.Publisher('/sim_robot_state', sim_robot, queue_size = 10)
        
    def setJointPrsCallback(self, msg):
        #print 'Set Joint Pressures to ', msg
        joint = msg.joint
        if joint == 0:
            self.pPlusDesLast[0] = msg.target_position[0]/1000.
            self.pPlusDesLast[1] = msg.target_position[2]/1000.
            self.pMinusDesLast[0] = msg.target_position[1]/1000.
            self.pMinusDesLast[1] = msg.target_position[3]/1000.
        elif joint == 1:
            self.pPlusDesLast[2] = msg.target_position[0]/1000.
            self.pPlusDesLast[3] = msg.target_position[2]/1000.
            self.pMinusDesLast[2] = msg.target_position[1]/1000.
            self.pMinusDesLast[3] = msg.target_position[3]/1000.      
        elif joint == 2:
            self.pPlusDesLast[4] = msg.target_position[0]/1000.
            self.pPlusDesLast[5] = msg.target_position[2]/1000.
            self.pMinusDesLast[4] = msg.target_position[1]/1000.
            self.pMinusDesLast[5] = msg.target_position[3]/1000.
        else:
            print 'ERROR Cannot handle over 6 joints/ 3 nodes'
            
        return 1

    def setRobotCtrlModeCallback(self, msg):
        #print 'Control mode set to ', msg 
        
        return 1

    def model_3(self):

        if self.numOfJts == 1:
            self.M = np.matrix(self.nasaDyn.M(self.params)).reshape(self.numOfJts, self.numOfJts)  # *10.
            self.tauGrav = self.nasaDyn.g()
        else:
            self.M = np.matrix(self.nasaDyn.M(self.params, self.q)).reshape(self.numOfJts, self.numOfJts)  # *10.
            self.tauGrav = self.nasaDyn.g(self.params, self.q)
            
        self.C = np.matrix(np.zeros([self.numOfJts, self.numOfJts]))

        self.Ks = np.matrix(np.diag([12.2844]*self.numOfJts))*0.1  # Joint Stiffness matrix
        self.Kd = np.matrix(np.diag([1.5]*self.numOfJts))  # Joint Damping matrix
        
        #print 'M : ', self.M
        #print 'Ks : ', self.Kd
        
        #print 'C : 
        #        raw_input()

        # self.Gamma_plus = np.matrix(np.diag([0.9465,0.9465,0.9465])) # Joint torque mapping for plus bladders
        # self.Gamma_minus = np.matrix(np.diag([0.9465,0.9465, 0.9465])) # Joint torque mapping for min

        self.a_plus = 1.5*0.279900
        #self.a_minus = 0.2799*0.5*0.5*0.75*0.8
        self.a_minus = 1.5*0.27990#*0.8
        self.b = -0.3548

        # self.Gamma_plus = np.matrix(np.diag([0.9465,0.9465,0.9465])) # Joint torque mapping for plus bladders
        # self.Gamma_minus = np.matrix(np.diag([0.9465,0.9465,0.9465])) # Joint
        # torque mapping for minus bladders
        # Joint torque mapping for plus bladders
        #self.Gamma_plus = np.matrix(np.diag([self.a_plus*0.5*2., self.a_plus*0.8, self.a_plus*1.5]))
        self.Gamma_plus = np.matrix(np.diag([self.a_plus]*self.numOfJts))
        # Joint torque mapping for minus bladders
        #self.Gamma_minus = np.matrix(np.diag([self.a_minus*1.5, self.a_minus*1.1, self.a_minus*1.5]))
        self.Gamma_minus = np.matrix(np.diag([self.a_minus]*self.numOfJts))

        #self.a = 0.5
        #self.b = 1.767
        #self.c = 1.772

        self.alpha = 16
        self.beta = 16

        #self.Alpha_plus = np.matrix(np.diag([self.b/self.a,self.b/self.a]))
        #self.Alpha_minus = np.matrix(np.diag([self.b/self.a,self.b/self.a]))
        #self.Beta_plus = np.matrix(np.diag([self.c/self.a,self.c/self.a]))
        #self.Beta_minus = np.matrix(np.diag([self.c/self.a,self.c/self.a]))
        self.Alpha_plus = np.matrix(np.diag([self.alpha]*self.numOfJts))
        self.Alpha_minus = np.matrix(np.diag([self.alpha]*self.numOfJts))
        self.Beta_plus = np.matrix(np.diag([self.beta]*self.numOfJts))
        self.Beta_minus = np.matrix(np.diag([self.beta]*self.numOfJts))

    def calc_A_B(self, model):
        # add pertinate models to if statment if needed
        #if model == 1:
        #    self.mode_1()
        #    Arow1 = np.hstack([-
        #                       1 *
        #                       linalg.solve(self.M, self.Kd), 1 *
        #                       linalg.solve(self.M, self.Ks), -
        #                       1 *
        #                       linalg.solve(self.M, self.Gamma_plus), -
        #                       1 *
        #                       linalg.solve(self.M, self.Gamma_minus)])
        #    Arow2 = np.hstack(
        #        [np.eye(self.num_jts), np.zeros([self.num_jts, self.num_jts]), np.zeros(
        #            [self.num_jts, self.num_jts]), np.zeros([self.num_jts, self.num_jts])])
        #    Arow3 = np.hstack(
        #        [np.zeros([self.num_jts, self.num_jts]), np.zeros([self.num_jts, self.num_jts]), -1 *
        #         self.Alpha_plus, np.zeros([self.num_jts, self.num_jts])])
        #    Arow4 = np.hstack(
        #        [np.zeros([self.num_jts, self.num_jts]), np.zeros(
        #            [self.num_jts, self.num_jts]), np.zeros([self.num_jts, self.num_jts]), -1 *
        #         self.Alpha_minus])


        #    A = np.matrix(np.vstack([Arow1, Arow2, Arow3, Arow4]))


        #    Brow1 = np.hstack(
        #        [np.zeros([self.num_jts, self.num_jts]), np.zeros([self.num_jts, self.num_jts])])
        #    Brow2 = np.hstack(
        #        [np.zeros([self.num_jts, self.num_jts]), np.zeros([self.num_jts, self.num_jts])])
        #    Brow3 = np.hstack([self.Beta_plus, np.zeros([self.num_jts, self.num_jts])])
        #    Brow4 = np.hstack([np.zeros([self.num_jts, self.num_jts]), self.Beta_minus])

        #    B = np.matrix(np.vstack([Brow1, Brow2, Brow3, Brow4]))

        #    A_d, B_d = self.discretize(A, B, 1)

        #elif model == 2:
        #    self.model_2()
        if model == 3:
            self.model_3()
            #print 'M : ', self.Gamma_plus
            #raw_input()
            Arow1 = np.hstack([-
                               linalg.solve(self.M, self.Kd), -
                               linalg.solve(self.M, self.Ks), linalg.solve(self.M, self.Gamma_plus), -
                               linalg.solve(self.M, self.Gamma_minus)])

            Arow2 = np.hstack([ np.eye(self.numOfJts),np.zeros([self.numOfJts,self.numOfJts]), np.zeros([self.numOfJts, self.numOfJts]), np.zeros([self.numOfJts, self.numOfJts])])
            Arow3 = np.hstack(
                [np.zeros([self.numOfJts, self.numOfJts]), np.zeros([self.numOfJts, self.numOfJts]), -self.Alpha_plus, np.zeros([self.numOfJts, self.numOfJts])])
            Arow4 = np.hstack(
                [np.zeros([self.numOfJts, self.numOfJts]), np.zeros(
                    [self.numOfJts, self.numOfJts]), np.zeros([self.numOfJts, self.numOfJts]), -
                 self.Alpha_minus])
            np.set_printoptions(precision = 3, linewidth = 250)

            #print Arow1
            #print Arow2
            #print Arow3
            #print Arow4

            A = np.vstack([Arow1, Arow2, Arow3, Arow4])

            
            #print A


            Brow1 = np.hstack(
                [linalg.solve(self.M, np.matrix(np.eye(self.numOfJts))), np.zeros([self.numOfJts, self.numOfJts]), np.zeros([self.numOfJts, self.numOfJts])])
            Brow2 = np.hstack(
                [np.zeros([self.numOfJts, self.numOfJts]), np.zeros([self.numOfJts, self.numOfJts]), np.zeros([self.numOfJts, self.numOfJts])])
            Brow3 = np.hstack([np.zeros([self.numOfJts, self.numOfJts]), self.Beta_plus, np.zeros([self.numOfJts, self.numOfJts])])
            Brow4 = np.hstack([np.zeros([self.numOfJts, self.numOfJts]), np.zeros([self.numOfJts, self.numOfJts]), self.Beta_minus])

            B = np.vstack([Brow1, Brow2, Brow3, Brow4])

            #print Brow1
            #print Brow2
            #print Brow3
            #print Brow4

            #print B

            #print linalg.inv(self.M)
            #print self.M

            #raw_input()
            # print " A : ", A
            # print " B : ", B

            A_d, B_d = self.discretize(A, B, 1)

            # print " A_d : ", A_d
            # print " B_d : ", B_d

            # print "Eig Val A: ", linalg.eig(A), '\n'
            # print "Eig Val A_d: ", linalg.eig(A_d)

        else:
            print "No valid model selected. Using model ", model, ".\n"
            A_d = None
            B_d = None
        return A_d, B_d

    def discretize(self, A, B, mthd):
        # Matrix Exponential
        if mthd == 1:
            A_d = linalg.expm2(A*self.dt)
            my_eye = np.eye(self.numOfJts*4)
            Asinv = linalg.solve(A, my_eye)
            B_d = np.dot(np.dot(Asinv, (A_d-my_eye)), B)
                # Zero order hold, allows A to be singular, needed for model 3
        elif mthd == 2:
            C = np.eye(4*self.numOfJts)
            D = np.zeros((4*self.numOfJts, 3*self.numOfJts))
            A_d, B_d, C_d, D_d, dt = signal.cont2discrete((A, B, C, D), self.dt, method='bilinear')
        else:
            rospy.signal_shutdown('No discretization method selected')

        return A_d, B_d

    def updateData(self):
        #self.lock.acquire()
        #try:
            # self.comp_filter_2_dof()
            # self.comp_filter_joint_3(3)
        #print 'Update'
        #print 'q : ', self.q, '\n'
        #print 'q_ : ', self.q_, '\n'
        self.q = deepcopy( self.q_)
        #print 'q : ', self.q, '\n'
        self.qdot = deepcopy(self.qdot_)
        self.pPlus = self.pPlus_
        self.pMinus = self.pMinus_

        #finally:
        #    self.lock.release()

    def runSim(self):
        
        self.updateData()

        model = 3
        Ad,Bd = self.calc_A_B(model)

        #print 'Forward simulate'
        #print 'q : ', self.q, '\n'

        x = np.vstack([np.matrix(self.qdot).reshape(self.numOfJts,1),np.matrix(self.q).reshape(self.numOfJts,1),np.matrix(self.pPlus).reshape(self.numOfJts,1), np.matrix(self.pMinus).reshape(self.numOfJts,1)])
        u = np.vstack([1*np.matrix(self.tauGrav).reshape(self.numOfJts,1),np.matrix(self.pPlusDesLast).reshape(self.numOfJts,1), np.matrix(self.pMinusDesLast).reshape(self.numOfJts,1)])

        xPlus = Ad*x + Bd*u

        xPlusArray = xPlus.A1.tolist()

        #print 'x : ', x, '\n'
        #print 'qdot : ', self.qdot, '\n'
        #print 'q : ', self.q, '\n'
        #print 'pPlus : ', self.pPlus, '\n'
        #print 'pMinus : ', self.pMinus, '\n'


        self.qdot_ = xPlusArray[0:self.numOfJts]
        self.q_ = xPlusArray[self.numOfJts:2*self.numOfJts]
        self.pPlus_ = xPlusArray[2*self.numOfJts:3*self.numOfJts]
        self.pMinus_ = xPlusArray[3*self.numOfJts:4*self.numOfJts]

        for i in xrange(self.numOfJts):
            if self.q_[i] > pi/2:
                self.q_[i] = pi/2
            elif self.q_[i] < -pi/2:
                self.q_[i] = -pi/2


        #print 'x_next : ', xPlusArray, '\n'
        #print 'qdot new : ', self.qdot_, '\n'
        #print 'q new : ', self.q_, '\n'
        #print 'pPlus new : ', self.pPlus_, '\n'
        #print 'pMinus new : ', self.pMinus_, '\n'
        
        #print u

        #self.q_save.append(self.q_)
        #self.qdot_save.append(self.qdot_)
        #self.pPlus_save.append(self.pPlus_)
        #self.pMinus_save.append(self.pMinus_)

        msg = sim_robot()
        msg.numJts = self.numOfJts
        msg.qdot = self.qdot_
        msg.q = self.q_
        msg.pPlus = self.pPlus_
        msg.pMinus = self.pMinus_

        self.robot_state_pub.publish(msg)
        

if __name__ == '__main__':
    rospy.init_node('NightCrawler_Sim_Node')
    numJts = rospy.get_param("/DOF")
    nasaDyn = importlib.import_module("models.nasa_"+str(numJts)+"_dof_dynamics")
    calc_params = dyn_params
    params = calc_params.compute(numJts)
    #print params
    
    simulation = NightCrawlerSim(numJts, nasaDyn, params)

    rate = rospy.Rate(simulation.rate)
    
    print 'Running ...' 

    while not rospy.is_shutdown():
        simulation.runSim()
        #raw_input()
        rate.sleep()

    print 'Shutting down ...'

    
