#!/usr/bin/env python

import rospy
import numpy as np
from geometry_msgs.msg import PoseStamped
import tf.transformations as tft
from threading import RLock
import copy
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import arm_plotter as ap
from ego_byu.tools import constructors as con

from ego_core.kinematics import kinematics as kin
import matplotlib.pyplot as plt
from byu_robot.msg import states
from byu_robot.msg import commands

class Kinematic_Visualization(object):
    def __init__(self):
        self.LinkTransforms = rospy.get_param("LinkTransforms")
        self.LinkNum = len(self.LinkTransforms)
        self.JointHeights = rospy.get_param("JointHeights")
        self.LinkMasses = rospy.get_param("LinkMasses")
        self.LinkRadii = rospy.get_param("LinkRadii")
        self.BaseHom = rospy.get_param("Base_Default_Pose")

        self.arm = kin.Arm()
        # Link Type Initialization
        for i in range(len(self.LinkTransforms)):
            # NO ROTATIONAL JOINT BEFORE EACH LINK

            # JOINT CCC For each link
            j = kin.JointCCC(self.JointHeights[i])
            j.setJointStiffnessScalar(0) #Set Later
            l = kin.Link(j)
            gtopend = kt.ax_vec2homog(np.array(self.LinkTransforms[i][:3]),np.array(self.LinkTransforms[i][3:]))
            l.g_top_end_ = gtopend

            #Inertia Taken form Z offset only for now
            r = self.LinkRadii[i]
            m = self.LinkMasses[i]
            length = self.LinkTransforms[i][-1]
            Ix = m * (3.0 * r * r + length) / 12.0
            Iy = m * (3.0 * r * r + length) / 12.0
            Iz = m * (r * r) / 2.0
            I = np.array([Ix, Iy, Iz])
            pos = np.array([self.LinkTransforms[i][3]/2.0, self.LinkTransforms[i][4]/2.0, self.LinkTransforms[i][5]/2.0])
            l.setInertiaSelf(m, pos, I)
            l.InitInertia()

            # addLink
            self.arm.addLink(l)

        # Real Joint Initialization for CCC Joints
        # CCC Joints have 2 DOFS natively, Bending aboud +X axis (u) and Bending about +Y (v)
        # real_joint_idx specifies which axes motion is occuring about
        jts = rospy.get_param("ControllableJointAxes")
        real_joint_idx =[]
        for i in range(len(jts)):
            if jts[i] == "X":
                real_joint_idx.append(2.0*i)
            if jts[i] == "Y":
                real_joint_idx.append(2.0*i+1.0)
        self.arm.setRealJointIDX(np.array(real_joint_idx))

        # Base variables
        g = kt.ax_vec2homog(np.array(self.BaseHom[:3]),np.array(self.BaseHom[3:]))
        self.arm.g_world_arm_ = np.array(g)

        # Limits
        l = rospy.get_param("JointLimitsLower")
        u = rospy.get_param("JointLimitsUpper")
        for i in range(self.LinkNum):
            bounds_upper = np.array([u,u])
            bounds_lower = np.array([l,l])
            self.arm.links_[i].joint_.limits().setLimitsVector(bounds_lower, bounds_upper)

        angs = np.array([.5,0,0,0,0,0])
        self.arm.setJointAngsReal(angs)
        self.arm.calcFK()

        plt.figure()
        ax = plt.gca(projection="3d")
        arm_plotter = ap.SimpleArmPlotter(self.arm)
        arm_plotter.plot(ax)
        ax.set_xlim3d(-1, 2)
        ax.set_ylim3d(-1, 2)
        ax.set_zlim3d(-1, 2)
        for link in self.arm.links_:
            ap.plot_gnomon(ax,link.g_world_end_)
        plt.show()

    # def Run(self):

if __name__ == '__main__':
    rospy.init_node('Visualization', anonymous=False)
    visualization = Kinematic_Visualization()
    # visualization.Run()
