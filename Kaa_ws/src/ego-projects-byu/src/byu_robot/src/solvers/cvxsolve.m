% Produced by CVXGEN, 2017-05-23 15:26:17 -0400.
% CVXGEN is Copyright (C) 2006-2012 Jacob Mattingley, jem@cvxgen.com.
% The code in this file is Copyright (C) 2006-2012 Jacob Mattingley.
% CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial
% applications without prior written permission from Jacob Mattingley.

% Filename: cvxsolve.m.
% Description: Solution file, via cvx, for use with sample.m.
function [vars, status] = cvxsolve(params, settings)
A11 = params.A11;
A12 = params.A12;
A13 = params.A13;
A14 = params.A14;
A21 = params.A21;
A22 = params.A22;
A23 = params.A23;
A24 = params.A24;
A33 = params.A33;
A44 = params.A44;
B12 = params.B12;
B13 = params.B13;
B22 = params.B22;
B23 = params.B23;
B32 = params.B32;
B43 = params.B43;
J = params.J;
Q = params.Q;
R = params.R;
S = params.S;
p_max = params.p_max;
p_min = params.p_min;
p_minus_0 = params.p_minus_0;
p_minus_des_0 = params.p_minus_des_0;
p_minus_t = params.p_minus_t;
p_plus_0 = params.p_plus_0;
p_plus_des_0 = params.p_plus_des_0;
p_plus_t = params.p_plus_t;
q_0 = params.q_0;
q_dist = params.q_dist;
q_goal = params.q_goal;
q_int = params.q_int;
qdot_0 = params.qdot_0;
qdot_dist = params.qdot_dist;
cvx_begin
  % Caution: automatically generated by cvxgen. May be incorrect.
  variable q_1;
  variable p_plus_1;
  variable p_minus_1;
  variable q_2;
  variable p_plus_2;
  variable p_minus_2;
  variable q_3;
  variable p_plus_3;
  variable p_minus_3;
  variable q_4;
  variable p_plus_4;
  variable p_minus_4;
  variable q_5;
  variable p_plus_5;
  variable p_minus_5;
  variable q_6;
  variable p_plus_6;
  variable p_minus_6;
  variable q_7;
  variable p_plus_7;
  variable p_minus_7;
  variable q_8;
  variable p_plus_8;
  variable p_minus_8;
  variable q_9;
  variable p_plus_9;
  variable p_minus_9;
  variable q_10;
  variable p_plus_10;
  variable p_minus_10;
  variable q_11;
  variable p_plus_11;
  variable p_minus_11;
  variable q_12;
  variable p_plus_12;
  variable p_minus_12;
  variable q_13;
  variable p_plus_13;
  variable p_minus_13;
  variable q_14;
  variable p_plus_14;
  variable p_minus_14;
  variable q_15;
  variable p_plus_15;
  variable p_minus_15;
  variable q_16;
  variable p_plus_16;
  variable p_minus_16;
  variable q_17;
  variable p_plus_17;
  variable p_minus_17;
  variable q_18;
  variable p_plus_18;
  variable p_minus_18;
  variable q_19;
  variable p_plus_19;
  variable p_minus_19;
  variable q_20;
  variable p_plus_20;
  variable p_minus_20;
  variable p_plus_des_1;
  variable p_minus_des_1;
  variable p_plus_des_2;
  variable p_minus_des_2;
  variable p_plus_des_3;
  variable p_minus_des_3;
  variable p_plus_des_4;
  variable p_minus_des_4;
  variable p_plus_des_5;
  variable p_minus_des_5;
  variable p_plus_des_6;
  variable p_minus_des_6;
  variable p_plus_des_7;
  variable p_minus_des_7;
  variable p_plus_des_8;
  variable p_minus_des_8;
  variable p_plus_des_9;
  variable p_minus_des_9;
  variable p_plus_des_10;
  variable p_minus_des_10;
  variable p_plus_des_11;
  variable p_minus_des_11;
  variable p_plus_des_12;
  variable p_minus_des_12;
  variable p_plus_des_13;
  variable p_minus_des_13;
  variable p_plus_des_14;
  variable p_minus_des_14;
  variable p_plus_des_15;
  variable p_minus_des_15;
  variable p_plus_des_16;
  variable p_minus_des_16;
  variable p_plus_des_17;
  variable p_minus_des_17;
  variable p_plus_des_18;
  variable p_minus_des_18;
  variable p_plus_des_19;
  variable p_minus_des_19;
  variable p_plus_des_20;
  variable p_minus_des_20;
  variable p_plus_des_21;
  variable p_minus_des_21;
  variable qdot_21;
  variable qdot_1;
  variable qdot_2;
  variable qdot_3;
  variable qdot_4;
  variable qdot_5;
  variable qdot_6;
  variable qdot_7;
  variable qdot_8;
  variable qdot_9;
  variable qdot_10;
  variable qdot_11;
  variable qdot_12;
  variable qdot_13;
  variable qdot_14;
  variable qdot_15;
  variable qdot_16;
  variable qdot_17;
  variable qdot_18;
  variable qdot_19;
  variable qdot_20;
  variable q_21;
  variable p_plus_21;
  variable p_minus_21;

  minimize(quad_form(q_goal - q_1 + q_int, Q) + quad_form(p_plus_t - p_plus_1, J) + quad_form(p_minus_t - p_minus_1, J) + quad_form(q_goal - q_2 + q_int, Q) + quad_form(p_plus_t - p_plus_2, J) + quad_form(p_minus_t - p_minus_2, J) + quad_form(q_goal - q_3 + q_int, Q) + quad_form(p_plus_t - p_plus_3, J) + quad_form(p_minus_t - p_minus_3, J) + quad_form(q_goal - q_4 + q_int, Q) + quad_form(p_plus_t - p_plus_4, J) + quad_form(p_minus_t - p_minus_4, J) + quad_form(q_goal - q_5 + q_int, Q) + quad_form(p_plus_t - p_plus_5, J) + quad_form(p_minus_t - p_minus_5, J) + quad_form(q_goal - q_6 + q_int, Q) + quad_form(p_plus_t - p_plus_6, J) + quad_form(p_minus_t - p_minus_6, J) + quad_form(q_goal - q_7 + q_int, Q) + quad_form(p_plus_t - p_plus_7, J) + quad_form(p_minus_t - p_minus_7, J) + quad_form(q_goal - q_8 + q_int, Q) + quad_form(p_plus_t - p_plus_8, J) + quad_form(p_minus_t - p_minus_8, J) + quad_form(q_goal - q_9 + q_int, Q) + quad_form(p_plus_t - p_plus_9, J) + quad_form(p_minus_t - p_minus_9, J) + quad_form(q_goal - q_10 + q_int, Q) + quad_form(p_plus_t - p_plus_10, J) + quad_form(p_minus_t - p_minus_10, J) + quad_form(q_goal - q_11 + q_int, Q) + quad_form(p_plus_t - p_plus_11, J) + quad_form(p_minus_t - p_minus_11, J) + quad_form(q_goal - q_12 + q_int, Q) + quad_form(p_plus_t - p_plus_12, J) + quad_form(p_minus_t - p_minus_12, J) + quad_form(q_goal - q_13 + q_int, Q) + quad_form(p_plus_t - p_plus_13, J) + quad_form(p_minus_t - p_minus_13, J) + quad_form(q_goal - q_14 + q_int, Q) + quad_form(p_plus_t - p_plus_14, J) + quad_form(p_minus_t - p_minus_14, J) + quad_form(q_goal - q_15 + q_int, Q) + quad_form(p_plus_t - p_plus_15, J) + quad_form(p_minus_t - p_minus_15, J) + quad_form(q_goal - q_16 + q_int, Q) + quad_form(p_plus_t - p_plus_16, J) + quad_form(p_minus_t - p_minus_16, J) + quad_form(q_goal - q_17 + q_int, Q) + quad_form(p_plus_t - p_plus_17, J) + quad_form(p_minus_t - p_minus_17, J) + quad_form(q_goal - q_18 + q_int, Q) + quad_form(p_plus_t - p_plus_18, J) + quad_form(p_minus_t - p_minus_18, J) + quad_form(q_goal - q_19 + q_int, Q) + quad_form(p_plus_t - p_plus_19, J) + quad_form(p_minus_t - p_minus_19, J) + quad_form(q_goal - q_20 + q_int, Q) + quad_form(p_plus_t - p_plus_20, J) + quad_form(p_minus_t - p_minus_20, J) + quad_form(p_plus_des_1 - p_plus_des_0, R) + quad_form(p_minus_des_1 - p_minus_des_0, R) + quad_form(p_plus_des_2 - p_plus_des_1, R) + quad_form(p_minus_des_2 - p_minus_des_1, R) + quad_form(p_plus_des_3 - p_plus_des_2, R) + quad_form(p_minus_des_3 - p_minus_des_2, R) + quad_form(p_plus_des_4 - p_plus_des_3, R) + quad_form(p_minus_des_4 - p_minus_des_3, R) + quad_form(p_plus_des_5 - p_plus_des_4, R) + quad_form(p_minus_des_5 - p_minus_des_4, R) + quad_form(p_plus_des_6 - p_plus_des_5, R) + quad_form(p_minus_des_6 - p_minus_des_5, R) + quad_form(p_plus_des_7 - p_plus_des_6, R) + quad_form(p_minus_des_7 - p_minus_des_6, R) + quad_form(p_plus_des_8 - p_plus_des_7, R) + quad_form(p_minus_des_8 - p_minus_des_7, R) + quad_form(p_plus_des_9 - p_plus_des_8, R) + quad_form(p_minus_des_9 - p_minus_des_8, R) + quad_form(p_plus_des_10 - p_plus_des_9, R) + quad_form(p_minus_des_10 - p_minus_des_9, R) + quad_form(p_plus_des_11 - p_plus_des_10, R) + quad_form(p_minus_des_11 - p_minus_des_10, R) + quad_form(p_plus_des_12 - p_plus_des_11, R) + quad_form(p_minus_des_12 - p_minus_des_11, R) + quad_form(p_plus_des_13 - p_plus_des_12, R) + quad_form(p_minus_des_13 - p_minus_des_12, R) + quad_form(p_plus_des_14 - p_plus_des_13, R) + quad_form(p_minus_des_14 - p_minus_des_13, R) + quad_form(p_plus_des_15 - p_plus_des_14, R) + quad_form(p_minus_des_15 - p_minus_des_14, R) + quad_form(p_plus_des_16 - p_plus_des_15, R) + quad_form(p_minus_des_16 - p_minus_des_15, R) + quad_form(p_plus_des_17 - p_plus_des_16, R) + quad_form(p_minus_des_17 - p_minus_des_16, R) + quad_form(p_plus_des_18 - p_plus_des_17, R) + quad_form(p_minus_des_18 - p_minus_des_17, R) + quad_form(p_plus_des_19 - p_plus_des_18, R) + quad_form(p_minus_des_19 - p_minus_des_18, R) + quad_form(p_plus_des_20 - p_plus_des_19, R) + quad_form(p_minus_des_20 - p_minus_des_19, R) + quad_form(p_plus_des_21 - p_plus_des_20, R) + quad_form(p_minus_des_21 - p_minus_des_20, R) + quad_form(qdot_21, S));
  subject to
    qdot_1 == A11*qdot_0 + A12*q_0 + A13*p_plus_0 + A14*p_minus_0 + B12*p_plus_des_1 + B13*p_minus_des_1 + qdot_dist;
    qdot_2 == A11*qdot_1 + A12*q_1 + A13*p_plus_1 + A14*p_minus_1 + B12*p_plus_des_2 + B13*p_minus_des_2 + qdot_dist;
    qdot_3 == A11*qdot_2 + A12*q_2 + A13*p_plus_2 + A14*p_minus_2 + B12*p_plus_des_3 + B13*p_minus_des_3 + qdot_dist;
    qdot_4 == A11*qdot_3 + A12*q_3 + A13*p_plus_3 + A14*p_minus_3 + B12*p_plus_des_4 + B13*p_minus_des_4 + qdot_dist;
    qdot_5 == A11*qdot_4 + A12*q_4 + A13*p_plus_4 + A14*p_minus_4 + B12*p_plus_des_5 + B13*p_minus_des_5 + qdot_dist;
    qdot_6 == A11*qdot_5 + A12*q_5 + A13*p_plus_5 + A14*p_minus_5 + B12*p_plus_des_6 + B13*p_minus_des_6 + qdot_dist;
    qdot_7 == A11*qdot_6 + A12*q_6 + A13*p_plus_6 + A14*p_minus_6 + B12*p_plus_des_7 + B13*p_minus_des_7 + qdot_dist;
    qdot_8 == A11*qdot_7 + A12*q_7 + A13*p_plus_7 + A14*p_minus_7 + B12*p_plus_des_8 + B13*p_minus_des_8 + qdot_dist;
    qdot_9 == A11*qdot_8 + A12*q_8 + A13*p_plus_8 + A14*p_minus_8 + B12*p_plus_des_9 + B13*p_minus_des_9 + qdot_dist;
    qdot_10 == A11*qdot_9 + A12*q_9 + A13*p_plus_9 + A14*p_minus_9 + B12*p_plus_des_10 + B13*p_minus_des_10 + qdot_dist;
    qdot_11 == A11*qdot_10 + A12*q_10 + A13*p_plus_10 + A14*p_minus_10 + B12*p_plus_des_11 + B13*p_minus_des_11 + qdot_dist;
    qdot_12 == A11*qdot_11 + A12*q_11 + A13*p_plus_11 + A14*p_minus_11 + B12*p_plus_des_12 + B13*p_minus_des_12 + qdot_dist;
    qdot_13 == A11*qdot_12 + A12*q_12 + A13*p_plus_12 + A14*p_minus_12 + B12*p_plus_des_13 + B13*p_minus_des_13 + qdot_dist;
    qdot_14 == A11*qdot_13 + A12*q_13 + A13*p_plus_13 + A14*p_minus_13 + B12*p_plus_des_14 + B13*p_minus_des_14 + qdot_dist;
    qdot_15 == A11*qdot_14 + A12*q_14 + A13*p_plus_14 + A14*p_minus_14 + B12*p_plus_des_15 + B13*p_minus_des_15 + qdot_dist;
    qdot_16 == A11*qdot_15 + A12*q_15 + A13*p_plus_15 + A14*p_minus_15 + B12*p_plus_des_16 + B13*p_minus_des_16 + qdot_dist;
    qdot_17 == A11*qdot_16 + A12*q_16 + A13*p_plus_16 + A14*p_minus_16 + B12*p_plus_des_17 + B13*p_minus_des_17 + qdot_dist;
    qdot_18 == A11*qdot_17 + A12*q_17 + A13*p_plus_17 + A14*p_minus_17 + B12*p_plus_des_18 + B13*p_minus_des_18 + qdot_dist;
    qdot_19 == A11*qdot_18 + A12*q_18 + A13*p_plus_18 + A14*p_minus_18 + B12*p_plus_des_19 + B13*p_minus_des_19 + qdot_dist;
    qdot_20 == A11*qdot_19 + A12*q_19 + A13*p_plus_19 + A14*p_minus_19 + B12*p_plus_des_20 + B13*p_minus_des_20 + qdot_dist;
    qdot_21 == A11*qdot_20 + A12*q_20 + A13*p_plus_20 + A14*p_minus_20 + B12*p_plus_des_21 + B13*p_minus_des_21 + qdot_dist;
    q_1 == A21*qdot_0 + A22*q_0 + A23*p_plus_0 + A24*p_minus_0 + B22*p_plus_des_1 + B23*p_minus_des_1 + q_dist;
    q_2 == A21*qdot_1 + A22*q_1 + A23*p_plus_1 + A24*p_minus_1 + B22*p_plus_des_2 + B23*p_minus_des_2 + q_dist;
    q_3 == A21*qdot_2 + A22*q_2 + A23*p_plus_2 + A24*p_minus_2 + B22*p_plus_des_3 + B23*p_minus_des_3 + q_dist;
    q_4 == A21*qdot_3 + A22*q_3 + A23*p_plus_3 + A24*p_minus_3 + B22*p_plus_des_4 + B23*p_minus_des_4 + q_dist;
    q_5 == A21*qdot_4 + A22*q_4 + A23*p_plus_4 + A24*p_minus_4 + B22*p_plus_des_5 + B23*p_minus_des_5 + q_dist;
    q_6 == A21*qdot_5 + A22*q_5 + A23*p_plus_5 + A24*p_minus_5 + B22*p_plus_des_6 + B23*p_minus_des_6 + q_dist;
    q_7 == A21*qdot_6 + A22*q_6 + A23*p_plus_6 + A24*p_minus_6 + B22*p_plus_des_7 + B23*p_minus_des_7 + q_dist;
    q_8 == A21*qdot_7 + A22*q_7 + A23*p_plus_7 + A24*p_minus_7 + B22*p_plus_des_8 + B23*p_minus_des_8 + q_dist;
    q_9 == A21*qdot_8 + A22*q_8 + A23*p_plus_8 + A24*p_minus_8 + B22*p_plus_des_9 + B23*p_minus_des_9 + q_dist;
    q_10 == A21*qdot_9 + A22*q_9 + A23*p_plus_9 + A24*p_minus_9 + B22*p_plus_des_10 + B23*p_minus_des_10 + q_dist;
    q_11 == A21*qdot_10 + A22*q_10 + A23*p_plus_10 + A24*p_minus_10 + B22*p_plus_des_11 + B23*p_minus_des_11 + q_dist;
    q_12 == A21*qdot_11 + A22*q_11 + A23*p_plus_11 + A24*p_minus_11 + B22*p_plus_des_12 + B23*p_minus_des_12 + q_dist;
    q_13 == A21*qdot_12 + A22*q_12 + A23*p_plus_12 + A24*p_minus_12 + B22*p_plus_des_13 + B23*p_minus_des_13 + q_dist;
    q_14 == A21*qdot_13 + A22*q_13 + A23*p_plus_13 + A24*p_minus_13 + B22*p_plus_des_14 + B23*p_minus_des_14 + q_dist;
    q_15 == A21*qdot_14 + A22*q_14 + A23*p_plus_14 + A24*p_minus_14 + B22*p_plus_des_15 + B23*p_minus_des_15 + q_dist;
    q_16 == A21*qdot_15 + A22*q_15 + A23*p_plus_15 + A24*p_minus_15 + B22*p_plus_des_16 + B23*p_minus_des_16 + q_dist;
    q_17 == A21*qdot_16 + A22*q_16 + A23*p_plus_16 + A24*p_minus_16 + B22*p_plus_des_17 + B23*p_minus_des_17 + q_dist;
    q_18 == A21*qdot_17 + A22*q_17 + A23*p_plus_17 + A24*p_minus_17 + B22*p_plus_des_18 + B23*p_minus_des_18 + q_dist;
    q_19 == A21*qdot_18 + A22*q_18 + A23*p_plus_18 + A24*p_minus_18 + B22*p_plus_des_19 + B23*p_minus_des_19 + q_dist;
    q_20 == A21*qdot_19 + A22*q_19 + A23*p_plus_19 + A24*p_minus_19 + B22*p_plus_des_20 + B23*p_minus_des_20 + q_dist;
    q_21 == A21*qdot_20 + A22*q_20 + A23*p_plus_20 + A24*p_minus_20 + B22*p_plus_des_21 + B23*p_minus_des_21 + q_dist;
    p_plus_1 == A33*p_plus_0 + B32*p_plus_des_1;
    p_plus_2 == A33*p_plus_1 + B32*p_plus_des_2;
    p_plus_3 == A33*p_plus_2 + B32*p_plus_des_3;
    p_plus_4 == A33*p_plus_3 + B32*p_plus_des_4;
    p_plus_5 == A33*p_plus_4 + B32*p_plus_des_5;
    p_plus_6 == A33*p_plus_5 + B32*p_plus_des_6;
    p_plus_7 == A33*p_plus_6 + B32*p_plus_des_7;
    p_plus_8 == A33*p_plus_7 + B32*p_plus_des_8;
    p_plus_9 == A33*p_plus_8 + B32*p_plus_des_9;
    p_plus_10 == A33*p_plus_9 + B32*p_plus_des_10;
    p_plus_11 == A33*p_plus_10 + B32*p_plus_des_11;
    p_plus_12 == A33*p_plus_11 + B32*p_plus_des_12;
    p_plus_13 == A33*p_plus_12 + B32*p_plus_des_13;
    p_plus_14 == A33*p_plus_13 + B32*p_plus_des_14;
    p_plus_15 == A33*p_plus_14 + B32*p_plus_des_15;
    p_plus_16 == A33*p_plus_15 + B32*p_plus_des_16;
    p_plus_17 == A33*p_plus_16 + B32*p_plus_des_17;
    p_plus_18 == A33*p_plus_17 + B32*p_plus_des_18;
    p_plus_19 == A33*p_plus_18 + B32*p_plus_des_19;
    p_plus_20 == A33*p_plus_19 + B32*p_plus_des_20;
    p_plus_21 == A33*p_plus_20 + B32*p_plus_des_21;
    p_minus_1 == A44*p_minus_0 + B43*p_minus_des_1;
    p_minus_2 == A44*p_minus_1 + B43*p_minus_des_2;
    p_minus_3 == A44*p_minus_2 + B43*p_minus_des_3;
    p_minus_4 == A44*p_minus_3 + B43*p_minus_des_4;
    p_minus_5 == A44*p_minus_4 + B43*p_minus_des_5;
    p_minus_6 == A44*p_minus_5 + B43*p_minus_des_6;
    p_minus_7 == A44*p_minus_6 + B43*p_minus_des_7;
    p_minus_8 == A44*p_minus_7 + B43*p_minus_des_8;
    p_minus_9 == A44*p_minus_8 + B43*p_minus_des_9;
    p_minus_10 == A44*p_minus_9 + B43*p_minus_des_10;
    p_minus_11 == A44*p_minus_10 + B43*p_minus_des_11;
    p_minus_12 == A44*p_minus_11 + B43*p_minus_des_12;
    p_minus_13 == A44*p_minus_12 + B43*p_minus_des_13;
    p_minus_14 == A44*p_minus_13 + B43*p_minus_des_14;
    p_minus_15 == A44*p_minus_14 + B43*p_minus_des_15;
    p_minus_16 == A44*p_minus_15 + B43*p_minus_des_16;
    p_minus_17 == A44*p_minus_16 + B43*p_minus_des_17;
    p_minus_18 == A44*p_minus_17 + B43*p_minus_des_18;
    p_minus_19 == A44*p_minus_18 + B43*p_minus_des_19;
    p_minus_20 == A44*p_minus_19 + B43*p_minus_des_20;
    p_minus_21 == A44*p_minus_20 + B43*p_minus_des_21;
    p_min <= p_plus_des_1;
    p_min <= p_plus_des_2;
    p_min <= p_plus_des_3;
    p_min <= p_plus_des_4;
    p_min <= p_plus_des_5;
    p_min <= p_plus_des_6;
    p_min <= p_plus_des_7;
    p_min <= p_plus_des_8;
    p_min <= p_plus_des_9;
    p_min <= p_plus_des_10;
    p_min <= p_plus_des_11;
    p_min <= p_plus_des_12;
    p_min <= p_plus_des_13;
    p_min <= p_plus_des_14;
    p_min <= p_plus_des_15;
    p_min <= p_plus_des_16;
    p_min <= p_plus_des_17;
    p_min <= p_plus_des_18;
    p_min <= p_plus_des_19;
    p_min <= p_plus_des_20;
    p_min <= p_plus_des_21;
    p_plus_des_1 <= p_max;
    p_plus_des_2 <= p_max;
    p_plus_des_3 <= p_max;
    p_plus_des_4 <= p_max;
    p_plus_des_5 <= p_max;
    p_plus_des_6 <= p_max;
    p_plus_des_7 <= p_max;
    p_plus_des_8 <= p_max;
    p_plus_des_9 <= p_max;
    p_plus_des_10 <= p_max;
    p_plus_des_11 <= p_max;
    p_plus_des_12 <= p_max;
    p_plus_des_13 <= p_max;
    p_plus_des_14 <= p_max;
    p_plus_des_15 <= p_max;
    p_plus_des_16 <= p_max;
    p_plus_des_17 <= p_max;
    p_plus_des_18 <= p_max;
    p_plus_des_19 <= p_max;
    p_plus_des_20 <= p_max;
    p_plus_des_21 <= p_max;
    p_min <= p_minus_des_1;
    p_min <= p_minus_des_2;
    p_min <= p_minus_des_3;
    p_min <= p_minus_des_4;
    p_min <= p_minus_des_5;
    p_min <= p_minus_des_6;
    p_min <= p_minus_des_7;
    p_min <= p_minus_des_8;
    p_min <= p_minus_des_9;
    p_min <= p_minus_des_10;
    p_min <= p_minus_des_11;
    p_min <= p_minus_des_12;
    p_min <= p_minus_des_13;
    p_min <= p_minus_des_14;
    p_min <= p_minus_des_15;
    p_min <= p_minus_des_16;
    p_min <= p_minus_des_17;
    p_min <= p_minus_des_18;
    p_min <= p_minus_des_19;
    p_min <= p_minus_des_20;
    p_min <= p_minus_des_21;
    p_minus_des_1 <= p_max;
    p_minus_des_2 <= p_max;
    p_minus_des_3 <= p_max;
    p_minus_des_4 <= p_max;
    p_minus_des_5 <= p_max;
    p_minus_des_6 <= p_max;
    p_minus_des_7 <= p_max;
    p_minus_des_8 <= p_max;
    p_minus_des_9 <= p_max;
    p_minus_des_10 <= p_max;
    p_minus_des_11 <= p_max;
    p_minus_des_12 <= p_max;
    p_minus_des_13 <= p_max;
    p_minus_des_14 <= p_max;
    p_minus_des_15 <= p_max;
    p_minus_des_16 <= p_max;
    p_minus_des_17 <= p_max;
    p_minus_des_18 <= p_max;
    p_minus_des_19 <= p_max;
    p_minus_des_20 <= p_max;
    p_minus_des_21 <= p_max;
cvx_end
vars.p_minus_1 = p_minus_1;
vars.p_minus{1} = p_minus_1;
vars.p_minus_2 = p_minus_2;
vars.p_minus{2} = p_minus_2;
vars.p_minus_3 = p_minus_3;
vars.p_minus{3} = p_minus_3;
vars.p_minus_4 = p_minus_4;
vars.p_minus{4} = p_minus_4;
vars.p_minus_5 = p_minus_5;
vars.p_minus{5} = p_minus_5;
vars.p_minus_6 = p_minus_6;
vars.p_minus{6} = p_minus_6;
vars.p_minus_7 = p_minus_7;
vars.p_minus{7} = p_minus_7;
vars.p_minus_8 = p_minus_8;
vars.p_minus{8} = p_minus_8;
vars.p_minus_9 = p_minus_9;
vars.p_minus{9} = p_minus_9;
vars.p_minus_10 = p_minus_10;
vars.p_minus{10} = p_minus_10;
vars.p_minus_11 = p_minus_11;
vars.p_minus{11} = p_minus_11;
vars.p_minus_12 = p_minus_12;
vars.p_minus{12} = p_minus_12;
vars.p_minus_13 = p_minus_13;
vars.p_minus{13} = p_minus_13;
vars.p_minus_14 = p_minus_14;
vars.p_minus{14} = p_minus_14;
vars.p_minus_15 = p_minus_15;
vars.p_minus{15} = p_minus_15;
vars.p_minus_16 = p_minus_16;
vars.p_minus{16} = p_minus_16;
vars.p_minus_17 = p_minus_17;
vars.p_minus{17} = p_minus_17;
vars.p_minus_18 = p_minus_18;
vars.p_minus{18} = p_minus_18;
vars.p_minus_19 = p_minus_19;
vars.p_minus{19} = p_minus_19;
vars.p_minus_20 = p_minus_20;
vars.p_minus{20} = p_minus_20;
vars.p_minus_21 = p_minus_21;
vars.p_minus{21} = p_minus_21;
vars.p_minus_des_1 = p_minus_des_1;
vars.p_minus_des{1} = p_minus_des_1;
vars.p_minus_des_2 = p_minus_des_2;
vars.p_minus_des{2} = p_minus_des_2;
vars.p_minus_des_3 = p_minus_des_3;
vars.p_minus_des{3} = p_minus_des_3;
vars.p_minus_des_4 = p_minus_des_4;
vars.p_minus_des{4} = p_minus_des_4;
vars.p_minus_des_5 = p_minus_des_5;
vars.p_minus_des{5} = p_minus_des_5;
vars.p_minus_des_6 = p_minus_des_6;
vars.p_minus_des{6} = p_minus_des_6;
vars.p_minus_des_7 = p_minus_des_7;
vars.p_minus_des{7} = p_minus_des_7;
vars.p_minus_des_8 = p_minus_des_8;
vars.p_minus_des{8} = p_minus_des_8;
vars.p_minus_des_9 = p_minus_des_9;
vars.p_minus_des{9} = p_minus_des_9;
vars.p_minus_des_10 = p_minus_des_10;
vars.p_minus_des{10} = p_minus_des_10;
vars.p_minus_des_11 = p_minus_des_11;
vars.p_minus_des{11} = p_minus_des_11;
vars.p_minus_des_12 = p_minus_des_12;
vars.p_minus_des{12} = p_minus_des_12;
vars.p_minus_des_13 = p_minus_des_13;
vars.p_minus_des{13} = p_minus_des_13;
vars.p_minus_des_14 = p_minus_des_14;
vars.p_minus_des{14} = p_minus_des_14;
vars.p_minus_des_15 = p_minus_des_15;
vars.p_minus_des{15} = p_minus_des_15;
vars.p_minus_des_16 = p_minus_des_16;
vars.p_minus_des{16} = p_minus_des_16;
vars.p_minus_des_17 = p_minus_des_17;
vars.p_minus_des{17} = p_minus_des_17;
vars.p_minus_des_18 = p_minus_des_18;
vars.p_minus_des{18} = p_minus_des_18;
vars.p_minus_des_19 = p_minus_des_19;
vars.p_minus_des{19} = p_minus_des_19;
vars.p_minus_des_20 = p_minus_des_20;
vars.p_minus_des{20} = p_minus_des_20;
vars.p_minus_des_21 = p_minus_des_21;
vars.p_minus_des{21} = p_minus_des_21;
vars.p_plus_1 = p_plus_1;
vars.p_plus{1} = p_plus_1;
vars.p_plus_2 = p_plus_2;
vars.p_plus{2} = p_plus_2;
vars.p_plus_3 = p_plus_3;
vars.p_plus{3} = p_plus_3;
vars.p_plus_4 = p_plus_4;
vars.p_plus{4} = p_plus_4;
vars.p_plus_5 = p_plus_5;
vars.p_plus{5} = p_plus_5;
vars.p_plus_6 = p_plus_6;
vars.p_plus{6} = p_plus_6;
vars.p_plus_7 = p_plus_7;
vars.p_plus{7} = p_plus_7;
vars.p_plus_8 = p_plus_8;
vars.p_plus{8} = p_plus_8;
vars.p_plus_9 = p_plus_9;
vars.p_plus{9} = p_plus_9;
vars.p_plus_10 = p_plus_10;
vars.p_plus{10} = p_plus_10;
vars.p_plus_11 = p_plus_11;
vars.p_plus{11} = p_plus_11;
vars.p_plus_12 = p_plus_12;
vars.p_plus{12} = p_plus_12;
vars.p_plus_13 = p_plus_13;
vars.p_plus{13} = p_plus_13;
vars.p_plus_14 = p_plus_14;
vars.p_plus{14} = p_plus_14;
vars.p_plus_15 = p_plus_15;
vars.p_plus{15} = p_plus_15;
vars.p_plus_16 = p_plus_16;
vars.p_plus{16} = p_plus_16;
vars.p_plus_17 = p_plus_17;
vars.p_plus{17} = p_plus_17;
vars.p_plus_18 = p_plus_18;
vars.p_plus{18} = p_plus_18;
vars.p_plus_19 = p_plus_19;
vars.p_plus{19} = p_plus_19;
vars.p_plus_20 = p_plus_20;
vars.p_plus{20} = p_plus_20;
vars.p_plus_21 = p_plus_21;
vars.p_plus{21} = p_plus_21;
vars.p_plus_des_1 = p_plus_des_1;
vars.p_plus_des{1} = p_plus_des_1;
vars.p_plus_des_2 = p_plus_des_2;
vars.p_plus_des{2} = p_plus_des_2;
vars.p_plus_des_3 = p_plus_des_3;
vars.p_plus_des{3} = p_plus_des_3;
vars.p_plus_des_4 = p_plus_des_4;
vars.p_plus_des{4} = p_plus_des_4;
vars.p_plus_des_5 = p_plus_des_5;
vars.p_plus_des{5} = p_plus_des_5;
vars.p_plus_des_6 = p_plus_des_6;
vars.p_plus_des{6} = p_plus_des_6;
vars.p_plus_des_7 = p_plus_des_7;
vars.p_plus_des{7} = p_plus_des_7;
vars.p_plus_des_8 = p_plus_des_8;
vars.p_plus_des{8} = p_plus_des_8;
vars.p_plus_des_9 = p_plus_des_9;
vars.p_plus_des{9} = p_plus_des_9;
vars.p_plus_des_10 = p_plus_des_10;
vars.p_plus_des{10} = p_plus_des_10;
vars.p_plus_des_11 = p_plus_des_11;
vars.p_plus_des{11} = p_plus_des_11;
vars.p_plus_des_12 = p_plus_des_12;
vars.p_plus_des{12} = p_plus_des_12;
vars.p_plus_des_13 = p_plus_des_13;
vars.p_plus_des{13} = p_plus_des_13;
vars.p_plus_des_14 = p_plus_des_14;
vars.p_plus_des{14} = p_plus_des_14;
vars.p_plus_des_15 = p_plus_des_15;
vars.p_plus_des{15} = p_plus_des_15;
vars.p_plus_des_16 = p_plus_des_16;
vars.p_plus_des{16} = p_plus_des_16;
vars.p_plus_des_17 = p_plus_des_17;
vars.p_plus_des{17} = p_plus_des_17;
vars.p_plus_des_18 = p_plus_des_18;
vars.p_plus_des{18} = p_plus_des_18;
vars.p_plus_des_19 = p_plus_des_19;
vars.p_plus_des{19} = p_plus_des_19;
vars.p_plus_des_20 = p_plus_des_20;
vars.p_plus_des{20} = p_plus_des_20;
vars.p_plus_des_21 = p_plus_des_21;
vars.p_plus_des{21} = p_plus_des_21;
vars.q_1 = q_1;
vars.q{1} = q_1;
vars.q_2 = q_2;
vars.q{2} = q_2;
vars.q_3 = q_3;
vars.q{3} = q_3;
vars.q_4 = q_4;
vars.q{4} = q_4;
vars.q_5 = q_5;
vars.q{5} = q_5;
vars.q_6 = q_6;
vars.q{6} = q_6;
vars.q_7 = q_7;
vars.q{7} = q_7;
vars.q_8 = q_8;
vars.q{8} = q_8;
vars.q_9 = q_9;
vars.q{9} = q_9;
vars.q_10 = q_10;
vars.q{10} = q_10;
vars.q_11 = q_11;
vars.q{11} = q_11;
vars.q_12 = q_12;
vars.q{12} = q_12;
vars.q_13 = q_13;
vars.q{13} = q_13;
vars.q_14 = q_14;
vars.q{14} = q_14;
vars.q_15 = q_15;
vars.q{15} = q_15;
vars.q_16 = q_16;
vars.q{16} = q_16;
vars.q_17 = q_17;
vars.q{17} = q_17;
vars.q_18 = q_18;
vars.q{18} = q_18;
vars.q_19 = q_19;
vars.q{19} = q_19;
vars.q_20 = q_20;
vars.q{20} = q_20;
vars.q_21 = q_21;
vars.q{21} = q_21;
vars.qdot_1 = qdot_1;
vars.qdot{1} = qdot_1;
vars.qdot_2 = qdot_2;
vars.qdot{2} = qdot_2;
vars.qdot_3 = qdot_3;
vars.qdot{3} = qdot_3;
vars.qdot_4 = qdot_4;
vars.qdot{4} = qdot_4;
vars.qdot_5 = qdot_5;
vars.qdot{5} = qdot_5;
vars.qdot_6 = qdot_6;
vars.qdot{6} = qdot_6;
vars.qdot_7 = qdot_7;
vars.qdot{7} = qdot_7;
vars.qdot_8 = qdot_8;
vars.qdot{8} = qdot_8;
vars.qdot_9 = qdot_9;
vars.qdot{9} = qdot_9;
vars.qdot_10 = qdot_10;
vars.qdot{10} = qdot_10;
vars.qdot_11 = qdot_11;
vars.qdot{11} = qdot_11;
vars.qdot_12 = qdot_12;
vars.qdot{12} = qdot_12;
vars.qdot_13 = qdot_13;
vars.qdot{13} = qdot_13;
vars.qdot_14 = qdot_14;
vars.qdot{14} = qdot_14;
vars.qdot_15 = qdot_15;
vars.qdot{15} = qdot_15;
vars.qdot_16 = qdot_16;
vars.qdot{16} = qdot_16;
vars.qdot_17 = qdot_17;
vars.qdot{17} = qdot_17;
vars.qdot_18 = qdot_18;
vars.qdot{18} = qdot_18;
vars.qdot_19 = qdot_19;
vars.qdot{19} = qdot_19;
vars.qdot_20 = qdot_20;
vars.qdot{20} = qdot_20;
vars.qdot_21 = qdot_21;
vars.qdot{21} = qdot_21;
status.cvx_status = cvx_status;
% Provide a drop-in replacement for csolve.
status.optval = cvx_optval;
status.converged = strcmp(cvx_status, 'Solved');
