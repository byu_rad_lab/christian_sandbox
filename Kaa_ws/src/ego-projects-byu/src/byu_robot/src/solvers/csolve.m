% csolve  Solves a custom quadratic program very rapidly.
%
% [vars, status] = csolve(params, settings)
%
% solves the convex optimization problem
%
%   minimize(quad_form(q_goal - q_1 + q_int, Q) + quad_form(p_plus_t - p_plus_1, J) + quad_form(p_minus_t - p_minus_1, J) + quad_form(q_goal - q_2 + q_int, Q) + quad_form(p_plus_t - p_plus_2, J) + quad_form(p_minus_t - p_minus_2, J) + quad_form(q_goal - q_3 + q_int, Q) + quad_form(p_plus_t - p_plus_3, J) + quad_form(p_minus_t - p_minus_3, J) + quad_form(q_goal - q_4 + q_int, Q) + quad_form(p_plus_t - p_plus_4, J) + quad_form(p_minus_t - p_minus_4, J) + quad_form(q_goal - q_5 + q_int, Q) + quad_form(p_plus_t - p_plus_5, J) + quad_form(p_minus_t - p_minus_5, J) + quad_form(q_goal - q_6 + q_int, Q) + quad_form(p_plus_t - p_plus_6, J) + quad_form(p_minus_t - p_minus_6, J) + quad_form(q_goal - q_7 + q_int, Q) + quad_form(p_plus_t - p_plus_7, J) + quad_form(p_minus_t - p_minus_7, J) + quad_form(q_goal - q_8 + q_int, Q) + quad_form(p_plus_t - p_plus_8, J) + quad_form(p_minus_t - p_minus_8, J) + quad_form(q_goal - q_9 + q_int, Q) + quad_form(p_plus_t - p_plus_9, J) + quad_form(p_minus_t - p_minus_9, J) + quad_form(q_goal - q_10 + q_int, Q) + quad_form(p_plus_t - p_plus_10, J) + quad_form(p_minus_t - p_minus_10, J) + quad_form(q_goal - q_11 + q_int, Q) + quad_form(p_plus_t - p_plus_11, J) + quad_form(p_minus_t - p_minus_11, J) + quad_form(q_goal - q_12 + q_int, Q) + quad_form(p_plus_t - p_plus_12, J) + quad_form(p_minus_t - p_minus_12, J) + quad_form(q_goal - q_13 + q_int, Q) + quad_form(p_plus_t - p_plus_13, J) + quad_form(p_minus_t - p_minus_13, J) + quad_form(q_goal - q_14 + q_int, Q) + quad_form(p_plus_t - p_plus_14, J) + quad_form(p_minus_t - p_minus_14, J) + quad_form(q_goal - q_15 + q_int, Q) + quad_form(p_plus_t - p_plus_15, J) + quad_form(p_minus_t - p_minus_15, J) + quad_form(q_goal - q_16 + q_int, Q) + quad_form(p_plus_t - p_plus_16, J) + quad_form(p_minus_t - p_minus_16, J) + quad_form(q_goal - q_17 + q_int, Q) + quad_form(p_plus_t - p_plus_17, J) + quad_form(p_minus_t - p_minus_17, J) + quad_form(q_goal - q_18 + q_int, Q) + quad_form(p_plus_t - p_plus_18, J) + quad_form(p_minus_t - p_minus_18, J) + quad_form(q_goal - q_19 + q_int, Q) + quad_form(p_plus_t - p_plus_19, J) + quad_form(p_minus_t - p_minus_19, J) + quad_form(q_goal - q_20 + q_int, Q) + quad_form(p_plus_t - p_plus_20, J) + quad_form(p_minus_t - p_minus_20, J) + quad_form(p_plus_des_1 - p_plus_des_0, R) + quad_form(p_minus_des_1 - p_minus_des_0, R) + quad_form(p_plus_des_2 - p_plus_des_1, R) + quad_form(p_minus_des_2 - p_minus_des_1, R) + quad_form(p_plus_des_3 - p_plus_des_2, R) + quad_form(p_minus_des_3 - p_minus_des_2, R) + quad_form(p_plus_des_4 - p_plus_des_3, R) + quad_form(p_minus_des_4 - p_minus_des_3, R) + quad_form(p_plus_des_5 - p_plus_des_4, R) + quad_form(p_minus_des_5 - p_minus_des_4, R) + quad_form(p_plus_des_6 - p_plus_des_5, R) + quad_form(p_minus_des_6 - p_minus_des_5, R) + quad_form(p_plus_des_7 - p_plus_des_6, R) + quad_form(p_minus_des_7 - p_minus_des_6, R) + quad_form(p_plus_des_8 - p_plus_des_7, R) + quad_form(p_minus_des_8 - p_minus_des_7, R) + quad_form(p_plus_des_9 - p_plus_des_8, R) + quad_form(p_minus_des_9 - p_minus_des_8, R) + quad_form(p_plus_des_10 - p_plus_des_9, R) + quad_form(p_minus_des_10 - p_minus_des_9, R) + quad_form(p_plus_des_11 - p_plus_des_10, R) + quad_form(p_minus_des_11 - p_minus_des_10, R) + quad_form(p_plus_des_12 - p_plus_des_11, R) + quad_form(p_minus_des_12 - p_minus_des_11, R) + quad_form(p_plus_des_13 - p_plus_des_12, R) + quad_form(p_minus_des_13 - p_minus_des_12, R) + quad_form(p_plus_des_14 - p_plus_des_13, R) + quad_form(p_minus_des_14 - p_minus_des_13, R) + quad_form(p_plus_des_15 - p_plus_des_14, R) + quad_form(p_minus_des_15 - p_minus_des_14, R) + quad_form(p_plus_des_16 - p_plus_des_15, R) + quad_form(p_minus_des_16 - p_minus_des_15, R) + quad_form(p_plus_des_17 - p_plus_des_16, R) + quad_form(p_minus_des_17 - p_minus_des_16, R) + quad_form(p_plus_des_18 - p_plus_des_17, R) + quad_form(p_minus_des_18 - p_minus_des_17, R) + quad_form(p_plus_des_19 - p_plus_des_18, R) + quad_form(p_minus_des_19 - p_minus_des_18, R) + quad_form(p_plus_des_20 - p_plus_des_19, R) + quad_form(p_minus_des_20 - p_minus_des_19, R) + quad_form(p_plus_des_21 - p_plus_des_20, R) + quad_form(p_minus_des_21 - p_minus_des_20, R) + quad_form(qdot_21, S))
%   subject to
%     qdot_1 == A11*qdot_0 + A12*q_0 + A13*p_plus_0 + A14*p_minus_0 + B12*p_plus_des_1 + B13*p_minus_des_1 + qdot_dist
%     qdot_2 == A11*qdot_1 + A12*q_1 + A13*p_plus_1 + A14*p_minus_1 + B12*p_plus_des_2 + B13*p_minus_des_2 + qdot_dist
%     qdot_3 == A11*qdot_2 + A12*q_2 + A13*p_plus_2 + A14*p_minus_2 + B12*p_plus_des_3 + B13*p_minus_des_3 + qdot_dist
%     qdot_4 == A11*qdot_3 + A12*q_3 + A13*p_plus_3 + A14*p_minus_3 + B12*p_plus_des_4 + B13*p_minus_des_4 + qdot_dist
%     qdot_5 == A11*qdot_4 + A12*q_4 + A13*p_plus_4 + A14*p_minus_4 + B12*p_plus_des_5 + B13*p_minus_des_5 + qdot_dist
%     qdot_6 == A11*qdot_5 + A12*q_5 + A13*p_plus_5 + A14*p_minus_5 + B12*p_plus_des_6 + B13*p_minus_des_6 + qdot_dist
%     qdot_7 == A11*qdot_6 + A12*q_6 + A13*p_plus_6 + A14*p_minus_6 + B12*p_plus_des_7 + B13*p_minus_des_7 + qdot_dist
%     qdot_8 == A11*qdot_7 + A12*q_7 + A13*p_plus_7 + A14*p_minus_7 + B12*p_plus_des_8 + B13*p_minus_des_8 + qdot_dist
%     qdot_9 == A11*qdot_8 + A12*q_8 + A13*p_plus_8 + A14*p_minus_8 + B12*p_plus_des_9 + B13*p_minus_des_9 + qdot_dist
%     qdot_10 == A11*qdot_9 + A12*q_9 + A13*p_plus_9 + A14*p_minus_9 + B12*p_plus_des_10 + B13*p_minus_des_10 + qdot_dist
%     qdot_11 == A11*qdot_10 + A12*q_10 + A13*p_plus_10 + A14*p_minus_10 + B12*p_plus_des_11 + B13*p_minus_des_11 + qdot_dist
%     qdot_12 == A11*qdot_11 + A12*q_11 + A13*p_plus_11 + A14*p_minus_11 + B12*p_plus_des_12 + B13*p_minus_des_12 + qdot_dist
%     qdot_13 == A11*qdot_12 + A12*q_12 + A13*p_plus_12 + A14*p_minus_12 + B12*p_plus_des_13 + B13*p_minus_des_13 + qdot_dist
%     qdot_14 == A11*qdot_13 + A12*q_13 + A13*p_plus_13 + A14*p_minus_13 + B12*p_plus_des_14 + B13*p_minus_des_14 + qdot_dist
%     qdot_15 == A11*qdot_14 + A12*q_14 + A13*p_plus_14 + A14*p_minus_14 + B12*p_plus_des_15 + B13*p_minus_des_15 + qdot_dist
%     qdot_16 == A11*qdot_15 + A12*q_15 + A13*p_plus_15 + A14*p_minus_15 + B12*p_plus_des_16 + B13*p_minus_des_16 + qdot_dist
%     qdot_17 == A11*qdot_16 + A12*q_16 + A13*p_plus_16 + A14*p_minus_16 + B12*p_plus_des_17 + B13*p_minus_des_17 + qdot_dist
%     qdot_18 == A11*qdot_17 + A12*q_17 + A13*p_plus_17 + A14*p_minus_17 + B12*p_plus_des_18 + B13*p_minus_des_18 + qdot_dist
%     qdot_19 == A11*qdot_18 + A12*q_18 + A13*p_plus_18 + A14*p_minus_18 + B12*p_plus_des_19 + B13*p_minus_des_19 + qdot_dist
%     qdot_20 == A11*qdot_19 + A12*q_19 + A13*p_plus_19 + A14*p_minus_19 + B12*p_plus_des_20 + B13*p_minus_des_20 + qdot_dist
%     qdot_21 == A11*qdot_20 + A12*q_20 + A13*p_plus_20 + A14*p_minus_20 + B12*p_plus_des_21 + B13*p_minus_des_21 + qdot_dist
%     q_1 == A21*qdot_0 + A22*q_0 + A23*p_plus_0 + A24*p_minus_0 + B22*p_plus_des_1 + B23*p_minus_des_1 + q_dist
%     q_2 == A21*qdot_1 + A22*q_1 + A23*p_plus_1 + A24*p_minus_1 + B22*p_plus_des_2 + B23*p_minus_des_2 + q_dist
%     q_3 == A21*qdot_2 + A22*q_2 + A23*p_plus_2 + A24*p_minus_2 + B22*p_plus_des_3 + B23*p_minus_des_3 + q_dist
%     q_4 == A21*qdot_3 + A22*q_3 + A23*p_plus_3 + A24*p_minus_3 + B22*p_plus_des_4 + B23*p_minus_des_4 + q_dist
%     q_5 == A21*qdot_4 + A22*q_4 + A23*p_plus_4 + A24*p_minus_4 + B22*p_plus_des_5 + B23*p_minus_des_5 + q_dist
%     q_6 == A21*qdot_5 + A22*q_5 + A23*p_plus_5 + A24*p_minus_5 + B22*p_plus_des_6 + B23*p_minus_des_6 + q_dist
%     q_7 == A21*qdot_6 + A22*q_6 + A23*p_plus_6 + A24*p_minus_6 + B22*p_plus_des_7 + B23*p_minus_des_7 + q_dist
%     q_8 == A21*qdot_7 + A22*q_7 + A23*p_plus_7 + A24*p_minus_7 + B22*p_plus_des_8 + B23*p_minus_des_8 + q_dist
%     q_9 == A21*qdot_8 + A22*q_8 + A23*p_plus_8 + A24*p_minus_8 + B22*p_plus_des_9 + B23*p_minus_des_9 + q_dist
%     q_10 == A21*qdot_9 + A22*q_9 + A23*p_plus_9 + A24*p_minus_9 + B22*p_plus_des_10 + B23*p_minus_des_10 + q_dist
%     q_11 == A21*qdot_10 + A22*q_10 + A23*p_plus_10 + A24*p_minus_10 + B22*p_plus_des_11 + B23*p_minus_des_11 + q_dist
%     q_12 == A21*qdot_11 + A22*q_11 + A23*p_plus_11 + A24*p_minus_11 + B22*p_plus_des_12 + B23*p_minus_des_12 + q_dist
%     q_13 == A21*qdot_12 + A22*q_12 + A23*p_plus_12 + A24*p_minus_12 + B22*p_plus_des_13 + B23*p_minus_des_13 + q_dist
%     q_14 == A21*qdot_13 + A22*q_13 + A23*p_plus_13 + A24*p_minus_13 + B22*p_plus_des_14 + B23*p_minus_des_14 + q_dist
%     q_15 == A21*qdot_14 + A22*q_14 + A23*p_plus_14 + A24*p_minus_14 + B22*p_plus_des_15 + B23*p_minus_des_15 + q_dist
%     q_16 == A21*qdot_15 + A22*q_15 + A23*p_plus_15 + A24*p_minus_15 + B22*p_plus_des_16 + B23*p_minus_des_16 + q_dist
%     q_17 == A21*qdot_16 + A22*q_16 + A23*p_plus_16 + A24*p_minus_16 + B22*p_plus_des_17 + B23*p_minus_des_17 + q_dist
%     q_18 == A21*qdot_17 + A22*q_17 + A23*p_plus_17 + A24*p_minus_17 + B22*p_plus_des_18 + B23*p_minus_des_18 + q_dist
%     q_19 == A21*qdot_18 + A22*q_18 + A23*p_plus_18 + A24*p_minus_18 + B22*p_plus_des_19 + B23*p_minus_des_19 + q_dist
%     q_20 == A21*qdot_19 + A22*q_19 + A23*p_plus_19 + A24*p_minus_19 + B22*p_plus_des_20 + B23*p_minus_des_20 + q_dist
%     q_21 == A21*qdot_20 + A22*q_20 + A23*p_plus_20 + A24*p_minus_20 + B22*p_plus_des_21 + B23*p_minus_des_21 + q_dist
%     p_plus_1 == A33*p_plus_0 + B32*p_plus_des_1
%     p_plus_2 == A33*p_plus_1 + B32*p_plus_des_2
%     p_plus_3 == A33*p_plus_2 + B32*p_plus_des_3
%     p_plus_4 == A33*p_plus_3 + B32*p_plus_des_4
%     p_plus_5 == A33*p_plus_4 + B32*p_plus_des_5
%     p_plus_6 == A33*p_plus_5 + B32*p_plus_des_6
%     p_plus_7 == A33*p_plus_6 + B32*p_plus_des_7
%     p_plus_8 == A33*p_plus_7 + B32*p_plus_des_8
%     p_plus_9 == A33*p_plus_8 + B32*p_plus_des_9
%     p_plus_10 == A33*p_plus_9 + B32*p_plus_des_10
%     p_plus_11 == A33*p_plus_10 + B32*p_plus_des_11
%     p_plus_12 == A33*p_plus_11 + B32*p_plus_des_12
%     p_plus_13 == A33*p_plus_12 + B32*p_plus_des_13
%     p_plus_14 == A33*p_plus_13 + B32*p_plus_des_14
%     p_plus_15 == A33*p_plus_14 + B32*p_plus_des_15
%     p_plus_16 == A33*p_plus_15 + B32*p_plus_des_16
%     p_plus_17 == A33*p_plus_16 + B32*p_plus_des_17
%     p_plus_18 == A33*p_plus_17 + B32*p_plus_des_18
%     p_plus_19 == A33*p_plus_18 + B32*p_plus_des_19
%     p_plus_20 == A33*p_plus_19 + B32*p_plus_des_20
%     p_plus_21 == A33*p_plus_20 + B32*p_plus_des_21
%     p_minus_1 == A44*p_minus_0 + B43*p_minus_des_1
%     p_minus_2 == A44*p_minus_1 + B43*p_minus_des_2
%     p_minus_3 == A44*p_minus_2 + B43*p_minus_des_3
%     p_minus_4 == A44*p_minus_3 + B43*p_minus_des_4
%     p_minus_5 == A44*p_minus_4 + B43*p_minus_des_5
%     p_minus_6 == A44*p_minus_5 + B43*p_minus_des_6
%     p_minus_7 == A44*p_minus_6 + B43*p_minus_des_7
%     p_minus_8 == A44*p_minus_7 + B43*p_minus_des_8
%     p_minus_9 == A44*p_minus_8 + B43*p_minus_des_9
%     p_minus_10 == A44*p_minus_9 + B43*p_minus_des_10
%     p_minus_11 == A44*p_minus_10 + B43*p_minus_des_11
%     p_minus_12 == A44*p_minus_11 + B43*p_minus_des_12
%     p_minus_13 == A44*p_minus_12 + B43*p_minus_des_13
%     p_minus_14 == A44*p_minus_13 + B43*p_minus_des_14
%     p_minus_15 == A44*p_minus_14 + B43*p_minus_des_15
%     p_minus_16 == A44*p_minus_15 + B43*p_minus_des_16
%     p_minus_17 == A44*p_minus_16 + B43*p_minus_des_17
%     p_minus_18 == A44*p_minus_17 + B43*p_minus_des_18
%     p_minus_19 == A44*p_minus_18 + B43*p_minus_des_19
%     p_minus_20 == A44*p_minus_19 + B43*p_minus_des_20
%     p_minus_21 == A44*p_minus_20 + B43*p_minus_des_21
%     p_min <= p_plus_des_1
%     p_min <= p_plus_des_2
%     p_min <= p_plus_des_3
%     p_min <= p_plus_des_4
%     p_min <= p_plus_des_5
%     p_min <= p_plus_des_6
%     p_min <= p_plus_des_7
%     p_min <= p_plus_des_8
%     p_min <= p_plus_des_9
%     p_min <= p_plus_des_10
%     p_min <= p_plus_des_11
%     p_min <= p_plus_des_12
%     p_min <= p_plus_des_13
%     p_min <= p_plus_des_14
%     p_min <= p_plus_des_15
%     p_min <= p_plus_des_16
%     p_min <= p_plus_des_17
%     p_min <= p_plus_des_18
%     p_min <= p_plus_des_19
%     p_min <= p_plus_des_20
%     p_min <= p_plus_des_21
%     p_plus_des_1 <= p_max
%     p_plus_des_2 <= p_max
%     p_plus_des_3 <= p_max
%     p_plus_des_4 <= p_max
%     p_plus_des_5 <= p_max
%     p_plus_des_6 <= p_max
%     p_plus_des_7 <= p_max
%     p_plus_des_8 <= p_max
%     p_plus_des_9 <= p_max
%     p_plus_des_10 <= p_max
%     p_plus_des_11 <= p_max
%     p_plus_des_12 <= p_max
%     p_plus_des_13 <= p_max
%     p_plus_des_14 <= p_max
%     p_plus_des_15 <= p_max
%     p_plus_des_16 <= p_max
%     p_plus_des_17 <= p_max
%     p_plus_des_18 <= p_max
%     p_plus_des_19 <= p_max
%     p_plus_des_20 <= p_max
%     p_plus_des_21 <= p_max
%     p_min <= p_minus_des_1
%     p_min <= p_minus_des_2
%     p_min <= p_minus_des_3
%     p_min <= p_minus_des_4
%     p_min <= p_minus_des_5
%     p_min <= p_minus_des_6
%     p_min <= p_minus_des_7
%     p_min <= p_minus_des_8
%     p_min <= p_minus_des_9
%     p_min <= p_minus_des_10
%     p_min <= p_minus_des_11
%     p_min <= p_minus_des_12
%     p_min <= p_minus_des_13
%     p_min <= p_minus_des_14
%     p_min <= p_minus_des_15
%     p_min <= p_minus_des_16
%     p_min <= p_minus_des_17
%     p_min <= p_minus_des_18
%     p_min <= p_minus_des_19
%     p_min <= p_minus_des_20
%     p_min <= p_minus_des_21
%     p_minus_des_1 <= p_max
%     p_minus_des_2 <= p_max
%     p_minus_des_3 <= p_max
%     p_minus_des_4 <= p_max
%     p_minus_des_5 <= p_max
%     p_minus_des_6 <= p_max
%     p_minus_des_7 <= p_max
%     p_minus_des_8 <= p_max
%     p_minus_des_9 <= p_max
%     p_minus_des_10 <= p_max
%     p_minus_des_11 <= p_max
%     p_minus_des_12 <= p_max
%     p_minus_des_13 <= p_max
%     p_minus_des_14 <= p_max
%     p_minus_des_15 <= p_max
%     p_minus_des_16 <= p_max
%     p_minus_des_17 <= p_max
%     p_minus_des_18 <= p_max
%     p_minus_des_19 <= p_max
%     p_minus_des_20 <= p_max
%     p_minus_des_21 <= p_max
%
% with variables
% p_minus_1   1 x 1
% p_minus_2   1 x 1
% p_minus_3   1 x 1
% p_minus_4   1 x 1
% p_minus_5   1 x 1
% p_minus_6   1 x 1
% p_minus_7   1 x 1
% p_minus_8   1 x 1
% p_minus_9   1 x 1
% p_minus_10   1 x 1
% p_minus_11   1 x 1
% p_minus_12   1 x 1
% p_minus_13   1 x 1
% p_minus_14   1 x 1
% p_minus_15   1 x 1
% p_minus_16   1 x 1
% p_minus_17   1 x 1
% p_minus_18   1 x 1
% p_minus_19   1 x 1
% p_minus_20   1 x 1
% p_minus_21   1 x 1
% p_minus_des_1   1 x 1
% p_minus_des_2   1 x 1
% p_minus_des_3   1 x 1
% p_minus_des_4   1 x 1
% p_minus_des_5   1 x 1
% p_minus_des_6   1 x 1
% p_minus_des_7   1 x 1
% p_minus_des_8   1 x 1
% p_minus_des_9   1 x 1
% p_minus_des_10   1 x 1
% p_minus_des_11   1 x 1
% p_minus_des_12   1 x 1
% p_minus_des_13   1 x 1
% p_minus_des_14   1 x 1
% p_minus_des_15   1 x 1
% p_minus_des_16   1 x 1
% p_minus_des_17   1 x 1
% p_minus_des_18   1 x 1
% p_minus_des_19   1 x 1
% p_minus_des_20   1 x 1
% p_minus_des_21   1 x 1
% p_plus_1   1 x 1
% p_plus_2   1 x 1
% p_plus_3   1 x 1
% p_plus_4   1 x 1
% p_plus_5   1 x 1
% p_plus_6   1 x 1
% p_plus_7   1 x 1
% p_plus_8   1 x 1
% p_plus_9   1 x 1
% p_plus_10   1 x 1
% p_plus_11   1 x 1
% p_plus_12   1 x 1
% p_plus_13   1 x 1
% p_plus_14   1 x 1
% p_plus_15   1 x 1
% p_plus_16   1 x 1
% p_plus_17   1 x 1
% p_plus_18   1 x 1
% p_plus_19   1 x 1
% p_plus_20   1 x 1
% p_plus_21   1 x 1
% p_plus_des_1   1 x 1
% p_plus_des_2   1 x 1
% p_plus_des_3   1 x 1
% p_plus_des_4   1 x 1
% p_plus_des_5   1 x 1
% p_plus_des_6   1 x 1
% p_plus_des_7   1 x 1
% p_plus_des_8   1 x 1
% p_plus_des_9   1 x 1
% p_plus_des_10   1 x 1
% p_plus_des_11   1 x 1
% p_plus_des_12   1 x 1
% p_plus_des_13   1 x 1
% p_plus_des_14   1 x 1
% p_plus_des_15   1 x 1
% p_plus_des_16   1 x 1
% p_plus_des_17   1 x 1
% p_plus_des_18   1 x 1
% p_plus_des_19   1 x 1
% p_plus_des_20   1 x 1
% p_plus_des_21   1 x 1
%      q_1   1 x 1
%      q_2   1 x 1
%      q_3   1 x 1
%      q_4   1 x 1
%      q_5   1 x 1
%      q_6   1 x 1
%      q_7   1 x 1
%      q_8   1 x 1
%      q_9   1 x 1
%     q_10   1 x 1
%     q_11   1 x 1
%     q_12   1 x 1
%     q_13   1 x 1
%     q_14   1 x 1
%     q_15   1 x 1
%     q_16   1 x 1
%     q_17   1 x 1
%     q_18   1 x 1
%     q_19   1 x 1
%     q_20   1 x 1
%     q_21   1 x 1
%   qdot_1   1 x 1
%   qdot_2   1 x 1
%   qdot_3   1 x 1
%   qdot_4   1 x 1
%   qdot_5   1 x 1
%   qdot_6   1 x 1
%   qdot_7   1 x 1
%   qdot_8   1 x 1
%   qdot_9   1 x 1
%  qdot_10   1 x 1
%  qdot_11   1 x 1
%  qdot_12   1 x 1
%  qdot_13   1 x 1
%  qdot_14   1 x 1
%  qdot_15   1 x 1
%  qdot_16   1 x 1
%  qdot_17   1 x 1
%  qdot_18   1 x 1
%  qdot_19   1 x 1
%  qdot_20   1 x 1
%  qdot_21   1 x 1
%
% and parameters
%      A11   1 x 1
%      A12   1 x 1
%      A13   1 x 1
%      A14   1 x 1
%      A21   1 x 1
%      A22   1 x 1
%      A23   1 x 1
%      A24   1 x 1
%      A33   1 x 1
%      A44   1 x 1
%      B12   1 x 1
%      B13   1 x 1
%      B22   1 x 1
%      B23   1 x 1
%      B32   1 x 1
%      B43   1 x 1
%        J   1 x 1    PSD
%        Q   1 x 1    PSD
%        R   1 x 1    PSD
%        S   1 x 1    PSD
%    p_max   1 x 1
%    p_min   1 x 1
% p_minus_0   1 x 1
% p_minus_des_0   1 x 1
% p_minus_t   1 x 1
% p_plus_0   1 x 1
% p_plus_des_0   1 x 1
% p_plus_t   1 x 1
%      q_0   1 x 1
%   q_dist   1 x 1
%   q_goal   1 x 1
%    q_int   1 x 1
%   qdot_0   1 x 1
% qdot_dist   1 x 1
%
% Note:
%   - Check status.converged, which will be 1 if optimization succeeded.
%   - You don't have to specify settings if you don't want to.
%   - To hide output, use settings.verbose = 0.
%   - To change iterations, use settings.max_iters = 20.
%   - You may wish to compare with cvxsolve to check the solver is correct.
%
% Specify params.A11, ..., params.qdot_dist, then run
%   [vars, status] = csolve(params, settings)
% Produced by CVXGEN, 2017-05-23 15:26:17 -0400.
% CVXGEN is Copyright (C) 2006-2012 Jacob Mattingley, jem@cvxgen.com.
% The code in this file is Copyright (C) 2006-2012 Jacob Mattingley.
% CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial
% applications without prior written permission from Jacob Mattingley.

% Filename: csolve.m.
% Description: Help file for the Matlab solver interface.
