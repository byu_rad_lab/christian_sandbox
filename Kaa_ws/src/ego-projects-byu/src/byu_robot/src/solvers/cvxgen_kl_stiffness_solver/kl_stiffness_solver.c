#include<boost/python.hpp>
#include "solver.h"
#include <iostream>

Vars vars;
Params params;
Workspace work;
Settings settings;

int flags = 0;

boost::python::list runController(double q_0,
				  double qdot_0,
				  double p_plus_0,
				  double p_minus_0,
				  double k,
				  double kdot,
				  double p_plus_des_0,
				  double p_minus_des_0,
				  double A11,
				  double A12,
				  double A13,
				  double A14,
				  double A21,
				  double A22,
				  double A23,
				  double A24,
				  double A33,
				  double A44,
				  double A51,
				  double A52,
				  double A53,
				  double A54,
				  double A55,
				  double B12,
				  double B13,
				  double B22,
				  double B23,
				  double B32,
				  double B43,
                  		  double Qpos,
				  double Qpres,
				  double Qstiff,
				  double Qsdot,
				  double Qvel,
				  double q_goal,
				  double k_goal,
				  double p_max,
				  double p_min,
				  double q_max,
				  double q_min,
				  double dt,
				  double qdot_dist,
				  double q_dist,
				  double qInt)
{
  if (flags == 0)
    {
      set_defaults();
      setup_indexing();
      flags = 1;
      settings.verbose = 0;
      settings.eps = 9.5*10e-4; // this could be changed to e-6 to be more accurate if necessary
    }

  // define the optimization parameters.

  int i;
  //int num_jts;

  // num_jts = boost::python::len(q_0);
  //std::cout << "Number of Joints : " << num_jts << std::endl;

  /** for(i=0; i< boost::python::len(p_des_0); ++i)
    {
      std::cout << "p_des_0 : " << boost::python::extract<double>(p_des_0[i]) << std::endl;
      std::cout << i << std::endl;
      }**/

  //for (i = 0; i < boost::python::len(q_0); i++)
  // {
      //std::cout << " i : " << i << std::endl;
      //std::cout << " i+num_jts : " << i+num_jts << std::endl;
  //  std::cout << "Got Here" << std::endl;
  i = 0;
  params.q_0[i] = q_0;

  params.qdot_0[i] = qdot_0;
  params.p_plus_0[i] = p_plus_0;
  params.p_minus_0[i] = p_minus_0;
  params.k_0[i] = k;
  params.kdot[i] = kdot;
  params.p_plus_des_0[i] = p_plus_des_0;
  params.p_minus_des_0[i] = p_minus_des_0;
  //}




  //for (i = 0; i < boost::python::len(A11); i++)
  // {
  params.A11[i] = A11;
  params.A12[i] = A12;
  params.A13[i] = A13;
  params.A14[i] = A14;
  params.A21[i] = A21;
  params.A22[i] = A22;
  params.A23[i] = A23;
  params.A24[i] = A24;
      //params.A31[i] = boost::python::extract<double>(A31[i]);
      //params.A32[i] = boost::python::extract<double>(A32[i]);
  params.A33[i] = A33;
      //params.A34[i] = boost::python::extract<double>(A34[i]);
      //params.A41[i] = boost::python::extract<double>(A41[i]);
      //params.A42[i] = boost::python::extract<double>(A42[i]);
      //params.A43[i] = boost::python::extract<double>(A43[i]);
  params.A44[i] = A44;
  params.A51[i] = A51;
  params.A52[i] = A52;
  params.A53[i] = A53;
  params.A54[i] = A54;
  params.A55[i] = A55;
  params.B12[i] = B12;
  params.B13[i] = B13;
      //params.B21[i] = boost::python::extract<double>(B21[i]);
  params.B22[i] = B22;
  params.B23[i] = B23;
      //params.B31[i] = boost::python::extract<double>(B31[i]);
  params.B32[i] = B32;
      //params.B41[i] = boost::python::extract<double>(B41[i]);
  params.B43[i] = B43;
  params.Qpos[i] = Qpos;
  params.Qpres[i] = Qpres;
  params.Qstiff[i] = Qstiff;
  params.Qsdot[i] = Qsdot;
  params.Qvel[i] = Qvel;
  //}
  
 


  //for (i= 0; i< boost::python::len(q_goal); i++)
  //  {
  params.q_goal[i] = q_goal;
  params.k_goal[i] = k_goal;
  /* params.p_plus_t[i] = p_plus_t; */
  /* params.p_minus_t[i] = p_minus_t; */
  params.p_max[i] = p_max;
  params.p_min[i] = p_min;
  params.q_max[i] = q_max;
  params.q_min[i] = q_min;
      //std::cout << i << std::endl;
      //std::cout << "p_delta : " << boost::python::extract<double>(p_delta[i]) << std::endl;
      //      params.p_delta[i] = boost::python::extract<double>(p_delta[i]);
  //}

  //for(i=0; i< boost::python::len(p_delta); ++i)
  //  {
  /* params.p_delta[i] = p_delta; */
  params.dt[i] = dt;
  params.qdot_dist[i] = qdot_dist;
  params.q_dist[i] = q_dist;
      //  }



  solve();

  //  boost::python::list P_cmd;
  //  boost::python::list all_data;

  //  boost::python::list P0cmd;
  //  boost::python::list P1cmd;
  //  boost::python::list theta_dot;
  //  boost::python::list theta;
  //  boost::python::list P_0;
  //  boost::python::list P_1;

  // this code was used to return the entire input vector instead of just the first time step
  // this was for debugging purposes and can be done again if this code is updated.
  /* if (work.converged == 1) */
  /*   { */
  /*     int k; */
  /*     for (k = 0; k < horizon_size; k++) */
  /* 	{ */
  /* 	  boost::python::list buff; */
  /* 	  for (i = 0; i< boost::python::len(n_inputs); i++) */
  /* 	    { */
  /* 	      buff.append(vars.q_des[k][i]); // do we need this [0]? */
  /* 	    } */
  /* 	  q_des.append(buff); */
  /* 	} */
  /*   } */

  
  boost::python::list p_plus_cmd;
  boost::python::list p_minus_cmd;
  boost::python::list q;
  boost::python::list qdot;
  boost::python::list p_plus;
  boost::python::list p_minus;
  boost::python::list k;
  
  std::cout << "converged status : " << work.converged << std::endl;
 
  //std::cout << "q_plus_des : " << vars.p_plus_des_1[0] << std::endl;
  

  if (work.converged == 1)
  {


      double buff_p_plus_cmd;
      double buff_p_minus_cmd;
      double buff_q;
      double buff_qdot;
      double buff_p_plus;
      double buff_p_minus;
      double buff_k;

     
      //std::cout << "Got Here " <<  std::endl;
      p_plus_cmd.append(vars.p_plus_des_1[0]);

      //      p_plus_cmd.append(vars.p_plus_des_1[1]);
      //      p_plus_cmd.append(vars.p_plus_des_1[2]);
      p_plus_cmd.append(vars.p_plus_des_2[0]);

      //      p_plus_cmd.append(vars.p_plus_des_2[1]);
      //      p_plus_cmd.append(vars.p_plus_des_2[2]);
      p_plus_cmd.append(vars.p_plus_des_3[0]);
      //      p_plus_cmd.append(vars.p_plus_des_3[1]);
      //      p_plus_cmd.append(vars.p_plus_des_3[2]);
      p_plus_cmd.append(vars.p_plus_des_4[0]);
      //      p_plus_cmd.append(vars.p_plus_des_4[1]);
      //      p_plus_cmd.append(vars.p_plus_des_4[2]);
      p_plus_cmd.append(vars.p_plus_des_5[0]);
      //      p_plus_cmd.append(vars.p_plus_des_5[1]);
      //      p_plus_cmd.append(vars.p_plus_des_5[2]);
      p_plus_cmd.append(vars.p_plus_des_6[0]);
      //      p_plus_cmd.append(vars.p_plus_des_6[1]);
      //      p_plus_cmd.append(vars.p_plus_des_6[2]);
      p_plus_cmd.append(vars.p_plus_des_7[0]);
      //      p_plus_cmd.append(vars.p_plus_des_7[1]);
      //      p_plus_cmd.append(vars.p_plus_des_7[2]);
      p_plus_cmd.append(vars.p_plus_des_8[0]);
      //      p_plus_cmd.append(vars.p_plus_des_8[1]);
      //      p_plus_cmd.append(vars.p_plus_des_8[2]);
      p_plus_cmd.append(vars.p_plus_des_9[0]);
      //      p_plus_cmd.append(vars.p_plus_des_9[1]);
      //      p_plus_cmd.append(vars.p_plus_des_9[2]);

      /**
      p_plus_cmd.append(vars.p_plus_des_10[0]);
      p_plus_cmd.append(vars.p_plus_des_10[1]);
      p_plus_cmd.append(vars.p_plus_des_10[2]);
      p_plus_cmd.append(vars.p_plus_des_11[0]);
      p_plus_cmd.append(vars.p_plus_des_11[1]);
      p_plus_cmd.append(vars.p_plus_des_11[2]);
      p_plus_cmd.append(vars.p_plus_des_12[0]);
      p_plus_cmd.append(vars.p_plus_des_12[1]);
      p_plus_cmd.append(vars.p_plus_des_12[2]);
      p_plus_cmd.append(vars.p_plus_des_13[0]);
      p_plus_cmd.append(vars.p_plus_des_13[1]);
      p_plus_cmd.append(vars.p_plus_des_13[2]);
      p_plus_cmd.append(vars.p_plus_des_14[0]);
      p_plus_cmd.append(vars.p_plus_des_14[1]);
      p_plus_cmd.append(vars.p_plus_des_14[2]);
      p_plus_cmd.append(vars.p_plus_des_15[0]);
      p_plus_cmd.append(vars.p_plus_des_15[1]);
      p_plus_cmd.append(vars.p_plus_des_15[2]);
      **/
      //std::cout << "Got Here 0 : "  << std::endl;

      p_minus_cmd.append(vars.p_minus_des_1[0]);
      //      p_minus_cmd.append(vars.p_minus_des_1[1]);
      //      p_minus_cmd.append(vars.p_minus_des_1[2]);
      p_minus_cmd.append(vars.p_minus_des_2[0]);
      //      p_minus_cmd.append(vars.p_minus_des_2[1]);
      //      p_minus_cmd.append(vars.p_minus_des_2[2]);
      p_minus_cmd.append(vars.p_minus_des_3[0]);
      //      p_minus_cmd.append(vars.p_minus_des_3[1]);
      //      p_minus_cmd.append(vars.p_minus_des_3[2]);
      p_minus_cmd.append(vars.p_minus_des_4[0]);
      //      p_minus_cmd.append(vars.p_minus_des_4[1]);
      //      p_minus_cmd.append(vars.p_minus_des_4[2]);
      p_minus_cmd.append(vars.p_minus_des_5[0]);
      //      p_minus_cmd.append(vars.p_minus_des_5[1]);
      //      p_minus_cmd.append(vars.p_minus_des_5[2]);
      p_minus_cmd.append(vars.p_minus_des_6[0]);
      //      p_minus_cmd.append(vars.p_minus_des_6[1]);
      //      p_minus_cmd.append(vars.p_minus_des_6[2]);
      p_minus_cmd.append(vars.p_minus_des_7[0]);
      //      p_minus_cmd.append(vars.p_minus_des_7[1]);
      //      p_minus_cmd.append(vars.p_minus_des_7[2]);
      p_minus_cmd.append(vars.p_minus_des_8[0]);
      //      p_minus_cmd.append(vars.p_minus_des_8[1]);
      //      p_minus_cmd.append(vars.p_minus_des_8[2]);
      p_minus_cmd.append(vars.p_minus_des_9[0]);
      //      p_minus_cmd.append(vars.p_minus_des_9[1]);
      //      p_minus_cmd.append(vars.p_minus_des_9[2]);
      /**
      p_minus_cmd.append(vars.p_minus_des_10[0]);
      p_minus_cmd.append(vars.p_minus_des_10[1]);
      p_minus_cmd.append(vars.p_minus_des_10[2]);
      p_minus_cmd.append(vars.p_minus_des_11[0]);
      p_minus_cmd.append(vars.p_minus_des_11[1]);
      p_minus_cmd.append(vars.p_minus_des_11[2]);
      p_minus_cmd.append(vars.p_minus_des_12[0]);
      p_minus_cmd.append(vars.p_minus_des_12[1]);
      p_minus_cmd.append(vars.p_minus_des_12[2]);
      p_minus_cmd.append(vars.p_minus_des_13[0]);
      p_minus_cmd.append(vars.p_minus_des_13[1]);
      p_minus_cmd.append(vars.p_minus_des_13[2]);
      p_minus_cmd.append(vars.p_minus_des_14[0]);
      p_minus_cmd.append(vars.p_minus_des_14[1]);
      p_minus_cmd.append(vars.p_minus_des_14[2]);
      p_minus_cmd.append(vars.p_minus_des_15[0]);
      p_minus_cmd.append(vars.p_minus_des_15[1]);
      p_minus_cmd.append(vars.p_minus_des_15[2]);
      **/
      //std::cout << "Got Here 1 : "  << std::endl;

      q.append(vars.q_1[0]);
      //      q.append(vars.q_1[1]);
      //      q.append(vars.q_1[2]);
      q.append(vars.q_2[0]);
      //      q.append(vars.q_2[1]);
      //      q.append(vars.q_2[2]);
      q.append(vars.q_3[0]);
      //      q.append(vars.q_3[1]);
      //      q.append(vars.q_3[2]);
      q.append(vars.q_4[0]);
      //      q.append(vars.q_4[1]);
      //      q.append(vars.q_4[2]);
      q.append(vars.q_5[0]);
      //      q.append(vars.q_5[1]);
      //      q.append(vars.q_5[2]);
      q.append(vars.q_6[0]);
      //      q.append(vars.q_6[1]);
      //      q.append(vars.q_6[2]);
      q.append(vars.q_7[0]);
      //      q.append(vars.q_7[1]);
      //      q.append(vars.q_7[2]);
      q.append(vars.q_8[0]);
      //      q.append(vars.q_8[1]);
      //      q.append(vars.q_8[2]);
      q.append(vars.q_9[0]);
      //      q.append(vars.q_9[1]);
      //      q.append(vars.q_9[2]);
      /**
      q.append(vars.q_10[0]);
      q.append(vars.q_10[1]);
      q.append(vars.q_10[2]);
      q.append(vars.q_11[0]);
      q.append(vars.q_11[1]);
      q.append(vars.q_11[2]);
      q.append(vars.q_12[0]);
      q.append(vars.q_12[1]);
      q.append(vars.q_12[2]);
      q.append(vars.q_13[0]);
      q.append(vars.q_13[1]);
      q.append(vars.q_13[2]);
      q.append(vars.q_14[0]);
      q.append(vars.q_14[1]);
      q.append(vars.q_14[2]);
      q.append(vars.q_15[0]);
      q.append(vars.q_15[1]);
      q.append(vars.q_15[2]);
      **/
      //std::cout << "Got Here 2 : "  << std::endl;

      qdot.append(vars.qdot_1[0]);
      //      qdot.append(vars.qdot_1[1]);
      //      qdot.append(vars.qdot_1[2]);
      qdot.append(vars.qdot_2[0]);
      //      qdot.append(vars.qdot_2[1]);
      //      qdot.append(vars.qdot_2[2]);
      qdot.append(vars.qdot_3[0]);
      //      qdot.append(vars.qdot_3[1]);
      //      qdot.append(vars.qdot_3[2]);
      qdot.append(vars.qdot_4[0]);
      //      qdot.append(vars.qdot_4[1]);
      //      qdot.append(vars.qdot_4[2]);
      qdot.append(vars.qdot_5[0]);
      //      qdot.append(vars.qdot_5[1]);
      //      qdot.append(vars.qdot_5[2]);
      qdot.append(vars.qdot_6[0]);
      //      qdot.append(vars.qdot_6[1]);
      //      qdot.append(vars.qdot_6[2]);
      qdot.append(vars.qdot_7[0]);
      //      qdot.append(vars.qdot_7[1]);
      //      qdot.append(vars.qdot_7[2]);
      qdot.append(vars.qdot_8[0]);
      //      qdot.append(vars.qdot_8[1]);
      //      qdot.append(vars.qdot_8[2]);
      qdot.append(vars.qdot_9[0]);
      //      qdot.append(vars.qdot_9[1]);
      //      qdot.append(vars.qdot_9[2]);
      
      k.append(vars.k_1[0])
      k.append(vars.k_2[0])
      k.append(vars.k_3[0])
      k.append(vars.k_4[0])
      k.append(vars.k_5[0])
      k.append(vars.k_6[0])
      k.append(vars.k_7[0])
      k.append(vars.k_8[0])
      k.append(vars.k_9[0])

      /**
      qdot.append(vars.qdot_10[0]);
      qdot.append(vars.qdot_10[1]);
      qdot.append(vars.qdot_10[2]);
      qdot.append(vars.qdot_11[0]);
      qdot.append(vars.qdot_11[1]);
      qdot.append(vars.qdot_11[2]);
      qdot.append(vars.qdot_12[0]);
      qdot.append(vars.qdot_12[1]);
      qdot.append(vars.qdot_12[2]);
      qdot.append(vars.qdot_13[0]);
      qdot.append(vars.qdot_13[1]);
      qdot.append(vars.qdot_13[2]);
      qdot.append(vars.qdot_14[0]);
      qdot.append(vars.qdot_14[1]);
      qdot.append(vars.qdot_14[2]);
      qdot.append(vars.qdot_15[0]);
      qdot.append(vars.qdot_15[1]);
      qdot.append(vars.qdot_15[2]);
      **/
      //std::cout << "Got Here 3 : "  << std::endl;
      p_plus.append(vars.p_plus_1[0]);
      //      p_plus.append(vars.p_plus_1[1]);
      //      p_plus.append(vars.p_plus_1[2]);
      p_plus.append(vars.p_plus_2[0]);
      //      p_plus.append(vars.p_plus_2[1]);
      //      p_plus.append(vars.p_plus_2[2]);
      p_plus.append(vars.p_plus_3[0]);
      //      p_plus.append(vars.p_plus_3[1]);
      //      p_plus.append(vars.p_plus_3[2]);
      p_plus.append(vars.p_plus_4[0]);
      //      p_plus.append(vars.p_plus_4[1]);
      //      p_plus.append(vars.p_plus_4[2]);
      p_plus.append(vars.p_plus_5[0]);
      //      p_plus.append(vars.p_plus_5[1]);
      //      p_plus.append(vars.p_plus_5[2]);
      p_plus.append(vars.p_plus_6[0]);
      //      p_plus.append(vars.p_plus_6[1]);
      //      p_plus.append(vars.p_plus_6[2]);
      p_plus.append(vars.p_plus_7[0]);
      //      p_plus.append(vars.p_plus_7[1]);
      //      p_plus.append(vars.p_plus_7[2]);
      p_plus.append(vars.p_plus_8[0]);
      //      p_plus.append(vars.p_plus_8[1]);
      //      p_plus.append(vars.p_plus_8[2]);
      p_plus.append(vars.p_plus_9[0]);
      //      p_plus.append(vars.p_plus_9[1]);
      //      p_plus.append(vars.p_plus_9[2]);

      /**
      p_plus.append(vars.p_plus_10[0]);
      p_plus.append(vars.p_plus_10[1]);
      p_plus.append(vars.p_plus_10[2]);
      p_plus.append(vars.p_plus_11[0]);
      p_plus.append(vars.p_plus_11[1]);
      p_plus.append(vars.p_plus_11[2]);
      p_plus.append(vars.p_plus_12[0]);
      p_plus.append(vars.p_plus_12[1]);
      p_plus.append(vars.p_plus_12[2]);
      p_plus.append(vars.p_plus_13[0]);
      p_plus.append(vars.p_plus_13[1]);
      p_plus.append(vars.p_plus_13[2]);
      p_plus.append(vars.p_plus_14[0]);
      p_plus.append(vars.p_plus_14[1]);
      p_plus.append(vars.p_plus_14[2]);
      p_plus.append(vars.p_plus_15[0]);
      p_plus.append(vars.p_plus_15[1]);
      p_plus.append(vars.p_plus_15[2]);
      **/
      //std::cout << "Got Here 4 : "  << std::endl;
      p_minus.append(vars.p_minus_1[0]);
      //      p_minus.append(vars.p_minus_1[1]);
      //      p_minus.append(vars.p_minus_1[2]);
      p_minus.append(vars.p_minus_2[0]);
      //      p_minus.append(vars.p_minus_2[1]);
      //      p_minus.append(vars.p_minus_2[2]);
      p_minus.append(vars.p_minus_3[0]);
      //      p_minus.append(vars.p_minus_3[1]);
      //      p_minus.append(vars.p_minus_3[2]);
      p_minus.append(vars.p_minus_4[0]);
      //      p_minus.append(vars.p_minus_4[1]);
      //      p_minus.append(vars.p_minus_4[2]);
      p_minus.append(vars.p_minus_5[0]);
      //      p_minus.append(vars.p_minus_5[1]);
      //      p_minus.append(vars.p_minus_5[2]);
      p_minus.append(vars.p_minus_6[0]);
      //      p_minus.append(vars.p_minus_6[1]);
      //      p_minus.append(vars.p_minus_6[2]);
      p_minus.append(vars.p_minus_7[0]);
      //      p_minus.append(vars.p_minus_7[1]);
      //      p_minus.append(vars.p_minus_7[2]);
      p_minus.append(vars.p_minus_8[0]);
      //      p_minus.append(vars.p_minus_8[1]);
      //      p_minus.append(vars.p_minus_8[2]);
      p_minus.append(vars.p_minus_9[0]);
      //      p_minus.append(vars.p_minus_9[1]);
      //      p_minus.append(vars.p_minus_9[2]);
      /**
      p_minus.append(vars.p_minus_10[0]);
      p_minus.append(vars.p_minus_10[1]);
      p_minus.append(vars.p_minus_10[2]);
      p_minus.append(vars.p_minus_11[0]);
      p_minus.append(vars.p_minus_11[1]);
      p_minus.append(vars.p_minus_11[2]);
      p_minus.append(vars.p_minus_12[0]);
      p_minus.append(vars.p_minus_12[1]);
      p_minus.append(vars.p_minus_12[2]);
      p_minus.append(vars.p_minus_13[0]);
      p_minus.append(vars.p_minus_13[1]);
      p_minus.append(vars.p_minus_13[2]);
      p_minus.append(vars.p_minus_14[0]);
      p_minus.append(vars.p_minus_14[1]);
      p_minus.append(vars.p_minus_14[2]);
      p_minus.append(vars.p_minus_15[0]);
      p_minus.append(vars.p_minus_15[1]);
      p_minus.append(vars.p_minus_15[2]);
      **/

	
      
      //p_plus_cmd.append(buff_p_plus_cmd);
      //p_minus_cmd.append(buff_p_minus_cmd);
      //q.append(buff_q);
      //qdot.append(buff_qdot);
      //p_plus.append(buff_p_plus);
      //p_minus.append(buff_p_minus);
      //std::cout << "Got Here end : "  << std::endl;    
      
  }
  else
  {
    double buff_plus;
    double buff_minus;
    //for (i=0; i< num_jts; i++)
    //{
      //      val_plus = boost::python::extract<double>(p_plus_d_0)
    p_plus_cmd.append(params.p_plus_des_0[i]);
    p_minus_cmd.append(params.p_minus_des_0[i]);
    //}
    //p_plus_cmd.append(buff_plus);
    //p_minus_cmd.append(buff_minus);
  }

  boost::python::list p_cmd;

  p_cmd.append(p_plus_cmd);
  p_cmd.append(p_minus_cmd);
  //  p_cmd.append(q);
  //  p_cmd.append(qdot);
  //  p_cmd.append(p_plus);
  //  p_cmd.append(p_minus);
  
    //  boost::python::list p_cmd;
    //p_cmd.append(10.)
  return p_cmd;


}

char const* greet()
{
  printf("got into greet");
  return "hello, world (and you)";
}

// this is what tells boost what the function names and definitions should be (DON'T DELETE)
BOOST_PYTHON_MODULE(kl_stiffness_solver)
{
  using namespace boost::python;
  def("greet", greet);
  def("runController", runController);
}
