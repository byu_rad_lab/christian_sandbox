/* Produced by CVXGEN, 2017-05-23 15:26:45 -0400.  */
/* CVXGEN is Copyright (C) 2006-2012 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2012 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: testsolver.c. */
/* Description: Basic test harness for solver.c. */
#include "solver.h"
Vars vars;
Params params;
Workspace work;
Settings settings;
#define NUMTESTS 0
int main(int argc, char **argv) {
  int num_iters;
#if (NUMTESTS > 0)
  int i;
  double time;
  double time_per;
#endif
  set_defaults();
  setup_indexing();
  load_default_data();
  /* Solve problem instance for the record. */
  settings.verbose = 1;
  num_iters = solve();
#ifndef ZERO_LIBRARY_MODE
#if (NUMTESTS > 0)
  /* Now solve multiple problem instances for timing purposes. */
  settings.verbose = 0;
  tic();
  for (i = 0; i < NUMTESTS; i++) {
    solve();
  }
  time = tocq();
  printf("Timed %d solves over %.3f seconds.\n", NUMTESTS, time);
  time_per = time / NUMTESTS;
  if (time_per > 1) {
    printf("Actual time taken per solve: %.3g s.\n", time_per);
  } else if (time_per > 1e-3) {
    printf("Actual time taken per solve: %.3g ms.\n", 1e3*time_per);
  } else {
    printf("Actual time taken per solve: %.3g us.\n", 1e6*time_per);
  }
#endif
#endif
  return 0;
}
void load_default_data(void) {
  params.q_goal[0] = 0.20319161029830202;
  params.q_int[0] = 0.8325912904724193;
  /* Make this a diagonal PSD matrix, even though it's not diagonal. */
  params.Q[0] = 1.2909047389129444;
  params.p_plus_t[0] = 0.04331042079065206;
  /* Make this a diagonal PSD matrix, even though it's not diagonal. */
  params.J[0] = 1.8929469543476547;
  params.p_minus_t[0] = 1.5851723557337523;
  params.p_plus_des_0[0] = -1.497658758144655;
  /* Make this a diagonal PSD matrix, even though it's not diagonal. */
  params.R[0] = 1.2072428781381868;
  params.p_minus_des_0[0] = -1.7941311867966805;
  /* Make this a diagonal PSD matrix, even though it's not diagonal. */
  params.S[0] = 1.4408098436506365;
  params.A11[0] = -1.8804951564857322;
  params.qdot_0[0] = -0.17266710242115568;
  params.A12[0] = 0.596576190459043;
  params.q_0[0] = -0.8860508694080989;
  params.A13[0] = 0.7050196079205251;
  params.p_plus_0[0] = 0.3634512696654033;
  params.A14[0] = -1.9040724704913385;
  params.p_minus_0[0] = 0.23541635196352795;
  params.B12[0] = -0.9629902123701384;
  params.B13[0] = -0.3395952119597214;
  params.qdot_dist[0] = -0.865899672914725;
  params.A21[0] = 0.7725516732519853;
  params.A22[0] = -0.23818512931704205;
  params.A23[0] = -1.372529046100147;
  params.A24[0] = 0.17859607212737894;
  params.B22[0] = 1.1212590580454682;
  params.B23[0] = -0.774545870495281;
  params.q_dist[0] = -1.1121684642712744;
  params.A33[0] = -0.44811496977740495;
  params.B32[0] = 1.7455345994417217;
  params.A44[0] = 1.9039816898917352;
  params.B43[0] = 0.6895347036512547;
  params.p_min[0] = 1.6113364341535923;
  params.p_max[0] = 1.383003485172717;
}
