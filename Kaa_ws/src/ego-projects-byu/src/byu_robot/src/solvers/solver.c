/* Produced by CVXGEN, 2017-05-23 15:26:31 -0400.  */
/* CVXGEN is Copyright (C) 2006-2012 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2012 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: solver.c. */
/* Description: Main solver file. */
#include "solver.h"
double eval_gap(void) {
  int i;
  double gap;
  gap = 0;
  for (i = 0; i < 84; i++)
    gap += work.z[i]*work.s[i];
  return gap;
}
void set_defaults(void) {
  settings.resid_tol = 1e-6;
  settings.eps = 1e-4;
  settings.max_iters = 25;
  settings.refine_steps = 1;
  settings.s_init = 1;
  settings.z_init = 1;
  settings.debug = 0;
  settings.verbose = 1;
  settings.verbose_refinement = 0;
  settings.better_start = 1;
  settings.kkt_reg = 1e-7;
}
void setup_pointers(void) {
  work.y = work.x + 166;
  work.s = work.x + 290;
  work.z = work.x + 374;
  vars.p_minus_1 = work.x + 0;
  vars.p_minus_2 = work.x + 1;
  vars.p_minus_3 = work.x + 2;
  vars.p_minus_4 = work.x + 3;
  vars.p_minus_5 = work.x + 4;
  vars.p_minus_6 = work.x + 5;
  vars.p_minus_7 = work.x + 6;
  vars.p_minus_8 = work.x + 7;
  vars.p_minus_9 = work.x + 8;
  vars.p_minus_10 = work.x + 9;
  vars.p_minus_11 = work.x + 10;
  vars.p_minus_12 = work.x + 11;
  vars.p_minus_13 = work.x + 12;
  vars.p_minus_14 = work.x + 13;
  vars.p_minus_15 = work.x + 14;
  vars.p_minus_16 = work.x + 15;
  vars.p_minus_17 = work.x + 16;
  vars.p_minus_18 = work.x + 17;
  vars.p_minus_19 = work.x + 18;
  vars.p_minus_20 = work.x + 19;
  vars.p_minus_21 = work.x + 20;
  vars.p_minus_des_1 = work.x + 21;
  vars.p_minus_des_2 = work.x + 22;
  vars.p_minus_des_3 = work.x + 23;
  vars.p_minus_des_4 = work.x + 24;
  vars.p_minus_des_5 = work.x + 25;
  vars.p_minus_des_6 = work.x + 26;
  vars.p_minus_des_7 = work.x + 27;
  vars.p_minus_des_8 = work.x + 28;
  vars.p_minus_des_9 = work.x + 29;
  vars.p_minus_des_10 = work.x + 30;
  vars.p_minus_des_11 = work.x + 31;
  vars.p_minus_des_12 = work.x + 32;
  vars.p_minus_des_13 = work.x + 33;
  vars.p_minus_des_14 = work.x + 34;
  vars.p_minus_des_15 = work.x + 35;
  vars.p_minus_des_16 = work.x + 36;
  vars.p_minus_des_17 = work.x + 37;
  vars.p_minus_des_18 = work.x + 38;
  vars.p_minus_des_19 = work.x + 39;
  vars.p_minus_des_20 = work.x + 40;
  vars.p_minus_des_21 = work.x + 41;
  vars.p_plus_1 = work.x + 42;
  vars.p_plus_2 = work.x + 43;
  vars.p_plus_3 = work.x + 44;
  vars.p_plus_4 = work.x + 45;
  vars.p_plus_5 = work.x + 46;
  vars.p_plus_6 = work.x + 47;
  vars.p_plus_7 = work.x + 48;
  vars.p_plus_8 = work.x + 49;
  vars.p_plus_9 = work.x + 50;
  vars.p_plus_10 = work.x + 51;
  vars.p_plus_11 = work.x + 52;
  vars.p_plus_12 = work.x + 53;
  vars.p_plus_13 = work.x + 54;
  vars.p_plus_14 = work.x + 55;
  vars.p_plus_15 = work.x + 56;
  vars.p_plus_16 = work.x + 57;
  vars.p_plus_17 = work.x + 58;
  vars.p_plus_18 = work.x + 59;
  vars.p_plus_19 = work.x + 60;
  vars.p_plus_20 = work.x + 61;
  vars.p_plus_21 = work.x + 62;
  vars.p_plus_des_1 = work.x + 63;
  vars.p_plus_des_2 = work.x + 64;
  vars.p_plus_des_3 = work.x + 65;
  vars.p_plus_des_4 = work.x + 66;
  vars.p_plus_des_5 = work.x + 67;
  vars.p_plus_des_6 = work.x + 68;
  vars.p_plus_des_7 = work.x + 69;
  vars.p_plus_des_8 = work.x + 70;
  vars.p_plus_des_9 = work.x + 71;
  vars.p_plus_des_10 = work.x + 72;
  vars.p_plus_des_11 = work.x + 73;
  vars.p_plus_des_12 = work.x + 74;
  vars.p_plus_des_13 = work.x + 75;
  vars.p_plus_des_14 = work.x + 76;
  vars.p_plus_des_15 = work.x + 77;
  vars.p_plus_des_16 = work.x + 78;
  vars.p_plus_des_17 = work.x + 79;
  vars.p_plus_des_18 = work.x + 80;
  vars.p_plus_des_19 = work.x + 81;
  vars.p_plus_des_20 = work.x + 82;
  vars.p_plus_des_21 = work.x + 83;
  vars.q_1 = work.x + 84;
  vars.q_2 = work.x + 85;
  vars.q_3 = work.x + 86;
  vars.q_4 = work.x + 87;
  vars.q_5 = work.x + 88;
  vars.q_6 = work.x + 89;
  vars.q_7 = work.x + 90;
  vars.q_8 = work.x + 91;
  vars.q_9 = work.x + 92;
  vars.q_10 = work.x + 93;
  vars.q_11 = work.x + 94;
  vars.q_12 = work.x + 95;
  vars.q_13 = work.x + 96;
  vars.q_14 = work.x + 97;
  vars.q_15 = work.x + 98;
  vars.q_16 = work.x + 99;
  vars.q_17 = work.x + 100;
  vars.q_18 = work.x + 101;
  vars.q_19 = work.x + 102;
  vars.q_20 = work.x + 103;
  vars.q_21 = work.x + 104;
  vars.qdot_1 = work.x + 105;
  vars.qdot_2 = work.x + 106;
  vars.qdot_3 = work.x + 107;
  vars.qdot_4 = work.x + 108;
  vars.qdot_5 = work.x + 109;
  vars.qdot_6 = work.x + 110;
  vars.qdot_7 = work.x + 111;
  vars.qdot_8 = work.x + 112;
  vars.qdot_9 = work.x + 113;
  vars.qdot_10 = work.x + 114;
  vars.qdot_11 = work.x + 115;
  vars.qdot_12 = work.x + 116;
  vars.qdot_13 = work.x + 117;
  vars.qdot_14 = work.x + 118;
  vars.qdot_15 = work.x + 119;
  vars.qdot_16 = work.x + 120;
  vars.qdot_17 = work.x + 121;
  vars.qdot_18 = work.x + 122;
  vars.qdot_19 = work.x + 123;
  vars.qdot_20 = work.x + 124;
  vars.qdot_21 = work.x + 125;
}
void setup_indexed_params(void) {
  /* In CVXGEN, you can say */
  /*   parameters */
  /*     A[i] (5,3), i=1..4 */
  /*   end */
  /* This function sets up A[2] to be a pointer to A_2, which is a length-15 */
  /* vector of doubles. */
  /* If you access parameters that you haven't defined in CVXGEN, the result */
  /* is undefined. */
  params.p_plus_des[0] = params.p_plus_des_0;
  params.p_minus_des[0] = params.p_minus_des_0;
  params.qdot[0] = params.qdot_0;
  params.q[0] = params.q_0;
  params.p_plus[0] = params.p_plus_0;
  params.p_minus[0] = params.p_minus_0;
}
void setup_indexed_optvars(void) {
  /* In CVXGEN, you can say */
  /*   variables */
  /*     x[i] (5), i=2..4 */
  /*   end */
  /* This function sets up x[3] to be a pointer to x_3, which is a length-5 */
  /* vector of doubles. */
  /* If you access variables that you haven't defined in CVXGEN, the result */
  /* is undefined. */
  vars.q[1] = vars.q_1;
  vars.p_plus[1] = vars.p_plus_1;
  vars.p_minus[1] = vars.p_minus_1;
  vars.q[2] = vars.q_2;
  vars.p_plus[2] = vars.p_plus_2;
  vars.p_minus[2] = vars.p_minus_2;
  vars.q[3] = vars.q_3;
  vars.p_plus[3] = vars.p_plus_3;
  vars.p_minus[3] = vars.p_minus_3;
  vars.q[4] = vars.q_4;
  vars.p_plus[4] = vars.p_plus_4;
  vars.p_minus[4] = vars.p_minus_4;
  vars.q[5] = vars.q_5;
  vars.p_plus[5] = vars.p_plus_5;
  vars.p_minus[5] = vars.p_minus_5;
  vars.q[6] = vars.q_6;
  vars.p_plus[6] = vars.p_plus_6;
  vars.p_minus[6] = vars.p_minus_6;
  vars.q[7] = vars.q_7;
  vars.p_plus[7] = vars.p_plus_7;
  vars.p_minus[7] = vars.p_minus_7;
  vars.q[8] = vars.q_8;
  vars.p_plus[8] = vars.p_plus_8;
  vars.p_minus[8] = vars.p_minus_8;
  vars.q[9] = vars.q_9;
  vars.p_plus[9] = vars.p_plus_9;
  vars.p_minus[9] = vars.p_minus_9;
  vars.q[10] = vars.q_10;
  vars.p_plus[10] = vars.p_plus_10;
  vars.p_minus[10] = vars.p_minus_10;
  vars.q[11] = vars.q_11;
  vars.p_plus[11] = vars.p_plus_11;
  vars.p_minus[11] = vars.p_minus_11;
  vars.q[12] = vars.q_12;
  vars.p_plus[12] = vars.p_plus_12;
  vars.p_minus[12] = vars.p_minus_12;
  vars.q[13] = vars.q_13;
  vars.p_plus[13] = vars.p_plus_13;
  vars.p_minus[13] = vars.p_minus_13;
  vars.q[14] = vars.q_14;
  vars.p_plus[14] = vars.p_plus_14;
  vars.p_minus[14] = vars.p_minus_14;
  vars.q[15] = vars.q_15;
  vars.p_plus[15] = vars.p_plus_15;
  vars.p_minus[15] = vars.p_minus_15;
  vars.q[16] = vars.q_16;
  vars.p_plus[16] = vars.p_plus_16;
  vars.p_minus[16] = vars.p_minus_16;
  vars.q[17] = vars.q_17;
  vars.p_plus[17] = vars.p_plus_17;
  vars.p_minus[17] = vars.p_minus_17;
  vars.q[18] = vars.q_18;
  vars.p_plus[18] = vars.p_plus_18;
  vars.p_minus[18] = vars.p_minus_18;
  vars.q[19] = vars.q_19;
  vars.p_plus[19] = vars.p_plus_19;
  vars.p_minus[19] = vars.p_minus_19;
  vars.q[20] = vars.q_20;
  vars.p_plus[20] = vars.p_plus_20;
  vars.p_minus[20] = vars.p_minus_20;
  vars.p_plus_des[1] = vars.p_plus_des_1;
  vars.p_minus_des[1] = vars.p_minus_des_1;
  vars.p_plus_des[2] = vars.p_plus_des_2;
  vars.p_minus_des[2] = vars.p_minus_des_2;
  vars.p_plus_des[3] = vars.p_plus_des_3;
  vars.p_minus_des[3] = vars.p_minus_des_3;
  vars.p_plus_des[4] = vars.p_plus_des_4;
  vars.p_minus_des[4] = vars.p_minus_des_4;
  vars.p_plus_des[5] = vars.p_plus_des_5;
  vars.p_minus_des[5] = vars.p_minus_des_5;
  vars.p_plus_des[6] = vars.p_plus_des_6;
  vars.p_minus_des[6] = vars.p_minus_des_6;
  vars.p_plus_des[7] = vars.p_plus_des_7;
  vars.p_minus_des[7] = vars.p_minus_des_7;
  vars.p_plus_des[8] = vars.p_plus_des_8;
  vars.p_minus_des[8] = vars.p_minus_des_8;
  vars.p_plus_des[9] = vars.p_plus_des_9;
  vars.p_minus_des[9] = vars.p_minus_des_9;
  vars.p_plus_des[10] = vars.p_plus_des_10;
  vars.p_minus_des[10] = vars.p_minus_des_10;
  vars.p_plus_des[11] = vars.p_plus_des_11;
  vars.p_minus_des[11] = vars.p_minus_des_11;
  vars.p_plus_des[12] = vars.p_plus_des_12;
  vars.p_minus_des[12] = vars.p_minus_des_12;
  vars.p_plus_des[13] = vars.p_plus_des_13;
  vars.p_minus_des[13] = vars.p_minus_des_13;
  vars.p_plus_des[14] = vars.p_plus_des_14;
  vars.p_minus_des[14] = vars.p_minus_des_14;
  vars.p_plus_des[15] = vars.p_plus_des_15;
  vars.p_minus_des[15] = vars.p_minus_des_15;
  vars.p_plus_des[16] = vars.p_plus_des_16;
  vars.p_minus_des[16] = vars.p_minus_des_16;
  vars.p_plus_des[17] = vars.p_plus_des_17;
  vars.p_minus_des[17] = vars.p_minus_des_17;
  vars.p_plus_des[18] = vars.p_plus_des_18;
  vars.p_minus_des[18] = vars.p_minus_des_18;
  vars.p_plus_des[19] = vars.p_plus_des_19;
  vars.p_minus_des[19] = vars.p_minus_des_19;
  vars.p_plus_des[20] = vars.p_plus_des_20;
  vars.p_minus_des[20] = vars.p_minus_des_20;
  vars.p_plus_des[21] = vars.p_plus_des_21;
  vars.p_minus_des[21] = vars.p_minus_des_21;
  vars.qdot[21] = vars.qdot_21;
  vars.qdot[1] = vars.qdot_1;
  vars.qdot[2] = vars.qdot_2;
  vars.qdot[3] = vars.qdot_3;
  vars.qdot[4] = vars.qdot_4;
  vars.qdot[5] = vars.qdot_5;
  vars.qdot[6] = vars.qdot_6;
  vars.qdot[7] = vars.qdot_7;
  vars.qdot[8] = vars.qdot_8;
  vars.qdot[9] = vars.qdot_9;
  vars.qdot[10] = vars.qdot_10;
  vars.qdot[11] = vars.qdot_11;
  vars.qdot[12] = vars.qdot_12;
  vars.qdot[13] = vars.qdot_13;
  vars.qdot[14] = vars.qdot_14;
  vars.qdot[15] = vars.qdot_15;
  vars.qdot[16] = vars.qdot_16;
  vars.qdot[17] = vars.qdot_17;
  vars.qdot[18] = vars.qdot_18;
  vars.qdot[19] = vars.qdot_19;
  vars.qdot[20] = vars.qdot_20;
  vars.q[21] = vars.q_21;
  vars.p_plus[21] = vars.p_plus_21;
  vars.p_minus[21] = vars.p_minus_21;
}
void setup_indexing(void) {
  setup_pointers();
  setup_indexed_params();
  setup_indexed_optvars();
}
void set_start(void) {
  int i;
  for (i = 0; i < 166; i++)
    work.x[i] = 0;
  for (i = 0; i < 124; i++)
    work.y[i] = 0;
  for (i = 0; i < 84; i++)
    work.s[i] = (work.h[i] > 0) ? work.h[i] : settings.s_init;
  for (i = 0; i < 84; i++)
    work.z[i] = settings.z_init;
}
double eval_objv(void) {
  int i;
  double objv;
  /* Borrow space in work.rhs. */
  multbyP(work.rhs, work.x);
  objv = 0;
  for (i = 0; i < 166; i++)
    objv += work.x[i]*work.rhs[i];
  objv *= 0.5;
  for (i = 0; i < 166; i++)
    objv += work.q[i]*work.x[i];
  objv += work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_803904221184[0]+work.quad_68028641280[0]+work.quad_212708605952[0]+work.quad_786390380544[0]+work.quad_293549047808[0];
  return objv;
}
void fillrhs_aff(void) {
  int i;
  double *r1, *r2, *r3, *r4;
  r1 = work.rhs;
  r2 = work.rhs + 166;
  r3 = work.rhs + 250;
  r4 = work.rhs + 334;
  /* r1 = -A^Ty - G^Tz - Px - q. */
  multbymAT(r1, work.y);
  multbymGT(work.buffer, work.z);
  for (i = 0; i < 166; i++)
    r1[i] += work.buffer[i];
  multbyP(work.buffer, work.x);
  for (i = 0; i < 166; i++)
    r1[i] -= work.buffer[i] + work.q[i];
  /* r2 = -z. */
  for (i = 0; i < 84; i++)
    r2[i] = -work.z[i];
  /* r3 = -Gx - s + h. */
  multbymG(r3, work.x);
  for (i = 0; i < 84; i++)
    r3[i] += -work.s[i] + work.h[i];
  /* r4 = -Ax + b. */
  multbymA(r4, work.x);
  for (i = 0; i < 124; i++)
    r4[i] += work.b[i];
}
void fillrhs_cc(void) {
  int i;
  double *r2;
  double *ds_aff, *dz_aff;
  double mu;
  double alpha;
  double sigma;
  double smu;
  double minval;
  r2 = work.rhs + 166;
  ds_aff = work.lhs_aff + 166;
  dz_aff = work.lhs_aff + 250;
  mu = 0;
  for (i = 0; i < 84; i++)
    mu += work.s[i]*work.z[i];
  /* Don't finish calculating mu quite yet. */
  /* Find min(min(ds./s), min(dz./z)). */
  minval = 0;
  for (i = 0; i < 84; i++)
    if (ds_aff[i] < minval*work.s[i])
      minval = ds_aff[i]/work.s[i];
  for (i = 0; i < 84; i++)
    if (dz_aff[i] < minval*work.z[i])
      minval = dz_aff[i]/work.z[i];
  /* Find alpha. */
  if (-1 < minval)
      alpha = 1;
  else
      alpha = -1/minval;
  sigma = 0;
  for (i = 0; i < 84; i++)
    sigma += (work.s[i] + alpha*ds_aff[i])*
      (work.z[i] + alpha*dz_aff[i]);
  sigma /= mu;
  sigma = sigma*sigma*sigma;
  /* Finish calculating mu now. */
  mu *= 0.011904761904761904;
  smu = sigma*mu;
  /* Fill-in the rhs. */
  for (i = 0; i < 166; i++)
    work.rhs[i] = 0;
  for (i = 250; i < 458; i++)
    work.rhs[i] = 0;
  for (i = 0; i < 84; i++)
    r2[i] = work.s_inv[i]*(smu - ds_aff[i]*dz_aff[i]);
}
void refine(double *target, double *var) {
  int i, j;
  double *residual = work.buffer;
  double norm2;
  double *new_var = work.buffer2;
  for (j = 0; j < settings.refine_steps; j++) {
    norm2 = 0;
    matrix_multiply(residual, var);
    for (i = 0; i < 458; i++) {
      residual[i] = residual[i] - target[i];
      norm2 += residual[i]*residual[i];
    }
#ifndef ZERO_LIBRARY_MODE
    if (settings.verbose_refinement) {
      if (j == 0)
        printf("Initial residual before refinement has norm squared %.6g.\n", norm2);
      else
        printf("After refinement we get squared norm %.6g.\n", norm2);
    }
#endif
    /* Solve to find new_var = KKT \ (target - A*var). */
    ldl_solve(residual, new_var);
    /* Update var += new_var, or var += KKT \ (target - A*var). */
    for (i = 0; i < 458; i++) {
      var[i] -= new_var[i];
    }
  }
#ifndef ZERO_LIBRARY_MODE
  if (settings.verbose_refinement) {
    /* Check the residual once more, but only if we're reporting it, since */
    /* it's expensive. */
    norm2 = 0;
    matrix_multiply(residual, var);
    for (i = 0; i < 458; i++) {
      residual[i] = residual[i] - target[i];
      norm2 += residual[i]*residual[i];
    }
    if (j == 0)
      printf("Initial residual before refinement has norm squared %.6g.\n", norm2);
    else
      printf("After refinement we get squared norm %.6g.\n", norm2);
  }
#endif
}
double calc_ineq_resid_squared(void) {
  /* Calculates the norm ||-Gx - s + h||. */
  double norm2_squared;
  int i;
  /* Find -Gx. */
  multbymG(work.buffer, work.x);
  /* Add -s + h. */
  for (i = 0; i < 84; i++)
    work.buffer[i] += -work.s[i] + work.h[i];
  /* Now find the squared norm. */
  norm2_squared = 0;
  for (i = 0; i < 84; i++)
    norm2_squared += work.buffer[i]*work.buffer[i];
  return norm2_squared;
}
double calc_eq_resid_squared(void) {
  /* Calculates the norm ||-Ax + b||. */
  double norm2_squared;
  int i;
  /* Find -Ax. */
  multbymA(work.buffer, work.x);
  /* Add +b. */
  for (i = 0; i < 124; i++)
    work.buffer[i] += work.b[i];
  /* Now find the squared norm. */
  norm2_squared = 0;
  for (i = 0; i < 124; i++)
    norm2_squared += work.buffer[i]*work.buffer[i];
  return norm2_squared;
}
void better_start(void) {
  /* Calculates a better starting point, using a similar approach to CVXOPT. */
  /* Not yet speed optimized. */
  int i;
  double *x, *s, *z, *y;
  double alpha;
  work.block_33[0] = -1;
  /* Make sure sinvz is 1 to make hijacked KKT system ok. */
  for (i = 0; i < 84; i++)
    work.s_inv_z[i] = 1;
  fill_KKT();
  ldl_factor();
  fillrhs_start();
  /* Borrow work.lhs_aff for the solution. */
  ldl_solve(work.rhs, work.lhs_aff);
  /* Don't do any refinement for now. Precision doesn't matter too much. */
  x = work.lhs_aff;
  s = work.lhs_aff + 166;
  z = work.lhs_aff + 250;
  y = work.lhs_aff + 334;
  /* Just set x and y as is. */
  for (i = 0; i < 166; i++)
    work.x[i] = x[i];
  for (i = 0; i < 124; i++)
    work.y[i] = y[i];
  /* Now complete the initialization. Start with s. */
  /* Must have alpha > max(z). */
  alpha = -1e99;
  for (i = 0; i < 84; i++)
    if (alpha < z[i])
      alpha = z[i];
  if (alpha < 0) {
    for (i = 0; i < 84; i++)
      work.s[i] = -z[i];
  } else {
    alpha += 1;
    for (i = 0; i < 84; i++)
      work.s[i] = -z[i] + alpha;
  }
  /* Now initialize z. */
  /* Now must have alpha > max(-z). */
  alpha = -1e99;
  for (i = 0; i < 84; i++)
    if (alpha < -z[i])
      alpha = -z[i];
  if (alpha < 0) {
    for (i = 0; i < 84; i++)
      work.z[i] = z[i];
  } else {
    alpha += 1;
    for (i = 0; i < 84; i++)
      work.z[i] = z[i] + alpha;
  }
}
void fillrhs_start(void) {
  /* Fill rhs with (-q, 0, h, b). */
  int i;
  double *r1, *r2, *r3, *r4;
  r1 = work.rhs;
  r2 = work.rhs + 166;
  r3 = work.rhs + 250;
  r4 = work.rhs + 334;
  for (i = 0; i < 166; i++)
    r1[i] = -work.q[i];
  for (i = 0; i < 84; i++)
    r2[i] = 0;
  for (i = 0; i < 84; i++)
    r3[i] = work.h[i];
  for (i = 0; i < 124; i++)
    r4[i] = work.b[i];
}
long solve(void) {
  int i;
  int iter;
  double *dx, *ds, *dy, *dz;
  double minval;
  double alpha;
  work.converged = 0;
  setup_pointers();
  pre_ops();
#ifndef ZERO_LIBRARY_MODE
  if (settings.verbose)
    printf("iter     objv        gap       |Ax-b|    |Gx+s-h|    step\n");
#endif
  fillq();
  fillh();
  fillb();
  if (settings.better_start)
    better_start();
  else
    set_start();
  for (iter = 0; iter < settings.max_iters; iter++) {
    for (i = 0; i < 84; i++) {
      work.s_inv[i] = 1.0 / work.s[i];
      work.s_inv_z[i] = work.s_inv[i]*work.z[i];
    }
    work.block_33[0] = 0;
    fill_KKT();
    ldl_factor();
    /* Affine scaling directions. */
    fillrhs_aff();
    ldl_solve(work.rhs, work.lhs_aff);
    refine(work.rhs, work.lhs_aff);
    /* Centering plus corrector directions. */
    fillrhs_cc();
    ldl_solve(work.rhs, work.lhs_cc);
    refine(work.rhs, work.lhs_cc);
    /* Add the two together and store in aff. */
    for (i = 0; i < 458; i++)
      work.lhs_aff[i] += work.lhs_cc[i];
    /* Rename aff to reflect its new meaning. */
    dx = work.lhs_aff;
    ds = work.lhs_aff + 166;
    dz = work.lhs_aff + 250;
    dy = work.lhs_aff + 334;
    /* Find min(min(ds./s), min(dz./z)). */
    minval = 0;
    for (i = 0; i < 84; i++)
      if (ds[i] < minval*work.s[i])
        minval = ds[i]/work.s[i];
    for (i = 0; i < 84; i++)
      if (dz[i] < minval*work.z[i])
        minval = dz[i]/work.z[i];
    /* Find alpha. */
    if (-0.99 < minval)
      alpha = 1;
    else
      alpha = -0.99/minval;
    /* Update the primal and dual variables. */
    for (i = 0; i < 166; i++)
      work.x[i] += alpha*dx[i];
    for (i = 0; i < 84; i++)
      work.s[i] += alpha*ds[i];
    for (i = 0; i < 84; i++)
      work.z[i] += alpha*dz[i];
    for (i = 0; i < 124; i++)
      work.y[i] += alpha*dy[i];
    work.gap = eval_gap();
    work.eq_resid_squared = calc_eq_resid_squared();
    work.ineq_resid_squared = calc_ineq_resid_squared();
#ifndef ZERO_LIBRARY_MODE
    if (settings.verbose) {
      work.optval = eval_objv();
      printf("%3d   %10.3e  %9.2e  %9.2e  %9.2e  % 6.4f\n",
          iter+1, work.optval, work.gap, sqrt(work.eq_resid_squared),
          sqrt(work.ineq_resid_squared), alpha);
    }
#endif
    /* Test termination conditions. Requires optimality, and satisfied */
    /* constraints. */
    if (   (work.gap < settings.eps)
        && (work.eq_resid_squared <= settings.resid_tol*settings.resid_tol)
        && (work.ineq_resid_squared <= settings.resid_tol*settings.resid_tol)
       ) {
      work.converged = 1;
      work.optval = eval_objv();
      return iter+1;
    }
  }
  return iter;
}
