#include<boost/python.hpp>
#include "solver.h"
#include <iostream>

Vars vars;
Params params;
Workspace work;
Settings settings;

int flags = 0;

boost::python::list runController(double A11,
				  double A12,
				  double A13,
				  double A14,
				  double A21,
				  double A22,
				  double A23,
				  double A24,
				  double A33,
				  double A44,
				  double A51,
				  double A52,
				  double A53,
				  double A54,
				  double A55,
				  double B12,
				  double B13,
				  double B22,
				  double B23,
				  double B32,
				  double B43,
				  double B52,
				  double B53,
				  double qdot0,
				  double q0,
				  double pPlus0,
				  double pMinus0,
				  double pPlusDes0,
				  double pMinusDes0,
				  double k0,
				  double kdot,
				  double dt,
				  double qGoal,
				  double kGoal,
				  double qMax,
				  double qMin,
				  double pMax,
				  double pMin,
				  double qdotDist,
				  double qDist,
				  double Qstiff,
				  double Qsdot,
				  double Qpres,
				  double Qpos,
				  double Qvel)
{
  if (flags == 0)
    {
      set_defaults();
      setup_indexing();
      flags = 1;
      settings.verbose = 0;
      settings.eps = 9.5*10e-4; // this could be changed to e-6 to be more accurate if necessary
    }

  // define the optimization parameters.

  int i;
  //int num_jts;

  // num_jts = boost::python::len(q_0);
  //std::cout << "Number of Joints : " << num_jts << std::endl;

  /** for(i=0; i< boost::python::len(p_des_0); ++i)
    {
      std::cout << "p_des_0 : " << boost::python::extract<double>(p_des_0[i]) << std::endl;
      std::cout << i << std::endl;
      }**/

  //for (i = 0; i < boost::python::len(q_0); i++)
  // {
      //std::cout << " i : " << i << std::endl;
      //std::cout << " i+num_jts : " << i+num_jts << std::endl;
  //  std::cout << "Got Here" << std::endl;

  /* Values for state space matricies */
  params.A11[0] = A11;
  params.A12[0] = A12;
  params.A13[0] = A13;
  params.A14[0] = A14;

  params.A21[0] = A21;
  params.A22[0] = A22;
  params.A23[0] = A23;
  params.A24[0] = A24;

  params.A33[0] = A33;

  params.A44[0] = A44;

  params.A51[0] = A51;
  params.A52[0] = A52;
  params.A53[0] = A53;
  params.A54[0] = A54;
  params.A55[0] = A55;

  params.B12[0] = B12;
  params.B13[0] = B13;

  params.B22[0] = B22;
  params.B23[0] = B23;

  params.B32[0] = B32;
  params.B43[0] = B43;
  
  params.B52[0] = B52;
  params.B53[0] = B53;

  /* Initial conditions for states and inputs*/

  params.qdot_0[0] = qdot0;
  params.q_0[0] = q0;
  params.pPlus_0[0] = pPlus0;
  params.pMinus_0[0] = pMinus0;
  params.pPlusDes_0[0] = pPlusDes0;
  params.pMinusDes_0[0] = pMinusDes0;
  params.k_0[0] = k0;

  /* other parameters for solver */

  params.kdot[0] = kdot;
  params.dt[0] = dt;
  params.qGoal[0] = qGoal;
  params.kGoal[0] = kGoal;
  params.qMax[0] = qMax;
  params.qMin[0] = qMin;
  params.pMax[0] = pMax;
  params.pMin[0] = pMin;
  params.qdotDist[0] = qdotDist;
  params.qDist[0] = qDist;
  
  /* Weights for cost function */
  params.Qstiff[0] = Qstiff;
  params.Qsdot[0] = Qsdot;
  params.Qpres[0] = Qpres;
  params.Qpos[0] = Qpos;
  params.Qvel[0] = Qvel;

  solve();

  
  std::cout << "converged status : " << work.converged << std::endl;

  boost::python::list pPlusCmd;
  boost::python::list pMinusCmd;

  if (work.converged == 1)
  {

    pPlusCmd.append(vars.pPlusDes_1[0]);
    pPlusCmd.append(vars.pPlusDes_2[0]);
    pPlusCmd.append(vars.pPlusDes_3[0]);
    pPlusCmd.append(vars.pPlusDes_4[0]);
    pPlusCmd.append(vars.pPlusDes_5[0]);

    pMinusCmd.append(vars.pMinusDes_1[0]);
    pMinusCmd.append(vars.pMinusDes_2[0]);
    pMinusCmd.append(vars.pMinusDes_3[0]);
    pMinusCmd.append(vars.pMinusDes_4[0]);
    pMinusCmd.append(vars.pMinusDes_5[0]);
      
  }
  else
  {

    pPlusCmd.append(params.pPlusDes_0[0]);
    pMinusCmd.append(params.pMinusDes_0[0]);
  }

  boost::python::list pCmd;

  pCmd.append(pPlusCmd);
  pCmd.append(pMinusCmd);

  return pCmd;


}

char const* greet()
{
  printf("got into greet");
  return "hello, world (and you)";
}

// this is what tells boost what the function names and definitions should be (DON'T DELETE)
BOOST_PYTHON_MODULE(kl_stiffness_solver)
{
  using namespace boost::python;
  def("greet", greet);
  def("runController", runController);
}
