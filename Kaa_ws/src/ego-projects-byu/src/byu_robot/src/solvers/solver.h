/* Produced by CVXGEN, 2017-05-23 15:26:45 -0400.  */
/* CVXGEN is Copyright (C) 2006-2012 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2012 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: solver.h. */
/* Description: Header file with relevant definitions. */
#ifndef SOLVER_H
#define SOLVER_H
/* Uncomment the next line to remove all library dependencies. */
/*#define ZERO_LIBRARY_MODE */
#ifdef MATLAB_MEX_FILE
/* Matlab functions. MATLAB_MEX_FILE will be defined by the mex compiler. */
/* If you are not using the mex compiler, this functionality will not intrude, */
/* as it will be completely disabled at compile-time. */
#include "mex.h"
#else
#ifndef ZERO_LIBRARY_MODE
#include <stdio.h>
#endif
#endif
/* Space must be allocated somewhere (testsolver.c, csolve.c or your own */
/* program) for the global variables vars, params, work and settings. */
/* At the bottom of this file, they are externed. */
#ifndef ZERO_LIBRARY_MODE
#include <math.h>
#define pm(A, m, n) printmatrix(#A, A, m, n, 1)
#endif
typedef struct Params_t {
  double q_goal[1];
  double q_int[1];
  double Q[1];
  double p_plus_t[1];
  double J[1];
  double p_minus_t[1];
  double p_plus_des_0[1];
  double R[1];
  double p_minus_des_0[1];
  double S[1];
  double A11[1];
  double qdot_0[1];
  double A12[1];
  double q_0[1];
  double A13[1];
  double p_plus_0[1];
  double A14[1];
  double p_minus_0[1];
  double B12[1];
  double B13[1];
  double qdot_dist[1];
  double A21[1];
  double A22[1];
  double A23[1];
  double A24[1];
  double B22[1];
  double B23[1];
  double q_dist[1];
  double A33[1];
  double B32[1];
  double A44[1];
  double B43[1];
  double p_min[1];
  double p_max[1];
  double *p_plus_des[1];
  double *p_minus_des[1];
  double *qdot[1];
  double *q[1];
  double *p_plus[1];
  double *p_minus[1];
} Params;
typedef struct Vars_t {
  double *q_1; /* 1 rows. */
  double *p_plus_1; /* 1 rows. */
  double *p_minus_1; /* 1 rows. */
  double *q_2; /* 1 rows. */
  double *p_plus_2; /* 1 rows. */
  double *p_minus_2; /* 1 rows. */
  double *q_3; /* 1 rows. */
  double *p_plus_3; /* 1 rows. */
  double *p_minus_3; /* 1 rows. */
  double *q_4; /* 1 rows. */
  double *p_plus_4; /* 1 rows. */
  double *p_minus_4; /* 1 rows. */
  double *q_5; /* 1 rows. */
  double *p_plus_5; /* 1 rows. */
  double *p_minus_5; /* 1 rows. */
  double *q_6; /* 1 rows. */
  double *p_plus_6; /* 1 rows. */
  double *p_minus_6; /* 1 rows. */
  double *q_7; /* 1 rows. */
  double *p_plus_7; /* 1 rows. */
  double *p_minus_7; /* 1 rows. */
  double *q_8; /* 1 rows. */
  double *p_plus_8; /* 1 rows. */
  double *p_minus_8; /* 1 rows. */
  double *q_9; /* 1 rows. */
  double *p_plus_9; /* 1 rows. */
  double *p_minus_9; /* 1 rows. */
  double *q_10; /* 1 rows. */
  double *p_plus_10; /* 1 rows. */
  double *p_minus_10; /* 1 rows. */
  double *q_11; /* 1 rows. */
  double *p_plus_11; /* 1 rows. */
  double *p_minus_11; /* 1 rows. */
  double *q_12; /* 1 rows. */
  double *p_plus_12; /* 1 rows. */
  double *p_minus_12; /* 1 rows. */
  double *q_13; /* 1 rows. */
  double *p_plus_13; /* 1 rows. */
  double *p_minus_13; /* 1 rows. */
  double *q_14; /* 1 rows. */
  double *p_plus_14; /* 1 rows. */
  double *p_minus_14; /* 1 rows. */
  double *q_15; /* 1 rows. */
  double *p_plus_15; /* 1 rows. */
  double *p_minus_15; /* 1 rows. */
  double *q_16; /* 1 rows. */
  double *p_plus_16; /* 1 rows. */
  double *p_minus_16; /* 1 rows. */
  double *q_17; /* 1 rows. */
  double *p_plus_17; /* 1 rows. */
  double *p_minus_17; /* 1 rows. */
  double *q_18; /* 1 rows. */
  double *p_plus_18; /* 1 rows. */
  double *p_minus_18; /* 1 rows. */
  double *q_19; /* 1 rows. */
  double *p_plus_19; /* 1 rows. */
  double *p_minus_19; /* 1 rows. */
  double *q_20; /* 1 rows. */
  double *p_plus_20; /* 1 rows. */
  double *p_minus_20; /* 1 rows. */
  double *p_plus_des_1; /* 1 rows. */
  double *p_minus_des_1; /* 1 rows. */
  double *t_01; /* 1 rows. */
  double *t_02; /* 1 rows. */
  double *t_03; /* 1 rows. */
  double *t_04; /* 1 rows. */
  double *t_05; /* 1 rows. */
  double *t_06; /* 1 rows. */
  double *t_07; /* 1 rows. */
  double *t_08; /* 1 rows. */
  double *t_09; /* 1 rows. */
  double *t_10; /* 1 rows. */
  double *t_11; /* 1 rows. */
  double *t_12; /* 1 rows. */
  double *t_13; /* 1 rows. */
  double *t_14; /* 1 rows. */
  double *t_15; /* 1 rows. */
  double *t_16; /* 1 rows. */
  double *t_17; /* 1 rows. */
  double *t_18; /* 1 rows. */
  double *t_19; /* 1 rows. */
  double *t_20; /* 1 rows. */
  double *t_21; /* 1 rows. */
  double *t_22; /* 1 rows. */
  double *t_23; /* 1 rows. */
  double *t_24; /* 1 rows. */
  double *t_25; /* 1 rows. */
  double *t_26; /* 1 rows. */
  double *t_27; /* 1 rows. */
  double *t_28; /* 1 rows. */
  double *t_29; /* 1 rows. */
  double *t_30; /* 1 rows. */
  double *t_31; /* 1 rows. */
  double *t_32; /* 1 rows. */
  double *t_33; /* 1 rows. */
  double *t_34; /* 1 rows. */
  double *t_35; /* 1 rows. */
  double *t_36; /* 1 rows. */
  double *t_37; /* 1 rows. */
  double *t_38; /* 1 rows. */
  double *t_39; /* 1 rows. */
  double *t_40; /* 1 rows. */
  double *qdot_21; /* 1 rows. */
  double *p_plus_des_2; /* 1 rows. */
  double *p_plus_des_3; /* 1 rows. */
  double *p_plus_des_4; /* 1 rows. */
  double *p_plus_des_5; /* 1 rows. */
  double *p_plus_des_6; /* 1 rows. */
  double *p_plus_des_7; /* 1 rows. */
  double *p_plus_des_8; /* 1 rows. */
  double *p_plus_des_9; /* 1 rows. */
  double *p_plus_des_10; /* 1 rows. */
  double *p_plus_des_11; /* 1 rows. */
  double *p_plus_des_12; /* 1 rows. */
  double *p_plus_des_13; /* 1 rows. */
  double *p_plus_des_14; /* 1 rows. */
  double *p_plus_des_15; /* 1 rows. */
  double *p_plus_des_16; /* 1 rows. */
  double *p_plus_des_17; /* 1 rows. */
  double *p_plus_des_18; /* 1 rows. */
  double *p_plus_des_19; /* 1 rows. */
  double *p_plus_des_20; /* 1 rows. */
  double *p_plus_des_21; /* 1 rows. */
  double *p_minus_des_2; /* 1 rows. */
  double *p_minus_des_3; /* 1 rows. */
  double *p_minus_des_4; /* 1 rows. */
  double *p_minus_des_5; /* 1 rows. */
  double *p_minus_des_6; /* 1 rows. */
  double *p_minus_des_7; /* 1 rows. */
  double *p_minus_des_8; /* 1 rows. */
  double *p_minus_des_9; /* 1 rows. */
  double *p_minus_des_10; /* 1 rows. */
  double *p_minus_des_11; /* 1 rows. */
  double *p_minus_des_12; /* 1 rows. */
  double *p_minus_des_13; /* 1 rows. */
  double *p_minus_des_14; /* 1 rows. */
  double *p_minus_des_15; /* 1 rows. */
  double *p_minus_des_16; /* 1 rows. */
  double *p_minus_des_17; /* 1 rows. */
  double *p_minus_des_18; /* 1 rows. */
  double *p_minus_des_19; /* 1 rows. */
  double *p_minus_des_20; /* 1 rows. */
  double *p_minus_des_21; /* 1 rows. */
  double *qdot_1; /* 1 rows. */
  double *qdot_2; /* 1 rows. */
  double *qdot_3; /* 1 rows. */
  double *qdot_4; /* 1 rows. */
  double *qdot_5; /* 1 rows. */
  double *qdot_6; /* 1 rows. */
  double *qdot_7; /* 1 rows. */
  double *qdot_8; /* 1 rows. */
  double *qdot_9; /* 1 rows. */
  double *qdot_10; /* 1 rows. */
  double *qdot_11; /* 1 rows. */
  double *qdot_12; /* 1 rows. */
  double *qdot_13; /* 1 rows. */
  double *qdot_14; /* 1 rows. */
  double *qdot_15; /* 1 rows. */
  double *qdot_16; /* 1 rows. */
  double *qdot_17; /* 1 rows. */
  double *qdot_18; /* 1 rows. */
  double *qdot_19; /* 1 rows. */
  double *qdot_20; /* 1 rows. */
  double *q_21; /* 1 rows. */
  double *p_plus_21; /* 1 rows. */
  double *p_minus_21; /* 1 rows. */
  double *q[22];
  double *p_plus[22];
  double *p_minus[22];
  double *p_plus_des[22];
  double *p_minus_des[22];
  double *qdot[22];
} Vars;
typedef struct Workspace_t {
  double h[84];
  double s_inv[84];
  double s_inv_z[84];
  double b[124];
  double q[166];
  double rhs[458];
  double x[458];
  double *s;
  double *z;
  double *y;
  double lhs_aff[458];
  double lhs_cc[458];
  double buffer[458];
  double buffer2[458];
  double KKT[969];
  double L[1263];
  double d[458];
  double v[458];
  double d_inv[458];
  double gap;
  double optval;
  double ineq_resid_squared;
  double eq_resid_squared;
  double block_33[1];
  /* Pre-op symbols. */
  double quad_803904221184[1];
  double quad_68028641280[1];
  double quad_212708605952[1];
  double quad_786390380544[1];
  double quad_293549047808[1];
  int converged;
} Workspace;
typedef struct Settings_t {
  double resid_tol;
  double eps;
  int max_iters;
  int refine_steps;
  int better_start;
  /* Better start obviates the need for s_init and z_init. */
  double s_init;
  double z_init;
  int verbose;
  /* Show extra details of the iterative refinement steps. */
  int verbose_refinement;
  int debug;
  /* For regularization. Minimum value of abs(D_ii) in the kkt D factor. */
  double kkt_reg;
} Settings;
extern Vars vars;
extern Params params;
extern Workspace work;
extern Settings settings;
/* Function definitions in ldl.c: */
void ldl_solve(double *target, double *var);
void ldl_factor(void);
double check_factorization(void);
void matrix_multiply(double *result, double *source);
double check_residual(double *target, double *multiplicand);
void fill_KKT(void);

/* Function definitions in matrix_support.c: */
void multbymA(double *lhs, double *rhs);
void multbymAT(double *lhs, double *rhs);
void multbymG(double *lhs, double *rhs);
void multbymGT(double *lhs, double *rhs);
void multbyP(double *lhs, double *rhs);
void fillq(void);
void fillh(void);
void fillb(void);
void pre_ops(void);

/* Function definitions in solver.c: */
double eval_gap(void);
void set_defaults(void);
void setup_pointers(void);
void setup_indexed_params(void);
void setup_indexed_optvars(void);
void setup_indexing(void);
void set_start(void);
double eval_objv(void);
void fillrhs_aff(void);
void fillrhs_cc(void);
void refine(double *target, double *var);
double calc_ineq_resid_squared(void);
double calc_eq_resid_squared(void);
void better_start(void);
void fillrhs_start(void);
long solve(void);

/* Function definitions in testsolver.c: */
int main(int argc, char **argv);
void load_default_data(void);

/* Function definitions in util.c: */
void tic(void);
float toc(void);
float tocq(void);
void printmatrix(char *name, double *A, int m, int n, int sparse);
double unif(double lower, double upper);
float ran1(long*idum, int reset);
float randn_internal(long *idum, int reset);
double randn(void);
void reset_rand(void);

#endif
