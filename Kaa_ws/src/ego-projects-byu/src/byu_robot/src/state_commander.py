#!/usr/bin/env python

# This takes current states and decides desired states for the robot.
# It incorporates functionality from the planner and other modes as well.

import rospy
import numpy as np
import os
from byu_robot.msg import states
from byu_robot.msg import commands
from ego_byu.tools import constructors as con
from ego_byu.tools import geometries as geom
from ego_byu.tools import paths
from ego_byu.tools import qpik
from ego_byu.tools import arm_tools as at
from ego_byu.tools import kin_tools as kt
from scripts.path_planner.QPIK import PlanQPIK
import time
from copy import deepcopy
from dynamic_reconfigure.server import Server as DynamicReconfigureServer
from byu_robot.cfg import commanderConfig as ConfigType


class State_Commander():

    def __init__(self):
        # General
        self.DOF = rospy.get_param("/DOF")
        self.q = [0.]*self.DOF
        self.qdot = [0.]*self.DOF
        self.t = rospy.get_rostime().to_sec()
        self.g_world_base = []
        self.g_world_tool = []
        self.g_world_objects = []
        self.PrimaryObjectIndex = 0
        self.SecondaryObjectIndex = 1
        self.object_wts = []
        self.Pws = []
        self.Rws = []
        self.mode = rospy.get_param("state_commander/Command_Mode")

        # Start Subscriber/Publisher
        self.rate = rospy.Rate(rospy.get_param("state_commander/Command_Rate"))
        self.dt = 1.0/(rospy.get_param("state_commander/Command_Rate"))
        self.publisher = rospy.Publisher('/commands', commands, queue_size=1000)
        self.subscriber = rospy.Subscriber('/states', states, self.Callback)

        # Planner Parameters
        self.got_estimates = False
        self.Visualize_Planning = True

        # Other Mode Parameter Defaults
        self.delta_tol = rospy.get_param("state_commander/Update_Command_Tolerance")
        self.q_manual_command = [0.]*self.DOF
        self.MotionTime = rospy.get_param("state_commander/MotionTime")

        # Dynamic Reconfigure Module
        self.server = DynamicReconfigureServer(ConfigType, self.Reconfigure)
        
        # Internal Model
        self.CheckForBase()  # Ensures g_world_base is loaded from subscriber
        
        self.base = con.FixedKrex(self.g_world_base)
        self.arm = at.ArmFromYAML()
        self.qpik = qpik.QPIK_SINGLE_ARM(self.arm, True)  # Used for IK later

    def Reconfigure(self, config, level):
        self.MotionTime = config["MotionTime"]
        self.mode = config['Mode']
        rospy.set_param('state_commander/Command_Mode',self.mode)

        # Set Primary Object Index, If index out of bounds, do nothing.
        if config['PrimaryObjectIndex']+1 <= len(self.g_world_objects):
            self.PrimaryObjectIndex = config['PrimaryObjectIndex']
        else:
            config['PrimaryObjectIndex'] = self.PrimaryObjectIndex
        self.SecondaryObjectIndex = config['SecondaryObjectIndex']
        self.ArmsInPlanner = config['ArmsInPlanner']

        # Visualize planning GUI - Set false to automatically begin after planning
        self.Visualize_Planning = config["Visualize_Planning"]

        # If Planner is defined, kill it
        try:
            self.Planner.Toplevel.KillSwitch = config["QuitPlanning"]
        except:
            pass

        # Manual mode - Set config[] as well to update GUI from current estimates
        if self.mode == 8:
            for i in range(self.DOF):
                self.q_manual_command[i] = config["q"+str(i)]
        else:
            for i in range(self.DOF):
                config["q"+str(i)] = self.q[i]
        return config

    def Callback(self, msg):
        
        self.q = list(msg.q)
        self.qdot = list(msg.qdot)
        self.g_world_base = kt.ax_veclist2homog(msg.BasePose)
        if list(msg.ToolPose) != []:
            self.g_world_tool = kt.ax_veclist2homog(msg.ToolPose)

        # Object and object weights
        gs = []
        object_wts = []
        Pw = []
        Rw = []

        # Strip message for gs, and weights
        for i in range(len(msg.ObjectPoses)/6):
            g_list = msg.ObjectPoses[i*6:(i+1)*6]
            g_weight = msg.ObjectWeights[i*6:(i+1)*6]
            if g_list[-1] == 20:  # Not Found
                gs.append([])
                object_wts.append([])
                Pw.append([])
                Rw.append([])
            else:
                gs.append(kt.ax_veclist2homog(g_list))
                object_wts.append(np.diag(g_weight[3:]+g_weight[:3]))
                Pw.append(g_weight[3:])
                Rw.append(g_weight[:3])
        self.g_world_objects = gs
        self.object_wts = object_wts
        self.Pws = Pw
        self.Rws = Rw
        self.got_estimates = True

    def Run(self):
        self.CheckForBase()
        self.UpdateRosParams()
        # Prepare for motion (Planning Occurs Here)
        if self.mode == 0:
            self.PrepareEstimatedStateCommander()
        elif self.mode == 1:
            self.PrepareZeroStateCommander()
        elif self.mode == 2:
            self.PrepareIKToPose()
        elif self.mode == 3:
            self.PreparePlanToPose()
        elif self.mode == 4:
            self.PreparePNP()
        elif self.mode == 5:
            self.PrepareSearchGround()
        elif self.mode == 6:
            self.PrepareBracePickup()
        elif self.mode == 7:
            self.PrepareCurlLastJoints()
        elif self.mode == 8:
            self.PrepareManualControl()
        elif self.mode == 9:
            self.PrepareManualControl()
        else:
            print "Mode not recognized"
            assert(False)
        print "Done Preparing"

        current_mode = self.mode
        start_time = time.time()
        print "Task ", self.mode, " Initiated."

        # Execute Motion
        while not rospy.is_shutdown():
            # Check if Mode Changed
            if self.mode != current_mode:
                break  # Break and restart run loop

            message = commands()
            self.UpdateCommands(time.time()-start_time)
            message.q = self.q_command
            message.qdot = self.qdot_command

            self.publisher.publish(message)
            self.rate.sleep()

        if self.mode == "Quit" or rospy.is_shutdown():
            print "Exiting"
            exit()
        else:
            self.Run()

    # Outdated funciton - should be in dynamic reconfigure
    def UpdateRosParams(self):
        self.mobile_base = rospy.get_param("MobileBase")
        self.desired_pose = rospy.get_param("MobileBase")

        if self.mobile_base:
            self.base = con.MobileKrex(self.g_world_base)
        else:
            self.base = con.FixedKrex(self.g_world_base)

    def UpdateCommands(self, t):
        if self.FollowPlannedPath:
            i = np.argmax(self.plan_times > t)
            if t > self.plan_times[-1]:
                i = len(self.plan_times)-1
            # Format Message
            msg = commands()
            self.q_command = self.Planner.FinalPathConfigs[i].tolist()
            self.qdot_command = self.Planner.FinalPathVels[i].tolist()
        else:
            # Estimated State Update
            if self.mode == 0:
                # First Time - set q_comman - then check differences
                try:
                    if abs(np.linalg.norm(self.q-self.q_command)) > self.delta_tol:
                        self.q_command = self.q
                        self.qdot_command = self.qdot
                except:
                    self.q_command = self.q
                    self.qdot_command = self.qdot

            # Zero State
            if self.mode == 1:
                self.q_command = [0.]*self.DOF
                self.qdot_command = [0.]*self.DOF

            #SLAM Mode
            if self.mode == 9:
                self.q_command = [0.]*self.DOF
                self.qdot_command = [0.]*self.DOF

            # QPIK - Servoing Mode - Takes twist between g_world_tool and
            # g_world_object[Primary] and uses QPIk to drive arm in that motion
            if self.mode == 2:
                # Define Desired Pose (Passed in Manually or Through Sensor)
                # Find q_command through IK solver
                self.qpik.angs = np.array([self.q]).T
                self.qpik.arm.calcFK()
                g_tool_target = kt.homog_inv(
                    self.g_world_tool).dot(
                    self.g_world_objects[
                        self.PrimaryObjectIndex])
                self.qpik.Pf = self.object_wts[self.PrimaryObjectIndex]
                self.qpik.step(g_tool_target)
                self.q_command = self.qpik.angs.flatten()
                # self.qdot_command = [(A-B)/self.dt for A, B in zip(self.q_command, self.q)]
                self.qdot_command = [0.]*self.DOF

            # Curl Mode - Curls joints
            if self.mode == 7:
                q = self.q
                for i in range(1, 5):
                    if np.linalg.norm(q[-i]+np.pi/2.0) > .1:
                        q[-i] = q[-i]-.10*(q[-i]+np.pi/2.0)
                        break
                self.q_command = q
                self.qdot_command = self.qdot

            if self.mode == 8:
                self.q_command = self.q_manual_command
                self.qdot_command = self.DOF*[0.0]

    def InitializeAndRunPlanner(self, path, constraints, jac):
        # Get Joint Angles
        arms = con.YAMLArms()

        self.Planner = PlanQPIK.Planner(path, arms, self.base, constraints, True, self.q)
        self.Planner.smoothpts = rospy.get_param(
            "state_commander/Command_Rate")*rospy.get_param("state_commander/MotionTime")
        self.Planner.Toplevel.max_iter = rospy.get_param("state_commander/MaxIterations")
        self.Planner.Toplevel.jacobian_used = jac

        self.Planner.Plan()
        self.Planner.ScaleFinalPathTimes(self.MotionTime)
        if self.Visualize_Planning:
            self.Planner.LaunchGUI()

        self.q_plans = self.Planner.FinalPathConfigs
        self.qdot_plans = self.Planner.FinalPathVels
        self.plan_times = self.Planner.FinalPathTimes

        # Reposition KREX
        if self.mobile_base:
            print "Path has been planned with Mobile Base Assumption."
            print "Move to:"
            print planner.arms[0].g_world_arm_
            print "Then apply replan with MobileBase Option Turned Off"
            rospy.is_shutdown()

    def PrepareBracePickup(self):
        # rospy.sleep(1)
        self.FollowPlannedPath = True
        rb, tb = kt.hom2ax_vec(self.g_world_objects[self.SecondaryObjectIndex])
        rb, tt = kt.hom2ax_vec(self.g_world_objects[self.PrimaryObjectIndex])
        constraints = []
        constraints.append(geom.Ground())
        ct = {'shape': "Plane",
              'point': tb.reshape(3, 1),
              'normal': self.g_world_brace[:3, 2].reshape(3, 1),
              "weight": 1,
              "type": "touch",
              "link_nums": np.array([3])}
        constraints.append(ct)
        ct = {'shape': "Sphere",
              'point': tt.reshape(3, 1),
              'radius': .05,
              "weight": .1,
              "type": "collision",
              "link_nums:": np.array([-5, -4, -3, -2])}
        constraints.append(ct)
        g1 = self.arm.getGWorldTool()
        g2 = deepcopy(self.g_world_objects[self.PrimaryObjectIndex])
        g2[0, 3] = g2[0, 3]-.10
        path = paths.LineBetween(g1, g2, self.Pws[self.PrimaryObjectIndex], self.Rws[self.PrimaryObjectIndex])
        jac = "end_effector"
        self.InitializeAndRunPlanner(path, constraints, jac)

    def PreparePNP(self):
        self.CheckForObject()
        self.FollowPlannedPath = True
        gpick = self.g_world_objects[self.PrimaryObjectIndex]
        gplace = kt.ax_veclist2homog([0, np.pi, 0, 0, -.4, .5])
        path = paths.PNP(gpick, gplace)
        constraints = []
        constraints.append(geom.Ground())
        jac = "end_effector"
        self.InitializeAndRunPlanner(path, constraints, jac)

    def PrepareIKToPose(self):
        self.FollowPlannedPath = False
        self.CheckForObject()
        self.CheckForTool()

    def PreparePlanToPose(self):
        self.CheckForObject()
        self.FollowPlannedPath = True
        g1 = self.arm.getGWorldTool()
        g2 = self.g_world_objects[self.PrimaryObjectIndex]
        path = paths.LineBetween(g1, g2, self.Pws[self.PrimaryObjectIndex], self.Rws[self.PrimaryObjectIndex])
        constraints = []
        constraints.append(geom.Ground())
        jac = "end_effector"
        self.InitializeAndRunPlanner(path, constraints, jac)

    def PrepareZeroStateCommander(self):
        self.FollowPlannedPath = False

    def PrepareSearchGround(self):
        self.CheckForObject()
        self.FollowPlannedPath = True
        offset = self.g_world_objects[self.PrimaryObjectIndex]
        dx = .35
        dy = .10
        path = paths.Surface(self.ArmsInPlanner, offset, 7, dx, dy)

        self.qpik.angs = np.array([self.q]).T
        self.qpik.arm.calcFK()
        g_world_tool = self.qpik.arm.getGWorldTool()
        constraints = []
        constraints.append(geom.Ground())
        jac = 2  # second to last link used instead of end effector to track path
        self.InitializeAndRunPlanner(path, constraints, jac)

    def PrepareEstimatedStateCommander(self):
        self.FollowPlannedPath = False

    def PrepareCurlLastJoints(self):
        self.FollowPlannedPath = False

    def PrepareManualControl(self):
        self.FollowPlannedPath = False

    def CheckForBase(self):
        while self.g_world_base == [] and not rospy.is_shutdown():
            print "Waiting For Base Data (Commander)"
            rospy.sleep(3.0)
        if rospy.is_shutdown():
            exit()

    def CheckForObject(self):
        while self.g_world_objects == [] and not rospy.is_shutdown():
            print "Waiting for Object Data (Commander)"
            rospy.sleep(3.0)
        if rospy.is_shutdown():
            exit()

    def CheckForTool(self):
        while self.g_world_tool == [] and not rospy.is_shutdown():
            print "Waiting for Tool Data (Commander)"
            rospy.sleep(3.0)
        if rospy.is_shutdown():
            exit()

if __name__ == '__main__':
    rospy.init_node('Joint_Commander', anonymous=False)
    commander = State_Commander()
    commander.Run()
