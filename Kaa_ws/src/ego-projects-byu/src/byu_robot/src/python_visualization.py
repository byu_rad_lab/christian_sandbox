#!/usr/bin/env python

import rospy
import numpy as np
from geometry_msgs.msg import PoseStamped
import tf.transformations as tft
from threading import RLock
import copy
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import arm_tools as at
from ego_byu.tools import arm_plotter as ap
import matplotlib.pyplot as plt
from byu_robot.msg import states
from byu_robot.msg import commands
from tf2_msgs.msg import TFMessage
from ego_byu.tools import pickle_tools as pk
import os

from dynamic_reconfigure.server import Server as DynamicReconfigureServer
from byu_robot.cfg import visualizationConfig as ConfigType

class Visualization(object):
    def __init__(self):
        self.subscriber = rospy.Subscriber('/states', states, self.states_callback)
        self.subscriber = rospy.Subscriber('/commands', commands, self.commands_callback)

        self.DOF = rospy.get_param("/DOF")
        self.q_states = [0.]*self.DOF
        self.q_commands = [0.]*self.DOF
        self.g_world_base = []
        self.g_world_target = []
        self.g_basetracker_base = kt.ax_veclist2homog(rospy.get_param("state_estimator/g_basetracker_base"))

        self.got_estimates = False
        self.got_commands = False

        self.server = DynamicReconfigureServer(ConfigType, self.Reconfigure)

        #Internal Model
        while(self.got_estimates == False or self.got_commands == False) and not rospy.is_shutdown():
            rospy.sleep(.1)

        self.arm_states = at.ArmFromYAML()
        self.arm_states.setJointAngsReal(np.array(self.q_states))
        self.arm_states.calcFK()

        self.arm_commands = at.ArmFromYAML()
        self.arm_commands.setJointAngsReal(np.array(self.q_commands))
        self.arm_commands.calcFK()

        #Plotters
        self.state_plotter = ap.SimpleArmPlotter(self.arm_states,'k')
        self.command_plotter = ap.SimpleArmPlotter(self.arm_commands,'k')
        self.command_plotter.alpha = .5
        self.fig = plt.figure(1)
        self.ax = plt.gca(projection="3d")

        if rospy.get_param("Plot_Vive_Frames") and rospy.get_param("state_estimator/JointEstimation") == "Vive":
            self.World_Vive_Num = rospy.get_param("state_estimator/World_Vive_Num")
            self.Base_Vive_Num = rospy.get_param("state_estimator/Base_Vive_Num")
            self.Link_Vive_Nums = rospy.get_param("state_estimator/Link_Vive_Nums")
            self.Object_Vive_Nums = rospy.get_param("state_estimator/Object_Vive_Nums")
            self.Estimation_Vive_Links = rospy.get_param("state_estimator/Estimation_Vive_Links")

            #Load Calibrated Zero State from calibrations folder - These were set in the Vive Estimator
            self.cal_path = os.environ['EGO_BYU']+"/python/ego_byu/calibrations/"
            self.g_tracker_tops =pk.load_pickle(self.cal_path+'calibration_vive_offsets_tops')
            self.g_tracker_ends =pk.load_pickle(self.cal_path+'calibration_vive_offsets_ends')

            # For Vive to potentially fill
            self.g_lighthouse_world = []
            self.g_lighthouse_base = []
            self.g_lighthouse_links = [[] for i in range(len(self.Link_Vive_Nums))]
            self.g_lighthouse_objects = [[] for i in range(len(self.Object_Vive_Nums))]

            rospy.Subscriber('/tf', TFMessage, self.ViveCallback)
            rospy.sleep(1.0)

    def Reconfigure(self,config,level):
        self.hang = config["HorzAng"]
        self.vang = config["VertAng"]
        self.z = config["ZoomFactor"]
        return config

    def ViveCallback(self, msg):
        frame_id = msg.transforms[0].child_frame_id
        if frame_id[:3] == "neo":
            i = int(msg.transforms[0].child_frame_id[-1])
            q = msg.transforms[0].transform.rotation
            t = msg.transforms[0].transform.translation
            g = tft.quaternion_matrix([q.x, q.y, q.z, q.w])
            g[:3, 3] = [t.x, t.y, t.z]
            if i == self.World_Vive_Num:
                self.g_lighthouse_world = g
                self.g_world_lighthouse = kt.homog_inv(g)
            if i == self.Base_Vive_Num:
                self.g_lighthouse_base = g.dot(self.g_basetracker_base)
            for x in range(len(self.Link_Vive_Nums)):
                if i == self.Link_Vive_Nums[x]:
                    self.g_lighthouse_links[x] = g
            for x in range(len(self.Object_Vive_Nums)):
                if i == self.Object_Vive_Nums[x]:
                    self.g_lighthouse_objects[x] = g

    def states_callback(self, msg):
        self.q_states = msg.q
        self.g_world_base = kt.ax_veclist2homog(msg.BasePose)
        self.g_world_objects = []
        for i in range(len(msg.ObjectPoses)/6):
            self.g_world_objects.append(kt.ax_veclist2homog(msg.ObjectPoses[i*6:(i+1)*6]))
        self.g_world_tool = kt.ax_veclist2homog(msg.ToolPose)

        self.got_estimates = True

    def commands_callback(self, msg):
        self.q_commands = msg.q
        self.got_commands = True

    def Run(self):
        while not rospy.is_shutdown():
            self.UpdateArms()
            self.Plot()

    def UpdateArms(self):
        self.arm_states.g_world_arm_ = self.g_world_base
        self.arm_commands.g_world_arm_ = self.g_world_base
        self.arm_states.setJointAngsReal(np.array(self.q_states))
        self.arm_commands.setJointAngsReal(np.array(self.q_commands))
        self.arm_states.calcFK()
        self.arm_commands.calcFK()

    def Plot(self):
        plt.cla()
        if rospy.get_param("Estimated_States_Viz"):
            self.state_plotter.plot(self.ax)
        if rospy.get_param("Commanded_States_Viz"):
            self.command_plotter.plot(self.ax)
        self.ax.set_xlim3d(-.5*self.z, 1.5*self.z)
        self.ax.set_ylim3d(-1*self.z, 1*self.z)
        self.ax.set_zlim3d(-1*self.z, 1*self.z)
        self.ax.view_init(elev=self.vang, azim=self.hang)
        ap.plot_gnomon(self.ax,self.arm_states.getGWorldTool())
        ap.plot_gnomon(self.ax,self.arm_states.g_world_arm_)

        for g in self.g_world_objects:
            ap.plot_gnomon(self.ax,g,.1,5,['g','g','g'])

        ap.plot_gnomon(self.ax,self.g_world_tool,.1,5,['g','g','g'])
        self.g_world_lasttracker = self.g_world_lighthouse.dot(self.g_lighthouse_links[-1])
        ap.plot_gnomon(self.ax,self.g_world_lasttracker,.1,5,['r','g','b'])

        #Drawing Blue frames representing Vive Estimations of where tops of joints are
        if rospy.get_param("Plot_Vive_Frames") and rospy.get_param("state_estimator/JointEstimation") == "Vive":
            for i,frame in enumerate(self.g_lighthouse_links):
                g = self.g_world_lighthouse.dot(frame).dot(self.g_tracker_tops[i])
                # g = g_world_worldvive.dot(frame)
                ap.plot_gnomon(self.ax,g,.1,5,['b','b','b'])


        plt.figure(1).canvas.draw()
        plt.figure(1).show()



if __name__ == '__main__':
    if rospy.get_param("/Visualization") == True:
        rospy.init_node('Visualization', anonymous=False)
        visualization = Visualization()
        visualization.Run()
