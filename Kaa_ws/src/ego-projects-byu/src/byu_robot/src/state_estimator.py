#!/usr/bin/env python

import os
import rospy
import numpy as np
from math import *
from geometry_msgs.msg import PoseStamped
from byu_robot.msg import commands
from byu_robot.msg import sim_robot
from byu_robot.msg import states

import tf.transformations as tft
from tf2_msgs.msg import TFMessage
from threading import RLock
from copy import copy
from ego_byu.tools import kin_tools as kt
from ego_byu.tools import pickle_tools as pk
from ego_byu.tools import arm_tools as at
# from ego_byu.madgwick import madgwick as md  #For imu subscriber, if needed uncomment line with md further down
#from ego_byu.tools import IMU_Filter as imu #For imu subscriber, if needed uncomment lines with imu 
from dynamic_reconfigure.server import Server as DynamicReconfigureServer
from byu_robot.cfg import estimatorConfig as ConfigType

# Base subscriber class that contains callbacks, methods, and variables shared by all estimation methods


class State_Estimator(object):

    def __init__(self):
        # Global Variables
        self.lock = RLock()
        self.DOF = rospy.get_param("/DOF")
        self.NumSensors = rospy.get_param("state_estimator/NumberOfSensors")
        self.robotName = rospy.get_param("state_estimator/Robot_Name")

        self.q_prev = [0.]*self.DOF
        self.t = rospy.get_time()
        self.t_prev = rospy.get_time()-1.0/rospy.get_param("state_estimator/Estimation_Rate")

        #Calibrations will be set to the path "EGO_BYU" as set in bashrc - IMU and Vive offsets are saved here
        #e.g. export PYTHONPATH=$PYTHONPATH:/home/radlab/git/ego/ego-projects-byu
        #     export EGO_BYU=/home/radlab/git/ego/ego-projects-byu
        self.cal_path = os.environ['EGO_BYU']+"/python/ego_byu/calibrations/"

        # Message Variables - Must be filled in ComputeStates Function
        self.q = [0.]*self.DOF  # Joint Angles
        self.qdot = [0.]*self.DOF
        self.pPlus = [0.]*self.DOF
        self.pMinus = [0.]*self.DOF
        self.g_world_base = []
        self.g_world_tool = []
        self.g_world_objects_list = []  # Default is no objects

        #Options for later in dynamic reconfigure
        self.FixObjectFrames = False # -Stop updating g_world_objects_list when selected
        self.SetToolOffset = False #resets g_lasttracker_tool to g_lasttracker_object1

        #Pfs refers to weighitngs assigned on targets - e.g. 0,0,0,1,1,1 weighitng is all on position, nothing orientation
        self.Pfs = list(np.array([rospy.get_param("state_estimator/Object_Weighting_Default") for i in range(2)]))

        # How are we going to estimate the Base, joint angles and objects? - YAML
        self.BaseEstMode = rospy.get_param("state_estimator/BaseEstimation")
        self.JointEstMode = rospy.get_param("state_estimator/JointEstimation")
        self.ObjectEstimation = rospy.get_param("state_estimator/ObjectEstimation")

        # Dynamic Reconfigure Module
        self.server = DynamicReconfigureServer(ConfigType, self.Reconfigure)

        # Start All Necessary Subscribers
        if rospy.get_param("state_estimator/Robot_Subscriber"):
            if self.robotName == "Kaa":
                from byu_robot.msg import robot_state
                rospy.Subscriber("/robot_state", robot_state, self.RobotCallback)
            elif self.robotName == "KingLouieRight":
                from low_level_control.msg import sensors
                rospy.Subscriber("/PressureData", sensors, self.RobotCallback)

        if rospy.get_param("state_estimator/Tactile_Sensor"):
            self.rows = rospy.get_param("state_estimator/Rows")
            self.cols = rospy.get_param("state_estimator/Cols")
            self.ADC_reading = np.array(np.zeros([self.rows, self.cols]))
            rospy.Subscriber("/serial", String, self.TactileCallback, tcp_nodelay=False)

        if rospy.get_param("state_estimator/Vive_Subscriber"):
            self.Load_Print_Sensor_Nums()

            # For Vive Subscriber to fill
            self.g_lighthouse_world = []
            self.g_world_lighthouse = []  # Default is no targets
            self.g_lighthouse_base = []
            self.g_lighthouse_linktrackers = [[] for i in range(len(self.Link_Vive_Nums))]
            self.g_lighthouse_objects = [[] for i in range(len(self.Object_Vive_Nums))]

            self.g_basetracker_base = kt.ax_veclist2homog(rospy.get_param("state_estimator/g_basetracker_base"))
            self.g_lasttracker_tool = kt.ax_veclist2homog(rospy.get_param("state_estimator/g_lasttracker_tool"))
            rospy.Subscriber('/tf', TFMessage, self.ViveCallback)

        if rospy.get_param("state_estimator/Cortex_Subscriber"):
            self.Load_Print_Sensor_Nums()
            self.g_cortex_base = []
            self.g_cortex_links = [[] for i in range(self.Link_Cortex_Nums)]
            self.g_cortex_objects = [[] for i in range(self.Object_Cortex_Nums)]
            self.JointAxes = rospy.get_param("ControllableJointAxes")

            rospy.Subscriber('/base/base', PoseStamped, self.callback, ("base"))
            for i in self.Link_Cortex_Nums:
                name = 'link'+str(i)
                rospy.Subscriber("/"+name+"/"+name, PoseStamped, self.CortexCallback, (name))
            for i in self.Object_Cortex_Nums:
                name = 'object'+str(i)
                rospy.Subscriber("/"+name+"/"+name, PoseStamped, self.CortexCallback, (name))

        if rospy.get_param("state_estimator/IMU_Subscriber"):
            gain = rospy.get_param("state_estimator/IMU_Gain")
            calibration = rospy.get_param("state_estimator/IMUCalibrated")
            publish = rospy.get_param("Publish_IMU_Frames_To_Rviz")
            # world_frame = md.NWU
            self.Base_IMU_Num = rospy.get_param('state_estimator/Base_IMU_Num')
            self.Link_IMU_Nums = rospy.get_param('state_estimator/Link_IMU_Num')
            self.IMU_Nums = self.Base_IMU_Num + self.Link_IMU_Nums

            # Subscribers held in imu_set - Base_IMU is not necessarily aligned with base
            #self.imu_set = imu.IMU_Set(self.IMU_Nums, world_frame, gain, calib=calibration, pub=publish)

        if rospy.get_param("state_estimator/Simulator_Subscriber"):
            rospy.Subscriber('/sim_robot_state', sim_robot, self.SimulatorCallback)

        if rospy.get_param("state_estimator/Command_Subscriber"):
            self.alpha = .05
            print "Starting Command Subscriber"
            rospy.Subscriber('/commands', commands, self.CommandCallback)

        # Ensure Consistent Options Are Chosen in YAML
        self.OptionsCheck()

        # OUTPUT Publisher
        self.publisher = rospy.Publisher('/states', states, queue_size=1000)
        self.rate = rospy.Rate(rospy.get_param("state_estimator/Estimation_Rate"))

        #Now that subscribers are running, we set g_world_base
        self.SetBase()

        #We calibrate offsets (sensors to top and bottom of joints)
        self.CalibrateSystem()
        rospy.set_param("state_estimator/SystemCalibrated", True)

    def Reconfigure(self, config, level):
        #Source of Object Frames that will be used in planner
        self.ObjectEstimation = config["ObjectEstimation"]

        #Option to stop updating frames (Set them in place)
        self.FixObjectFrames = config["FixObjectFrames"]

        #Option to set the tool offset using a sensor instead of the default in YAML
        if config["SetToolOffset"] and self.ObjectEstimation == 1 and self.g_world_objects_list != []:
            g_world_object = kt.ax_veclist2homog(self.g_world_objects_list[:6])
            g_world_lasttracker = self.g_world_lighthouse.dot(self.g_lighthouse_linktrackers[-1])
            self.g_lasttracker_tool = kt.homog_inv(g_world_lasttracker).dot(g_world_object)
            rospy.set_param("g_lasttracker_tool", kt.hom2ax_vec_list(self.g_lasttracker_tool))
        config["SetToolOffset"] = False

        #Weightings on Object Frames
        self.Pfs = map(float, config["Object1WeightingVector"].split(
            ',')) + map(float, config["Object2WeightingVector"].split(','))

        #May manually input objects if needs be
        if self.ObjectEstimation == 0:
            self.g_world_objects_list = map(float, config["Object1Vector"].split(
                ','))+map(float, config["Object2Vector"].split(','))

        #Checks options to maintain consistency
        self.OptionsCheck()
        return config

    def SimulatorCallback(self, msg):
        self.q = msg.q
        self.qdot = msg.qdot
        self.pPlus = msg.pPlus
        self.pMinus = msg.pMinus
        self.t = rospy.get_time()

    def CommandCallback(self, msg):
        self.q = [(A-B)*self.alpha+B for A, B in zip(msg.q, self.q)]
        self.t = rospy.get_time()

    def RobotCallback(self, msg):
        if self.robotName == "Kaa":
            for i in xrange(self.DOF):
                self.pPlus[i] = msg.sensors[i].prs_cal[0]
                self.pMinus[i] = msg.sensors[i].prs_cal[1]
        elif self.robotName == "KingLouieRight":
            # !!!!------Data is converted to PSI ------!!!!!!
            paps = 0.000145037738 
            poff = 14.6959488
            j = 0
            for i in xrange(self.DOF):
                self.pPlus[i] = msg.p[2*j]*self.paps - self.poff
                self.pMinus[i] = msg.p[2*j+1]*self.paps - self.poff
                # This is done to skip the elbow data reported by sensors
                if j == 1:
                    j = j + 2
                else:
                    j = j + 1
            

    #Callback for neo_trackers#
    def ViveCallback(self, msg):
        #Pull frame_id out of the message - the vive neo_trackers reported in rviz must line up with indices set in the YAML file
        frame_id = msg.transforms[0].child_frame_id
        if frame_id[:3] == "neo":
            i = int(msg.transforms[0].child_frame_id[-1])
            q = msg.transforms[0].transform.rotation
            t = msg.transforms[0].transform.translation
            g = tft.quaternion_matrix([q.x, q.y, q.z, q.w])
            g[:3, 3] = [t.x, t.y, t.z]
            if i == self.World_Vive_Num:
                self.g_lighthouse_world = g
                self.g_world_lighthouse = kt.homog_inv(g)
            if i == self.Base_Vive_Num:
                self.g_lighthouse_base = g.dot(self.g_basetracker_base)
            for x in range(len(self.Link_Vive_Nums)):
                if i == self.Link_Vive_Nums[x]:
                    self.g_lighthouse_linktrackers[x] = g
            for x in range(len(self.Object_Vive_Nums)):
                if i == self.Object_Vive_Nums[x]:
                    self.g_lighthouse_objects[x] = g

            self.t = msg.transforms[0].header.stamp.to_sec()

    def CortexCallback(self, msg, name):
        q = [msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w]
        t = [msg.pose.position.x, msg.pose.position.y, msg.pose.position.z]
        g = tft.quaternion_matrix(q)
        g[:3, 3] = [t.x, t.y, t.z]
        if name == "base":
            self.g_cortex_base = g
        elif name[:4] == "link":
            i = int(name[-1])
            for x in range(len(self.Link_Cortex_Nums)):
                if i == self.Link_Cortex_Nums[x]:
                    self.g_cortex_links[x] = g
        elif name[:3] == "object":
            i = int(name[-1])
            for x in range(len(self.Object_Cortex_Nums)):
                if i == self.Object_Cortex_Nums[x]:
                    self.g_cortex_objects[x] = g

        self.t = msg.header.stamp.to_sec()

    def TactileCallback(self, msg):
        self.data_in = msg.data
        data = np.fromstring(self.data_in, count=self.cols+1, sep=',')
        self.ADC_reading[data[0]] = data[1:self.cols+1]

    #This function takes a rotation matrix, and the axes around which the joint is bending and returns joint angs.
    def AngsFromRot(self, R21, joint_type):
        # R21 rotates the proximal frame (1) into the distal frame (2) (R_(bot_i+1)_(end_i))
        qs = []
        if joint_type == "XY":
            qs.append(atan2(R21[1, 2], R21[1, 1]))
            qs.append(atan2(R21[2, 0], R21[0, 0]))
        elif joint_type == "YX":
            qs.append(atan2(-R21[0,2],R21[0,0]))
            qs.append(atan2(-R21[1,2],R21[1,1]))
        elif joint_type == "X":
            qs.append(atan2(R21[1, 2], R21[1, 1]))
        elif joint_type == "Y":
            qs.append(atan2(R21[2, 0], R21[0, 0]))
        elif joint_type == "-YZ":
            qs.append(atan2(-R21[1, 0], R21[1, 1]))
            qs.append(atan2(R21[0, 2], R21[2, 2]))
        elif joint_type == "Z-Y":
            qs.append(atan2(-R21[2, 1], R21[2, 2]))
            qs.append(atan2(R21[0, 1], R21[1, 1]))
        elif joint_type == "Z":
            qs.append(atan2(R21[0, 2], R21[0, 0]))
        elif joint_type == "-Y":
            qs.append(atan2(R21[0, 1], R21[0, 0]))
        return qs

    # Ensures all data members are being taken from subscribers for joint estimation
    # Compute Offsets in the Zero Configuration
    def CalibrateSystem(self):
        # IMU's Previously Calibrated in imu_set instantiation
        if rospy.get_param("state_estimator/IMU_Subscriber") and rospy.get_param("state_estimator/JointEstimation") == "IMUs":

            # SetBase Called -> g_world_base set
            arm = at.ArmFromYAML()
            arm.setJointAngsZero()
            arm.calcFK()

            self.JointTypes = rospy.get_param('state_estimator/Estimation_IMU_JointTypes')
            self.IMU_Links = rospy.get_param('state_estimator/Estimation_IMU_Links')

            # TODO:  We're using the first IMU to mark "q_imuworld_base".
            print "Taking q_imuworld_base from zeroed IMU"
            parameters = pk.load_pickle(self.cal_path+'calibration_imu'+str(self.Base_IMU_Num))
            self.q_imuworld_base = parameters['q_imuworld_zero']  # TODO
            g_base_world = kt.homog_inv(arm.g_world_arm())

            # Set IMU_Base
            self.imu_set.IMUs[0].q_imu_top = [0, 0, 0, 1.0]  # .dot(g_zero_base) #TODO
            self.imu_set.IMUs[0].q_imu_end = [0, 0, 0, 1.0]  # .dot(g_zero_base) #TODO
            self.imu_set.IMUs[0].q_imuworld_base = self.q_imuworld_base

            for i in range(len(self.IMU_Links)):
                imu_link = int(self.IMU_Links[i])
                self.imu_set.IMUs[i].q_imuworld_base = self.q_imuworld_base
                q_base_top = tft.quaternion_from_matrix(g_base_world.dot(arm.links_[imu_link].g_world_top()))
                q_base_end = tft.quaternion_from_matrix(g_base_world.dot(arm.links_[imu_link].g_world_end()))
                q_imu_imuworld = tft.quaternion_inverse(self.imu_set.IMUs[i].q_imuworld_zero)
                q_imu_base = tft.quaternion_multiply(q_imu_imuworld, self.q_imuworld_base)
                self.imu_set.IMUs[i].q_imu_top = tft.quaternion_multiply(q_imu_base, q_base_top)
                self.imu_set.IMUs[i].q_imu_end = tft.quaternion_multiply(q_imu_base, q_base_end)
            self.dts = np.zeros([len(self.JointTypes)])
        # Cortex's Previously Calibrated in set up (must have frames in zero
        # configuration when marker set made)
        elif rospy.get_param("state_estimator/Cortex_Subscriber") and rospy.get_param("state_estimator/JointEstimation") == "Cortex":
            if self.quatBase == [] or all(v == [] for v in self.quatLinks):
                print "Waiting For Cortex to Send Base and Link Positions"
            self.JointTypes = rospy.get_param('state_estimator/Estimation_Cortex_JointTypes')
        # Calibrate Vive offsets using kinematic model
        elif rospy.get_param("state_estimator/Vive_Subscriber") and rospy.get_param("state_estimator/JointEstimation") == "Vive":
            raw_input("Set Arm In Zero State and Press Enter")
            while (any(g ==[] for g in self.g_lighthouse_linktrackers) and not rospy.is_shutdown()):
                print "ERROR: Link Tracker Not Read"
                rospy.sleep(1.0)

            arm = at.ArmFromYAML()
            arm.setJointAngsZero()
            arm.calcFK()

            # Compute g_tracker_top and g_tracker_b for every set
            self.Estimation_Vive_Links = rospy.get_param("state_estimator/Estimation_Vive_Links")
            self.g_linktracker_tops = [[] for i in range(len(self.Estimation_Vive_Links))]  # top of joint (bottom link)
            self.g_linktracker_ends = [[] for i in range(len(self.Estimation_Vive_Links))]  # end of i+1 joint  (end of link)
            for i, vive_link in enumerate(self.Estimation_Vive_Links):
                g_linktracker_world = kt.homog_inv(
                    self.g_lighthouse_linktrackers[i]).dot(
                    self.g_lighthouse_world)
                #Get top and end frames using kinematics
                g_world_top = arm.links_[int(vive_link)].g_world_top()
                g_world_end = arm.links_[int(vive_link)].g_world_end()
                self.g_linktracker_tops[i] = g_linktracker_world.dot(g_world_top)
                self.g_linktracker_ends[i] = g_linktracker_world.dot(g_world_end)

            #save off pickle for visualization later
            pk.save_pickle(self.cal_path+'calibration_vive_offsets_tops', self.g_linktracker_tops)
            pk.save_pickle(self.cal_path+'calibration_vive_offsets_ends', self.g_linktracker_ends)

            # Offsets between Sensors and frames are now calibrated and estimation can take place
            self.JointTypes = rospy.get_param('state_estimator/Estimation_Vive_JointTypes')
        else:
            print "Error: Specified YAML options don't allow for estimation"
            assert(False)
        print "Arm Calibrated and Base Located"


    # Find and set rosparam g_world_base
    def SetBase(self):
        print "SETTING BASE TRANSFORM"
        if self.BaseEstMode == "Vive":
            while (self.g_world_lighthouse == [] or self.g_lighthouse_base == []) and not rospy.is_shutdown():
                print "Waiting For Base Data (Estimator)"
                rospy.sleep(1.0)
            if rospy.is_shutdown():
                exit()
            self.g_world_base = self.g_world_lighthouse.dot(self.g_lighthouse_base)
            print "SET BASE TO TRANSFORM BETWEEN WORLD AND BASE TRACKERS"
        elif self.BaseEstMode == "Cortex":
            while self.g_cortex_base == [] and not rospy.is_shutdown():
                print "Waiting For Base Data (Estimator)"
            self.g_world_base = self.g_cortex_base
            print "SET BASE TO TRANSFORM BETWEEN CORTEX WORLD AND BASE"
        elif self.BaseEstMode == "Default":
            self.g_world_base = kt.ax_veclist2homog(rospy.get_param("g_world_base"))  # Default Value Used
            print "SET BASE TO DEFAULT SPECIFIED IN YAML"
        else:
            print "ERROR: 'BaseEstimation' parameter not recognized"
            assert(False)
        rt = kt.hom2ax_vec_list(self.g_world_base)
        rospy.set_param("g_world_base", rt)

    def ComputeStates(self):
        # Joint Estimation
        self.lock.acquire()
        self.EstimateJointAnglesAndTool()
        self.JointDerivativeEstimation()
        self.EstimateObjects()
        # Pressure Sensors Passed Through Callbacks
        self.lock.release()

    #Finds Joint angles and g_world_tool
    def EstimateJointAnglesAndTool(self):
        
        if self.JointEstMode == "Vive":
            qs = []
            #Finds frames g_world_bot (Rw1) and g_world_top (Rw2) for joints. Then finds joint Rotation R21
            for i, JtType in enumerate(self.JointTypes):
                if i == 0:
                    Rw1 = self.g_lighthouse_base[0:3, 0:3]  # Base vive is lined up with robot first Joint
                else:
                    Rw1 = (self.g_lighthouse_linktrackers[i-1].dot(self.g_linktracker_ends[i-1]))[0:3, 0:3]

                Rw2 = (self.g_lighthouse_linktrackers[i].dot(self.g_linktracker_tops[i]))[0:3, 0:3]
                R21 = Rw2.T.dot(Rw1)
                qs = qs+self.AngsFromRot(R21, JtType)
            self.q = qs
            self.g_world_lasttracker = self.g_world_lighthouse.dot(self.g_lighthouse_linktrackers[-1])
            self.g_world_tool = self.g_world_lasttracker.dot(self.g_lasttracker_tool)
        elif self.JointEstMode == "IMUs":
            qs = []
            for i, JtType in enumerate(self.JointTypes):
                Rw1 = tft.quaternion_matrix(self.imu_set.IMUs[i].q_imuworld_top)[0:3][0:3]
                Rw2 = tft.quaternion_matrix(self.imu_set.IMUs[i+1].q_imuworld_end)[0:3][0:3]
                R21 = Rw2.T.dot(Rw1)
                qs = qs+self.AngsFromRot(R21, JtType)
                self.dts[i] = self.imu_set.IMUs[i].dt
            self.q = qs
        elif self.JointEstMode == "Cortex":
            # Assuming quats in order from base to end
            qs = []
            for i, JtType in enumerate(self.JointTypes):
                if i == 0:
                    Rw1 = self.g_world_base[:3, :3]
                else:
                    Rw1 = tft.quaternion_matrix(self.quatLinks[i-1])[0:3][0:3]  # May Need Transposing
                Rw2 = tft.quaternion_matrix(self.quatLinks[i])[0:3][0:3]
                R21 = Rw2.T.dot(Rw1)
                qs = qs+self.AngsFromRot(R21, JtType)
            self.q = qs
        elif self.JointEstMode == "ZeroState":
            pass  # q left untouched
        elif self.JointEstMode == "Sim" or self.JointEstMode == "CommandState":
            pass  # CommandCallback sets q
        elif self.JointEstMode == "SinState":
            self.t = rospy.get_time().to_sec()
            self.q[0] = 0+.3*np.sin(2*np.pi/3.0*t)
        else:
            print "ERROR: JointEstimation Method Invalid"
            assert(False)
            # Assign Target from tracker if present

    #Estimates where object frames are in the world - object frames are useful for tasks
    def EstimateObjects(self):
        if not self.FixObjectFrames:
            v = np.array([])
            w = np.array([])
            if self.ObjectEstimation == 2: #Objects from Cortex
                for i, g in enumerate(self.g_cortex_objects):
                    if g == []:
                        v = np.append(v, [20, 20, 20, 20, 20, 20])  # Not Found
                        w = np.append(w, [20,20,20,20,20,20])
                    else:
                        v = np.append(v, kt.hom2ax_vec_list(g))
                        w = np.append(w, self.Pfs[i:i+6])
                self.g_world_objects_list = v.tolist()
                self.object_weights = w.tolist()

            if self.ObjectEstimation == 1: #Objects from Vive
                for i, g in enumerate(self.g_lighthouse_objects):
                    if g == []:
                        v = np.append(v, [20,20,20,20,20,20])  # Not Found
                        w = np.append(w, [20,20,20,20,20,20])
                    else:
                        v = np.append(v, kt.hom2ax_vec_list(self.g_world_lighthouse.dot(g)))
                        w = np.append(w, self.Pfs[i:i+6])
                self.g_world_objects_list = v.tolist()
                self.object_weights = w.tolist()

            if self.ObjectEstimation == 0: # Dynamic Reconfigure
                #g_world_objects_list set in reconfig
                self.object_weights = self.Pfs
                pass

    def JointDerivativeEstimation(self):
        # Numerical Differencing - May want to add a low pass filter in the future
        if self.JointEstMode == "IMUs":
            self.qdot = [(A-B)/dt for A, B, dt in zip(self.q, self.q_prev, self.dts)]
        else:
            try:
                self.qdot = [(A-B)/(self.t-self.t_prev) for A, B in zip(self.q, self.q_prev)]
            except:
                # print "No Input Received: Defaulting to Estimator Rate for Derivative Computation"
                dt = 1.0/rospy.get_param("state_estimator/Estimation_Rate")
                self.qdot = [(A-B)/(dt) for A, B in zip(self.q, self.q_prev)]
            self.t_prev = self.t
        self.q_prev = self.q

    def Run(self):
        # Get message from subscriber(s) and publish single state message
        while not rospy.is_shutdown():
            if rospy.get_param("state_estimator/SystemCalibrated"):
                message = states()

                #All Estimation happens in ComputeStates - Certain variables must be set in initialization for this to work.
                self.ComputeStates()

                #Package message to send
                message.q = self.q
                message.qdot = self.qdot
                message.pPlus = self.pPlus
                message.pMinus = self.pMinus
                message.BasePose = kt.hom2ax_vec_list(self.g_world_base)
                if self.g_world_tool != []:
                    message.ToolPose = kt.hom2ax_vec_list(self.g_world_tool)
                message.ObjectPoses = self.g_world_objects_list
                message.ObjectWeights = self.object_weights
                self.publisher.publish(message)
            self.rate.sleep()

    def CheckMessageData(self):
        if self.g_world_tool == []:
            print "warning: g_world_tool unset with current estimation method"
        if self.g_world_base == []:
            print "warning: g_world_base unset with current estimation method"

    def OptionsCheck(self):
        # Ensures no inconsistent YAML settings are give
        if (rospy.get_param("state_estimator/JointEstimation") == "Vive" or rospy.get_param("state_estimator/BaseEstimation") ==
                "Vive" or rospy.get_param("state_estimator/ObjectEstimation") == 1) and not rospy.get_param("state_estimator/Vive_Subscriber"):
            print "ERROR: Turn On Vive Subscriber for Vive Joint, Base, Objects Estimation"
            assert(False)
        if (rospy.get_param("state_estimator/JointEstimation") == "Cortex" or rospy.get_param("state_estimator/BaseEstimation") ==
                "Cortex" or rospy.get_param("state_estimator/ObjectEstimation") == 2) and not rospy.get_param("state_estimator/Cortex_Subscriber"):
            print "ERROR: Turn On Vive Subscriber for Cortex Joint, Base, Objects Estimation"
            assert(False)
        if rospy.get_param("state_estimator/JointEstimation") == "IMU" and not rospy.get_param("state_estimator/IMU_Subscriber"):
            print "ERROR:Turn On Vive Subscriber for Vive Joint Estimation"
            assert(False)
        if rospy.get_param("state_estimator/JointEstimation") == "CommandState" and not rospy.get_param(
                "state_estimator/Command_Subscriber"):
            print "ERROR: Turn On Command Subscriber for Command State Joint Estimation"
            assert(False)
        if self.ObjectEstimation == 1 and rospy.get_param("state_estimator/Vive_Subscriber") == False:
            print "ERROR: Turn On Vive Subscriber for Vive Object Estimation"
            assert(False)

        if len(rospy.get_param("LinkTransforms")) != self.DOF:
            print "ERROR: Check DOF and # of Links"
            assert(False)
        if len(rospy.get_param("ControllableJointAxes")) != self.DOF:
            print "ERROR: Check ControllableJointAxes and DOF"
            assert(False)
        if len(rospy.get_param("JointHeights")) != self.DOF:
            print "ERROR: Check JointHeights and DOF"
            assert(False)

    def Load_Print_Sensor_Nums(self):
        if rospy.get_param("state_estimator/Vive_Subscriber"):
            self.World_Vive_Num = rospy.get_param("state_estimator/World_Vive_Num")
            self.Base_Vive_Num = rospy.get_param("state_estimator/Base_Vive_Num")
            self.Link_Vive_Nums = rospy.get_param("state_estimator/Link_Vive_Nums")
            self.Object_Vive_Nums = rospy.get_param("state_estimator/Object_Vive_Nums")
            print
            print "World Vive Sensor Number: ", self.World_Vive_Num
            print "Base Vive Sensor Number: ", self.Base_Vive_Num
            print "Link Vive Sensor Numbers: ", self.Link_Vive_Nums
            print "Object Vive Sensor Numbers: ", self.Object_Vive_Nums
            print

        if rospy.get_param("state_estimator/Cortex_Subscriber"):
            self.Base_Cortex_Nums = rospy.get_param("state_estimator/Base_Cortex_Num")
            self.Link_Cortex_Nums = rospy.get_param("state_estimator/Link_Cortex_Nums")
            self.Object_Cortex_Nums = rospy.get_param("state_estimator/Object_Cortex_Nums")
            print
            print "Base Cortex Sensor Number: ", self.Base_Cortex_Num
            print "Link Cortex Sensor Numbers: ", self.Link_Cortex_Nums
            print "Object Cortex Sensor Numbers: ", self.Object_Cortex_Nums
            print

if __name__ == '__main__':
    rospy.init_node('Estimation', anonymous=False)
    estimator = State_Estimator()
    estimator.Run()
