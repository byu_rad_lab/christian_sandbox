#!/usr/bin/env python

import rospy
import numpy as np
import math
import time
from copy import deepcopy
import scipy.io as sio
from scipy.linalg import expm
from scipy import linalg
from scipy import signal
from std_msgs.msg import Float64
from threading import RLock, Timer
from collections import deque
from solvers import nasa_1_dof_cons_zero as solver # nasa_1_dof_p_delta #nasa_1_dof_disturbance # nasa_1_dof_dist is the deprecated solver as 
from math import pi, cos, sin
import argparse
import falkor.msg as robot_msgs
import sys
#from nasa_mpc.msg import angles_velocity as angles
#from nasa_mpc.msg import commands
#from nasa_mpc.msg import robot_ready
#from nasa_mpc.msg import send_pressures
from byu_robot.msg import sim_robot

from falkor.srv import set_robot_ctrl_mode
from falkor.srv import move_joint_prs
from falkor.srv import move_joint_valve

#from serial_data.msg import calibration2 as tactileData

from threading import RLock, Timer
from collections import deque
import tf.transformations as tft

from geometry_msgs.msg import PoseArray
#from nasa_4_dof_dyn_params import params
#import nasa_4_dof_dynamics as nasaDyn

import argparse

from models import kaa_dyn_params
import importlib
from controllers import kaa_mpc_n_dof, pid_controller

def main(jointNum,nasaDyn,params,solver):
    mode = rospy.get_param('Controller_Mode')
    if mode == 'MPC':
        rospy.init_node('MPC_Robot_Controller_joint' + str(jointNum), anonymous = False)
    elif mode == 'PID':
        rospy.init_node('PID_Robot_Controller', anonymous = False)

    if mode == 'MPC':
        controller = mpc_n_dof.MPCController(jointNum, nasaDyn, params, solver)
    elif mode == 'PID':
        controller = pd_controller.PDControl(nasaDyn, params)
    else:
        print 'Please specify a control mode available in the setting.py module'
        exit()
    print 'Controller ' + mode + ' starting'
    rate = rospy.Rate(controller.rate)
    rospy.on_shutdown(controller.shutdown)

    while not rospy.is_shutdown():
        controller.runControl()
        rate.sleep()
        
if __name__== '__main__':
    #parser = argparse.ArgumentParser()
    #parser.add_argument("jointNum", help="The joint you wish to control", type = int)
    #args = parser.parse_args()
    #print args
    #raw_input() 
    #jointNum = args.jointNum
    jointNum = int(sys.argv[1])
    numJts = rospy.get_param('DOF')
    nasaDyn = importlib.import_module("models.kaa_"+str(numJts)+"_dof_dynamics")
    startJoint = 3
    endJoint  = 6
    params = kaa_dyn_params.compute(startJ, endJoint)

    if jointNum > numJts:
        print 'Error, trying to control more joints then are being modeled'
        
    else:
        main(jointNum, nasaDyn, params, solver)
