#!/usr/bin/env python

import rospy
import numpy as np
import math
import time
from copy import deepcopy
import scipy.io as sio
from scipy.linalg import expm
from scipy import linalg
from scipy import signal
from std_msgs.msg import Float64
from threading import RLock, Timer
from collections import deque
# from solvers import nasa_1_dof_cons_zero as solver # nasa_1_dof_p_delta #nasa_1_dof_disturbance # nasa_1_dof_dist is the deprecated solver as 
# from solvers import kl_stiffness_solver as solver
from math import pi, cos, sin
import argparse
import falkor.msg as robot_msgs
import sys
#from nasa_mpc.msg import angles_velocity as angles
#from nasa_mpc.msg import commands
#from nasa_mpc.msg import robot_ready
#from nasa_mpc.msg import send_pressures
from byu_robot.msg import sim_robot

from falkor.srv import set_robot_ctrl_mode
from falkor.srv import move_joint_prs
from falkor.srv import move_joint_valve
from byu_robot.msg import states

#from serial_data.msg import calibration2 as tactileData

from threading import RLock, Timer
from collections import deque
import tf.transformations as tft

from geometry_msgs.msg import PoseArray
#from nasa_4_dof_dyn_params import params
#import nasa_4_dof_dynamics as nasaDyn

import argparse


from models import kaa_dyn_params, kl_dyn_params 
from kaa_models.kaa_dyn_params import DynParams as kaaDynParamsClass
import importlib
# from controllers import kaa_mpc_n_dof, pid_controller, kl_mpc_stiff_n_dof
from controllers import kaa_mpc_n_dof, pid_controller
# from controllers import kaa_mpc_n_dof, kl_mpc_stiff_n_dof
import yaml
import rosparam




def main(jointNum,robotDyn,params,solver,mode):
    #mode = rospy.get_param('Controller_Mode')
    robot = rospy.get_param('state_estimator/Robot_Name')
    if mode == 'MPC' and robot == 'Kaa':
        rospy.init_node('MPC_Robot_Controller_joint' + str(jointNum), anonymous = False)
        file = open("/home/rtpc/git/ego/ego-projects-byu/src/byu_robot/yaml/control_gains_" + str(jointNum) +".yaml")
        gains = yaml.load(file)
        file.close()
        rosparam.upload_params("/control_node_" + str(jointNum), gains)
        controller = kaa_mpc_n_dof.MPCController(jointNum, robotDyn, params, solver)

    elif mode == 'MPC' and robot == 'KingLouieRight':
        rospy.init_node('MPC_Stiffness_controller_joint' + str(jointNum), anonymous = False)
        controller = kl_mpc_stiff_n_dof.MPCController(jointNum, robotDyn, params,solver)
    elif mode == 'PID' and robot == 'Kaa':
        rospy.init_node('PID_Robot_Controller', anonymous = False)
        file = open("/home/rtpc/git/ego/ego-projects-byu/src/byu_robot/yaml/pidControlGains.yaml")
        gains = yaml.load(file)
        print gains
        file.close()
        rosparam.upload_params("/pidControlNode", gains)
        controller = pid_controller.PDControl(robotDyn, params)
    else:
        print 'Please specify a control mode available in the setting.py module'
        exit()


    print 'Controller ' + mode + ' starting'
    rate = rospy.Rate(controller.rate)
    rospy.on_shutdown(controller.shutdown)

    while not rospy.is_shutdown():
        controller.runControl()
        rate.sleep()

        
if __name__== '__main__':
    #parser = argparse.ArgumentParser()
    #parser.add_argument("jointNum", help="The joint you wish to control", type = int)
    #args = parser.parse_args()
    #print args
    #raw_input() 
    #jointNum = args.jointNum
    jointNum = int(sys.argv[1])
    numJts = rospy.get_param('DOF')
    #numJts = 4
    robotDyn = importlib.import_module("models.kaa_"+str(numJts)+"_dof_dynamics")
    # robotDyn = importlib.import_module("models.kl_right_dynamics")
    # startJoint = 3
    # endJoint  = 6
    joints = [1,2,3,4,5,6]
    kaaDynParams = kaaDynParamsClass(joints)
    params = kaaDynParams.params
    # params = kl_dyn_params.params

    mode = 'PID'
    if mode=='PID':
        solver = None
    if jointNum > numJts:
        print 'Error, trying to control more joints then are being modeled'
        
    else:
        main(jointNum, robotDyn, params, solver,mode)
