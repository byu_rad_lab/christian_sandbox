import rospy
import numpy as np
import math
import time
from copy import deepcopy
import scipy.io as sio
from scipy.linalg import expm
from scipy import linalg
from scipy import signal
from std_msgs.msg import Float64
from threading import RLock, Timer
from collections import deque
#from solvers import nasa_1_dof_cons_zero# nasa_1_dof_p_delta #nasa_1_dof_disturbance # nasa_1_dof_dist is the deprecated solver as 
from math import pi, cos, sin
import argparse

# import falkor.msg as robot_msgs
#from nasa_mpc.msg import angles_velocity as angles
from byu_robot.msg import commands
# from nasa_mpc.msg import robot_ready
from byu_robot.msg import send_pressures
# from nasa_mpc.msg import sim_robot

# from falkor.srv import set_robot_ctrl_mode
# from falkor.srv import move_joint_prs
# from falkor.srv import move_joint_valve

# # from serial_data.msg import calibration2 as tactileData

from threading import RLock, Timer
from collections import deque
import tf.transformations as tft

from geometry_msgs.msg import PoseArray
from byu_robot.msg import states
#from nasa_3_dof_dyn_params import params
#import nasa_3_dof_dynamics as nasaDyn

from dynamic_reconfigure.server import Server as DynamicReconfigureServer
from byu_robot.cfg import controllerConfig as ConfigType
import rosparam



class MPCController():
    
    def __init__(self, jointNum, nasaDyn, params, solver):

        print 'Initializing MPC Variables \n'
        #initialize control prameters
        self.rate = rospy.get_param('Controller_MPC_Rate')
        self.dt = 1/self.rate
        self.numJts = rospy.get_param('DOF')
        self.jointNum = jointNum-1
        self.nasaDyn = nasaDyn
        self.params = params
        self.solver = solver
        self.simulation = rospy.get_param('state_estimator/Simulator_Subscriber')
        self.debug = False
        self.record = False
        self.verboseSolve = False
        self.start = time.time()
        self.lock = RLock()

        # initialize variables and states
        self.q_ = [None]*self.numJts
        self.qdot_ = [None]*self.numJts
        self.pPlus_ = [0.]*self.numJts
        self.pMinus_ = [0.]*self.numJts

        self.q = [None]*self.numJts
        self.qdot = [None]*self.numJts
        if self.jointNum == 5:
            self.pPlus = [30.]*self.numJts
            self.pMinus = [30.]*self.numJts
        else:
            self.pPlus = [50.]*self.numJts
            self.pMinus = [50.]*self.numJts
        self.pPlusDesLast = self.pPlus
        self.pMinusDesLast = self.pMinus

        self.qGoal = [0.]*self.numJts
        self.qdotGoal = [0.]*self.numJts
        self.qGoal_ = self.qGoal
        self.qdotGoal_ = self.qdotGoal

        self.error = 0
        self.errorLast = deque([self.error],maxlen = 6)
        self.errorInt = 0.

        
        #MPC parameters and settings
        self.Q = rospy.get_param('control_node_'+str(self.jointNum+1)+'/Q')
        self.R = rospy.get_param('control_node_'+str(self.jointNum+1)+'/R')
        self.S = rospy.get_param('control_node_'+str(self.jointNum+1)+'/S')
        self.mpcKi = rospy.get_param('control_node_'+str(self.jointNum+1)+'/mpcKi')
        self.mpcMaxInt = rospy.get_param('control_node_'+str(self.jointNum+1)+'/mpcMaxInt')


        # Things to get rid of
        self.Q_end = 0
        self.pPlusT =60.
        self.pMinusT = 60.
        self.pDelta = 0
        
        self.qMax =  100.*pi/180.
        self.qMin = -100.*pi/180.
        if self.jointNum == 5 :
            self.pMax = 100.0
            
        else:
            self.pMax = 150.0

        self.pMin = 13.0

        #check if we have data from subscribers before controlling
        self.gotEstimates = False
        self.gotPres = False
        np.set_printoptions(precision=3, linewidth=150)
        np.set_printoptions()

        #goalFlag tells what type of trajectory to follow
        #'path'  = subscribe to joint angels from a path or spline trajectory generator
        #'constant' = stay at a constant angle as defined in setGoal
        #'step' = step between two different joint angle lists as defined in setGoal
        #'increment' = increment between angles as defined in a list in setGoal
        #Should use 'path' most often
        self.goalFlag = 'path'

        #record the data
        self.timeSave = deque()
        self.qSave = deque()
        self.qGoalSave = deque()
        self.qdotSave = deque()
        self.pPlusSave = deque()
        self.pMinusSave = deque()
        self.pPlusDesSave = deque()
        self.pMinusDesSave = deque()
        
        self.subscriber = rospy.Subscriber('/states', states, self.Callback)
        self.server = DynamicReconfigureServer(ConfigType, self.Reconfigure)
        # check if data is coming from simulation or from actual hardware for angles
#        if self.simulation == True:
#            rospy.Subscriber('/robot_state', sim_robot, self.simulationCallback)
        # else:
            # subscribe from data from the estimator and the robot
            # rospy.Subscriber("/robot_angles", angles, self.angles_callback)
            # rospy.Subscriber('robot_state', robot_msgs.robot_state, self.robot_state_callback)
            
        # get pressure commands from other controllers 
        for i in xrange(self.numJts):
            if i != self.jointNum:
                rospy.Subscriber('/pressure_cmd_' + str(i), send_pressures, self.pressureCmdCallback)
        

        #For Planner
        if self.goalFlag == 'path':
            rospy.Subscriber("/commands", commands, self.commands_callback)
            # self.on_path_publisher = rospy.Publisher("/ready_flag", robot_ready, queue_size=2)

        # publisher for pressure commands to the controller_to_robots node    
        self.pressure_cmd = rospy.Publisher('/pressure_cmd_' + str(self.jointNum+1),send_pressures, queue_size = 10)


    def Callback(self, msg):
        self.q_ = list(msg.q)
        self.qdot_ = list(msg.qdot)
        self.pPlus_ = list(msg.pPlus)
        self.pMinus_ = list(msg.pMinus)
        self.gotEstimates = True

    def Reconfigure(self, config, level):
        self.Q = config["Q"]
        self.R = config["R"]
        self.S = config["S"]
        self.mpcKi = config["mpcKi"]
        self.mpcMaxInt = config["mpcMaxInt"]
        if config["Save_Gains"] == True:
            rosparam.dump_params("/home/rtpc/git/ego/ego-projects-byu/src/byu_robot/yaml/control_gains_" + str(self.jointNum) + ".yaml", "/control_nodel_" + str(self.jointNum))
            self.config["Save_Gains"] = False

            
        return config


    # ###### CALLBACKS ############
    def simulationCallback(self,data):
        #print 'Got to sim callback'
        #print data.q[0][
        #print self.q_[0]
        #for i in xrange(data.numJts):
        #    self.q_[i] = data.q[i] 
        self.q_ = data.q
        #print self.q_
        self.qdot_ = data.qdot
        self.pPlus_ = data.pPlus
        self.pMinus_ = data.pMinus
        #print self.p_plus_des_last, '\n'
        #if self.got_data == False:
        #    self.p_plus_des_last = self.p_plus_
        #    self.p_minus_des_last = self.p_minus_
            #print self.p_plus_des_last
        #raw_input()
        self.gotEstimates = True
        self.gotPres = True

    def angles_callback(self, msg):
        self.q_[0] = msg.q[0]
        self.q_[1] = msg.q[1]
        self.q_[2] = msg.q[2]

        self.qdot_[0] = msg.qdot[0]
        self.qdot_[1] = msg.qdot[1]
        self.qdot_[2] = msg.qdot[2]


        self.gotEstimates = True

    def robot_state_callback(self, msg):
        robot_state = msg
        #self.p_plus_ = [robot_state.sensors[0].prs_cal[0]*self.paps, robot_state.sensors[1].prs_cal[0]*self.paps]
        #self.p_minus_ = [robot_state.sensors[0].prs_cal[1]*self.paps, robot_state.sensors[1].prs_cal[1]*self.paps]
        self.pPlus_ = [
            robot_state.sensors[0].prs_cal[0] /
            1000.0,
            robot_state.sensors[1].prs_cal[0] /
            1000.0,
            robot_state.sensors[2].prs_cal[0] /
            1000.0]
        self.pMinus_ = [
            robot_state.sensors[0].prs_cal[1] /
            1000.0,
            robot_state.sensors[1].prs_cal[1] /
            1000.0,
            robot_state.sensors[2].prs_cal[1] /
            1000.0]


        if self.gotPres == False:
            self.pPlusDesLast = self.pPlus_
            self.pMinusDesLast = self.pMinus_
        self.gotPres = True

    def pressureCmdCallback(self,msg):
        whichJoint = msg.jointNum
        self.pPlusDesLast[whichJoint] = msg.pPlus
        self.pMinusDesLast[whichJoint] = msg.pMinus

    def commands_callback(self,msg):
        self.qGoal_ = deepcopy(list(msg.q))
        self.qdotGoal_ = deepcopy(list(msg.qdot))
        for i in xrange(len(self.qGoal_)):
            self.qGoal_[i] = -self.qGoal_[i]
            self.qdotGoal_[i] = -self.qdotGoal_[i]
            
    def model(self):
        self.M = np.matrix(self.nasaDyn.M(self.params, self.q)).reshape(self.numJts, self.numJts)  #*100.
        self.C = np.matrix(np.zeros([self.numJts, self.numJts]))
        self.tauGrav = np.array(self.nasaDyn.g(self.params, self.q))
        self.tauGrav = self.tauGrav.tolist()
        print "tauGrav : ", self.tauGrav, '\n'

        if self.jointNum == 5:
            self.Ks = np.matrix(np.diag([12.2844]*self.numJts))*0.3  # Joint Stiffness Matrix
        elif self.jointNum == 4:
            self.Ks = np.matrix(np.diag([12.2844]*self.numJts))*0.05  # Joint Stiffness Matrix
        else:
            self.Ks = np.matrix(np.diag([12.2844]*self.numJts))*0.1  # Joint Stiffness Matrix                
        self.Kd = np.matrix(np.diag([1.5]*self.numJts))  # Joint Damping matrix

        if self.jointNum == 3:

            self.aPlus = 1.5*0.27990*1.5
            self.aMinus = 1.5*0.27990*1.5
        else:
            self.aPlus = 1.5*0.27990*1.5
            self.aMinus = 1.5*0.27990*1.5
        self.b = -0.3548



        # Joint torque mapping for plus bladders

        self.GammaPlus = np.matrix(np.diag([self.aPlus]*self.numJts))
        # Joint torque mapping for minus bladders

        self.GammaMinus = np.matrix(np.diag([self.aMinus]*self.numJts))


        # pressure Dynamics

        self.alpha = 16
        self.beta = 16


        self.AlphaPlus = np.matrix(np.diag([self.alpha]*self.numJts))
        self.AlphaMinus = np.matrix(np.diag([self.alpha]*self.numJts))
        self.BetaPlus = np.matrix(np.diag([self.beta]*self.numJts))
        self.BetaMinus = np.matrix(np.diag([self.beta]*self.numJts))

    def Calc_A_B(self):
        # add pertinate models to if statment if needed
        
        
        self.model()
            # print 'M : ', self.M

        Arow1 = np.hstack([-
                           linalg.solve(self.M, self.Kd), -
                           linalg.solve(self.M, self.Ks), linalg.solve(self.M, self.GammaPlus), -
                           linalg.solve(self.M, self.GammaMinus)])

        Arow2 = np.hstack([ np.eye(self.numJts), np.zeros([self.numJts,self.numJts]), np.zeros([self.numJts, self.numJts]), np.zeros([self.numJts, self.numJts])])
        Arow3 = np.hstack(
            [np.zeros([self.numJts, self.numJts]), np.zeros([self.numJts, self.numJts]), -self.AlphaPlus, np.zeros([self.numJts, self.numJts])])
        Arow4 = np.hstack(
            [np.zeros([self.numJts, self.numJts]), np.zeros(
                [self.numJts, self.numJts]), np.zeros([self.numJts, self.numJts]), -
             self.AlphaMinus])
        np.set_printoptions(precision = 1, linewidth = 250)
        
        A = np.vstack([Arow1, Arow2, Arow3, Arow4])

        #print 'A : ', A, '\n'

        


        Brow1 = np.hstack(
            [np.matrix(np.eye(self.numJts,self.numJts)), np.zeros([self.numJts, self.numJts]), np.zeros([self.numJts, self.numJts])])
        Brow2 = np.hstack(
            [np.zeros([self.numJts, self.numJts]), np.zeros([self.numJts, self.numJts]), np.zeros([self.numJts, self.numJts])])
        Brow3 = np.hstack([np.zeros([self.numJts, self.numJts]), self.BetaPlus, np.zeros([self.numJts, self.numJts])])
        Brow4 = np.hstack([np.zeros([self.numJts, self.numJts]), np.zeros([self.numJts, self.numJts]), self.BetaMinus])
        
        B = np.vstack([Brow1, Brow2, Brow3, Brow4])
        
        
        Ad, Bd = self.discretize(A, B, 1)

        #print 'Ad : ', Ad, '\n'

        return A, B, Ad, Bd
        
        

    def discretize(self, A, B, mthd):
        # Matrix Exponential
        if mthd == 1:
            print self.dt
            Ad = linalg.expm2(A*self.dt)
            my_eye = np.eye(self.numJts*4)
            Asinv = linalg.solve(A, my_eye)
            Bd = np.dot(np.dot(Asinv, (Ad-my_eye)), B)
        # Zero order hold, allows A to be singular
        elif mthd == 2:
            C = np.eye(4*self.numJts)
            D = np.zeros((4*self.numJts, 3*self.numJts))
            Ad, Bd, Cd, Dd, dt = signal.cont2discrete((A, B, C, D), self.dt, method='bilinear')
        else:
            rospy.signal_shutdown('No discretization method selected')
            
        return Ad, Bd

    def updateData(self):
        self.lock.acquire()
        try:
            self.q = deepcopy(self.q_)
            self.qdot = deepcopy(self.qdot_)
            self.pPlus = deepcopy(self.pPlus_)
            self.pMinus = deepcopy(self.pMinus_)
            for i in xrange(len(self.q_)):
                self.q[i] = -self.q[i]
                self.qdot[i] = -self.qdot[i]
                self.pPlus[i] = self.pPlus[i]/1000.
                self.pMinus[i] = self.pMinus[i]/1000.

        finally:
            self.lock.release()

    def setGoal(self):
        if self.goalFlag == 'constant':
            self.qGoal = [-45.0*pi/180., -0.0*pi/180., 20.0*pi/180]  # radians
            self.qdotGoal = [-0.0*pi/180., -0.0*pi/180., -0.0*pi/180]  # radians
        elif self.goalFlag == 'steps':
            # print 'time : ', self.now
            # Goal steps between 2 angles once every 5 seconds
            period = 10
            if math.sin(2*pi*self.now/period) > 0:
                self.qGoal =    [ 30.0*pi/180.,  30.*pi/180.,   30.0*pi/180]
                self.qdotGoal = [  0.0*pi/180.,   0.0*pi/180.,   0.0*pi/180]  # radians
            else:
                self.qGoal =    [-30.0*pi/180., -30.0*pi/180., -30.0*pi/180]
                self.qdotGoal = [  0.0*pi/180.,   0.0*pi/180.,   0.0*pi/180]  # radians

        elif self.goalFlag == 'increment':									# Goal incriments through list of any length  once every 10 seconds
            # pickup on right and move to left then move back to right.
            period = 2
            qlist_1 = [-30.0, -10.0, 10.0, 30.0, 30.0, 30.0, 10.0, -20.0, -40.0]
            qlist_2 = [-20.0, 20.0, 30.0, 20.0, -20.0, 20.0, 25.0, 20.0, -20.0]
            qlist_3 = [-40.0, -30.0, 0.0, 25.0, 40.0, 25.0, 0.0, -30.0, -40.0]

            print_list = [
                'move 1',
                'move 2',
                'move 3',
                'move 4',
                'move 5',
                'move 6',
                'move 7',
                'move 8',
                'move 9']

            qdlist = [-40, -30, 0, 30, 40, 30, 0, -30, -40]
            self.qGoal = [qlist_1[int(self.now/period) %
                        len(qlist_1)]*pi/180., qlist_2[int(self.now/period) %
                        len(qlist_2)]*pi/180., qlist_3[int(self.now/period) %
                        len(qlist_3)]*pi/180.]
            self.qdotGoal = [-0.0*pi/180., -0.0*pi/180., -0.0*pi/180]  # radians
           
            print print_list[int(self.now/period) % len(print_list)], '\n'

        elif self.goalFlag == 'path':
            self.lock.acquire()
            try:
                self.qGoal = deepcopy(self.qGoal_)
                self.qdotGoal = deepcopy(self.qdotGoal_)
            finally:
                self.lock.release()

    def controlLoop(self):

        if self.gotEstimates == False:#or  self.gotPres == False:
            print 'No data \n'
            return

        self.updateData()
        self.setGoal()

        numJts = self.numJts
        A, B, Ad, Bd = self.Calc_A_B()

        A = np.matrix(A)
        B = np.matrix(B)
        Ad = np.matrix(Ad)
        Bd = np.matrix(Bd)

        # print 'A :', A, '\n'
        # print 'B :', B, '\n'
        # print 'Ad :', Ad, '\n'
        # print 'Bd :', Bd, '\n'
        
        #print self.pPlus
        #print np.matrix(self.qdot).reshape(numJts,1)
        #print np.matrix(self.q).reshape(numJts,1)
        #print np.matrix(self.pPlus).reshape(numJts,1)
        #print np.matrix(self.pMinus).reshape(numJts,1)

        Amap = np.matrix([[1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
                           0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0])

        myIndex = [self.jointNum,self.jointNum+numJts,self.jointNum+2*numJts,self.jointNum+3*numJts]

        Adistqdot = A[self.jointNum,:].A1
        # Adistq = A[self.jointNum+numJts,:].A1
        # AdistpPlus = A[self.jointNum+numJts*2,:].A1
        # AdistpMinus = A[self.jointNum+numJts*3,:].A1 

        Bdistqdot = B[self.jointNum,:].A1
        # Bdistq = B[self.jointNum+numJts,:].A1
        # BdistpPlus = B[self.jointNum+numJts*2,:].A1
        # BdistpMinus = B[self.jointNum+numJts*3,:].A1   

        self.lin_coeff_A_qdot = []
        self.lin_coeff_A_q = []
        self.lin_coeff_A_pPlus = []
        self.lin_coeff_A_pMinus = []

        self.lin_coeff_B_qdot = []
        self.lin_coeff_B_q = []
        self.lin_coeff_B_pPlus = []
        self.lin_coeff_B_pMinus = []

        self.lin_coeff_A_qdot.append(A[self.jointNum,myIndex[0]])
        self.lin_coeff_A_qdot.append(A[self.jointNum,myIndex[1]])
        self.lin_coeff_A_qdot.append(A[self.jointNum,myIndex[2]])
        self.lin_coeff_A_qdot.append(A[self.jointNum,myIndex[3]])

        self.lin_coeff_A_q.append(A[self.jointNum+numJts,myIndex[0]])
        self.lin_coeff_A_q.append(A[self.jointNum+numJts,myIndex[1]])
        self.lin_coeff_A_q.append(A[self.jointNum+numJts,myIndex[2]])
        self.lin_coeff_A_q.append(A[self.jointNum+numJts,myIndex[3]])

        self.lin_coeff_A_pPlus.append(A[self.jointNum+2*numJts,myIndex[0]])
        self.lin_coeff_A_pPlus.append(A[self.jointNum+2*numJts,myIndex[1]])
        self.lin_coeff_A_pPlus.append(A[self.jointNum+2*numJts,myIndex[2]])
        self.lin_coeff_A_pPlus.append(A[self.jointNum+2*numJts,myIndex[3]])

        self.lin_coeff_A_pMinus.append(A[self.jointNum+3*numJts,myIndex[0]])
        self.lin_coeff_A_pMinus.append(A[self.jointNum+3*numJts,myIndex[1]])
        self.lin_coeff_A_pMinus.append(A[self.jointNum+3*numJts,myIndex[2]])
        self.lin_coeff_A_pMinus.append(A[self.jointNum+3*numJts,myIndex[3]])

        self.lin_coeff_B_qdot.append(B[self.jointNum,myIndex[0]])
        self.lin_coeff_B_qdot.append(B[self.jointNum,myIndex[1]])
        self.lin_coeff_B_qdot.append(B[self.jointNum,myIndex[2]])

        self.lin_coeff_B_q.append(B[self.jointNum+numJts,myIndex[0]])
        self.lin_coeff_B_q.append(B[self.jointNum+numJts,myIndex[1]])
        self.lin_coeff_B_q.append(B[self.jointNum+numJts,myIndex[2]])

        self.lin_coeff_B_pPlus.append(B[self.jointNum+2*numJts,myIndex[0]])
        self.lin_coeff_B_pPlus.append(B[self.jointNum+2*numJts,myIndex[1]])
        self.lin_coeff_B_pPlus.append(B[self.jointNum+2*numJts,myIndex[2]])

        self.lin_coeff_B_pMinus.append(B[self.jointNum+3*numJts,myIndex[0]])
        self.lin_coeff_B_pMinus.append(B[self.jointNum+3*numJts,myIndex[1]])
        self.lin_coeff_B_pMinus.append(B[self.jointNum+3*numJts,myIndex[2]])


        # print Adistqdot
        # print Adistq
        # print AdistpPlus
        # print AdistpMinus


        for i in xrange(len(myIndex)):
            # print i
            Adistqdot[myIndex[i]] = 0.0
            # Adistq[myIndex[i]] = 0.0
            # AdistpPlus[myIndex[i]] = 0.0
            # AdistpMinus[myIndex[i]] = 0.0


        # print Adistqdot
        # print Adistq
        # print AdistpPlus
        # print AdistpMinus



        # for i in xrange(len(disturbance)):
            # lin_coeff = A[i,np.where(Amap[i,:] == 1)]
            # disturbance_coeff = np.sum(A[i,np.where(Amap[i,:] == 0)])
            # disturbanceBuff = 
            # for j in xrange(len(self.))
            # disturbance[0] = A


        x = np.vstack([np.matrix(self.qdot).reshape(numJts,1),np.matrix(self.q).reshape(numJts,1),np.matrix(self.pPlus).reshape(numJts,1), np.matrix(self.pMinus).reshape(numJts,1)])
        u = np.vstack([1*np.matrix(self.tauGrav).reshape(numJts,1),np.matrix(self.pPlusDesLast).reshape(numJts,1), np.matrix(self.pMinusDesLast).reshape(numJts,1)])


        distAqdot = np.dot(Adistqdot,x.A1)
        # distAq = Adistq*x.A1
        # distApPlus = AdistpPlus*x.A1
        # distApMinus = AdistpMinus*x.A1

        distBqdot = np.dot(Bdistqdot,u.A1)
        # distBq = Bdistq*u.A1
        # distBpPlus = BdistpPlus*u.A1
        # distBpMinus = BdistpMinus*u.A1

        distqdot = (distAqdot+distBqdot)*self.dt

        # print 'distAqdot: ', distAqdot, '\n'
        # print 'distAq: ', distAq, '\n'
        # print 'distApPlus: ', distApPlus, '\n'
        # print 'distApMinus: ', distApMinus, '\n'

        # print 'distBqdot: ', distBqdot, '\n'
        # print 'distBq: ', distBq, '\n'
        # print 'distBpPlus: ', distBpPlus, '\n'
        # print 'distBpMinus: ', distBpMinus, '\n'

        print 'distqdot: ', distqdot, '\n'




        #print 'u : ', u, '\n'



        DistA = A*x
        DistB = B*u
        DistAd = Ad*x
        DistBd = Bd*u

        #print 'DistA : ', DistA, '\n'
        #print 'DistB : ', DistB, '\n'



        self.A11 = self.lin_coeff_A_qdot[0]*self.dt + 1.
        self.A12 = self.lin_coeff_A_qdot[1]*self.dt
        self.A13 = self.lin_coeff_A_qdot[2]*self.dt
        self.A14 = self.lin_coeff_A_qdot[3]*self.dt

        self.A21 = self.lin_coeff_A_q[0]*self.dt
        self.A22 = self.lin_coeff_A_q[1]*self.dt + 1.
        self.A23 = self.lin_coeff_A_q[2]*self.dt
        self.A24 = self.lin_coeff_A_q[3]*self.dt

        self.A31 = self.lin_coeff_A_pPlus[0]*self.dt
        self.A32 = self.lin_coeff_A_pPlus[1]*self.dt
        self.A33 = self.lin_coeff_A_pPlus[2]*self.dt + 1.
        self.A34 = self.lin_coeff_A_pPlus[3]*self.dt

        self.A41 = self.lin_coeff_A_pMinus[0]*self.dt
        self.A42 = self.lin_coeff_A_pMinus[1]*self.dt
        self.A43 = self.lin_coeff_A_pMinus[2]*self.dt
        self.A44 = self.lin_coeff_A_pMinus[3]*self.dt + 1.

        self.B11 = self.lin_coeff_B_qdot[0]*self.dt
        self.B12 = self.lin_coeff_B_qdot[1]*self.dt
        self.B13 = self.lin_coeff_B_qdot[2]*self.dt

        self.B21 = self.lin_coeff_B_q[0]*self.dt
        self.B22 = self.lin_coeff_B_q[1]*self.dt
        self.B23 = self.lin_coeff_B_q[2]*self.dt

        self.B31 = self.lin_coeff_B_pPlus[0]*self.dt
        self.B32 = self.lin_coeff_B_pPlus[1]*self.dt
        self.B33 = self.lin_coeff_B_pPlus[2]*self.dt

        self.B41 = self.lin_coeff_B_pMinus[0]*self.dt
        self.B42 = self.lin_coeff_B_pMinus[1]*self.dt
        self.B43 = self.lin_coeff_B_pMinus[2]*self.dt


        # Arow1Dist = DistA[self.jointNum]          - self.A11*self.qdot[self.jointNum] - self.A12*self.q[self.jointNum] - self.A13*self.pPlus[self.jointNum] - self.A14*self.pMinus[self.jointNum]
        # Arow2Dist = DistA[self.jointNum+numJts]   - self.A21*self.qdot[self.jointNum] - self.A22*self.q[self.jointNum] - self.A23*self.pPlus[self.jointNum] - self.A24*self.pMinus[self.jointNum]
        # Arow3Dist = DistA[self.jointNum+2*numJts] - self.A31*self.qdot[self.jointNum] - self.A32*self.q[self.jointNum] - self.A33*self.pPlus[self.jointNum] - self.A34*self.pMinus[self.jointNum]
        # Arow4Dist = DistA[self.jointNum+3*numJts] - self.A41*self.qdot[self.jointNum] - self.A42*self.q[self.jointNum] - self.A43*self.pPlus[self.jointNum] - self.A44*self.pMinus[self.jointNum]

        # Brow1Dist = DistB[self.jointNum]          - self.B12*self.pPlus[self.jointNum] - self.B13*self.pMinus[self.jointNum]
        # Brow2Dist = DistB[self.jointNum+numJts]   - self.B22*self.pPlus[self.jointNum] - self.B23*self.pMinus[self.jointNum]
        # Brow2Dist = DistB[self.jointNum+2*numJts] - self.B32*self.pPlus[self.jointNum] - self.B33*self.pMinus[self.jointNum]
        # # Brow2Dist = DistB[self.jointNum+3*numJts] - self.B42*self.pPlus[self.jointNum] - self.B43*self.pMinus[self.jointNum]


        # print 'DistA : ', DistA, '\n'
        # print 'DistB : ', DistB, '\n'



        # self.A11d = Ad[self.jointNum,self.jointNum]
        # self.A12d = Ad[self.jointNum,self.jointNum+numJts]
        # self.A13d = Ad[self.jointNum,self.jointNum+2*numJts]
        # self.A14d = Ad[self.jointNum,self.jointNum+3*numJts]

        # self.A21d = Ad[self.jointNum+numJts,self.jointNum]
        # self.A22d = Ad[self.jointNum+numJts,self.jointNum+numJts]
        # self.A23d = Ad[self.jointNum+numJts,self.jointNum+2*numJts]
        # self.A24d = Ad[self.jointNum+numJts,self.jointNum+3*numJts]

        # self.A31d = Ad[self.jointNum+2*numJts,self.jointNum]
        # self.A32d = Ad[self.jointNum+2*numJts,self.jointNum+numJts]
        # self.A33d = Ad[self.jointNum+2*numJts,self.jointNum+2*numJts]
        # self.A34d = Ad[self.jointNum+2*numJts,self.jointNum+3*numJts]

        # self.A41d = Ad[self.jointNum+3*numJts,self.jointNum]
        # self.A42d = Ad[self.jointNum+3*numJts,self.jointNum+numJts]
        # self.A43d = Ad[self.jointNum+3*numJts,self.jointNum+2*numJts]
        # self.A44d = Ad[self.jointNum+3*numJts,self.jointNum+3*numJts]

        # self.B11d = Bd[self.jointNum,self.jointNum]
        # self.B12d = Bd[self.jointNum,self.jointNum+numJts]
        # self.B13d = Bd[self.jointNum,self.jointNum+2*numJts]

        # self.B21d = Bd[self.jointNum+numJts,self.jointNum]
        # self.B22d = Bd[self.jointNum+numJts,self.jointNum+numJts]
        # self.B23d = Bd[self.jointNum+numJts,self.jointNum+2*numJts]

        # self.B31d = Bd[self.jointNum+2*numJts,self.jointNum]
        # self.B32d = Bd[self.jointNum+2*numJts,self.jointNum+numJts]
        # self.B33d = Bd[self.jointNum+2*numJts,self.jointNum+2*numJts]

        # self.B41d = Bd[self.jointNum+3*numJts,self.jointNum]
        # self.B42d = Bd[self.jointNum+3*numJts,self.jointNum+numJts]
        # self.B43d = Bd[self.jointNum+3*numJts,self.jointNum+2*numJts]

        # Arow1Distd = DistAd[self.jointNum]          - self.A11d*self.qdot[self.jointNum] - self.A12d*self.q[self.jointNum] - self.A13d*self.pPlus[self.jointNum] - self.A14d*self.pMinus[self.jointNum]
        # Arow2Distd = DistAd[self.jointNum+numJts]   - self.A21d*self.qdot[self.jointNum] - self.A22d*self.q[self.jointNum] - self.A23d*self.pPlus[self.jointNum] - self.A24d*self.pMinus[self.jointNum]
        # Arow3Distd = DistAd[self.jointNum+2*numJts] - self.A31d*self.qdot[self.jointNum] - self.A32d*self.q[self.jointNum] - self.A33d*self.pPlus[self.jointNum] - self.A34d*self.pMinus[self.jointNum]
        # Arow4Distd = DistAd[self.jointNum+3*numJts] - self.A41d*self.qdot[self.jointNum] - self.A42d*self.q[self.jointNum] - self.A43d*self.pPlus[self.jointNum] - self.A44d*self.pMinus[self.jointNum]

        # Brow1Distd = DistBd[self.jointNum]          - self.B12d*self.pPlus[self.jointNum] - self.B13d*self.pMinus[self.jointNum]
        # Brow2Distd = DistBd[self.jointNum+numJts]   - self.B22d*self.pPlus[self.jointNum] - self.B23d*self.pMinus[self.jointNum]
        # Brow2Distd = DistBd[self.jointNum+2*numJts] - self.B32d*self.pPlus[self.jointNum] - self.B33d*self.pMinus[self.jointNum]
        # Brow2Distd = DistBd[self.jointNum+3*numJts] - self.B42d*self.pPlus[self.jointNum] - self.B43d*self.pMinus[self.jointNum]
 
       



        # qDist = self.dt*(Arow2Dist + Brow2Dist).A1.tolist()[0]
        # qdotDist = self.dt*(Arow1Dist + Brow1Dist).A1.tolist()[0]
        # print 'qDist : ', qDist, '\n'
        # print 'qdotDist : ', qdotDist, '\n'
        # qDistd = (Arow2Distd + Brow2Distd).A1.tolist()[0]
        # qdotDistd = (Arow1Distd + Brow1Distd).A1.tolist()[0]
        # print 'qDistd : ', qDistd, '\n'
        # print 'qdotDistd : ', qdotDistd, '\n'
        # qDist = qDistd
        # qdotDist = qdotDistd
        qdotDist = float(distqdot)
        # print qdotDist
        qDist = 0.
        # qdotDist = 0.
        # if self.jointNum == 0:
            # qDist = 0
            # qdotDist = 0

        
        self.error = self.qGoal[self.jointNum] - self.q[self.jointNum]
        self.errorLast.append(self.error)
        if self.errorLast[-1] > 0 and self.error < 0:
            self.errorInt = 0
        elif self.errorLast[-1] < 0 and self.error > 0:
            self.errorInt = 0
            
        Ki = self.mpcKi
        #Ki = 0.
        
        self.errorInt = Ki*self.error + self.errorInt
        errorDif = deque()
        for i in xrange(len(self.errorLast)-1):
            errorDif.append(self.errorLast[i+1] - self.errorLast[i])
        
        
        if abs(self.error) > 50.*pi/180.:
            Ki = 0.
            self.errorInt = 0.
            
        max = 1.

        if self.errorInt > max:
            self.errorInt = max
        elif self.errorInt < -max:
            self.errorInt = -max

        pPlusDes, pMinusDes = self.solveMPC(qDist,qdotDist)
        
        msg = send_pressures()
        msg.pPlus = pPlusDes
        msg.pMinus = pMinusDes
        msg.jointNum = self.jointNum+1
        self.pressure_cmd.publish(msg)

        print 'q : ',  self.q, '\n'
        # print self.dt
        print 'qdot : ',  self.qdot, '\n'
        print 'qGoal : ', self.qGoal, '\n'
        print 'p+ : ', self.pPlus, '\n'
        print 'p- : ', self.pMinus, '\n'
        print 'p + Des :', pPlusDes, '\n'
        print 'p - Des :', pMinusDes, '\n'


        if self.record == True:
            self.recordData()


    def runControl(self):
        self.controlLoop()

        
    def solveMPC(self, qDist, qdotDist):
        self.qInt = self.errorInt
        self.out_qInt = self.qInt
        self.out_qdotDist = qdotDist
        self.out_qDist = qDist
        self.out_q0 = self.q[self.jointNum]
        self.out_qdot0 = self.qdot[self.jointNum]
        self.out_pPlus0 = self.pPlus[self.jointNum]
        self.out_pMinus0 = self.pMinus[self.jointNum]
        self.out_pPlusDesLast0 = self.pPlusDesLast[self.jointNum]
        self.out_pMinusDesLast0 = self.pMinusDesLast[self.jointNum]
        
        # print 'A11    : ', self.A11
        # print 'A11d   : ', self.A11d
        # print 'A11New : ', (self.lin_coeff_A_qdot[0]*self.dt) + 1

        self.out_A11 = self.A11
        self.out_A12 = self.A12
        self.out_A13 = self.A13
        self.out_A14 = self.A14

        self.out_A21 = self.A21
        self.out_A22 = self.A22
        self.out_A23 = self.A23
        self.out_A24 = self.A24

        self.out_A31 = self.A31
        self.out_A32 = self.A32
        self.out_A33 = self.A33
        self.out_A34 = self.A34

        self.out_A41 = self.A41
        self.out_A42 = self.A42
        self.out_A43 = self.A43
        self.out_A44 = self.A44

        self.out_B12 = self.B12
        self.out_B13 = self.B13

        self.out_B22 = self.B22
        self.out_B23 = self.B23

        self.out_B32 = self.B32
        self.out_B33 = self.B33

        self.out_B42 = self.B42
        self.out_B43 = self.B43

        #self.Q = #self.Q + 1/(self.error+0.001)
        self.out_Q = self.Q
        print 'out_Q  :', self.out_Q
        #self.R = 2000 + 1000*1/(self.error + 0.01)
        self.out_R = self.R
        self.out_S = self.S
        self.out_Q_end = self.Q_end


        self.out_qGoal = self.qGoal[self.jointNum]
        print 'Out q Goal : ', self.out_qGoal
        self.out_qdotGoal = self.qdotGoal[self.jointNum]
        self.out_pPlusT = self.pPlusT
        self.out_pMinusT = self.pMinusT

        self.out_pMax = self.pMax
        self.out_pMin = self.pMin
        self.out_qMax = self.qMax
        self.out_qMin = self.qMin
        self.out_pDelta = self.pDelta


        self.debug = False
        if self.debug:
            print 'q0 : ',  float(self.out_q0)
            print 'qdot0 : ',  self.out_qdot0
            print 'pPlus0 : ',  self.out_pPlus0
            print 'pMinus_0 : ',  self.out_pMinus0
            print 'pPlusDes0 : ',  self.out_pPlusDesLast0
            print 'pMinusDes0 : ',  self.out_pMinusDesLast0
            #print 'Ad : ', Ad
            #print 'Bd : ', Bd
            print 'A11 : ',  float(self.out_A11)
            print 'A12 : ',  float(self.out_A12)
            print 'A13 : ',  float(self.out_A13)
            print 'A14 : ',  float(self.out_A14)
            print 'A21 : ',  float(self.out_A21)
            print 'A22 : ',  float(self.out_A22)
            print 'A23 : ',  float(self.out_A23)
            print 'A24 : ',  float(self.out_A24)
            print 'A31 : ',  float(self.out_A31)
            print 'A32 : ',  float(self.out_A32)
            print 'A33 : ',  float(self.out_A33)
            print 'A34 : ',  float(self.out_A34)
            print 'A41 : ',  float(self.out_A41)
            print 'A42 : ',  float(self.out_A42)
            print 'A43 : ',  float(self.out_A43)
            print 'A44 : ',  float(self.out_A44)
            print 'B12 : ',  float(self.out_B12)
            print 'B13 : ',  float(self.out_B13)
            print 'B22 : ',  float(self.out_B22)
            print 'B23 : ',  float(self.out_B23)
            print 'B32 : ',  float(self.out_B32)
            print 'B43 : ',  float(self.out_B43)
            print 'Q : ',  self.out_Q
            print 'R : ',  self.out_R
            print 'S : ',  self.out_S
            print 'Q_end: ',  self.out_Q_end
            print 'q_goal : ',  self.out_qGoal
            print 'qdot_goal : ',  self.out_qdotGoal
            print 'p_plus_t : ',  self.out_pPlusT
            print 'p_mius_t : ',  self.out_pMinusT
            print 'p_max : ',  self.out_pMax
            print 'p_min : ',  self.out_pMin
            print 'q_max : ',  self.out_qMax
            print 'q_min : ',  self.out_qMin
            print 'p_delta : ',  self.out_pDelta
            print 'qdot_dist : ',  float(self.out_qdotDist)
            print 'q_dist : ',  float(self.out_qDist)
            raw_input()



        cmd = self.solver.runController(float(self.out_q0),
                                            float(self.out_qdot0),
                                            float(self.out_pPlus0),
                                            float(self.out_pMinus0),
                                            float(self.out_pPlusDesLast0),
                                            float(self.out_pMinusDesLast0),
                                            float(self.out_A11),
                                            float(self.out_A12),
                                            float(self.out_A13),
                                            float(self.out_A14),
                                            float(self.out_A21),
                                            float(self.out_A22),
                                            float(self.out_A23),
                                            float(self.out_A24),
                                            float(self.out_A31),
                                            float(self.out_A32),
                                            float(self.out_A33),
                                            float(self.out_A34),
                                            float(self.out_A41),
                                            float(self.out_A42),
                                            float(self.out_A43),
                                            float(self.out_A44),
                                            float(self.out_B12),
                                            float(self.out_B13),
                                            float(self.out_B22),
                                            float(self.out_B23),
                                            float(self.out_B32),
                                            float(self.out_B43),
                                            float(self.out_Q),
                                            float(self.out_R),
                                            float(self.out_S),
                                            float(self.out_Q_end),
                                            float(self.out_qGoal),
                                            float(self.out_qdotGoal),
                                            float(self.out_pPlusT),
                                            float(self.out_pMinusT),
                                            float(self.out_pMax),
                                            float(self.out_pMin),
                                            float(self.out_qMax),
                                            float(self.out_qMin),
                                            float(self.out_pDelta),
                                            float(self.out_qdotDist),
                                            float(self.out_qDist),
                                            float(self.out_qInt))

        print 'cmd : ', cmd, '\n'

        pPlusDes = cmd[0][0]
        pMinusDes = cmd[1][0]
        self.pPlusDesLast[self.jointNum] = pPlusDes
        self.pMinusDesLast[self.jointNum] = pMinusDes
        
        return pPlusDes, pMinusDes


    def recordData(self):
        self.qSave.append(deepcopy(self.q))
        self.qGoalSave.append(deepcopy(self.qGoal))
        self.qdotSave.append(deepcopy(self.qdot))
        self.pPlusSave.append(self.pPlus)
        self.pMinusSave.append(self.pMinus)
        self.timeSave.append(time.time()-self.start)

    def shutdown(self):

        if self.record:
            print 'Recording Data'
            dataDict = {
                't': self.timeSave,
                'q': self.qSave,
                'q_goal': self.qGoalSave,
                'qdot': self.qdotSave,
                'p_plus': self.pPlusSave,
                'p_minus': self.pMinusSave,
                'q_solve': self.qSolveSave}#,
                #'qdot_solve': self.qdotSolveSave}#,
                #'p_plus_solve': self.p_plusSolve_save,
                #'p_minus_solve': self.p_minus_solve_save,
                #'p_plus_des_solve': self.p_plus_des_solve_save,
                #'p_minus_des_solve': self.p_minus_des_solve_save}
            sio.savemat('data/kaa_1_dof_mpc_joint_'+ str(self.jointNum)+'_test_cons_zero_5', dataDict)

            print 'Data Recorded'
