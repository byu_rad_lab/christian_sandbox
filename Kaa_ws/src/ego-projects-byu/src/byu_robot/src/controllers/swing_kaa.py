#!/usr/bin/env python

import rospy
import numpy as np
import math
import time
from copy import deepcopy
from nasa_mpc.msg import send_pressures

print "done importing"

def main():
    rospy.init_node('swing_kaa',anonymous=True)
    rate = rospy.Rate(500)
    pubJoint1 = rospy.Publisher('/pressure_cmd_1',send_pressures,queue_size=10)
    pubJoint2 = rospy.Publisher('/pressure_cmd_2',send_pressures,queue_size=10)
    pubJoint3 = rospy.Publisher('/pressure_cmd_3',send_pressures,queue_size=10)
    pubJoint4 = rospy.Publisher('/pressure_cmd_4',send_pressures,queue_size=10)
    pubJoint5 = rospy.Publisher('/pressure_cmd_5',send_pressures,queue_size=10)
    pubJoint6 = rospy.Publisher('/pressure_cmd_6',send_pressures,queue_size=10)
    start = time.time()
    print "Here"
    while time.time()-start < 8.:
        print "Sending"
        msg1 = send_pressures()
        msg1.pPlus = 200
        msg1.pMinus = 10
        msg1.jointNum = 1
        msg2 = send_pressures()
        msg2.pPlus = 50
        msg2.pMinus = 50
        msg2.jointNum = 2
        pubJoint1.publish(msg1)
        pubJoint2.publish(msg2)
        msg1.pPlus = 10
        msg1.pMinus = 200
        msg1.jointNum = 3
        pubJoint3.publish(msg1)
        msg2.jointNum = 4
        pubJoint4.publish(msg2)
        msg1.jointNum = 5
        pubJoint5.publish(msg1)
        msg2.jointNum = 6
        pubJoint6.publish(msg2)
        rate.sleep()
    start = time.time()
    while time.time()-start < 4.:
        print "Sending"
        msg1 = send_pressures()
        msg1.pPlus = 10
        msg1.pMinus = 200
        msg1.jointNum = 1
        msg2 = send_pressures()
        msg2.pPlus = 50
        msg2.pMinus = 50
        msg2.jointNum = 2
        pubJoint1.publish(msg1)
        pubJoint2.publish(msg2)
        msg1.pPlus = 200
        msg1.pMinus = 10
        msg1.jointNum = 3
        pubJoint3.publish(msg1)
        msg2.jointNum = 4
        pubJoint4.publish(msg2)
        msg1.jointNum = 5
        pubJoint5.publish(msg1)
        msg2.jointNum = 6
        pubJoint6.publish(msg2)
        rate.sleep()
    start = time.time()
    while time.time()-start < 7.:
        print "Sending"
        msg1 = send_pressures()
        msg1.pPlus = 60
        msg1.pMinus = 20
        msg1.jointNum = 1
        msg2 = send_pressures()
        msg2.pPlus = 50
        msg2.pMinus = 50
        msg2.jointNum = 2
        pubJoint1.publish(msg1)
        pubJoint2.publish(msg2)
        msg1.pPlus = 20
        msg1.pMinus = 60
        msg1.jointNum = 3
        pubJoint3.publish(msg1)
        msg2.jointNum = 4
        pubJoint4.publish(msg2)
        msg1.jointNum = 5
        pubJoint5.publish(msg1)
        msg2.jointNum = 6
        pubJoint6.publish(msg2)
        rate.sleep()
        
    

if __name__=='__main__':
    print "Starting"
    main()
