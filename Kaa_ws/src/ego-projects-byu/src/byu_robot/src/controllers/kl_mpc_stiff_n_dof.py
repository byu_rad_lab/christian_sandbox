import rospy
import numpy as np
import math
import time
from copy import deepcopy
import scipy.io as sio
from scipy.linalg import expm
from scipy import linalg
from scipy import signal
from std_msgs.msg import Float64
from threading import RLock, Timer
from collections import deque
#from solvers import nasa_1_dof_cons_zero# nasa_1_dof_p_delta #nasa_1_dof_disturbance # nasa_1_dof_dist is the deprecated solver as 
from math import pi, cos, sin
import argparse


# import falkor.msg as robot_msgs
#from nasa_mpc.msg import angles_velocity as angles
from byu_robot.msg import commands
# from nasa_mpc.msg import robot_ready
from byu_robot.msg import send_pressures
# from nasa_mpc.msg import sim_robot

# from falkor.srv import set_robot_ctrl_mode
# from falkor.srv import move_joint_prs
# from falkor.srv import move_joint_valve

# # from serial_data.msg import calibration2 as tactileData

from threading import RLock, Timer
from collections import deque
import tf.transformations as tft

from geometry_msgs.msg import PoseArray
from byu_robot.msg import states
#from nasa_3_dof_dyn_params import params
#import nasa_3_dof_dynamics as nasaDyn

from dynamic_reconfigure.server import Server as DynamicReconfigureServer
# from byu_robot.cfg import klcontrollerConfig as ConfigType
import rosparam
from low_level_control.msg import sensors as sensor
from louie_control.msg import mpc_msg


class MPCController():
    
    def __init__(self, jointNum, robotDyn, params, solver):

        print 'Initializing MPC Variables \n'
        #initialize control prameters
        self.rate = rospy.get_param('Controller_MPC_Rate')
        self.dt = 1/self.rate
        self.numJts = rospy.get_param('DOF')
        self.jointNum = jointNum-1
        self.robotDyn = robotDyn
        self.params = params
        self.solver = solver
        self.simulation = rospy.get_param('state_estimator/Simulator_Subscriber')
        self.debug = False
        self.record = True
        self.verboseSolve = False
        self.start = time.time()
        self.lock = RLock()


        self.cPlus =[-2.65, -2.65,  -5.301, -5.301]
        self.cMinus = [2.65, 2.65,  5.301, 5.301]

        self.MPlus = 1.4857e-04
        self.MMinus = -1.4857e-04

        self.b = 1.222e-03

        self.aPlus = [0.0011*1000., 0.0009*1000., 0.0008*1000., 0.0008*1000.]#1.5*0.27990*1.5
        self.aMinus = [-0.0011*1000., -0.0009*1000., -0.0008*1000., -0.0008*1000.]# 1.5*0.27990*1.5
        # self.b = -0.3548



        # initialize variables and states
        self.q_ = [None]*self.numJts
        self.qdot_ = [None]*self.numJts
        self.pPlus_ = [0.]*self.numJts
        self.pMinus_ = [0.]*self.numJts
        self.k_ = [20.]*self.numJts

        # parameters for sitffness model

        self.q = [0.]*self.numJts
        self.qdot = [0.]*self.numJts
        # if self.jointNum == 5:
        #    self.pPlus = [30.]*self.numJts
        #     self.pMinus = [30.]*self.numJts
        # else:                   
        self.pPlus = [50.]*self.numJts
        self.pMinus = [50.]*self.numJts

        self.pPlusDesLast = self.pPlus        
        self.pMinusDesLast = self.pMinus

        #initalize the model
        self.model()
        
        self.k = self.calcStiff()
        self.kdot = self.calcStiffDot()
        


        

        self.qGoal = [0.]*self.numJts 
        self.qdotgoal = [0.]*self.numJts
        self.qGoal_ = self.qGoal
        self.qdotGoal_ = [0.0]*self.numJts

        # self.kGoal = [30.0, 30.0, 20.0, 25.0]
        self.kGoal = [40.0, 40.0, 40.0, 40.0]
        self.kGoal_ = [40.0, 40.0, 40.0, 40.0]
        
        self.error = 0
        self.errorLast = deque([self.error],maxlen = 6)
        self.errorInt = 0.

        
        #MPC parameters and settings
        self.Qpos =  rospy.get_param('control_node_'+str(self.jointNum+1)+'/Qpos')
        self.Qstiff= rospy.get_param('control_node_'+str(self.jointNum+1)+'/Qstiff')
        self.Qvel= rospy.get_param('control_node_'+str(self.jointNum+1)+'/Qvel')
        self.Qsdot = rospy.get_param('control_node_'+str(self.jointNum+1)+'/Qsdot')
        self.Qpres= rospy.get_param('control_node_'+str(self.jointNum+1)+'/Qpres')
        # self.R = rospy.get_param('control_node_'+str(self.jointNum+1)+'/R')
        # self.S = rospy.get_param('control_node_'+str(self.jointNum+1)+'/S')
        self.mpcKi = rospy.get_param('control_node_'+str(self.jointNum+1)+'/mpcKi')
        self.mpcMaxInt = rospy.get_param('control_node_'+str(self.jointNum+1)+'/mpcMaxInt')

        # self.Qpos =   [300.,   1.,    1.,   1.]
        # self.Qpres =    [1.1,     1.5,    1.,   1.]
        # self.Qvel =   [.0001,      .000001,    1.,   1.]
        # self.Qstiff = [1000.,    10.,    1.,   1.]
        # self.Qsdot =  [.000001,     .00000001,    1.,   1.]

        # gains for no q constraints
        self.Qpos =   [1200.,    800.,    500.,      500.]
        self.Qpres =    [.8,    .8,    .8,          .8]
        self.Qvel =   [.001,    .001,    .001,      .001]
        self.Qstiff = [.2,      .8,    .8,          1.2]
        self.Qsdot =  [.0001,   .0001,    .00001,   .00001]


        # Things to get rid of
        self.Q_end = 0
        self.pPlusT =60.
        self.pMinusT = 60.
        self.pDelta = 0
        
        self.paps = 0.000145037738
        self.poff = 14.6959488


    

        self.qMax =  120.*pi/180.
        self.qMin = -120.*pi/180.
        if jointNum == 1:
            self.qMax = 15.*pi/180
        if self.jointNum == 5 :
            self.pMax = 206.8
        elif self.jointNum == 3 or self.jointNum==2:
            self.pMax = 175.0
        else:
            self.pMax = 30/self.paps/1000 # kPa

        self.pMin = 10. # kPa

        #check if we have data from subscribers before controlling
        self.gotEstimates = False
        self.gotPres = False
        np.set_printoptions(precision=3, linewidth=150)
        np.set_printoptions()

        #goalFlag tells what type of trajectory to follow
        #'path'  = subscribe to joint angels from a path or spline trajectory generator
        #'constant' = stay at a constant angle as defined in setGoal
        #'step' = step between two different joint angle lists as defined in setGoal
        #'increment' = increment between angles as defined in a list in setGoal
        #Should use 'path' most often
        self.goalFlag = 'path'

        #record the data
        self.timeSave = deque()
        self.qSave = deque()
        self.qGoalSave = deque()
        self.qdotSave = deque()
        self.pPlusSave = deque()
        self.pMinusSave = deque()
        self.pPlusDesSave = deque()
        self.pMinusDesSave = deque()
        self.kSave = deque()
        
        self.subscriber = rospy.Subscriber('/states', states, self.Callback)
        self.subscriber = rospy.Subscriber('/PressureData', sensor, self.pressureCallback)
        # self.server = DynamicReconfigureServer(ConfigType, self.Reconfigure)
        # check if data is coming from simulation or from actual hardware for angles
#        if self.simulation == True:
#            rospy.Subscriber('/robot_state', sim_robot, self.simulationCallback)
        # else:
            # subscribe from data from the estimator and the robot
            # rospy.Subscriber("/robot_angles", angles, self.angles_callback)
            # rospy.Subscriber('robot_state', robot_msgs.robot_state, self.robot_state_callback)
            
        # get pressure commands from other controllers 
        for i in xrange(4):
            if i != self.jointNum and i != 2:
                # rospy.Subscriber('/pressure_cmd_' + str(i), send_pressures, self.pressureCmdCallback)
                rospy.Subscriber('/Joint' + str(self.jointNum) + 'DesiredPressures', mpc_msg, self.pressureCmdCallback, (i))
        

        #For Planner
        if self.goalFlag == 'path':
            rospy.Subscriber("/commands", commands, self.commands_callback)
            # self.on_path_publisher = rospy.Publisher("/ready_flag", robot_ready, queue_size=2)

        # publisher for pressure commands to the controller_to_robots node    
        if self.jointNum >= 2:
            publisher_name = '/Joint' + str(self.jointNum+1) + 'DesiredPressures'
        else:
            publisher_name = '/Joint' + str(self.jointNum) + 'DesiredPressures'

        
        self.pressure_cmd = rospy.Publisher(publisher_name, mpc_msg, queue_size = 1)
        
        if self.jointNum >= 2:
            self.pubStiff = rospy.Publisher('/k' + str(self.jointNum+1), Float64, queue_size = 1)
        else:
            self.pubStiff = rospy.Publisher('/k' + str(self.jointNum), Float64, queue_size = 1)
        # self.pressure_cmd = rospy.Publisher('/pressure_cmd_' + str(self.jointNum+1),send_pressures, queue_size = 10)


    def Callback(self, msg):
        self.q_ = list(msg.q)
        self.qdot_ = list(msg.qdot)
        # self.pPlus_ = list(msg.pPlus)
        # self.pMinus_ = list(msg.pMinus)
        self.gotEstimates = True
    
    def pressureCallback(self, msg):
        skipElbow = 0
        self.lock.acquire()
        # self.pPlus_ = []
        # self.pMinus_ = []
        for i in range(self.numJts):
            self.pPlus_[i]= (msg.p[int(2*skipElbow+1)]*self.paps - self.poff)/self.paps
            self.pMinus_[i] =(msg.p[int(2*skipElbow)]*self.paps - self.poff)/self.paps
            
            if skipElbow == 1:
                skipElbow = skipElbow + 2
            else:
                skipElbow = skipElbow + 1
        self.lock.release()

    def Reconfigure(self, config, level):
        self.Qpos = config["Qpos"]
        self.Qstiff = config["Qstiff"]
        self.Qsdot = config["Qsdot"]
        self.Qpres = config["Qpres"]
        self.Qvel = config["Qvel"]
        self.mpcKi = config["mpcKi"]
        self.mpcMaxInt = config["mpcMaxInt"]
        if config["Save_Gains"] == True:
            rosparam.dump_params("/home/rtpc/git/ego/ego-projects-byu/src/byu_robot/yaml/klcontrol_gains_" + str(self.jointNum) + ".yaml", "/control_nodel_" + str(self.jointNum))
            self.config["Save_Gains"] = False

            
        return config


    # ###### CALLBACKS ############
    def simulationCallback(self,data):
        #print 'Got to sim callback'
        #print data.q[0][
        #print self.q_[0]
        #for i in xrange(data.numJts):
        #    self.q_[i] = data.q[i] 
        self.q_ = data.q
        #print self.q_
        self.qdot_ = data.qdot
        self.pPlus_ = data.pPlus
        self.pMinus_ = data.pMinus
        #print self.p_plus_des_last, '\n'
        #if self.got_data == False:
        #    self.p_plus_des_last = self.p_plus_
        #    self.p_minus_des_last = self.p_minus_
            #print self.p_plus_des_last
        #raw_input()
        self.gotEstimates = True
        self.gotPres = True

    def angles_callback(self, msg):
        self.q_[0] = msg.q[0]
        self.q_[1] = msg.q[1]
        self.q_[2] = msg.q[2]

        self.qdot_[0] = msg.qdot[0]
        self.qdot_[1] = msg.qdot[1]
        self.qdot_[2] = msg.qdot[2]


        self.gotEstimates = True

    def robot_state_callback(self, msg):
        robot_state = msg
        #self.p_plus_ = [robot_state.sensors[0].prs_cal[0]*self.paps, robot_state.sensors[1].prs_cal[0]*self.paps]
        #self.p_minus_ = [robot_state.sensors[0].prs_cal[1]*self.paps, robot_state.sensors[1].prs_cal[1]*self.paps]
        self.pPlus_ = [
            robot_state.sensors[0].prs_cal[0] /
            1000.0,
            robot_state.sensors[1].prs_cal[0] /
            1000.0,
            robot_state.sensors[2].prs_cal[0] /
            1000.0]
        self.pMinus_ = [
            robot_state.sensors[0].prs_cal[1] /
            1000.0,
            robot_state.sensors[1].prs_cal[1] /
            1000.0,
            robot_state.sensors[2].prs_cal[1] /
            1000.0]


        if self.gotPres == False:
            self.pPlusDesLast = self.pPlus_
            self.pMinusDesLast = self.pMinus_
        self.gotPres = True

    def pressureCmdCallback(self,msg,whichJoint):
        self.pPlusDesLast[whichJoint] = msg.p_d[0]
        self.pMinusDesLast[whichJoint] = msg.p_d[1]

    def commands_callback(self,msg):
        self.qGoal_ = deepcopy(list(msg.q))
        self.qdotGoal_ = deepcopy(list(msg.qdot))
        for i in xrange(len(self.qGoal_)):
            self.qGoal_[i] = self.qGoal_[i]
            self.qdotGoal_[i] = self.qdotGoal_[i]
            
    def model(self):
        
        

        self.M = np.matrix(self.robotDyn.M(self.params, self.q)).reshape(self.numJts, self.numJts)  #*100.
        # self.M = np.matrix(np.eye(self.numJts,self.numJts))

        # self.M = np.matrix(np.eye(self.numJts))
        self.C = np.matrix(np.zeros([self.numJts, self.numJts]))
        self.tauGrav = np.array(self.robotDyn.g(self.params, self.q))
        self.tauGrav = self.tauGrav.tolist()
        print "tauGrav : ", self.tauGrav, '\n'

        # if self.jointNum == 5:
        #     self.Ks = np.matrix(np.diag([12.2844]*self.numJts))*0.3  # Joint Stiffness Matrix
        # elif self.jointNum == 4:
        #     self.Ks = np.matrix(np.diag([12.2844]*self.numJts))*0.05  # Joint Stiffness Matrix
        # else:
        Ks = [.10,.1, 0.1, 0.1]
        self.Ks = np.matrix(np.diag([12.2844]*self.numJts))*Ks[self.jointNum]  # Joint Stiffness Matrix             

        Kd = [50, 8., 10., 20.]
        self.Kd = np.matrix(np.diag([1.5]*self.numJts))*Kd[self.jointNum]  # Joint Damping matrix

        # if self.jointNum == 3:

        #     self.aPlus[self.jointNum] = 1.5*0.27990*1.5
        #     self.aMinus[self.jointNum] = 1.5*0.27990*1.5
        # else:



        # Joint torque mapping for plus bladders

        self.GammaPlus = np.matrix(np.diag([self.aPlus[self.jointNum]]*self.numJts))
        # Joint torque mapping for minus bladders

        self.GammaMinus = np.matrix(np.diag([-self.aMinus[self.jointNum]]*self.numJts))


        # pressure Dynamics



        self.AlphaPlus = np.matrix(np.diag([-self.cPlus[self.jointNum]]*self.numJts))
        self.AlphaMinus = np.matrix(np.diag([-self.cPlus[self.jointNum]]*self.numJts))
        self.BetaPlus = np.matrix(np.diag([self.cMinus[self.jointNum]]*self.numJts))
        self.BetaMinus = np.matrix(np.diag([self.cMinus[self.jointNum]]*self.numJts))

        
    def calcStiff(self):
        k = np.zeros(self.numJts)
        print 'k : ', k, '\n'
        for i in xrange(self.numJts):

            k[i] = (self.aPlus[self.jointNum]*self.MPlus*self.pPlus[i])/(self.MPlus*abs(self.q[i]) + self.b) + (self.aMinus[self.jointNum]*self.MMinus*self.pMinus[i])/(self.MMinus*abs(self.q[i]) + self.b)
        return k

    def calcStiffDot(self):

        kdot = np.zeros(self.numJts)

        for i in xrange(self.numJts):
            alphaPlus = self.MPlus/(self.MPlus*self.q[i] + self.b)
            alphaMinus = self.MMinus/(self.MMinus*self.q[i] + self.b)
            kdot[i] = self.cPlus[self.jointNum]*self.k[i] + self.aPlus[i]*alphaPlus*self.cMinus[self.jointNum]*self.pPlusDesLast[i] + self.aMinus[i]*alphaMinus*self.cMinus[self.jointNum]*self.pMinusDesLast[i] - self.qdot[i]*(self.aPlus[i]*alphaPlus*alphaPlus*self.pPlus[i] + self.aMinus[i]*alphaMinus*alphaMinus*self.pMinus[i])
        return kdot
                       
    

    def calcPartials(q,qdot,pPlus,pMinus,pPlusDes,pMinusDes,k):
        
        alphaPlus = self.MPlus/(self.MPlus*q + self.b)
        alphaMius = self.MMinus/(self.MMinus*q + self.b)
        
        partialk = c0
        partialq = qdot*(2*(self.MPlus**3)*self.aPlus[self.jointNum]*pPlus)/((self.b + self.MPlus*q)**3) + (2*self.MMinus**3*self.aMinus[self.jointNum]*pMinus)/(self.b + self.MMinus*q)**3 - (self.MPlus**2*self.aPlus[self.jointNum]*self.cMinus[self.jointNum]*pPlusDes)/(self.b + self.MPlus*q)**2 - (self.MMinus**2*self.aMinus[self.jointNum]*self.cMinus[self.jointNum]*pMinusDes)/(self.b + self.MMinus*q)**2
        partialqdot = -(self.MPlus**2*self.aPlus[self.jointNum]*pPlus)/(self.b+self.MPlus*q)**2 -(self.MMinus**2*self.aMinus[self.jointNum]*pMinus)/(self.b+self.MMinus*q)**2
        partialpPlus = -(self.MPlus**2*self.aPlus[self.jointNum]*qdot)/(self.b + self.MPlus*q)**2
        partialpMinus = -(self.MMinus**2*self.aMinus[self.jointNum]*qdot)/(self.b + self.MMinus*q)**2
        partialpPlusDes = (self.MPlus*self.aPlus[self.jointNum]*cMinus[self.jointNum])/(self.b+self.MPlus*q)
        partialpMinusDes = (self.MMinus*self.aMinus[self.jointNum]*cMinus[self.jointNum])/(self.b+self.MMinus*q)
        kdot = self.cPlus[self.jointNum]*k + self.aPlus[self.jointNum]*alphaPlus*cMinus[self.jointNum]*pPlusDes + self.aMinus[self.jointNum]*alphaMius*cPlus[self.jointNum]*pMinusDes - qdot*(self.aPlus[self.jointNum]*alphaPlus**2*pPlus + self.aMinus[self.jointNum]*alphaMius**2*pMinus)

        return partialk, partialq, partialqdot, partialpPlus, partialpMinus, partialpPlusDes, partialpMinusDes, kdot

    def Calc_A_B(self): 
        # add pertinate models to if statment if needed
        
        
        self.model()

        partk = np.zeros(self.numJts)
        partq = np.zeros(self.numJts)
        partqdot = np.zeros(self.numJts)
        partpPlus = np.zeros(self.numJts)
        partpMinus = np.zeros(self.numJts)
        partpPlusDes = np.zeros(self.numJts)
        partpMinusDes = np.zeros(self.numJts)
        kdot = np.zeros(self.numJts)
        
        # print 'number of joint : ', self.numJts, '\n'
        for i in xrange(self.numJts):
            alphaPlus = self.MPlus/(self.MPlus*self.q[i] + self.b)
            alphaMinus = self.MMinus/(self.MMinus*self.q[i] + self.b)
            
            partk[i] = self.cPlus[i]
            partq[i] = self.qdot[i]*(2*(self.MPlus**3)*self.aPlus[i]*self.pPlus[i])/((self.b + self.MPlus*self.q[i])**3) + (2*self.MMinus**3*self.aMinus[i]*self.pMinus[i])/(self.b + self.MMinus*self.q[i])**3 - (self.MPlus**2*self.aPlus[i]*self.cMinus[i]*self.pPlusDesLast[i])/(self.b + self.MPlus*self.q[i])**2 - (self.MMinus**2*self.aMinus[i]*self.cMinus[i]*self.pMinusDesLast[i])/(self.b + self.MMinus*self.q[i])**2
            partqdot[i] = -(self.MPlus**2*self.aPlus[i]*self.pPlus[i])/(self.b+self.MPlus*self.q[i])**2 -(self.MMinus**2*self.aMinus[i]*self.pMinus[i])/(self.b+self.MMinus*self.q[i])**2
            partpPlus[i] = -(self.MPlus**2*self.aPlus[i]*self.qdot[i])/(self.b + self.MPlus*self.q[i])**2
            partpMinus[i] = -(self.MMinus**2*self.aMinus[i]*self.qdot[i])/(self.b + self.MMinus*self.q[i])**2
            partpPlusDes[i] = (self.MPlus*self.aPlus[i]*self.cMinus[i])/(self.b+self.MPlus*self.q[i])
            partpMinusDes[i] = (self.MMinus*self.aMinus[i]*self.cMinus[i])/(self.b+self.MMinus*self.q[i])
            kdot[i] = self.cPlus[i]*self.k[i] + self.aPlus[i]*alphaPlus*self.cMinus[i]*self.pPlusDesLast[i] + self.aMinus[i]*alphaMinus*self.cMinus[i]*self.pMinusDesLast[i] - self.qdot[i]*(self.aPlus[i]*alphaPlus**2*self.pPlus[i] + self.aMinus[i]*alphaMinus**2*self.pMinus[i])
            # print self.q[i], '\n'
            # print self.qdot[i], '\n'
            # print self.pPlus[i], '\n'
            # print self.pMinus[i], '\n'
            # print self.pPlusDesLast[i], '\n'
            # print self.pMinusDesLast[i], '\n'
            # print self.k[i], '\n'
            # partk[i], partq[i], partqdot[i], partpPlus[i], partpMinus[i], partpPlusDes[i], partpMinusDes[i], kdot[i] = self.calcPartials(self.q[i],self.qdot[i],self.pPlus[i],self.pMinus[i],self.pPlusDesLast[i],self.pMinusDesLast[i],self.k[i])
            
            
        z = np.zeros([self.numJts, self.numJts])
            
        Arow1 = np.hstack([-linalg.solve(self.M, self.Kd),
                           -linalg.solve(self.M, self.Ks),
                            linalg.solve(self.M, self.GammaPlus),
                           -linalg.solve(self.M, self.GammaMinus)])

        Arow2 = np.hstack([ np.eye(self.numJts), z, z, z])
        Arow3 = np.hstack([z, z, -self.AlphaPlus, z])
        Arow4 = np.hstack([z, z, z, -self.AlphaMinus])
        Arow5 = np.hstack([np.diag(partqdot),np.diag(partq),np.diag(partpPlus),
                           np.diag(partpMinus),np.diag(partk)])

        AkSimple = self.dt*Arow5
        

        
        np.set_printoptions(precision = 1, linewidth = 250)
        
        # print Arow1, '\n\n'
        # print Arow2, '\n\n'
        # print Arow3, '\n\n'
        # print Arow4, '\n\n'
        # print Arow5, '\n\n'


        A = np.vstack([Arow1, Arow2, Arow3, Arow4])

        # print 'A : ', A, '\n'

        
        I = np.matrix(np.eye(self.numJts,self.numJts))

        Brow1 = np.hstack([-linalg.solve(self.M,I), z, z])
        Brow2 = np.hstack([z, z, z])
        Brow3 = np.hstack([z, self.BetaPlus, z])
        Brow4 = np.hstack([z, z, self.BetaMinus])
        Brow5 = np.hstack([z, np.diag(partpPlusDes), np.diag(partpMinusDes)])
        
        B = np.vstack([Brow1, Brow2, Brow3, Brow4])

        BkSimple = self.dt*Brow5
        
        
        Ad, Bd = self.discretize(A, B, 1)

        #print 'Ad : ', Ad, '\n'

        return A, B, Ad, Bd, AkSimple, BkSimple
        
        

    def discretize(self, A, B, mthd):
        # Matrix Exponential
        if mthd == 1:
            print self.dt
            Ad = linalg.expm2(A*self.dt)
            my_eye = np.eye(self.numJts*4)
            Asinv = linalg.solve(A, my_eye)
            Bd = np.dot(np.dot(Asinv, (Ad-my_eye)), B)
        # Zero order hold, allows A to be singular
        elif mthd == 2:
            C = np.eye(4*self.numJts)
            D = np.zeros((4*self.numJts, 3*self.numJts))
            Ad, Bd, Cd, Dd, dt = signal.cont2discrete((A, B, C, D), self.dt, method='bilinear')
        else:
            rospy.signal_shutdown('No discretization method selected')
            
        return Ad, Bd

    def updateData(self):
        self.lock.acquire()
        try:
            self.q = deepcopy(self.q_)
            self.qdot = deepcopy(self.qdot_)
            self.pPlus = deepcopy(self.pPlus_)
            self.pMinus = deepcopy(self.pMinus_)

            print 'PPLUS ____ : ', self.pPlus, '\n'
            print 'PMINUS ____ : ', self.pMinus, '\n'
            
            self.q[3] = -self.q[3]
            self.qdot[3] = -self.qdot[3]
            for i in xrange(self.numJts):
                # print self.q
                # print 'pPlus_ : ', self.pPlus_, '\n'
                # print 'pPlus : ', self.pPlus, '\n'
                self.pPlus[i] = self.pPlus[i]/1000.
                self.pMinus[i] = self.pMinus[i]/1000.

            self.k_ = self.calcStiff()
            self.k = deepcopy(self.k_)
            self.kdot = self.calcStiffDot()

        finally:
            self.lock.release()

    def setGoal(self):
        if self.goalFlag == 'constant':
            self.qGoal = [-45.0*pi/180., -0.0*pi/180., 20.0*pi/180]  # radians
            self.qdotGoal = [-0.0*pi/180., -0.0*pi/180., -0.0*pi/180]  # radians
        elif self.goalFlag == 'steps':
            # print 'time : ', self.now
            # Goal steps between 2 angles once every 5 seconds
            period = 10
            if math.sin(2*pi*self.now/period) > 0:
                self.qGoal =    [ 30.0*pi/180.,  30.*pi/180.,   30.0*pi/180]
                self.qdotGoal = [  0.0*pi/180.,   0.0*pi/180.,   0.0*pi/180]  # radians
            else:
                self.qGoal =    [-30.0*pi/180., -30.0*pi/180., -30.0*pi/180]
                self.qdotGoal = [  0.0*pi/180.,   0.0*pi/180.,   0.0*pi/180]  # radians

        elif self.goalFlag == 'increment':									# Goal incriments through list of any length  once every 10 seconds
            # pickup on right and move to left then move back to right.
            period = 2
            qlist_1 = [-30.0, -10.0, 10.0, 30.0, 30.0, 30.0, 10.0, -20.0, -40.0]
            qlist_2 = [-20.0, 20.0, 30.0, 20.0, -20.0, 20.0, 25.0, 20.0, -20.0]
            qlist_3 = [-40.0, -30.0, 0.0, 25.0, 40.0, 25.0, 0.0, -30.0, -40.0]

            print_list = [
                'move 1',
                'move 2',
                'move 3',
                'move 4',
                'move 5',
                'move 6',
                'move 7',
                'move 8',
                'move 9']

            qdlist = [-40, -30, 0, 30, 40, 30, 0, -30, -40]
            self.qGoal = [qlist_1[int(self.now/period) %
                        len(qlist_1)]*pi/180., qlist_2[int(self.now/period) %
                        len(qlist_2)]*pi/180., qlist_3[int(self.now/period) %
                        len(qlist_3)]*pi/180.]
            self.qdotGoal = [-0.0*pi/180., -0.0*pi/180., -0.0*pi/180]  # radians
           
            print print_list[int(self.now/period) % len(print_list)], '\n'

        elif self.goalFlag == 'path':
            self.lock.acquire()
            try:
                self.qGoal = deepcopy(self.qGoal_)
                self.qdotGoal = deepcopy(self.qdotGoal_)
            finally:
                self.lock.release()

    def controlLoop(self):

        if self.gotEstimates == False:#or  self.gotPres == False:
            print 'No data \n'
            return

        self.updateData()
        self.setGoal()

        numJts = self.numJts
        A, B, Ad, Bd, AkSimple, BkSimple = self.Calc_A_B()

        A = np.matrix(A)
        B = np.matrix(B)
        Ad = np.matrix(Ad)
        Bd = np.matrix(Bd)

        # print 'A :', A, '\n'
        # print 'B :', B, '\n'
        # print 'Ad :', Ad, '\n'
        # print 'Bd :', Bd, '\n'
        
        print 'PPLUS ____ : ', self.pPlus, '\n'
        print 'PMINUS ____ : ', self.pMinus, '\n'
        #print np.matrix(self.qdot).reshape(numJts,1)
        #print np.matrix(self.q).reshape(numJts,1)
        #print np.matrix(self.pPlus).reshape(numJts,1)
        #print np.matrix(self.pMinus).reshape(numJts,1)

        Amap = np.matrix([[1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
                           0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0])

        myIndex = [self.jointNum,self.jointNum+numJts,self.jointNum+2*numJts,self.jointNum+3*numJts]

        Adistqdot = Ad[self.jointNum,:].A1
        # Adistq = A[self.jointNum+numJts,:].A1
        # AdistpPlus = A[self.jointNum+numJts*2,:].A1
        # AdistpMinus = A[self.jointNum+numJts*3,:].A1 

        Bdistqdot = Bd[self.jointNum,:].A1
        # Bdistq = B[self.jointNum+numJts,:].A1
        # BdistpPlus = B[self.jointNum+numJts*2,:].A1
        # BdistpMinus = B[self.jointNum+numJts*3,:].A1   

        #  self.lin_coeff_A_qdot = []
        #  self.lin_coeff_A_q = []
        #  self.lin_coeff_A_pPlus = []
        #  self.lin_coeff_A_pMinus = []

        # self.lin_coeff_B_qdot = []
        # self.lin_coeff_B_q = []
        # self.lin_coeff_B_pPlus = []
        # self.lin_coeff_B_pMinus = []

        # self.lin_coeff_A_qdot.append(A[self.jointNum,myIndex[0]])
        # self.lin_coeff_A_qdot.append(A[self.jointNum,myIndex[1]])
        # self.lin_coeff_A_qdot.append(A[self.jointNum,myIndex[2]])
        # self.lin_coeff_A_qdot.append(A[self.jointNum,myIndex[3]])

        # self.lin_coeff_A_q.append(A[self.jointNum+numJts,myIndex[0]])
        # self.lin_coeff_A_q.append(A[self.jointNum+numJts,myIndex[1]])
        # self.lin_coeff_A_q.append(A[self.jointNum+numJts,myIndex[2]])
        # self.lin_coeff_A_q.append(A[self.jointNum+numJts,myIndex[3]])

        # self.lin_coeff_A_pPlus.append(A[self.jointNum+2*numJts,myIndex[0]])
        # self.lin_coeff_A_pPlus.append(A[self.jointNum+2*numJts,myIndex[1]])
        # self.lin_coeff_A_pPlus.append(A[self.jointNum+2*numJts,myIndex[2]])
        # self.lin_coeff_A_pPlus.append(A[self.jointNum+2*numJts,myIndex[3]])

        # self.lin_coeff_A_pMinus.append(A[self.jointNum+3*numJts,myIndex[0]])
        # self.lin_coeff_A_pMinus.append(A[self.jointNum+3*numJts,myIndex[1]])
        # self.lin_coeff_A_pMinus.append(A[self.jointNum+3*numJts,myIndex[2]])
        # self.lin_coeff_A_pMinus.append(A[self.jointNum+3*numJts,myIndex[3]])

        # self.lin_coeff_B_qdot.append(B[self.jointNum,myIndex[0]])
        # self.lin_coeff_B_qdot.append(B[self.jointNum,myIndex[1]])
        # self.lin_coeff_B_qdot.append(B[self.jointNum,myIndex[2]])

        # self.lin_coeff_B_q.append(B[self.jointNum+numJts,myIndex[0]])
        # self.lin_coeff_B_q.append(B[self.jointNum+numJts,myIndex[1]])
        # self.lin_coeff_B_q.append(B[self.jointNum+numJts,myIndex[2]])

        # self.lin_coeff_B_pPlus.append(B[self.jointNum+2*numJts,myIndex[0]])
        # self.lin_coeff_B_pPlus.append(B[self.jointNum+2*numJts,myIndex[1]])
        # self.lin_coeff_B_pPlus.append(B[self.jointNum+2*numJts,myIndex[2]])

        # self.lin_coeff_B_pMinus.append(B[self.jointNum+3*numJts,myIndex[0]])
        # self.lin_coeff_B_pMinus.append(B[self.jointNum+3*numJts,myIndex[1]])
        # self.lin_coeff_B_pMinus.append(B[self.jointNum+3*numJts,myIndex[2]])


        # # print Adistqdot
        # # print Adistq
        # # print AdistpPlus
        # # print AdistpMinus


        # for i in xrange(len(myIndex)):
        #     # print i
        #     Adistqdot[myIndex[i]] = 0.0
        #     # Adistq[myIndex[i]] = 0.0
        #     # AdistpPlus[myIndex[i]] = 0.0
        #     # AdistpMinus[myIndex[i]] = 0.0


        # # print Adistqdot
        # # print Adistq
        # # print AdistpPlus
        # # print AdistpMinus



        # # for i in xrange(len(disturbance)):
        #     # lin_coeff = A[i,np.where(Amap[i,:] == 1)]
        #     # disturbance_coeff = np.sum(A[i,np.where(Amap[i,:] == 0)])
        #     # disturbanceBuff = 
        #     # for j in xrange(len(self.))
        #     # disturbance[0] = A


        x = np.vstack([np.matrix(self.qdot).reshape(numJts,1),np.matrix(self.q).reshape(numJts,1),np.matrix(self.pPlus).reshape(numJts,1), np.matrix(self.pMinus).reshape(numJts,1)])
        u = np.vstack([1*np.matrix(self.tauGrav).reshape(numJts,1),np.matrix(self.pPlusDesLast).reshape(numJts,1), np.matrix(self.pMinusDesLast).reshape(numJts,1)])


        # distAqdot = np.dot(Adistqdot,x.A1)
        # # distAq = Adistq*x.A1
        # # distApPlus = AdistpPlus*x.A1
        # # distApMinus = AdistpMinus*x.A1

        # distBqdot = np.dot(Bdistqdot,u.A1)
        # # distBq = Bdistq*u.A1
        # # distBpPlus = BdistpPlus*u.A1
        # # distBpMinus = BdistpMinus*u.A1

        # distqdot = (distAqdot+distBqdot)*self.dt

        # # print 'distAqdot: ', distAqdot, '\n'
        # # print 'distAq: ', distAq, '\n'
        # # print 'distApPlus: ', distApPlus, '\n'
        # # print 'distApMinus: ', distApMinus, '\n'

        # # print 'distBqdot: ', distBqdot, '\n'
        # # print 'distBq: ', distBq, '\n'
        # # print 'distBpPlus: ', distBpPlus, '\n'
        # # print 'distBpMinus: ', distBpMinus, '\n'

        # print 'distqdot: ', distqdot, '\n'




        # #print 'u : ', u, '\n'



        # DistA = A*x
        # DistB = B*u
        DistAd = Ad*x
        DistBd = Bd*u

        # print 'DistAd ', DistAd, '\n'
        # print 'DistBd : ', DistBd, '\n'



        # self.A11 = self.lin_coeff_A_qdot[0]*self.dt + 1.
        # self.A12 = self.lin_coeff_A_qdot[1]*self.dt
        # self.A13 = self.lin_coeff_A_qdot[2]*self.dt
        # self.A14 = self.lin_coeff_A_qdot[3]*self.dt

        # self.A21 = self.lin_coeff_A_q[0]*self.dt
        # self.A22 = self.lin_coeff_A_q[1]*self.dt + 1.
        # self.A23 = self.lin_coeff_A_q[2]*self.dt
        # self.A24 = self.lin_coeff_A_q[3]*self.dt

        # self.A31 = self.lin_coeff_A_pPlus[0]*self.dt
        # self.A32 = self.lin_coeff_A_pPlus[1]*self.dt
        # self.A33 = self.lin_coeff_A_pPlus[2]*self.dt + 1.
        # self.A34 = self.lin_coeff_A_pPlus[3]*self.dt

        # self.A41 = self.lin_coeff_A_pMinus[0]*self.dt
        # self.A42 = self.lin_coeff_A_pMinus[1]*self.dt
        # self.A43 = self.lin_coeff_A_pMinus[2]*self.dt
        # self.A44 = self.lin_coeff_A_pMinus[3]*self.dt + 1.

        # self.B11 = self.lin_coeff_B_qdot[0]*self.dt
        # self.B12 = self.lin_coeff_B_qdot[1]*self.dt
        # self.B13 = self.lin_coeff_B_qdot[2]*self.dt

        # self.B21 = self.lin_coeff_B_q[0]*self.dt
        # self.B22 = self.lin_coeff_B_q[1]*self.dt
        # self.B23 = self.lin_coeff_B_q[2]*self.dt

        # self.B31 = self.lin_coeff_B_pPlus[0]*self.dt
        # self.B32 = self.lin_coeff_B_pPlus[1]*self.dt
        # self.B33 = self.lin_coeff_B_pPlus[2]*self.dt

        # self.B41 = self.lin_coeff_B_pMinus[0]*self.dt
        # self.B42 = self.lin_coeff_B_pMinus[1]*self.dt
        # self.B43 = self.lin_coeff_B_pMinus[2]*self.dt


        # Arow1Dist = DistA[self.jointNum]          - self.A11*self.qdot[self.jointNum] - self.A12*self.q[self.jointNum] - self.A13*self.pPlus[self.jointNum] - self.A14*self.pMinus[self.jointNum]
        # Arow2Dist = DistA[self.jointNum+numJts]   - self.A21*self.qdot[self.jointNum] - self.A22*self.q[self.jointNum] - self.A23*self.pPlus[self.jointNum] - self.A24*self.pMinus[self.jointNum]
        # Arow3Dist = DistA[self.jointNum+2*numJts] - self.A31*self.qdot[self.jointNum] - self.A32*self.q[self.jointNum] - self.A33*self.pPlus[self.jointNum] - self.A34*self.pMinus[self.jointNum]
        # Arow4Dist = DistA[self.jointNum+3*numJts] - self.A41*self.qdot[self.jointNum] - self.A42*self.q[self.jointNum] - self.A43*self.pPlus[self.jointNum] - self.A44*self.pMinus[self.jointNum]

        # Brow1Dist = DistB[self.jointNum]          - self.B12*self.pPlus[self.jointNum] - self.B13*self.pMinus[self.jointNum]
        # Brow2Dist = DistB[self.jointNum+numJts]   - self.B22*self.pPlus[self.jointNum] - self.B23*self.pMinus[self.jointNum]
        # Brow2Dist = DistB[self.jointNum+2*numJts] - self.B32*self.pPlus[self.jointNum] - self.B33*self.pMinus[self.jointNum]
        # Brow2Dist = DistB[self.jointNum+3*numJts] - self.B42*self.pPlus[self.jointNum] - self.B43*self.pMinus[self.jointNum]


        # print 'DistA : ', DistA, '\n'
        # print 'DistB : ', DistB, '\n'



        self.A11d = Ad[self.jointNum,self.jointNum]
        self.A12d = Ad[self.jointNum,self.jointNum+numJts]
        self.A13d = Ad[self.jointNum,self.jointNum+2*numJts]
        self.A14d = Ad[self.jointNum,self.jointNum+3*numJts]
        #self.A15d = Ad[self.jointNum,self.jointNum+4*numJts]

        self.A21d = Ad[self.jointNum+numJts,self.jointNum]
        self.A22d = Ad[self.jointNum+numJts,self.jointNum+numJts]
        self.A23d = Ad[self.jointNum+numJts,self.jointNum+2*numJts]
        self.A24d = Ad[self.jointNum+numJts,self.jointNum+3*numJts]
        #self.A25d = Ad[self.jointNum+numJts,self.jointNum+4*numJts]        

        #self.A31d = Ad[self.jointNum+2*numJts,self.jointNum]
        #self.A32d = Ad[self.jointNum+2*numJts,self.jointNum+numJts]
        self.A33d = Ad[self.jointNum+2*numJts,self.jointNum+2*numJts]
        #self.A34d = Ad[self.jointNum+2*numJts,self.jointNum+3*numJts]
        #self.A35d = Ad[self.jointNum+2*numJts,self.jointNum+4*numJts]        

        #self.A41d = Ad[self.jointNum+3*numJts,self.jointNum]
        #self.A42d = Ad[self.jointNum+3*numJts,self.jointNum+numJts]
        #self.A43d = Ad[self.jointNum+3*numJts,self.jointNum+2*numJts]
        self.A44d = Ad[self.jointNum+3*numJts,self.jointNum+3*numJts]
        #self.A45d = Ad[self.jointNum+3*numJts,self.jointNum+4*numJts]

        self.A51d = AkSimple[self.jointNum,self.jointNum]
        self.A52d = AkSimple[self.jointNum,self.jointNum+numJts]
        self.A53d = AkSimple[self.jointNum,self.jointNum+2*numJts]
        self.A54d = AkSimple[self.jointNum,self.jointNum+3*numJts]
        self.A55d = AkSimple[self.jointNum,self.jointNum+4*numJts]

        
        #self.B11d = Bd[self.jointNum,self.jointNum]
        self.B12d = Bd[self.jointNum,self.jointNum+numJts]
        self.B13d = Bd[self.jointNum,self.jointNum+2*numJts]
        # self.B14d = Bd[self.jointNum,self.jointNum+3*numJts]

        #self.B21d = Bd[self.jointNum+numJts,self.jointNum]
        self.B22d = Bd[self.jointNum+numJts,self.jointNum+numJts]
        self.B23d = Bd[self.jointNum+numJts,self.jointNum+2*numJts]
        # self.B24d = Bd[self.jointNum+numJts,self.jointNum+3*numJts]

        #self.B31d = Bd[self.jointNum+2*numJts,self.jointNum]
        self.B32d = Bd[self.jointNum+2*numJts,self.jointNum+numJts]
        #self.B33d = Bd[self.jointNum+2*numJts,self.jointNum+2*numJts]
        # self.B34d = Bd[self.jointNum+2*numJts,self.jointNum+3*numJts]
        

        #self.B41d = Bd[self.jointNum+3*numJts,self.jointNum]
        #self.B42d = Bd[self.jointNum+3*numJts,self.jointNum+numJts]
        self.B43d = Bd[self.jointNum+3*numJts,self.jointNum+2*numJts]
        # self.B44d = Bd[self.jointNum+3*numJts,self.jointNum+3*numJts]

        #self.B51d = BkSimple[self.jointNum,self.jointNum]
        self.B52d = BkSimple[self.jointNum,self.jointNum+numJts]
        self.B53d = BkSimple[self.jointNum,self.jointNum+2*numJts]



        # Arow1Dist = DistAd[self.jointNum]          - self.A11d*self.qdot[self.jointNum] - self.A12d*self.q[self.jointNum] - self.A13d*self.pPlus[self.jointNum] - self.A14d*self.pMinus[self.jointNum]
        # Arow2Dist = DistAd[self.jointNum+numJts]   - self.A21d*self.qdot[self.jointNum] - self.A22d*self.q[self.jointNum] - self.A23d*self.pPlus[self.jointNum] - self.A24d*self.pMinus[self.jointNum]
        # Arow3Dist = DistAd[self.jointNum+2*numJts] - self.A31d*self.qdot[self.jointNum] - self.A32d*self.q[self.jointNum] - self.A33d*self.pPlus[self.jointNum] - self.A34d*self.pMinus[self.jointNum]
        # Arow4Dist = DistAd[self.jointNum+3*numJts] - self.A41d*self.qdot[self.jointNum] - self.A42d*self.q[self.jointNum] - self.A43d*self.pPlus[self.jointNum] - self.A44d*self.pMinus[self.jointNum]

        # Brow1Dist = DistBd[self.jointNum]          - self.B12d*self.pPlus[self.jointNum] - self.B13d*self.pMinus[self.jointNum]
        # Brow2Dist = DistBd[self.jointNum+numJts]   - self.B22d*self.pPlus[self.jointNum] - self.B23d*self.pMinus[self.jointNum]
        # Brow2Dist = DistBd[self.jointNum+2*numJts] - self.B32d*self.pPlus[self.jointNum] - self.B33d*self.pMinus[self.jointNum]
        # Brow2Dist = DistBd[self.jointNum+3*numJts] - self.B42d*self.pPlus[self.jointNum] - self.B43d*self.pMinus[self.jointNum]

        Arow1Distd = DistAd[self.jointNum]          - self.A11d*self.qdot[self.jointNum] - self.A12d*self.q[self.jointNum] - self.A13d*self.pPlus[self.jointNum] - self.A14d*self.pMinus[self.jointNum]
        Arow2Distd = DistAd[self.jointNum+numJts]   - self.A21d*self.qdot[self.jointNum] - self.A22d*self.q[self.jointNum] - self.A23d*self.pPlus[self.jointNum] - self.A24d*self.pMinus[self.jointNum]
        # Arow3Distd = DistAd[self.jointNum+2*numJts] - self.A31d*self.qdot[self.jointNum] - self.A32d*self.q[self.jointNum] - self.A33d*self.pPlus[self.jointNum] - self.A34d*self.pMinus[self.jointNum]
        # Arow4Distd = DistAd[self.jointNum+3*numJts] - self.A41d*self.qdot[self.jointNum] - self.A42d*self.q[self.jointNum] - self.A43d*self.pPlus[self.jointNum] - self.A44d*self.pMinus[self.jointNum]
        # Arow5Distd = DistAd[self.jointNum+4*numJts] - self.A51d*self.qdot[self.jointNum] - self.A52d*self.q[self.jointNum] - self.A53d*self.pPlus[self.jointNum] - self.A54d*self.pMinus[self.jointNum]

        Brow1Distd = DistBd[self.jointNum]          - self.B12d*self.pPlus[self.jointNum] - self.B13d*self.pMinus[self.jointNum]
        Brow2Distd = DistBd[self.jointNum+numJts]   - self.B22d*self.pPlus[self.jointNum] - self.B23d*self.pMinus[self.jointNum]
        # Brow3Distd = DistBd[self.jointNum+2*numJts] - self.B32d*self.pPlus[self.jointNum] - self.B33d*self.pMinus[self.jointNum]
        # Brow4Distd = DistBd[self.jointNum+3*numJts] - self.B42d*self.pPlus[self.jointNum] - self.B43d*self.pMinus[self.jointNum]
        # Brow5Distd = DistBd[self.jointNum+4*numJts] - self.B52d*self.pPlus[self.jointNum] - self.B53d*self.pMinus[self.jointNum]
 
       


        qdotDistd = (Arow1Distd + Brow1Distd).A1.tolist()[0]
        qDistd = (Arow2Distd + Brow2Distd).A1.tolist()[0]

        # qDist = self.dt*(Arow2Dist + Brow2Dist).A1.tolist()[0]
        # qdotDist = self.dt*(Arow1Dist + Brow1Dist).A1.tolist()[0]
        print 'qDistd : ', qDistd, '\n'
        print 'qdotDistd : ', qdotDistd, '\n'
        # qDistd = (Arow2Distd + Brow2Distd).A1.tolist()[0]
        # qdotDistd = (Arow1Distd + Brow1Distd).A1.tolist()[0]
        # print 'qDistd : ', qDistd, '\n'
        # print 'qdotDistd : ', qdotDistd, '\n'
        # qdist = qDistd
        # qdotDist = qdotDistd
        # qdotDist = float(distqdot)
        # # print qdotDist
        # qDistd = 0.
        # qdotDistd = 0.
        # if self.jointNum == 0:
            # qDist = 0
            # qdotDist = 0

        
        self.error = self.qGoal[self.jointNum] - self.q[self.jointNum]
        # self.errorLast.append(self.error)
        # if self.errorLast[-1] > 0 and self.error < 0:
        #     self.errorInt = 0
        # elif self.errorLast[-1] < 0 and self.error > 0:
        #     self.errorInt = 0
            
        # Ki = self.mpcKi
        Ki = [.001, 0.005, 0.005,0.001]
        #Ki = 0.
        
        self.errorInt = Ki[self.jointNum]*self.error + self.errorInt
        # errorDif = deque()
        # for i in xrange(len(self.errorLast)-1):
        #     errorDif.append(self.errorLast[i+1] - self.errorLast[i])
        
        
        if abs(self.error) > 30.*pi/180.:
            Ki = 0.
            self.errorInt = 0.
            
        max = 1.

        if self.errorInt > max:
            self.errorInt = max
        elif self.errorInt < -max:
            self.errorInt = -max


        pPlusDes, pMinusDes = self.solveMPC(qDistd,qdotDistd)
        
        # value = 1

        # if pPlusDes - self.pPlusDesLast[self.jointNum] > value:
        #     pPlusDes = pPlusDes + value
        # elif pPlusDes - self.pPlusDesLast[self.jointNum] < value:
        #     pPlusDes = pPlusDes - value

        # if pMinusDes - self.pMinusDesLast[self.jointNum] > value:
        #     pMinusDes = pMinusDes + value
        # elif pPlusDes - self.pMinusDesLast[self.jointNum] < value:
        #     pMinusDes = pMinusDes - value


        # if pPlusDes > self.pMax-5.:
        #     pPlusDes = self.pMax-5.
        # elif pPlusDes < self.pMin + 5.:
        #     pPlusDes = self.pMin + 5.

        # if pMinusDes > self.pMax-5.:
        #     pMinusDes = self.pMax-5.
        # elif pMinusDes < self.pMin + 5.:
        #     pMinusDes = self.pMin + 5.


        pPlusDes = self.paps*pPlusDes*1000.+self.poff
        pMinusDes = self.paps*pMinusDes*1000.+self.poff
        msg = mpc_msg([pPlusDes,pMinusDes],1)
        # msg = send_pressures()
        # msg.pPlus = pPlusDes
        # msg.pMinus = pMinusDes
        # msg.jointNum = self.jointNum+1
        self.pressure_cmd.publish(msg)
        newmsg = Float64()
        newmsg.data = self.k[self.jointNum]
        self.pubStiff.publish(newmsg)

        print 'q : ',  self.q, '\n'
        print 'k : ',  self.k, '\n'
        # print self.dt
        print 'qdot : ',  self.qdot, '\n'
        print 'qGoal : ', self.qGoal, '\n'
        print 'kGoal : ', self.kGoal, '\n'
        print 'p+ : ', self.pPlus, '\n'
        print 'p- : ', self.pMinus, '\n'
        print 'p + Des :', (pPlusDes)#-self.poff)/self.paps, '\n'
        print 'p - Des :', (pMinusDes)#-self.poff)/self.paps, '\n'


        if self.record == True:
            self.recordData(pPlusDes,pMinusDes)


    def runControl(self):
        self.controlLoop()

        
    def solveMPC(self, qDist, qdotDist):
        self.qInt = self.errorInt
        self.out_qInt = self.qInt
        self.out_qdotDist = qdotDist
        self.out_qDist = qDist
        # self.out_qdotDist = 0.
        # self.out_qDist = 0.
        self.out_q0 = self.q[self.jointNum]
        self.out_qdot0 = self.qdot[self.jointNum]
        self.out_pPlus0 = self.pPlus[self.jointNum]
        self.out_pMinus0 = self.pMinus[self.jointNum]
        self.out_pPlusDesLast0 = self.pPlusDesLast[self.jointNum]
        self.out_pMinusDesLast0 = self.pMinusDesLast[self.jointNum]
        self.out_k0 = self.k[self.jointNum]
        self.out_kdot = self.kdot[self.jointNum]
        self.out_dt = self.dt
        
        # print 'A11    : ', self.A11
        # print 'A11d   : ', self.A11d
        # print 'A11New : ', (self.lin_coeff_A_qdot[0]*self.dt) + 1

        self.out_A11 = self.A11d
        self.out_A12 = self.A12d
        self.out_A13 = self.A13d
        self.out_A14 = self.A14d

        self.out_A21 = self.A21d
        self.out_A22 = self.A22d
        self.out_A23 = self.A23d
        self.out_A24 = self.A24d

        #self.out_A31 = self.A31d
        #self.out_A32 = self.A32d
        self.out_A33 = self.A33d
        #self.out_A34 = self.A34d

        #self.out_A41 = self.A41d
        #self.out_A42 = self.A42d
        #self.out_A43 = self.A43d
        self.out_A44 = self.A44d

        self.out_A51 = self.A51d
        self.out_A52 = self.A52d
        self.out_A53 = self.A53d
        self.out_A54 = self.A54d
        self.out_A55 = self.A55d

        self.out_B12 = self.B12d
        self.out_B13 = self.B13d

        self.out_B22 = self.B22d
        self.out_B23 = self.B23d

        self.out_B32 = self.B32d
        #self.out_B33 = self.B33d

        #self.out_B42 = self.B42d
        self.out_B43 = self.B43d

        self.out_B52 = self.B52d
        self.out_B53 = self.B53d
        
        #self.Q = #self.Q + 1/(self.error+0.001)
        # self.out_Q = self.Q
        # print 'out_Q  :', self.out_Q
        # #self.R = 2000 + 1000*1/(self.error + 0.01)
        # self.out_R = self.R
        # self.out_S = self.S
        # self.out_Q_end = self.Q_end
        self.out_Qpos = self.Qpos[self.jointNum]
        self.out_Qpres = self.Qpres[self.jointNum]
        self.out_Qvel = self.Qvel[self.jointNum]
        self.out_Qstiff = self.Qstiff[self.jointNum]
        self.out_Qsdot = self.Qsdot[self.jointNum]
        if self.qGoal[self.jointNum] > self.qMax:
            self.qGoal[self.jointNum] = self.qMax
        elif self.qGoal[self.jointNum] < self.qMin:
            self.qGoal[self.jointNum] = self.qMin
        self.out_qGoal = self.qGoal[self.jointNum] + self.errorInt
        # else:
        #     self.out_qGoal = self.qGoal[self.jointNum]
        print 'Out q Goal : ', self.out_qGoal
        self.out_qdotGoal = self.qdotGoal[self.jointNum]
        self.out_kGoal = self.kGoal[self.jointNum]
        print 'Out k Goal : ', self.out_kGoal
        self.out_pPlusT = self.pPlusT
        self.out_pMinusT = self.pMinusT

        self.out_pMax = self.pMax
        self.out_pMin = self.pMin
        self.out_qMax = self.qMax
        self.out_qMin = self.qMin
        # self.out_pDelta = self.pDelta


        self.debug = False
        if self.debug:
            #print 'Ad : ', Ad
            #print 'Bd : ', Bd
            print 'A11 : ',  float(self.out_A11)
            print 'A12 : ',  float(self.out_A12)
            print 'A13 : ',  float(self.out_A13)
            print 'A14 : ',  float(self.out_A14)
            print 'A21 : ',  float(self.out_A21)
            print 'A22 : ',  float(self.out_A22)
            print 'A23 : ',  float(self.out_A23)
            print 'A24 : ',  float(self.out_A24)
            print 'A33 : ',  float(self.out_A33)
            print 'A44 : ',  float(self.out_A44)
            print 'A51 : ',  float(self.out_A51)
            print 'A52 : ',  float(self.out_A52)
            print 'A53 : ',  float(self.out_A53)
            print 'A54 : ',  float(self.out_A54)
            print 'A55 : ',  float(self.out_A55)
            print 'B12 : ',  float(self.out_B12)
            print 'B13 : ',  float(self.out_B13)
            print 'B22 : ',  float(self.out_B22)
            print 'B23 : ',  float(self.out_B23)
            print 'B32 : ',  float(self.out_B32)
            print 'B43 : ',  float(self.out_B43)
            print 'B52 : ',  float(self.out_B52)
            print 'B53 : ',  float(self.out_B53)
            print 'qdot0 : ',  self.out_qdot0
            print 'q0 : ',  float(self.out_q0)
            print 'pPlus0 : ',  self.out_pPlus0
            print 'pMinus0 : ',  self.out_pMinus0
            print 'pPlusDes0 : ',  self.out_pPlusDesLast0
            print 'pMinusDes0 : ',  self.out_pMinusDesLast0
            print 'k0 : ',  float(self.out_k0)
            print 'kdot : ',  float(self.out_kdot)
            print 'dt : ',  float(self.out_dt)
            print 'qGoal : ',  self.out_qGoal
            # print 'qdot_goal : ',  self.out_qdotGoal
            print 'kGoal : ', self.out_kGoal
            print 'qMax : ',  self.out_qMax
            print 'qMin : ',  self.out_qMin
            print 'pMax : ',  self.out_pMax
            print 'pMin : ',  self.out_pMin
            print 'qdot_dist : ',  float(self.out_qdotDist)
            print 'q_dist : ',  float(self.out_qDist)
            print 'Qstiff : ',  self.out_Qstiff
            print 'Qsdot : ',  self.out_Qsdot
            print 'Qpres : ',  self.out_Qpres
            print 'Qpos : ',  self.out_Qpos
            print 'Qvel : ',  self.out_Qvel

            # # print 'R : ',  self.out_R
            # # print 'S : ',  self.out_S
            # # print 'Q_end: ',  self.out_Q_end
            # print 'q_goal : ',  self.out_qGoal
            # # print 'qdot_goal : ',  self.out_qdotGoal
            # print 'k_goal : ', self.out_kGoal
            # print 'p_plus_t : ',  self.out_pPlusT
            # print 'p_mius_t : ',  self.out_pMinusT
            # print 'p_max : ',  self.out_pMax
            # print 'p_min : ',  self.out_pMin
            # print 'q_max : ',  self.out_qMax
            # print 'q_min : ',  self.out_qMin
            # # print 'p_delta : ',  self.out_pDelta

            #raw_input()



        cmd = self.solver.runController(float(self.out_A11),
                                        float(self.out_A12),
                                        float(self.out_A13),
                                        float(self.out_A14),
                                        float(self.out_A21),
                                        float(self.out_A22),
                                        float(self.out_A23),
                                        float(self.out_A24),
                                        float(self.out_A33),
                                        float(self.out_A44),
                                        float(self.out_A51),
                                        float(self.out_A52),
                                        float(self.out_A53),
                                        float(self.out_A54),
                                        float(self.out_A55),
                                        float(self.out_B12),
                                        float(self.out_B13),
                                        float(self.out_B22),
                                        float(self.out_B23),
                                        float(self.out_B32),
                                        float(self.out_B43),
                                        float(self.out_B52),
                                        float(self.out_B53),
                                        float(self.out_qdot0),
                                        float(self.out_q0),
                                        float(self.out_pPlus0),
                                        float(self.out_pMinus0),
                                        float(self.out_pPlusDesLast0),
                                        float(self.out_pMinusDesLast0),
                                        float(self.out_k0),
                                        float(self.out_kdot),
                                        float(self.out_dt),
                                        float(self.out_qGoal),
                                        float(self.out_kGoal),
                                        float(self.out_qMax),
                                        float(self.out_qMin),
                                        float(self.out_pMax),
                                        float(self.out_pMin),
                                        float(self.out_qdotDist),
                                        float(self.out_qDist),
                                        float(self.out_Qstiff),
                                        float(self.out_Qsdot),
                                        float(self.out_Qpres),
                                        float(self.out_Qpos),
                                        float(self.out_Qvel))

        print 'cmd : ', cmd, '\n'

        #raw_input()
        pPlusDes = cmd[0][0]
        pMinusDes = cmd[1][0]
        self.pPlusDesLast[self.jointNum] = pPlusDes
        self.pMinusDesLast[self.jointNum] = pMinusDes
        
        # if self.jointNum==3:
        #     self.pPlusDesLast[self.jointNum] = pMinusDes
        #     self.pMinusDesLast[self.jointNum] = pPlusDes
        #     pPlusDes = cmd[1][0]
        #     pMinusDes = cmd[0][0]

        return pPlusDes, pMinusDes


    def recordData(self,pPlusDes,pMinusDes):
        
        self.qSave.append(deepcopy(self.q))
        self.qGoalSave.append(deepcopy(self.qGoal))
        self.qdotSave.append(deepcopy(self.qdot))
        self.pPlusSave.append(deepcopy(self.pPlus))
        self.pMinusSave.append(deepcopy(self.pMinus))
        self.pPlusDesSave.append(deepcopy(pPlusDes))
        self.pMinusDesSave.append(deepcopy(pMinusDes))
        self.timeSave.append(time.time()-self.start)
        self.kSave.append(deepcopy(self.k))

    def shutdown(self):

        if self.record:
            print 'Recording Data'
            dataDict = {
                't': self.timeSave,
                'q': self.qSave,
                'q_goal': self.qGoalSave,
                'qdot': self.qdotSave,
                'p_plus': self.pPlusSave,
                'p_minus': self.pMinusSave,
                'p_plus_des': self.pPlusDesSave,
                'p_minus_des': self.pMinusDesSave,
                'k':self.kSave}
                #'q_solve': self.qSolveSave}#,
                #'qdot_solve': self.qdotSolveSave}#,
                #'p_plus_solve': self.p_plusSolve_save,
                #'p_minus_solve': self.p_minus_solve_save,
                #'p_plus_des_solve': self.p_plus_des_solve_save,
                #'p_minus_des_solve': self.p_minus_des_solve_save}
            sio.savemat('/home/radlab/data/kl_1_dof_mpc_joint_'+ str(self.jointNum)+'_kl_stiff_drop_6', dataDict)

            print 'Data Recorded'
