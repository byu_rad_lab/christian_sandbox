#!/usr/bin/env python
import rospy
import numpy as np
import math
import time
from copy import deepcopy
from nasa_mpc.msg import send_pressures

from falkor.srv import set_robot_ctrl_mode
from falkor.srv import move_joint_prs

rospy.init_node('phil_kaa',anonymous=True)
rate = rospy.Rate(500)
sendPrsCmd = rospy.ServiceProxy('MoveJointPrs', move_joint_prs, persistent=True)
setCtrl = rospy.ServiceProxy('/SetRobotCtrlMode', set_robot_ctrl_mode, persistent=True)

try:
    worked = setCtrl(2)
except rospy.ServiceException as e:
    print 'Error :', e


def send_pressures_to_kaa(pressures):
    move_time = 0
    move_type = 1
    sendPrsCmd(0,[pressures[0],pressures[1],pressures[2],pressures[3]],move_time,move_type)
    sendPrsCmd(1,[pressures[4],pressures[5],pressures[6],pressures[7]],move_time,move_type)
    sendPrsCmd(2,[pressures[8],pressures[9],pressures[10],pressures[11]],move_time,move_type)


def main():
    start = time.time()
    while time.time()-start < 20.:
        pressures = np.array([0,200,0,200,0,200,0,200,0,200,0,200])*1000.0
        send_pressures_to_kaa(pressures)
        rate.sleep()
    start = time.time()
    while time.time()-start < 20.:
        pressures = np.array([200,0,200,0,200,0,200,0,200,0,200,0])*1000.0
        send_pressures_to_kaa(pressures)
        rate.sleep()
    start = time.time()
    while time.time()-start < 10.:
        send_pressures_to_kaa([0,0,0,0,0,0,0,0,0,0,0,0])
        rate.sleep()
        
    

if __name__=='__main__':
    print "Starting"
    main()
