#!/usr/bin/env python

import rospy
import numpy as np
import math
import time
from copy import deepcopy
import scipy.io as sio
from scipy.linalg import expm
from scipy import linalg
from scipy import signal
from std_msgs.msg import Float64
from threading import RLock, Timer
from collections import deque
from math import pi, cos, sin
import argparse

import falkor.msg as robot_msgs
#from nasa_mpc.msg import angles_velocity as angles
from byu_robot.msg import commands
from nasa_mpc.msg import robot_ready
from nasa_mpc.msg import send_pressures
from nasa_mpc.msg import sim_robot

from falkor.srv import set_robot_ctrl_mode
from falkor.srv import move_joint_prs
from falkor.srv import move_joint_valve
from byu_robot.msg import states

#from serial_data.msg import calibration2 as tactileData

from threading import RLock, Timer
from collections import deque
import tf.transformations as tft

from geometry_msgs.msg import PoseArray
#from nasa_4_dof_dyn_params import params
#import nasa_4_dof_dynamics as nasaDyn

from dynamic_reconfigure.server import Server as DynamicReconfigureServer
from byu_robot.cfg import pidcontrollerConfig as ConfigType

class PDControl():

    def __init__(self, robotDyn, params):

        self.rate = rospy.get_param('Controller_PID_Rate')

        self.debug = False
        self.simulation = rospy.get_param('state_estimator/Simulator_Subscriber')

        self.dt = 1.0/self.rate
        self.numJts = rospy.get_param('DOF')
        self.start = time.time()
        self.lock = RLock()

        self.q_ = [None]*self.numJts
        self.qdot_ = [None]*self.numJts
        self.pPlus_ = [0]*self.numJts
        self.pMinus_ = [0]*self.numJts

        self.q = [None]*self.numJts
        self.qdot = [None]*self.numJts
        self.pPlus = [0]*self.numJts
        self.pMinus = [0]*self.numJts

        self.qGoal = [0.0]*self.numJts
        self.qdotGoal = [0.0]*self.numJts
        self.qGoal_ = self.qGoal
        self.qdotGoal_ = self.qdotGoal

        self.gotEstimates = False
        self.goalFlag = 3
        self.robotDyn = robotDyn
        self.params = params

        # initialize imu values/parameters
        self.acc_x_ = []
        self.acc_x = []
        self.acc_y_ = []
        self.acc_y = []
        self.acc_z_ = []
        self.acc_z = []
        self.gyro_x_ = []
        self.gyro_x = []
        self.gyro_y_ = []
        self.gyro_y = []
        self.gyro_z_ = []
        self.gyro_z = []
        self.eInt = np.array([0]*self.numJts)
        self.eLast = np.array([0., 0., 0., 0.])


        self.Kp = [1.0]*self.numJts
        self.Kd = [0.0001]*self.numJts
        self.Ki = [0.0001]*self.numJts
        self.maxInt = [1.0]*self.numJts
        self.Kff = [3.0]*self.numJts

        self.pmax = 150.0
        self.pmin = 13.0
        self.commandMode = 0



        # subscribers and service calls
        #self.setCtrl = rospy.ServiceProxy('/SetRobotCtrlMode', set_robot_ctrl_mode, persistent=True)

        # try:
        #    worked = self.setCtrl(self.control)
        # except rospy.ServiceException as e:
        #    print 'Error :', e

        #self.sendValveCmd = rospy.ServiceProxy('MoveJointValve', move_joint_valve, persistent=True)
        #self.sendPrsCmd = rospy.ServiceProxy('MoveJointPrs', move_joint_prs, persistent=True)

        self.subscriber = rospy.Subscriber('/states', states, self.Callback)
        self.server = DynamicReconfigureServer(ConfigType, self.Reconfigure)


        if self.simulation:
            rospy.Subscriber('/robot_state', sim_robot, self.simulationCallback)
            # rospy.Subscriber("/robot_angles", angles, self.angles_callback)
            # rospy.Subscriber('robot_state', robot_msgs.robot_state, self.robot_state_callback)

        #self.pressure_pub_sim0 = rospy.Publisher('/pressure_0_des', Float64, queue_size=1000)
        #self.pressure_pub_sim1 = rospy.Publisher('/pressure_1_des', Float64, queue_size=1000)

        self.pressure_cmd = [None]*self.numJts
        for i in xrange(self.numJts):
            self.pressure_cmd[i] = rospy.Publisher('/pressure_cmd_' + str(i+1), send_pressures, queue_size=10)

        if self.goalFlag == 3:
            rospy.Subscriber("/commands", commands, self.commands_callback)

    def Callback(self, msg):
        self.q_ = list(msg.q)
        self.qdot_ = list(msg.qdot)
        self.pPlus_ = list(msg.pPlus)
        self.pMinus_ = list(msg.pMinus)
        self.gotEstimates = True

    def commands_callback(self,msg):
        self.qGoal_ = deepcopy(list(msg.q))
        self.qdotGoal_ = deepcopy(list(msg.qdot))
        for i in xrange(len(self.qGoal_)):
            self.qGoal_[i] = -self.qGoal_[i]
            self.qdotGoal_[i] = -self.qdotGoal_[i]

    def simulationCallback(self, data):
        # print 'Got to sim callback'
        # print data.q[0][
        # print self.q_[0]
        # for i in xrange(data.numJts):
        #    self.q_[i] = data.q[i]
        self.q_ = data.q
        # print self.q_
        self.qdot_ = data.qdot
        self.pPlus_ = data.pPlus
        self.pMinus_ = data.pMinus
        self.gotEstimates = True

    def angles_callback(self, msg):
        for i in range(0, self.numJts):
            self.q_[i] = msg.q[i]
            self.qdot_[i] = msg.qdot[i]

        self.gotEstimates = True

    def Reconfigure(self, config, level):
        self.Kp[0] = config["Kp0"]
        self.Kp[1] = config["Kp1"]
        self.Kp[2] = config["Kp2"]
        self.Kp[3] = config["Kp3"]
        self.Kp[4] = config["Kp4"]
        self.Kp[5] = config["Kp5"]

        self.Kd[0] = config["Kd0"]
        self.Kd[1] = config["Kd1"]
        self.Kd[2] = config["Kd2"]
        self.Kd[3] = config["Kd3"]
        self.Kd[4] = config["Kd4"]
        self.Kd[5] = config["Kd5"]

        self.Ki[0] = config["Ki0"]
        self.Ki[1] = config["Ki1"]
        self.Ki[2] = config["Ki2"]
        self.Ki[3] = config["Ki3"]
        self.Ki[4] = config["Ki4"]
        self.Ki[5] = config["Ki5"]

        self.maxInt[0] = config["MaxInt0"]
        self.maxInt[1] = config["MaxInt1"]
        self.maxInt[2] = config["MaxInt2"]
        self.maxInt[3] = config["MaxInt3"]
        self.maxInt[4] = config["MaxInt4"]
        self.maxInt[5] = config["MaxInt5"]

        self.Kff[0] = config["Kff0"]
        self.Kff[1] = config["Kff1"]
        self.Kff[2] = config["Kff2"]
        self.Kff[3] = config["Kff3"]
        self.Kff[4] = config["Kff4"]
        self.Kff[5] = config["Kff5"]

        if config["Save_Gains"] == True:
            rosparam.dump_params("/home/rtpc/git/ego/ego-projects-byu/src/byu_robot/yaml/pid_control_gains_" + str(self.jointNum) + ".yaml", "/pid_control_" + str(self.jointNum))
            self.config["Save_Gains"] = False

            
        return config


    def robot_state_callback(self, msg):
        robot_state = msg
        #self.p_plus_ = [robot_state.sensors[0].prs_cal[0]*self.paps, robot_state.sensors[1].prs_cal[0]*self.paps]
        #self.p_minus_ = [robot_state.sensors[0].prs_cal[1]*self.paps, robot_state.sensors[1].prs_cal[1]*self.paps]
        for i in range(0, self.numJts):
            self.pPlus_[i] = robot_state.sensors[i].prs_cal[0]/1000.0
            self.pMinus_[i] = robot_state.sensors[i].prs_cal[1]/1000.0

            # self.acc_x_ = [
            #     robot_state.sensors[0].acc_cal[0],
            #     robot_state.sensors[1].acc_cal[0],
            #     robot_state.sensors[3].acc_cal[0]]
            # self.acc_y_ = [
            #     robot_state.sensors[0].acc_cal[1],
            #     robot_state.sensors[1].acc_cal[1],
            #     robot_state.sensors[3].acc_cal[1]]
            # self.acc_z_ = [
            #     robot_state.sensors[0].acc_cal[2],
            #     robot_state.sensors[1].acc_cal[2],
            #     robot_state.sensors[3].acc_cal[2]]
            # self.gyro_x_ = [
            #     robot_state.sensors[0].gyr_cal[0],
            #     robot_state.sensors[1].gyr_cal[0],
            #     robot_state.sensors[3].gyr_cal[0]]
            # self.gyro_y_ = [
            #     robot_state.sensors[0].gyr_cal[1],
            #     robot_state.sensors[1].gyr_cal[1],
            #     robot_state.sensors[3].gyr_cal[1]]
            # self.gyro_z_ = [
            #     robot_state.sensors[0].gyr_cal[2],
            #     robot_state.sensors[1].gyr_cal[2],
            #     robot_state.sensors[3].gyr_cal[2]]

    def model_4(self):
        self.tauGrav = self.robotDyn.g(self.params, self.qGoal)

    def updateData(self):
        self.lock.acquire()
        try:
            self.q = deepcopy(self.q_)
            self.qdot = deepcopy(self.qdot_)
            self.pPlus = deepcopy(self.pPlus_)
            self.pMinus = deepcopy(self.pMinus_)
            for i in xrange(len(self.q_)):
                self.q[i] = -self.q[i]
                self.qdot[i] = -self.qdot[i]
                self.pPlus[i] = self.pPlus[i]/1000.
                self.pMinus[i] = self.pMinus[i]/1000.

        finally:
            self.lock.release()

    def setGoal(self):
        if self.goalFlag == 0:
            # self.qGoal = [-20.0*pi/180., -0.0*pi/180., -0.0*pi/180]  # radians
            # self.qdotGoal = [-0.0*pi/180., -0.0*pi/180., -0.0*pi/180]  # radians
            print 'Goal Set at beginning'
        elif self.goalFlag == 1:
            # print 'time : ', self.now
            # Goal steps between 2 angles once every 5 seconds
            period = 10
            if math.sin(2*pi*self.now/period) > 0:
                self.qGoal = (np.ones(self.numJts)*30*pi/180.0).tolist()
                self.qdotGoal = (np.ones(self.numJts)*0).tolist()
            else:
                self.qGoal = (np.ones(self.numJts)*-30*pi/180.0).tolist()
                self.qdotGoal = (np.ones(self.numJts)*0).tolist()

        elif self.goalFlag == 2:									# Goal incriments through list of any length  once every 10 seconds
            # pickup on right and move to left then move back to right.
            #NOT WORKING
            print("this isn't implemented!")
            assert(False)
            period = 2
            # qlist_1 = [-30.0, -10.0, 10.0, 30.0, 30.0, 30.0, 10.0, -20.0, -40.0]
            # qlist_2 = [-20.0, 20.0, 30.0, 20.0, -20.0, 20.0, 25.0, 20.0, -20.0]
            # qlist_3 = [-40.0, -30.0, 0.0, 25.0, 40.0, 25.0, 0.0, -30.0, -40.0]

            # print_list = [
            #     'move 1',
            #     'move 2',
            #     'move 3',
            #     'move 4',
            #     'move 5',
            #     'move 6',
            #     'move 7',
            #     'move 8',
            #     'move 9']

            # qdlist = [-40, -30, 0, 30, 40, 30, 0, -30, -40]
            # #qdlist = [-60, -30, 0, 30, 60, 30, 0, -30,-60]
            # # qdlist = [-90, -45, 0, 45, 90, 45, 0, -45,-90]
            # # qdlist = [-90, 90]
            # # qdlist = [-75, -75, -85, -80, -85, -80, -85, -80, -85, -80, -85, -80, -85, -80, 0, 0, 0, 0, 0, 0]
            # self.qGoal = [qlist_1[int(self.now/period) %
            #                       len(qlist_1)]*pi/180., qlist_2[int(self.now/period) %
            #                                                      len(qlist_2)]*pi/180., qlist_3[int(self.now/period) %
            #                                                                                     len(qlist_3)]*pi/180.]
            # self.qdotGoal = [-0.0*pi/180., -0.0*pi/180., -0.0*pi/180]  # radians

            # print print_list[int(self.now/period) % len(print_list)], '\n'

        elif self.goalFlag == 3:
            self.lock.acquire()
            try:
                self.qGoal = self.qGoal_
                self.qdotGoal = self.qdotGoal_
            finally:
                self.lock.release()

            dist = np.linalg.norm(np.array(self.q)-np.array(self.qGoal))
            msg = robot_ready()
            # if dist < self.distance_to_path_tol:
            #    msg.robot_is_ready = True
            # else:
            #    msg.robot_is_ready = False
            # self.on_path_publisher.publish(msg)

        print 'self.qGoal : ', self.qGoal, '\n'

    def runControl(self):

        self.now = time.time() - self.start

        if not self.gotEstimates:
            print "No Data"
            return
        self.updateData()
        self.model_4()

        self.setGoal()

        # KpSched = [25, 25, 25, 25]

        # Kd = 0.1
        # Ki = 0.1

        e = np.array(self.qGoal) - np.array(self.q)
        ed = np.array(self.qdotGoal) - np.array(self.qdot)
        self.eInt = self.eInt + e


        for i in xrange(len(self.maxInt)):
            if self.eInt[i] > self.maxInt[i]:
                self.eInt[i] = self.maxInt[i]

        print 'self.eInt : ', self.eInt, '\n'



        # Kp = np.matrix(np.eye(self.numJts))
# 
        # Kp[1, 1] = 20
        # Kp[2, 2] = 150
        # Kp[3, 3] = 150
# 
        # if e[0] >= 0.2:
            # Kp[0, 0] = KpSched[0]
        # if e[0] >= 0.15 and e[0] < 0.3:
            # Kp[0, 0] = KpSched[1]
        # elif e[0] >= 0.1 and e[0] < 0.15:
            # Kp[0, 0] = KpSched[2]
        # elif e[0] >= 0.05 and e[0] < 0.1:
            # Kp[0, 0] = KpSched[3]
        # elif e[0] < 0.05:
            # Kp[0, 0] = KpSched[3]
        # else:
            # Kp[0, 0] = KpSched[0]

        # if abs(e[0]-self.eLast[0]) > 0.1:
            # self.int[0] = 0.
        # if abs(e[0]-self.eLast[0]) < 0.1 and abs(e[0]) > 0.1:
            # self.int[0] = self.int[0] + e[0]
# 
        # if abs(e[1]-self.eLast[1]) > 0.1:
            # self.int[1] = 0.
        # if abs(e[1]-self.eLast[1]) < 0.1 and abs(e[1]) > 0.1:
            # self.int[1] = self.int[1] + e[1]
# 
        # if abs(e[2]-self.eLast[2]) > 0.1:
            # self.int[2] = 0.
        # if abs(e[2]-self.eLast[2]) < 0.1 and abs(e[2]) > 0.1:
            # self.int[2] = self.int[2] + e[2]
# 
        # if abs(e[3]-self.eLast[3]) > 0.1:
            # self.int[3] = 0.
        # if abs(e[3]-self.eLast[3]) < 0.1 and abs(e[3]) > 0.1:
            # self.int[3] = self.int[3] + e[3]

            # print 'here'

        # print ' int : ', self.int, '\n\n'
        # print ' e-eLast : ', e[0] - self.eLast[0], '\n\n'
        print 'e : ', e, '\n\n'
        # print temp.A1.tolist()
        # print ed
        # Kgrav = np.array([3.0, 1.0, 4.5,4.5,4.5,4.5])


        # print self.tauGrav
        tauP = -np.array(self.Kff)*np.array(self.tauGrav)

        #print e
        #print type(e)
        #print np.array(self.Kp)*e
        # print Kd*ed
        # print tauP
        #delP =  tauP
        tempKpe = np.array(self.Kp)*e
        tempKde = np.array(self.Kd)*ed
        tempKie = np.array(self.Ki)*self.eInt

        # tempKie = 0.0

        delP = tempKpe + tempKde + tempKie + tauP

        print delP


        pMid = np.array([80.,80.,80.,60.,40.,40.])
        pPlusBuff = pMid + delP/2.
        pMinusBuff = pMid - delP/2.

        # print 'pPlusBuff : ', pPlusBuff, '\n'
        # print 'pMinusBuff : ', pMinusBuff, '\n'

        for j in xrange(self.numJts):
            if pMinusBuff[j] < self.pmin:
                pPlusBuff[j] = pPlusBuff[j] + self.pmin - pMinusBuff[j]
                pMinusBuff[j] = self.pmin
            elif pPlusBuff[j] < self.pmin:
                pMinusBuff[j] = pMinusBuff[j] + self.pmin - pPlusBuff[j]
                pPlusBuff[j] = self.pmin
            if pPlusBuff[j] > self.pmax:
                pPlusBuff[j] = self.pmax
            if pMinusBuff[j] > self.pmax:
                pMinusBuff[j] = self.pmax

        print 'pPlusBuff : ', pPlusBuff, '\n'
        print 'pMinusBuff : ', pMinusBuff, '\n'

        pPlusCmd = pPlusBuff
        pMinusCmd = pMinusBuff

        moveTime = 0.0
        moveType = 1  # 0 = Stop, 1 = Step, 2 = Sinusoid, 3 = CubicSpline

        # sendPres0 = [pPlusBuff[0]*1000., pMinusBuff[0]*1000., pPlusBuff[1]*1000., pMinusBuff[1]*1000.]
        # sendPres1 = [pPlusBuff[2]*1000., pMinusBuff[2]*1000., pPlusBuff[3]*1000., pMinusBuff[3]*1000.]
        self.eLast = e

        #result = self.sendPrsCmd(0, sendPres0, moveTime, moveType)
        #result = self.sendPrsCmd(1, sendPres1, moveTime, moveType)
        jointsToTune = [0,1,2,3,4,5]

        self.commandMode = rospy.get_param("state_commander/Command_Mode")
        print "Command Mode ----: ", self.commandMode, '\n\n\n\n\n'
        if self.commandMode == 9:
            pPlusCmd[1] = 13000.
            pMinusCmd[1] = 80000.
            pPlusCmd[3] = 10000.
            pMinusCmd[3] = 100000.
        # pMinusCmd[3] = 10000.
        # pPlusCmd[3] = 30000.
        for i in jointsToTune:
            msg = send_pressures()
            msg.pPlus = pPlusCmd[i]
            msg.pMinus = pMinusCmd[i]
            msg.jointNum = i+1
            self.pressure_cmd[i].publish(msg)

    def shutdown(self):
        print "Shutting Down PID Controller Node........"
    
if __name__ == '__main__':
    rospy.init_node('pd_control', anonymous=True)
    numJts = rospy.get_param('DOF')
    robotDyn = importlib.import_module("models.nasa_"+str(numJts)+"_dof_dynamics")
    params = dyn_params.compute(numJts)
    controller = PDControl(robotDyn, params)
    rate = rospy.Rate(controller.rate)

    while not rospy.is_shutdown():
        controller.runControl()
        rate.sleep()
