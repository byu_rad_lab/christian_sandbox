#!/usr/bin/env python

import rospy
import numpy as np
import math
import time
from copy import deepcopy
from nasa_mpc.msg import send_pressures
from pdb import set_trace as pause

print "done importing"

def main():
    rospy.init_node('swing_kaa',anonymous=True)
    rate = rospy.Rate(500)
    pubJoint1 = rospy.Publisher('/pressure_cmd_1',send_pressures,queue_size=10)
    pubJoint2 = rospy.Publisher('/pressure_cmd_2',send_pressures,queue_size=10)
    pubJoint3 = rospy.Publisher('/pressure_cmd_3',send_pressures,queue_size=10)
    pubJoint4 = rospy.Publisher('/pressure_cmd_4',send_pressures,queue_size=10)
    pubJoint5 = rospy.Publisher('/pressure_cmd_5',send_pressures,queue_size=10)
    pubJoint6 = rospy.Publisher('/pressure_cmd_6',send_pressures,queue_size=10)
    while True:
        # start = time.time()
        # print "Here"
        # print "Sending"
        # msg1 = send_pressures()
        # msg2 = send_pressures()
        # msg3 = send_pressures()
        # msg4 = send_pressures()
        # msg5 = send_pressures()
        # msg6 = send_pressures()
        # msg1.pPlus = 150
        # msg1.pMinus = 10
        # msg1.jointNum = 1
        # msg2.pPlus = 50
        # msg2.pMinus = 150
        # msg2.jointNum = 2
        # msg3.pPlus = 50
        # msg3.pMinus = 150
        # msg3.jointNum = 3
        # msg4.pPlus = 50
        # msg4.pMinus = 150
        # msg4.jointNum = 4
        # msg5.pPlus = 50
        # msg5.pMinus = 150
        # msg5.jointNum = 5
        # msg6.pPlus = 150
        # msg6.pMinus = 50
        # msg6.jointNum = 6
        # pubJoint1.publish(msg1)
        # pubJoint2.publish(msg2)
        # pubJoint3.publish(msg3)
        # pubJoint4.publish(msg4)
        # pubJoint5.publish(msg5)
        # pubJoint6.publish(msg6)
        # rate.sleep()

        if False:
            msg1 = send_pressures()
            msg2 = send_pressures()
            msg3 = send_pressures()
            msg4 = send_pressures()
            msg5 = send_pressures()
            msg6 = send_pressures()
            msg1.pPlus = 30
            msg1.pMinus = 150
            msg1.jointNum = 1
            msg2.pPlus = 150
            msg2.pMinus = 50
            msg2.jointNum = 2
            msg3.pPlus = 150
            msg3.pMinus = 10
            msg3.jointNum = 3
            msg4.pPlus = 150
            msg4.pMinus = 50
            msg4.jointNum = 4
            msg5.pPlus = 150
            msg5.pMinus = 50
            msg5.jointNum = 5
            msg6.pPlus = 50
            msg6.pMinus = 150
            msg6.jointNum = 6
            pubJoint1.publish(msg1)
            pubJoint2.publish(msg2)
            pubJoint3.publish(msg3)
            pubJoint4.publish(msg4)
            pubJoint5.publish(msg5)
            pubJoint6.publish(msg6)
            rate.sleep()

        if False:
            print "Sending"
            msg1 = send_pressures()
            msg1.pPlus = 200
            msg1.pMinus = 10
            msg1.jointNum = 1
            msg2 = send_pressures()
            msg2.pPlus = 50
            msg2.pMinus = 50
            msg2.jointNum = 2
            pubJoint1.publish(msg1)
            pubJoint2.publish(msg2)
            msg1.pPlus = 10
            msg1.pMinus = 150
            msg1.jointNum = 3
            pubJoint3.publish(msg1)
            msg2.jointNum = 4
            pubJoint4.publish(msg2)
            msg1.jointNum = 5
            pubJoint5.publish(msg1)
            msg2.jointNum = 6
            pubJoint6.publish(msg2)
            rate.sleep()
        if True:
            print "Sending"
            msg1 = send_pressures()
            msg1.pPlus = 10
            msg1.pMinus = 150
            msg1.jointNum = 1
            msg2 = send_pressures()
            msg2.pPlus = 50
            msg2.pMinus = 50
            msg2.jointNum = 2
            pubJoint1.publish(msg1)
            pubJoint2.publish(msg2)
            msg1.pPlus = 150
            msg1.pMinus = 10
            msg1.jointNum = 3
            pubJoint3.publish(msg1)
            msg2.jointNum = 4
            pubJoint4.publish(msg2)
            msg1.jointNum = 5
            pubJoint5.publish(msg1)
            msg2.jointNum = 6
            pubJoint6.publish(msg2)
            rate.sleep()
        # # raw_input("Hit Enter to Repeat")



if __name__=='__main__':
    print "Starting"
    main()
