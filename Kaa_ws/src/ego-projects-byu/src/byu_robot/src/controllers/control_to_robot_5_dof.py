#!/usr/bin/env python

import rospy
import numpy as np
import math
import time
from copy import deepcopy
import scipy.io as sio
from scipy.linalg import expm
from scipy import linalg
from scipy import signal
from std_msgs.msg import Float64
from threading import RLock, Timer
from collections import deque
from math import pi, cos, sin
import argparse

import falkor.msg as robot_msgs
from nasa_mpc.msg import send_pressures
from falkor.srv import set_robot_ctrl_mode
from falkor.srv import move_joint_prs
from falkor.srv import move_joint_valve

from threading import RLock, Timer
from collections import deque

class PressureCmd():

    def __init__(self,numOfJts):
        self.numOfJts = numOfJts
        self.totalJts = 6
        self.pPlusCmd_ = [0.]*self.totalJts
        self.pMinusCmd_ = [0.]*self.totalJts
        self.pPlusCmd = [0.]*self.totalJts
        self.pMinusCmd = [0.]*self.totalJts

        self.gotData = [False]*self.totalJts

        self.lock = RLock()
        self.rate = 500.0

        for i in xrange(self.totalJts):
            rospy.Subscriber('/pressure_cmd_' + str(i+1), send_pressures, self.pressureCmdCallback)

        # subscribers and service calls

        self.control = 2  # control mode # 0 = Stop, 1 = Valve, 2 = Pressure, 3 = Position


        self.sendPrsCmd = rospy.ServiceProxy('MoveJointPrs', move_joint_prs, persistent=True)
        self.sendValveCmd = rospy.ServiceProxy('MoveJointValve', move_joint_valve, persistent=True)

        self.setCtrl = rospy.ServiceProxy('/SetRobotCtrlMode', set_robot_ctrl_mode, persistent=True)

        try:
            worked = self.setCtrl(self.control)
        except rospy.ServiceException as e:
            print 'Error :', e

        prsCmdNode0 = [50.*1000.,50.*1000.,50.*1000.,50.*1000.]
        prsCmdNode1 = [30.*1000.,30.*1000.,30.*1000.,30.*1000.]
        prsCmdNode2 = [30.*1000.,30.*1000.,30.*1000.,30.*1000.]
        move_time = 4.0
        move_type = 2  # 0 = Stop, 1 = Step, 2 = Sinusoid, 3 = CubicSpline

        node = 0
        result = self.sendPrsCmd(node, prsCmdNode0, move_time, move_type)
        node = 1
        result = self.sendPrsCmd(node, prsCmdNode1, move_time, move_type)
        node = 2
        result = self.sendPrsCmd(node, prsCmdNode2, move_time, move_type)

        rospy.sleep(0.7)


    def pressureCmdCallback(self,msg):
        whichJoint = msg.jointNum
        self.pPlusCmd_[whichJoint-1] = msg.pPlus
        self.pMinusCmd_[whichJoint-1] = msg.pMinus
        self.gotData[whichJoint-1] = True

    def updateData(self):

        print 'got Data : ', self.gotData, '\n'

        self.lock.acquire()
        for i in xrange(self.totalJts):
            if self.gotData[i]:
                self.pPlusCmd[i] = self.pPlusCmd_[i]
                self.pMinusCmd[i] = self.pMinusCmd_[i]
            else:
                if i ==0:
                    self.pPlusCmd[i] = 50.
                    self.pMinusCmd[i] = 50.
                elif i == 1:
                    self.pPlusCmd[i] = 30.
                    self.pMinusCmd[i] = 30.
                elif i == 3:
                    self.pPlusCmd[i] = 30.
                    self.pMinusCmd[i] = 30.
                else:
                    self.pPlusCmd[i] = 30.0
                    self.pMinusCmd[i] = 30.0
        self.lock.release()

    def run(self):
        self.updateData()
        pMax = 150
        for i  in xrange(len(self.pPlusCmd)):
            if self.pPlusCmd[i] > pMax:
                self.pPlusCmd[i] = pMax
            if self.pMinusCmd[i] > pMax:
                self.pMinusCmd[i] = pMax


        prsCmdNode0 = [self.pPlusCmd[0]*1000.,self.pMinusCmd[0]*1000.,self.pPlusCmd[1]*1000.,self.pMinusCmd[1]*1000.]
        prsCmdNode1 = [self.pPlusCmd[2]*1000.,self.pMinusCmd[2]*1000.,self.pPlusCmd[3]*1000.,self.pMinusCmd[3]*1000.]
        # due to the hardware setup right now you must swap pplus and pminus
        prsCmdNode2 = [self.pMinusCmd[4]*1000.,self.pPlusCmd[4]*1000.,30*1000.,30.*1000.]        # prsCmdNode1 = [self.pPlusCmd[2]*1000.,self.pMinusCmd[2]*1000.,self.pPlusCmd[3]*1000.,30.0*1000.]
        move_time = 0.0
        move_type = 1  # 0 = Stop, 1 = Step, 2 = Sinusoid, 3 = CubicSpline

        node = 0
        result = self.sendPrsCmd(node, prsCmdNode0, move_time, move_type)
        node = 1
        result = self.sendPrsCmd(node, prsCmdNode1, move_time, move_type)
        node = 2
        result = self.sendPrsCmd(node, prsCmdNode2, move_time, move_type)

        print 'prsCmdNode0 : ',  prsCmdNode0, '\n'
        print 'prsCmdNode1 : ',  prsCmdNode1, '\n'
        print 'prsCmdNode2 : ',  prsCmdNode2, '\n'

    def shutDown(self):

        worked = 1
        count = 0
        while(worked == 1):
            try:
                worked = self.setCtrl(0)
                worked = 0
            except rospy.ServiceException as e:
                worked = 1
                count = count + 1
                if count > 5:
                    worked = 0
        worked = 1
        count = 0
        while(worked == 1):
            try:
                worked = self.setCtrl(1)
                worked = 0
            except rospy.ServiceException as e:
                worked = 1
                count = count + 1
                if count > 5:
                    worked = 0

        worked = 1
        count = 0
        while(worked == 1):
            try:
                worked = self.sendValveCmd(0, [-10, -10, -10, -10], 0.0, 1)
                worked = self.sendValveCmd(1, [-10, -10, -10, -10], 0.0, 1)
                worked = self.sendValveCmd(2, [-10, -10, -10, -10], 0.0, 1)
                worked = 0
            except rospy.ServiceException as e:
                worked = 1
                count = count + 1
                if count > 10:
                    worked = 0

        rospy.sleep(4)

        worked = 1
        count = 0
        while(worked == 1):
            try:
                worked = self.setCtrl(0)
                worked = 0
            except rospy.ServiceException as e:
                worked = 1
                count = count + 1
                if count > 5:
                    worked = 0




def main():

    rospy.init_node('pressureCmd', anonymous=True)
    numOfJts = 3;
    sendPressures = PressureCmd(numOfJts)
    rate = rospy.Rate(sendPressures.rate)

    rospy.on_shutdown(sendPressures.shutDown)

    while not rospy.is_shutdown():
        sendPressures.run()
        rate.sleep()






if __name__=='__main__':
    main()
