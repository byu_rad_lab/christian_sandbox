#ifndef SRC_PATH_PLANNER_INCLUDE_PATH_PLANNER_QPIK_H_
#define SRC_PATH_PLANNER_INCLUDE_PATH_PLANNER_QPIK_H_
#include <eigen3/Eigen/Dense>
#include "kinematics/types.h"
#include "kinematics/arm.h"
#include "cvxgen/cvxgen.h"

using kinematics::Arm;
using kinematics::Vec6;
using kinematics::Hom;
using kinematics::Twist;

class QPIK {
    public:
    QPIK();
    QPIK(Arm* arm);
    ~QPIK();

    void Solve(Hom g_goal);
    void Step();
    void SetAngs(Vec angs);
    void UpdateArm();
    Hom ClampGoal(Hom g_delta);
    void ClampAngsDelta();
    void NullSpaceProject(Mat J);
    void CalcError();

    //Joint Limit Setup Functions
    void FormatJointLimitMats();
    Mat A_;
    Mat b_;
    Mat c_;
    Mat Areal_;
    Mat breal_;
    Mat creal_;

    //Arm
    Arm* arm_;
    int n; //Number of Active Joints
    int n_all; //Total Num of Jts

    //Solver Copies and Data Members
    Vec angs_;
    Vec angs_delta_;
    Vec angs_delta_full_;
    double rho_;
    Mat Pth_;
    Vec6 weights_;
    Mat Pf_;
    double error_ratio_;
    double error_last_;
    double scale_;
    double rot_clamp_max_;
    double vec_clamp_max_;
    double error_term_tol_;
    double error_improv_tol_;
    double max_step_;
    double step_scale_;
    double zero_step_weight_;
    Hom g_goal_;
    double error_tot_;
    Twist v_delta_full_;

    //Solve Variables
    int max_iter_;
    double steps_taken_;
    std::string status_;

    //Solver
    CVXGen cvxgen_;

};
#endif
