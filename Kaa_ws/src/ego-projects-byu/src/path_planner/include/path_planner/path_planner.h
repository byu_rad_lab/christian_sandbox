#ifndef SRC_PATH_PLANNER_INCLUDE_PATH_PLANNER_PATH_PLANNER_H_
#define SRC_PATH_PLANNER_INCLUDE_PATH_PLANNER_PATH_PLANNER_H_
#include <vector>
#include <eigen3/Eigen/Dense>
#include "kinematics/types.h"
#include "kinematics/arm.h"
#include "cvxgen/cvxgen.h"
#include "path_planner/path_planner.h"
#include "path_planner/qpik.h"

using kinematics::Arm;
using kinematics::Hom;
using kinematics::Vec;
using kinematics::Mat;

class PathPlanner {
 public:
  PathPlanner();
  PathPlanner(Arm* arm);
  ~PathPlanner();

  // Arm
  Arm* arm_;

  // QPIK Object
  QPIK qpik_;

};
#endif
