#include "path_planner.h"
#include "qpik.h"
#include "kinematics/arm.h"
#include "kinematics/types.h"
#include "kinematics/sdls.h"
#include "kinematics/arm.h"
#include "kinematics/joint_rot.h"
#include "kinematics/joint_ccc.h"

#include <stdio.h>
#include <iostream>
#include <string>
#include <eigen3/Eigen/Dense>
#include <vector>
#include <math.h>
#include <cmath>

using kinematics::Arm;
using kinematics::Vec;
using kinematics::Vec3;
using kinematics::JointRot;
using kinematics::Link;
using kinematics::Rot;
using kinematics::Hom;
using std::endl;
using std::cout;

int main() {
  static Arm arm;

  Vec lengths = Vec(3);
  lengths.setOnes();
  double height = 0.00;

  double limit = M_PI / 2.0;

  // Build Architecture of Robot
  for (unsigned int i = 0; i < lengths.size(); i++) {
    // Rotational Joint
    JointRot joint(height);
    Link link;
    joint.limits()->setLimitsScalar(limit);
    link = Link(joint);
    link.setGTopEnd(lengths(i), 0, 0);

    double m = .453;
    double r = 0.07;
    double Ix = m * (3.0 * r * r + lengths[i]) / 12.0;
    double Iy = m * (3.0 * r * r + lengths[i]) / 12.0;
    double Iz = m * (r * r) / 2.0;

    link.setInertiaSelf(m, Vec3(0.0, 0.0, lengths[i] / 2.0), Vec3(Ix, Iy, Iz));
    arm.addLink(link);
  }

  // Setup Planner
  arm.setJointAngsZero();
  arm.calcFK();
  PathPlanner mypathplanner(&arm);
  int n = 10;
  double xt, yt;
  for (double i = 0; i <= n; i++) {
    xt = 1.0;
    yt = (-1.5 + i / n * 3);
    Vec3 r, t;
    r << 0, 0, 0;
    Rot rot = kinematics::AxToRot(r);
    t << xt, yt, 0;
    Hom g_goal;
    g_goal.setIdentity();
    g_goal.topLeftCorner(3, 3) = rot;
    g_goal.topRightCorner(3, 1) = t;
    cout << "GOAL" << endl;
    cout << g_goal << endl;
    mypathplanner.qpik_.Solve(g_goal);
    cout << "status" << endl << mypathplanner.qpik_.status_ << endl;
    cout <<mypathplanner.arm_->getJointAngs() << endl;
  }

  return 0;
}
