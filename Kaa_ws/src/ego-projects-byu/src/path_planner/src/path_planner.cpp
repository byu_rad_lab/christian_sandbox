#include <vector>
#include <eigen3/Eigen/Dense>
#include "kinematics/types.h"
#include "path_planner.h"
#include <iostream>

#include "kinematics/arm.h"
#include "kinematics/types.h"
#include "kinematics/sdls.h"
#include "kinematics/arm.h"
#include "kinematics/joint_rot.h"
#include "kinematics/joint_ccc.h"

// using kinematics::Arm;
using namespace kinematics;
using std::cout;
using std::endl;

// Uses a qpik object, and a can be combined with Dijkstra's to generate efficient paths
PathPlanner::PathPlanner() {}
PathPlanner::PathPlanner(Arm* arm) {
  arm_ = arm;
  qpik_ = QPIK(arm);
}
PathPlanner::~PathPlanner() {}



