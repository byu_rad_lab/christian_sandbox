#include <vector>
#include <eigen3/Eigen/Dense>
#include "kinematics/types.h"
#include "path_planner.h"
#include "qpik.h"
#include <iostream>
#include "cmath"

using kinematics::Arm;
using kinematics::Hom;
using kinematics::Twist;
using std::cout;
using std::cin;
using std::endl;

QPIK::QPIK() {}
QPIK::QPIK(Arm* arm) {
  // Arm
  arm_ = arm;
  n = arm_->getRealJointIdx().size();
  n_all = arm_->num_dof_;

  FormatJointLimitMats();

  angs_ = arm_->getJointAngsReal();

  // Settings
  rho_ = 1.0e-1;
  Pth_.setIdentity(n, n);
  Pth_ *= rho_;
  weights_ << 1.0, 1.0, 1.0, .05, .05, .05;
  Pf_ = weights_.asDiagonal();
  error_ratio_ = 1.0;
  scale_ = 1.0;
  rot_clamp_max_ = scale_ * 30.0 * 180 / M_PI;
  vec_clamp_max_ = scale_ * 1.5;
  max_iter_ = 100;
  error_term_tol_ = 1e-4;
  error_improv_tol_ = 1e-5;
  error_last_ = std::numeric_limits<double>::infinity();
  max_step_ = 2;
  step_scale_ = .30;
  zero_step_weight_ = .05;
  g_goal_.setIdentity();

  cvxgen_ = CVXGen();
}

QPIK::~QPIK() {}

// Functionality Here is copied from qpik.py
void QPIK::FormatJointLimitMats() {
  // Format for Entire Arm First, Then Extract "real" entries
  A_.resize(2 * n_all, n_all);
  b_.resize(2 * n_all, 1);
  c_.resize(n_all, 1);
  A_.setZero();
  b_.setZero();
  c_.setZero();

  Vec jt_limits;
  for (unsigned int i = 0; i < arm_->links_.size(); i++) {
    int ndofp = arm_->links_[i].num_dof_prox_;
    int ndof = arm_->links_[i].num_dof_total_;
    int dof_jt = ndof - ndofp;
    A_.block(2 * ndofp, ndofp, 2 * dof_jt, dof_jt) =
        arm_->links_[i].joint()->limits()->a();
    b_.block(2 * ndofp, 0, 2 * dof_jt, 1) = arm_->links_[i].joint()->limits()->b();
    c_.block(ndofp, 0, dof_jt, 1) = arm_->links_[i].joint()->limits()->c();
  }

  Mat Atemp_;
  Atemp_.resize(2 * n_all, n);
  // Extract Only Columns/Entries from mats that correspond to real jts
  for (int i = 0; i < n; i++) {
    Atemp_.col(i) = A_.col(arm_->getRealJointIdx()[i]);
  }
  // Extract rows corresponding with real joints (nonzero rows)
  Areal_.resize(2 * n, n);
  breal_.resize(2 * n, 1);
  creal_.resize(n, 1);
  int row_num = 0;
  for (int i = 0; i < Atemp_.rows(); i++) {
    if (!Atemp_.row(i).isZero()) {
      Areal_.row(row_num) = Atemp_.row(i);
      breal_(row_num) = b_(i);
      row_num += 1;
    }
  }
  for (int i = 0; i < creal_.size(); i++) {
    creal_(i) = c_(arm_->getRealJointIdx()[i]);
  }
}

void QPIK::Solve(Hom g_goal) {
  g_goal_ = g_goal;
  angs_ = arm_->getJointAngsReal();
  UpdateArm();
  error_last_ = std::numeric_limits<double>::infinity();

  for (int i = 0; i < max_iter_; i++) {
    Step();
    // cout << "step" << endl;
    if (error_tot_ < error_term_tol_) {
      steps_taken_ = i + 1;
      status_ = "Solved";
      // cout << "QPIK Solved in " << i << " iterations" << endl;
      return;
    }
    if (error_tot_ - error_last_ > -error_improv_tol_) {
      steps_taken_ = i + 1;
      status_ = "Stuck";
      return;
    }
    error_last_ = error_tot_;
  }
  // cout << "Out of for loop" << endl;
  status_ = "Iterations";
  steps_taken_ += 1;
}

void QPIK::Step() {
  Mat J = arm_->getJacBodyReal();

  // Find Delta Position, Clamp, and Convert to Twist
  Hom g_start = arm_->getGWorldTool();
  Hom g_delta_full = kinematics::HomInv(g_start) * g_goal_;
  Hom g_delta = ClampGoal(g_delta_full);
  Twist v_delta = kinematics::VeeHom(g_delta);
  v_delta_full_ = kinematics::VeeHom(g_delta_full);

  // Setup QP
  Mat JPfJ = J.transpose() * Pf_ * J;
  cvxgen_.P_ = 2 * JPfJ + Pth_;
  cvxgen_.B_ = -2 * J.transpose() * Pf_ * v_delta;
  cvxgen_.G_ = Areal_;
  cvxgen_.h_ = breal_ + Areal_ * (creal_ - angs_);
  cvxgen_.load_data();
  // cvxgen_.PrintParams();
  cvxgen_.Solve();

  angs_delta_full_ = cvxgen_.x_;
  ClampAngsDelta();
  NullSpaceProject(J);

  angs_ += angs_delta_;
  UpdateArm();
  CalcError();
}

void QPIK::SetAngs(Vec angs) {
  angs_ = angs;
  UpdateArm();
}

void QPIK::UpdateArm() {
  arm_->setJointAngsReal(angs_);
  arm_->calcJacReal();
}

Hom QPIK::ClampGoal(Hom g_delta) {
  double rot_mag = kinematics::RotToAx(g_delta.topLeftCorner(3, 3)).norm();
  double vec_mag = g_delta.topRightCorner(3, 1).norm();
  double rot_ratio = rot_mag / rot_clamp_max_;
  double vec_ratio = vec_mag / vec_clamp_max_;
  double max_ratio = std::max(rot_ratio, vec_ratio);
  if (max_ratio > 1.0) {
    return kinematics::HomScale(g_delta, 1.0 / max_ratio);
  } else {
    return g_delta;
  }
}

void QPIK::ClampAngsDelta() {
  Vec angs_delta_scale = angs_delta_full_ * step_scale_;
  double mag = angs_delta_scale.norm();
  if (mag > max_step_) {
    angs_delta_ = angs_delta_scale / mag * max_step_;
  } else {
    angs_delta_ = angs_delta_scale;
  }
}

void QPIK::NullSpaceProject(Mat J) {
  double rho = 1e-9;
  Mat J_inv = J.transpose() *
              (J * J.transpose() + rho * Eigen::MatrixXd::Identity(6, 6)).inverse();

  Mat null_projector = Eigen::MatrixXd::Identity(n, n) - J_inv * J;
  angs_delta_ += zero_step_weight_ * null_projector * (creal_ - angs_);
}

void QPIK::CalcError() { error_tot_ = v_delta_full_.transpose() * Pf_ * v_delta_full_; }
