#!/usr/bin/python        

import rospy
import numpy as np
import scipy.io as sio

import nasa_grub.srv as srvc
import nasa_grub.msg as msgs

from collections import deque

global robot_state
robot_state = None

def subscriber_callback(msg):
    global robot_state
    robot_state = msg
   # print robot_state
    




def main():
    
    global robot_state

    rospy.init_node('grub_data_subscriber', anonymous=True)
    rospy.Subscriber('/robot_state', msgs.robot_state_msg, subscriber_callback) 
    rate = rospy.Rate(50)

    p0 = deque(maxlen = 10000)
    p1 = deque(maxlen = 10000)

    pd0 = deque(maxlen = 10000)
    pd1 = deque(maxlen = 10000)
    
    p0des = deque(maxlen = 10000)
    p1des = deque(maxlen = 10000)


    while (not rospy.is_shutdown()):
        
        try:
            reference = robot_state
        
            p0.append(robot_state.status.ana[0])
            p1.append(robot_state.status.ana[1])
            pd0.append(robot_state.status.ana_dot[0])
            pd1.append(robot_state.status.ana_dot[1])
            p0des.append(robot_state.command.reference[0])
            p1des.append(robot_state.command.reference[1])
            print "Got State"
        #print robot_state
        except: 
            print "error"
            

        rate.sleep()
        
    dataDict = {"P0":p0, "P1":p1, "Pd0": pd0, "Pd1": pd1, "P0des": p0des, "P1des": p1des}
    sio.savemat("newgrub_data.mat", dataDict, appendmat= False);


if __name__=='__main__':
    main()
