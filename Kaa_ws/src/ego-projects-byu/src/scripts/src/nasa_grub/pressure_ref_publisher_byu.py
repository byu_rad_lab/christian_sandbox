#!/usr/bin/python

import rospy

import numpy as np

import nasa_grub.srv as srvc
import nasa_grub.msg as msgs

class PressureRefPublisher(object):
    def __init__(self, pressure_mean, pressure_amp, pressure_freq, publish_freq=100.0):
        self.pressure_mean = pressure_mean
        self.pressure_amp = pressure_amp
        self.pressure_freq = pressure_freq
        self.publish_freq = publish_freq
        
        rospy.init_node('PressureRefPublisher', anonymous=True)
        
        rospy.Subscriber('robot_state', msgs.robot_state_msg, self.subscriber_callback)
        self.publisher = rospy.Publisher('SetPressureRef', msgs.pressure_reference_msg, queue_size=1000)
        
        self.rate = rospy.Rate(self.publish_freq)
        
        self.robot_state = None
        self.t0 = None
        
        rospy.on_shutdown(self.shutdown)
        
    def subscriber_callback(self, msg):
        self.robot_state = msg
    
    def run(self):
        rospy.loginfo('Getting initial pressures')
        while not self.robot_state:
            rospy.sleep(rospy.Duration(0.25))
            rospy.loginfo('Waiting for robot_state message...')
            
        rospy.loginfo('Got robot_state message')
        
        rospy.loginfo('Moving to initial pressure')
        self.move_to_initial_pressures()
        
        rospy.loginfo('Entering main publisher loop at %5.1fHz' % (self.publish_freq))
        rospy.loginfo('Press Ctrl+C to exit')
        self.t0 = rospy.Time.now().to_sec()
        while (not rospy.is_shutdown()):
            self.iterate_pressure_ref()
            self.rate.sleep()
        
    def move_to_initial_pressures(self):
        try:
            service = rospy.ServiceProxy('SetRobotCtrlMode', srvc.set_robot_ctrl_mode_srv)
            service(msgs.robot_state_msg.kRobotControlModeOff)
        except rospy.ServiceException as err:
            rospy.logerr('Service call failed: %s' % err)    
            return
        
        try:
            service = rospy.ServiceProxy('SetRobotCtrlMode', srvc.set_robot_ctrl_mode_srv)
            service(msgs.robot_state_msg.kRobotControlModePressure)
        except rospy.ServiceException as err:
            rospy.logerr('Service call failed: %s' % err)    
            return
        
        try:
            service = rospy.ServiceProxy('MovePressure', srvc.move_joint_srv)
            service(target_value=[self.pressure_mean, self.pressure_mean], target_value_dot=[0.0, 0.0], move_time=3.0, move_type=msgs.robot_state_msg.kMoveRefTypeSin)
        except rospy.ServiceException as err:
            rospy.logerr('Service call failed: %s' % err)    
            return
        
        rospy.sleep(3.0)
        
        try:
            service = rospy.ServiceProxy('SetRobotCtrlMode', srvc.set_robot_ctrl_mode_srv)
            service(msgs.robot_state_msg.kRobotControlModePressureSubscriber)
        except rospy.ServiceException as err:
            rospy.logerr('Service call failed: %s' % err)    
            return
        
    def iterate_pressure_ref(self):
        p1 = self.pressure_mean + self.pressure_amp * np.sin(self.pressure_freq * 2.0 * np.pi * (rospy.Time.now().to_sec() - self.t0))
        p2 = self.pressure_mean - self.pressure_amp * np.sin(self.pressure_freq * 2.0 * np.pi * (rospy.Time.now().to_sec() - self.t0))
        v1 = self.pressure_amp * self.pressure_freq * 2.0 * np.pi * np.cos(2.0 * np.pi * (rospy.Time.now().to_sec() - self.t0))
        v2 = -self.pressure_amp * self.pressure_freq * 2.0 * np.pi * np.cos(2.0 * np.pi * (rospy.Time.now().to_sec() - self.t0))
        msg = msgs.pressure_reference_msg()
        msg.reference = [p1, p2]
        #msg.reference_dot = [v1, v2]
        msg.reference_dot = [0.0, 0.0]
        self.publisher.publish(msg)
    
    def shutdown(self):
        rospy.loginfo('Turning off')
        try:
            service = rospy.ServiceProxy('SetRobotCtrlMode', srvc.set_robot_ctrl_mode_srv)
            service(msgs.robot_state_msg.kRobotControlModeOff)
        except rospy.ServiceException as err:
            rospy.logerr('Service call failed: %s' % err)    
            return
            
        rospy.loginfo('Exiting cleanly...')

def main(pressure_mean, pressure_amp, pressure_freq, publish_freq):
    p = PressureRefPublisher(pressure_mean, pressure_amp, pressure_freq, publish_freq)
    p.run()       
      
if __name__ == '__main__':
    main(50000,10000,0.2,100)
