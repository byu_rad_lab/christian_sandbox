#!/usr/bin/python

import numpy as np
from matplotlib import pyplot as plt
import ego_core.log_tools.bag_tools as bag_tools
import os

files = os.listdir('./')
filenames = {}
for f in files:
    if f.endswith('.bag'):
        if f.startswith('00psi'):
            pass
        else:
            s = f.split('psi')
            if (len(s) > 1):
                p = float(s[0])            
                print p, f
                filenames[f] = p * 6894.76
            
num_channels = 2

plt.close('all')
for channel in range(num_channels):
    sensor_raw = np.array([])
    sensor_cal = np.array([])
    pressure_real = np.array([])
    node = 0
    for filename, pressure in filenames.iteritems():   
        msg_data, msg_timestamps = bag_tools.bag_to_dict(filename, ['robot_state'])
        num_messages = len(msg_data['robot_state'])
        
        pressure_real = np.append(pressure_real, pressure)
        tmp = np.zeros((num_messages,))        
        for i in range(num_messages):
            tmp[i] = msg_data['robot_state'][i].status.ana[channel]

        tmp = np.mean(tmp)
#        print tmp
        sensor_raw = np.append(sensor_raw, tmp)

    x = sensor_raw    
    p = np.polyfit(x, pressure_real, 1)
    y1 = np.polyval(p, x)

    plt.figure()
    plt.plot(x, pressure_real, 'x')
    N = len(x)
    y = np.zeros((N,))
    for i in range(N):
        y[i] = (x[i]) * p[0] + p[1]
    error = np.sqrt(np.mean((y - pressure_real) ** 2))
    plt.plot(x, y)
    plt.plot(x, y1)
    plt.grid()
    plt.xlabel('ADC')    
    plt.ylabel('Pressure (Pa)')
    plt.title('Node %d, CHannel %d\nRMS Error = %fPa' % (0, channel , error))
    
    print 'gen.add("bias_%d",       double_t,   0,    "Pressure sensor bias",   %f,	-100000.0,  100000.0)' % (channel, p[1])
    print 'gen.add("gain_%d",       double_t,   0,    "Pressure sensor gain",   %f,   -100000.0,  100000.0)' % (channel, p[0])
