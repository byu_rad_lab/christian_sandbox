#ifndef SRC_CVXGEN_INCLUDE_CVXGEN_CVXGEN_H_
#define SRC_CVXGEN_INCLUDE_CVXGEN_CVXGEN_H_
#include <vector>
#include <eigen3/Eigen/Dense>
#include "kinematics/types.h"

using kinematics::Vec;
using kinematics::Mat;

extern "C" void set_defaults();
extern "C" void setup_indexing();
extern "C" long solve();
class CVXGen {
 public:
  // minimize .5x^TPx+B^Tx
  // s.t. Gx <= h
  // Ax = b
  CVXGen();
  CVXGen(Mat A, Mat B, Mat G, Mat h);
  void load_data();
  void Solve();
  void PrintParams();
  Mat P_;
  Mat B_;
  Mat G_;
  Mat h_;

  int n; // Max Solver Joint Number

  Vec x_;
};
#endif
