#include <vector>
#include <eigen3/Eigen/Dense>
#include "cvxgen/cvxgen.h"
#include "kinematics/types.h"
#include "cvxgen/solver.h"
// #include "solver.h"
#include <math.h>
// #include "assert.h"

Vars vars;
Params params;
Workspace work;
Settings settings;

using kinematics::Mat;
using std::cout;
using std::endl;

CVXGen::CVXGen() {
  n = sqrt(sizeof(params.P) / sizeof(double));
  set_defaults();
  settings.verbose = 0;
  setup_indexing();
}

CVXGen::CVXGen(Mat P, Mat B, Mat G, Mat h) {

  P_ = P;
  B_ = B;
  G_ = G;
  h_ = h;

  n = sqrt(sizeof(params.P) / sizeof(double));
  load_data();
  set_defaults();
  settings.verbose = 0;
  setup_indexing();
}

void CVXGen::PrintParams() {
  cout << "INTERNAL CVXGen VARIABLES" << endl;
  cout << "P_" << endl;
  cout << P_ << endl;
  cout << "B_" << endl;
  cout << B_ << endl;
  cout << "G_" << endl;
  cout << G_ << endl;
  cout << "h_" << endl;
  cout << h_ << endl
       << endl;

  cout << "Solver VARIABLES, First Numbers Represent 1st Column" << endl;

  cout << "params.P" << endl;
  for (int i = 0; i < n * n; i++) {
    cout << params.P[i] << " ";
  }
  cout << endl
       << "params.B" << endl;
  for (int i = 0; i < n; i++) {
    cout << params.B[i] << " ";
  }
  cout << endl
       << "params.G" << endl;
  for (int i = 0; i < 2 * n * n; i++) {
    cout << params.G[i] << " ";
  }
  cout << endl
       << "params.h" << endl;
  for (int i = 0; i < 2 * n; i++) {
    cout << params.h[i] << " ";
  }
}

void CVXGen::load_data() {
  // In this function, load all problem instance data.
  if (n * n < P_.rows() * P_.cols()) {
    cout << "ERROR: SOLVER CAN'T HANDLE MORE THAN " << n << " DOF ROBOT" << endl;
    assert(false);
  }

  // go down columns for P, and just rows for B
  for (int i = 0; i < P_.rows(); i++) {
    for (int j = 0; j < P_.cols(); j++) {
      // P
      params.P[i + n * j] = P_(i, j);
    }
    params.B[i] = B_(i);
  }

  for (int i = 0; i < G_.rows(); i++) {
    for (int j = 0; j < G_.cols(); j++) {
      params.G[i + 2 * n * j] = G_(i, j);
    }
    params.h[i] = h_(i);
  }
}

void CVXGen::Solve() {
  solve();
  x_.resize(P_.rows());
  for (int i = 0; i < P_.rows(); i++) {
    x_(i) = vars.x[i];
  }
}
