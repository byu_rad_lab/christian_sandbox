#include "cvxgen/cvxgen.h"
#include <iostream>
#include <eigen3/Eigen/Dense>
#include "kinematics/types.h"
#include "math.h"
#include "cvxgen/solver.h"


using kinematics::Mat;

int main(){
    int n = 2; // Num Joints
    Mat P;
    Mat B;
    Mat G;
    Mat h;

    P.resize(n,n);
    B.resize(n,1);
    G.resize(2*n,n);
    h.resize(2*n,1);


    P << 1,0,
        0,1;

    B << 1,1;

    G << -1,0,
        1,0,
        0,-1,
        0,1;

    h << M_PI/2.0,
       M_PI/2.0,
      M_PI/2.0,
     M_PI/2.0; 

    // std::cout << P << "\n" <<  B << "\n" <<  G << "\n" <<  h << std::endl;

    CVXGen mycvx(P,B,G,h);
    mycvx.PrintParams();
    mycvx.Solve();
    // std::cout << mycvx.P_;


}
