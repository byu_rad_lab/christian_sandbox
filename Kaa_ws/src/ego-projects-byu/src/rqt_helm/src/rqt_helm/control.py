# #!/usr/bin/env python
 
# Otherlab, Inc ("COMPANY") CONFIDENTIAL
# Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains the property of COMPANY.
# The intellectual and technical concepts contained herein are proprietary to COMPANY
# and may be covered by U.S. and Foreign Patents, patents in process, and are protected
# by trade secret or copyright law. Dissemination of this information or reproduction
# of this material is strictly forbidden unless prior written permission is obtained
# from COMPANY. Access to the source code contained herein is hereby forbidden to
# anyone except current COMPANY employees, managers or contractors who have executed
# Confidentiality and Non-disclosure agreements explicitly covering such access.
#
# The copyright notice above does not evidence any actual or intended publication or
# disclosure of this source code, which includes information that is confidential
# and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
# DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
# SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
# IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
# OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
# REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
# ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
  
import sys
import time
import os
 
import python_qt_binding.QtGui as QtGui
import python_qt_binding.QtCore as QtCore

from qt_gui.plugin import Plugin 
import rospy

import nasa_grub.srv as srvc
import nasa_grub.msg as msgs
from PyKDL import Joint

import common
import monitor

def generateControlModeToStringDict():
    map = {}
    for item in dir(msgs.robot_state_msg):
        if item.startswith('kRobotControlMode'):  # and not item.endswith('Init'):
            refType = getattr(msgs.robot_state_msg, item)
            map[refType] = '%d:%s' % (refType, item)
    
    mapInv = {v: k for k, v in map.items()}
    
    return map, mapInv

def generateRefTypeToStringMap():
    map = {}
    for item in dir(msgs.robot_state_msg):
        if item.startswith('kMoveRefType') and not item.endswith('Test'):
            refType = getattr(msgs.robot_state_msg, item)
            map[refType] = '%d:%s' % (refType, item)
    
    mapInv = {v: k for k, v in map.items()}
    
    return map, mapInv

def generateMoveTypeArgs():
    for item in dir(srvc):
        if item.startswith('move_joint_') and item.endswith('Request'):
            tmp = getattr(srvc, item)
            print tmp.__slots__

def makeModeSelectComboBox(caller):
    modeStringToComboBoxIndex = {}
    comboBox = QtGui.QComboBox(caller)
    ind = 0
    for val in HelmWidget.controlModeToStringDict.itervalues():
        comboBox.addItem(val)
        modeStringToComboBoxIndex[val] = ind
        ind += 1
    comboBox.setCurrentIndex(HelmWidget.stringToControlModeDict['0:kRobotControlModeOff'])
    return comboBox, modeStringToComboBoxIndex

def makeRefTypeComboBox(caller):
    modeStringToComboBoxIndex = {}
    comboBox = QtGui.QComboBox(caller)
    ind = 0
    for val in HelmWidget.refTypeToStringDict.itervalues():
        comboBox.addItem(val)
        modeStringToComboBoxIndex[val] = ind
        ind += 1
    comboBox.setCurrentIndex(HelmWidget.stringToRefTypeDict['2:kMoveRefTypeSin'])  # self.controlModeGroupBox.modeStringToComboBoxIndex[modeStr])
    
    return comboBox, modeStringToComboBoxIndex

class GridGroupBox(QtGui.QGroupBox):
    def __init__(self, title=''):
        super(GridGroupBox, self).__init__(title)
        self.grid = QtGui.QGridLayout()
        self.setStyleSheet('QGroupBox { border:1px solid rgb(128, 128, 128); }')
        self.setLayout(self.grid)
      
class MoveGroupBox(GridGroupBox):
    moveDataNames = ['target_value', 'move_time']
    def __init__(self, parent, title=''):
        super(MoveGroupBox, self).__init__(title)
        row = 0
        col = 0
        self.parent = parent
        
        self.controlModeComboBox, self.modeStringToComboBoxIndex = makeModeSelectComboBox(self)
        self.refTypeComboBox, _ = makeRefTypeComboBox(self)
        
        self.grid.addWidget(QtGui.QLabel('Control Target'), row, col)
        self.grid.addWidget(self.controlModeComboBox, row + 1, col)
        col += 1
        
        self.grid.addWidget(QtGui.QLabel('move_type'), row, col)
        self.grid.addWidget(self.refTypeComboBox, row + 1, col)
        col += 1

        self.lineEdits = {}
        for name in self.moveDataNames:
            self.lineEdits[name] = QtGui.QLineEdit()
            self.grid.addWidget(QtGui.QLabel(name), row, col)
            self.grid.addWidget(self.lineEdits[name], row + 1, col)
            col += 1
        row += 2
        
        button0 = QtGui.QPushButton('Execute Move')
        button0.clicked.connect(self.executeMoveButtonCallback)
        self.grid.addWidget(button0, row, 0, 1, (len(self.moveDataNames) + 2))
        row += 1
        
        self.serviceResultLabel = QtGui.QLabel('')
        self.grid.addWidget(self.serviceResultLabel, row, 0, 1, len(self.moveDataNames) + 2)
        
        self.lineEdits['target_value'].setText('0.0,0.0')
        self.lineEdits['move_time'].setText('3.0')
            
    def executeMoveButtonCallback(self):
        refTypeStr = self.refTypeComboBox.currentText()
        refTypeInt = HelmWidget.stringToRefTypeDict[refTypeStr]
            
        controlTargetStr = self.controlModeComboBox.currentText()
        controlTargetInt = HelmWidget.stringToControlModeDict[controlTargetStr]
        
        if (refTypeInt == msgs.robot_state_msg.kMoveRefTypeStop):
            try:
                rospy.wait_for_service('MoveValve', timeout=0.5)
            except rospy.ROSException, e:
                self.serviceResultLabel.setText('Failed to get service MoveValve')
                return
            try:
                service = rospy.ServiceProxy('MoveValve', srvc.move_joint_srv)
                service(target_value=[0.0, 0.0], move_time=0.0, move_type=refTypeInt)
                self.serviceResultLabel.setText('Successfully requested move')
                return
            except rospy.ServiceException, e:
                self.serviceResultLabel.setText('Service call failed: %s' % e)            
            return
          
        if (controlTargetInt == msgs.robot_state_msg.kRobotControlModeOff):
            self.serviceResultLabel.setText('Nothing to do for kRobotControlModeOff')            
            return
        else:
            try:
                target_value = self.parseValueListString(self.lineEdits['target_value'].text(), float)
            except ValueError as err:
                self.serviceResultLabel.setText('Invalid \"target_value\" argument %s' % err)
                return
            
            try:
                move_time = self.parseValueScalarString(self.lineEdits['move_time'].text(), float)
            except ValueError as err:
                self.serviceResultLabel.setText('Invalid \"move_time\" argument %s' % err)
                return
                        
        if (controlTargetInt == msgs.robot_state_msg.kRobotControlModeValve):
            exp = 2
            got = len(target_value)
            if (exp != got):
                self.serviceResultLabel.setText('Wrong number of arguments for \"target_value\". Should be %d, got %d' % (exp, got))
                return
            try:
                rospy.wait_for_service('MoveValve', timeout=0.5)
            except rospy.ROSException as err:
                self.serviceResultLabel.setText('Failed to get service MoveValve %s' % err)
                return
            try:
                service = rospy.ServiceProxy('MoveValve', srvc.move_joint_srv)
                service(target_value=target_value, move_time=move_time, move_type=refTypeInt)
                self.serviceResultLabel.setText('Successfully requested move')
                return
            except rospy.ServiceException, e:
                self.serviceResultLabel.setText('Service call failed: %s' % e)    
            return
        
        elif (controlTargetInt == msgs.robot_state_msg.kRobotControlModePressure):
            exp = 2
            got = len(target_value)
            if (exp != got):
                self.serviceResultLabel.setText('Wrong number of arguments for \"target_value\". Should be %d, got %d' % (exp, got))
                return
            
            try:
                rospy.wait_for_service('MovePressure', timeout=0.5)
            except rospy.ROSException, e:
                self.serviceResultLabel.setText('Failed to get service MovePressure')
                return
            try:
                service = rospy.ServiceProxy('MovePressure', srvc.move_joint_srv)
                service(target_value=target_value, move_time=move_time, move_type=refTypeInt)
                self.serviceResultLabel.setText('Successfully requested move')
                return
            except rospy.ServiceException, e:
                self.serviceResultLabel.setText('Service call failed: %s' % e)    
            return
        
        else:
            self.serviceResultLabel.setText('Control target %d not recognized' % (controlTargetInt))
    
    def cleanup(self):
        pass
        
    @staticmethod
    def parseValueListString(msg, type):
        tmp = msg.split(',')
        res = []
        for p in tmp:
            res.append(type(p))
        return res
    
    @staticmethod
    def parseValueScalarString(msg, type):
        res = type(msg)
        return res

class SetModeGroupBox(GridGroupBox):
    def __init__(self, title=''):
        super(SetModeGroupBox, self).__init__(title)

        self.grid.addWidget(QtGui.QLabel('Requested Mode'), 0, 0, 1, 1)
        
        self.comboBox, self.modeStringToComboBoxIndex = makeModeSelectComboBox(self)
        self.comboBox.activated[str].connect(self.modeSelectComboBoxCallback)
    
        self.grid.addWidget(self.comboBox, 1, 0, 1, 1)
            
        self.serviceResultLabel = QtGui.QLabel('')
        self.grid.addWidget(self.serviceResultLabel, 2, 0, 1, 1)
            
    def modeSelectComboBoxCallback(self, msg):
        mode = HelmWidget.stringToControlModeDict[msg]
        print msg, mode
         
        try:
            rospy.wait_for_service('SetRobotCtrlMode', timeout=0.5)
        except rospy.ROSException, e:
            self.serviceResultLabel.setText('Failed to get service SetRobotCtrlMode')
            return
         
        try:
            setMode = rospy.ServiceProxy('SetRobotCtrlMode', srvc.set_robot_ctrl_mode_srv)
            setMode(mode)
            self.serviceResultLabel.setText('Successfully requested mode %d' % mode)
        except rospy.ServiceException, e:
            self.serviceResultLabel.setText('Service call failed: %s' % e)    

    def cleanup(self):
        pass
            
class HelmWidget(QtGui.QWidget):
    controlModeToStringDict, stringToControlModeDict = generateControlModeToStringDict()
    refTypeToStringDict, stringToRefTypeDict = generateRefTypeToStringMap()
    numMoveGroupBoxes = 4
    
    def __init__(self):
        super(HelmWidget, self).__init__()
                
        row = 0
        col = 0
        
        self.robotState = None
        self.robotStateSubscriber = rospy.Subscriber('robot_state', msgs.robot_state_msg, self.rosRobotStateCallback)

        # Group Boxes
        self.controlModeGroupBox = SetModeGroupBox()  # 'Set Control Mode')  
        
        # Overall Grid Layout
        self.mainGridLayout = QtGui.QGridLayout()
        self.mainGridLayout.addWidget(self.controlModeGroupBox, row, col, 1, 1)
        row += 1
                
        self.moveJointGroupBoxes = []
        for i in range(self.numMoveGroupBoxes):
            self.moveJointGroupBoxes.append(MoveGroupBox(self))  # 'Move Joint'))  
        
            self.mainGridLayout.addWidget(self.moveJointGroupBoxes[i], row, 0, 1, 1)
            row += 1
        
        self.setLayout(self.mainGridLayout)
        
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.periodicTask)
        self.timer.start(100)
    
    def periodicTask(self):
        if self.robotState:
            modeInt = self.robotState.robot_control_mode
            modeStr = HelmWidget.controlModeToStringDict[modeInt]
            comboBoxIndex = self.controlModeGroupBox.modeStringToComboBoxIndex[modeStr]
            self.controlModeGroupBox.comboBox.setCurrentIndex(comboBoxIndex)
        
    def cleanup(self):
        self.robotStateSubscriber.unregister()
        
        self.controlModeGroupBox.cleanup()
        
        for i in range(self.numMoveGroupBoxes):
            self.moveJointGroupBoxes[i].cleanup()
            
    
    def rosRobotStateCallback(self, msg):
        self.robotState = msg

