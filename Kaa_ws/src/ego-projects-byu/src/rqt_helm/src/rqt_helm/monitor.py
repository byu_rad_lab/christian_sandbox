#!/usr/bin/env python
 
# Otherlab, Inc ("COMPANY") CONFIDENTIAL
# Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains the property of COMPANY.
# The intellectual and technical concepts contained herein are proprietary to COMPANY
# and may be covered by U.S. and Foreign Patents, patents in process, and are protected
# by trade secret or copyright law. Dissemination of this information or reproduction
# of this material is strictly forbidden unless prior written permission is obtained
# from COMPANY. Access to the source code contained herein is hereby forbidden to
# anyone except current COMPANY employees, managers or contractors who have executed
# Confidentiality and Non-disclosure agreements explicitly covering such access.
#
# The copyright notice above does not evidence any actual or intended publication or
# disclosure of this source code, which includes information that is confidential
# and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
# DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
# SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
# IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
# OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
# REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
# ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
  
import sys
import time
import os
 
import python_qt_binding.QtGui as QtGui
import python_qt_binding.QtCore as QtCore

from qt_gui.plugin import Plugin 
import rospy

import nasa_grub.srv as stmp
import nasa_grub.msg as msgs

import common 
          
class BuildInfoGroupBox(common.GridGroupBox):
    def __init__(self, title=''):
        super(BuildInfoGroupBox, self).__init__(title)
        row = 0
        col = 0
        self.labels = [QtGui.QLabel(''), QtGui.QLabel(''), QtGui.QLabel('')]
        self.grid.addWidget(QtGui.QLabel("<b>Nodes Configured<b>"), row, col)
        self.grid.addWidget(self.labels[row], row, col + 1)
        row += 1
        self.grid.addWidget(QtGui.QLabel("<b>Nodes Responding<b>"), row, col)
        self.grid.addWidget(self.labels[row], row, col + 1)
        row += 1
        self.grid.addWidget(QtGui.QLabel("<b>Build Info<b>"), row, col)
        self.grid.addWidget(self.labels[row], row, col + 1)
        row += 1
        
    def update(self, robot_state):
        if robot_state:
            self.labels[0].setText('%d' % robot_state.num_nodes_configured)
            self.labels[1].setText('%d' % robot_state.num_nodes_responding)                        
                                
    def updateGrid(self, robot_state):
        if (robot_state):                 
            for joint in range(robot_state.num_joints):
                for axis in range(PressureSlewLimitGroupBox.numChannels):
                    limiting = (robot_state.pressure_control[joint].ref_pos[axis] != robot_state.pressure_control[joint].ref_pos_safe[axis])
                    key = '%d_%d' % (joint, axis)
                    self.labels[key].setText(str(limiting))
                    if limiting:
#                         self.labels[key].setText('%f!=%f'%(robot_state.pressure_control[joint].ref_pos[axis], robot_state.pressure_control[joint].ref_pos_safe[axis]))
                        self.labels[key].setStyleSheet("QLabel { color : red; font: bold; }")
                    else:
#                         self.labels[key].setText('%f==%f'%(robot_state.pressure_control[joint].ref_pos[axis], robot_state.pressure_control[joint].ref_pos_safe[axis]))
                        self.labels[key].setStyleSheet("QLabel { color : black; }")
                   
        
class FaultInfoGroupBox(common.GridGroupBox):
    faultIndexToString = {
    # Bit      # Name 
    
        # Medulla Faults                 
        0:    "kFaultHandshake",
        1:    "kFaultCom",
        2:    "kFaultVoltage",
        3:    "kFaultPressureHigh",
        4:    "kFaultModeMismatch",
        #5:    "kFault5",
        #6:    "kFault6",
        #7:    "kFault7",
        
        # Medulla Flags
        8:    "kFlagCalibrated",
        #9:    "kFlag9",
        #10:   "kFlag10",
        #11:   "kFlag11",
        
        # Poppy 0 Faults
        12:    "kFaultVoltage_bp_0",
        13:    "kFaultCurrentFast_bp_0",
        14:    "kFaultCurrentSlow_bp_0",
        15:    "kFaultTemperature_bp_0",
        16:    "kFaultOther_bp_0",
    
        # Poppy 1 Faults
        17:    "kFaultVoltage_bp_1",
        18:    "kFaultCurrentFast_bp_1",
        19:    "kFaultCurrentSlow_bp_1",
        20:    "kFaultTemperature_bp_1",
        21:    "kFaultOther_bp_1",
      
#         # Poppy 2 Faults
        22:    "kFaultVoltage_bp_2",
        23:    "kFaultCurrentFast_bp_2",
        24:    "kFaultCurrentSlow_bp_2",
        25:    "kFaultTemperature_bp_2",
        26:    "kFaultOther_bp_2",
        
        # Poppy 3 Faults
        27:    "kFaultVoltage_bp_3",
        28:    "kFaultCurrentFast_bp_3",
        29:    "kFaultCurrentSlow_bp_3",
        30:    "kFaultTemperature_bp_3",
        31:    "kFaultOther_bp_3"
    }   
        
    def __init__(self, title='',):
        super(FaultInfoGroupBox, self).__init__(title)
        self.faults = False
        self.labels = {}
        self.headers = []
        self.initialized = False
        
        row = 0             
        self.grid.addWidget(QtGui.QLabel('<b>Fault Name<b>'), row, 0)
        row += 1
        col = 0
        for v in FaultInfoGroupBox.faultIndexToString.itervalues():
            self.grid.addWidget(QtGui.QLabel(v), row, col) 
            row += 1
    
    def initGrid(self, robot_state):
        if (robot_state):
            if robot_state.num_nodes_responding > 0:
                row = 0
                for i in range(robot_state.num_nodes_responding):
                    self.headers.append(QtGui.QLabel('<b>Node %d<b>' % i))
                    self.grid.addWidget(self.headers[i], row, i + 1)
                    
                row += 1
                for v in FaultInfoGroupBox.faultIndexToString.itervalues():
                    col = 1
                    for i in range(robot_state.num_nodes_responding):
                        k = '%s_%d' % (v, i)
                        self.labels[k] = QtGui.QLabel('0')
                        self.grid.addWidget(self.labels[k], row, col)
                        col += 1
        
                    row += 1
                
                self.initialized = True
                
    def updateGrid(self, robot_state, robot_state_prev):
        if (robot_state and robot_state_prev):
            self.faults = False
            for i in range(robot_state.num_nodes_responding):
                if (robot_state.status.rtpc_counter == robot_state_prev.status.rtpc_counter) \
                    or robot_state.status.loop_counter == robot_state_prev.status.loop_counter:
                    self.headers[i].setStyleSheet("QLabel { color : red; font: bold; }")
                    self.faults = True
                else:
                    self.headers[i].setStyleSheet("QLabel { color : black; }")
                
            for k, v in FaultInfoGroupBox.faultIndexToString.iteritems():
                for i in range(robot_state.num_nodes_responding):
                    key = '%s_%d' % (v, i)
                    fault = ((robot_state.status.faults) & (0x1 << k)) >> k
                    if fault:
                        self.labels[key].setText("X")
                        self.labels[key].setStyleSheet("QLabel { color : red; font: bold; }")
                        self.faults = True
                    else:
                        self.labels[key].setText("O")
                        self.labels[key].setStyleSheet("QLabel { color : black; }")    
                         
class MonitorWidget(QtGui.QWidget):
    # rosTopicRxSignal = QtCore.Signal()
    
    def __init__(self, name="", parent=None):
        super(MonitorWidget, self).__init__()
        self.name = name
        rospy.loginfo('Starting %s' % self.name)
 
        # self.rosTopicRxSignal.connect(self.periodicTask)
        self.parent = parent
        self.robotState = None
        self.robotStatePrev = None
        
        self.robotStateSubscriber = rospy.Subscriber('robot_state', msgs.robot_state_msg, self.rosRobotStateCallback)
        
        # Group Boxes
        self.faultInfoGroupBox = FaultInfoGroupBox() 
        
        self.grid = QtGui.QGridLayout()
        self.grid.addWidget(self.faultInfoGroupBox, 0, 0)
        
        self.setLayout(self.grid)
        
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.periodicTask)
        self.timer.start(100)

        self.show()
              
    def periodicTask(self):
        if not self.faultInfoGroupBox.initialized or (self.robotState.num_nodes_configured != self.robotStatePrev.num_nodes_configured):
            self.faultInfoGroupBox.initGrid(self.robotState)
        else:
            self.faultInfoGroupBox.updateGrid(self.robotState, self.robotStatePrev)
            
        if self.faultInfoGroupBox.faults:
            self.parent.tabBar().setTabTextColor(0, QtGui.QColor(255, 0, 0))
        else:
            self.parent.tabBar().setTabTextColor(0, QtGui.QColor(0, 0, 0))
                 
    def rosRobotStateCallback(self, msg):
        self.robotStatePrev = self.robotState
        self.robotState = msg
         
    def cleanup(self):        
        rospy.loginfo('Exiting %s...' % self.name)
        self.robotStateSubscriber.unregister()
