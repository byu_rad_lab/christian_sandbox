#!/usr/bin/env python
 
# Otherlab, Inc ("COMPANY") CONFIDENTIAL
# Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains the property of COMPANY.
# The intellectual and technical concepts contained herein are proprietary to COMPANY
# and may be covered by U.S. and Foreign Patents, patents in process, and are protected
# by trade secret or copyright law. Dissemination of this information or reproduction
# of this material is strictly forbidden unless prior written permission is obtained
# from COMPANY. Access to the source code contained herein is hereby forbidden to
# anyone except current COMPANY employees, managers or contractors who have executed
# Confidentiality and Non-disclosure agreements explicitly covering such access.
#
# The copyright notice above does not evidence any actual or intended publication or
# disclosure of this source code, which includes information that is confidential
# and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
# DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
# SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
# IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
# OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
# REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
# ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
import python_qt_binding.QtGui as QtGui
import python_qt_binding.QtCore as QtCore

from qt_gui.plugin import Plugin 

import os

import control
import monitor

class TabCollection(QtGui.QTabWidget):
    def __init__(self):
        super(TabCollection, self).__init__()
        
        self.monitor = monitor.MonitorWidget(parent=self)
        self.addTab(self.monitor, "Fault Monitor")
        
        self.valve_control = control.HelmWidget()
        self.addTab(self.valve_control, "Valve Voltage Control")
       
        self.pressure_control = control.HelmWidget()
        self.addTab(self.pressure_control, "Pressure Control")
         
        for box in self.valve_control.moveJointGroupBoxes:
            box.lineEdits['target_value'].setText('0.0,0.0')
            box.lineEdits['move_time'].setText('0.0')

            box.controlModeComboBox.setCurrentIndex(control.HelmWidget.stringToControlModeDict['1:kRobotControlModeValve'])
            box.refTypeComboBox.setCurrentIndex(control.HelmWidget.stringToRefTypeDict['1:kMoveRefTypeStep'])
        
        for box in self.pressure_control.moveJointGroupBoxes:
            box.lineEdits['target_value'].setText('10000,10000')
            box.lineEdits['move_time'].setText('3.0')

            box.controlModeComboBox.setCurrentIndex(control.HelmWidget.stringToControlModeDict['2:kRobotControlModePressure'])
            box.refTypeComboBox.setCurrentIndex(control.HelmWidget.stringToRefTypeDict['2:kMoveRefTypeSin'])
            
    def cleanup(self):
        self.valve_control.cleanup()
        self.pressure_control.cleanup()
        self.monitor.cleanup()
    
class Helm(Plugin):
    def __init__(self, context):
        super(Helm, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('Helm')

        # Process standalone plugin command-line arguments
        from argparse import ArgumentParser
        parser = ArgumentParser()
        # Add argument(s) to the parser.
        parser.add_argument("-q", "--quiet", action="store_true",
                      dest="quiet",
                      help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())
        if not args.quiet:
            print 'arguments: ', args
            print 'unknowns: ', unknowns

        # Create QWidget
        self._widget = TabCollection()  # HelmWidget()  # HelmWindow()  # QtGui.QWidget()
        # Get path to UI file which should be in the "resource" folder of this package
        # ui_file = os.path.join(rospkg.RosPack().get_path('rqt_mypkg'), 'resource', 'MyPlugin.ui')
        # Extend the widget with all attributes and children from UI file
        # loadUi(ui_file, self._widget)
        # Give QObjects reasonable names
        self._widget.setObjectName('Helm')
        self._widget.setWindowIcon(QtGui.QIcon('%s/pneubotics-logo-symbol.png' % os.path.dirname(os.path.realpath(__file__))))
        # Show _widget.windowTitle on left-top of each plugin (when 
        # it's set in _widget). This is useful when you open multiple 
        # plugins at once. Also if you open multiple instances of your 
        # plugin at once, these lines add number to make it easy to 
        # tell from pane to pane.
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        # Add widget to the user interface
        context.add_widget(self._widget)
            
    def shutdown_plugin(self):
        self._widget.cleanup()
        
    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

    # def trigger_configuration(self):
        # Comment in to signal that the plugin has a way to configure
        # This will enable a setting button (gear icon) in each dock widget title bar
        # Usually used to open a modal configuration dialog
        
if __name__ == '__main__':
#      Window window;
#      window.show();
#      return app.exec();
    app = QtGui.QApplication(sys.argv)
    ex = HelmWidget()
    ex.show()
    sys.exit(app.exec_())
