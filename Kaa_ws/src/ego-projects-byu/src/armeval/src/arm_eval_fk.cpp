#include "armeval/arm_eval_fk.h"
#include "kinematics/arm.h"
#include "kinematics/types.h"
#include "kinematics/sdls.h"
#include "kinematics/arm.h"
#include "kinematics/joint_rot.h"
#include "kinematics/joint_ccc.h"
#include "path_planner/qpik.h"

#include <queue>
#include <stdio.h>
#include <iostream>
#include <string>
#include <eigen3/Eigen/Dense>
#include <vector>
#include <math.h>
#include <cmath>
#include <assert.h>
#include <time.h>
#include <algorithm>
#include <iomanip>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

using std::cout;
using std::endl;
using std::vector;
using namespace kinematics;

// namespace armeval {
// Constructors
ArmEvalFK::ArmEvalFK() { InitializeDefaultArm(); }
ArmEvalFK::ArmEvalFK(Arm* arm) {
  arm_ = arm;
  if (arm_->getRealJointIdx().size() == 0) {
    cout << "Real Joint Idx not set" << endl;
    assert(false);
  }
}

ArmEvalFK::~ArmEvalFK() {}

// Run the Evaluation
void ArmEvalFK::Run() {
  clock_gettime(CLOCK_MONOTONIC, &start_time_);
  srand(time(NULL));

  // Modes of Evaluation
  switch (mode_) {
    // FK is randomly sampled until a consecutive number of samples fall within redundant
    // points in the discretization
    case PERCENT_REDUNDANT:
      while (redundancy_percent_ < redundancy_cutoff_) {
        RandomlySampleFK();
      }
      break;

    //  Samples FK until user input is detected. Less efficient with kbhit
    case USER_INPUT:
      while (!kbhit()) {
        RandomlySampleFK();
      }
      break;

    //  Samples FK for fixed time
    case FIXED_TIME:
      clock_gettime(CLOCK_MONOTONIC, &present_);
      while (DiffTimeNano(start_time_, present_) / 1000000000 < end_time_) {
        RandomlySampleFK();
        clock_gettime(CLOCK_MONOTONIC, &present_);
      }
      break;

    // FK Samples until Redundancy Cutoff, Then an exhaustive search through every bin is
    // initiated.  If a pose has found, we seed the FK pose from the configuration already
    // found, and try and IK to neighbors without that same orientation.
    case HYBRID_TESTS:
      while (redundancy_percent_ < redundancy_cutoff_) {
        RandomlySampleFK();
      }
      cout << "--------------Switching to IK Approach------------" << endl;
      ExhaustiveIKFromNeighbors();
      break;

    // Samples joint angles systematically through tree structure
    case SYSTEMATIC:
      Vec angles(arm_->getRealJointIdx().size());
      SystematicallySampleFK(angles, 0);
      break;
  }
  // Scores summed of averaged  (can be modified)
  CalcScore();
  clock_gettime(CLOCK_MONOTONIC, &present_);
  total_time_ = DiffTimeNano(start_time_, present_);
}

void ArmEvalFK::RandomlySampleFK() {
  FindRandomPose();
  EvalPose();
}

void ArmEvalFK::EvalPose() {
  if (ValidPose()) {
    BinPose();
    // Numerical instabilities occur infrequently in RotToAx. These poses are skipped
    if (!skip_pose_) {
      if (path_plan_) {  // For Path Planning.
        UpdateLookUpTable();
      }
      // Collect Orientations and Compute Optional Metrics
      MarkDiscretization();
      CalcMetrics();

      iterations_ += 1;

      if (iterations_ % redundant_check_iter_ == 0) {
        Report();
        if (mode_ == HYBRID_TESTS && checking_ == false && IK_triggered_ == false) {
          checking_ = true;  // Prevent Recursion
          CheckTimes();
          checking_ = false;
        }
      }

      if (IK_triggered_ == true && mode_ == HYBRID_TESTS && iterations_ % 10 == 0) {
        clock_gettime(CLOCK_MONOTONIC, &present_);
        time_tracker_ik_.push_back(DiffTimeNano(start_time_, present_) / 1000000000.0);
        poses_found_tracker_ik_.push_back(new_poses_);
        redundancy_percent_tracker_.push_back(redundancy_percent_ * 100);
      }
    }
  }
}

void ArmEvalFK::CalcMetrics() {
  // Collect special metrics at ground
  if (EENearGroundPlane()) {
    arm_->calcJacReal();
    CalcCondNumInv();
    if (!skip_pose_) {
      CalcMaxLiftForce();

      // // Optional Secondary Objectives
      // CalcVelocityEllipsoidVolume();
      // CalcForceTransformationRatio();
      // CalcVelocityTransformationRatio();
      // CalcUNorm();
    }
  }
  if (arm_->type_ == LEG)
  {
      if(CanSupportWeight() && !redundant_flag_)
      {
          CalcBodyPayloadScore();
      }
  }
}

void ArmEvalFK::CheckTimes() {
  time_samples_ = true;

  // Sample Pure FK - (+1 in while loop exists so that first new sample thrown out. This
  // first sample was timed from the previous set and is erroneous
  int poses_found = new_poses_;
  while (new_poses_ - poses_found < (qsize_ + 1)) {
    RandomlySampleFK();
  }
  double fk_avg_time = avg_sample_time_ / 1000000000.0;
  avg_sample_time_FK.push_back(fk_avg_time);

  // Sample IK Approach - No Update Jacobian
  poses_found = new_poses_;
  while (new_poses_ - poses_found < (qsize_ + 1)) {
    RandomlySampleFK();
    IKToNeighbors(0);
  }
  double pik_avg_time_no_update = avg_sample_time_ / 1000000000.0;
  avg_sample_time_pik_no_update.push_back(pik_avg_time_no_update);

  // Sample IK Approach - Update Jacobian
  poses_found = new_poses_;
  while (new_poses_ - poses_found < (qsize_ + 1)) {
    RandomlySampleFK();
    IKToNeighbors(1);
  }
  double pik_avg_time = avg_sample_time_ / 1000000000.0;
  avg_sample_time_pik.push_back(pik_avg_time);

  // Sample QPIK Approach
  poses_found = new_poses_;
  while (new_poses_ - poses_found < (qsize_ + 1)) {
    RandomlySampleFK();
    IKToNeighbors(2);
  }
  double qpik_avg_time = avg_sample_time_ / 1000000000.0;
  avg_sample_time_qpik.push_back(qpik_avg_time);

  clock_gettime(CLOCK_MONOTONIC, &present_);
  avg_sample_time_tracker.push_back(DiffTimeNano(start_time_, present_) / 1000000000.0);

  if (print_on) {
    cout << "PureFK: " << fk_avg_time << "PIK_No_Jac_Update: " << pik_avg_time_no_update
         << " PIK_Jac_Update: " << pik_avg_time << " QPIK: " << qpik_avg_time << endl;
  }

  // Testing has shown Hybrid approach inefficient - Redundancy Cutoff is the default
  // Implement a method of checking times of all 4 methods and switching to most
  // effecient method.
  // if (ik_avg_time_ < fk_avg_time) {
  //   IK_triggered_ = true;
  // }
  time_samples_ = false;
}

// Sets Valid Joints to Random Between -pi/2 and pi/2 (adjustable)
void ArmEvalFK::FindRandomPose() {
  arm_->setJointAngsRandom();
  arm_->calcFK();
  FKnum_++;
}

// Recursive algorithm that explores a tree of angles in all joints by incremented by
// jt_idx
void ArmEvalFK::SystematicallySampleFK(Vec angs, int jt_idx) {
  for (int i = 0; i < dth_num_ + 1; i++) {
    angs[jt_idx] = i / double(dth_num_) * M_PI - M_PI / 2.0;
    if (jt_idx != arm_->getRealJointIdx().size()) {
      SystematicallySampleFK(angs, jt_idx + 1);
    } else {
      arm_->setJointAngsReal(angs);
      arm_->calcFK();
      EvalPose();
      FKnum_++;
      break;
    }
  }
}

// Keeps track of orientations found at different positions: Used as Metric 1
void ArmEvalFK::MarkDiscretization() {
  discretization_[xi_][yi_][zi_].metrics_at_pt.FKsHere_ += 1;

  // Fast way to check if we've seen this pose before
  if (PoseAlreadyFound(xi_, yi_, zi_, stdvecind_)) {
    redundant_poses_ += 1;
    redundant_flag_ = 1;
  } else {
    redundant_flag_ = 0;
    discretization_[xi_][yi_][zi_].pose_indices_found.push_back(stdvecind_);
    discretization_[xi_][yi_][zi_].metrics_at_pt.orientations_ += 1;

    // Track configuration found there
    if (mode_ == HYBRID_TESTS) {
      discretization_[xi_][yi_][zi_].configurations_found[stdvecind_] =
          arm_->getJointAngsReal();
    }

    new_poses_ += 1;             // Total new poses
    new_poses_this_round_ += 1;  // Unique poses in last 'redundant iter check' poses
    if (time_samples_ == true) {
      TimeSample();
    }
  }
}

bool ArmEvalFK::PoseAlreadyFound(int xi, int yi, int zi, int std_vec_ind) {
  return std::find(discretization_[xi][yi][zi].pose_indices_found.begin(),
                   discretization_[xi][yi][zi].pose_indices_found.end(),
                   std_vec_ind) != discretization_[xi][yi][zi].pose_indices_found.end();
}

void ArmEvalFK::TimeSample() {
  clock_gettime(CLOCK_MONOTONIC, &present_);
  sample_time_ = DiffTimeNano(last_sample_time_, present_);
  clock_gettime(CLOCK_MONOTONIC, &last_sample_time_);

  if (sample_time_queue.size() < qsize_) {
    sample_time_queue.push(sample_time_);
    avg_sample_time_ += sample_time_ / qsize_;
  } else {
    avg_sample_time_ += (sample_time_ - sample_time_queue.front()) / qsize_;
    sample_time_queue.push(sample_time_);
    sample_time_queue.pop();
  }
}
// Metric 2:Jacobian Velocity Ellipsoid Volume (3.124 Sciliano)
void ArmEvalFK::CalcVelocityEllipsoidVolume() {
  Eigen::MatrixXd inner_matrix = arm_->jac_body_real_ * arm_->jac_body_real_.transpose();
  double vol = sqrt(inner_matrix.determinant());

  if (vol > discretization_[xi_][yi_][zi_].metrics_at_pt.velocity_volume_) {
    discretization_[xi_][yi_][zi_].metrics_at_pt.velocity_volume_ = vol;
  }
}

// Metric 3:Jacobian Force in direction down (3.127 Sciliano)
void ArmEvalFK::CalcForceTransformationRatio() {
  Vec6 u;
  u << 0, 0, -1, 0, 0, 0;
  double alpha = 1.0 / sqrt(u.transpose() * arm_->links_.back().jac_hybrid_ *
                            arm_->links_.back().jac_hybrid_.transpose() * u);

  if (alpha > discretization_[xi_][yi_][zi_].metrics_at_pt.force_ratio_) {
    discretization_[xi_][yi_][zi_].metrics_at_pt.force_ratio_ = alpha;
  }
}

// Metric 4:Jacobian Force in direction down (3.127 Sciliano)
void ArmEvalFK::CalcVelocityTransformationRatio() {
  Vec6 u;
  u << 0, 0, -1, 0, 0, 0;
  double alpha = 1.0 / sqrt(u.transpose() *
                            (arm_->links_.back().jac_hybrid_ *
                             arm_->links_.back().jac_hybrid_.transpose()).inverse() *
                            u);

  if (alpha > discretization_[xi_][yi_][zi_].metrics_at_pt.velocity_ratio_) {
    discretization_[xi_][yi_][zi_].metrics_at_pt.velocity_ratio_ = alpha;
  }
}

// Assumes Jacobian and Stiffness Mats Computed
void ArmEvalFK::CalcMaxLiftForce() {
  double delta_max = .10;

  // Saturate at 80 newtons (above this load we don't care)
  double max_force = fmin(arm_->calcMaxStaticLiftForce(delta_max), 80.0);

  if (max_force == 0) {
    unsupportable_poses_ += 1;
  }

  if (max_force > discretization_[xi_][yi_][zi_].metrics_at_pt.max_force_) {
    discretization_[xi_][yi_][zi_].metrics_at_pt.max_force_ = max_force;
  }
}

// Metric 6: Condition number of Jacobian
void ArmEvalFK::CalcCondNumInv() {
  Eigen::MatrixXf jac_body = (arm_->jac_body_real_).cast<float>();
  Eigen::JacobiSVD<Eigen::MatrixXf> svd(jac_body);
  double CondInv = svd.singularValues().minCoeff() / svd.singularValues().maxCoeff();

  if (CondInv > discretization_[xi_][yi_][zi_].metrics_at_pt.cond_inv_) {
    discretization_[xi_][yi_][zi_].metrics_at_pt.cond_inv_ = CondInv;
  }

  if (1.0 / CondInv > 1000000) {
    skip_pose_ = true;
    poses_skipped_ += 1;
  }
}

// Metric 6: Norm of angs required to move in direction by 10 cm
void ArmEvalFK::CalcUNorm() {
  Vec6 xdes;
  xdes << 0, 0, .1, 0, 0, 0;  // L2 Norm of angle to go 10 cm up
  double UNorm = arm_->calcUNorm(xdes);

  if ((UNorm < discretization_[xi_][yi_][zi_].metrics_at_pt.UNorm_ ||
       discretization_[xi_][yi_][zi_].metrics_at_pt.UNorm_ == 0)) {
    discretization_[xi_][yi_][zi_].metrics_at_pt.UNorm_ = UNorm;
  }
}

//scores distance from a target plane
void ArmEvalFK::CalcTargetZScore(){
    double distance = (arm_->links_.back().g_world_end_(2, 3) - grid_center_(2)) - target_z_;
    scores_.reach_score_ -= sqrt(distance*distance);

}

//scores how much more than own weight a pose can support
void ArmEvalFK::CalcBodyPayloadScore(){
    arm_->calcJac();
    arm_->calcTorqueGravityLeg();
    
    Vec torques_gravity = arm_->getTorqueGravityLeg();
    Vec torques;
    torques = arm_->getJacSpatial().transpose()*body_wrench_ + torques_gravity;
    arm_->calcTorqueLimits();

    std::vector<double> diff;

    for (int i = 0; i<torques.size(); i++)
    {
        if (torques(i) < 0)
        {
            diff.push_back(std::abs(arm_->min_torques_[i] - torques(i)));
        }
        else
        {
            diff.push_back(std::abs(arm_->max_torques_[i] - torques(i)));
        }
    }

    int limit_idx = std::distance(diff.begin(), std::min_element(diff.begin(), diff.end())); //find the index of the minimum value in diff (the torque that is closest to its limit)

    Vec6 new_wrench;

    if (torques(limit_idx) < 0){
        new_wrench = arm_->min_torques_[limit_idx]/torques(limit_idx) * body_wrench_;
    }
    else{
        new_wrench = arm_->max_torques_[limit_idx]/torques(limit_idx) * body_wrench_;
    }

    // static int payload_counter = 0;
    // payload_counter++;
    // std::cout << "count: " << payload_counter << std::endl;
    // std::cout << "max_payload_ " << max_payload_  << std::endl;
    // std::cout << "new_wrench " << new_wrench(2)  << std::endl;
    // std::cout << "score " << std::abs(new_wrench(2) - body_wrench_(2))  << std::endl;

    scores_.payload_score_ += std::min(std::abs(new_wrench(2) - body_wrench_(2)),max_payload_); //do absolute value because sometimes the bigger force is more negative depending on the coordinate system

}

void ArmEvalFK::CalcScore() {
  // SCORING
  int count = 0;
  for (unsigned int i = 0; i < x_values_.size(); i++) {
    for (unsigned int j = 0; j < y_values_.size(); j++) {
      for (unsigned int k = 0; k < z_values_.size(); k++) {
        if (discretization_[i][j][k].metrics_at_pt.orientations_ != 0) {
          scores_.orientations_ += discretization_[i][j][k].metrics_at_pt.orientations_;
          // scores_.velocity_volume_ +=
          //     discretization_[i][j][k].metrics_at_pt.velocity_volume_;
          // scores_.force_ratio_ +=
          // discretization_[i][j][k].metrics_at_pt.force_ratio_;
          // scores_.velocity_ratio_ +=
          //     discretization_[i][j][k].metrics_at_pt.velocity_ratio_;
          scores_.max_force_ += discretization_[i][j][k].metrics_at_pt.max_force_;
          // scores_.cond_inv_ += discretization_[i][j][k].metrics_at_pt.cond_inv_;
          // scores_.UNorm_ += discretization_[i][j][k].metrics_at_pt.UNorm_;
          // }
          count += 1;
        }
      }
    }
  }
  if (count == 0) {
    cout << "WARNING: Arm failed to touch discretization" << endl;
  }

  scores_.payload_score_ = scores_.payload_score_/scores_.orientations_; //this is to normalize the score to get an average over all the valid configurations

  // Either average or total scores throughout discretization
  // scores_.orientations_ =
  //     scores_.orientations_;  // / count;// Total #  Orientations Found
  // scores_.velocity_volume_ = scores_.velocity_volume_ / count;  // Average Velocity
  // Volume
  // scores_.force_ratio_ =
  //     scores_.force_ratio_ / count;  // Average Force Ratio (@ Ground Plane?)
  // scores_.velocity_ratio_ =
  //     scores_.velocity_ratio_ / count;      // Average Velocity Ratio (@ Ground
  //     Plane?)
  // scores_.max_force_ = scores_.max_force_;  // / count;  // Max Force Neg Z Custom
  // Metric
  // scores_.cond_inv_ = scores_.cond_inv_ / count;  // Average Condition Number
  // scores_.UNorm_ = scores_.UNorm_ / count;        // Average UNorm

  // Report Scores
  if (print_on == true) {
    cout << "Total Configurations Simulated: " << FKnum_ << endl;
    cout << "Unique SE(3) Poses Found: " << new_poses_ << endl;

    cout << "scores_. Freq:" << scores_.orientations_ << endl;
    cout << "scores_. max_force:" << scores_.max_force_ << endl;
    cout << "scores_. payload_score:" << scores_.payload_score_ << endl;
    // cout << "scores_. Vel. Ellipse Vol.:" << scores_.velocity_volume_ << endl;
    // cout << "scores_. force trans.:" << scores_.force_ratio_ << endl;
    // cout << "scores_. vel. trans. ratio:" << scores_.velocity_ratio_ << endl;
    // cout << "scores_. UNorm_:" << scores_.UNorm_ << endl;
  }
}

// Reports progress every "redundant check iter" Valid poses
void ArmEvalFK::Report() {
  redundancy_percent_ = redundant_poses_ / redundant_check_iter_;

  clock_gettime(CLOCK_MONOTONIC, &present_);
  time_tracker_.push_back(DiffTimeNano(start_time_, present_) / 1000000000.0);
  poses_found_tracker_.push_back(new_poses_);
  redundancy_percent_tracker_.push_back(redundancy_percent_ * 100);

  if (print_on) {
    cout << time_tracker_.back() << " Sec.- "
         << round(new_poses_ / possible_poses_ * 10000) / 100 << " % poses found - "
         << redundancy_percent_ * 100 << "% Redundant - " << round(FKnum_ / 100000) / 10
         << "e6"
         << " FK iter - " << endl;
  }
  redundant_poses_ = 0;
  new_poses_this_round_ = 0;
}

// Checks if EE is in discretization, and all links are above ground
bool ArmEvalFK::ValidPose() {
  // JabberWock
  if (arm_->type_ == JABBERWOCK)
  {
      if (EEInDiscretization() && LinksAboveGround() && EEInDesiredOrientation() &&
          EEInTaskRegion())
        return true;
      else {
        return false;
      }
  }

  // Leg
  else if (arm_->type_ == LEG)
  {
      if (EEInDiscretization() && EEInDesiredOrientation() && EEInInfiniteSlab()) // && EEInDesiredXYRegion()) // && CanSupportWeight())
      {
          return true;
      }
      else {
          return false;
      }
  }

  // NASA
  else if (arm_->type_ == NASA)
  {
      if (EEInDiscretization() && LinksAboveGround()) {
        return true;
      } else {
        return false;
      }
  }

  else{
      std::cout<< "Arm is not of a Valid Type to be optimized.  Either add a case in arm_eval_fk.cpp in ValidPose or make the arm of a valid type" << std::endl;
      assert (false);
      return false;
  }
}

// all z points of homogoneous transforms are greater than 0
bool ArmEvalFK::LinksAboveGround() {
  for (unsigned int i = 0; i < arm_->links_.size(); i++) {
    if (arm_->links_[i].g_world_end_(2, 3) < 0) {
      return (false);
    }
  }
  return true;
}

// In front of the robot base of the arm: NOTE: this is a 3x1.5x3m discretization
bool ArmEvalFK::EEInDiscretization() {
  if (fabs(arm_->links_.back().g_world_end_(0, 3) - grid_center_(0)) <
          bounds_ / 2.0 + dx_ / 2.0 &&
      fabs(arm_->links_.back().g_world_end_(1, 3) - grid_center_(1)) <
          bounds_ / 2.0 + dx_ / 2.0 &&
      fabs(arm_->links_.back().g_world_end_(2, 3) - grid_center_(2)) <
          bounds_ / 2.0 + dx_ / 2.0)
    return true;
  else {
    return false;
  }
}

// Can be defined
bool ArmEvalFK::EEInTaskRegion() {
  // For Jabberwock 2m x 1m x .
  if (fabs(arm_->links_.back().g_world_end_(0, 3) - grid_center_(0)) <
          xdim_ / 2.0 + dx_ / 2.0 &&
      fabs(arm_->links_.back().g_world_end_(1, 3) - grid_center_(1)) <
          ydim_ / 2.0 + dx_ / 2.0 &&
      fabs(arm_->links_.back().g_world_end_(2, 3) - grid_center_(2)) <
          zdim_ / 2.0 + dx_ / 2.0)
    return true;
  else {
    return false;
  }
}

bool ArmEvalFK::EEInInfiniteSlab() {
    // std::cout << "end z - grid_center_:    " << arm_->links_.back().g_world_end_(2,3) - grid_center_(2) << std::endl;
    // std::cout << "grid_center:  " << grid_center_(2) << std::endl;
    // std::cout << "thres:    " << slabthres_ << std::endl;
    // std::cout << "end z:    " << arm_->links_.back().g_world_end_(2,3) << std::endl;
  if (arm_->links_.back().g_world_end_(2, 3) - grid_center_(2) >
          slabthresmin_ && arm_->links_.back().g_world_end_(2, 3) - grid_center_(2) <
          slabthresmax_ && arm_->links_.back().g_world_end_(1, 3) - grid_center_(1) >= 0)
   { //std::cout << "True" << std::endl;
    return true;}
  else {
      // std::cout << "False" << std::endl;
    return false;
  }
}

// Can be defined
bool ArmEvalFK::EEInDesiredOrientation() {
  // For Jabberwock: The Z axis component Dotted with [0,1,0]
  // gives the component in the +y direction, if it's negative
  // we're point slightly -y. .5 ==> 60 Degrees about +y
  if (arm_->links_.back().g_world_end_(axis_, 2) > cos_ang_){
    return true;
  }
  else {
    return false;
  }
}

bool ArmEvalFK::EEInDesiredXYRegion(){
  //Defines acceptable position (x,y) of the end-effector relative to the base
  //test whether x and y satisfy 1) x >= tan(theta)*y or 2) x >= tan(pi/2-theta)*y
  //line_1_test and line_2_test are in the private variables towards end 
  bool line_1_test = (arm_->links_.back().g_world_end_(0, 3)) >= tan(x_y_reach_ang_)*(arm_->links_.back().g_world_end_(1, 3));  
  bool line_2_test = (arm_->links_.back().g_world_end_(0, 3)) >= tan(1.5708 - x_y_reach_ang_)*(arm_->links_.back().g_world_end_(1, 3));  
  if (line_1_test||line_2_test){
    return true;
  }
  else{
    return false;
  }
}

// EE on ground Plane
bool ArmEvalFK::EENearGroundPlane() {
  if (z_ >= 0 && z_ <= .2) {
    return true;
  } else {
    return false;
  }
}

bool ArmEvalFK::CanSupportWeight() {
    arm_->calcJac();
    arm_->calcTorqueGravityLeg();
    
    Vec torques_gravity = arm_->getTorqueGravityLeg();
    Vec torques;
    torques = arm_->getJacSpatial().transpose()*body_wrench_ + torques_gravity;
    arm_->calcTorqueLimits();

    for (int i=0; i < torques.size(); i++)
    {
        if (torques(i) > arm_->max_torques_[i] || torques(i) < arm_->min_torques_[i])
        {
            // std::cout << "Can't support weight \n\t Torque: " << torques(i) << "\t Max: " << arm_->max_torques_[i] << "\t Min: " << arm_->min_torques_[i] << std::endl;
           return false;
        } 
    }

    if(NotSlipping(torques))
    {
        return true;
    }
    else{
        return false;
    }
}

bool ArmEvalFK::NotSlipping(Vec torques) {
    //get number of dof of the last joint to know how many torques are being applied at the end of the link
    int num_dof_last = arm_->links_[arm_->links_.size() - 1].joint_->num_dof_;

    //initialize the torques at the beginning of last link (the last joint) to 0
    Vec3 torque_last;
    torque_last.setZero();

    //set this torque equal to the correct torques corresponding to the last joint
   torque_last.head(num_dof_last) = torques.tail(num_dof_last);

   //declare homogeneous transformations that are needed in the equations of motion
   Hom g_world_joint_top = arm_->links_[arm_->links_.size() - 1].g_world_top_; //need to check if top or bottom (need top for the t_joint_com, etc., but may not be accurate for rotating the joint torques I think I need bottom
   Hom g_world_joint_bot = arm_->links_[arm_->links_.size() - 1].g_world_bot_; //NOT USING THIS AT MOMENT BECAUSE PRETTY SURE NEED TOP_ need to check if top or bottom (need top for the t_joint_com, etc., but may not be accurate for rotating the joint torques I think I need bottom
   Hom g_world_com = arm_->links_[arm_->links_.size() - 1].g_world_com_; 
   Hom g_world_end = arm_->g_world_tool(); //need to check if top or bottom

   //find the various translation vectors from the homogeneous transformations
   Vec3 t_world_joint = g_world_joint_top.topRightCorner(3,1);
   Vec3 t_world_com = g_world_com.topRightCorner(3,1);
   Vec3 t_world_end = g_world_end.topRightCorner(3,1);

   //find the necessary relative translations
   Vec3 t_joint_com = t_world_com - t_world_joint;
   Vec3 t_joint_end = t_world_end - t_world_joint;

   //make skew symmetric matrices from the translation vectors to be able to do cross products
   Rot t_joint_com_skew = VecHat(t_joint_com);
   Rot t_joint_end_skew = VecHat(t_joint_end);


   //declare and assign link weight, normal force and friction vectors
   Vec3 link_weight, normal_force, test_friction;
   link_weight << 0.0, 0.0, arm_->links_[arm_->links_.size() - 1].inertia_self_.mass*gravity_;
   normal_force << 0.0, 0.0, -(mass_body_ + mass_whole_arm_)*gravity_;

   //correctly rotate the joint torques into the world frame (this is where top seems more correct, but may not be)
   Vec3 torque_last_world = g_world_joint_top.topLeftCorner(3,3)*torque_last;

   Vec3 required_friction_torque;

   //calculate the required torque from friction to keep the leg in static equilibrium
   required_friction_torque = -torque_last_world - t_joint_com_skew*link_weight - t_joint_end_skew*normal_force;

   //calculate friction force
   test_friction <<  required_friction_torque(1)/t_joint_end(2), -required_friction_torque(0)/t_joint_end(2), 0.0;

   //this was used to test by model because this value should always end up as the same as the required_friction_torque(2)
   // double test_Z = required_friction_torque(0)*-t_joint_end(0)/t_joint_end(2) - t_joint_end(1)/t_joint_end(2)*required_friction_torque(1);

   // std::cout << "Test Z: " << test_Z << std::endl;
   // std::cout << "Act Z: " << required_friction_torque(2) << std::endl;
   // std::cout << "Test Friction: " << test_friction << std::endl;
   // std::cout << "test_friction: " << test_friction.norm() << std::endl;
   // std::cout << "greatest possible friction force: " << -mew_*normal_force(2) << std::endl;

   if (test_friction.norm() > -mew_*normal_force(2))
   {
       // std::cout << "FALSE!!" << std::endl << std::endl;
       return false;
   }
   else{
       // std::cout << "TRUE!!" << std::endl << std::endl;
       return true;
   }

   


}

// Binning the EE effector into the XYZ and Rotation Discretization
void ArmEvalFK::BinPose() {
  BinXYZ();
  BinRot();
}

void ArmEvalFK::BinXYZ() {
  xi_ = round((arm_->links_.back().g_world_end_(0, 3) - grid_center_(0)) / dx_) +
        XYZRes_ / 2.0;
  yi_ = round((arm_->links_.back().g_world_end_(1, 3) - grid_center_(1)) / dx_) +
        XYZRes_ / 2.0;
  zi_ = round((arm_->links_.back().g_world_end_(2, 3) - grid_center_(2)) / dx_) +
        XYZRes_ / 2.0;
  // std::cout << "x: " << arm_->links_.back().g_world_end_(0, 3) << std::endl;
  // std::cout << "y: " << arm_->links_.back().g_world_end_(1, 3) << std::endl;

  if (xi_ > XYZRes_ || xi_ < 0 || yi_ > XYZRes_ || yi_ < 0 || zi_ > XYZRes_ || zi_ < 0) {
    cout << "Manipulator reached out of discretization, adjust discretization" << endl;
    cout << "GWorldTool" << endl
         << arm_->getGWorldTool() << endl;
    assert(false);
  }

  x_ = x_values_[xi_];
  y_ = y_values_[yi_];
  z_ = z_values_[zi_];
  // std::cout << "z value: " << z_ <<  std::endl;
  // std::cout << "index:  " << zi_ << std::endl;
}

void ArmEvalFK::BinRot() {
  skip_pose_ = false;
  // Discretize With Respect to Base Frame to avoid nonsymmetries in discretization.
  Vec3 ax = RotToAx(arm_->getGArmTool().topLeftCorner(3, 3));
  // Hom g_arm_end = HomInv(arm_->g_world_arm_) * arm_->links_.back().g_world_end_;
  // Vec3 ax = RotToAx(g_arm_end.topLeftCorner(3, 3));
  //
  // Vec3 ax = RotToAx(arm_->links_.back().g_world_end_.topLeftCorner(3, 3));

  wxi_ = round((ax[0] - rot_values_[0]) / dth_);
  wyi_ = round((ax[1] - rot_values_[0]) / dth_);
  wzi_ = round((ax[2] - rot_values_[0]) / dth_);

  // Check for numerical instabilities in RotToAx: This error is a pain to catch.
  if ((wxi_ >= SO3Res_ || wyi_ >= SO3Res_ || wzi_ >= SO3Res_ || wxi_ < 0 || wyi_ < 0 ||
       wzi_ < 0)) {
    cout << "Axial Rotation rounded to outside rot discretization: Skipping Pose" << endl;
    skip_pose_ = true;
    poses_skipped_ += 1;
  } else {

    while (sqrt(pow(rot_values_[wxi_], 2) + pow(rot_values_[wyi_], 2) +
                pow(rot_values_[wzi_], 2)) > M_PI) {
      wxi_ = RoundSO3Index(wxi_);
      wyi_ = RoundSO3Index(wyi_);
      wzi_ = RoundSO3Index(wzi_);
    }

    // Maps the orientation Bin to a map of std orientations
    stdvecind_ = orientation_map_[wxi_][wyi_][wzi_];
    wx_ = rot_values_[wxi_];
    wy_ = rot_values_[wyi_];
    wz_ = rot_values_[wzi_];
  }
}
void ArmEvalFK::ExhaustiveIKFromNeighbors() {
  IK_triggered_ = true;
  for (uint i = 0; i < x_values_.size(); i++) {
    cout << "x = " << x_values_[i] << endl;
    for (uint j = 0; j < y_values_.size(); j++) {
      for (uint k = 0; k < z_values_.size(); k++) {
        for (int ind = 0; ind < num_orientation_vecs_; ind++) {
          if (discretization_[i][j][k].configurations_found[ind].size() != 0) {
            arm_->setJointAngsReal(discretization_[i][j][k].configurations_found[ind]);
            arm_->calcJacReal();
            for (uint l = -IK_n; l < IK_n + 1; l++) {
              for (uint m = -IK_n; m < IK_n + 1; m++) {
                for (uint n = -IK_n; n < IK_n + 1; n++) {
                  int xn = i + l;
                  int yn = j + m;
                  int zn = k + n;
                  Vec3 temp;
                  temp << xn, yn, zn;
                  // if neighbor is not found in discretization
                  if (temp.maxCoeff() < x_values_.size() && temp.minCoeff() >= 0) {
                    if (discretization_[xn][yn][zn].configurations_found[ind].size() ==
                        0) {
                      Hom g_world_target = arm_->getGWorldTool();
                      g_world_target(0, 3) += l * dx_;
                      g_world_target(1, 3) += m * dx_;
                      g_world_target(2, 3) += n * dx_;
                      IKAndEval(g_world_target, 0);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

void ArmEvalFK::IKToNeighbors(int flag) {
  arm_->calcJacReal();

  for (uint i = -IK_n; i < IK_n + 1; i++) {
    for (uint j = -IK_n; j < IK_n + 1; j++) {
      for (uint k = -IK_n; k < IK_n + 1; k++) {
        int xn = xi_ + i;
        int yn = yi_ + j;
        int zn = zi_ + k;
        Vec3 temp;
        temp << xn, yn, zn;
        // if neighbor is in discretization
        if (temp.maxCoeff() < x_values_.size() && temp.minCoeff() >= 0) {
          // If neighbor doesn't have that orientation

          if (!PoseAlreadyFound(xn, yn, zn, stdvecind_)) {
            // Measured with respect to the arm base
            Hom g_world_target = arm_->getGWorldTool();
            g_world_target(0, 3) += i * dx_;
            g_world_target(1, 3) += j * dx_;
            g_world_target(2, 3) += k * dx_;
            if (flag == 0 || flag == 1) {
              IKAndEval(g_world_target, flag);  // Uses Pseudo Inverse 10x slower
            } else if (flag == 2) {
              QPIKAndEval(g_world_target);  // Uses QPIK - +100x slower than FK sample
            }
          }
        }
      }
    }
  }
  // newIKpose_tracker.push_back(newIKposes_);
  // if (newIKpose_tracker.size() > 10) {
  //   newIKpose_tracker.erase(newIKpose_tracker.begin());
  //   double max = *max_element(newIKpose_tracker.begin(), newIKpose_tracker.end());
  //   if (max < IKTol) {
  //     Grid_Saturated = true;
  //   }
  // }
}

void ArmEvalFK::QPIKAndEval(Hom g_world_target) {
  Vec angs = arm_->getJointAngsReal();
  qpik_.Solve(g_world_target);
  if (qpik_.status_ == "Solved") {
    EvalPose();
  }
  // Set Back
  arm_->setJointAngsReal(angs);
  arm_->calcJac();
  BinPose();
}

void ArmEvalFK::IKAndEval(Hom g_world_target, int jac_update_flag) {
  Vec angs = arm_->getJointAngsReal();

  for (uint i = 0; i < IKMaxCount_; i++) {
    Hom g_tool_target = HomInv(arm_->getGWorldTool()) * g_world_target;
    Twist V = TwistFromHom(g_tool_target);

    // Damped Psuedo Inverse
    Jac J = arm_->getJacBodyReal();
    int n = arm_->getRealJointIdx().size();
    Eigen::MatrixXd I = Eigen::MatrixXd::Identity(n, n);
    pinv_ = J.transpose() * (J * J.transpose() + I * .01).inverse();  // dof more than 6

    // IK
    Vec delta_q = pinv_ * V * rho_;
    Vec new_angs = arm_->getJointAngsReal() + delta_q;
    if (new_angs.cwiseAbs().maxCoeff() > jt_limit_) {
      break;
    } else {
      arm_->setJointAngsReal(new_angs);
      if (jac_update_flag == 1) {
        arm_->calcJacReal();
      } else if (jac_update_flag == 0) {
        arm_->calcFK();
      }
    }
    if (HomError(arm_->getGWorldTool(), g_world_target, .5) < .01) {
      EvalPose();
      break;
    }
  }

  // Set Back
  arm_->setJointAngsReal(angs);
  if (jac_update_flag == 1) {
    arm_->calcJacReal();
  } else if (jac_update_flag == 0) {
    arm_->calcFK();
  }
  BinPose();
}

int ArmEvalFK::RoundSO3Index(int ind) {
  if (ind > ceil((SO3Res_ - 1.0) / 2.0)) {
    ind -= 1;
  }
  if (ind < floor((SO3Res_ - 1.0) / 2.0)) {
    ind += 1;
  }
  return ind;
}

// For path_planning - Sample based approach with Dijkstra's
void ArmEvalFK::UpdateLookUpTable() {
  configuration current_config;
  current_config.jt_angs = arm_->getJointAngs();

  for (auto& pt : path_) {
    current_config.error_from_pt = (arm_->getGWorldTool().topRightCorner(3, 1) -
                                    pt.g_world_des.topRightCorner(3, 1)).norm();
    pt.close_configs.push_back(current_config);
    std::push_heap(pt.close_configs.begin(), pt.close_configs.end(), compare_distance);

    if (pt.close_configs.size() > storage_num_) {
      std::pop_heap(pt.close_configs.begin(), pt.close_configs.end(), compare_distance);
      pt.close_configs.pop_back();
    }
  }
}

void ArmEvalFK::ShowDiscretization() {
  cout << "x_values" << endl;
  for (unsigned int i = 0; i < x_values_.size(); i++) {
    cout << x_values_[i] << " ";
  }
  cout << endl;
  cout << "y_values" << endl;
  for (unsigned int i = 0; i < y_values_.size(); i++) {
    cout << y_values_[i] << " ";
  }
  cout << endl;
  cout << "z_values" << endl;
  for (unsigned int i = 0; i < z_values_.size(); i++) {
    cout << z_values_[i] << " ";
  }
  cout << endl;
}

void ArmEvalFK::Initialize() {
  // Discretization can be changed externally until initialized
  SetDiscretization();

  // Arm Initialization
  arm_->setJointStateZero();
  arm_->calcFK();

  // Setting parameters to zero
  time_tracker_.clear();
  time_tracker_ik_.clear();
  time_tracker_.push_back(0);
  poses_found_tracker_.clear();
  poses_found_tracker_ik_.clear();
  poses_found_tracker_.push_back(0);
  redundancy_percent_tracker_.clear();
  redundancy_percent_tracker_.push_back(0);

  // Indexes points in the orientation grid
  num_orientation_vecs_ = 0;
  int ind = 0;
  for (int i = 0; i < rot_values_.size(); i++) {
    for (int j = 0; j < rot_values_.size(); j++) {
      for (int k = 0; k < rot_values_.size(); k++) {
        if (sqrt(rot_values_[i] * rot_values_[i] + rot_values_[j] * rot_values_[j] +
                 rot_values_[k] * rot_values_[k]) <= M_PI) {
          num_orientation_vecs_++;
        }
        orientation_map_[i][j][k] = ind;
        ind++;
      }
    }
  }

  // Count possible poses in entire discretization
  possible_poses_ =
      num_orientation_vecs_ * x_values_.size() * y_values_.size() * z_values_.size();
  //
  // Setup Look Up Table
  if (path_plan_) {
    for (auto& pt : path_) {
      auto begin = pt.close_configs.begin();
      auto end = pt.close_configs.end();
      std::make_heap(begin, end, compare_distance);
    }
  }

  if (mode_ == HYBRID_TESTS) {
    qpik_.arm_ = arm_;
    qpik_.error_term_tol_ = .05;
    qpik_.step_scale_ = .5;
    qpik_.max_iter_ = 1;
  }

  //for JABBERWOCK
  // axis_ = 1;
  // cos_ang_ = .5;
  //
  // for LEG
  axis_ = 2; //means the world z axis
  cos_ang_ = .707; //45 degrees
  slabthresmin_ = 0.25; //means the min desired reaching for leg is 0.25 meters below the body
  slabthresmax_ = 2.0; //means the max desired reaching for leg is 2.0 meters below the body
  target_z_ = 0.4064; //for reach_score_ (around 16 inches for now)
  x_y_reach_ang_ = -.78538; //-45 degrees

  mass_body_ = 10.0; //mass of body for leg evaluation
  gravity_ = 9.81;

  mass_whole_arm_ = 0.0;

  for (int i = 0; i < arm_->links_.size(); i++)
  {
      mass_whole_arm_ = mass_whole_arm_ + arm_->links_[i].inertia_self_.mass + arm_->links_[i].joint_->mass_;
  }

  mew_ = 0.3; //estimated coefficient of friction


  max_payload_ = 2*mass_body_*gravity_;

  body_wrench_ << 0.0, 0.0, -.5*mass_body_*gravity_, 0.0, 0.0, 0.0; //wrench that is exerted on the base from the body of a leg
}

void ArmEvalFK::SetMount(Hom g_world_arm) { arm_->g_world_arm_ = g_world_arm; }

// Dynamic Allocation of Memory for different discretizations
void ArmEvalFK::SetDiscretization() {
  // Resize Vectors to Discretization
  discretization_.resize(XYZRes_ + 1);
  for (unsigned int i = 0; i < discretization_.size(); i++) {
    discretization_[i].resize(XYZRes_ + 1);
    for (unsigned int j = 0; j < discretization_[i].size(); j++) {
      discretization_[i][j].resize(XYZRes_ + 1);
      if (mode_ == HYBRID_TESTS) {
        for (uint k = 0; k < discretization_[i][j].size(); k++) {
          discretization_[i][j][k].configurations_found.resize(SO3Res_ * SO3Res_ *
                                                               SO3Res_);
        }
      }
    }
  }
  orientation_map_.resize(SO3Res_);
  for (unsigned int i = 0; i < orientation_map_.size(); i++) {
    orientation_map_[i].resize(SO3Res_);
    for (unsigned int j = 0; j < orientation_map_[i].size(); j++) {
      orientation_map_[i][j].resize(SO3Res_);
    }
  }

  // Discretization Definitions
  // We assume cube discretizations, rectangular are a no go (BinXYZ)
  dx_ = bounds_ / (XYZRes_);  // space between grid points
  x_values_.resize(XYZRes_ + 1);
  y_values_.resize(XYZRes_ + 1);
  z_values_.resize(XYZRes_ + 1);
  indices_.resize(XYZRes_ + 1);
  for (int i = 0; i < XYZRes_ + 1.0; i++) {
    x_values_[i] = -bounds_ / 2.0 + grid_center_(0) + i * dx_;
    y_values_[i] = -bounds_ / 2.0 + grid_center_(1) + i * dx_;
    z_values_[i] = -bounds_ / 2.0 + grid_center_(2) + i * dx_;
    indices_[i] = i;
  }

  // Orientation Discretization
  double th_end = (SO3Res_ - 1.0) / SO3Res_ * M_PI;
  rot_values_ = Eigen::VectorXf::LinSpaced(SO3Res_, -th_end, th_end);
  dth_ = rot_values_[1] - rot_values_[0];
}

// Default Arm for Constructor
void ArmEvalFK::InitializeDefaultArm() {
  Vec lengths = Vec(6);
  lengths.setOnes();
  lengths = lengths * .23;

  double height = 0.02;
  static Arm arm;

  // Joint Limits
  Vec bounds_lower = Vec(2);
  Vec bounds_upper = Vec(2);
  bounds_lower << -jt_limit_, 0;
  bounds_upper << jt_limit_, 0;

  // Build Architecture of Robot
  for (unsigned int i = 0; i < lengths.size(); i++) {
    // Rotational Joint
    Link link;
    JointRot joint(0);
    joint.limits()->setLimitsScalar(0);
    link = Link(joint);
    link.setGTopEnd(0, 0, 0);
    link.setInertiaSelf(0, Vec3(0, 0, 0), Vec3(0, 0, 0));
    arm.addLink(link);

    // CCC Joint: 2DOF!
    JointCCC jointccc(height);
    jointccc.limits()->setLimitsVector(bounds_lower, bounds_upper);
    link = Link(jointccc);
    link.setGTopEnd(0, 0, lengths[i]);

    // double m = 1.0 * lengths[i];
    double m = .453;
    double r = 0.07;
    double Ix = m * (3.0 * r * r + lengths[i]) / 12.0;
    double Iy = m * (3.0 * r * r + lengths[i]) / 12.0;
    double Iz = m * (r * r) / 2.0;
    link.setInertiaSelf(m, Vec3(0.0, 0.0, lengths[i] / 2.0), Vec3(Ix, Iy, Iz));
    arm.addLink(link);

    // Rotate actuators 90 degrees relative to eachother: very important
    Vec3 w;
    w << 0, 0, -M_PI / 2.0;
    Hom hom = kinematics::HomRotate(w);
    arm.links_[2 * i + 1].g_top_end_ *= hom;
  }

  // Initialize Stiffness
  double active_stiffness = 1000000;
  double passive_stiffness = 270;
  double torsional_stiffness = 50;
  for (unsigned int i = 0; i < lengths.size(); i++) {
    // torsional
    arm.links_[2 * i].joint_->setJointStiffnessScalar(torsional_stiffness);
    Vec jt_stiffness(2);

    // u, v
    jt_stiffness << active_stiffness, passive_stiffness;
    arm.links_[2 * i + 1].joint_->setJointStiffnessVector(jt_stiffness);
  }

  // SetRealJoints
  Vec real_joint_idx(lengths.size());
  for (int i = 0; i < lengths.size(); i++) {
    real_joint_idx(i) = 3 * i + 1;
  }
  arm.setRealJointIDX(real_joint_idx);

  // Initialize Mounting Position
  Vec3 th;
  th << -M_PI / 2.0, 0.0, 0.0;
  // th << -0.0, 0.0, 0.0;
  Rot rot = AxToRot(th);
  Vec3 z;
  z << 0, 0, 1.5;
  Hom g_world_arm;
  g_world_arm.setIdentity();
  g_world_arm.topLeftCorner(3, 3) = rot;
  g_world_arm.topRightCorner(3, 1) = z;
  arm.g_world_arm_ = g_world_arm;

  // Initialize Arm
  arm_ = &arm;
}

// Checks to see if key's been hit: Found Online, Slows speed though
bool ArmEvalFK::kbhit(void) {
  struct termios oldt, newt;
  int ch;
  int oldf;
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
  ch = getchar();
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);
  if (ch != EOF) {
    ungetc(ch, stdin);
    return 1;
  }
  return 0;
}

double ArmEvalFK::DiffTimeNano(struct timespec start, struct timespec end) {
  double diff = (end.tv_sec - start.tv_sec) * 1000000000 + end.tv_nsec - start.tv_nsec;
  return diff;
}

void ArmEvalFK::PrintQueue(const std::queue<double> queue) {
  std::queue<double> queue_copy(queue);
  while (!queue_copy.empty()) {
    cout << queue_copy.front() << " ";
    queue_copy.pop();
  }
  cout << endl;
}

// }  // end class
