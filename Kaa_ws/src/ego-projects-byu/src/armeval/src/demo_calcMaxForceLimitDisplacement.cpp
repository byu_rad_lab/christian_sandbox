#include "armeval/arm_eval_fk.h"
#include <kinematics/joint_base.h>
#include <kinematics/joint_ccc.h>
#include <kinematics/joint_rot.h>
#include "kinematics/types.h"
#include "kinematics/link.h"
#include "kinematics/arm.h"
#include <time.h>
#include <stdio.h>
#include <iostream>
#include "kinematics/BenchTimer.h"
#include <eigen3/Eigen/Dense>

using namespace armeval;
using namespace kinematics;

using std::cout;
using std::endl;

int main() {
  srand(time(NULL));
  // Use Default Arm

  ArmEvalFK eval;

  // Set Discretization
  Vec3 Center;
  Center << 0,1.5,1.5;
  eval.setXYZRes(30);
  eval.setSO3Res(5);
  eval.setBounds(3);
  eval.setCenterDiscretization(Center);
  eval.initialize();
  eval.print_on = true;

  // Termination Mode
  // eval.mode_ = USER_INPUT
  eval.mode_ = PERCENT_REDUNDANT;
  // eval.mode_ = SYSTEMATIC;
  //
  // Settings
  // eval.dth_num_ = 20;
  eval.redundancy_cutoff_ = .999;

  // Vec angs(eval.arm_->getRealJointIdx().size());
  // angs.setOnes();
  // angs = angs*M_PI/2;
  // eval.arm_->setJointAngsReal(angs);
  eval.arm_->setJointAngsRandom();
  cout << "Angles Set to:\n";
  cout << eval.arm_->joint_angs_ << endl << endl;
  eval.arm_->calcJacReal();
  eval.arm_->calcStiffness();

  double limit;
  Vec3 direction;
  direction << 0,0,-1.0;

  double delta=.1;

  limit = eval.arm_->calcMaxForceLimitDisplacement(direction,delta);
  cout << "Limit Force" << limit << endl<<endl;
}
