#include <kinematics/joint_base.h>
#include <kinematics/joint_ccc.h>
#include "kinematics/types.h"
#include "kinematics/link.h"
#include "kinematics/arm.h"
#include "armeval/arm_eval.h"

#include <stdio.h>

#include <iostream>

using kinematics::Link;
using kinematics::Arm;
using kinematics::Vec;
using kinematics::Mat;
using kinematics::JointBase;
using kinematics::JointCCC;
using kinematics::Vec3;
using kinematics::Hom;

using std::cout;
using std::endl;

int main() {

  Vec lengths = Vec(7);
  lengths << 0.30, 0.05, 0.30, 0.05, .30, .05, .30;
  double height = 0.2;

  Arm my_arm(height, lengths, kinematics::kJointTypeCC1D);
  for (unsigned int i = 0; i < my_arm.links_.size(); i++) {
    Hom hom;
    Vec3 w;
    w << 0, 0, -M_PI / 2.0;
    hom = kinematics::HomRotate(w);
    my_arm.links_[i].g_top_end_ *= hom;
    // cout  <<  "Link Transform" << endl << my_arm.links_[i].g_top_end_ << endl;
  }
  armeval::ArmEval eval(&my_arm);
  eval.initialize();
  cout << "Running Eval Object" << endl;
  eval.run();
}
