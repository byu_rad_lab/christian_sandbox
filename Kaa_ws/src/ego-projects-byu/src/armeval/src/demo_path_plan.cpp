#include "armeval/arm_eval_fk.h"
#include <kinematics/joint_base.h>
#include <kinematics/joint_ccc.h>
#include <kinematics/joint_rot.h>
#include "kinematics/types.h"
#include "kinematics/link.h"
#include "kinematics/arm.h"
#include <stdio.h>
#include <iostream>
#include "kinematics/BenchTimer.h"
#include <eigen3/Eigen/Dense>
#include <time.h>
// #include <chrono>
#include <ctime>

using namespace armeval;
using namespace kinematics;

using std::cout;
using std::endl;

int main() {
  // Use Default Arm

  ArmEvalFK eval;

  // Set Discretization
  Vec3 Center;
  Center << 0,1.5,1.5;
  eval.setXYZRes(30);
  eval.setSO3Res(5);
  eval.setBounds(3);
  eval.setCenterDiscretization(Center);
  eval.print_on = false;
  eval.mode_ = PERCENT_REDUNDANT;
  eval.redundancy_cutoff_ = .9;
  eval.path_plan_ = true;

  for (int i=0;i<10;i++){
      path_pt mypt;
      double x = i/10.0;
      double y = 0;
      double z = 0;
      mypt.g_world_des = HomTranslate(x,y,z);
      eval.path_.push_back(mypt);
  }
  
  eval.initialize();
  double begin = std::clock();
  eval.run();
  double end1 = std::clock();
  cout << "total time:" << end1 - begin << endl;

  // for (unsigned int i=0;i<eval.path_.size();i++){
  //     cout << "GOAL" << endl;
  //     cout << eval.path_[i].g_world_des << endl;
  //     cout << eval.path_[i].close_configs.size() << " configurations found" << endl;
  //     for (unsigned int j = 0;j<eval.path_[i].close_configs.size();j++){
  //         eval.arm_->setJointAngs(eval.path_[i].close_configs[j].jt_angs);
  //         eval.arm_->calcFK();
  //         cout << eval.arm_->getGWorldTool() << endl;
  //     }
  //     cout << endl << endl;
  // }
  cout << "Finished Evaluating" << endl << endl;

}
