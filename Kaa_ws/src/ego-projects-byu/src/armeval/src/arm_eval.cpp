#include "armeval/arm_eval.h"
#include "kinematics/arm.h"
#include "kinematics/types.h"
#include "kinematics/arm_ik.h"
#include "kinematics/sdls.h"
#include "kinematics/arm.h"
#include <stdio.h>
#include <iostream>
#include <string>
#include <eigen3/Eigen/Dense>
#include <vector>
#include <math.h>
#include <cmath>
#include <time.h>
#include <assert.h>

using std::cout;
using std::endl;
using std::vector;


namespace armeval {

ArmEval::ArmEval(Arm* arm) : arm_(arm) {}

void ArmEval::run() {
  start_time_ = time(0);
  switch (mode_) {
    case FK:
      while (redundancy_percent_ < redundancy_cutoff_) {
        fk_run();
      }
      break;
    case IK:
      ik_run();
      break;
    case HYBRID:
      mode_ = FK;
      while (redundancy_percent_ < redundancy_cutoff_) {
        fk_run();
      }
      cout << endl
           << "FK Total Time: " << difftime(time(0), start_time_) << endl;
      cout << "Switching to IK... " << endl;
      mode_ = IK;
      ik_run();
      break;
  }

  // Metric 1: Orientation Scoring
  double w1 = 1;
  orientation_score_ = w1 * unique_poses_ / possible_poses_;

  // Metric 2: Dexterity Control
  double w2 = 0;
  manipulability_score_ = w2 * get_manipulability_score();

  score_ = orientation_score_ + manipulability_score_;

}

void ArmEval::fk_run() {
  arm_->setJointAngsRandom();
  arm_->calcJac();  // depending on metrics Jac or FK
  compute_metrics();
}

void ArmEval::compute_metrics() {
  // Grid found pose into xyz
  Vec3 xyz = arm_->links_.back().g_world_end_.topRightCorner(3, 1);
  Vec3 xyz_ind = find_xyz_bin(xyz);

  // Check if in task region
  if (in_task_region(values_[xyz_ind[0]], values_[xyz_ind[1]], values_[xyz_ind[2]])) {
    Vec3 ax_vec = RotToAx(arm_->links_.back().g_world_end_.topLeftCorner(3, 3));
    Vec3 rot_ind = find_rot_bin(ax_vec);
    unique_ = true;
    x = xyz_ind[0];
    y = xyz_ind[1];
    z = xyz_ind[2];
    wx = rot_ind[0];
    wy = rot_ind[1];
    wz = rot_ind[2];

    // Metric 1: Orientation reached within WS (Also used for terminating criterion)
    if (!gridded_space_[x][y][z][wx][wy][wz].empty()) {
      unique_ = false;
      redundant_poses_ += 1;
    }
    if (unique_) {
      gridded_space_[x][y][z][wx][wy][wz].push_back(arm_->joint_angs_);
      unique_poses_ += 1;
      unique_poses_this_round_ += 1;
    }

    // Metric 2: Velocity Ellipsoid Volume
    Eigen::MatrixXd inner_matrix =
        arm_->links_.back().jac_body_ * arm_->links_.back().jac_body_.transpose();
    double vol = sqrt(inner_matrix.determinant());

    if (vol > velocity_ellipse_volume_[x][y][z]) {
      velocity_ellipse_volume_[x][y][z] = vol;
    }

    iterations += 1;
    report();
  }
}

void ArmEval::report() {
  if (mode_ == FK) {
    if (iterations % redundant_check_iter_ == 0) {
      redundancy_percent_ = redundant_poses_ / redundant_check_iter_;

      times_.push_back(difftime(time(0), start_time_));
      pose_percent_.push_back(unique_poses_ / possible_poses_);

      if (print_on) {
        cout << "Mode FK -- " << difftime(time(0), start_time_) << " Seconds Elapsed -- "
             << unique_poses_ / possible_poses_ * 100 << "% poses found -- "
             << redundancy_percent_ * 100 << "% Redundant" << endl;
      }

      redundant_poses_ = 0;
      unique_poses_this_round_ = 0;
    }
  }

  if (mode_ == IK) {
    if (iterations % IK_check_iter_ == 0) {
      pose_percent_.push_back(unique_poses_ / possible_poses_);

      times_.push_back(difftime(time(0), start_time_));
      percent_complete_ = iterations / poses_left_;

      if (print_on) {
        cout << "Mode IK -- " << difftime(time(0), start_time_) << " Seconds Elapsed -- "
             << unique_poses_ / possible_poses_ * 100 << "% poses found -- "
             << percent_complete_ * 100 << "% Complete" << endl;
      }
    }
  }
}

void ArmEval::ik_run() {
  ik_count_ = 0;
  iterations = 0;
  poses_left_ = possible_poses_ - unique_poses_;
  for (int xi = 0; xi < values_.size(); xi++) {
    for (int yi = 0; yi < values_.size(); yi++) {
      for (int zi = 0; zi < values_.size(); zi++) {
        if (in_task_region(values_[xi], values_[yi], values_[zi])) {
          for (int wxi = 0; wxi < rot_values_.size(); wxi++) {
            for (int wyi = 0; wyi < rot_values_.size(); wyi++) {
              for (int wzi = 0; wzi < rot_values_.size(); wzi++) {
                if (gridded_space_[xi][yi][zi][wxi][wyi][wzi].empty()) {
                  if (sqrt(rot_values_[wxi] * rot_values_[wxi] +
                           rot_values_[wyi] * rot_values_[wyi] +
                           rot_values_[wzi] * rot_values_[wzi]) <= M_PI) {
                    ik_count_ += 1;
                    ik_here(xi, yi, zi, wxi, wyi, wzi);
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

void ArmEval::ik_here(const int& xi, const int& yi, const int& zi, const int& wxi,
                      const int& wyi, const int& wzi) {

  Vec3 Ax;
  Ax[0] = rot_values_[wxi];
  Ax[1] = rot_values_[wyi];
  Ax[2] = rot_values_[wzi];
  Rot R = AxToRot(Ax);
  Vec3 P;
  P[0] = values_[xi];
  P[1] = values_[yi];
  P[2] = values_[zi];
  Hom Target;
  Target.setIdentity();
  Target.topLeftCorner<3, 3>() = R;
  Target.topRightCorner<3, 1>() = P;

  arm_->setJointAngsZero();
  arm_->calcJac();
  arm_ik_.calcIK(Target);

  for (int i = 0; i < arm_->joint_angs_.size(); i++) {
    if (fabs(arm_->joint_angs_[i]) > jt_max_) {
      arm_ik_.status_ = false;
    }
  }

  if (arm_ik_.status_ == false) {
    arm_->setJointAngsRandom();
    arm_->calcJac();
    arm_ik_.calcIK(Target);

    for (int i = 0; i < arm_->joint_angs_.size(); i++) {
      if (fabs(arm_->joint_angs_[i]) > jt_max_) {
        arm_ik_.status_ = false;
      }
    }
  }

  if (arm_ik_.status_ == true) {
    unique_poses_ += 1;
    unique_poses_this_round_ += 1;
    gridded_space_[xi][yi][zi][wxi][wyi][wzi].push_back(arm_->joint_angs_);
  }

  iterations += 1;
  report();
}

Vec3 ArmEval::find_xyz_bin(Vec3 xyz) {
  Vec3 ind;
  double xind = round(xyz[0] / dx_) + round((values_.size() - 1) / 2.0);
  double yind = round(xyz[1] / dx_) + round((values_.size() - 1) / 2.0);
  double zind = round(xyz[2] / dx_) + round((values_.size() - 1) / 2.0);
  ind[0] = xind;
  ind[1] = yind;
  ind[2] = zind;
  return ind;
}

Vec3 ArmEval::find_rot_bin(Vec3 ax) {
  Vec3 ind;
  double wxind = round((ax[0] - rot_values_[0]) / dth_);
  double wyind = round((ax[1] - rot_values_[0]) / dth_);
  double wzind = round((ax[2] - rot_values_[0]) / dth_);
  while (sqrt(pow(rot_values_[wxind], 2) + pow(rot_values_[wyind], 2) +
              pow(rot_values_[wzind], 2)) > M_PI) {
    wxind = onetowardzero(wxind);
    wyind = onetowardzero(wxind);
    wzind = onetowardzero(wxind);
  }
  ind << wxind, wyind, wzind;
  return ind;
}

double ArmEval::get_manipulability_score() {
  double score = 0;
  int count = 0;
  for (int i = 0; i < values_.size(); i++) {
    for (int j = 0; j < values_.size(); j++) {
      for (int k = 0; k < values_.size(); k++) {
        if (in_task_region(values_[i], values_[j], values_[k])) {
          score += velocity_ellipse_volume_[i][j][k];
          if (velocity_ellipse_volume_[i][j][k] > 10) {
            cout << "BIG PORTION ADDED TO MATRIX" << endl;
            cout << velocity_ellipse_volume_[i][j][k] << endl;
            // cout << i << endl;
            // cout << j << endl;
            // cout << k << endl;
          }
          count += 1;
        }
      }
    }
  }
  // cout << "SCORE Sum" << endl;
  // cout << score << endl;
  // cout << "Count" << endl;
  // cout << count << endl;
  score = score / count;
  // cout << "score" << endl;
  // cout << score << endl;
  return score;
}

bool ArmEval::in_task_region(double x, double y, double z) {
  // Check to see if point is in WS of interest.
  double norm = sqrt(x * x + y * y + z * z);
  // if (norm > .8 * arm_length_ || norm < .2 * arm_length_ || z < 0) {
  if (norm > .9 * arm_length_ || norm < .1 * arm_length_) { // || z < 0) {
    return false;
  } else {
    return true;
  }
  // return true;
}

int ArmEval::onetowardzero(int ind) {
  if (ind > ceil((n_ - 1) / 2.0)) {
    ind -= 1;
  }
  if (ind < floor(((n_ - 1) / 2.0))) {
    ind += 1;
  }
  return ind;
}

void ArmEval::initialize() {
  // Setting Internal Variables
  times_.clear();
  pose_percent_.clear();
  score_ = 0;

  // Arm Initialization
  arm_->setJointStateZero();
  arm_->calcFK();
  arm_length_ = arm_->links_.back().g_world_end_(2, 3);

  // Gridding initialization
  values_ = Eigen::VectorXf::LinSpaced(2 * resolution_ + 1, -arm_length_, arm_length_);
  dx_ = arm_length_ / (resolution_);
  indices_ = Eigen::VectorXf::LinSpaced(2 * resolution_ + 1, 0, values_.size() - 1);

  // Orientation Discretization
  n_ = 3;
  double th_end = (n_ - 1) / n_ * M_PI;
  rot_values_ = Eigen::VectorXf::LinSpaced(n_, -th_end, th_end);
  dth_ = rot_values_[1] - rot_values_[0];

  // Default Options
  redundancy_percent_ = 0;
  redundant_poses_ = 0;
  unique_poses_ = 0;
  possible_poses_ = 0;
  unique_poses_this_round_ = 0;
  iterations = 0;
  redundancy_cutoff_ = .95;

  num_orientation_vecs_ = 0;

  for (int i = 0; i < rot_values_.size(); i++) {
    for (int j = 0; j < rot_values_.size(); j++) {
      for (int k = 0; k < rot_values_.size(); k++) {
        if (sqrt(rot_values_[i] * rot_values_[i] + rot_values_[j] * rot_values_[j] +
                 rot_values_[k] * rot_values_[k]) <= M_PI) {
          num_orientation_vecs_++;
        }
      }
    }
  }
  // cout << "Num of Orientation Vecs" << endl;
  // cout << num_orientation_vecs_ << endl;

  // count possible poses
  for (int i = 0; i < values_.size(); i++) {
    for (int j = 0; j < values_.size(); j++) {
      for (int k = 0; k < values_.size(); k++) {
        if (in_task_region(values_[i], values_[j], values_[k])) {
          possible_poses_ += num_orientation_vecs_;
        }
      }
    }
  }
  //
  // ArmIK
  arm_ik_.arm_ = arm_;
  arm_ik_.e_clamp_ = 3;
  arm_ik_.sdls_solver.g_max_ = 20;
  arm_ik_.end_tol_ = .01;
  arm_ik_.max_iter_ = 10;
  jt_max_ = M_PI / 2.0;
  //
  // Initilialize Ellipse Vol to 0
  for (int i = 0; i < values_.size(); i++) {
    for (int j = 0; j < values_.size(); j++) {
      for (int k = 0; k < values_.size(); k++) {
        velocity_ellipse_volume_[i][j][k] = 0;
      }
    }
  }
}
}
