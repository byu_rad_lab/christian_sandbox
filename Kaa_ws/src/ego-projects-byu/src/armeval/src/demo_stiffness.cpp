#include "armeval/arm_eval_fk.h"
#include <stdio.h>
#include <iostream>
#include "kinematics/BenchTimer.h"
#include <eigen3/Eigen/Dense>

using namespace armeval;
using namespace kinematics;

using std::cout;
using std::endl;

int main() {
  // Use Default Arm

  ArmEvalFK eval;

  // Set Discretization
  // eval.arm_->setJointAngsZero();
  // eval.arm_->setJointAngsRandom();
  Vec angs(eval.arm_->getRealJointIdx().size());
  angs.setOnes();
  // angs = angs*M_PI/12;
  angs = angs*0;

  eval.arm_->setJointAngsReal(angs);
  cout << "Angles Set to:\n";
  cout << eval.arm_->joint_angs_ << endl << endl;
  eval.arm_->calcJacReal();
  cout << "world to tool" << endl << eval.arm_->getGWorldTool()<<endl<<endl;
  eval.arm_->calcStiffness();

  cout << "\nBody Jacobian Set at\n" << eval.arm_->getJacBody() << endl;
  cout << "\nHybrid Jacobian Set at\n" << eval.arm_->getJacHybrid() << endl;
  // cout << "\nBody Stiffness Set at\n" << eval.arm_->getStiffnessBody() << endl;
  // cout << "\nHybrid Stiffness Set at\n" << eval.arm_->getStiffnessHybrid() << endl;
  cout << "\nBody Flexibility Set at\n" << eval.arm_->getFlexibilityBody() << endl;
  cout << "\nHybrid Flexibility Set at\n" << eval.arm_->getFlexibilityHybrid() << endl;
  cout << endl << endl;
}
