#include "armeval/arm_eval_fk.h"
#include <kinematics/joint_base.h>
#include <kinematics/joint_ccc.h>
#include <kinematics/joint_rot.h>
#include "kinematics/types.h"
#include "kinematics/link.h"
#include "kinematics/arm.h"
#include <stdio.h>
#include <iostream>
#include "kinematics/BenchTimer.h"
#include <eigen3/Eigen/Dense>

//using namespace armeval;
using namespace kinematics;

using std::cout;
using std::endl;

int main() {
  // Use Default Arm
  ArmEvalFK eval;

  // Set Discretization
  for (int i = 0; i < 1000; i++) {
    Vec angs(eval.arm_->getRealJointIdx().size());
    srand((unsigned int)time(0));
    angs.setRandom();
    // angs = angs*M_PI/4;
    // angs = angs*0;
    // angs[0] = angs[0]+.005;

    eval.arm_->setJointAngsReal(angs);
    // cout << "Angles Set to:\n" << eval.arm_->joint_angs_ << endl << endl;
    eval.arm_->calcJacReal();
    eval.arm_->calcStiffness();
    // cout << "flexibility matrix" << endl;
    // cout << eval.arm_->getFlexibilityHybrid() << endl;

    double limit_passive;
    double limit_active;

    // direction of force
    Vec3 direction;
    direction << 0, 0, -1;

    // Tolerance
    double delta = .2;

    // MAXIMUM FORCE TO KEEP ARM FROM DEFLECTING OUTSIDE OF SPHERE

    limit_passive = eval.arm_->calcMaxForceLimitDisplacement(direction, delta);
    // limit_active = eval.arm_->calcMaxForceLimitTorque(direction);

    cout << "calcMaxLiftForce Metric " << endl;
    cout << "Max Allowable Force Within Sphere Deflection Limit: " << limit_passive
         << endl;
    cout << "Max Allowable Force Within Active Torque Constraints: " << limit_active <<
    endl;
  }
}
