#include "armeval/arm_eval_fk.h"
#include <kinematics/joint_base.h>
#include <kinematics/joint_ccc.h>
#include <kinematics/joint_rot.h>
#include "kinematics/types.h"
#include "kinematics/link.h"
#include "kinematics/arm.h"
#include <stdio.h>
#include <iostream>
#include "kinematics/BenchTimer.h"
#include <eigen3/Eigen/Dense>

//using namespace armeval;
using namespace kinematics;

using std::cout;
using std::endl;

int main() {
  // Use Default Arm

  ArmEvalFK eval;

  // Set Discretization
  Vec3 Center;
  Center << 0,0,1.5;
  eval.setXYZRes(30);
  eval.setSO3Res(5);
  eval.setBounds(3.0);
  eval.setCenterDiscretization(Center);

  // Termination Mode
  // eval.mode_ = USER_INPUT;
  // eval.mode_ = PERCENT_REDUNDANT;
  // eval.mode_ = FIXED_TIME;
  // eval.mode_ = SYSTEMATIC;
  // eval.mode_ = HYBRID;
  eval.mode_ = HYBRID_TESTS;
  //
  // Settings
  eval.dth_num_ = 10;
  // eval.redundancy_cutoff_ = .9;
  // eval.end_time_ = 5;

  // Set Discretization
  // eval.arm_->setJointAngsZero();
  // eval.arm_->setJointAngsRandom();
  eval.print_on = true;
  eval.Initialize();
  eval.Run();
  cout << "Finished Evaluating" << endl << endl;

}
