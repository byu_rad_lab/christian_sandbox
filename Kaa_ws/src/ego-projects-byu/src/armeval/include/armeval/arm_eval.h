// #ifndef SOFTWARE_SRC_ARMEVAL_INCLUDE_ARMEVAL_ARM_EVAL_H_
// #define SOFTWARE_SRC_ARMEVAL_INCLUDE_ARMEVAL_ARM_EVAL_H_

#include <eigen3/Eigen/Dense>
#include "kinematics/types.h"
#include "kinematics/arm.h"
#include "kinematics/arm_ik.h"
#include <string>
#include <vector>
#include <stdio.h>
#include <iostream>
#include <time.h>

using kinematics::Arm;
using kinematics::ArmIK;
using kinematics::Rot;
using kinematics::Hom;
using kinematics::Vec;
using kinematics::Vec3;
using kinematics::RotToAx;
using kinematics::AxToRot;

namespace armeval {
typedef std::vector<Vec> bin;

class ArmEval {
 public:
  void initialize();
  void run();
  void fk_run();
  void compute_metrics();
  void report();
  void ik_run();
  void ik_here(const int& xi, const int& yi, const int& zi, const int& wxi, const int& wyi,
               const int& wzi);
  Vec3 find_xyz_bin(Vec3);
  Vec3 find_rot_bin(Vec3);
  double get_manipulability_score();
  bool in_task_region(double, double, double);
  int onetowardzero(int);

  // ArmEval
  ArmEval(Arm* arm);
  Arm* arm_;
  ArmIK arm_ik_;
  double arm_length_;
  time_t start_time_;
  double curr_time_;
  std::vector<double> times_;
  std::vector<double> pose_percent_;
  bool print_on = false;
  double orientation_score_;
  double manipulability_score_;
  double score_;

  // Grid
  static const int resolution_ = 10;
  static const int SO3Res_ = 3;
  double dx_;
  Eigen::VectorXf values_;
  int x, y, z, wx, wy, wz;

  double n_;
  double dth_;
  Eigen::VectorXf rot_values_;
  int num_orientation_vecs_;

  Eigen::VectorXf indices_;
  bin gridded_space_[2 * resolution_ + 1][2 * resolution_ + 1][2 * resolution_ +
                                                               1][SO3Res_][SO3Res_][SO3Res_];
  double velocity_ellipse_volume_[2 * resolution_ + 1][2 * resolution_ + 1][2 * resolution_ + 1];

  // FK Parameters
  bool unique_;
  double redundancy_cutoff_;
  double redundancy_percent_;
  double redundant_poses_;
  double unique_poses_;
  double possible_poses_;
  double unique_poses_this_round_;
  int iterations;
  int redundant_check_iter_ = 10000;

  // IK Parameters
  double poses_left_;
  double percent_complete_;
  int ik_count_;
  int IK_check_iter_ = 1000;

  enum Mode { FK, IK, HYBRID };
  Mode mode_ = FK;
  double jt_max_;
};
}

// #endif  // SOFTWARE_SRC_ARMEVAL_INCLUDE_ARMEVAL_IK_ARM_EVAL_H_
