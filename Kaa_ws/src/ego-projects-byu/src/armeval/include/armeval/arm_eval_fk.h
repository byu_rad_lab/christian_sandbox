#ifndef SRC_ARMEVAL_INCLUDE_ARMEVAL_ARM_EVAL_H_
#define SRC_ARMEVAL_INCLUDE_ARMEVAL_ARM_EVAL_H_
#include <eigen3/Eigen/Dense>
#include "kinematics/types.h"
#include "kinematics/arm.h"
#include "path_planner/qpik.h"
#include <string>
#include <vector>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <queue>

using kinematics::Arm;
using kinematics::Link;
using kinematics::Rot;
using kinematics::Hom;
using kinematics::Vec;
using kinematics::Vec3;
using kinematics::Vec6;
using kinematics::RotToAx;
using kinematics::AxToRot;

// MODE To Run Evaluator With, Default is Percent Redundant
enum mode {
  PERCENT_REDUNDANT,
  USER_INPUT,
  FIXED_TIME,
  HYBRID_TESTS,
  SYSTEMATIC
};

struct metrics {
  // Dexterous Measurement
  int orientations_ = 0;  // Number of standard orientations found at pt
  int FKsHere_ = 0;

  // Jacobian based metrics in Sciliano
  double velocity_volume_ = 0;  // Velocity of velocity Ellipsoid
  double force_ratio_ = 0;      // Force Transformation Ratio
  double velocity_ratio_ = 0;   // Velocity Transformation Ratio

  // Other metrics: Max Force exertible in hybrid frame
  // passive: Infinite stiffness in active directions, limited displacement
  // active: Limited Torque Constraints at joints
  // max_force =  min(passive,active)
  double max_force_ = 0;
  // Condition number inverse of Jac
  double cond_inv_ = 0;
  // Norm of angles required to move 10 cm
  double UNorm_ = 0;
  // Composite Score of pose
  double best_composite_score_ = 0;
  // target score (related to number of orientations, but used for gradient based optimization
  double reach_score_ = 0;
  // payload score (will divid by number of orientations found so that it is a normalized average payload)
  double payload_score_ = 0;
};

struct discretization_pt {
  metrics metrics_at_pt;
  std::vector<int> pose_indices_found;    // Used to "Bin"
  std::vector<Vec> configurations_found;  // Used in Exhaustive IK to seed IK from FK
};

struct configuration {
  Vec jt_angs;
  double error_from_pt;
};

struct path_pt {
  Hom g_world_des;
  std::vector<configuration> close_configs;
};

bool compare_distance(configuration a, configuration b) {
  return a.error_from_pt < b.error_from_pt;
  return true;
}

// struct configuration
class ArmEvalFK {
 public:
  // Constructors
  ArmEvalFK();
  ArmEvalFK(Arm* arm);
  ~ArmEvalFK();
  Arm* arm_;
  void ShowDiscretization();

  // Getters and Setters
  inline std::vector<double> getx_values_(void) const { return x_values_; }
  inline std::vector<double> gety_values_(void) const { return y_values_; }
  inline std::vector<double> getz_values_(void) const { return z_values_; }
  inline int getnum_orientation_vecs_(void) const { return num_orientation_vecs_; }

  void setBounds(const double& bounds) { bounds_ = bounds; }
  void setCenterDiscretization(const Vec3& center) { grid_center_ = center; }
  void setXYZRes(const int& res) { XYZRes_ = res; }
  void setSO3Res(const int& res) { SO3Res_ = res; }
  void setSlabThresMin(const double& thres) { slabthresmin_ = thres; }
  void setSlabThresMax(const double& thres) { slabthresmax_ = thres; }
  void setTargetZ(const double& target) { target_z_ = target; }
  inline Vec3 getCenterDiscretization(void) const { return grid_center_; }

  // Initialization Routines
  void Initialize();
  void SetMount(Hom);
  void SetDiscretization();
  void InitializeDefaultArm();
  void Run();

  // Mode Specific Variables
  mode mode_ = PERCENT_REDUNDANT;
  int end_time_ = 20;               // For FIXED TIME mode
  int dth_num_ = 10;                // For SYSTEMATIC Mode
  double redundancy_cutoff_ = .98;  // for PERCENT REDUNDANT mode

  // Other Variables
  // Timing Variables
  double total_time_;
  std::vector<double> time_tracker_;
  std::vector<double> time_tracker_ik_;
  std::vector<double> poses_found_tracker_;         // Found poses/Total Poses in Grid
  std::vector<double> poses_found_tracker_ik_;      // Found poses/Total Poses in Grid
  std::vector<double> redundancy_percent_tracker_;  // Percent of last 100000 that were

  std::vector<double> avg_sample_time_FK;
  std::vector<double> avg_sample_time_pik;
  std::vector<double> avg_sample_time_pik_no_update;
  std::vector<double> avg_sample_time_qpik;
  std::vector<double> avg_sample_time_tracker;

  // Scoring
  metrics scores_;  // Final Scores

  // Global Data
  int FKnum_ = 0;
  int poses_skipped_ = 0;
  int unsupportable_poses_ = 0;
  bool print_on = false;

  //desired orientation variables
  int axis_; //0 for x, 1 for y, 2 for z
  double cos_ang_; //cos of max allowed angle away from the desired orientation
  double x_y_reach_ang_; //angle for acceptable range of (x,y) position of Leg end-effector

  // SWIG Helper Functions
  metrics get_metric(int i, int j, int k) {
    return discretization_[i][j][k].metrics_at_pt;
  }
  double getValFromVecDouble(int i, std::vector<double> myvec) { return myvec[i]; }
  double getValFromVecInt(int i, std::vector<int> myvec) { return myvec[i]; }

  // PathPlanning Functionality
  // Path Planning
  bool path_plan_ = false;
  std::vector<path_pt> path_;
  int storage_num_ = 10;

  int getNumConfigs(path_pt pt) { return pt.close_configs.size(); }
  Vec getConfig(int i, path_pt pt) { return pt.close_configs[i].jt_angs; }
  void add_path_pt(Hom desired_pose) {
    path_pt mpt;
    mpt.g_world_des = desired_pose;
    path_.push_back(mpt);
  }
  path_pt getPathPt(int i) { return path_[i]; }

  Vec6 body_wrench_;

  double mass_body_;
  double gravity_;
  double mass_whole_arm_;
  double mew_;
  double max_payload_;

  bool redundant_flag_; //used to make sure torque and other scores aren't added up if a pose as already been found

 private:
  // FK Sampling Routines
  void FindRandomPose();
  void RandomlySampleFK();
  void SystematicallySampleFK(Vec, int);
  void MarkDiscretization();

  // IK Routines
  void ExhaustiveIKFromNeighbors();
  void IKToNeighbors(int);
  void IKAndEval(Hom g_end_target, int jac_update_flag);
  void QPIKAndEval(Hom g_end_target);

  // Discretizing Routines
  void BinPose();
  void BinXYZ();
  void BinRot();
  bool ValidPose();
  bool EEInDiscretization();
  bool LinksAboveGround();
  bool EEInTaskRegion();
  bool EEInInfiniteSlab();
  bool EEInDesiredOrientation();
  bool EEInDesiredXYRegion(); //added for leg
  bool EENearGroundPlane();
  bool CanSupportWeight();
  bool NotSlipping(Vec torques);
  int RoundSO3Index(int);

  // Update/Check Routines
  bool PoseAlreadyFound(int xi, int yi, int zi, int std_vec_ind);
  void UpdateLookUpTable();

  // Evaulation Routines
  void EvalPose();
  void CalcMetrics();
  void CalcVelocityEllipsoidVolume();
  void CalcForceTransformationRatio();
  void CalcVelocityTransformationRatio();
  void CalcMaxLiftForce();
  void CalcCondNumInv();
  void CalcUNorm();
  void CalcTargetZScore(); //scores distance from a target plane
  void CalcBodyPayloadScore(); //find how much more than its own body weight it can support

  // Other Routines
  void CalcScore();
  void Report();

  // IK Attempt
  bool kbhit(void);

  // Timing
  double DiffTimeNano(struct timespec t1, struct timespec t2);
  void CheckTimes();
  void TimeSample();

  //--------------------VARIABLES--------------------
  // CurrentPose Data
  double x_, y_, z_, wx_, wy_, wz_;
  int xi_, yi_, zi_, wxi_, wyi_, wzi_, stdvecind_;

  // Timing and Tracking Variables
  struct timespec start_time_, present_, last_sample_time_;
  std::queue<double> sample_time_queue;
  double avg_sample_time_ = 0;
  double sample_time_;

  // Discretization Variables
  std::vector<std::vector<std::vector<discretization_pt>>> discretization_;
  std::vector<std::vector<std::vector<int>>> orientation_map_;  // Mapping from poses to
  double dth_;
  double dx_;  // Space between grid points
  std::vector<double> x_values_, y_values_, z_values_;
  Eigen::VectorXf rot_values_;
  std::vector<int> indices_;
  int num_orientation_vecs_;
  int XYZRes_ = 30;  // How many sections (not points) (even)
  int SO3Res_ = 5;   // How many points in discretization (odd)
  Vec3 grid_center_;
  double bounds_ = 3;  // discretization space (i.e 3x3x3) meters

  // Implicit
  double jt_limit_ = M_PI / 2.0;
  QPIK qpik_;

  // IK Variables
  Eigen::MatrixXd pinv_ = Eigen::MatrixXd::Identity(6, 6);
  int IKMaxCount_ = 10;
  int IK_n = 2;  // neighborhood size
  int IKTol = 3;  // If the IK solver doesn't find more than " poses consistently break
  int qsize_ = 50;
  double rho_ = .5;
  std::vector<int> newIKpose_tracker;

  // Triggers
  bool skip_pose_ = false;
  bool checking_ = false;
  bool time_samples_ = false;
  bool IK_triggered_ = false;
  bool Grid_Saturated = false;

  // For Task Region of Interest Option (must be set here)
  double xdim_ = 2.0;
  double ydim_ = .5;
  double zdim_ = 1.0;

  //desired region variables for infinite slab
  double slabthresmin_;
  double slabthresmax_;
  double target_z_;

  // Global Data
  int possible_poses_ = 0;
  int new_poses_ = 0;
  int redundant_check_iter_ = 200000;
  int iterations_ = 0;
  double redundancy_percent_ = 0;
  double redundant_poses_ = 0;
  int new_poses_this_round_ = 0;

  // Helper Functions
  void PrintQueue(const std::queue<double> queue);

};

#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_IK_ARM_EVAL_H_
