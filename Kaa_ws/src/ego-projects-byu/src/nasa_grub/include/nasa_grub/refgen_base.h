/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written permission
 * is obtained from COMPANY. Access to the source code contained herein is hereby
 * forbidden to anyone except current COMPANY employees, managers or contractors who
 * have executed Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 * MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE
 * OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY
 * PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE
 * RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
 * MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#ifndef SRC_NASA_GRUB_INCLUDE_NASA_GRUB_REFGEN_BASE_H_
#define SRC_NASA_GRUB_INCLUDE_NASA_GRUB_REFGEN_BASE_H_

#include <primitives/reference_generator.h>
#include <primitives/task.h>

#include <string>

#include "nasa_grub/hardware_interface.h"
#include "nasa_grub/limits.h"

namespace nasa_grub {
namespace refgen {

class RefgenBase : public primitives::task::Task {
 public:
  RefgenBase(const std::string &name, const std::string &parentName,
             nasa_grub::robot_state_msg *robotState);
  virtual ~RefgenBase();

 protected:
  virtual bool jointMoving(void) const = 0;
  virtual void getNextMoveFromQueue(void) = 0;
  nasa_grub::robot_state_msg *robotState_;
  primitives::refgen::RefBase *referenceGenerator_[limits::MAX_NUM_PRESSURE_CHANNELS];
  uint8_t refMode_[limits::MAX_NUM_PRESSURE_CHANNELS];

  bool moveCancelRequest_;

  primitives::mutex::Mutex lock_;
};

}  // namespace refgen
}  // namespace nasa_grub

#endif  // SRC_NASA_GRUB_INCLUDE_NASA_GRUB_REFGEN_BASE_H_
