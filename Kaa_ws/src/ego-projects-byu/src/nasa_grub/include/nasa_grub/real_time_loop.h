/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written permission
 * is obtained from COMPANY. Access to the source code contained herein is hereby
 * forbidden to anyone except current COMPANY employees, managers or contractors who
 * have executed Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 * MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE
 * OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY
 * PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE
 * RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
 * MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#ifndef SRC_NASA_GRUB_INCLUDE_NASA_GRUB_REAL_TIME_LOOP_H_
#define SRC_NASA_GRUB_INCLUDE_NASA_GRUB_REAL_TIME_LOOP_H_

#include <ros/ros.h>

#include <primitives/loop.h>
#include <primitives/time.h>
#include <primitives/thread.h>
#include <primitives/mutex.h>
#include <primitives/exception.h>

#include <ethercat_master/ethercat.h>

#include <string>
#include <map>

#include "nasa_grub/set_robot_ctrl_mode_srv.h"
#include "nasa_grub/move_joint_srv.h"
#include "nasa_grub/pressure_reference_msg.h"

#include "nasa_grub/hardware_interface.h"
#include "nasa_grub/pressure_refgen.h"
#include "nasa_grub/valve_refgen.h"

namespace nasa_grub {
namespace exception {
class RealTimeLoopException : public primitives::exception::Exception {
 public:
  explicit RealTimeLoopException(const std::string &msg)
      : primitives::exception::Exception(msg) {}

  virtual ~RealTimeLoopException() throw() {}
};
}  // namespace exception

namespace rtl {

class RealTimeLoop : public primitives::loop::Loop {
 public:
  RealTimeLoop(const std::string &name, primitives::thread::app_type_t type,
               primitives::thread::thread_prio_t prio,
               const primitives::time::Duration &period,
               ethercat::EthercatMasterLoop *ethercatLoop);

  RealTimeLoop(const std::string &name, const std::string &parentName,
               primitives::thread::app_type_t type,
               primitives::thread::thread_prio_t prio,
               const primitives::time::Duration &period,
               ethercat::EthercatMasterLoop *ethercatLoop);

  virtual ~RealTimeLoop();

  void setPressureRefCallback(const nasa_grub::pressure_reference_msg::ConstPtr &msg);

  refgen::PrsRefGen *getPrsRefGen(void) const;

  void initializeRobotState(void);
  void resetControllers(void);

  nasa_grub::robot_state_msg getRobotState(void);

  bool setRobotCtrlMode(
      nasa_grub::set_robot_ctrl_mode_srv::Request &req,  // NOLINT(runtime/references)
      nasa_grub::set_robot_ctrl_mode_srv::Response &
          res);  // NOLINT(runtime/references)

  bool movePressure(
      nasa_grub::move_joint_srv::Request &req,    // NOLINT(runtime/references)
      nasa_grub::move_joint_srv::Response &res);  // NOLINT(runtime/references)

  bool moveValve(
      nasa_grub::move_joint_srv::Request &req,    // NOLINT(runtime/references)
      nasa_grub::move_joint_srv::Response &res);  // NOLINT(runtime/references)

  void updatePoppyOutputTypes(const uint8_t output1, const uint8_t output2);

  static const std::map<int, std::string> robotControlModeNameMap_;
  static const std::map<int, std::string> valveControlModeNameMap_;

 protected:
  virtual void doInit(void);
  virtual void doIterate(void);
  virtual void doCleanup(void);

 private:
  void packValveControlModeBits(void);

  static std::map<int, std::string> robotControlModeNameMapInit(void);
  static std::map<int, std::string> valveControlModeNameMapInit(void);

  uint8_t poppyOutputType1_;
  uint8_t poppyOutputType2_;

  double pressureRef_[limits::MAX_NUM_PRESSURE_CHANNELS];
  double pressureRefDot_[limits::MAX_NUM_PRESSURE_CHANNELS];

  nasa_grub::robot_state_msg robotState_;
  primitives::mutex::Mutex robotStateLock_;

  ethercat::EthercatMasterLoop *ethercatLoop_;

  refgen::PrsRefGen *pressureRefgen_;
  refgen::ValveRefGen *valveRefgen_;

  hardware::HardwareIO *hardwareInterface_;

  uint8_t ctrlModePrev_;

  const ros::Duration ethercatStatusPeriod_;
  ros::Time ethercatStatusTime_;

  ros::Time processStartTime_;
};

}  // namespace rtl
}  // namespace nasa_grub

#endif  // SRC_NASA_GRUB_INCLUDE_NASA_GRUB_REAL_TIME_LOOP_H_
