/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written permission
 * is obtained from COMPANY. Access to the source code contained herein is hereby
 * forbidden to anyone except current COMPANY employees, managers or contractors who
 * have executed Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 * MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE
 * OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY
 * PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE
 * RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
 * MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#ifndef SRC_NASA_GRUB_INCLUDE_NASA_GRUB_LIMITS_H_
#define SRC_NASA_GRUB_INCLUDE_NASA_GRUB_LIMITS_H_

#include <primitives/utils.h>

#include "nasa_grub/hardware_interface.h"

namespace nasa_grub {
namespace limits {

static constexpr int MAX_NUM_PRESSURE_CHANNELS = 2;  //

static constexpr double MIN_VALVE_VOLTAGE = -4.8001;  //
static constexpr double MAX_VALVE_VOLTAGE = 4.8001;   //

//
static constexpr double MIN_PRESSURE_PA = 0.0;       // 0psig
static constexpr double MAX_PRESSURE_PA = 413685.0;  // 60psig;

template <typename T>
inline T valve_voltage_saturate(const T &val) {
  return primitives::utils::Saturate(val, static_cast<T>(MIN_VALVE_VOLTAGE),
                                     static_cast<T>(MAX_VALVE_VOLTAGE));
}

template <typename T>
inline T valve_voltage_in_range(const T &val) {
  return primitives::utils::InRange(val, static_cast<T>(MIN_VALVE_VOLTAGE),
                                    static_cast<T>(MAX_VALVE_VOLTAGE));
}

template <typename T>
inline T pressure_channel_in_range(const T &val) {
  return primitives::utils::InRange(val, static_cast<T>(0),
                                    static_cast<T>(MAX_NUM_PRESSURE_CHANNELS - 1));
}

template <typename T>
inline T pressure_value_saturate(const T &val) {
  return primitives::utils::Saturate(val, static_cast<T>(MIN_PRESSURE_PA),
                                     static_cast<T>(MAX_PRESSURE_PA));
}

template <typename T>
inline T pressure_value_in_range(const T &val) {
  return primitives::utils::InRange(val, static_cast<T>(MIN_PRESSURE_PA),
                                    static_cast<T>(MAX_PRESSURE_PA));
}

}  // namespace limits
}  // namespace nasa_grub

#endif  // SRC_NASA_GRUB_INCLUDE_NASA_GRUB_LIMITS_H_
