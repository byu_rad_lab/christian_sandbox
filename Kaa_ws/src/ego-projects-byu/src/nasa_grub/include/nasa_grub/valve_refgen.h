/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written permission
 * is obtained from COMPANY. Access to the source code contained herein is hereby
 * forbidden to anyone except current COMPANY employees, managers or contractors who
 * have executed Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 * MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE
 * OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY
 * PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE
 * RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
 * MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#ifndef SRC_NASA_GRUB_INCLUDE_NASA_GRUB_VALVE_REFGEN_H_
#define SRC_NASA_GRUB_INCLUDE_NASA_GRUB_VALVE_REFGEN_H_

#include <primitives/reference_generator.h>

#include <list>
#include <string>

#include "nasa_grub/refgen_base.h"
#include "nasa_grub/hardware_interface.h"
#include "nasa_grub/limits.h"

#include "nasa_grub/move_joint_srv.h"

namespace nasa_grub {
namespace refgen {

class ValveRefGen : public RefgenBase {
 public:
  ValveRefGen(const std::string &name, const std::string &parentName,
              nasa_grub::robot_state_msg *robotState);
  ~ValveRefGen();

  bool moveValve(
      nasa_grub::move_joint_srv::Request &req,    // NOLINT(runtime/references)
      nasa_grub::move_joint_srv::Response &res);  // NOLINT(runtime/references)

  bool jointMoving(void) const;

 protected:
  virtual void doInit(void);
  virtual void doIterate(void);
  virtual void doCleanup(void);

  void getNextMoveFromQueue(void);

 private:
  std::list<nasa_grub::move_joint_srv::Request> moveQueue_;
};

}  // namespace refgen
}  // namespace nasa_grub

#endif  // SRC_NASA_GRUB_INCLUDE_NASA_GRUB_VALVE_REFGEN_H_
