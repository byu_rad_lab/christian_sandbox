/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written permission
 * is obtained from COMPANY. Access to the source code contained herein is hereby
 * forbidden to anyone except current COMPANY employees, managers or contractors who
 * have executed Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 * MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE
 * OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY
 * PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE
 * RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
 * MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#ifndef SRC_NASA_GRUB_INCLUDE_NASA_GRUB_CTRL_NODE_H_
#define SRC_NASA_GRUB_INCLUDE_NASA_GRUB_CTRL_NODE_H_

#include <ethercat_master/ethercat.h>
#include <primitives/exception.h>
#include <primitives/shared_object.h>
#include <ros_utils/ros_node.h>

#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>

#include <string>

// Dynamic reconfigure services
#include "nasa_grub/ctrl_node_paramsConfig.h"
#include "nasa_grub/ethercat_loop_paramsConfig.h"
#include "nasa_grub/real_time_loop_paramsConfig.h"
#include "nasa_grub/pressure_control_paramsConfig.h"
#include "nasa_grub/sensors_paramsConfig.h"

// Other services
#include "nasa_grub/set_robot_ctrl_mode_srv.h"

// Other classes
#include "nasa_grub/real_time_loop.h"

namespace nasa_grub {
namespace exception {
class CtrlNodeException : public primitives::exception::Exception {
 public:
  explicit CtrlNodeException(const std::string &msg)
      : primitives::exception::Exception(msg) {}

  virtual ~CtrlNodeException() throw() {}
};
}  // namespace exception

namespace ctrl_node {

class CtrlNode : public ros_utils::RosNode {
 public:
  CtrlNode(const std::string &name, const std::string &parentName,
           const ros::NodeHandle &parentRosNodeHandle);
  virtual ~CtrlNode();

  static const uint8_t ETHERCAT_COMMAND_SYNC_INDEX = 0;
  static const uint16_t ETHERCAT_COMMAND_SII_ADDRESS = 0x1600;
  static const uint8_t ETHERCAT_STATUS_SYNC_INDEX = 1;
  static const uint16_t ETHERCAT_STATUS_SII_ADDRESS = 0x1a00;

 protected:
  virtual void appInit(void);
  virtual void appIterate(void);
  virtual void appCleanup(void);

 private:
  ros::NodeHandle nodeHandle_;

  void ctrlNodeDrcCallback(
      ctrl_node_paramsConfig &config,  // NOLINT(runtime/references)
      uint32_t level);
  void ethercatLoopDrcCallback(
      ethercat_loop_paramsConfig &config,  // NOLINT(runtime/references)
      uint32_t level);
  void rtlDrcCallback(
      real_time_loop_paramsConfig &config,  // NOLINT(runtime/references)
      uint32_t level);

  dynamic_reconfigure::Server<ctrl_node_paramsConfig> ctrlNodeDrcServer_;
  dynamic_reconfigure::Server<ethercat_loop_paramsConfig> ethercatLoopDrcServer_;
  dynamic_reconfigure::Server<real_time_loop_paramsConfig> rtlDrcServer_;
  dynamic_reconfigure::Server<pressure_control_paramsConfig> pressureControlDrcServer_;
  dynamic_reconfigure::Server<sensors_paramsConfig> sensorsDrcServer_;

  ros::ServiceServer setRobotCtrlModeSrv_;
  ros::ServiceServer movePressureSrv_;
  ros::ServiceServer moveValveSrv_;

  ethercat::EthercatMasterLoop *ethercatLoop_;
  rtl::RealTimeLoop *realTimeLoop_;

  ros::Publisher robotStatePublisher_;
  ros::Subscriber pressureRefSubscriber_;
};

}  // namespace ctrl_node
}  // namespace nasa_grub
#endif  // SRC_NASA_GRUB_INCLUDE_NASA_GRUB_CTRL_NODE_H_
