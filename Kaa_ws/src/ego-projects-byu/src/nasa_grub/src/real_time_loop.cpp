/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written permission
 * is obtained from COMPANY. Access to the source code contained herein is hereby
 * forbidden to anyone except current COMPANY employees, managers or contractors who
 * have executed Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 * MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE
 * OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY
 * PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE
 * RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
 * MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#include "nasa_grub/real_time_loop.h"

#include <ros_utils/ros_param.h>

#include <primitives/time.h>

#include <string>
#include <map>

#include "nasa_grub/hardware_interface.h"

namespace nasa_grub {
namespace rtl {

RealTimeLoop::RealTimeLoop(const std::string &name,
                           primitives::thread::app_type_t type,
                           primitives::thread::thread_prio_t prio,
                           const primitives::time::Duration &period,
                           ethercat::EthercatMasterLoop *ethercatLoop)
    : primitives::loop::Loop(name, type, prio, period),
      poppyOutputType1_(nasa_grub::robot_state_msg::kPoppyOutputVoltageCoil),
      poppyOutputType2_(nasa_grub::robot_state_msg::kPoppyOutputCurrentCoil),
      ethercatLoop_(ethercatLoop),
      pressureRefgen_(NULL),
      valveRefgen_(NULL),
      hardwareInterface_(NULL),
      ctrlModePrev_(nasa_grub::robot_state_msg::kRobotControlModeInit),
      ethercatStatusPeriod_(1.0),
      ethercatStatusTime_(ros::Time::now()),
      processStartTime_(ros::Time::now()) {}

RealTimeLoop::RealTimeLoop(const std::string &name, const std::string &parentName,
                           primitives::thread::app_type_t type,
                           primitives::thread::thread_prio_t prio,
                           const primitives::time::Duration &period,
                           ethercat::EthercatMasterLoop *ethercatLoop)
    : primitives::loop::Loop(name, parentName, type, prio, period),
      poppyOutputType1_(nasa_grub::robot_state_msg::kPoppyOutputVoltageCoil),
      poppyOutputType2_(nasa_grub::robot_state_msg::kPoppyOutputCurrentCoil),
      ethercatLoop_(ethercatLoop),
      pressureRefgen_(NULL),
      valveRefgen_(NULL),
      hardwareInterface_(NULL),
      ctrlModePrev_(nasa_grub::robot_state_msg::kRobotControlModeInit),
      ethercatStatusPeriod_(1.0),
      ethercatStatusTime_(ros::Time::now()),
      processStartTime_(ros::Time::now()) {}

RealTimeLoop::~RealTimeLoop() {
  if (pressureRefgen_) {
    delete pressureRefgen_;
  }
  if (hardwareInterface_) {
    delete hardwareInterface_;
  }
  if (valveRefgen_) {
    delete valveRefgen_;
  }
}

void RealTimeLoop::setPressureRefCallback(
    const nasa_grub::pressure_reference_msg::ConstPtr &msg) {
  for (int i = 0; i < limits::MAX_NUM_PRESSURE_CHANNELS; i++) {
    pressureRef_[i] = msg->reference[i];
    pressureRefDot_[i] = msg->reference_dot[i];
  }
}

refgen::PrsRefGen *RealTimeLoop::getPrsRefGen(void) const { return pressureRefgen_; }

void RealTimeLoop::initializeRobotState(void) {
  int num_slaves = robotState_.num_nodes_configured;
  std::memset(&robotState_, 0, sizeof(robotState_));

  // Pressure control specific
  std::stringstream ss;
  for (int i = 0; i < hardware::HardwareIO::kNumBellowsPerMedulla_; i++) {
    double tmp;
    ss.str("");
    ss << "/" + parent_name() + "/" + name() + "/" + "pressure_gain_" << i;
    ros_utils::RosParam::getParam(ss.str(), tmp);
    robotState_.command.pressure_gain[i] = tmp;

    ss.str("");
    ss << "/" + parent_name() + "/" + name() + "/" + "pressure_bias_" << i;
    ros_utils::RosParam::getParam(ss.str(), tmp);
    robotState_.command.pressure_bias[i] = tmp;
  }

  robotState_.poppy_output_type_1 =
      nasa_grub::robot_state_msg::kPoppyOutputVoltageCoil;
  robotState_.poppy_output_type_2 =
      nasa_grub::robot_state_msg::kPoppyOutputCurrentCoil;

  robotState_.robot_control_mode = nasa_grub::robot_state_msg::kRobotControlModeInit;
  robotState_.valve_control_mode = nasa_grub::robot_state_msg::kValveControlModeOff;

  robotState_.message_count = 0;
  robotState_.num_nodes_configured = num_slaves;
  robotState_.time_now = ros::Time::now();
  robotState_.time_since_last_iteration = ros::Duration(0.0);
  robotState_.time_used_by_real_time_loop = ros::Duration(0.0);
}

void RealTimeLoop::resetControllers(void) {
  for (int i = 0; i < hardware::HardwareIO::kNumPressureAxes_; i++) {
    robotState_.command.reference[i] = 0.0;
    robotState_.command.reference_dot[i] = 0.0;
  }
}

nasa_grub::robot_state_msg RealTimeLoop::getRobotState(void) {
  robotStateLock_.lock();
  nasa_grub::robot_state_msg robotState(robotState_);
  robotStateLock_.unlock();
  return robotState;
}

bool RealTimeLoop::setRobotCtrlMode(
    nasa_grub::set_robot_ctrl_mode_srv::Request &req,
    nasa_grub::set_robot_ctrl_mode_srv::Response &res) {
  if (RealTimeLoop::robotControlModeNameMap_.find(req.mode) ==
      RealTimeLoop::robotControlModeNameMap_.end()) {
    std::stringstream ss;
    ss << "Robot controller mode " << static_cast<int>(req.mode)
       << " is invalid. Valid modes are:" << std::endl;

    for (std::map<int, std::string>::const_iterator i =
             RealTimeLoop::robotControlModeNameMap_.begin();
         i != RealTimeLoop::robotControlModeNameMap_.end(); i++) {
      ss << i->second << "=" << i->first << std::endl;
    }

    Warn(ss.str());
    res.result = 1;
    return false;
  }

  std::stringstream ss;
  ss << "Setting robot controller mode to "
     << RealTimeLoop::robotControlModeNameMap_.find(req.mode)->second;
  Info(ss.str());

  robotState_.robot_control_mode = req.mode;
  return true;
}

bool RealTimeLoop::movePressure(nasa_grub::move_joint_srv::Request &req,
                                nasa_grub::move_joint_srv::Response &res) {
  return pressureRefgen_->movePressure(req, res);
}

bool RealTimeLoop::moveValve(nasa_grub::move_joint_srv::Request &req,
                             nasa_grub::move_joint_srv::Response &res) {
  return valveRefgen_->moveValve(req, res);
}

void RealTimeLoop::updatePoppyOutputTypes(const uint8_t output1,
                                          const uint8_t output2) {
  poppyOutputType1_ = output1;
  poppyOutputType2_ = output2;
}

void RealTimeLoop::doInit(void) {
  robotState_.num_nodes_configured = ethercatLoop_->numSlaves();
  if (robotState_.num_nodes_configured == 0) {
    throw primitives::exception::ThreadException(
        "Cannot initialize real time loop before setting number of EtherCAT nodes to "
        "> 0");
  }

  hardwareInterface_ = new hardware::HardwareIO(
      "hardware_io", parent_name() + "/" + name(), &robotState_, ethercatLoop_);
  pressureRefgen_ = new refgen::PrsRefGen("pressure_refgen",
                                          parent_name() + "/" + name(), &robotState_);
  valveRefgen_ = new refgen::ValveRefGen("valve_refgen", parent_name() + "/" + name(),
                                         &robotState_);
  initializeRobotState();

  pressureRefgen_->init();
  valveRefgen_->init();

  processStartTime_ = ros::Time::now();

  for (int i = 0; i < limits::MAX_NUM_PRESSURE_CHANNELS; i++) {
    pressureRef_[i] = 0.0;
    pressureRefDot_[i] = 0.0;
  }
}

void RealTimeLoop::doIterate(void) {
  bool runSlowUpdate = false;
  robotStateLock_.lock();

  /*
   * Update Time
   */
  ros::Time loopStartTime = ros::Time::now();
  robotState_.time_since_last_iteration = loopStartTime - robotState_.time_now;
  robotState_.time_now = loopStartTime;
  robotState_.message_count = loopCnt_;

  robotState_.poppy_output_type_1 = poppyOutputType1_;
  robotState_.poppy_output_type_2 = poppyOutputType2_;

  if ((robotState_.time_now - ethercatStatusTime_) >= ethercatStatusPeriod_) {
    robotState_.num_nodes_responding = ethercatLoop_->numSlavesResponding();
    if (robotState_.num_nodes_responding != robotState_.num_nodes_configured) {
      ROS_ERROR("Number of EtherCAT nodes has changed! Used to be %d, now %d",
                robotState_.num_nodes_configured, robotState_.num_nodes_responding);
    }
    ethercatStatusTime_ = robotState_.time_now;
    runSlowUpdate = true;
  }

  /*
   * Do all tasks
   */
  hardwareInterface_->read();

  bool controlModeChange = false;
  if (ctrlModePrev_ != robotState_.robot_control_mode) {
    controlModeChange = true;
  }

  ctrlModePrev_ = robotState_.robot_control_mode;

  ros::Duration timeSinceStart = ros::Time::now() - processStartTime_;

  //  pressureRefgen_->iterate();
  //  valveRefgen_->iterate();

  switch (robotState_.robot_control_mode) {
    case nasa_grub::robot_state_msg::kRobotControlModeInit:
      if (timeSinceStart > ros::Duration(3.0)) {
        robotState_.robot_control_mode =
            nasa_grub::robot_state_msg::kRobotControlModeOff;
      }
      break;

    case nasa_grub::robot_state_msg::kRobotControlModeOff:
      robotState_.valve_control_mode = robotState_.kValveControlModeOff;
      if (controlModeChange) {
        resetControllers();
      }
      break;

    case nasa_grub::robot_state_msg::kRobotControlModeValve:
      robotState_.valve_control_mode = robotState_.kRobotControlModeValve;
      if (controlModeChange) {
        resetControllers();
      }
      valveRefgen_->iterate();
      for (int i = 0; i < limits::MAX_NUM_PRESSURE_CHANNELS; i++) {
        robotState_.command.reference[i] = robotState_.valve_control.reference[i];
        robotState_.command.reference_dot[i] = 0.0;
      }
      break;

    case nasa_grub::robot_state_msg::kRobotControlModePressure:
      robotState_.valve_control_mode = robotState_.kValveControlModePressure;
      if (controlModeChange) {
        resetControllers();
        for (int i = 0; i < limits::MAX_NUM_PRESSURE_CHANNELS; i++) {
          robotState_.pressure_control.reference[i] = robotState_.status.ana[i];
          robotState_.pressure_control.reference_dot[i] =
              robotState_.status.ana_dot[i];
        }
      }
      pressureRefgen_->iterate();
      for (int i = 0; i < limits::MAX_NUM_PRESSURE_CHANNELS; i++) {
        robotState_.command.reference[i] = robotState_.pressure_control.reference[i];
        robotState_.command.reference_dot[i] =
            robotState_.pressure_control.reference_dot[i];
      }
      break;

    case nasa_grub::robot_state_msg::kRobotControlModePressureSubscriber:
      robotState_.valve_control_mode = robotState_.kValveControlModePressure;
      if (controlModeChange) {
        resetControllers();
        for (int i = 0; i < limits::MAX_NUM_PRESSURE_CHANNELS; i++) {
          robotState_.pressure_control.reference[i] = robotState_.status.ana[i];
          robotState_.pressure_control.reference_dot[i] =
              robotState_.status.ana_dot[i];

          pressureRef_[i] = robotState_.status.ana[i];
          pressureRefDot_[i] = robotState_.status.ana_dot[i];
        }
      }
      for (int i = 0; i < limits::MAX_NUM_PRESSURE_CHANNELS; i++) {
        robotState_.pressure_control.reference[i] = pressureRef_[i];
        robotState_.pressure_control.reference_dot[i] = pressureRefDot_[i];

        robotState_.command.reference[i] = pressureRef_[i];
        robotState_.command.reference_dot[i] = pressureRefDot_[i];
      }
      break;

    default:
      throw exception::RealTimeLoopException(
          "Robot control mode set to invalid value!");
      break;
  }

  packValveControlModeBits();
  robotState_.command.rtpc_counter = loopCnt_;
  hardwareInterface_->write();

  robotState_.time_used_by_real_time_loop = ros::Time::now() - loopStartTime;
  robotStateLock_.unlock();
}

void RealTimeLoop::packValveControlModeBits(void) {
  robotState_.command.mode = 0x00000000;
  robotState_.command.mode |= (robotState_.valve_control_mode << 0) & 0x00000FFF;
  robotState_.command.mode |= (robotState_.poppy_output_type_1 << 16) & 0x00FF0000;
  robotState_.command.mode |= (robotState_.poppy_output_type_2 << 24) & 0xFF000000;

  robotState_.command.rtpc_counter = robotState_.message_count;
}

void RealTimeLoop::doCleanup(void) {
  pressureRefgen_->cleanup();
  valveRefgen_->cleanup();
  hardwareInterface_->write();
}

const std::map<int, std::string> RealTimeLoop::robotControlModeNameMap_ =
    RealTimeLoop::robotControlModeNameMapInit();

std::map<int, std::string> RealTimeLoop::robotControlModeNameMapInit(void) {
  std::map<int, std::string> tmp;
  tmp[nasa_grub::robot_state_msg::kRobotControlModeInit] = "kRobotControlModeInit";
  tmp[nasa_grub::robot_state_msg::kRobotControlModeOff] = "kRobotControlModeOff";
  tmp[nasa_grub::robot_state_msg::kRobotControlModePressure] =
      "kRobotControlModePressure";
  tmp[nasa_grub::robot_state_msg::kRobotControlModeValve] = "kRobotControlModeValve";
  tmp[nasa_grub::robot_state_msg::kRobotControlModePressureSubscriber] =
      "kRobotControlModePressureSubscriber";
  return tmp;
}

const std::map<int, std::string> RealTimeLoop::valveControlModeNameMap_ =
    RealTimeLoop::valveControlModeNameMapInit();

std::map<int, std::string> RealTimeLoop::valveControlModeNameMapInit(void) {
  std::map<int, std::string> tmp;
  tmp[nasa_grub::robot_state_msg::kValveControlModeOff] = "kValveControlModeOff";
  tmp[nasa_grub::robot_state_msg::kValveControlModeCurrent] =
      "kValveControlModeCurrent";
  tmp[nasa_grub::robot_state_msg::kValveControlModeVoltage] =
      "kValveControlModeVoltage";
  tmp[nasa_grub::robot_state_msg::kValveControlModeSafe] = "kValveControlModeSafe";
  tmp[nasa_grub::robot_state_msg::kValveControlModePressure] =
      "kValveControlModePressure";
  return tmp;
}

}  // namespace rtl
}  // namespace nasa_grub
