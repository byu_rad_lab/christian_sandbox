/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written permission
 * is obtained from COMPANY. Access to the source code contained herein is hereby
 * forbidden to anyone except current COMPANY employees, managers or contractors who
 * have executed Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 * MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE
 * OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY
 * PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE
 * RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
 * MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#include "nasa_grub/pressure_refgen.h"

#include <ros_utils/ros_param.h>

#include <string>

#include "nasa_grub/hardware_interface.h"
#include "nasa_grub/limits.h"

namespace nasa_grub {
namespace refgen {

PrsRefGen::PrsRefGen(const std::string &name, const std::string &parentName,
                     nasa_grub::robot_state_msg *robotState)
    : RefgenBase(name, parentName, robotState) {
  for (int axisIndex = 0; axisIndex < limits::MAX_NUM_PRESSURE_CHANNELS; axisIndex++) {
    referenceGenerator_[axisIndex] = NULL;
    refMode_[axisIndex] = nasa_grub::robot_state_msg::kMoveRefTypeStop;
  }
}

PrsRefGen::~PrsRefGen() {}

void PrsRefGen::gainsDrcCallback(pressure_control_paramsConfig &config,
                                 uint32_t level) {
  ROS_INFO("Updating pressure control gains dynamic reconfigure parameters");
  robotState_->command.kp[0] = config.k_p_0;
  robotState_->command.ki[0] = config.k_i_0;
  robotState_->command.ki_limit[0] = config.k_i_limit_0;
  robotState_->command.kd[0] = config.k_d_0;
  robotState_->command.voltage_feedforward[0] = config.v_ff_0;
  robotState_->command.voltage_zero[0] = config.v_zero_0;

  robotState_->command.kp[1] = config.k_p_1;
  robotState_->command.ki[1] = config.k_i_1;
  robotState_->command.ki_limit[1] = config.k_i_limit_1;
  robotState_->command.kd[1] = config.k_d_1;
  robotState_->command.voltage_feedforward[1] = config.v_ff_1;
  robotState_->command.voltage_zero[1] = config.v_zero_1;

  robotState_->command.voltage_max = config.v_max;
}

void PrsRefGen::sensorsDrcCallback(sensors_paramsConfig &config, uint32_t level) {
  ROS_INFO("Updating pressure sensors dynamic reconfigure parameters");

  robotState_->command.pressure_bias[0] = config.bias_0;
  robotState_->command.pressure_bias[1] = config.bias_1;

  robotState_->command.pressure_gain[0] = config.gain_0;
  robotState_->command.pressure_gain[1] = config.gain_1;
}

bool PrsRefGen::movePressure(nasa_grub::move_joint_srv::Request &req,
                             nasa_grub::move_joint_srv::Response &res) {
  std::stringstream ss;
  ss << std::endl;
  ss << "Moving Pressure Reference" << std::endl;
  ss << "--------------------" << std::endl;
  ss << "Target Pressure 0    : " << req.target_value[0] << std::endl;
  ss << "Target Pressure 1    : " << req.target_value[1] << std::endl;
  ss << "Target Pressure Dot 0: " << req.target_value_dot[0] << std::endl;
  ss << "Target Pressure Dot 1: " << req.target_value_dot[1] << std::endl;
  ss << "Move Time            : " << req.move_time << std::endl;
  ss << "Move Type            : " << req.move_type << std::endl;
  Info(ss.str());

  for (int i = 0; i < limits::MAX_NUM_PRESSURE_CHANNELS; i++) {
    if (!limits::pressure_value_in_range(static_cast<double>(req.target_value[i]))) {
      ROS_WARN("Target pressure %d out of range!", i);
      return false;
    }
  }

  lock_.lock();
  if (req.move_type == nasa_grub::robot_state_msg::kMoveRefTypeStop) {
    moveQueue_.clear();
    moveCancelRequest_ = true;
    refMode_[0] = nasa_grub::robot_state_msg::kMoveRefTypeStop;
    refMode_[1] = nasa_grub::robot_state_msg::kMoveRefTypeStop;
  } else {
    moveQueue_.push_back(req);
  }
  lock_.unlock();
  return true;
}

bool PrsRefGen::jointMoving(void) const {
  for (int channelIndex = 0; channelIndex < limits::MAX_NUM_PRESSURE_CHANNELS;
       channelIndex++) {
    if (refMode_[channelIndex] != nasa_grub::robot_state_msg::kMoveRefTypeStop) {
      return true;
    }
  }
  return false;
}

void PrsRefGen::getNextMoveFromQueue(void) {
  lock_.lock();
  if (!moveQueue_.empty()) {
    nasa_grub::move_joint_srv::Request move = moveQueue_.front();
    moveQueue_.pop_front();
    lock_.unlock();

    for (int axisIndex = 0; axisIndex < limits::MAX_NUM_PRESSURE_CHANNELS;
         axisIndex++) {
      primitives::refgen::RefState stateInitial;
      primitives::refgen::RefState stateFinal;

      stateInitial.pos = robotState_->pressure_control.reference[axisIndex];
      stateInitial.vel = robotState_->pressure_control.reference_dot[axisIndex];
      stateInitial.time = robotState_->time_now.toSec();

      stateFinal.pos = move.target_value[axisIndex];
      stateFinal.vel = move.target_value_dot[axisIndex];
      stateFinal.time = stateInitial.time + move.move_time;

      switch (move.move_type) {
        case nasa_grub::robot_state_msg::kMoveRefTypeStop:
          refMode_[axisIndex] = nasa_grub::robot_state_msg::kMoveRefTypeStop;
          break;

        case nasa_grub::robot_state_msg::kMoveRefTypeStep:
          if (referenceGenerator_[axisIndex]) {
            delete referenceGenerator_[axisIndex];
          }
          referenceGenerator_[axisIndex] =
              new primitives::refgen::StepRef(stateInitial, stateFinal);
          refMode_[axisIndex] = nasa_grub::robot_state_msg::kMoveRefTypeStep;
          break;

        case nasa_grub::robot_state_msg::kMoveRefTypeSin:
          if (referenceGenerator_[axisIndex]) {
            delete referenceGenerator_[axisIndex];
          }
          referenceGenerator_[axisIndex] =
              new primitives::refgen::SinRef(stateInitial, stateFinal);
          refMode_[axisIndex] = nasa_grub::robot_state_msg::kMoveRefTypeSin;
          break;

        case nasa_grub::robot_state_msg::kMoveRefTypeChirpTest:
        default:
          ROS_ERROR("Invalid move type %d! PrsRefGen::moveAxisSrvCallback()",
                    move.move_type);
          break;
      }
    }
  } else {
    lock_.unlock();
  }
}

void PrsRefGen::doInit(void) {}

void PrsRefGen::doIterate(void) {
  if (!jointMoving()) {
    getNextMoveFromQueue();
  }
  for (int axisIndex = 0; axisIndex < limits::MAX_NUM_PRESSURE_CHANNELS; axisIndex++) {
    robotState_->pressure_control.actual[axisIndex] =
        robotState_->status.ana[axisIndex];
    robotState_->pressure_control.actual_dot[axisIndex] =
        robotState_->status.ana_dot[axisIndex];

    switch (refMode_[axisIndex]) {
      case nasa_grub::robot_state_msg::kMoveRefTypeStop:
        // Do nothing
        break;

      case nasa_grub::robot_state_msg::kMoveRefTypeStep:
      case nasa_grub::robot_state_msg::kMoveRefTypeSin:
      case nasa_grub::robot_state_msg::kMoveRefTypeCubicSpline:
      case nasa_grub::robot_state_msg::kMoveRefTypeChirpTest:
        if (!referenceGenerator_[axisIndex]->initialized()) {
          referenceGenerator_[axisIndex]->init(robotState_->time_now.toSec());
        } else {
          primitives::refgen::RefState tmp =
              referenceGenerator_[axisIndex]->iterate(robotState_->time_now.toSec());
          robotState_->pressure_control.reference[axisIndex] = tmp.pos;
          robotState_->pressure_control.reference_dot[axisIndex] = tmp.vel;
        }
        if (referenceGenerator_[axisIndex]->done()) {
          refMode_[axisIndex] = nasa_grub::robot_state_msg::kMoveRefTypeStop;
        }
        break;

      default:
        ROS_ERROR("Invalid reference mode (%d)! PrsRefGen::doIterate()",
                  refMode_[axisIndex]);
        break;
    }
  }
}

void PrsRefGen::doCleanup(void) {}

}  // namespace refgen
}  // namespace nasa_grub
