/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written permission
 * is obtained from COMPANY. Access to the source code contained herein is hereby
 * forbidden to anyone except current COMPANY employees, managers or contractors who
 * have executed Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 * MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE
 * OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY
 * PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE
 * RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
 * MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#include "nasa_grub/ctrl_node.h"

#include <ros_utils/ros_param.h>

#include <string>
#include <vector>

#include "nasa_grub/node_status_msg.h"
#include "nasa_grub/node_command_msg.h"

namespace nasa_grub {
namespace ctrl_node {

CtrlNode::CtrlNode(const std::string &name, const std::string &parentName,
                   const ros::NodeHandle &parentRosNodeHandle)
    : ros_utils::RosNode(name, parentName, ros::Rate(1.0), parentRosNodeHandle),
      ctrlNodeDrcServer_(ros::NodeHandle(parentRosNodeHandle, "loops/ros_publisher")),
      ethercatLoopDrcServer_(ros::NodeHandle(parentRosNodeHandle, "loops/ethercat")),
      rtlDrcServer_(ros::NodeHandle(parentRosNodeHandle, "loops/real_time")),
      pressureControlDrcServer_(
          ros::NodeHandle(parentRosNodeHandle, "pressure_control")),
      sensorsDrcServer_(ros::NodeHandle(parentRosNodeHandle, "sensors")),
      ethercatLoop_(NULL),
      realTimeLoop_(NULL) {}

CtrlNode::~CtrlNode() {
  if (ethercatLoop_) {
    delete ethercatLoop_;
  }
  if (realTimeLoop_) {
    delete realTimeLoop_;
  }
}

void CtrlNode::appInit(void) {
  double rate;
  int queueSize;
  int ethercatLoopPrio;
  int ethercatLoopAppType;
  double ethercatLoopRate;
  int nodeVendorId;
  int nodeSerialNumber;

  int rtlPrio;
  int rtlAppType;
  double rtlRate;

  /*
   * Load params
   */
  ROS_INFO("Reading parameters from server");
  ros_utils::RosParam::getParam("/" + parent_name() + "/" + name() + "/rate_hz", rate);
  loopRate_ = ros::Rate(rate);
  ros_utils::RosParam::getParam("/" + parent_name() + "/" + name() + "/queue_size",
                                queueSize);

  ros_utils::RosParam::getParam(
      "/" + parent_name() + "/" + name() + "/ethercat_master_loop/thread_priority",
      ethercatLoopPrio);
  ros_utils::RosParam::getParam(
      "/" + parent_name() + "/" + name() + "/ethercat_master_loop/app_type",
      ethercatLoopAppType);
  ros_utils::RosParam::getParam(
      "/" + parent_name() + "/" + name() + "/ethercat_master_loop/rate_hz",
      ethercatLoopRate);

  ros_utils::RosParam::getParam(
      "/" + parent_name() + "/" + name() + "/ethercat_master_loop/node/vendor_id",
      nodeVendorId);
  ros_utils::RosParam::getParam(
      "/" + parent_name() + "/" + name() + "/ethercat_master_loop/node/serial_number",
      nodeSerialNumber);

  ros_utils::RosParam::getParam(
      "/" + parent_name() + "/" + name() + "/" + "real_time_loop" + "/thread_priority",
      rtlPrio);
  ros_utils::RosParam::getParam(
      "/" + parent_name() + "/" + name() + "/" + "real_time_loop" + "/app_type",
      rtlAppType);
  ros_utils::RosParam::getParam(
      "/" + parent_name() + "/" + name() + "/" + "real_time_loop" + "/rate_hz",
      rtlRate);
  ROS_INFO("Done reading parameters from server");

  /*
   * Create EtherCAT master process
   */
  ROS_INFO("Creating EtherCAT loop with app type=%d, priority=%d, rate=%fHz",
           ethercatLoopAppType, ethercatLoopPrio, ethercatLoopRate);
  ethercatLoop_ = new ethercat::EthercatMasterLoop(
      "ethercat_master_loop", (primitives::thread::app_type_t)ethercatLoopAppType,
      ethercatLoopPrio, ros::Duration(1.0 / ethercatLoopRate));
  ROS_INFO("Done creating EtherCAT loop");

  ROS_INFO("Configuring EtherCAT nodes");
  ethercatLoop_->printNetworkNodeInformation();
  std::vector<ec_slave_info_t> info = ethercatLoop_->getNetworkNodeInformation();
  if (info.size() == 0) {
    throw exception::CtrlNodeException("No EtherCAT slaves detected");
  }

  bool nodeFound = false;
  for (unsigned int i = 0; i < info.size(); i++) {
    if (info[i].vendor_id == nodeVendorId) {
      if (info[i].serial_number == nodeSerialNumber) {
        if (!nodeFound) {
          ethercatLoop_->addNodeSyncConfiguration(ETHERCAT_COMMAND_SYNC_INDEX,
                                                  sizeof(nasa_grub::node_command_msg),
                                                  EC_DIR_OUTPUT,
                                                  ETHERCAT_COMMAND_SII_ADDRESS)
              ->addNodeSyncConfiguration(ETHERCAT_STATUS_SYNC_INDEX,
                                         sizeof(nasa_grub::node_status_msg),
                                         EC_DIR_INPUT, ETHERCAT_STATUS_SII_ADDRESS)
              ->processNodeSyncConfigurations(info[i].alias, info[i].position,
                                              info[i].vendor_id, info[i].product_code);
          ROS_INFO("Adding node %d", i);
          nodeFound = true;
        }
      } else {
        ROS_WARN("Ignoring node %d. Serial number not recognized. ", i);
      }
    } else {
      ROS_WARN("Ignoring node %d. Product ID number not recognized. ", i);
    }
  }
  if (!nodeFound) {
    throw exception::CtrlNodeException("Failed to find expected EtherCAT node");
  }

  ROS_INFO("Done configuring EtherCAT nodes");

  ROS_INFO("Starting EtherCAT loop");
  ethercatLoop_->start();
  ROS_INFO("Done starting EtherCAT loop");

  if (ethercatLoop_->numSlaves() == 0) {
    throw exception::CtrlNodeException("No EtherCAT slaves detected");
  }

  /*
   * Create the real-time loop
   */
  ROS_INFO("Creating real-time loop with name=%s, app type=%d, priority=%d, rate=%fHz",
           "real_time_loop", rtlAppType, rtlPrio, rtlRate);
  realTimeLoop_ =
      new rtl::RealTimeLoop("real_time_loop", parent_name() + "/" + name(),
                            (primitives::thread::app_type_t)rtlAppType, rtlPrio,
                            primitives::time::Duration(1.0 / rtlRate), ethercatLoop_);
  ROS_INFO("Done creating real-time loop");

  ROS_INFO("Starting real-time loop");
  realTimeLoop_->start();
  ROS_INFO("Done starting real-time loop");

  ROS_INFO("Setting up services");
  ros::NodeHandle n;

  setRobotCtrlModeSrv_ = n.advertiseService(
      "SetRobotCtrlMode", &rtl::RealTimeLoop::setRobotCtrlMode, realTimeLoop_);

  movePressureSrv_ = n.advertiseService(
      "MovePressure", &rtl::RealTimeLoop::movePressure, realTimeLoop_);

  moveValveSrv_ =
      n.advertiseService("MoveValve", &rtl::RealTimeLoop::moveValve, realTimeLoop_);

  ROS_INFO("Done setting up services");

  ROS_INFO("Setting up publishers");
  robotStatePublisher_ =
      n.advertise<nasa_grub::robot_state_msg>("robot_state", queueSize);
  ROS_INFO("Done setting up publishers");

  ROS_INFO("Setting up subscribers");
  pressureRefSubscriber_ =
      n.subscribe("SetPressureRef", queueSize,
                  &rtl::RealTimeLoop::setPressureRefCallback, realTimeLoop_);
  ROS_INFO("Done setting up subscribers");

  /*
   * Setup the dynamic reconfigure servers
   */
  ROS_INFO("Setting up the dynamic reconfigure servers");
  dynamic_reconfigure::Server<ctrl_node_paramsConfig>::CallbackType cb0 =
      boost::bind(&CtrlNode::ctrlNodeDrcCallback, this, _1, _2);
  ctrlNodeDrcServer_.setCallback(cb0);

  dynamic_reconfigure::Server<ethercat_loop_paramsConfig>::CallbackType cb1 =
      boost::bind(&CtrlNode::ethercatLoopDrcCallback, this, _1, _2);
  ethercatLoopDrcServer_.setCallback(cb1);

  dynamic_reconfigure::Server<real_time_loop_paramsConfig>::CallbackType cb2 =
      boost::bind(&CtrlNode::rtlDrcCallback, this, _1, _2);
  rtlDrcServer_.setCallback(cb2);

  dynamic_reconfigure::Server<pressure_control_paramsConfig>::CallbackType cb3 =
      boost::bind(&refgen::PrsRefGen::gainsDrcCallback, realTimeLoop_->getPrsRefGen(),
                  _1, _2);
  pressureControlDrcServer_.setCallback(cb3);

  dynamic_reconfigure::Server<sensors_paramsConfig>::CallbackType cb4 = boost::bind(
      &refgen::PrsRefGen::sensorsDrcCallback, realTimeLoop_->getPrsRefGen(), _1, _2);
  sensorsDrcServer_.setCallback(cb4);

  ROS_INFO("Done setting up the dynamic reconfigure servers");
}

void CtrlNode::appIterate(void) {
  /*
   * Publish ROS topics
   */
  nasa_grub::robot_state_msg robotState = realTimeLoop_->getRobotState();
  robotStatePublisher_.publish(robotState);
}

void CtrlNode::appCleanup(void) {
  ROS_INFO("Stopping real-time loop");
  //    realTimeLoop_->stop();
  ROS_INFO("Done stopping real-time loop");

  // Sleep before shutting down ethercat to make sure all the final commands get to the
  // robotState
  robotStatePublisher_.shutdown();
  ros::Duration(0.25).sleep();
  ROS_INFO("Stopping EtherCAT loop");
  ethercatLoop_->stop();
  ROS_INFO("Done stopping EtherCAT loop");
}

void CtrlNode::ctrlNodeDrcCallback(ctrl_node_paramsConfig &config, uint32_t level) {
  ROS_INFO("Updating control node dynamic reconfigure parameters");
  loopRate_ = ros::Rate(config.rate_hz);
  realTimeLoop_->updatePoppyOutputTypes((uint8_t)config.poppy_output_1,
                                        (uint8_t)config.poppy_output_2);
}

void CtrlNode::ethercatLoopDrcCallback(ethercat_loop_paramsConfig &config,
                                       uint32_t level) {
  ROS_INFO("Updating ethercat master dynamic reconfigure parameters");
  ethercatLoop_->setLoopPeriod(primitives::time::Duration(1.0 / config.rate_hz));
}

void CtrlNode::rtlDrcCallback(real_time_loop_paramsConfig &config, uint32_t level) {
  ROS_INFO("Updating real-time loop dynamic reconfigure parameters");
  realTimeLoop_->setLoopPeriod(primitives::time::Duration(1.0 / config.rate_hz));
}

}  // namespace ctrl_node
}  // namespace nasa_grub
