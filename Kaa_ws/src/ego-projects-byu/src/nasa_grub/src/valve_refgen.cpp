/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written permission
 * is obtained from COMPANY. Access to the source code contained herein is hereby
 * forbidden to anyone except current COMPANY employees, managers or contractors who
 * have executed Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 * MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE
 * OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY
 * PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE
 * RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
 * MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#include "nasa_grub/valve_refgen.h"

#include <ros_utils/ros_param.h>

#include <string>

#include "nasa_grub/hardware_interface.h"
#include "nasa_grub/limits.h"

namespace nasa_grub {
namespace refgen {

ValveRefGen::ValveRefGen(const std::string &name, const std::string &parentName,
                         nasa_grub::robot_state_msg *robotState)
    : RefgenBase(name, parentName, robotState) {
  for (int axisIndex = 0; axisIndex < limits::MAX_NUM_PRESSURE_CHANNELS; axisIndex++) {
    referenceGenerator_[axisIndex] = NULL;
    refMode_[axisIndex] = nasa_grub::robot_state_msg::kMoveRefTypeStop;
  }
}

ValveRefGen::~ValveRefGen() {}

bool ValveRefGen::moveValve(nasa_grub::move_joint_srv::Request &req,
                            nasa_grub::move_joint_srv::Response &res) {
  std::stringstream ss;
  ss << std::endl;
  ss << "Moving Valve Reference" << std::endl;
  ss << "--------------------" << std::endl;
  ss << "Target Voltage 0 : " << req.target_value[0] << std::endl;
  ss << "Target Voltage 1 : " << req.target_value[1] << std::endl;
  ss << "Move Time        : " << req.move_time << std::endl;
  ss << "Move Type        : " << req.move_type << std::endl;
  Info(ss.str());

  for (int i = 0; i < limits::MAX_NUM_PRESSURE_CHANNELS; i++) {
    if (!limits::valve_voltage_in_range(static_cast<double>(req.target_value[i]))) {
      ROS_WARN("Target voltage %d out of range!", i);
      return false;
    }
  }

  lock_.lock();
  if (req.move_type == nasa_grub::robot_state_msg::kMoveRefTypeStop) {
    moveQueue_.clear();
    moveCancelRequest_ = true;
    refMode_[0] = nasa_grub::robot_state_msg::kMoveRefTypeStop;
    refMode_[1] = nasa_grub::robot_state_msg::kMoveRefTypeStop;
  } else {
    moveQueue_.push_back(req);
  }
  lock_.unlock();
  return true;
}

bool ValveRefGen::jointMoving(void) const {
  for (int channelIndex = 0; channelIndex < limits::MAX_NUM_PRESSURE_CHANNELS;
       channelIndex++) {
    if (refMode_[channelIndex] != nasa_grub::robot_state_msg::kMoveRefTypeStop) {
      return true;
    }
  }
  return false;
}

void ValveRefGen::getNextMoveFromQueue(void) {
  lock_.lock();
  if (!moveQueue_.empty()) {
    nasa_grub::move_joint_srv::Request move = moveQueue_.front();
    moveQueue_.pop_front();
    lock_.unlock();

    for (int axisIndex = 0; axisIndex < limits::MAX_NUM_PRESSURE_CHANNELS;
         axisIndex++) {
      primitives::refgen::RefState stateInitial;
      primitives::refgen::RefState stateFinal;

      stateInitial.pos = robotState_->valve_control.reference[axisIndex];
      stateInitial.time = robotState_->time_now.toSec();

      stateFinal.pos = move.target_value[axisIndex];
      stateFinal.time = stateInitial.time + move.move_time;

      switch (move.move_type) {
        case nasa_grub::robot_state_msg::kMoveRefTypeStop:
          refMode_[axisIndex] = nasa_grub::robot_state_msg::kMoveRefTypeStop;
          break;

        case nasa_grub::robot_state_msg::kMoveRefTypeStep:
          if (referenceGenerator_[axisIndex]) {
            delete referenceGenerator_[axisIndex];
          }
          referenceGenerator_[axisIndex] =
              new primitives::refgen::StepRef(stateInitial, stateFinal);
          refMode_[axisIndex] = nasa_grub::robot_state_msg::kMoveRefTypeStep;
          break;

        case nasa_grub::robot_state_msg::kMoveRefTypeSin:
          if (referenceGenerator_[axisIndex]) {
            delete referenceGenerator_[axisIndex];
          }
          referenceGenerator_[axisIndex] =
              new primitives::refgen::SinRef(stateInitial, stateFinal);
          refMode_[axisIndex] = nasa_grub::robot_state_msg::kMoveRefTypeSin;
          break;

        case nasa_grub::robot_state_msg::kMoveRefTypeChirpTest:
        default:
          ROS_ERROR("Invalid move type %d! ValveRefGen::moveAxisSrvCallback()",
                    move.move_type);
          break;
      }
    }
  } else {
    lock_.unlock();
  }
}

void ValveRefGen::doInit(void) {}

void ValveRefGen::doIterate(void) {
  if (!jointMoving()) {
    getNextMoveFromQueue();
  }
  for (int axisIndex = 0; axisIndex < limits::MAX_NUM_PRESSURE_CHANNELS; axisIndex++) {
    switch (refMode_[axisIndex]) {
      case nasa_grub::robot_state_msg::kMoveRefTypeStop:
        // Do nothing
        break;

      case nasa_grub::robot_state_msg::kMoveRefTypeStep:
      case nasa_grub::robot_state_msg::kMoveRefTypeSin:
      case nasa_grub::robot_state_msg::kMoveRefTypeCubicSpline:
      case nasa_grub::robot_state_msg::kMoveRefTypeChirpTest:
        if (!referenceGenerator_[axisIndex]->initialized()) {
          referenceGenerator_[axisIndex]->init(robotState_->time_now.toSec());
        } else {
          primitives::refgen::RefState tmp =
              referenceGenerator_[axisIndex]->iterate(robotState_->time_now.toSec());
          robotState_->valve_control.reference[axisIndex] = tmp.pos;
        }
        if (referenceGenerator_[axisIndex]->done()) {
          refMode_[axisIndex] = nasa_grub::robot_state_msg::kMoveRefTypeStop;
        }
        break;

      default:
        ROS_ERROR("Invalid reference mode (%d)! ValveRefGen::doIterate()",
                  refMode_[axisIndex]);
        break;
    }
  }
}

void ValveRefGen::doCleanup(void) {}

}  // namespace refgen
}  // namespace nasa_grub
