/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written permission
 * is obtained from COMPANY. Access to the source code contained herein is hereby
 * forbidden to anyone except current COMPANY employees, managers or contractors who
 * have executed Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 * MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE
 * OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY
 * PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE
 * RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
 * MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#include <ros/ros.h>
#include <primitives/loop.h>

#include <string>

#include "nasa_grub/ctrl_node.h"

/*
 * Main entry point for the application
 */
int main(int argc, char **argv) {
  static const std::string name("nasa_grub");
  static const std::string ctrlName("control_node");

  /*
   * Initialize the ros node
   */
  ros::init(argc, argv, name, ros::init_options::NoSigintHandler);
  ros::NodeHandle n;

  /*
   * Create a nasa_grub node instance and run it
   */
  nasa_grub::ctrl_node::CtrlNode ctrlNode(ctrlName, name, n);
  ctrlNode.run();

  ROS_INFO("Exiting main...");
  return 0;
}
