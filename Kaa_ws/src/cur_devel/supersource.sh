cd control
source devel/setup.bash --extend
cd ..
cd estimation
source devel/setup.bash --extend
cd ..
cd experiments
source devel/setup.bash --extend
cd ..
cd models
source devel/setup.bash --extend
cd ..
cd robots
source devel/setup.bash --extend
cd ..
cd sensors
source devel/setup.bash --extend
cd ..
cd third_party
source devel/setup.bash --extend
cd ..
cd utils
source devel/setup.bash --extend
cd ..
