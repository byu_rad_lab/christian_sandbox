#!/usr/bin/env python
import copy
from geometry_msgs.msg import Wrench
from geometry_msgs.msg import WrenchStamped
import rospy
from subscribers import wrench_stamped_subscriber as w_sub
import time

if __name__ == "__main__":

    rospy.init_node('Force_Setter', anonymous=True)

    force_sub = w_sub.WrenchStampedSubscriber('/ftnode1/netft_data')
    pub = rospy.Publisher('/force_cmd',WrenchStamped)

    z = -24.0
    z_prev = z
    i = 0


    mew = 0.184
    F = mew*abs(z)

    rate = rospy.Rate(0.0333333)
    time.sleep(0.5)

    print("Commanding initial force")
    wrench_cmd_msg = WrenchStamped()
    #normal tests
    wrench_cmd_msg.wrench.force.x = 0.0
    wrench_cmd_msg.wrench.force.y = 0.0

    #front two legs
    #robot 66
   #wrench_cmd_msg.wrench.force.x = F*-0.73522597
   #wrench_cmd_msg.wrench.force.y = F*0.67782208
    #robot 4
   #wrench_cmd_msg.wrench.force.x = F*-0.78
   #wrench_cmd_msg.wrench.force.y = F*0.626

    #back two legs
    #robot 66
   #wrench_cmd_msg.wrench.force.x = F*0.73522597
   #wrench_cmd_msg.wrench.force.y = F*-0.67782208
    #robot 4
   #wrench_cmd_msg.wrench.force.x = F*0.78
   #wrench_cmd_msg.wrench.force.y = F*-0.626

    wrench_cmd_msg.wrench.force.z = z
    wrench_cmd_msg.wrench.torque.x = 0.0
    wrench_cmd_msg.wrench.torque.y = 0.0
    wrench_cmd_msg.wrench.torque.z = 0.0
    wrench_cmd_msg.header.stamp = rospy.Time.now()
    wrench_cmd_msg.header.frame_id = "base_link"
    pub.publish(wrench_cmd_msg)

    input("Check that it reached the desired starting point.  Take a picture and start video, then press enter.")
    z -= 10.0


    while not rospy.is_shutdown():

        force_sub.getData()
        f = force_sub.wrench.force
        
        rospy.sleep(20)

        z_prev = z
        z -= 10.0

        wrench_cmd_msg = WrenchStamped()
        wrench_cmd_msg.wrench.force.x = 0.0
        wrench_cmd_msg.wrench.force.y = 0.0
        wrench_cmd_msg.wrench.force.z = z
        wrench_cmd_msg.wrench.torque.x = 0.0
        wrench_cmd_msg.wrench.torque.y = 0.0
        wrench_cmd_msg.wrench.torque.z = 0.0
        wrench_cmd_msg.header.stamp = rospy.Time.now()
        wrench_cmd_msg.header.frame_id = "base_link"
        pub.publish(wrench_cmd_msg)



        # if abs(z_prev - f.z) < 3.0 or i==0:

        #     z_prev = z
        #     z -= 10.0

        #     wrench_cmd_msg = WrenchStamped()
        #     wrench_cmd_msg.wrench.force.x = 0.0
        #     wrench_cmd_msg.wrench.force.y = 0.0
        #     wrench_cmd_msg.wrench.force.z = z
        #     wrench_cmd_msg.wrench.torque.x = 0.0
        #     wrench_cmd_msg.wrench.torque.y = 0.0
        #     wrench_cmd_msg.wrench.torque.z = 0.0
        #     wrench_cmd_msg.header.stamp = rospy.Time.now()
        #     wrench_cmd_msg.header.frame_id = "base_link"
        #     pub.publish(wrench_cmd_msg)

        # else:
        #     print "Did not reach force."
        #     if i==0:
        #         print "Never successfully did any force."
        #     #else:
        #        #raw_input("End video and take picture")
        #        #print "Commanding to last successful force."
        #        #wrench_cmd_msg = WrenchStamped()
        #        #wrench_cmd_msg.wrench.force.x = 0.0
        #        #wrench_cmd_msg.wrench.force.y = 0.0
        #        #wrench_cmd_msg.wrench.force.z = z_prev + 10.0
        #        #wrench_cmd_msg.wrench.torque.x = 0.0
        #        #wrench_cmd_msg.wrench.torque.y = 0.0
        #        #wrench_cmd_msg.wrench.torque.z = 0.0
        #        #wrench_cmd_msg.header.stamp = rospy.Time.now()
        #        #wrench_cmd_msg.header.frame_id = "base_link"
        #        #pub.publish(wrench_cmd_msg)

        #     flag = raw_input("press enter to continue and keep trying or type 0 and press enter to exit")

        #     if flag == str(0):
        #         break
        #     else:
        #         z_prev = z
        #         z -= 10.0

        #         wrench_cmd_msg = WrenchStamped()
        #         wrench_cmd_msg.wrench.force.x = 0.0
        #         wrench_cmd_msg.wrench.force.y = 0.0
        #         wrench_cmd_msg.wrench.force.z = z
        #         wrench_cmd_msg.wrench.torque.x = 0.0
        #         wrench_cmd_msg.wrench.torque.y = 0.0
        #         wrench_cmd_msg.wrench.torque.z = 0.0
        #         wrench_cmd_msg.header.stamp = rospy.Time.now()
        #         wrench_cmd_msg.header.frame_id = "base_link"
        #         pub.publish(wrench_cmd_msg)

        i += 1

        rate.sleep()

    input("When ready, press enter to command xy force")

