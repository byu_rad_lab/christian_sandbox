#!/usr/bin/env python
from kinematics import arm_tools as at
import numpy as np
import os
import rospy
from std_msgs.msg import Float32
from subscribers import system_minimal_msg_subscriber as smm_sub
import sys
from tools import kin_tools as kt

def calcPayloadScore(arm,wrench):
    arm.calcJac()
    arm.calcTorqueGravity()

    torques_gravity = -1.0*arm.getTorqueGravity()
    torques_wrench = np.dot(arm.getJacHybrid().T,wrench)

    if not arm.areTorquesPossible(torques_gravity + torques_wrench):
        payload = 0

    else:

        max_torques = []
        min_torques = []

        jangles = arm.getJointAngs()

        k8 = 136.0
        k4 = 27.0

        kp8 = 1.0/2160.0
        kp4 = 1.0/6612.0

        max_pressure = 599844.0

        max_torques.append(kp8*max_pressure - k8*jangles[0])
        max_torques.append(kp8*max_pressure - k8*jangles[1])
        max_torques.append(kp4*max_pressure - k4*jangles[2])
        max_torques.append(kp4*max_pressure - k4*jangles[3])

        min_torques.append(-1.0*kp8*max_pressure - k8*jangles[0])
        min_torques.append(-1.0*kp8*max_pressure - k8*jangles[1])
        min_torques.append(-1.0*kp4*max_pressure - k4*jangles[2])
        min_torques.append(-1.0*kp4*max_pressure - k4*jangles[3])

        scale = []

        for i in range(len(torques_wrench)):

            if torques_wrench[i] < 0:
                scale.append(abs((min_torques[i] - torques_gravity[i])/torques_wrench[i]))

            elif torques_wrench[i] > 0:
                scale.append(abs((max_torques[i] - torques_gravity[i])/torques_wrench[i]))
            else:
                scale.append(sys.float_info.max)

        limit_idx = scale.index(min(scale))

        new_wrench = scale[limit_idx] * wrench

        payload = min(abs(new_wrench[2] - wrench[2])[0],100.0*9.81)

        torques2 = np.dot(arm.getJacHybrid().T,new_wrench) + torques_gravity

        if (not arm.areTorquesPossible(torques2)) or (scale[limit_idx] - 1.0) < -1e-6:

            print("PAYLOAD CALC IS WRONG!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            print("scale: ", scale[limit_idx])
            print("torques: ", torques2)

    return payload




if __name__=='__main__':

    #define leg
    density = 4.980 #4.980 was measured empirically of the aluminium used in the adjustable links made for the leg
    min_lengths = [0.185, 0.263, 0.143, 0.15]
    R = 0.18 #radius of shoulder mount
    d = 0.145 #offset to beginning of joint from shoulder mount rotation center

    #robot 66
    # lengths = [0.185, 0.263, 0.143, 0.15]
    # angles = [1.199,0.0,90.0*np.pi/180.0,0.0]
    # x_rotation = -1.2146
    # base_hom = kt.ax_vec2homog(np.array([x_rotation, 0, 0]), np.array([0.0, R + d*np.sin(x_rotation), d*np.cos(x_rotation)]))
    # base_width = 1.1169
    # base_length = 0.4537
    # mount_angle = 0.826

    #robot 4
    lengths = [0.207, 0.371, 0.143, 0.234]
    angles = [57.0*np.pi/180.0,0.0,63.0*np.pi/180.0,0.0]
    x_rotation = -95.0*np.pi/180.0
    base_hom = kt.ax_vec2homog(np.array([x_rotation, 0, 0]), np.array([0.0, R + d*np.sin(x_rotation), d*np.cos(x_rotation)]))
    base_width = 1.2917
    base_length = 1.1814
    mount_angle = 0.895

    quadruped = at.quadruped(lengths,angles,base_hom,base_width,base_length,mount_angle,0.205, density, min_lengths)
    quadruped.calcMass()

    wrench = np.array([[0.0],[0.0],[(0.5*quadruped.getBaseMass() + 2.0*quadruped.getMass())*9.81], [0.0], [0.0], [0.0]])

    rospy.init_node('Payload_Calc', anonymous=True)

    sub = smm_sub.SystemMinimalMsgSubscriber('/shere_khan_uv/estimation_minimal/')
    pub = rospy.Publisher('/payload_score',Float32)

    rate = rospy.Rate(1.0)

    while not rospy.is_shutdown():

        sub.getData()
        q = sub.q_est

        if q:
            #jangles = np.array([0.593412, -0.942478, 0.802851,-0.174533])
            #print "q: ", q
            quadruped.setJointAngs(np.array(q))
            payload = calcPayloadScore(quadruped,wrench)
            payload_msg = Float32()
            payload_msg.data = payload
            pub.publish(payload_msg)

        rate.sleep()




