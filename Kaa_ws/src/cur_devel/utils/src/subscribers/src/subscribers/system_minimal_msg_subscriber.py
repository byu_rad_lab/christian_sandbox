#!/usr/bin/env python
import copy
import rospy
from ros_msg.msg import JointFlexusMinimalMsg
from ros_msg.msg import SystemMinimalMsg
from ros_msg.msg import ToolStateMsg
from std_msgs.msg import Header
import threading

class SystemMinimalMsgSubscriber:
    def __init__(self, topic_name = "/system_minimal"):

        self.header_ = Header()
        self.header = Header()

        self.control_mode_ = 0
        self.control_mode = 0

        self.joints_est_ = []
        self.joints_est = []
        self.joints_ref_ = []
        self.joints_ref = []
        self.joints_sim_ = []
        self.joints_sim = []

        #the tool parts haven't been fully fledged out since we aren't using them yet
        self.tool_est_ = ToolStateMsg()
        self.tool_est = ToolStateMsg()
        self.tool_ref_ = ToolStateMsg()
        self.tool_ref = ToolStateMsg()

        rospy.Subscriber(topic_name, SystemMinimalMsg, self.callback)

        self.message_received = False

        self.lock = threading.Lock()

        self.q_est = []
        self.qd_est = []
        self.prs_bel_est = []
        self.q_ref = []
        self.qd_ref = []
        self.q_sim = []
        self.qd_sim = []

    def callback(self, msg):

        self.message_received = True

        self.lock.acquire()
        self.joints_est_ = []
        self.joints_ref_ = []
        self.joints_sim_ = []
        try:
            self.header_ = msg.header
            self.control_mode_ = msg.control_mode
            for i in range(len(msg.joints_est)):
                self.joints_est_.append(msg.joints_est[i])
            for i in range(len(msg.joints_ref)):
                self.joints_ref_.append(msg.joints_ref[i])
            for i in range(len(msg.joints_sim)):
                self.joints_sim_.append(msg.joints_sim[i])
            self.tool_est_ = msg.tool_est
            self.tool_ref_ = msg.tool_ref

        finally:
            self.lock.release()

    def getData(self):
        self.lock.acquire()
        try:
            self.header = copy.deepcopy(self.header_)
            self.control_mode = copy.deepcopy(self.control_mode_)
            self.joints_est = copy.deepcopy(self.joints_est_)
            self.joints_ref = copy.deepcopy(self.joints_ref_)
            self.joints_sim = copy.deepcopy(self.joints_sim_)
            self.tool_est = copy.deepcopy(self.tool_est_)
            self.tool_ref = copy.deepcopy(self.tool_ref_)

        finally:
            self.lock.release()

        self.q_est = []
        self.qd_est = []
        self.prs_bel_est = []
        self.q_ref = []
        self.qd_ref = []
        self.q_sim = []
        self.qd_sim = []

        for i in range(len(self.joints_est)):
            self.q_est.append(self.joints_est[i].q[0])
            self.q_est.append(self.joints_est[i].q[1])
            self.qd_est.append(self.joints_est[i].qd[0])
            self.qd_est.append(self.joints_est[i].qd[1])
            for j in range(len(self.joints_est[i].prs_bel)):
                self.prs_bel_est.append(self.joints_est[i].prs_bel[j])

        for i in range(len(self.joints_ref)):
            self.q_ref.append(self.joints_est[i].q[0])
            self.q_ref.append(self.joints_est[i].q[1])
            self.qd_ref.append(self.joints_est[i].qd[0])
            self.qd_ref.append(self.joints_est[i].qd[1])

        for i in range(len(self.joints_sim)):
            self.q_sim.append(self.joints_est[i].q[0])
            self.q_sim.append(self.joints_est[i].q[1])
            self.qd_sim.append(self.joints_est[i].qd[0])
            self.qd_sim.append(self.joints_est[i].qd[1])

if __name__ == "__main__":

    rospy.init_node('minimal_msg_subscriber', anonymous=True)

    subscriber = SystemMinimalMsgSubscriber('/atheris/system_minimal/')

    rate = rospy.Rate(100)

    while not rospy.is_shutdown():
        subscriber.getData()

#       print "It's looping!"
        if subscriber.message_received:
            print("q: ", subscriber.q_est)
            print("qd: ", subscriber.qd_est)

#           print "joint1: ", subscriber.joints_est[0].q
#           print "joint2: ", subscriber.joints_est[1].q
#           print "joint1 vel: ", subscriber.joints_est[1].qd
#           print "joint2 vel: ", subscriber.joints_est[1].qd

        rate.sleep()

