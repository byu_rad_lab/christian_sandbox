#!/usr/bin/env python
import copy
import rospy
from geometry_msgs.msg import Wrench
from geometry_msgs.msg import WrenchStamped
from std_msgs.msg import Header
import threading

class WrenchStampedSubscriber:
    def __init__(self, topic_name = "/system_minimal"):

        self.header_ = Header()
        self.header = Header()

        self.wrench_ = Wrench()
        self.wrench = Wrench()

        rospy.Subscriber(topic_name, WrenchStamped, self.callback)

        self.message_received = False

        self.lock = threading.Lock()

    def callback(self, msg):

        self.message_received = True

        self.lock.acquire()
        try:
            self.header_ = msg.header
            self.wrench_ = msg.wrench

        finally:
            self.lock.release()

    def getData(self):
        self.lock.acquire()
        try:
            self.header = copy.deepcopy(self.header_)
            self.wrench = copy.deepcopy(self.wrench_)

        finally:
            self.lock.release()

if __name__ == "__main__":

    rospy.init_node('wrench_subscriber', anonymous=True)

    subscriber = WrenchStampedSubscriber('/ftnode1/netft_data')

    rate = rospy.Rate(100)

    while not rospy.is_shutdown():
        subscriber.getData()

#       print "It's looping!"
        if subscriber.message_received:
            print("wrench: ", subscriber.wrench)

        rate.sleep()

