#!/usr/bin/env python

import rosbag
import csv
from std_msgs.msg import Int32, String
from geometry_msgs.msg import WrenchStamped, PoseStamped, Pose
from tf2_msgs.msg import TFMessage # message for vive pucks
from king_louie.msg import sensors as sensor # message for king louie pressures
from byu_robot.msg import states as states_msg # message for king louie joint angles
from king_louie.msg import mpc_msg # message for king louie desired pressures
import os


if __name__ == '__main__':

    bagpath = os.path.expanduser('~') + '/git/byu/cur_devel/utils/src/rad_recorders/src/2018-02-07-13-20-53.bag'

    output_file_name_tf = 'tf_data.txt' # vive data
    output_file_name_klp = 'klp_data.txt' #king louie pressures
    output_file_name_kljangles = 'kljangles_data.txt' # king louie joint angles
    output_file_name_klpd5 = 'kl_pd5.txt' # king louie desired pressures shoulder 1 joint
    output_file_name_klpd6 = 'kl_pd6.txt' # king louie desired pressures shoulder 2 joint
    output_file_name_klpd8 = 'kl_pd8.txt' # king louie desired pressures wrist 1 joint
    output_file_name_klpd9 = 'kl_pd9.txt' # king louie desired pressures wrist 2 joint
    output_file_name_klpj5 = 'kl_pj5.txt' # predicted joint angles for shoulder 1 for 20 time steps
    output_file_name_klpj6 = 'kl_pj6.txt' # predicted joint angles for shoulder 2 for 20 time steps
    output_file_name_klpj8 = 'kl_pj8.txt' # predicted joint angles for wrist 1 for 20 time steps
    output_file_name_klpj9 = 'kl_pj9.txt' # predicted joint angles for wrist 2 for 20 time steps

    topic_list = ['tf', 'PressureData', 'statesFiltered', 'Joint5DesiredPressures', 'Joint6DesiredPressures', 'Joint8DesiredPressures', 'Joint9DesiredPressures', 'Joint5PredictedStates', 'Joint6PredictedStates', 'Joint8PredictedStates', 'Joint9PredictedStates']

    bag = rosbag.Bag(bagpath)

    data_file_tf = open(output_file_name_tf,'w')
    data_file_klp = open(output_file_name_klp,'w')
    data_file_kljangles = open(output_file_name_kljangles,'w')
    data_file_klpd5 = open(output_file_name_klpd5,'w')
    data_file_klpd6 = open(output_file_name_klpd6,'w')
    data_file_klpd8 = open(output_file_name_klpd8,'w')
    data_file_klpd9 = open(output_file_name_klpd9,'w')
    data_file_klpj5 = open(output_file_name_klpj5,'w')
    data_file_klpj6 = open(output_file_name_klpj6,'w')
    data_file_klpj8 = open(output_file_name_klpj8,'w')
    data_file_klpj9 = open(output_file_name_klpj9,'w')

    for topic_name, msg, t in bag.read_messages(topics=topic_list):
        if topic_name == 'tf':
            transform_from = msg.transforms[0].child_frame_id
            transform = msg.transforms[0].transform #consists of translation and a quaternion
            px = transform.translation.x
            py = transform.translation.y
            pz = transform.translation.z
            ox = transform.rotation.x
            oy = transform.rotation.y
            oz = transform.rotation.z
            ow = transform.rotation.w
            if transform_from == 'neo_tracker1':
                vn = 1
            elif transform_from == 'neo_tracker2':
                vn = 2
            elif transform_from == 'neo_tracker3':
                vn = 3
            elif transform_from == 'neo_tracker5':
                vn = 4
            else:
                vn = 0

            data_file_tf.write(str(t)+',\t'+str(vn)+',\t'+str(px)+',\t'+str(py)+',\t'+str(pz)+',\t'+str(ox)+',\t'+str(oy)+',\t'+str(oz)+',\t'+str(ow)+'\n')
        elif topic_name == 'PressureData':
            LSPM = str(msg.p[12])
            LSPL = str(msg.p[13])
            LSDA = str(msg.p[14])
            LSDP = str(msg.p[15])
            LWPP = str(msg.p[18])
            LWPA = str(msg.p[19])
            LWDL = str(msg.p[20])
            LWDM = str(msg.p[21])
            data_file_klp.write(str(t) + ',\t' + LSPM  + ',\t' + LSPL + ',\t' + LSDA  + ',\t' + LSDP  + ',\t' + LWPP  + ',\t' + LWPA  + ',\t' + LWDL  + ',\t' + LWDM + '\n') 
        elif topic_name == 'statesFiltered':
            jt1 = str(msg.q[0])
            jt2 = str(msg.q[1])
            jt3 = str(msg.q[2])
            jt4 = str(msg.q[3])
            data_file_kljangles.write(str(t) + ',\t' + jt1  + ',\t' + jt2 + ',\t' + jt3  + ',\t' + jt4 + '\n') 
        elif topic_name == 'Joint5DesiredPressures':
            pd0 = str(msg.p_d[0])
            pd1 = str(msg.p_d[1])
            data_file_klpd5.write(str(t) + ',\t' + pd0 + ',\t' + pd1 + '\n')
        elif topic_name == 'Joint6DesiredPressures':
            pd0 = str(msg.p_d[0])
            pd1 = str(msg.p_d[1])
            data_file_klpd6.write(str(t) + ',\t' + pd0 + ',\t' + pd1 + '\n')
        elif topic_name == 'Joint8DesiredPressures':
            pd0 = str(msg.p_d[0])
            pd1 = str(msg.p_d[1])
            data_file_klpd8.write(str(t) + ',\t' + pd0 + ',\t' + pd1 + '\n')
        elif topic_name == 'Joint9DesiredPressures':
            pd0 = str(msg.p_d[0])
            pd1 = str(msg.p_d[1])
            data_file_klpd9.write(str(t) + ',\t' + pd0 + ',\t' + pd1 + '\n')
        elif topic_name == 'Joint5PredictedStates':
            if len(msg.p_d) == 20:
                data_file_klpj5.write(str(t) + ',\t')
                for i in range(len(msg.p_d)):
                    if i < len(msg.p_d)-1:
                        data_file_klpj5.write(str(msg.p_d[i]) + ',\t')
                    elif i == len(msg.p_d)-1:
                        data_file_klpj5.write(str(msg.p_d[i]) + '\n')
        elif topic_name == 'Joint6PredictedStates':
            if len(msg.p_d) == 20:
                data_file_klpj6.write(str(t) + ',\t')
                for i in range(len(msg.p_d)):
                    if i < len(msg.p_d)-1:
                        data_file_klpj6.write(str(msg.p_d[i]) + ',\t')
                    elif i == len(msg.p_d)-1:
                        data_file_klpj6.write(str(msg.p_d[i]) + '\n')
        elif topic_name == 'Joint8PredictedStates':
            if len(msg.p_d) == 20:
                data_file_klpj8.write(str(t) + ',\t')
                for i in range(len(msg.p_d)):
                    if i < len(msg.p_d)-1:
                        data_file_klpj8.write(str(msg.p_d[i]) + ',\t')
                    elif i == len(msg.p_d)-1:
                        data_file_klpj8.write(str(msg.p_d[i]) + '\n')
        elif topic_name == 'Joint9PredictedStates':
            if len(msg.p_d) == 20:
                data_file_klpj9.write(str(t) + ',\t')
                for i in range(len(msg.p_d)):
                    if i < len(msg.p_d)-1:
                        data_file_klpj9.write(str(msg.p_d[i]) + ',\t')
                    elif i == len(msg.p_d)-1:
                        data_file_klpj9.write(str(msg.p_d[i]) + '\n')

    data_file_tf.close()
    data_file_klp.close()
    data_file_kljangles.close()
    data_file_klpd5.close()
    data_file_klpd6.close()
    data_file_klpd8.close()
    data_file_klpd9.close()
    data_file_klpj5.close()
    data_file_klpj6.close()
    data_file_klpj8.close()
    data_file_klpj9.close()
    bag.close()
