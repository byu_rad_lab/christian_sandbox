#!/usr/bin/env python

import rosbag
import csv
import os

from std_msgs.msg import Int32, String
from byu_robot.msg import states as states_msg
from byu_robot.msg import commands as commands_msg
from byu_robot.msg import send_pressures
from std_msgs.msg import Float64MultiArray as pose_ax_ang
from geometry_msgs.msg import PoseStamped
from pdb import set_trace as pause

if __name__ == '__main__':
    bagpath = os.path.expanduser('~') + '/git/byu/cur_devel/design/src/evolutionary_optimization/src/optimization/multi_arm_data/multi_arm_data/trajectory_2/10sec/x4y4_10sec_baxter_trial9.bag'
    savepath = os.path.expanduser('~') + '/git/byu/cur_devel/design/src/evolutionary_optimization/src/optimization/multi_arm_data/multi_arm_data/trajectory_2/10sec/x4y4_10sec_baxter_trial9/'

    output_file_name_act_pose = savepath + 'bax_jangles.txt' # actual end effector pose

    topic_list = ['/robot/joint_states']

    bag = rosbag.Bag(bagpath)

    data_file_act_pose = open(output_file_name_act_pose, 'w')

    for topic_name, msg, t in bag.read_messages(topics=topic_list):
        t = t.to_sec()
        if topic_name == '/robot/joint_states':
            data_file_act_pose.write(str(t)+',\t'+str(msg.position[11])+',\t'+str(msg.position[12])+',\t'+str(msg.position[9])+',\t'+str(msg.position[10])+',\t'+str(msg.position[13])+',\t'+str(msg.position[14])+',\t'+str(msg.position[15])+'\n')

    data_file_act_pose.close()
    bag.close()
