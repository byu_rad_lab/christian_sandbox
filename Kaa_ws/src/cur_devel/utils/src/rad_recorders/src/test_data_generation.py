#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from rad_recorders.srv import LogData
import time

def talker():
    pub = rospy.Publisher('chatter', String, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(1000) # 10hz

    record_srv = rospy.ServiceProxy('log_data', LogData)

    cur_time = time.time()

    #could do this to write data forever
    #while not rospy.is_shutdown():

    #this starts data writing
    record_srv('chatter_messages_topic-')
    while time.time() - cur_time < 10:
        hello_str = "hello world %s" % rospy.get_time()
        rospy.loginfo(hello_str)
        pub.publish(hello_str)
        rate.sleep()
    #this ends data writing and input string does nothing (I think)
    record_srv('chatter_messages_topic-')

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
