#!/usr/bin/env python

import numpy as np
from geometry_msgs.msg import WrenchStamped, PoseStamped, Pose
from tf2_msgs.msg import TFMessage
from data_recorder import Recorder
import sys,rospy,os

class recorderInfo():
    def __init__(self,data_type,file_path,topics):
        # Specify Path
        self.file_path = file_path

        # For length of data_type, make dictionary of subscribers
        self.subsc_dict = {}

        # For length of data_type, make list of titles
        self.title_list = []
        num_entries = 0

        # For length of data_type, make dict of msgs
        self.msg_type_list = []

        # For length of data_type, make dict of topics
        self.topic_list = []

        # For length of data_type, make function list of subscribers
        self.function_list = []

        # For length of data_type, make dict of functions
        # For different types of messages, will need to make more entries in the if/else statement
        for i in range(len(data_type)):
            if data_type[i] == 'force':
                # Subscriber
                def sub(msg):
                    return (str(msg.header.seq)+',\t'+str(msg.header.stamp.secs+msg.header.stamp.nsecs*1e-9)+',\t'+str(msg.wrench.force.x)+',\t'+str(msg.wrench.force.y)+',\t'+str(msg.wrench.force.z)+',\t'+str(msg.wrench.torque.x)+',\t'+str(msg.wrench.torque.y)+',\t'+str(msg.wrench.torque.z)+' \n')

                # Title
                self.title_list.append('sequence,\t'+'time,\t'+'f_x,\t'+'f_y,\t'+'f_z,\t'+'tau_x,\t'+'tau_y,\t'+'tau_z\n')
                num_entries += 1

                # Messages
                self.msg_type_list.append(WrenchStamped)

            elif data_type[i] == 'PoseStamped':
                # Subscriber
                def sub(msg):
                    return (str(msg.header.seq)+',\t'+str(msg.header.stamp.secs+msg.header.stamp.nsecs*1e-9)+',\t'+str(msg.pose.position.x)+',\t'+str(msg.pose.position.y)+',\t'+str(msg.pose.position.z)+',\t'+str(msg.pose.orientation.x)+',\t'+str(msg.pose.orientation.y)+',\t'+str(msg.pose.orientation.z)+',\t'+str(msg.pose.orientation.w)+'\n')
                
                # Title
                self.title_list.append('sequence,\t'+'time,\t'+'p_x,\t'+'p_y,\t'+'p_z,\t'+'o_x,\t'+'o_y,\t'+'o_z,\t'+'o_w\n')
                num_entries += 1

                # Messages
                self.msg_type_list.append(PoseStamped)

            elif data_type[i] == 'Pose':
                # Subscriber
                def sub(msg):
                    return (str(msg.position.x)+',\t'+str(msg.position.y)+',\t'+str(msg.position.z)+',\t'+str(msg.orientation.x)+',\t'+str(msg.orientation.y)+',\t'+str(msg.orientation.z)+',\t'+str(msg.orientation.w)+'\n')
                
                # Title
                self.title_list.append('p_x,\t'+'p_y,\t'+'p_z,\t'+'o_x,\t'+'o_y,\t'+'o_z,\t'+'o_w\n')
                num_entries += 1

                # Messages
                self.msg_type_list.append(Pose)

            elif data_type[i] == 'TFMessage':
                # Subscriber
                def sub(msg):
                    transform_from = msg.transforms[0].child_frame_id
                    transform = msg.transforms[0].transform #consists of translation and a quaternion
                    px = transform.translation.x
                    py = transform.translation.y
                    pz = transform.translation.z
                    ox = transform.rotation.x
                    oy = transform.rotation.y
                    oz = transform.rotation.z
                    ow = transform.rotation.w
                    if transform_from == 'neo_tracker1': # base
                        vn = 1
                    elif transform_from == 'neo_tracker2': #arm1 or chest for kl
                        vn = 2
                    elif transform_from == 'neo_tracker3': # arm2
                        vn = 3
                    elif transform_from == 'neo_tracker5': # arm3
                        vn = 4
                    else:
                        vn = 0
                    return (str(vn)+',\t'+str(px)+',\t'+str(py)+',\t'+str(pz)+',\t'+str(ox)+',\t'+str(oy)+',\t'+str(oz)+',\t'+str(ow)+',\t'+str(rospy.Time.now().to_nsec())+',\t'+str(msg.transforms[0].header.stamp.to_nsec())+'\n')
                
                # Title
                self.title_list.append('vive#,\t'+'p_x,\t'+'p_y,\t'+'p_z,\t'+'o_x,\t'+'o_y,\t'+'o_z,\t'+'o_w\t'+'t_ros,\t'+'t_msg,\n')
                num_entries += 1

                # Messages
                self.msg_type_list.append(TFMessage)
            # Sort Titles
            self.title_list = np.reshape(np.asarray(self.title_list),(num_entries,)).tolist()

            # Sort Subscribers
            self.subsc_dict.update({'sub' + str(i):sub})

            # Topics
            self.topic_list = topics

        # Functions
        for key in sorted(self.subsc_dict.keys()):
            self.function_list.append(self.subsc_dict[key])

if __name__ == "__main__":
    if rospy.has_param('file_path') and rospy.has_param('types') and rospy.has_param('topics'):
        file_path = str(rospy.get_param('file_path'))
        if not os.path.exists(file_path):
            os.mkdir(file_path)
        types = rospy.get_param('types')
        topics = rospy.get_param('topics')
        ri = recorderInfo(types,file_path,topics)
        recorder_object = Recorder(ri.topic_list,
                                   ri.function_list,
                                   ri.title_list,
                                   ri.msg_type_list,
                                   ri.file_path,
                                   '',
                                   0.0005)
    else:
        print('Need to specify msg type, file path, and topics in rosparam server')
