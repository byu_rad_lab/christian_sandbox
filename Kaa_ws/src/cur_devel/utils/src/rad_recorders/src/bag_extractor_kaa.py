#!/usr/bin/env python

import rosbag
import csv
import os

from std_msgs.msg import Int32, String
from byu_robot.msg import states as states_msg
from byu_robot.msg import commands as commands_msg
from byu_robot.msg import send_pressures
from std_msgs.msg import Float64MultiArray as pose_ax_ang
from geometry_msgs.msg import PoseStamped
from pdb import set_trace as pause

if __name__ == '__main__':

    bagpath = os.path.expanduser('~') + '/git/byu/cur_devel/design/src/evolutionary_optimization/src/optimization/multi_arm_data/multi_arm_data/trajectory_1/10_sec_no_lag/10_sec_traj_1_no_lag_trial1_kaa.bag'
    savepath = os.path.expanduser('~') + '/git/byu/cur_devel/design/src/evolutionary_optimization/src/optimization/multi_arm_data/multi_arm_data/trajectory_1/10_sec_no_lag/10_sec_traj_1_no_lag_trial1_kaa/'

    output_file_name_act_pose = savepath + 'act_pose.txt' # actual end effector pose
    output_file_name_des_pose = savepath + 'des_pose.txt' # desired end effector pose
    output_file_name_table_pose = savepath + 'table_pose.txt' # desired end effector pose
    output_file_name_act_jangles = savepath + 'act_jangles.txt' # actual joint angles
    output_file_name_des_jangles = savepath + 'des_jangles.txt' # desired joint angles
    output_file_name_act_pplus = savepath + 'act_pplus.txt' # actual bladder pressures
    output_file_name_act_pminus = savepath + 'act_pminus.txt' # actual bladder pressures

    topic_list = ['/SetJointGoal', '/act_pose', '/desired_pose', '/states', '/table_pose']

    bag = rosbag.Bag(bagpath)

    data_file_act_pose = open(output_file_name_act_pose, 'w')
    data_file_des_pose = open(output_file_name_des_pose, 'w')
    data_file_table_pose = open(output_file_name_table_pose, 'w')
    data_file_act_jangles = open(output_file_name_act_jangles, 'w')
    data_file_des_jangles = open(output_file_name_des_jangles, 'w')
    data_file_act_pplus = open(output_file_name_act_pplus, 'w')
    data_file_act_pminus = open(output_file_name_act_pminus, 'w')

    for topic_name, msg, t in bag.read_messages(topics=topic_list):
        t = t.to_sec()
        if topic_name == '/act_pose':
            data_file_act_pose.write(str(t)+',\t'+str(msg.data[1])+',\t'+str(msg.data[2])+',\t'+str(msg.data[3])+',\t'+str(msg.data[4])+',\t'+str(msg.data[5])+',\t'+str(msg.data[6])+'\n')

        elif topic_name == '/desired_pose':
            data_file_des_pose.write(str(t)+',\t'+str(msg.data[1])+',\t'+str(msg.data[2])+',\t'+str(msg.data[3])+',\t'+str(msg.data[4])+',\t'+str(msg.data[5])+',\t'+str(msg.data[6])+'\n')

        elif topic_name == '/table_pose':
            data_file_table_pose.write(str(t)+',\t'+str(msg.data[1])+',\t'+str(msg.data[2])+',\t'+str(msg.data[3])+',\t'+str(msg.data[4])+',\t'+str(msg.data[5])+',\t'+str(msg.data[6])+'\n')

        elif topic_name == '/SetJointGoal':
            data_file_des_jangles.write(str(t)+',\t'+str(msg.q[0])+',\t'+str(msg.q[1])+',\t'+str(msg.q[2])+',\t'+str(msg.q[3])+',\t'+str(msg.q[4])+',\t'+str(msg.q[5])+'\n')

        elif topic_name == '/states':
            data_file_act_jangles.write(str(t)+',\t'+str(msg.q[0])+',\t'+str(msg.q[1])+',\t'+str(msg.q[2])+',\t'+str(msg.q[3])+',\t'+str(msg.q[4])+',\t'+str(msg.q[5])+'\n')
            data_file_act_pplus.write(str(t)+',\t'+str(msg.pPlus[0])+',\t'+str(msg.pPlus[1])+',\t'+str(msg.pPlus[2])+',\t'+str(msg.pPlus[3])+',\t'+str(msg.pPlus[4])+',\t'+str(msg.pPlus[5])+'\n')
            data_file_act_pminus.write(str(t)+',\t'+str(msg.pMinus[0])+',\t'+str(msg.pMinus[1])+',\t'+str(msg.pMinus[2])+',\t'+str(msg.pMinus[3])+',\t'+str(msg.pMinus[4])+',\t'+str(msg.pMinus[5])+'\n')

    data_file_act_pose.close()
    data_file_des_pose.close()
    data_file_act_jangles.close()
    data_file_des_jangles.close()
    data_file_act_pplus.close()
    data_file_act_pminus.close()
    bag.close()
