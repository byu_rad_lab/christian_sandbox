#!/usr/bin/env python

import rospy,sys,os,select,time,re
from rad_recorders.srv import LogData
import matplotlib.pyplot as plt
import numpy as np

# Removes first file that I can't stop
def remove_first_file(file_path,topics):
    record_srv = rospy.ServiceProxy('log_data',LogData)
    file_name = file_path
    record_srv(file_path)
    record_srv(file_path)
    for i in range(len(topics)):
        os.remove(file_path + '/data_'+topics[i]+'.txt')
        
# Begins recording given file path and file suffix
def start_recording(file_path,file_suffix,ploton = False):
    record_srv = rospy.ServiceProxy('log_data',LogData)
    file_name = file_path + '/' + file_suffix
    record_srv(file_name)

    #This segment runs until user hits enter (captures data until done)                  
    print("Collecting data","\n---------------","\n---------------","\n---------------","\n---------------","\n---------------","\n---------------","\n---------------")
    print("Press 'Enter' to stop test.")
    while True:
        if sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
           line = input()
           break
    record_srv(file_name) # stops service and closes file

    # Clear window
    os.system('cls' if os.name == 'nt' else 'clear')

if __name__ == "__main__":
    # Make sure record_init.py is running
    time.sleep(1.0)

    # Start node
    rospy.init_node('record_start',anonymous = True)

    # Get Needed Parameters, otherwise quit
    if rospy.has_param('file_path') and rospy.has_param('topics'):
        file_path = str(rospy.get_param('file_path'))
        topics = rospy.get_param('suffix') 
    else:
        print('Need to apply file_path to rosparam server')
        sys.exit()

    first_time = True
    new_trial = 'y'
    while not rospy.is_shutdown():
        # First time through, remove file since it doesn't have naming capabilities
        if first_time:
            remove_first_file(file_path,topics)
            first_time = False
        # Otherwise, make new files as desired
        while new_trial == 'y':
            print('Beginning Recording:\n')
            file_identifier = input('Input file identifier (appends to before topic name in file_path specified in rosparam server)\n\n')
            # start_recording(file_path,file_identifier,ploton = plot)
            start_recording(file_path,file_identifier,ploton = False)
            new_trial = input('Start new trial? (y/n)\n\n')
        break
