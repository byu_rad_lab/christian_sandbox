#!/usr/bin/env python

import rosbag
import csv
import os

from std_msgs.msg import Int32, String
from byu_robot.msg import states as states_msg
from byu_robot.msg import commands as commands_msg
from byu_robot.msg import send_pressures
from collections import deque
from std_msgs.msg import Float64MultiArray as pose_ax_ang
from geometry_msgs.msg import PoseStamped
from pdb import set_trace as pause
from geometry_msgs.msg import WrenchStamped
import scipy.io as sio

if __name__ == '__main__':

    flexType = 'large'
    capDate = '12_20_2018_15_59'

    # bagpath = os.path.expanduser('~') + '/data/hardware_data/stiffness_12_17_2018_10_31.bag'
    bagpath = os.path.expanduser('~') + '/data/stiff_'+flexType+'_'+capDate+'.bag'
    savepath = os.path.expanduser('~') + '/data/hardware_data/flex/'

    outputFileName = 'stiff_'+flexType+'_'+capDate+'.mat'

    # topic_list = ['/robot_state', '/ftnode1/netft_data', '/vrpn_client_node/flexBase/pose',  '/vrpn_client_node/flex'+flexType+'Tip/pose']
    if flexType == 'ninja_small':
        topic_list = ['/robot_state', '/ftnode1/netft_data', '/vrpn_client_node/flexBase/pose',  '/vrpn_client_node/ninjaflexSmallTip/pose']
    elif flexType == 'small':
            topic_list = ['/robot_state', '/ftnode1/netft_data', '/vrpn_client_node/flexBase/pose',  '/vrpn_client_node/flexSmallTip/pose']
    elif flexType == 'med':
        topic_list = ['/robot_state', '/ftnode1/netft_data', '/vrpn_client_node/flexBase/pose',  '/vrpn_client_node/flexMedTip/pose']
    elif flexType == 'large':
        topic_list = ['/robot_state', '/ftnode1/netft_data', '/vrpn_client_node/flexBase/pose',  '/vrpn_client_node/flexLargeTip/pose']
    else:
        print('wrong type FAIL')
    bag = rosbag.Bag(bagpath)

    # systSave = deque()
    qSave = deque()
    qtSave = deque()
    fttimeSave = deque()
    forceSave = deque()
    torqueSave = deque()
    posSaveBot = deque()
    posSaveTop = deque()
    postimeSaveBot = deque()
    postimeSaveTop = deque()
    prsSave = deque()
    prstimeSave = deque()
    for topic_name, msg, t in bag.read_messages(topics=topic_list):
        # tSave.append(t.to_sec())
        t = t.to_sec()
        # if topic_name == '/states':
        #     qtSave.append(t)
        #     qSave.append([msg.q[0], msg.q[1], msg.q[2], msg.q[3], msg.q[4], msg.q[5]])
        if topic_name == '/ftnode1/netft_data':
            fttimeSave.append(t)
            forceSave.append([msg.wrench.force.x,msg.wrench.force.y,msg.wrench.force.z])
            torqueSave.append([msg.wrench.torque.x, msg.wrench.torque.y, msg.wrench.torque.z])
        elif topic_name == '/robot_state':
            prstimeSave.append(t)
            prsSave.append(msg.sensors[0].prs_cal[0])
        elif topic_name == '/vrpn_client_node/flexBase/pose':
            postimeSaveBot.append(t)
            posSaveBot.append([msg.pose.position.x,msg.pose.position.y,msg.pose.position.z,msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z,msg.pose.orientation.w])
        elif topic_name == '/vrpn_client_node/ninjaflexSmallTip/pose':
            postimeSaveTop.append(t)
            posSaveTop.append([msg.pose.position.x,msg.pose.position.y,msg.pose.position.z,msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z,msg.pose.orientation.w])
        elif topic_name == '/vrpn_client_node/flexSmallTip/pose':
            postimeSaveTop.append(t)
            posSaveTop.append([msg.pose.position.x,msg.pose.position.y,msg.pose.position.z,msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z,msg.pose.orientation.w])
        elif topic_name == '/vrpn_client_node/flexMedTip/pose':
            postimeSaveTop.append(t)
            posSaveTop.append([msg.pose.position.x,msg.pose.position.y,msg.pose.position.z,msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z,msg.pose.orientation.w])
        elif topic_name == '/vrpn_client_node/flexLargeTip/pose':
            postimeSaveTop.append(t)
            posSaveTop.append([msg.pose.position.x,msg.pose.position.y,msg.pose.position.z,msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z,msg.pose.orientation.w])
    
        
        
        


    # saveDict = {'tq': qtSave,'tFT': fttimeSave, 'q': qSave, 'force': forceSave, 'torque': torqueSave} 
    saveDict = {'posBot': posSaveBot,'posTop': posSaveTop,'tPosBot': postimeSaveBot,'tFT': fttimeSave, 'tPosTop': postimeSaveTop, 'force': forceSave, 'torque': torqueSave,'prs': prsSave,'tPrs': prstimeSave}
    sio.savemat(savepath+outputFileName, saveDict,)

    bag.close
            
    
        
