#!/usr/bin/env python
import matplotlib.pyplot as plt
import scipy.io as sio
import numpy as np

data = sio.loadmat('./data/NEMPC_BLOW_MOLDED_ARM_with_integrator.mat')
noidata = sio.loadmat('./data/NEMPC_BLOW_MOLDED_ARM_without_integrator2.mat')

# data = sio.loadmat('./data/sys_id_validation.mat')
# data = sio.loadmat('./data/OSQP_BLOW_MOLDED_ARM.mat')
# data = sio.loadmat('./data/EMPC_BLOW_MOLDED_ARM2pt.mat')
# data = sio.loadmat('./data/EMPC_BLOW_MOLDED_ARM_3jts.mat')
# data = sio.loadmat('./data/OSQP_BLOW_MOLDED_ARM_3jts.mat')
# data = sio.loadmat('./data/no_adaptive.mat')
# data = sio.loadmat('./data/no_bertha.mat')
# data = sio.loadmat('./data/with_bertha.mat')

# t = data['t'].flatten() - 20.5
t = data['t'].flatten() - 25
noit = noidata['t'].flatten() - 25

p = np.vstack([data['p00'],
               data['p01'],
               data['p02'],
               data['p03'],
               data['p10'],
               data['p11'],
               data['p12'],
               data['p13'],               
               data['p20'],
               data['p21'],
               data['p22'],
               data['p23']])
noip = np.vstack([noidata['p00'],
               noidata['p01'],
               noidata['p02'],
               noidata['p03'],
               noidata['p10'],
               noidata['p11'],
               noidata['p12'],
               noidata['p13'],               
               noidata['p20'],
               noidata['p21'],
               noidata['p22'],
               noidata['p23']])

pref = np.vstack([data['pref00'],
                  data['pref01'],
                  data['pref02'],
                  data['pref03'],
                  data['pref10'],
                  data['pref11'],
                  data['pref12'],
                  data['pref13'],               
                  data['pref20'],
                  data['pref21'],
                  data['pref22'],
                  data['pref23']])
noipref = np.vstack([noidata['pref00'],
                  noidata['pref01'],
                  noidata['pref02'],
                  noidata['pref03'],
                  noidata['pref10'],
                  noidata['pref11'],
                  noidata['pref12'],
                  noidata['pref13'],               
                  noidata['pref20'],
                  noidata['pref21'],
                  noidata['pref22'],
                  noidata['pref23']])


q = np.vstack([data['u0'],
               data['v0'],
               data['u1'],
               data['v1'],
               data['u2'],
               data['v2']])
noiq = np.vstack([noidata['u0'],
                  noidata['v0'],
                  noidata['u1'],
                  noidata['v1'],
                  noidata['u2'],
                  noidata['v2']])

qd = np.vstack([data['udot0'],
                data['vdot0'],
                data['udot1'],
                data['vdot1'],
                data['udot2'],
                data['vdot2']])

plt.figure(1)
plt.title('Joint Positions')
for i in range(0,6):
    plt.subplot(3,2,i+1)
    plt.plot(t,q[i,:].T)
    plt.plot(noit,noiq[i,:].T)
    plt.axis([0,120,-.8,.8])
    plt.ylabel('Jt '+str(i)+' Angles (rad)')
plt.ion()
plt.show()


# plt.figure(2)
# plt.plot(t,qd[0:2,:].T,'r')
# plt.plot(t,qd[2:4,:].T,'g')
# plt.plot(t,qd[4:6,:].T,'b')
# plt.title('Joint Velocities')
# plt.show()

plt.figure(3)
plt.subplot(6,2,1)
plt.title('Joint Pressures')
plt.plot(t,p[0:4,:].T/1000,'r')
plt.plot(t,pref[0:4,:].T/1000,'r--')
# plt.axis([0,60,0,400])
plt.ylabel('Jt 0 Pressures (kPa)')

plt.subplot(3,1,2)
plt.plot(t,p[4:8,:].T/1000,'g')
plt.plot(t,pref[4:8,:].T/1000,'g--')
# plt.axis([0,60,0,400])
plt.ylabel('Jt 1 Pressures (kPa)')

plt.subplot(3,1,3)
plt.plot(t,p[8:12,:].T/1000,'b')
plt.plot(t,pref[8:12,:].T/1000,'b--')
# plt.axis([0,60,0,400])
plt.ylabel('Jt 2 Pressures (kPa)')
plt.xlabel('Time (s)')

plt.show()
plt.pause(1000)
