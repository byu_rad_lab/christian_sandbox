#!/usr/bin/env python
import matplotlib.pyplot as plt
import scipy.io as sio
import numpy as np

data1 = sio.loadmat('./dataMay92019/MPC_noWeight.mat')
data2 = sio.loadmat('./dataMay92019/AdaptiveMPC_noWeight.mat')
data3 = sio.loadmat('./dataMay92019/IntegralMPC_noWeight.mat')
data4 = sio.loadmat('./dataMay92019/MPC_withWeight.mat')
data5 = sio.loadmat('./dataMay92019/AdaptiveMPC_withWeight.mat')
data6 = sio.loadmat('./dataMay92019/IntegralMPC_withWeight.mat')

t1 = data1['t'].flatten()
t2 = data2['t'].flatten()
t3 = data3['t'].flatten()
t4 = data4['t'].flatten()
t5 = data5['t'].flatten()
t6 = data6['t'].flatten()

p1 = np.vstack([data1['p00'],
               data1['p01'],
               data1['p02'],
               data1['p03'],
               data1['p10'],
               data1['p11'],
               data1['p12'],
               data1['p13'],
               data1['p20'],
               data1['p21'],
               data1['p22'],
               data1['p23']])
pref1 = np.vstack([data1['pref00'],
                  data1['pref01'],
                  data1['pref02'],
                  data1['pref03'],
                  data1['pref10'],
                  data1['pref11'],
                  data1['pref12'],
                  data1['pref13'],
                  data1['pref20'],
                  data1['pref21'],
                  data1['pref22'],
                  data1['pref23']])

q1 = np.vstack([data1['u0'],
               data1['v0'],
               data1['u1'],
               data1['v1'],
               data1['u2'],
               data1['v2']])

qd1 = np.vstack([data1['udot0'],
                data1['vdot0'],
                data1['udot1'],
                data1['vdot1'],
                data1['udot2'],
                data1['vdot2']])

##############################################33333
p2 = np.vstack([data2['p00'],
               data2['p01'],
               data2['p02'],
               data2['p03'],
               data2['p10'],
               data2['p11'],
               data2['p12'],
               data2['p13'],
               data2['p20'],
               data2['p21'],
               data2['p22'],
               data2['p23']])
pref2 = np.vstack([data2['pref00'],
                  data2['pref01'],
                  data2['pref02'],
                  data2['pref03'],
                  data2['pref10'],
                  data2['pref11'],
                  data2['pref12'],
                  data2['pref13'],
                  data2['pref20'],
                  data2['pref21'],
                  data2['pref22'],
                  data2['pref23']])

q2 = np.vstack([data2['u0'],
               data2['v0'],
               data2['u1'],
               data2['v1'],
               data2['u2'],
               data2['v2']])

qd2 = np.vstack([data2['udot0'],
                data2['vdot0'],
                data2['udot1'],
                data2['vdot1'],
                data2['udot2'],
                data2['vdot2']])

####################################################
p3 = np.vstack([data3['p00'],
               data3['p01'],
               data3['p02'],
               data3['p03'],
               data3['p10'],
               data3['p11'],
               data3['p12'],
               data3['p13'],
               data3['p20'],
               data3['p21'],
               data3['p22'],
               data3['p23']])
pref3 = np.vstack([data3['pref00'],
                  data3['pref01'],
                  data3['pref02'],
                  data3['pref03'],
                  data3['pref10'],
                  data3['pref11'],
                  data3['pref12'],
                  data3['pref13'],
                  data3['pref20'],
                  data3['pref21'],
                  data3['pref22'],
                  data3['pref23']])

q3 = np.vstack([data3['u0'],
               data3['v0'],
               data3['u1'],
               data3['v1'],
               data3['u2'],
               data3['v2']])

qd3 = np.vstack([data3['udot0'],
                data3['vdot0'],
                data3['udot1'],
                data3['vdot1'],
                data3['udot2'],
                data3['vdot2']])

#############################################################

p4 = np.vstack([data4['p00'],
               data4['p01'],
               data4['p02'],
               data4['p03'],
               data4['p10'],
               data4['p11'],
               data4['p12'],
               data4['p13'],
               data4['p20'],
               data4['p21'],
               data4['p22'],
               data4['p23']])
pref4 = np.vstack([data4['pref00'],
                  data4['pref01'],
                  data4['pref02'],
                  data4['pref03'],
                  data4['pref10'],
                  data4['pref11'],
                  data4['pref12'],
                  data4['pref13'],
                  data4['pref20'],
                  data4['pref21'],
                  data4['pref22'],
                  data4['pref23']])

q4 = np.vstack([data4['u0'],
               data4['v0'],
               data4['u1'],
               data4['v1'],
               data4['u2'],
               data4['v2']])

qd4 = np.vstack([data4['udot0'],
                data4['vdot0'],
                data4['udot1'],
                data4['vdot1'],
                data4['udot2'],
                data4['vdot2']])

###############################################

p5 = np.vstack([data5['p00'],
               data5['p01'],
               data5['p02'],
               data5['p03'],
               data5['p10'],
               data5['p11'],
               data5['p12'],
               data5['p13'],
               data5['p20'],
               data5['p21'],
               data5['p22'],
               data5['p23']])
pref5 = np.vstack([data5['pref00'],
                  data5['pref01'],
                  data5['pref02'],
                  data5['pref03'],
                  data5['pref10'],
                  data5['pref11'],
                  data5['pref12'],
                  data5['pref13'],
                  data5['pref20'],
                  data5['pref21'],
                  data5['pref22'],
                  data5['pref23']])

q5 = np.vstack([data5['u0'],
               data5['v0'],
               data5['u1'],
               data5['v1'],
               data5['u2'],
               data5['v2']])

qd5 = np.vstack([data5['udot0'],
                data5['vdot0'],
                data5['udot1'],
                data5['vdot1'],
                data5['udot2'],
                data5['vdot2']])

#################################################

p6 = np.vstack([data6['p00'],
               data6['p01'],
               data6['p02'],
               data6['p03'],
               data6['p10'],
               data6['p11'],
               data6['p12'],
               data6['p13'],
               data6['p20'],
               data6['p21'],
               data6['p22'],
               data6['p23']])
pref6 = np.vstack([data6['pref00'],
                  data6['pref01'],
                  data6['pref02'],
                  data6['pref03'],
                  data6['pref10'],
                  data6['pref11'],
                  data6['pref12'],
                  data6['pref13'],
                  data6['pref20'],
                  data6['pref21'],
                  data6['pref22'],
                  data6['pref23']])

q6 = np.vstack([data6['u0'],
               data6['v0'],
               data6['u1'],
               data6['v1'],
               data6['u2'],
               data6['v2']])

qd6 = np.vstack([data6['udot0'],
                data6['vdot0'],
                data6['udot1'],
                data6['vdot1'],
                data6['udot2'],
                data6['vdot2']])



#Position Plotting section
plt.figure(1)
plt.subplot(3, 2, 1)
plt.plot(t1,q1[0:2,:].T,'r--')
plt.plot(t1,q1[2:4,:].T,'g--')
plt.plot(t1,q1[4:6,:].T,'b--')
plt.grid(True)
plt.ylim(-1.5, 1.5)
plt.title('MPC w/o weight Joint Positions')
plt.ion()
plt.show()

plt.subplot(3, 2, 3)
plt.plot(t2,q2[0:2,:].T,'r--')
plt.plot(t2,q2[2:4,:].T,'g--')
plt.plot(t2,q2[4:6,:].T,'b--')
plt.grid(True)
plt.ylim(-1.5, 1.5)
plt.title('Adaptive MPC w/o weight Joint Positions')
plt.ion()
plt.show()

plt.subplot(3, 2, 5)
plt.plot(t3,q3[0:2,:].T,'r--')
plt.plot(t3,q3[2:4,:].T,'g--')
plt.plot(t3,q3[4:6,:].T,'b--')
plt.grid(True)
plt.ylim(-1.5, 1.5)
plt.title('Integral MPC w/o weight Joint Positions')
plt.ion()
plt.show()

plt.subplot(3, 2, 2)
plt.plot(t4,q4[0:2,:].T,'r--')
plt.plot(t4,q4[2:4,:].T,'g--')
plt.plot(t4,q4[4:6,:].T,'b--')
plt.grid(True)
plt.ylim(-1.5, 1.5)
plt.title('MPC w/ weight Joint Positions')
plt.ion()
plt.show()

plt.subplot(3, 2, 4)
plt.plot(t5,q5[0:2,:].T,'r--')
plt.plot(t5,q5[2:4,:].T,'g--')
plt.plot(t5,q5[4:6,:].T,'b--')
plt.grid(True)
plt.ylim(-1.5, 1.5)
plt.title('Adaptive MPC w/ weight Joint Positions')
plt.ion()
plt.show()

plt.subplot(3, 2, 6)
plt.plot(t6,q6[0:2,:].T,'r--')
plt.plot(t6,q6[2:4,:].T,'g--')
plt.plot(t6,q6[4:6,:].T,'b--')
plt.grid(True)
plt.ylim(-1.5, 1.5)
plt.title('Integral MPC w/ weight Joint Positions')
plt.ion()
plt.show()


#Pressure Plotting section
plt.figure(2)
plt.subplot(3, 2, 1)
plt.plot(t1,p1[0:2,:].T,'r--')
plt.plot(t1,p1[2:4,:].T,'g--')
plt.plot(t1,p1[4:6,:].T,'b--')
plt.plot(t1,p1[6:8,:].T,'r--')
plt.plot(t1,p1[8:10,:].T,'g--')
plt.plot(t1,p1[10:12,:].T,'b--')
plt.grid(True)
plt.ylim(0, 350000)
plt.title('MPC w/o weight Joint Positions')
plt.ion()
plt.show()

plt.subplot(3, 2, 3)
plt.plot(t2,p2[0:2,:].T,'r--')
plt.plot(t2,p2[2:4,:].T,'g--')
plt.plot(t2,p2[4:6,:].T,'b--')
plt.plot(t2,p2[6:8,:].T,'r--')
plt.plot(t2,p2[8:10,:].T,'g--')
plt.plot(t2,p2[10:12,:].T,'b--')
plt.grid(True)
plt.ylim(0, 350000)
plt.title('Adaptive MPC w/o weight Joint Positions')
plt.ion()
plt.show()

plt.subplot(3, 2, 5)
plt.plot(t3,p3[0:2,:].T,'r--')
plt.plot(t3,p3[2:4,:].T,'g--')
plt.plot(t3,p3[4:6,:].T,'b--')
plt.plot(t3,p3[6:8,:].T,'r--')
plt.plot(t3,p3[8:10,:].T,'g--')
plt.plot(t3,p3[10:12,:].T,'b--')
plt.grid(True)
plt.ylim(0, 350000)
plt.title('Integral MPC w/o weight Joint Positions')
plt.ion()
plt.show()

plt.subplot(3, 2, 2)
plt.plot(t4,p4[0:2,:].T,'r--')
plt.plot(t4,p4[2:4,:].T,'g--')
plt.plot(t4,p4[4:6,:].T,'b--')
plt.plot(t4,p4[6:8,:].T,'r--')
plt.plot(t4,p4[8:10,:].T,'g--')
plt.plot(t4,p4[10:12,:].T,'b--')
plt.grid(True)
plt.ylim(0, 350000)
plt.title('MPC w/ weight Joint Positions')
plt.ion()
plt.show()

plt.subplot(3, 2, 4)
plt.plot(t5,p5[0:2,:].T,'r--')
plt.plot(t5,p5[2:4,:].T,'g--')
plt.plot(t5,p5[4:6,:].T,'b--')
plt.plot(t5,p5[6:8,:].T,'r--')
plt.plot(t5,p5[8:10,:].T,'g--')
plt.plot(t5,p5[10:12,:].T,'b--')
plt.grid(True)
plt.ylim(0, 350000)
plt.title('Adaptive MPC w/ weight Joint Positions')
plt.ion()
plt.show()

plt.subplot(3, 2, 6)
plt.plot(t6,p6[0:2,:].T,'r--')
plt.plot(t6,p6[2:4,:].T,'g--')
plt.plot(t6,p6[4:6,:].T,'b--')
plt.plot(t6,p6[6:8,:].T,'r--')
plt.plot(t6,p6[8:10,:].T,'g--')
plt.plot(t6,p6[10:12,:].T,'b--')
plt.grid(True)
plt.ylim(0, 350000)
plt.title('Integral MPC w/ weight Joint Positions')
plt.ion()
plt.show()


#plt.plot(t,qd[0:2,:].T,'r')
#plt.plot(t,qd[2:4,:].T,'g')
#plt.plot(t,qd[4:6,:].T,'b')
#plt.title('Joint Velocities')
#plt.show()

#plt.figure(3)
#plt.plot(t,p[0:4,:].T,'r')
#plt.plot(t,p[4:8,:].T,'g')
#plt.plot(t,p[8:12,:].T,'b')
#plt.plot(t,pref[0:4,:].T,'r--')
#plt.plot(t,pref[4:8,:].T,'g--')
#plt.plot(t,pref[8:12,:].T,'b--')
#plt.title('Joint Pressures')
#plt.show()

plt.pause(1000)
