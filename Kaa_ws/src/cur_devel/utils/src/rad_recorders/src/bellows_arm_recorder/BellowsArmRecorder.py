#!/usr/bin/env python

from ros_msg.msg import SystemMinimalMsg
import rospy
import numpy as np
from collections import deque
import time
import scipy.io as sio

class BellowsArmRecorder:

   def __init__(self):

      self.t = deque()
      self.my_t = deque()      
      self.t0 = time.time()

      # For joint 0
      self.p00 = deque()
      self.p01 = deque()
      self.p02 = deque()
      self.p03 = deque()
      self.pref00 = deque()
      self.pref01 = deque()
      self.pref02 = deque()
      self.pref03 = deque()
      self.u0 = deque()
      self.v0 = deque()
      self.udot0 = deque()
      self.vdot0 = deque()
      self.my_u0 = deque()
      self.my_v0 = deque()
      self.my_udot0 = deque()
      self.my_vdot0 = deque()

      # For joint 1
      self.p10 = deque()
      self.p11 = deque()
      self.p12 = deque()
      self.p13 = deque()
      self.pref10 = deque()
      self.pref11 = deque()
      self.pref12 = deque()
      self.pref13 = deque()
      self.u1 = deque()
      self.v1 = deque()
      self.udot1 = deque()
      self.vdot1 = deque()
      self.my_u1 = deque()
      self.my_v1 = deque()
      self.my_udot1 = deque()
      self.my_vdot1 = deque()

      # For joint 2
      self.p20 = deque()
      self.p21 = deque()
      self.p22 = deque()
      self.p23 = deque()
      self.pref20 = deque()
      self.pref21 = deque()
      self.pref22 = deque()
      self.pref23 = deque()
      self.u2 = deque()
      self.v2 = deque()
      self.udot2 = deque()
      self.vdot2 = deque()
      self.my_u2 = deque()
      self.my_v2 = deque()
      self.my_udot2 = deque()
      self.my_vdot2 = deque()

      self.system_state_sub = rospy.Subscriber('/atheris/system_minimal',SystemMinimalMsg,self.system_state_cb,queue_size=1)

   def system_state_cb(self,msg):

       self.t.append(time.time()-self.t0)

       # For joint 0
       self.p00.append(msg.joints_est[0].prs_bel[0])
       self.p01.append(msg.joints_est[0].prs_bel[1])
       self.p02.append(msg.joints_est[0].prs_bel[2])
       self.p03.append(msg.joints_est[0].prs_bel[3])
       self.pref00.append(msg.joints_ref[0].prs_bel[0])
       self.pref01.append(msg.joints_ref[0].prs_bel[1])
       self.pref02.append(msg.joints_ref[0].prs_bel[2])
       self.pref03.append(msg.joints_ref[0].prs_bel[3])
       self.u0.append(msg.joints_est[0].q[0])
       self.v0.append(msg.joints_est[0].q[1])
       self.udot0.append(msg.joints_est[0].qd[0])
       self.vdot0.append(msg.joints_est[0].qd[1])

       # For joint 1
       self.p10.append(msg.joints_est[1].prs_bel[0])
       self.p11.append(msg.joints_est[1].prs_bel[1])
       self.p12.append(msg.joints_est[1].prs_bel[2])
       self.p13.append(msg.joints_est[1].prs_bel[3])
       self.pref10.append(msg.joints_ref[1].prs_bel[0])
       self.pref11.append(msg.joints_ref[1].prs_bel[1])
       self.pref12.append(msg.joints_ref[1].prs_bel[2])
       self.pref13.append(msg.joints_ref[1].prs_bel[3])
       self.u1.append(msg.joints_est[1].q[0])
       self.v1.append(msg.joints_est[1].q[1])
       self.udot1.append(msg.joints_est[1].qd[0])
       self.vdot1.append(msg.joints_est[1].qd[1])

       # For joint 2
       self.p20.append(msg.joints_est[2].prs_bel[0])
       self.p21.append(msg.joints_est[2].prs_bel[1])
       self.p22.append(msg.joints_est[2].prs_bel[2])
       self.p23.append(msg.joints_est[2].prs_bel[3])
       self.pref20.append(msg.joints_ref[2].prs_bel[0])
       self.pref21.append(msg.joints_ref[2].prs_bel[1])
       self.pref22.append(msg.joints_ref[2].prs_bel[2])
       self.pref23.append(msg.joints_ref[2].prs_bel[3])
       self.u2.append(msg.joints_est[2].q[0])
       self.v2.append(msg.joints_est[2].q[1])
       self.udot2.append(msg.joints_est[2].qd[0])
       self.vdot2.append(msg.joints_est[2].qd[1])

   def save_data(self,filename='./data/data.mat'):
      data = {'p00':self.p00,
              'p01':self.p01,
              'p02':self.p02,
              'p03':self.p03,
              'pref00':self.pref00,
              'pref01':self.pref01,
              'pref02':self.pref02,
              'pref03':self.pref03,
              'u0':self.u0,
              'v0':self.v0,
              'udot0':self.udot0,
              'vdot0':self.vdot0,
              'p10':self.p10,
              'p11':self.p11,
              'p12':self.p12,
              'p13':self.p13,
              'pref10':self.pref10,
              'pref11':self.pref11,
              'pref12':self.pref12,
              'pref13':self.pref13,
              'u1':self.u1,
              'v1':self.v1,
              'udot1':self.udot1,
              'vdot1':self.vdot1,
              'p20':self.p20,
              'p21':self.p21,
              'p22':self.p22,
              'p23':self.p23,
              'pref20':self.pref20,
              'pref21':self.pref21,
              'pref22':self.pref22,
              'pref23':self.pref23,
              'u2':self.u2,
              'v2':self.v2,
              'udot2':self.udot2,
              'vdot2':self.vdot2,
              't':self.t}
       
      sio.savemat(filename,data)
      print("Saved data")
   

       
if __name__=='__main__':
   
   rospy.init_node('Record_robot_states_and_inputs',anonymous=True)
   
   r = BellowsArmRecorder()

   print("Recording")

   while not rospy.is_shutdown():
       rospy.sleep(.01)

   r.save_data('./data/NEMPC_BLOW_MOLDED_ARM_without_integrator4.mat')
   # r.save_data('./data/EMPC_BLOW_MOLDED_ARM_2pt.mat')
   # r.save_data('./data/EMPC_BLOW_MOLDED_ARM_3jts.mat')   

