#!/usr/bin/env python

from std_msgs.msg import String
from ros_msg.msg import SystemMinimalMsg
from data_recorder import Recorder
import time

if __name__ == '__main__':
    start_time = time.time()

    def write_blow_mold_data(msg):
        return str(time.time()-start_time)+',\t'+str(msg.joints_ref[0].prs_bel[0])+',\t'+str(msg.joints_ref[0].prs_bel[1])+',\t'+str(msg.joints_ref[0].prs_bel[2])+',\t'+str(msg.joints_ref[0].prs_bel[3])+',\t'+str(msg.joints_ref[1].prs_bel[0])+',\t'+str(msg.joints_ref[1].prs_bel[1])+',\t'+str(msg.joints_ref[1].prs_bel[2])+',\t'+str(msg.joints_ref[1].prs_bel[3])+',\t'+str(msg.joints_ref[2].prs_bel[0])+',\t'+str(msg.joints_ref[2].prs_bel[1])+',\t'+str(msg.joints_ref[2].prs_bel[2])+',\t'+str(msg.joints_ref[2].prs_bel[3])+',\t'+str(msg.joints_est[0].prs_bel[0])+',\t'+str(msg.joints_est[0].prs_bel[1])+',\t'+str(msg.joints_est[0].prs_bel[2])+',\t'+str(msg.joints_est[0].prs_bel[3])+',\t'+str(msg.joints_est[1].prs_bel[0])+',\t'+str(msg.joints_est[1].prs_bel[1])+',\t'+str(msg.joints_est[1].prs_bel[2])+',\t'+str(msg.joints_est[1].prs_bel[3])+',\t'+str(msg.joints_est[2].prs_bel[0])+',\t'+str(msg.joints_est[2].prs_bel[1])+',\t'+str(msg.joints_est[2].prs_bel[2])+',\t'+str(msg.joints_est[2].prs_bel[3])+',\t'+str(msg.joints_est[0].q[0])+',\t'+str(msg.joints_est[0].q[1])+',\t'+str(msg.joints_est[0].qd[0])+',\t'+str(msg.joints_est[0].qd[1])+',\t'+str(msg.joints_est[1].q[0])+',\t'+str(msg.joints_est[1].q[1])+',\t'+str(msg.joints_est[1].qd[0])+',\t'+str(msg.joints_est[0].qd[1])+',\t'+str(msg.joints_est[2].q[0])+',\t'+str(msg.joints_est[2].q[1])+',\t'+str(msg.joints_est[2].qd[0])+',\t'+str(msg.joints_est[2].qd[1])+'\n'

    title_list = ['time,\tpref00,\tpref01,\tpref02,\tpref03,\tpref10,\tpref11,\tpref12,\tpref13,\tpref20,\tpref21,\tpref22,\tpref23,\tp00,\tp01,\tp02,\tp03,\tp10,\tp11,\tp12,\tp13,\tp20,\tp21,\tp22,\tp23,\tu0,\tv0,\tudot0,\tvdot0,\tu1,\tv1,\tudot1,\tvdot1,\tu2,\tv2,\tudot2,\tvdot2\n']

    msg_type_list = [SystemMinimalMsg]

    topic_list = ['atheris/system_minimal']

    function_list = [write_blow_mold_data]

    recorder_object = Recorder(topic_list, 
                               function_list, 
                               title_list, 
                               msg_type_list, 
                               './', # relative path to save file
                               '', # file prefix
                               0.005) # amount of time to sleep between efforts to write to disk
    


