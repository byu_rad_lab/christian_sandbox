#!/usr/bin/env python

import rosbag
import rospy
import csv
from std_msgs.msg import Int32, String
from geometry_msgs.msg import WrenchStamped, PoseStamped, Pose
import scipy.io as sio
from tf2_msgs.msg import TFMessage # message for vive pucks
# from king_louie.msg import sensors as sensor # message for king louie pressures
# from byu_robot.msg import states as states_msg # message for king louie joint angles
# from king_louie.msg import mpc_msg # message for king louie desired pressures
import os

#script Vallan used to get the important data out of the bag files on the leg experiments


if __name__ == '__main__':

    bagpath = os.path.expanduser('~') + '/Downloads/marc_and_curtis_data/force_trial_06_155N.bag'
    # bagpath = os.path.expanduser('~') + '/git/byu/cur_devel/utils/src/rad_recorders/src/2019-04-20-13-06-44_0.bag'
    # bagpath = os.path.expanduser('~') + '/git/byu/cur_devel/utils/src/rad_recorders/src/2018-02-07-13-20-53.bag'
    print(bagpath)

    # dict_out = {}




    topic_list = ['/shere_khan_uv/estimation_minimal','/pressure_cmds', '/ftnode1/netft_data', '/payload_score','/force_cmd','/carpet_estimate','/atheris/system_minimal']

    print("monkey")

    bag = rosbag.Bag(bagpath)


    bag_start_time = bag.get_start_time()
    print("start time: ", bag_start_time)

    print("Message count: ", bag.get_message_count())
    # print "type and topic info: ", bag.get_type_and_topic_info()
    # cow = bag.get_type_and_topic_info()
    # print "first: ", cow[0]
    # print "read messages: ", bag.read_messages()

    print("Opened bag and starting for loop")

    time_p_cmd = []
    p_cmd = []

    time_p_est = []
    p_est = []
    p_sup = []

    time_uv_est = []
    uv_est = []

    time_f_est = []
    f_est = []

    time_payload = []
    payload_pred = []

    time_f_cmd = []
    f_cmd = []

    time_carpet_est = []
    carpet_est = []



    for topic_name, msg, t in bag.read_messages(topics=topic_list):
        time = t.to_sec() - bag_start_time
        # print "time2: ", time
        # print topic_name
        # print msg
        if topic_name == '/shere_khan_uv/estimation_minimal':
            time_uv_est.append(time)

            uv_est_msg = []
            for i in range(len(msg.joints_est)):
                for j in range(len(msg.joints_est[i].q)):
                    uv_est_msg.append(msg.joints_est[i].q[j])

            uv_est.append(uv_est_msg)

        if topic_name == '/pressure_cmds':
            time_p_cmd.append(time)
            p_cmd.append(msg.data)

        if topic_name == '/ftnode1/netft_data':
            time_f_est.append(time)

            force = msg.wrench.force
            torque = msg.wrench.torque
            f_est.append([force.x, force.y, force.z, torque.x, torque.y, torque.z])

        if topic_name == '/payload_score':
            time_payload.append(time)
            payload_pred.append(msg.data)

        if topic_name == '/force_cmd':
            time_f_cmd.append(time)

            force = msg.wrench.force
            torque = msg.wrench.torque
            f_cmd.append([force.x, force.y, force.z, torque.x, torque.y, torque.z])
            print(f_cmd)

        if topic_name == '/carpet_estimate':
            time_carpet_est.append(time)

            trans = msg.transform.translation
            carpet_est.append([trans.x, trans.y, trans.z])

        if topic_name == '/atheris/system_minimal':
            time_p_est.append(time)

            p_est_msg = []
            p_sup_msg = []
            for i in range(len(msg.joints_est)):
                p_sup_msg.append(msg.joints_est[i].prs_sup)
                for j in range(len(msg.joints_est[i].prs_bel)):
                    p_est_msg.append(msg.joints_est[i].prs_bel[j])

            p_est.append(p_est_msg)
            p_sup.append(p_sup_msg)



    dict_out = {'time_p_cmd': time_p_cmd, 'p_cmd': p_cmd, 'time_p_est': time_p_est, 'p_est': p_est,
                'p_sup': p_sup, 'time_uv_est': time_uv_est, 'uv_est': uv_est, 'time_f_est':time_f_est,
                'f_est':f_est, 'time_payload':time_payload, 'payload_pred':payload_pred, 'time_f_cmd': time_f_cmd,
                'f_cmd':f_cmd, 'time_carpet_est':time_carpet_est, 'carpet_est':carpet_est}

    sio.savemat(bagpath[-23:-4], dict_out)
    bag.close()
