load('baxter_moving_faster_raw.mat')

q0 = interp1(t_q,q0,t_qdes);
q1 = interp1(t_q,q1,t_qdes);
q2 = interp1(t_q,q2,t_qdes);
q3 = interp1(t_q,q3,t_qdes);
q4 = interp1(t_q,q4,t_qdes);
q5 = interp1(t_q,q5,t_qdes);
q6 = interp1(t_q,q6,t_qdes);

qd0 = interp1(t_qd,qd0,t_qdes);
qd1 = interp1(t_qd,qd1,t_qdes);
qd2 = interp1(t_qd,qd2,t_qdes);
qd3 = interp1(t_qd,qd3,t_qdes);
qd4 = interp1(t_qd,qd4,t_qdes);
qd5 = interp1(t_qd,qd5,t_qdes);
qd6 = interp1(t_qd,qd6,t_qdes);

t = t_qdes(2:end-1);
q0 = q0(2:end-1);
q1 = q1(2:end-1);
q2 = q2(2:end-1);
q3 = q3(2:end-1);
q4 = q4(2:end-1);
q5 = q5(2:end-1);
q6 = q6(2:end-1);

qd0 = qd0(2:end-1);
qd1 = qd1(2:end-1);
qd2 = qd2(2:end-1);
qd3 = qd3(2:end-1);
qd4 = qd4(2:end-1);
qd5 = qd5(2:end-1);
qd6 = qd6(2:end-1);

qdes0 = qdes0(2:end-1);
qdes1 = qdes1(2:end-1);
qdes2 = qdes2(2:end-1);
qdes3 = qdes3(2:end-1);
qdes4 = qdes4(2:end-1);
qdes5 = qdes5(2:end-1);
qdes6 = qdes6(2:end-1);