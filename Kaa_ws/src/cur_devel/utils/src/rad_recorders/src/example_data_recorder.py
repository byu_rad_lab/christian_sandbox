#!/usr/bin/env python

from std_msgs.msg import String
from ros_msg.msg import SystemMinimalMsg
from data_recorder import Recorder
import time

if __name__ == '__main__':
    start_time = time.time()

    def write_string(msg):
        return str(time.time()-start_time)+',\t'+str(msg.joints_est[0].prs_bel[0])+'\n'

    title_list = ['time, \t pressure 1']

    msg_type_list = [SystemMinimalMsg]

    topic_list = ['/atheris/system_minimal']

    function_list = [write_string]

    recorder_object = Recorder(topic_list, 
                               function_list, 
                               title_list, 
                               msg_type_list, 
                               './', # relative path to save file
                               '', # file prefix
                               0.005) # amount of time to sleep between efforts to write to disk
    


