#!/usr/bin/env python

from data_recorder import Recorder
from std_msgs.msg import Float32MultiArray
import time

start_t = time.time()
    
if __name__ == '__main__':


    def write_string(msg):
        return str(time.time()-start_t)+',\t'+str(msg.data[0])+',\t'+str(msg.data[1])+'\n'

    title_list = ['t,\tqdes0,\tqdes1\n','t,\tq0,\tq1\n','t,\tqd0,\tqd1\n']

    msg_type_list = [Float32MultiArray,Float32MultiArray,Float32MultiArray]

    topic_list = ['qdes','q','qd']

    function_list = [write_string,write_string,write_string]

    recorder_object = Recorder(topic_list, 
                               function_list, 
                               title_list, 
                               msg_type_list, 
                               './', # relative path to save file
                               './', # file prefix
                               0.005) # amount of time to sleep between efforts to write to disk
    


