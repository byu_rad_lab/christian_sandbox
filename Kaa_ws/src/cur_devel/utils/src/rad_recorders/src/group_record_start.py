#!/usr/bin/env python

import rospy,sys,os,select,time,re
from rad_recorders.srv import LogData
import matplotlib.pyplot as plt
import numpy as np

# Removes first file that I can't stop
def remove_first_file(file_path,topics):
    record_srv = rospy.ServiceProxy('log_data',LogData)
    file_name = file_path
    record_srv(file_path)
    record_srv(file_path)
    for i in range(len(topics)):
        os.remove(file_path + '/data_'+topics[i]+'.txt')

# Plots x data of Table to make sure data was got
def plot_prev_data(file_path,file_suffix):
    f = open(file_path+'/'+file_suffix+'Table.txt')
    p = f.readlines()
    f.close()
    for i in range(len(p)):
        p[i] = re.sub('[\s+]','',p[i])
        p[i] = p[i].split(',')
    for i in range(len(p)-1):
        for j in range(len(p[i+1])):
            p[i+1][j] = float(p[i+1][j])
    p.pop(0)
    plt.plot(np.asarray(p)[:,2])
    plt.show()
        
# Begins recording given file path and file suffix
def start_recording(file_path,file_suffix,ploton = False):
    record_srv = rospy.ServiceProxy('log_data',LogData)
    file_name = file_path + '/' + file_suffix
    record_srv(file_name)

    #This segment runs until user hits enter (captures data until done)                  
    print("Collecting data","\n---------------","\n---------------","\n---------------","\n---------------","\n---------------","\n---------------","\n---------------")
    print("Press 'Enter' to stop test.")
    while True:
        if sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
           line = input()
           break
    record_srv(file_name) # stops service and closes file

    # Clear window
    os.system('cls' if os.name == 'nt' else 'clear')

    # If you want to make sure data was collected from cortex
    if ploton == True:
        plot_prev_data(file_path,file_suffix)

if __name__ == "__main__":
    # Make sure record_init.py is running
    time.sleep(1.0)

    # Start node
    rospy.init_node('record_start',anonymous = True)

    # Get Needed Parameters, otherwise quit
    if rospy.has_param('file_path') and rospy.has_param('topics'):
        file_path = str(rospy.get_param('file_path'))
        topics = rospy.get_param('suffix')
        plot = rospy.get_param('plot')
    else:
        print('Need to apply file_path to rosparam server')
        sys.exit()

    group_number = input('Enter Group Number (01 02, etc):\t')
    new_file_path = file_path + '/' + group_number
    os.mkdir(new_file_path)        

    first_time = True
    new_trial = 'y'
    counter8 = '00'
    counter9 = '00'
    
    while not rospy.is_shutdown():
        # First time through, remove file since it doesn't have naming capabilities
        if first_time:
            remove_first_file(file_path,topics)
            first_time = False
            group_prefix = input('Input Controller Identifier (trig or nn):\t')

        # Otherwise, make new files as desired
        while new_trial == 'y':
            # Take in file task number
            print('Beginning Recording:\n')
            file_suffix = input('Input task number (08 for translation, 09 for rotation):\t')

            # apply file suffix and increment counter
            if file_suffix == '08':
                counter8 = str(int('9' + counter8) + 1)[1:]
                file_suffix = group_prefix + '_' + file_suffix + '_' + counter8 + '_'
            elif file_suffix == '09':
                counter9 = str(int('9' + counter9) + 1)[1:]
                file_suffix = group_prefix + '_' + file_suffix + '_' + counter9 + '_'

            # Begin Recording
            start_recording(new_file_path,file_suffix,ploton = plot)
            new_trial = input('Start new trial? (y/n)\n\n')
        break
