/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#ifndef SRC_ROS_UTILS_INCLUDE_ROS_UTILS_ROS_PARAM_H_
#define SRC_ROS_UTILS_INCLUDE_ROS_UTILS_ROS_PARAM_H_

#include <primitives/exception.h>

#include <ros/ros.h>

#include <string>

namespace exception {
class RosParamException : public primitives::exception::Exception {
 public:
  explicit RosParamException(const std::string &msg) : primitives::exception::Exception(msg) {}
  ~RosParamException() throw() {}
};

class RosParamVectorLenException : public RosParamException {
 public:
  explicit RosParamVectorLenException(const std::string &msg) : RosParamException(msg) {}
  ~RosParamVectorLenException() throw() {}
};
}  // namespace exception

namespace ros_utils {

class RosParam {
 public:
  RosParam() {}

  ~RosParam() {}

  template <class T>
  static void getParam(const std::string &key, T &val);  // NOLINT(runtime/references)

  template <class T>
  static void setParam(const std::string &key, const T &val);

  static void throwLoadException(const std::string &key);
  static void throwVectorLenException(const std::string &key, const int expected,
                                      const int actual);
};

template <class T>
void RosParam::getParam(const std::string &key, T &val) {
  if (ros::param::get(key, val)) {
    return;
  }
  throwLoadException(key);
}

template <class T>
void RosParam::setParam(const std::string &key, const T &val) {
  ros::param::set(key, val);
}

}  // namespace ros_utils

#endif  // SRC_ROS_UTILS_INCLUDE_ROS_UTILS_ROS_PARAM_H_
