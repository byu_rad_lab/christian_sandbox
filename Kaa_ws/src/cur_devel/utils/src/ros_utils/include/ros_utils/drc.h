// Otherlab, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
#ifndef SRC_ROS_UTILS_INCLUDE_ROS_UTILS_DRC_H_
#define SRC_ROS_UTILS_INCLUDE_ROS_UTILS_DRC_H_
#include <dynamic_reconfigure/server.h>
#include <ros/ros.h>

#include <string>

namespace ros_utils {

template <class data, class owner>
class DRCServer {
  typedef void (owner::*CallbackFunction)(data &config,  // NOLINT(runtime/references)
                                          uint32_t level);

  typedef void (owner::*CallbackFunctionIndexed)(
      data &config,  // NOLINT(runtime/references)
      uint32_t level,
      int index);

  typedef void (owner::*CallbackFunctionSubindexed)(
      data &config,  // NOLINT(runtime/references)
      uint32_t level,
      int index,
      int subindex);

 public:
  DRCServer() {}
  ~DRCServer() {
    if (server_) {
      delete server_;
    }
  }

  void Init(const std::string &name,
            ros::NodeHandle n,
            CallbackFunction callback,
            owner *object) {
    typename dynamic_reconfigure::Server<data>::CallbackType cb =
        boost::bind(callback, object, _1, _2);

    server_ = new dynamic_reconfigure::Server<data>(ros::NodeHandle(n, name));
    server_->setCallback(cb);
  }

  void Init(const std::string &name,
            ros::NodeHandle n,
            CallbackFunctionIndexed callback,
            owner *object,
            int index) {
    typename dynamic_reconfigure::Server<data>::CallbackType cb =
        boost::bind(callback, object, _1, _2, index);

    server_ = new dynamic_reconfigure::Server<data>(ros::NodeHandle(n, name));
    server_->setCallback(cb);
  }

  void Init(const std::string &name,
            ros::NodeHandle n,
            CallbackFunctionSubindexed callback,
            owner *object,
            int index,
            int subindex) {
    typename dynamic_reconfigure::Server<data>::CallbackType cb =
        boost::bind(callback, object, _1, _2, index, subindex);

    server_ = new dynamic_reconfigure::Server<data>(ros::NodeHandle(n, name));
    server_->setCallback(cb);
  }

 private:
  dynamic_reconfigure::Server<data> *server_ = NULL;
};

}  // namespace ros_utils

#endif  // SRC_ROS_UTILS_INCLUDE_ROS_UTILS_DRC_H_
