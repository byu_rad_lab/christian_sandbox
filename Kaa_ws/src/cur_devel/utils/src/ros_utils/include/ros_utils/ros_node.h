/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#ifndef SRC_ROS_UTILS_INCLUDE_ROS_UTILS_ROS_NODE_H_
#define SRC_ROS_UTILS_INCLUDE_ROS_UTILS_ROS_NODE_H_

#include <primitives/exception.h>
#include <primitives/task.h>

#include <ros/ros.h>

#include <signal.h>
#include <string>

namespace exception {
class RosNodeException : public primitives::exception::Exception {
 public:
  explicit RosNodeException(const std::string &msg)
      : primitives::exception::Exception(msg) {}
  ~RosNodeException() throw() {}
};

}  // namespace exception

namespace ros_utils {

class RosNode : public primitives::task::Task {
 public:
  RosNode(const std::string &name,
          ros::Rate loopRate,
          const ros::NodeHandle &parentRosNodeHandle);
  RosNode(const std::string &name,
          const std::string &parentName,
          ros::Rate loopRate,
          const ros::NodeHandle &parentRosNodeHandle);
  virtual ~RosNode();

  void run(void);

 protected:
  void doIterate(void);
  void doInit(void);
  void doCleanup(void);

  virtual void appInit(void) {}
  virtual void appIterate(void) {}
  virtual void appCleanup(void) {}

  ros::Rate loopRate_;
  ros::NodeHandle rosNodeHandle_;

 private:
  static void sigIntHandler(int sig);
  static void shutdownCallback(
      XmlRpc::XmlRpcValue &params,   // NOLINT(runtime/references)
      XmlRpc::XmlRpcValue &result);  // NOLINT(runtime/references)
  static sig_atomic_t volatile shutdown_;
};

}  // namespace ros_utils
#endif  // SRC_ROS_UTILS_INCLUDE_ROS_UTILS_ROS_NODE_H_
