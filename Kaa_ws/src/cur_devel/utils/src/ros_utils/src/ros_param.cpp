/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#include "ros_utils/ros_param.h"

#include <ros/ros.h>

#include <string>

namespace ros_utils {

void RosParam::throwLoadException(const std::string &key) {
  std::stringstream ss;
  ss << "Failed to load parameter from parameter server: \"" << key << "\"";
  ROS_ERROR("%s", ss.str().c_str());
  throw exception::RosParamException(ss.str());
}

void RosParam::throwVectorLenException(const std::string &key, const int expected,
                                       const int actual) {
  std::stringstream ss;
  ss << "Parameter vector is wrong size: \"" << key << "\". Expected " << expected << " but got "
     << actual;
  ROS_ERROR("%s", ss.str().c_str());
  throw exception::RosParamVectorLenException(ss.str());
}

}  // namespace ros_utils
