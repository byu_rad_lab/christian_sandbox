// Otherlab, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of COMPANY.
// The intellectual and technical concepts contained herein are proprietary to COMPANY
// and may be covered by U.S. and Foreign Patents, patents in process, and are
// protected  by trade secret or copyright law. Dissemination of this information or
// reproduction of this material is strictly forbidden unless prior written permission
// is obtained from COMPANY. Access to the source code contained herein is hereby
// forbidden to anyone except current COMPANY employees, managers or contractors who
// have executed Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended publication or
// disclosure of this source code, which includes information that is confidential
// and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
// MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE
// OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY
// PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE
// RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
// OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
// MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
#include "ros_utils/ros_node.h"

#include <ros/xmlrpc_manager.h>

#include <string>

namespace ros_utils {

RosNode::RosNode(const std::string &name,
                 ros::Rate loopRate,
                 const ros::NodeHandle &parentRosNodeHandle)
    : primitives::task::Task(name),
      loopRate_(loopRate),
      rosNodeHandle_(parentRosNodeHandle) {}

RosNode::RosNode(const std::string &name,
                 const std::string &parentName,
                 ros::Rate loopRate,
                 const ros::NodeHandle &parentRosNodeHandle)
    : primitives::task::Task(name, parentName),
      loopRate_(loopRate),
      rosNodeHandle_(parentRosNodeHandle, name) {}

RosNode::~RosNode() {}

void RosNode::run(void) {
  Init();
  Iterate();
  Cleanup();
}

void RosNode::doInit(void) {
  // Override the SIGINT shutdown
  signal(SIGINT, RosNode::sigIntHandler);

  // Override XMLRPC shutdown
  ros::XMLRPCManager::instance()->unbind("shutdown");
  ros::XMLRPCManager::instance()->bind("shutdown", RosNode::shutdownCallback);
  Info("Initializing");
  appInit();
}

void RosNode::doIterate(void) {
  Info("Running main loop");
  while (!RosNode::shutdown_) {
    appIterate();
    ros::spinOnce();
    loopRate_.sleep();
    iterationCount_++;
  }
  Info("Exiting main loop");
}

void RosNode::doCleanup(void) {
  Warn("Cleaning up");
  appCleanup();
}

void RosNode::sigIntHandler(int sig) {
  RosNode::shutdown_ = 1;
  ROS_WARN("Shutdown request received. Reason: [Ctrl+C]");
}

void RosNode::shutdownCallback(XmlRpc::XmlRpcValue &params, XmlRpc::XmlRpcValue &result) {
  int num_params = 0;
  if (params.getType() == XmlRpc::XmlRpcValue::TypeArray) num_params = params.size();
  if (num_params > 1) {
    std::string reason = params[1];
    ROS_WARN("Shutdown request received. Reason: [%s]", reason.c_str());
    RosNode::shutdown_ = 1;  // Set flag
  }
  result = ros::xmlrpc::responseInt(1, "", 0);
}
sig_atomic_t volatile RosNode::shutdown_ = 0;

}  // namespace ros_utils
