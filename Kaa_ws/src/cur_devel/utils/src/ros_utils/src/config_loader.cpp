// Otherlab, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
#include <ros/ros.h>
#include <ros/package.h>

#include <ros_utils/ros_param.h>
#include <primitives/exception.h>
#include <primitives/stringprintf.h>

#include <string>
#include <fstream>
#include <vector>
#include <iostream>

class ExceptionConfigurationLoader : public primitives::exception::Exception {
 public:
  explicit ExceptionConfigurationLoader(const std::string& msg)
      : primitives::exception::Exception(msg) {}

  virtual ~ExceptionConfigurationLoader() throw() {}
};

/*
 * Main entry point for the application
 */
int main(int argc, char** argv) {
  static const std::string name("config_loader");

  /*
   * Initialize the ros node
   */
  ros::init(argc, argv, name, ros::init_options::NoSigintHandler);  // Must use
                                                                    // NoSigintHandler
                                                                    // option
                                                                    //  ros::NodeHandle n;

  if (argc < 2) {
    ROS_ERROR("No package name specified!");
    throw ExceptionConfigurationLoader("No package name specified!");
  } else if (argc < 4) {
    throw ExceptionConfigurationLoader(primitives::stringsprintf(
        "Package provided (%s) but no filename/param_name pairs specified!", argv[1]));
  }

  std::string package_name = std::string(argv[1]);

  ROS_INFO("Looking for path to package %s", package_name.c_str());
  std::string base_path = ros::package::getPath(package_name);
  if (base_path == "") {
    throw ExceptionConfigurationLoader(primitives::stringsprintf(
        "Base file path evaluated as blank string! This may mean the package %s does not "
        "exist.",
        package_name.c_str()));
  } else {
    ROS_INFO("Base file path: %s", base_path.c_str());
  }

  for (int i = 0; i < (argc - 2); i += 2) {
    std::string file_name(argv[i + 2]);
    std::string param_name(argv[i + 3]);

    ROS_INFO("Loading %s to param server as string", file_name.c_str());

    std::string full_path = base_path + "/yaml/" + file_name;
    std::ifstream tfilestream(full_path);
    std::string filestring((std::istreambuf_iterator<char>(tfilestream)),
                           std::istreambuf_iterator<char>());

    if (filestring == "") {
      throw ExceptionConfigurationLoader(primitives::stringsprintf(
          "%s evaluated as blank string! This may mean the file did not exist.",
          file_name.c_str()));
    }

    ros_utils::RosParam::setParam(param_name, filestring);
  }

  return 0;
}
