#ifndef STABILITY_UTILS_H_
#define STABILITY_UTILS_H_

#include <algorithm>
#include <limits>
#include <math.h>
#include <vector>
#include <eigen3/Eigen/Dense>

namespace stability_utils {

    struct Point {
        double x;
        double y;

        Point()
        {
            x = 0.0;
            y = 0.0;
        }

        Point(double xIn, double yIn)
        {
            x = xIn;
            y = yIn;
        }

        double norm()
        {
            return sqrt(x*x + y*y);
        }

    };

    Point operator+(const Point& lhs, const Point& rhs)
    {
        Point output;

        output.x = lhs.x + rhs.x;
        output.y = lhs.y + rhs.y;

        return output;
    }

    Point operator-(const Point& lhs, const Point& rhs)
    {
        Point output;

        output.x = lhs.x - rhs.x;
        output.y = lhs.y - rhs.y;

        return output;
    }

    double operator*(const Point& lhs, const Point& rhs)
    {
        double output;

        output = lhs.x*rhs.x + lhs.y*rhs.y;

        return output;
    }

    Point operator*(const double lhs, const Point& rhs)
    {
        Point output;

        output.x = lhs*rhs.x;
        output.y = lhs*rhs.y;

        return output;
    }

    Point operator*(const Point& lhs, const double rhs)
    {
        Point output;

        output = rhs*lhs;

        return output;
    }

    bool operator<(const Point& lhs, const Point& rhs)
    {
        return lhs.x < rhs.x || (lhs.x == rhs.x && lhs.y < rhs.y);
    }

    struct Vertices {
        std::vector<Point> vertices;

        Vertices()
        {
        }

        Vertices(std::vector<Point> verticesIn)
        {
            vertices = verticesIn;
        }
    };

    double WrapAngle(double angle)
    {
        while(angle > M_PI)
        {
            angle -= 2.0*M_PI;
        }

        while(angle < -M_PI)
        {
            angle += 2.0*M_PI;
        }

        return angle;
    }

    double Cross(const Point& O, const Point& A, const Point& B)
    {
        return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
    }

    Vertices FindConvexHull(std::vector<Point>& points) //this algorithm was modified from the code found at https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain
    {
        int n = points.size(); 
        int k = 0;

        Vertices vertices;

        vertices.vertices.resize(2*n);

        // Sort points lexicographically
        std::sort(points.begin(), points.end());

        // Build lower hull
        for (int i =0; i < n; ++i)
        {
            while (k >= 2 && Cross(vertices.vertices[k-2], vertices.vertices[k-1], points[i]) <= 0)
            {
                k--;
            }
            vertices.vertices[k++] = points[i];
        }

        // Build upper hull
        for (int i = n-2, t = k+1; i >=0; i--)
        {
            while (k >= t && Cross(vertices.vertices[k-2], vertices.vertices[k-1], points[i]) <= 0)
            {
                k--;
            }
            vertices.vertices[k++] = points[i];
        }

        vertices.vertices.resize(k-1);

        return vertices;

    }

    double StaticStabilityMargin(const Vertices& support_polygon, const Point& CoG)
    {
        double output = std::numeric_limits<double>::max();

        for(int i = 0; i<support_polygon.vertices.size(); ++i)
        {
            Point P_vertex1_CoG = CoG - support_polygon.vertices[i];
            Point P_vertex1_vertex2;
            if (i<support_polygon.vertices.size()-1)
            {
                P_vertex1_vertex2 = support_polygon.vertices[i+1] - support_polygon.vertices[i];
            }
            else
            {
                P_vertex1_vertex2 = support_polygon.vertices[0] - support_polygon.vertices[i];
            }

            Point projection = (P_vertex1_CoG*P_vertex1_vertex2)/(P_vertex1_vertex2*P_vertex1_vertex2)*P_vertex1_vertex2;

            double distance = (P_vertex1_CoG - projection).norm();

            double alpha = atan2(P_vertex1_CoG.y,P_vertex1_CoG.x);
            double beta = atan2(P_vertex1_vertex2.y,P_vertex1_vertex2.x);

            output = std::min(output, (WrapAngle(alpha - beta) >= 0 ? 1.0 : 0.0)*distance);
        }

        return output;

    }

    double LongitudinalStaticStabilityMargin(const Vertices& support_polygon) //this function currently assumes the CoG is at (0,0), and that we only care about the distance in the x axis
    {
        double output = std::numeric_limits<double>::max();

        for(int i = 0; i<support_polygon.vertices.size(); ++i)
        {
            Point P_vertex1_vertex2;
            Point Pt1;
            Point Pt2;
            bool calc = false;
            if (i<support_polygon.vertices.size()-1)
            {
                calc = support_polygon.vertices[i+1].y * support_polygon.vertices[i].y < 0.0;
                if (calc)
                {
                   P_vertex1_vertex2 = support_polygon.vertices[i+1] - support_polygon.vertices[i];
                   Pt1 = Point(support_polygon.vertices[i].x, support_polygon.vertices[i].y);
                   Pt2 = Point(support_polygon.vertices[i+1].x, support_polygon.vertices[i+1].y);
                }
            }
            else
            {
                calc = support_polygon.vertices[0].y * support_polygon.vertices[i].y < 0.0;
                if (calc)
                {
                   P_vertex1_vertex2 = support_polygon.vertices[0] - support_polygon.vertices[i];
                   Pt1 = Point(support_polygon.vertices[i].x, support_polygon.vertices[i].y);
                   Pt2 = Point(support_polygon.vertices[0].x, support_polygon.vertices[0].y);
                }
            }

            if (calc)
            {
                Point P_vertex1_CoG = -1.0*support_polygon.vertices[i];

                double distance = std::fabs(Pt1.x - (Pt2.x - Pt1.x) / (Pt2.y - Pt1.y) * Pt1.y);
                double alpha = atan2(P_vertex1_CoG.y,P_vertex1_CoG.x);
                double beta = atan2(P_vertex1_vertex2.y,P_vertex1_vertex2.x);

                output = std::min(output, (WrapAngle(alpha - beta) >= 0 ? 1.0 : 0.0)*distance);
            }

        }

        if (output == std::numeric_limits<double>::max())
        {
            output = 0.0;
        }

        return output;

        // Eigen::Matrix2f A;
        // A << 1, 2, -1, 2;
        // Eigen::Vector2f b;
        // b << 5, 6;

        // // Eigen::Vector2f x = A.colPivHouseHolderQr().solve(b);

        // Eigen::Vector2f x = A.inverse()*b;

        // // Eigen::ColPivHouseholderQR<Eigen::Matrix2f> decomp(A);

        // // Eigen::Vector2f x = decomp.solve(b);

        // std::cout << x << std::endl;

        // return 5.0;
        

    }

    double PointsToStaticStabilityMargin(std::vector<Point> points, const Point& CoG)
    {
        Vertices support_polygon = FindConvexHull(points);
        double output = StaticStabilityMargin(support_polygon, CoG);

        return output;
    }

    double PointsToLongitudinalStaticStabilityMargin(std::vector<Point> points)
    {
        Vertices support_polygon = FindConvexHull(points);
        double output = LongitudinalStaticStabilityMargin(support_polygon);

        return output;
    }


} // namespace stability_utils

#endif  // STABILITY_UTILS_H_
