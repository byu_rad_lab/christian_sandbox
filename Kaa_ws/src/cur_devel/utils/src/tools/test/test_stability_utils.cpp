#include "stability_utils/stability_utils.h"
#include <gtest/gtest.h>
// #include <boost/tuple/tuple.hpp>
// #include <boost/assign/list_of.hpp>
#include <boost/chrono.hpp>

using namespace stability_utils;

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

TEST(PointsOperators_Test, add_subtract)
{
    //define vertices
    Point vertex_1;
    Point vertex_2;

    vertex_1.x = 1.0;
    vertex_1.y = 2.0;
    vertex_2.x = -1.0;
    vertex_2.y = 3.0;

    Point expectedAddPoint;
    expectedAddPoint.x = 0.0;
    expectedAddPoint.y = 5.0;

    Point actAddPoint = vertex_1 + vertex_2;

    EXPECT_EQ(expectedAddPoint.x, actAddPoint.x);
    EXPECT_EQ(expectedAddPoint.y, actAddPoint.y);

    Point expectedSubtractPoint;
    expectedSubtractPoint.x = 2.0;
    expectedSubtractPoint.y = -1.0;

    Point actSubtractPoint = vertex_1 - vertex_2;

    EXPECT_EQ(expectedSubtractPoint.x, actSubtractPoint.x);
    EXPECT_EQ(expectedSubtractPoint.y, actSubtractPoint.y);
}

TEST(PointsOperators_Test, scalar_multiplication)
{
    //define vertices
    Point vertex_1;

    vertex_1.x = 1.0;
    vertex_1.y = 2.0;

    Point expectedMultiplication;
    expectedMultiplication.x = 5.0;
    expectedMultiplication.y = 10.0;

    Point actMultiplication = 5.0*vertex_1;

    EXPECT_EQ(expectedMultiplication.x, actMultiplication.x);
    EXPECT_EQ(expectedMultiplication.y, actMultiplication.y);

    actMultiplication = vertex_1*5.0;

    EXPECT_EQ(expectedMultiplication.x, actMultiplication.x);
    EXPECT_EQ(expectedMultiplication.y, actMultiplication.y);
}

TEST(PointsOperators_Test, dot_product)
{
    //define vertices
    Point vertex_1;
    Point vertex_2;

    vertex_1.x = 1.0;
    vertex_1.y = 2.0;
    vertex_2.x = -1.0;
    vertex_2.y = 3.0;

    double expectedOutput = 5.0;

    double actOutput = vertex_1 * vertex_2;

    EXPECT_EQ(expectedOutput, actOutput);
}

TEST(WrapAngle_Test, wrapangle)
{
    EXPECT_EQ(M_PI/2.0, WrapAngle(M_PI/2.0));
    EXPECT_EQ(0.0, WrapAngle(2.0*M_PI));
    EXPECT_EQ(M_PI/2.0, WrapAngle(-3.0*M_PI/2.0));
}

// class StaticStabilityMargin_Test : public testing::TestWithParam<boost::tuple<Point[], Point, double> >
// {
//     public:
//         virtual void SetUp(){}
//         virtual void TearDown(){}
// };
 
// INSTANTIATE_TEST_CASE_P(StaticStabilityMargin,
//                         StaticStabilityMargin_Test,
//                         ::testing::Values(
//                         boost::make_tuple({Point(1.0,0.0),Point(-1.0,0.0),Point(0.0,2.0)},Point(0.0,1.0), 1.0/sqrt(5.0))
//                             ));
 
// TEST_P(StaticStabilityMargin_Test, StaticStabilityMargin)
// {
//     EXPECT_EQ(1.0,1.0);
// }

TEST(StaticStabilityMargin_Test, hexagon)
{
    //define vertices
    Point vertex_1;
    Point vertex_2;
    Point vertex_3;
    Point vertex_4;
    Point vertex_5;
    Point vertex_6;

    vertex_1.x = 2.0;
    vertex_1.y = -1.0;
    vertex_2.x = 4.0;
    vertex_2.y = 2.0;
    vertex_3.x = 0.0;
    vertex_3.y = 5.0;
    vertex_4.x = -4.0;
    vertex_4.y = 5.0;
    vertex_5.x = -4.0;
    vertex_5.y = 3.0;
    vertex_6.x = -2.0;
    vertex_6.y = -1.0;

    Vertices support_polygon;

    support_polygon.vertices.push_back(vertex_1);
    support_polygon.vertices.push_back(vertex_2);
    support_polygon.vertices.push_back(vertex_3);
    support_polygon.vertices.push_back(vertex_4);
    support_polygon.vertices.push_back(vertex_5);
    support_polygon.vertices.push_back(vertex_6);

    //define center of gravity
    Point CoG;

    CoG.x = 0.0;
    CoG.y = 0.0;

    double expectedOutput = 1.0;


    boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();

    double actOutput = StaticStabilityMargin(support_polygon, CoG);

    boost::chrono::system_clock::time_point end = boost::chrono::system_clock::now();
    boost::chrono::duration<double> sec = boost::chrono::system_clock::now() - start;
    std::cout << "hexagon " << sec.count() << " seconds\n";
    

    EXPECT_EQ(expectedOutput, actOutput);
}

TEST(StaticStabilityMargin_Test, triangle)
{
    //define vertices
    Point vertex_1;
    Point vertex_2;
    Point vertex_3;

    vertex_1.x = -1.0;
    vertex_1.y = 0.0;
    vertex_2.x = 1.0;
    vertex_2.y = 0.0;
    vertex_3.x = 0.0;
    vertex_3.y = 2.0;

    Vertices support_polygon;

    support_polygon.vertices.push_back(vertex_1);
    support_polygon.vertices.push_back(vertex_2);
    support_polygon.vertices.push_back(vertex_3);

    //define center of gravity
    Point CoG;

    CoG.x = 0.0;
    CoG.y = 1.0;

    double expectedOutput = 1.0/sqrt(5);

    //boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();

    double actOutput = StaticStabilityMargin(support_polygon, CoG);

    //boost::chrono::system_clock::time_point end = boost::chrono::system_clock::now();
    //boost::chrono::duration<double> sec = boost::chrono::system_clock::now() - start;
    //std::cout << "triangle " << sec.count() << " seconds\n";
    

    EXPECT_EQ(expectedOutput, actOutput);
}

TEST(StaticStabilityMargin_Test, not_in_triangle)
{
    //define vertices
    Point vertex_1;
    Point vertex_2;
    Point vertex_3;

    vertex_1.x = -1.0;
    vertex_1.y = 0.0;
    vertex_2.x = 1.0;
    vertex_2.y = 0.0;
    vertex_3.x = 0.0;
    vertex_3.y = 2.0;

    Vertices support_polygon;

    support_polygon.vertices.push_back(vertex_1);
    support_polygon.vertices.push_back(vertex_2);
    support_polygon.vertices.push_back(vertex_3);

    //define center of gravity
    Point CoG;

    CoG.x = 0.0;
    CoG.y = -1.0;

    double expectedOutput = 0.0;

    //boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();

    double actOutput = StaticStabilityMargin(support_polygon, CoG);

    //boost::chrono::system_clock::time_point end = boost::chrono::system_clock::now();
    //boost::chrono::duration<double> sec = boost::chrono::system_clock::now() - start;
    //std::cout << "not in triangle " << sec.count() << " seconds\n";
    

    EXPECT_EQ(expectedOutput, actOutput);
}

TEST(FindConvexHull_Test, triangle)
{
    //define vertices
    Point vertex_1;
    Point vertex_2;
    Point vertex_3;

    vertex_1.x = 1.0;
    vertex_1.y = 0.0;
    vertex_2.x = -1.0;
    vertex_2.y = 0.0;
    vertex_3.x = 0.0;
    vertex_3.y = 2.0;

    std::vector <Point> point_vec;

    point_vec.push_back(vertex_1);
    point_vec.push_back(vertex_2);
    point_vec.push_back(vertex_3);

    Vertices expectedOutput;

    expectedOutput.vertices.push_back(vertex_2);
    expectedOutput.vertices.push_back(vertex_1);
    expectedOutput.vertices.push_back(vertex_3);

    //boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();

    Vertices actOutput = FindConvexHull(point_vec);

    //boost::chrono::system_clock::time_point end = boost::chrono::system_clock::now();
    //boost::chrono::duration<double> sec = boost::chrono::system_clock::now() - start;
    //std::cout << "Convex hull triangle " << sec.count() << " seconds\n";
    
    for (int i = 0; i<expectedOutput.vertices.size(); ++i)
    {
        EXPECT_EQ(expectedOutput.vertices[i].x, actOutput.vertices[i].x);
        EXPECT_EQ(expectedOutput.vertices[i].y, actOutput.vertices[i].y);
    }
}

TEST(FindConvexHull_Test, quadrilateral)
{
    //define vertices
    Point vertex_1;
    Point vertex_2;
    Point vertex_3;
    Point vertex_4;

    vertex_1.x = 2.0;
    vertex_1.y = 4.0;
    vertex_2.x = -3.0;
    vertex_2.y = -2.0;
    vertex_3.x = 2.0;
    vertex_3.y = -2.0;
    vertex_4.x = -3.0;
    vertex_4.y = 4.0;

    std::vector <Point> point_vec;

    point_vec.push_back(vertex_1);
    point_vec.push_back(vertex_2);
    point_vec.push_back(vertex_3);
    point_vec.push_back(vertex_4);

    Vertices expectedOutput;

    expectedOutput.vertices.push_back(vertex_2);
    expectedOutput.vertices.push_back(vertex_3);
    expectedOutput.vertices.push_back(vertex_1);
    expectedOutput.vertices.push_back(vertex_4);

    //boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();

    Vertices actOutput = FindConvexHull(point_vec);

    //boost::chrono::system_clock::time_point end = boost::chrono::system_clock::now();
    //boost::chrono::duration<double> sec = boost::chrono::system_clock::now() - start;
    //std::cout << "Convex hull quadrilateral " << sec.count() << " seconds\n";
    
    for (int i = 0; i<expectedOutput.vertices.size(); ++i)
    {
        EXPECT_EQ(expectedOutput.vertices[i].x, actOutput.vertices[i].x);
        EXPECT_EQ(expectedOutput.vertices[i].y, actOutput.vertices[i].y);
    }
}

TEST(PointsToStaticStabilityMargin_Test, triangle)
{
    //define vertices
    Point vertex_1;
    Point vertex_2;
    Point vertex_3;

    vertex_1.x = 1.0;
    vertex_1.y = 0.0;
    vertex_2.x = -1.0;
    vertex_2.y = 0.0;
    vertex_3.x = 0.0;
    vertex_3.y = 2.0;

    std::vector <Point> point_vec;

    point_vec.push_back(vertex_1);
    point_vec.push_back(vertex_2);
    point_vec.push_back(vertex_3);

    //define center of gravity
    Point CoG;

    CoG.x = 0.0;
    CoG.y = 1.0;

    double expectedOutput = 1.0/sqrt(5);

    //boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();

    double actOutput = PointsToStaticStabilityMargin(point_vec, CoG);

    //boost::chrono::system_clock::time_point end = boost::chrono::system_clock::now();
    //boost::chrono::duration<double> sec = boost::chrono::system_clock::now() - start;
    //std::cout << "Total triangle " << sec.count() << " seconds\n";
    
    EXPECT_EQ(expectedOutput, actOutput);
}

TEST(LongitudinalStaticStabilityMargin_Test, in_triangle)
{
    //define vertices
    Point vertex_1;
    Point vertex_2;
    Point vertex_3;

    vertex_1.x = -1.0;
    vertex_1.y = -1.0;
    vertex_2.x = 1.0;
    vertex_2.y = -1.0;
    vertex_3.x = 0.0;
    vertex_3.y = 2.0;

    Vertices support_polygon;

    support_polygon.vertices.push_back(vertex_1);
    support_polygon.vertices.push_back(vertex_2);
    support_polygon.vertices.push_back(vertex_3);

    double expectedOutput = 2.0/3.0;

    boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();

    double actOutput = LongitudinalStaticStabilityMargin(support_polygon);

    boost::chrono::system_clock::time_point end = boost::chrono::system_clock::now();
    boost::chrono::duration<double> sec = boost::chrono::system_clock::now() - start;
    std::cout << "not in triangle " << sec.count() << " seconds\n";
    

    EXPECT_DOUBLE_EQ(expectedOutput, actOutput);
}

TEST(LongitudinalStaticStabilityMargin_Test, on_border_triangle)
{
    //define vertices
    Point vertex_1;
    Point vertex_2;
    Point vertex_3;

    vertex_1.x = -1.0;
    vertex_1.y = 0.0;
    vertex_2.x = 1.0;
    vertex_2.y = 0.0;
    vertex_3.x = 0.0;
    vertex_3.y = 2.0;

    Vertices support_polygon;

    support_polygon.vertices.push_back(vertex_1);
    support_polygon.vertices.push_back(vertex_2);
    support_polygon.vertices.push_back(vertex_3);

    double expectedOutput = 0.0;

    boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();

    double actOutput = LongitudinalStaticStabilityMargin(support_polygon);

    boost::chrono::system_clock::time_point end = boost::chrono::system_clock::now();
    boost::chrono::duration<double> sec = boost::chrono::system_clock::now() - start;
    std::cout << "not in triangle " << sec.count() << " seconds\n";
    

    EXPECT_EQ(expectedOutput, actOutput);
}

TEST(LongitudinalStaticStabilityMargin_Test, outside_triangle_all_above_x_axis)
{
    //define vertices
    Point vertex_1;
    Point vertex_2;
    Point vertex_3;

    vertex_1.x = -1.0;
    vertex_1.y = 1.0;
    vertex_2.x = 1.0;
    vertex_2.y = 1.0;
    vertex_3.x = 0.0;
    vertex_3.y = 2.0;

    Vertices support_polygon;

    support_polygon.vertices.push_back(vertex_1);
    support_polygon.vertices.push_back(vertex_2);
    support_polygon.vertices.push_back(vertex_3);

    double expectedOutput = 0.0;

    boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();

    double actOutput = LongitudinalStaticStabilityMargin(support_polygon);

    boost::chrono::system_clock::time_point end = boost::chrono::system_clock::now();
    boost::chrono::duration<double> sec = boost::chrono::system_clock::now() - start;
    std::cout << "not in triangle " << sec.count() << " seconds\n";
    

    EXPECT_EQ(expectedOutput, actOutput);
}

TEST(LongitudinalStaticStabilityMargin_Test, outside_triangle_crossing_x_axis)
{
    //define vertices
    Point vertex_1;
    Point vertex_2;
    Point vertex_3;

    vertex_1.x = 1.0;
    vertex_1.y = -1.0;
    vertex_2.x = 3.0;
    vertex_2.y = -1.0;
    vertex_3.x = 0.0;
    vertex_3.y = 2.0;

    Vertices support_polygon;

    support_polygon.vertices.push_back(vertex_1);
    support_polygon.vertices.push_back(vertex_2);
    support_polygon.vertices.push_back(vertex_3);

    double expectedOutput = 0.0;

    boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();

    double actOutput = LongitudinalStaticStabilityMargin(support_polygon);

    boost::chrono::system_clock::time_point end = boost::chrono::system_clock::now();
    boost::chrono::duration<double> sec = boost::chrono::system_clock::now() - start;
    std::cout << "not in triangle " << sec.count() << " seconds\n";
    
    EXPECT_EQ(expectedOutput, actOutput);
}

TEST(LongitudinalStaticStabilityMargin_Test, hexagon)
{
    //define vertices
    Point vertex_1;
    Point vertex_2;
    Point vertex_3;
    Point vertex_4;
    Point vertex_5;
    Point vertex_6;

    vertex_1.x = 2.0;
    vertex_1.y = -1.0;
    vertex_2.x = 4.0;
    vertex_2.y = 2.0;
    vertex_3.x = 0.0;
    vertex_3.y = 5.0;
    vertex_4.x = -4.0;
    vertex_4.y = 5.0;
    vertex_5.x = -4.0;
    vertex_5.y = 3.0;
    vertex_6.x = -2.0;
    vertex_6.y = -1.0;

    Vertices support_polygon;

    support_polygon.vertices.push_back(vertex_1);
    support_polygon.vertices.push_back(vertex_2);
    support_polygon.vertices.push_back(vertex_3);
    support_polygon.vertices.push_back(vertex_4);
    support_polygon.vertices.push_back(vertex_5);
    support_polygon.vertices.push_back(vertex_6);

    double expectedOutput = 2.5;


    boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();

    double actOutput = LongitudinalStaticStabilityMargin(support_polygon);

    boost::chrono::system_clock::time_point end = boost::chrono::system_clock::now();
    boost::chrono::duration<double> sec = boost::chrono::system_clock::now() - start;
    std::cout << "hexagon " << sec.count() << " seconds\n";
    

    EXPECT_EQ(expectedOutput, actOutput);
}