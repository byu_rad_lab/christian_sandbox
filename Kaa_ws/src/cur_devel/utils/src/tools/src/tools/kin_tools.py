#!/usr/bin/env python
import numpy as np
import math
import scipy as sp
from pdb import set_trace as pause
# from scipy import linalg

__author__ = 'tomfallen'

"""
Kinematics tools.  Deal with homogeneous transforms,
twists, etc.  Very incomplete.
"""

def test():
    print(test)


def print_mat(mat):
    for row in mat:
        print("[", end=' ')
        for elem in row:
            print("%10.4f   " % elem, end=' ')
        print("]")


def hat3(a):
    a = a.flatten()
    return np.array([
        [0, -a[2], a[1]],
        [a[2], 0, -a[0]],
        [-a[1], a[0], 0]
        ])


def hat4(xi):
    xi = xi.flatten()
    return np.array([
        [0, -xi[5], xi[4], xi[0]],
        [xi[5], 0, -xi[3], xi[1]],
        [-xi[4], xi[3], 0, xi[2]],
        [0, 0, 0, 0]
        ])


# takes np array of shape (3,)
def vee3(xh):
    return np.array([-xh[1, 2], xh[0, 2], -xh[0, 1]])


def vee4(xh):
    return np.array([xh[0, 3], xh[1, 3], xh[2, 3],
                     -xh[1, 2], xh[0, 2], -xh[0, 1]])


def hom_ident():
    return np.eye(4)


def homog_inv(g):
    R = g[:3, :3]
    ginv = np.eye(4)
    ginv[:3, :3] = R.T
    ginv[:3, 3] = -np.dot(R.T, g[:3, 3])
    return ginv


def hom_interp(g, h):
    r, t = hom2ax_vec(g)
    return ax_vec2homog(r*h, t*h)

def hom_interp2(g1,g2,h):
    g12 = np.dot(homog_inv(g1),g2)
    return np.dot(g1,hom_interp(g12,h))

def hom_avg(h1, h2):
    r1, t1 = hom2ax_vec(h1)
    r2, t2 = hom2ax_vec(h2)
    r = (r1+r2)/2.0
    t = (t1+t2)/2.0
    return ax_vec2homog(r, t)


def homog2adj(g):
    ph = hat3(g[:3, 3])
    R = g[:3, :3]

    adj = np.zeros((6, 6))
    adj[:3, :3] = R
    adj[3:, 3:] = R
    adj[:3, 3:] = np.dot(ph, R)

    return adj


def adj_inv(A):
    Ainv = np.zeros((6, 6))
    R = A[:3, :3]
    phat = np.dot(A[:3, 3:], R.T)
    Ainv[:3, :3] = R.T
    Ainv[3:, 3:] = R.T
    Ainv[:3, 3:] = -np.dot(R.T, phat)
    return Ainv


def homog_translate(x, y, z):
    g = nprospy
    g[0, 3] = x
    g[1, 3] = y
    g[2, 3] = z
    return g


def ax2rot(w):
    th = np.linalg.norm(w)
    if th < 1e-9:
        return np.eye(3)
    else:
        w = w / th
        wh = hat3(w)
        return np.eye(3) + wh * np.sin(th) + np.dot(wh, wh) * (1 - np.cos(th))


# depricated
def ax_ang2rot(w):
    th = np.linalg.norm(w)
    if th < 1e-9:
        return np.eye(3)
    else:
        w = w / th
        wh = hat3(w)
        return np.eye(3) + wh * np.sin(th) + np.dot(wh, wh) * (1 - np.cos(th))


def rot_vec2homog(R, p):
    return np.vstack((
        np.hstack((R, p.reshape(3, 1))),
        np.array([0, 0, 0, 1])
        ))


# function v, w, th = 1.0, return xi
def ax_vec2twist(v, w, th=1.0):
    return th * np.hstack((v.reshape((1, 3)), w.reshape((1, 3))))
    # return th * np.hstack(( v.flatten(), w.flatten() ))


def twist2ax_vec(xi):
    v = xi.flatten()[:3]
    w = xi.flatten()[3:]
    return v, w


def homog2rot_vec(g):
    R = g[:3, :3]
    p = g[:3, 3]
    return R, p


def twist2homog(xi):
    # convert twist to homog
    v, w = twist2ax_vec(xi)

    mag = np.linalg.norm(w)
    if mag < 1e-9:
        R = np.eye(3)
        p = v
    else:
        # th = mag
        w1 = w / mag
        v1 = v / mag
        R = ax_ang2rot(w)
        wh = hat3(w1)
        # p = np.dot( np.dot((np.eye(3) - R), wh), v1 ) + np.dot(np.outer(w1, w1), v1)*mag
        # p = np.dot( np.dot((np.eye(3) - R), wh) + np.outer(w1, w1)*mag, v1)
        A = np.dot((np.eye(3) - R), wh) + np.outer(w1, w1)*mag
        p = np.dot(A, v1)
    return rot_vec2homog(R, p)


# use care in dealing with unit vs non-unit twists
def homog2twist(g):
    # convert homog to twist
    R, p = homog2rot_vec(g)

    cos_th = (R[0, 0] + R[1, 1] + R[2, 2] - 1.0) / 2.0
    if cos_th > 1.0:
        th = 0.0
    elif cos_th < -1.0:
        th = np.pi
    else:
        th = np.arccos(cos_th)

    if th < 1e-6:
        w = np.array([0, 0, 0])
        v = p
    else:
        w1 = np.array([
            R[2, 1] - R[1, 2],
            R[0, 2] - R[2, 0],
            R[1, 0] - R[0, 1]]) / (2.0*np.sin(th))
        wh = hat3(w1)
        A = np.dot((np.eye(3) - R), wh) + np.outer(w1, w1)*th
        v1 = np.linalg.solve(A, p)
        w = w1*th
        v = v1*th
    return ax_vec2twist(v, w)


def rot2ax(R):
    temp = (np.trace(R) - 1) / 2.0
    if temp > 1.0:
        w = np.array([0, 0, 0])
        return w

    th = np.arccos(temp)
    if th < 1e-6 or math.isnan(th) or np.sin(th) < 1e-6:
        w = np.array([0, 0, 0])
    else:
        w = np.array([
            R[2, 1] - R[1, 2],
            R[0, 2] - R[2, 0],
            R[1, 0] - R[0, 1]
            ]) / (2 * np.sin(th))
    # r = th*w
    return th*w


def hom2ax_vec(hom):
    R = hom[:3, :3]
    r = rot2ax(R)
    t = hom[:3, 3]
    return r, t

def hom2ax_vec_list(hom):
    r,t = hom2ax_vec(hom)
    return np.hstack([r, t]).tolist()

def hom2ax_vec_list_pr(hom):
    r,t = hom2ax_vec(hom)
    return np.hstack([t, r]).tolist()

def ax_vec2homog(w, p):
    R = ax_ang2rot(w)
    return rot_vec2homog(R, p)

def eul_trans2homog(rots, transl):
    # converts xyz euler angles and xyz translations into a homogeneous transformation matrix
    # rots is a np.array as follows: np.array([rotation about x, rotation about y, rotation about z])
    # transl is a np.array as follows: np.array([trans along x, trans along y, trans along z])
    R = eul2rot(rots)
    return rot_vec2homog(R,transl)

def eulxzx_trans2homog(rots, transl):
    # converts xzx euler angles and xyz translations into a homogeneous transformation matrix
    # rots is a np.array as follows: np.array([rotation about x, rotation about z, rotation about x])
    # transl is a np.array as follows: np.array([trans along x, trans along y, trans along z])
    R = eulxzx2rot(rots)
    return rot_vec2homog(R,transl)

def eul2rot(rots):
    # Converts xyz euler angles to a rotation matrix using current axis rotations
    rx = rots[0]
    ry = rots[1]
    rz = rots[2]
    R = np.array([[np.cos(ry)*np.cos(rz), -np.cos(ry)*np.sin(rz), np.sin(ry)],
                [np.cos(rx)*np.sin(rz) + np.cos(rz)*np.sin(rx)*np.sin(ry), np.cos(rx)*np.cos(rz) - np.sin(rx)*np.sin(ry)*np.sin(rz), -np.cos(ry)*np.sin(rx)],
                [np.sin(rx)*np.sin(rz) - np.cos(rx)*np.cos(rz)*np.sin(ry), np.cos(rz)*np.sin(rx) + np.cos(rx)*np.sin(ry)*np.sin(rz), np.cos(rx)*np.cos(ry)]])
    return R

def eulxzx2rot(rots):
    # Converts xzx euler angles to a rotation matrix using current axis rotations
    rx1 = rots[0]
    rz = rots[1]
    rx2 = rots[2]
    R = np.array([[np.cos(rz), -np.cos(rx2)*np.sin(rz), np.sin(rz)*np.sin(rx2)], 
        [np.cos(rx1)*np.sin(rz), np.cos(rx1)*np.cos(rz)*np.cos(rx2) - np.sin(rx1)*np.sin(rx2), -np.cos(rx2)*np.sin(rx1) - np.cos(rx1)*np.cos(rz)*np.sin(rx2)],
        [np.sin(rx1)*np.sin(rz), np.cos(rx1)*np.sin(rx2) + np.cos(rz)*np.cos(rx2)*np.sin(rx1), np.cos(rx1)*np.cos(rx2) - np.cos(rz)*np.sin(rx1)*np.sin(rx2)]])
    return R

def ax_veclist2homog(wp):
    w = np.array(wp[:3])
    p = np.array(wp[3:])
    R = ax_ang2rot(w)
    return rot_vec2homog(R, p)

#Order of some functions are backwards
def ax_veclist2homog_pr(pw):
    w = np.array(pw[3:])
    p = np.array(pw[:3])
    R = ax_ang2rot(w)
    return rot_vec2homog(R, p)

# converts homogeneous tranform from cv to gl coordinates
def homog_cv2gl(hom):
    w = np.array([np.pi, 0, 0])
    t = np.array([0, 0, 0])
    g_cv2gl = ax_vec2homog(w, t)
    return np.dot(g_cv2gl, hom)


def homog_error(g1, g2, error_ratio):
    Rd = g2[:3, :3]
    R = g1[:3, :3]
    dg = g1[:3, 3] - g2[:3, 3]
    error_rot = 0.5 * np.trace(np.eye(3) - np.dot(Rd.T, R))
    error_pos = np.linalg.norm(dg, 2)
    error_total = np.sqrt(error_pos ** 2 + (error_ratio * error_rot) ** 2)
    return error_total, error_pos, error_rot


# function takes two vectors returns rotation that maps v2 to v1
# v1 = R12 v2
# R = min_rot_btw_vecs(vec1, vec2)
# vec1 = R12 * vec2
def min_rot_btw_vecs(vec1, vec2):
    axis = np.cross(vec2, vec1)
    cos_th = np.dot(vec2, vec1)
    sin_th = np.linalg.norm(axis)

    # if not colinear
    if np.abs(sin_th) > 1e-6:
        print("non-colinear")
        axis = axis / sin_th
        axis_hat = hat3(axis)
        rot = np.eye(3) + axis_hat * sin_th + axis_hat.dot(axis_hat) * (1 - cos_th)

    # cos the ~= 1.0
    # vectors are parallel
    elif cos_th > 0.0:
        print("parallel")
        rot = np.eye(3)

    # vectors are anti-paprallel
    # need to rotate 180 degrees around something perpendicular to vec2ity
    else:
        print("vectors are anti-parallel")
        print("vec2 =", vec2)
        index_min = np.argmin(np.abs(vec2))
        vec_non_parallel = np.zeros(3)
        vec_non_parallel[index_min] = 1
        vec_perp = np.cross(vec_non_parallel, vec2)
        vec_perp = vec_perp / np.linalg.norm(vec_perp)
        axis_hat = hat3(vec_perp)
        rot = np.eye(3) + axis_hat * sin_th + axis_hat.dot(axis_hat) * (1 - cos_th)
    return rot


def rand_rot():
    mag = 2.0
    while mag > 1.0:
        w = np.random.uniform(-1.0, 1.0, 3)
        mag = np.linalg.norm(w, 2)
    w = w / mag

    th = np.random.uniform(0.0, 2.0 * np.pi, 1)

    wh = hat3(w)

    # TODO: this should be a function: Rodriguez formula
    R = np.eye(3) + wh * np.sin(th[0]) + wh ** 2 * (1 - np.cos(th[0]))

    return R


def sample_circle():
    vals = np.random.uniform(-1.0, 1.0, 2)
    while vals[0] ** 2 + vals[1] ** 2 > 1:
        vals = np.random.uniform(-1.0, 1.0, 2)
    return vals


def sample_params(sample_range, n_acts):
    n = np.zeros(n_acts * 2)
    for i in range(n_acts):
        n[i * 2:i * 2 + 2] = sample_range * sample_circle()
    return n

def axfromquat(q):
    w = q[3]
    vec = q[0:3]
    temp = 1-w**2

    if (temp<0):
        return np.zeros(3)
    elif (temp <1e-6):
        return 2*vec
    else:
        return 2*np.arccos(w)/np.sqrt(1-w**2)*vec





if __name__ == '__main__':

    v = np.array([2, 3, 4])
    w = np.array([.1, .4, .1])
    xi = ax_vec2twist(v, w, th=1.0)

    print("v = ", v)
    print("w = ", w)
    print("xi = ", xi)

    # convert twist to homog
    v, w = twist2ax_vec(xi)

    mag = np.linalg.norm(w)
    if mag < 1e-9:
        R = np.eye(3)
        p = v
    else:
        # th = mag
        w1 = w / mag
        v1 = v / mag
        R = ax_ang2rot(w)
        wh = hat3(w1)
        # p = np.dot( np.dot((np.eye(3) - R), wh), v1 ) + np.dot(np.outer(w1, w1), v1)*mag
        p = np.dot(np.dot((np.eye(3) - R), wh) + np.outer(w1, w1)*mag, v1)
        A = np.dot((np.eye(3) - R), wh) + np.outer(w1, w1)*mag
        p2 = np.dot(A, v1)
    g = rot_vec2homog(R, p)

    # convert homog to twist
    th = np.arccos((R[0, 0] + R[1, 1] + R[2, 2] - 1) / 2)
    if th < 1e-9:
        w = np.array([0, 0, 0])
        v = p
    else:
        w2 = np.array([
            R[2, 1] - R[1, 2],
            R[0, 2] - R[2, 0],
            R[1, 0] - R[0, 1]]) / (2.0*np.sin(th))
        A2 = np.dot((np.eye(3) - R), wh) + np.outer(w2, w2)*th
        v2 = np.linalg.solve(A2, p)
        w2 = w2*th
        v2 = v2*th

    xihat = hat4(xi)
    print("xi = ", xi)
    print("xihat = ")
    print(xihat)

    g2 = sp.linalg.expm(xihat)
    print("g = ")
    print_mat(g)
    print("g2 = ")
    print_mat(g2)
