import pickle


def load_pickle(filename):
    filename_pickle = filename + '.pckl'
    file_object = open(filename_pickle, 'r')
    data = pickle.load(file_object)
    file_object.close()
    return data


def save_pickle(filename, data):
    filename_pickle = filename + '.pckl'
    file_object = open(filename_pickle, 'wb')
    pickle.dump(data, file_object)
    file_object.close()
    return
