%script to plot the 3D radviz data that is produced by optimization_tools.py in this directory

close all
clear
%gens =  ["Gen_1",'Gen_10','Gen_20','Gen_30','Gen_40','Gen_50'];
figure(1)
% directory = '~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_1/2018-07-22_16:57:05.965077/Gens_'
% directory = '~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_1/2018-07-22_17:54:58.825882/Gens_'
% directory = '~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_1/2018-07-21_10:04:48.338876/Gens_'
directory = '~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_2/second_round/2018-08-06_23:32:09.878799/Gens_'

perfect_scores = [1 0 0;
                  0 1 0;
                  -1 0 0;
                  0 -1 0;]
figure(1)

robot_number = 0:1:49;

plot3(perfect_scores(:,1),perfect_scores(:,2),perfect_scores(:,3),'*')
legend_str{1} = 'Extreme Solutions';
hold on


for i=22:1:22
    load(strcat(directory, strcat(int2str(i),'.mat')))
    plot3(x,y,robot_number,'o')
    view(0, 90)
    xlabel('Desired Direction Velocity <-> Dexterity')
    ylabel('Static Stability <-> Payload')
    legend_str{end+1} = strcat('Gen ',int2str(i));
    legend(legend_str)
    axis equal
    
%     figure(2)
%     plot(x,y,'o')
%     yyaxis left
%     xxaxis bottom
%     xlabel('X')
%     ylabel('Y')
%     
%     xxaxis top
%     xlabel('Monkey')
%     yyaxis right
%     ylabel('Cow')
    i
    hold on
end


% legend('Gen_1','Gen_10','Gen_20','Gen_30','Gen_40','Gen_50')
