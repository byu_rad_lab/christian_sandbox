%matlab script to plot and analyze pareto fronts from the optimization results

close all
clear

%10.45258 orientation, 73 SO3
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_2/2018-08-03_09:03:48.429762/pareto_front_scores.mat')
% 
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_2/second_round/2018-08-06_23:32:09.878799/pareto_front_scores.mat')
% num_gens = 43;
% bubble_scale = 1000;

% %10.4258 orientation, 5 SO3
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_2/2018-08-03_12:49:11.156491/pareto_front_scores.mat')
% 
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_2/second_round/2018-08-06_06:51:45.776135/pareto_front_scores.mat')
% num_gens = 60;
% bubble_scale = 1000;

%45 orientation, 73 SO3
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_2/2018-08-03_10:59:31.012454/pareto_front_scores.mat')
% 
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_2/second_round/2018-08-06_20:55:17.380452/pareto_front_scores.mat')
% num_gens = 43;
% bubble_scale = 10000;

%45 orientation, 5 SO3
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_2/2018-08-03_15:30:53.253205/pareto_front_scores.mat')
% 
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_2/second_round/2018-08-06_17:47:07.933337/pareto_front_scores.mat')
% num_gens = 32;
% bubble_scale = 10000;

%45 orientation 73 SO3 before switch of orientation of joint for torque
%model
%26 XYZ
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_1/2018-07-22_16:57:05.965077/pareto_front_scores.mat')
% num_gens = 22;
%20 XYZ
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_1/2018-07-21_10:04:48.338876/pareto_front_scores.mat')
% num_gens = 120;
% bubble_scale = 10000;


%200 pop 45 orientation 73 SO3 20 XYZ before switch of orientation of joint for torque
%model
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_1/2018-07-20_08:38:57.557198/pareto_front_scores.mat')
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_1/2018-07-20_09:44:10.893837/pareto_front_scores.mat')
% num_gens = 60;
% bubble_scale = 10000;

%Testing on mine to see if keeps extremes
% load('~/radlab/data/evolutionary_optimization/quadruped_optimization/2018-08-07_23:01:00.482031/pareto_front_scores.mat')
% num_gens = 50;
% bubble_scale = 10000;

% 45 orientation 73 SO3 20 XYZ 50 Pop 
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_3/2018-08-09_03:16:34.503728/pareto_front_scores.mat')
% num_gens = 60;
% bubble_scale = 10000;

% 45 orientation 73 SO3 20 XYZ 100 Pop 
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_3/2018-08-09_08:39:13.965759/pareto_front_scores.mat')
% num_gens = 60;
% bubble_scale = 10000;

% 45 orientation 73 SO3 20 XYZ 200 Pop 
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_3/2018-08-10_01:50:15.367459/pareto_front_scores.mat')
% num_gens = 60;
% bubble_scale = 10000;

%% 
% 45 orientation 73 SO3 20 XYZ 200 Pop 
load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_4/2018-08-15_18:10:12.376401/pareto_front_scores.mat')
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_4/2018-08-16_00:20:04.119875/pareto_front_scores.mat')
num_gens = 60;
bubble_scale = 2500;

% 45 orientation 73 SO3 20 XYZ 100 Pop 
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_4/2018-08-15_08:44:52.943098/pareto_front_scores.mat')
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_4/2018-08-15_08:02:58.621665/pareto_front_scores.mat')
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_4/2018-08-15_10:09:16.167255/pareto_front_scores.mat')
% num_gens = 50;
% bubble_scale = 10000;

% 45 orientation 73 SO3 20 XYZ 50 Pop
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_4/2018-08-15_06:37:13.761717/pareto_front_scores.mat')
% num_gens = 60;
% bubble_scale = 10000;

% 45 orientation 73 SO3 20 XYZ 100 Pop 45 psi 
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_4/2018-08-15_03:39:54.698256/pareto_front_scores.mat')
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_4/2018-08-15_11:04:00.562706/pareto_front_scores.mat')
% num_gens = 60;
% bubble_scale = 50000;

% 45 orientation 73 SO3 20 XYZ 50 Pop 45 psi
% load('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_4/2018-08-15_09:17:26.437469/pareto_front_scores.mat')
% num_gens = 60;
% bubble_scale = 50000;



for i=1:num_gens
    all_data{i} = eval(sprintf('Gen_%i',i))
end

last_gen = all_data{length(all_data)};
dexterity_max_last = max(all_data{length(all_data)}(:,1));
payload_max_last = max(all_data{length(all_data)}(:,2));
stability_min_last = min(all_data{length(all_data)}(:,3));
stability_max_last = max(all_data{length(all_data)}(:,3));
dv_min_last = min(all_data{length(all_data)}(:,4));
dv_max_last = max(all_data{length(all_data)}(:,4));

%% Finding extremes and "most balanced" of last generation
%normalize scores of last generation
for i=1:length(last_gen(1,:))
    min_score = min(last_gen(:,i));
    max_score = max(last_gen(:,i));
    last_gen_normalized(:,i) = (last_gen(:,i) - min_score)./(max_score-min_score);
end

last_gen_inv_normalized = 1 - last_gen_normalized;

% 
% projection_vector = ones(length(Gen_1(1,:)),1);
% projection_unit_vector = projection_vector/norm(projection_vector)
% 
% for i=1:length(Gen_1(:,1))
%     vec = Gen_28_normalized(i,:);
% 
%     error(i) = sqrt(dot(vec,vec) - dot(vec,projection_unit_vector)^2);
% end
% 
% [min_error, idx] = min(error)

%% Plot Matrix Scatter Plot
% figure()
% subplot(4,4,1)
figure()
plot3(last_gen(:,1),last_gen(:,2),last_gen(:,3),'.')
grid on
hold on

fitness_dext_pay = maxmin2(last_gen_inv_normalized(:,1),last_gen_inv_normalized(:,2));
pareto_dex_pay = find(fitness_dext_pay > 1);

plot_data = [];
for i=1:length(pareto_dex_pay)
    plot_data = [plot_data; last_gen(pareto_dex_pay(i),:);];
end
cow = length(plot_data(:,1))

plot3(plot_data(:,1),plot_data(:,2),plot_data(:,3),'o')
xlabel('Dexterity')
ylabel('Payload')
zlabel('Stability')
hold on

fitness_dext_stab = maxmin2(last_gen_inv_normalized(:,1),last_gen_inv_normalized(:,3));
pareto_dex_stab = find(fitness_dext_stab > 1);

plot_data = [];
for i=1:length(pareto_dex_stab)
    plot_data = [plot_data; last_gen(pareto_dex_stab(i),:);];
end
plot3(plot_data(:,1),plot_data(:,2),plot_data(:,3),'*')
hold on
cow = length(plot_data(:,1))

fitness_pay_stab = maxmin2(last_gen_inv_normalized(:,2),last_gen_inv_normalized(:,3));
pareto_pay_stab = find(fitness_pay_stab > 1);

plot_data = [];
for i=1:length(pareto_pay_stab)
    plot_data = [plot_data; last_gen(pareto_pay_stab(i),:);];
end
plot3(plot_data(:,1),plot_data(:,2),plot_data(:,3),'x')
hold on
cow = length(plot_data(:,1))

fitness_dext_dv = maxmin2(last_gen_inv_normalized(:,1),last_gen_inv_normalized(:,4));
pareto_dex_dv = find(fitness_dext_dv > 1);

plot_data = [];
for i=1:length(pareto_dex_dv)
    plot_data = [plot_data; last_gen(pareto_dex_dv(i),:);];
end
plot3(plot_data(:,1),plot_data(:,2),plot_data(:,3),'+')
hold on
cow = length(plot_data(:,1))

fitness_pay_dv = maxmin2(last_gen_inv_normalized(:,2),last_gen_inv_normalized(:,4));
pareto_pay_dv = find(fitness_pay_dv > 1);

plot_data = [];
for i=1:length(pareto_pay_dv)
    plot_data = [plot_data; last_gen(pareto_pay_dv(i),:);];
end
plot3(plot_data(:,1),plot_data(:,2),plot_data(:,3),'k>')
hold on
cow = length(plot_data(:,1))

fitness_stab_dv = maxmin2(last_gen_inv_normalized(:,3),last_gen_inv_normalized(:,4));
pareto_stab_dv = find(fitness_stab_dv > 1);

plot_data = [];
for i=1:length(pareto_stab_dv)
    plot_data = [plot_data; last_gen(pareto_stab_dv(i),:);];
end
cow = length(plot_data(:,1))
plot3(plot_data(:,1),plot_data(:,2),plot_data(:,3),'<')

figure()
plot3(last_gen(:,1),last_gen(:,2),last_gen(:,4),'.')
xlabel('Dexterity')
ylabel('Payload')
zlabel('DV')
grid on
%% Real Matrix of Scatter Plots
figure()

% pareto_fronts = [pareto_dex_pay, pareto_pay_stab, pareto_stab_dv,  pareto_dex_stab, pareto_pay_dv, pareto_dex_dv];


metric_titles = ["Dexterity (# Poses)", "Av. Payload (N)", "Av. Stability Margin (m)", "Av. Desired Velocity"];

for i=1:4
    plot_number = i
    for j=i+1:4 %columns
        subplot(3,3,plot_number)
        plot(last_gen(:,i),last_gen(:,j),'.','MarkerFaceColor', 'b', 'MarkerEdgeColor','b')
        hold on
        xlabel(metric_titles(i))
        ylabel(metric_titles(j))
        plot_number = plot_number + 3;
        
        fitness_objs = maxmin2(last_gen_inv_normalized(:,i),last_gen_inv_normalized(:,j));
        pareto_objs = find(fitness_objs > 1);

        plot_data = [];
        for k=1:length(pareto_objs)
            plot_data = [plot_data; last_gen(pareto_objs(k),:);];
        end
        plot(plot_data(:,i),plot_data(:,j), '.', 'MarkerFaceColor', [51/255.0 102/255.0 0], 'MarkerEdgeColor',[51/255.0 102/255.0 0])
        %label robots
        text(last_gen(1,i),last_gen(1,j),'A', 'FontSize', 14);
        text(last_gen(2,i),last_gen(2,j),'B', 'FontSize', 14);
        text(last_gen(3,i),last_gen(3,j),'C', 'FontSize', 14);
        text(last_gen(4,i),last_gen(4,j),'D', 'FontSize', 14);
%         text(last_gen(5,i),last_gen(5,j),'E', 'FontSize', 14);
%         text(last_gen(67,i),last_gen(67,j),'F', 'FontSize', 14);
    end
end


%% Bubble Plot
figure()
plot_num = 1;
for i=10:10:length(all_data)
% figure()
subplot(3,2,plot_num)

if i==10
%bubble legend code
a = bubble_scale*all_data{length(all_data)}(:,4)+100;
bubsizes = [min(a) (min(a)+max(a))/2 max(a)];
legentry=cell(size(bubsizes));
hold on
for ind = 1:numel(bubsizes)
%    bubleg(ind) = scatter(nan,nan,bubsizes(ind),nan);
   bubleg(ind) = plot(nan,nan,'ko','markersize',sqrt(bubsizes(ind)));
%    set(bubleg(ind),'visible','on')
   legentry{ind} = num2str(round((bubsizes(ind)-100)/bubble_scale,3));
end

% h = scatter(all_data{length(all_data)}(:,1),all_data{length(all_data)}(:,2),a,'r','MarkerFaceColor','red')
[hleg1, hobj1] = legend(legentry)
title(hleg1,'Desired Velocity')
hleg1.Title.Visible = 'on';
hleg1.Title.NodeChildren.Position = [0.5 1.1 0];
cow_face = get(hleg1,'position');
cow_face(4) = 0.27;
set(hleg1,'position',cow_face)
end

plot_num = plot_num+1;
scatter(all_data{i}(:,1),all_data{i}(:,2),bubble_scale*all_data{i}(:,4)+100,all_data{i}(:,3),'filled')
title(strcat('Pareto Front Gen. ',int2str(i)))
xlabel('Dexterity (# Poses)')
ylabel('Average Payload (N)')
xlim([0 1.1*dexterity_max_last])
ylim([0 1.1*payload_max_last])
% colormap(jet)
hcb = colorbar;
title(hcb,'Stability (m)')
caxis([0.9*stability_min_last,1.1*stability_max_last])
end

figure()
% colormap(jet)
hcb = colorbar;
title(hcb,'Stability (m)')
caxis([0.9*stability_min_last,1.1*stability_max_last])

%bubble legend code
a = bubble_scale*all_data{length(all_data)}(:,4)+100;
bubsizes = [min(a) (min(a)+max(a))/2 max(a)];
legentry=cell(size(bubsizes));
hold on
for ind = 1:numel(bubsizes)
%    bubleg(ind) = scatter(nan,nan,bubsizes(ind),nan);
   bubleg(ind) = plot(nan,nan,'ko','markersize',sqrt(bubsizes(ind)));
%    set(bubleg(ind),'visible','on')
   legentry{ind} = num2str(round((bubsizes(ind)-100)/bubble_scale,3));
end
% h = scatter(all_data{length(all_data)}(:,1),all_data{length(all_data)}(:,2),a,'r','MarkerFaceColor','red')
[hleg1, hobj1] = legend(legentry)
title(hleg1,'Desired Velocity')
hleg1.Title.Visible = 'on';
hleg1.Title.NodeChildren.Position = [0.5 1.1 0];
cow_face = get(hleg1,'position');
cow_face(4) = 0.27;
set(hleg1,'position',cow_face)
% hold on;
% Lh(1) = scatter(0, 50, bubble_scale*min(all_data{length(all_data)}(:,4))+100, .3, 'filled'); 
% hold on;
% Lh(2) = scatter(1, 200, bubble_scale*max(all_data{length(all_data)}(:,4))+100, .3, 'filled');
% legend(Lh, {'min Desired Velocity', 'max Desired Velocity'} )


% subplot(2,3,plot_num+1)
scatter(all_data{length(all_data)}(:,1),all_data{length(all_data)}(:,2),bubble_scale*all_data{length(all_data)}(:,4)+100,all_data{length(all_data)}(:,3),'filled')
title(strcat('Pareto Front Generation ',int2str(length(all_data))))
xlabel('Dexterity (# Poses)')
ylabel('Average Payload (N)')
xlim([0 1.1*dexterity_max_last])
ylim([0 1.1*payload_max_last])




% figure()
% scatter(Gen_1(:,1),Gen_1(:,2),1000*Gen_1(:,4)+100,Gen_1(:,3),'filled')
% xlabel('Dexterity')
% ylabel('Payload')
% hcb = colorbar;
% title(hcb,'Stability')

%% Percentile testing

percentile = 0;
for i=1:4
    indices{i} = find(all_data{length(all_data)}(:,i) > prctile(all_data{length(all_data)}(:,i),percentile));
end

dext_stab = intersect(indices{1},indices{3})
pay_dv = intersect(indices{2},indices{4})

for i=1:length(dext_stab)
    prc_dext_stab(i,1) = comp_percentile(all_data{length(all_data)}(:,1), all_data{length(all_data)}(dext_stab(i),1));
    prc_dext_stab(i,2) = comp_percentile(all_data{length(all_data)}(:,3), all_data{length(all_data)}(dext_stab(i),3));
end

for i=1:length(pay_dv)
    prc_pay_dv(i,1) = comp_percentile(all_data{length(all_data)}(:,2), all_data{length(all_data)}(pay_dv(i),2));
    prc_pay_dv(i,2) = comp_percentile(all_data{length(all_data)}(:,4), all_data{length(all_data)}(pay_dv(i),4));
end

prc_dext_stab
prc_pay_dv

prc_dext_stab_closest = prc_dext_stab - [95 95];
prc_pay_dv_closest = prc_pay_dv - [95 95];

for i=1:length(prc_dext_stab)
    prc_dext_stab_norms(i) = norm(prc_dext_stab_closest(i,:));
end
prc_dext_stab_norms

for i=1:length(prc_pay_dv)
    prc_pay_dv_norms(i) = norm(prc_pay_dv_closest(i,:));
end
prc_pay_dv_norms
%% Functions

function x = comp_percentile(datas,value)
    perc = prctile(datas,1:100);
    [c index] = min(abs(perc'-value));
    x = index;
end

function fitness = maxmin2(obj1,obj2)

    for i=1:length(obj1)
        temp = [];
        for j=1:length(obj1)
            if j ~= i
                temp = [temp; min(obj1(i) - obj1(j), obj2(i) - obj2(j));];
            end
        end
        
        fitness(i) = 1-max(temp);
    end
    
end
                
