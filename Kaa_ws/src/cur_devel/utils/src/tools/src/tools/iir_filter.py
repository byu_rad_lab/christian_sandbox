#!/usr/bin/env python

import numpy as np
from collections import deque as deq
import scipy.io as sio
import matplotlib.pyplot as plt

class iir_filter():
	def __init__(self, a, b):
		#enter in the full a and b vectors as given by matlab with a(0) = 1.0
		self.n = len(a)
		self.y = deq([],maxlen=len(a)-1)
		self.x = deq([],maxlen=len(a))
		self.a = a #numpy array
		self.b = b #numpy array

	def get_filt_val(self, xk):
		n = self.n

		if len(self.y) == 0:
			#initialize filter value and measurement value as original measurement value
			for i in range(n):
				self.y.append(xk)
				self.x.append(xk)

			yk = xk	

		else:
			self.x.append(xk)
			# x = self.x
			# y = self.y
			# a = self.a
			# b = self.b

			# yk = self.b[0]*x[4] + b[1]*x[3] + b[2]*x[2] + b[3]*x[1] + b[4]*x[0] - a[1]*y[3] - a[2]*y[2] - a[3]*y[1] - a[4]*y[0]


			yk = 0
			for i in range(n):
				yk = yk + self.b[i]*self.x[n-i-1] 

			for i in range(n-1):
				yk = yk - self.a[i+1]*self.y[n-2-i]

			self.y.append(yk)

		return yk


if __name__ == '__main__':
	data = sio.loadmat('recorded_data_chamber_3.mat')
	p = data['p']
	t = data['t']
	t = t[0]
	p = p[0]
	a = np.array([1, -3.671729089161945, 5.06799838673419, -3.115966925201750, 0.719910327291871])
	b = np.array([1.32937288987529e-05,	5.31749155950116e-05,	7.97623733925174e-05,	5.31749155950116e-05,	1.32937288987529e-05])

	filter = iir_filter(a,b)
	y = deq([])

	for i in range(len(p)):
		fp = filter.get_filt_val(p[i])
		y.append(fp)

	plt.figure(1)
	plt.plot(t,p)
	plt.plot(t,y)
	plt.show()