#!/usr/bin/env python
import rospy
import numpy as np
from geometry_msgs.msg import PoseStamped
import tf.transformations as tft
from numpy import matrix as mat
from threading import RLock
import time
from copy import deepcopy
from pdb import set_trace as pause

class relative_vive():

    def __init__(self):
        self.lock = RLock()
        self.Ts = 0.1
        rospy.init_node('get_tracker_transforms', anonymous=True)
        self.rate = rospy.Rate(1.0/self.Ts)
        self.v1 = '/neo_tracker1'
        self.v2 = '/neo_tracker2'
        self.T_vw_1_ = mat([[1.0, 0.0, 0.0, 0.0],[0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 1.0]])
        self.T_2_vw_ = mat([[1.0, 0.0, 0.0, 0.0],[0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 1.0]])
        self.T_vw_1 = mat([[1.0, 0.0, 0.0, 0.0],[0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 1.0]])
        self.T_2_vw = mat([[1.0, 0.0, 0.0, 0.0],[0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 1.0]])
        rospy.Subscriber(self.v1, PoseStamped, self.Tracker_1_Callback)
        rospy.Subscriber(self.v2, PoseStamped, self.Tracker_2_Callback)

    def Tracker_1_Callback(self, msg):
        self.lock.acquire()
        quaternion = [msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w]
        Tr = mat(tft.quaternion_matrix(quaternion))
        Tr[0,3] = msg.pose.position.x
        Tr[1,3] = msg.pose.position.y
        Tr[2,3] = msg.pose.position.z
        self.T_vw_1_ = self.homog_trans_inv(Tr)
        self.lock.release()

    def Tracker_2_Callback(self, msg):
        self.lock.acquire()
        quaternion = [msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w]
        self.T_2_vw_ = mat(tft.quaternion_matrix(quaternion))
        self.T_2_vw_[0,3] = msg.pose.position.x
        self.T_2_vw_[1,3] = msg.pose.position.y
        self.T_2_vw_[2,3] = msg.pose.position.z
        self.lock.release()

    def lock_data(self):
        self.lock.acquire()
        try:
            self.T_vw_1 = deepcopy(self.T_vw_1_)
            self.T_2_vw = deepcopy(self.T_2_vw_)
        finally:
            self.lock.release()

    def get_transform_2_relative_1(self):
        self.lock_data()
        T_2_1 = np.matmul(self.T_vw_1, self.T_2_vw)
        print('x', T_2_1[1,3])
        print('y', T_2_1[0,3])
        print('z', -T_2_1[2,3])
        print('a', np.arccos(T_2_1[0,0])*180./np.pi, '\n')

    def homog_trans_inv(self,Tr):
        Tinv = mat(np.zeros((4,4)))
        Tinv[0:3,0:3] = Tr[0:3,0:3].T
        Tinv[0:3,3] = (-Tr[0:3,0:3].T)*(Tr[0:3,3])
        Tinv[3,3] = 1.0
        return Tinv

if __name__=='__main__':
    vive_stuff = relative_vive()
    time.sleep(1.)
    while not rospy.is_shutdown():
        vive_stuff.get_transform_2_relative_1()
        vive_stuff.rate.sleep()
