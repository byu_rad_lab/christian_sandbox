import numpy as np


def points_to_hyperplane_normal(points,c=1):
    k = c*np.ones((len(points),1))
    normal = np.matrix.dot(np.linalg.inv(points),k)

    return normal

def distance_from_hyperplane(point,normal,c=1):
    dist = (np.dot(point,normal)-c)/np.linalg.norm(normal)
    return dist[0]


if __name__ == '__main__':

    pts = np.array([[20.0, 0.0],[0.0,20.0]])
    # pts = np.array([[20.0, 0.0, 0.0],[10.0,15.0,0.0],[0.0,0.0,100.0]])
    normal = points_to_hyperplane_normal(pts)
    print("normal: ", normal)

    pt = np.array([5.0,5.0])
    print("pt: ", pt)
    dist = distance_from_hyperplane(pt,normal)
    print("dist: ", dist)
