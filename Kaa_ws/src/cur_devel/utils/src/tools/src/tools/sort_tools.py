from operator import itemgetter

def sort_by_idx(list,idx):
    sorted_list = sorted(list,key=itemgetter(idx))
    return sorted_list

if __name__ == '__main__':
    list = [[0.0,1.0],[1.0,0.0],[0.5,0.5]]
    print("list: ", list)
    for i in range(len(list[0])):
        print("idx: ", i)
        list_sorted = sort_by_idx(list,i)
        print("sorted list: ", list_sorted)
        print("biggest: ", list_sorted[-1])

