import copy
import geometry_tools as gt
import numpy as np
import os
import pdb
import scipy.io as sio
import sort_tools as st

def normalize_scores(f):
    num_des = len(f)
    num_obj = len(f[0])

    f_norm = copy.deepcopy(f)

    for i in range(num_obj):
        f_sorted = st.sort_by_idx(f,i)
        min_score = f_sorted[0][i]
        max_score = f_sorted[-1][i]
        for j in range(num_des):
            f_norm[j][i] = (f[j][i] - min_score)/(max_score - min_score)

    return f_norm

def radviz(f_norm):
    num_des = len(f_norm)
    num_obj = len(f_norm[0])

    ux = []
    uy = []

    for i in range(num_des):
        sum_ux = 0
        sum_uy = 0

        for j in range(num_obj):
            theta = j*2.0*np.pi/num_obj
            sum_ux += f_norm[i][j]*np.cos(theta)
            sum_uy += f_norm[i][j]*np.sin(theta)

        ux.append(sum_ux/sum(f_norm[i]))
        uy.append(sum_uy/sum(f_norm[i]))

    return ux, uy



def points_for_3D_radviz(f,normal):
    num_des = len(f)
    num_obj = len(f[0])

    Z = []
    for i in range(num_obj):
        f_sorted = st.sort_by_idx(f,i)
        Z.append(f_sorted[-1])

    if normal is None:
        normal = gt.points_to_hyperplane_normal(Z)

    d = []
    for i in range(num_des):
        d.append(gt.distance_from_hyperplane(f[i],normal))

    f_norm = normalize_scores(f)

    ux, uy = radviz(f_norm)

    return ux, uy, d, normal

def swap_columns(list, cl1, cl2):
    list_mat = np.matrix(list)
    list_new_mat = np.matrix(copy.deepcopy(list))
    list_new_mat[:,cl1] = list_mat[:,cl2]
    list_new_mat[:,cl2] = list_mat[:,cl1]

    return np.array(list_new_mat)


if __name__ == '__main__':

    # pts = [[10.0, 0.0],[0.0,10.0]]
    # pts = [[20.0, 0.0, 0.0],[10.0,15.0,0.0],[0.0,0.0,100.0]]
    # pts = [[20.0, 0.0],[0.0,20.0],[5.0,5.0],[10.0,10.0],[15.0,15.0],[10.0,15.0]]
    # pts = [[20.0, 0.0],[0.0,20.0],[5.0,12.5],[10.0,10.0],[15.0,7.5]]
    # theta = np.linspace(0,np.pi/2,100)
    # pts = []
    # for i in range(len(theta)):
    #     pts.append([np.cos(theta[i]),np.sin(theta[i])])

    # directory = os.path.expanduser('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_1/2018-07-21_10:04:48.338876/')
    # directory = os.path.expanduser('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_1/2018-07-22_17:54:58.825882/')
    directory = os.path.expanduser('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_1/2018-07-22_16:57:05.965077/') #45 degree threshold
    # directory = os.path.expanduser('~/radlab/data/evolutionary_optimization/super_computer/quadruped/experiments_1/2018-07-20_12:15:21.473000/') #45 degree threshold bigger res (20)

    mat = sio.loadmat(directory+'pareto_front_scores.mat')

    pts = mat['Gen_22']

    print("here be yer points! ", pts)
    print("here be yer switch points! ", swap_columns(pts,2,3))
    pts_swap = swap_columns(pts,2,3)

    x,y,z,normal = points_for_3D_radviz(pts_swap,None)

    # print "Normal: ", normal
    # print "pts: ", pts
    # print "z: ", z


    # gens = ['Gen_1','Gen_10','Gen_20','Gen_30','Gen_40','Gen_50']
    for i in range(len(mat)-3):
        # pts = mat[gens[i]]
        pts = mat['Gen_'+str(i+1)]
        pts_swap = swap_columns(pts,2,3)
        x,y,z,normal = points_for_3D_radviz(pts_swap,normal)
        sio.savemat(directory+'Gens_'+str(i)+'.mat', {'x':x, 'y':y, 'z':z})

    # sio.savemat('results.mat', {'x':x, 'y':y, 'z':z})
    # print "x: ", x
    # print "y: ", y
    # print "z: ", z
    # print normalize_scores(pts)










