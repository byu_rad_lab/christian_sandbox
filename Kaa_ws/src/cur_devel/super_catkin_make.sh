function clean_ws {
    rm -rf build/ devel/
}

function clean_build {
    clean_ws
    catkin_make
    catkin_make run_tests
}

function build_ws {
    cd ~/git/byu/cur_devel/$1
    catkin_make
    catkin_make run_tests
    source devel/setup.bash --extend
}

function clean_build_ws {
    cd ~/git/byu/cur_devel/$1
    clean_ws
    catkin_make
    catkin_make run_tests
    source devel/setup.bash --extend
}

function source_baxter {
    source ~/ros_ws/devel/setup.bash --extend
}

function build_baxter {
    if [ ! -d ~/ros_ws ]; then
        install_baxter_jade
    fi

    cd ~/ros_ws
    catkin_make
    source_baxter
}

function build_baxter_clean {
    if [ ! -d ~/ros_ws ]; then
        install_baxter_jade
    fi

    cd ~/ros_ws
    rm -rf build/ devel/
    catkin_make
    source_baxter
}

function install_baxter_kinetic {

    #install necessary dependencies
    sudo apt-get update
    sudo apt-get install -y git-core python-argparse python-wstool python-vcstools python-rosdep ros-kinetic-control-msgs ros-kinetic-joystick-drivers ros-kinetic-python-orocos-kdl  ros-kinetic-orocos-kinematics-dynamics ros-kinetic-kdl-conversions ros-kinetic-orocos-kdl

    #make workspace for baxter install
    mkdir -p ~/ros_ws/src

    source /opt/ros/kinetic/setup.bash

    cd ~/ros_ws
    catkin_make
    catkin_make install

    # download and install necessary packages
    cd ~/ros_ws/src
    wstool init .
    wstool merge https://raw.githubusercontent.com/RethinkRobotics/baxter/master/baxter_sdk.rosinstall
    wstool update

    git clone https://github.com/RethinkRobotics/baxter_pykdl.git

    cd ~/ros_ws

    catkin_make
    catkin_make install

    source_baxter
}

function install_baxter_jade {

    #install necessary dependencies
    sudo apt-get update
    sudo apt-get install -y git-core python-argparse python-wstool python-vcstools python-rosdep ros-jade-control-msgs ros-jade-joystick-drivers ros-jade-python-orocos-kdl  ros-jade-orocos-kinematics-dynamics ros-jade-kdl-conversions ros-jade-orocos-kdl

    #make workspace for baxter install
    mkdir -p ~/ros_ws/src

    source /opt/ros/jade/setup.bash

    cd ~/ros_ws
    catkin_make
    catkin_make install

    # download and install necessary packages
    cd ~/ros_ws/src
    wstool init .
    wstool merge https://raw.githubusercontent.com/RethinkRobotics/baxter/master/baxter_sdk.rosinstall
    wstool update

    git clone https://github.com/RethinkRobotics/baxter_pykdl.git

    cd ~/ros_ws

    catkin_make
    catkin_make install

    source_baxter
}

function super_catkin_make {
    build_baxter
    cd ~/git/byu/cur_devel
    build_ws third_party
    build_ws utils
    build_ws sensors
    build_ws robots    
    build_ws models
    build_ws design
    build_ws estimation
    build_ws control
    build_ws experiments
    cd ~/git/byu/cur_devel
}

function super_catkin_make_clean {
    build_baxter_clean
    cd ~/git/byu/cur_devel
    clean_build_ws third_party
    clean_build_ws utils
    read -p "Press [Enter] key to build sensors..."
    clean_build_ws sensors
    read -p "Press [Enter] key to build robots..."
    clean_build_ws robots
    read -p "Press [Enter] key to build models..."
    clean_build_ws models
    read -p "Press [Enter] key to build design..."
    clean_build_ws design
    read -p "Press [Enter] key to build estimation..."
    clean_build_ws estimation
    read -p "Press [Enter] key to build control..."
    clean_build_ws control
    read -p "Press [Enter] key to build experiments..."
    clean_build_ws experiments
    cd ~/git/byu/cur_devel
}

function mega_source {
    # cd ~/git/byu/cur_devel
    source ~/git/byu/cur_devel/third_party/devel/setup.bash --extend
    source ~/git/byu/cur_devel/utils/devel/setup.bash --extend
    source ~/git/byu/cur_devel/sensors/devel/setup.bash --extend
    source ~/git/byu/cur_devel/robots/devel/setup.bash --extend
    source ~/git/byu/cur_devel/models/devel/setup.bash --extend
    source ~/git/byu/cur_devel/design/devel/setup.bash --extend
    source ~/git/byu/cur_devel/estimation/devel/setup.bash --extend
    source ~/git/byu/cur_devel/control/devel/setup.bash --extend
    source ~/git/byu/cur_devel/experiments/devel/setup.bash --extend
} 

function catkin_make_optimization {
    cd ~/git/byu/cur_devel
    build_ws utils
    build_ws models
    build_ws design
}


function catkin_make_optimization_clean {
    cd ~/git/byu/cur_devel
    clean_build_ws utils
    read -p "Press [Enter] key to build models..."
    clean_build_ws models
    read -p "Press [Enter] key to build design..."
    clean_build_ws design
}

