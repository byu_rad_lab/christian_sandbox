function clean_ws {
    rm -rf build/ devel/
}

function clean_build {
    clean_ws
    catkin_make
    catkin_make run_tests
}

function build_ws {
    cd ~/christian_sandbox/Kaa_ws/src/cur_devel/$1
    catkin_make
    catkin_make run_tests
    source devel/setup.bash --extend
}

function clean_build_ws {
    cd ~/christian_sandbox/Kaa_ws/src/cur_devel/$1
    clean_ws
    catkin_make
    catkin_make run_tests
    source devel/setup.bash --extend
}











function super_catkin_make {
    cd ~/christian_sandbox/Kaa_ws/src/cur_devel
    build_ws third_party
    build_ws utils
    build_ws sensors
    build_ws robots    
    build_ws models
    build_ws design
    build_ws estimation
    build_ws control
    cd ~/christian_sandbox/Kaa_ws/src/cur_devel
}

function super_catkin_make_clean {
    cd ~/christian_sandbox/Kaa_ws/src/cur_devel
    clean_build_ws third_party
    clean_build_ws utils
    read -p "Press [Enter] key to build sensors..."
    clean_build_ws sensors
    read -p "Press [Enter] key to build robots..."
    clean_build_ws robots
    read -p "Press [Enter] key to build models..."
    clean_build_ws models
    read -p "Press [Enter] key to build design..."
    clean_build_ws design
    read -p "Press [Enter] key to build estimation..."
    clean_build_ws estimation
    read -p "Press [Enter] key to build control..."
    clean_build_ws control
    cd ~/christian_sandbox/Kaa_ws/src/cur_devel
}

function mega_source {
    # cd ~/christian_sandbox/Kaa_ws/src/cur_devel
    source ~/christian_sandbox/Kaa_ws/src/cur_devel/third_party/devel/setup.bash --extend
    source ~/christian_sandbox/Kaa_ws/src/cur_devel/utils/devel/setup.bash --extend
    source ~/christian_sandbox/Kaa_ws/src/cur_devel/sensors/devel/setup.bash --extend
    source ~/christian_sandbox/Kaa_ws/src/cur_devel/robots/devel/setup.bash --extend
    source ~/christian_sandbox/Kaa_ws/src/cur_devel/models/devel/setup.bash --extend
    source ~/christian_sandbox/Kaa_ws/src/cur_devel/design/devel/setup.bash --extend
    source ~/christian_sandbox/Kaa_ws/src/cur_devel/estimation/devel/setup.bash --extend
    source ~/christian_sandbox/Kaa_ws/src/cur_devel/control/devel/setup.bash --extend
} 

function catkin_make_optimization {
    cd ~/christian_sandbox/Kaa_ws/src/cur_devel
    build_ws utils
    build_ws models
    build_ws design
}


function catkin_make_optimization_clean {
    cd ~/christian_sandbox/Kaa_ws/src/cur_devel
    clean_build_ws utils
    read -p "Press [Enter] key to build models..."
    clean_build_ws models
    read -p "Press [Enter] key to build design..."
    clean_build_ws design
}

