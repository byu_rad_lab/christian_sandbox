from numpy import matrix as mat
import numpy as np
import random as rand
from scipy.optimize import minimize
from math import sin, cos, radians, sqrt

def calc_R(x):
	#calculates a rotation matrix parameterized by rotations about x, y, and z
	phi = x[0]; #rotation about x
	theta = x[1]; #rotation about y
	psi = x[2]; #rotation about z

	#calculate current R
	Rx = mat([[1.0, 0.0, 0.0],[0.0, cos(radians(phi)), -sin(radians(phi))],[0.0, sin(radians(phi)), cos(radians(phi))]])
	Ry = mat([[cos(radians(theta)), 0.0, sin(radians(theta))],[0.0, 1.0, 0.0],[-sin(radians(theta)), 0.0, cos(radians(theta))]])
	Rz = mat([[cos(radians(psi)), -sin(radians(psi)), 0.0],[sin(radians(psi)), cos(radians(psi)), 0.0],[0.0, 0.0, 1.0]])
	R = Rx*Ry*Rz
	return R

def obj_fun_rot(x,Rai_aimin1, Rbi_bimin1):
	#calculates error in rotation based on current guess for rotations about x, y, and z
	#inputs are x: guess of rotations about x, y, and z in degrees,
	#			Rai_aimin1, and Rbi_bimin1: delta rotation matrices (types are lists of numpy matrices)

	n = len(Rai_aimin1)
	R = calc_R(x)
	error = 0 #initialize error
	for i in range(n):
		error_matrix = Rai_aimin1[i] - R*Rbi_bimin1[i]*R.T
		for j in range(3):
			for k in range(3):
				error = error + (error_matrix[j,k])**2
	return error

def obj_fun_rot_world(x,Rb_a, Ra_C1, Rb_C2):
	#used to find RC2_C1 (same process as obj_fun_rot)
	RC2_C1 = calc_R(x)
	n = len(Ra_C1)
	error = 0
	for i in range(n):
		error_matrix = RC2_C1 - Ra_C1[i]*Rb_a*Rb_C2[i].T
		for j in range(3):
			for k in range(3):
				error = error + (error_matrix[j,k])**2
	return error

def obj_fun_trans(x,Tai_aimin1, Tbi_bimin1, R):
	#calculates error in homogeneous transformation given correct R (numpy matrix), delta transformations (in lists of numpy matrices), and current guess x
	n = len(Tai_aimin1)
	dx = x[0] #translation in x
	dy = x[1] #translation in y
	dz = x[2] #translation in z

	#put together current T and calculate T inverse
	T = mat(np.zeros((4,4)))
	Tinv = mat(np.zeros((4,4)))
	T[0:3,0:3] = R
	T[0,3] = dx
	T[1,3] = dy
	T[2,3] = dz
	T[3,3] = 1.0

	Tinv[0:3,0:3] = R.T
	Tinv[0:3,3] = (-R.T)*(T[0:3,3])
	Tinv[3,3] = 1.0

	#calculate error
	error = 0
	for i in range(n):
		error_matrix = Tai_aimin1[i] - T*Tbi_bimin1[i]*Tinv
		for j in range(3):
			error = error + (error_matrix[j,3])**2
	return error

def obj_fun_trans_world(x, Tb_a, RC2_C1, Ta_C1, Tb_C2):
	#used to find TC2_C1 (same process as obj_fun_trans)
	n = len(Ta_C1)
	dx = x[0] #translation in x
	dy = x[1] #translation in y
	dz = x[2] #translation in z

	#put together current TC2_C1
	TC2_C1 = mat(np.zeros((4,4)))
	TC2_C1[0:3,0:3] = RC2_C1
	TC2_C1[0,3] = dx
	TC2_C1[1,3] = dy
	TC2_C1[2,3] = dz
	TC2_C1[3,3] = 1.0

	#calculate error
	error = 0
	for i in range(n):
		TC2_bi = mat(np.zeros((4,4)))
		TC2_bi[0:3,0:3] = Tb_C2[i][0:3,0:3].T
		TC2_bi[0:3,3] = (-Tb_C2[i][0:3,0:3].T)*(Tb_C2[i][0:3,3])
		TC2_bi[3,3] = 1.0
		error_matrix = TC2_C1 - Ta_C1[i]*Tb_a*TC2_bi
		for j in range(3):
			error = error + (error_matrix[j,3])**2

	return error

def calc_trans(type_opt, T):
	'''CALC_TRANS: Relates two coordinate frames where coordinate
	frame a and b are rigidly connected
	Detailed explanation: Two unrelated measurement systems each measure 3D
	orientation/position relative to their own frames (C1 and C2). Measurement
	system 1 returns the homogeneous transformation matrix of frame a
	relative to C1, and measurement system 2 returns the homogeneous
	transformation matrix of frame b relative to C2. For this function,
	coordinate frames a and b must be rigidly attached. In order to relate
	the two coordinate frames, follow this procedure:
		1.  Make sure frames a and b are rigidly attached.
		2.  Collect a data point (homogeneous trans.) from each system at
		    the same time (or at least while the object they are attached
		    to is in the same orientation and postion). These are denoted by Ta0_C1
		    (transform. from frame a to the measurement coordinate frame C1
		    at timestep 0 s.t. a point expressed in frame a premultiplied
		    by Ta0_C1 would now be expressed in frame C1 as follows:
		    p_C1 = Ta0_C1*p_a0) and Tb0_C2 respectively.
		3.  Rotate and translate the object they are rigidly attached to,
		    and repeat step 2 until you have as many data points for each system as you desire (more points is generally better).
		4.  Pass these data points into this function as a list of lists as follows T = [[Ta0_C1, Ta1_C1, ....],[Tb0_C2, Tb1_C2, ...]]
		5.  This function returns the homogeneous transformation matrix T which relates the two
		    measurement systems as follows: Ta1_a0 = T*Tb1_b0*T' where T is = Tb_a

		    ***NOTE: TC1_C2 = Tbi_C2*Ta_b*TC1_ai

	INPUTS:	type_opt - string either 'R' or 'T' based on if you want rotation matrices or homogeneus transformation matrices back
			Ta0_C1 etc. - numpy matrix (4x4 homogeneous transformation matrix)
			T - list of numpy matrices i.e. T = [[Ta0_C1, Ta1_C1, Ta2_C1, Ta3_C1,...], [Tb0_C2, Tb1_C2, Tb2_C2, Tb3_C2, ....]]
			***NOTE: You if you are only doing rotation set type_opt = 'R' and you can pass in 3x3 rotation matrices rather than homogeneous transformation matrices,
	OUTPUT: out = 	[Tb_a, TC2_C1] if type_opt = 'T' or out = [Rb_a, RC2_C1] if type_opt = 'R'
	 		Tb_a exists s.t. p_a = Tb_a*p_b (4x4 transformation matrix) and for TC2_C1: TC2_C1 = Tai_C1*Tb_a*TC2_bi
	 		Rb_a exists s.t. p_a = Rb_a*p_b (3x3 rotation matrix) and for RC2_C1: RC2_C1 = Rai_C1*Rb_a*RC2_bi
			(type_opt = 'R' if you want the rotation matrix or type_opt = 'T' for the transformation matrix)'''

	n = len(T[0]) #number of data points for each measurement system (they must be equal)
	Ta_C1 = T[0] #list of tranformations from a to C1 at time step 1, 2, 3, ..., n
	Tb_C2 = T[1] #list of tranformations from b to C2 at time step 1, 2, 3, ..., n

	if type_opt == 'R' and Ta_C1[0].shape[0] == 3 and Tb_C2[0].shape[0] == 3: #if the user only cares about rotation and supplied rotation matrices
		Ra_C1 = Ta_C1
		Rb_C2 = Tb_C2

	elif type_opt == 'T':
		#Split up data for rotation
		Ra_C1 = []
		Rb_C2 = []
		for i in range(n):
			Ra_C1.append(Ta_C1[i][0:3,0:3])
			Rb_C2.append(Tb_C2[i][0:3,0:3])

		#Find delta transformations
		Tai_aimin1 = [] ##list of delta transformations like Ta1_a0
		Tbi_bimin1 = [] #list of delta transformations like Tb1_b0
		for i in range(n-1):
			TC1_ai = mat(np.zeros((4,4))) #used to find Tai_aimin1
			TC1_ai[0:3,0:3] = Ra_C1[i].T
			TC1_ai[0:3,3] = (-Ra_C1[i].T)*(Ta_C1[i][0:3,3])
			TC1_ai[3,3] = 1.0
			Tai_aimin1.append(TC1_ai*Ta_C1[i+1])

			TC2_bi = mat(np.zeros((4,4))) #used to find Tbi_bimin1
			TC2_bi[0:3,0:3] = Rb_C2[i].T
			TC2_bi[0:3,3] = (-Rb_C2[i].T)*(Tb_C2[i][0:3,3])
			TC2_bi[3,3] = 1.0
			Tbi_bimin1.append(TC2_bi*Tb_C2[i+1])

	#Find delta rotations
	Rai_aimin1 = []
	Rbi_bimin1 = []
	for i in range(n-1):
		Rai_aimin1.append(Ra_C1[i].T*Ra_C1[i+1])
		Rbi_bimin1.append(Rb_C2[i].T*Rb_C2[i+1])

	#Optimization to find R checking many (100) starting points to avoid local minima
	#generate random starting points (rotations about x, y, and z in degrees)
	x0 = [rand.uniform(-90.0,90.0), rand.uniform(-90.0,90.0), rand.uniform(-90.0,90.0)]
	rot = minimize(obj_fun_rot, x0, args = (Rai_aimin1, Rbi_bimin1), method ='BFGS')
	num_optimize = 15 #number of times to run each optimization checking for local minimum
	for i in range(num_optimize):
		x0 = [rand.uniform(-90.0,90.0), rand.uniform(-90.0,90.0), rand.uniform(-90.0,90.0)]
		roti = minimize(obj_fun_rot, x0, args = (Rai_aimin1, Rbi_bimin1), method ='BFGS')
		if roti.fun < rot.fun: #if current error is less than previous error
			rot = roti
		if type_opt == 'T':
			if i == int(np.around(num_optimize*0.1,0)):
				print('10% done with optimization 1 of 4')
			elif i == int(np.around(num_optimize*0.2,0)):
				print('20% done with optimization 1 of 4')
			elif i == int(np.around(num_optimize*0.3,0)):
				print('30% done with optimization 1 of 4')
			elif i == int(np.around(num_optimize*0.4,0)):
				print('40% done with optimization 1 of 4')
			elif i == int(np.around(num_optimize*0.5,0)):
				print('50% done with optimization 1 of 4')
			elif i == int(np.around(num_optimize*0.6,0)):
				print('60% done with optimization 1 of 4')
			elif i == int(np.around(num_optimize*0.7,0)):
				print('70% done with optimization 1 of 4')
			elif i == int(np.around(num_optimize*0.8,0)):
				print('80% done with optimization 1 of 4')
			elif i == int(np.around(num_optimize*0.9,0)):
				print('90% done with optimization 1 of 4')

		elif type_opt == 'R':
			if i == int(np.around(num_optimize*0.1,0)):
				print('10% done with optimization 1 of 2')
			elif i == int(np.around(num_optimize*0.2,0)):
				print('20% done with optimization 1 of 2')
			elif i == int(np.around(num_optimize*0.3,0)):
				print('30% done with optimization 1 of 2')
			elif i == int(np.around(num_optimize*0.4,0)):
				print('40% done with optimization 1 of 2')
			elif i == int(np.around(num_optimize*0.5,0)):
				print('50% done with optimization 1 of 2')
			elif i == int(np.around(num_optimize*0.6,0)):
				print('60% done with optimization 1 of 2')
			elif i == int(np.around(num_optimize*0.7,0)):
				print('70% done with optimization 1 of 2')
			elif i == int(np.around(num_optimize*0.8,0)):
				print('80% done with optimization 1 of 2')
			elif i == int(np.around(num_optimize*0.9,0)):
				print('90% done with optimization 1 of 2')
	if type_opt == 'R':
		print('100% done with optimization 1 of 4')
	elif type_opt == 'T':
		print('100% done with optimization 1 of 2')
	Rb_a = calc_R(rot.x)

	#now find RC2_C1
	x0 = [rand.uniform(-90.0,90.0), rand.uniform(-90.0,90.0), rand.uniform(-90.0,90.0)]
	rotworld = minimize(obj_fun_rot_world, x0, args = (Rb_a, Ra_C1, Rb_C2), method ='BFGS')
	for i in range(num_optimize):
		x0 = [rand.uniform(-90.0,90.0), rand.uniform(-90.0,90.0), rand.uniform(-90.0,90.0)]
		rotworldi = minimize(obj_fun_rot_world, x0, args = (Rb_a, Ra_C1, Rb_C2), method ='BFGS')
		if rotworldi.fun < rotworld.fun: #if current error is less than previous error
			rotworld = rotworldi
		if type_opt == 'T':
			if i == int(np.around(num_optimize*0.1,0)):
				print('10% done with optimization 2 of 4')
			elif i == int(np.around(num_optimize*0.2,0)):
				print('20% done with optimization 2 of 4')
			elif i == int(np.around(num_optimize*0.3,0)):
				print('30% done with optimization 2 of 4')
			elif i == int(np.around(num_optimize*0.4,0)):
				print('40% done with optimization 2 of 4')
			elif i == int(np.around(num_optimize*0.5,0)):
				print('50% done with optimization 2 of 4')
			elif i == int(np.around(num_optimize*0.6,0)):
				print('60% done with optimization 2 of 4')
			elif i == int(np.around(num_optimize*0.7,0)):
				print('70% done with optimization 2 of 4')
			elif i == int(np.around(num_optimize*0.8,0)):
				print('80% done with optimization 2 of 4')
			elif i == int(np.around(num_optimize*0.9,0)):
				print('90% done with optimization 2 of 4')

		elif type_opt == 'R':
			if i == int(np.around(num_optimize*0.1,0)):
				print('10% done with optimization 2 of 2')
			elif i == int(np.around(num_optimize*0.2,0)):
				print('20% done with optimization 2 of 2')
			elif i == int(np.around(num_optimize*0.3,0)):
				print('30% done with optimization 2 of 2')
			elif i == int(np.around(num_optimize*0.4,0)):
				print('40% done with optimization 2 of 2')
			elif i == int(np.around(num_optimize*0.5,0)):
				print('50% done with optimization 2 of 2')
			elif i == int(np.around(num_optimize*0.6,0)):
				print('60% done with optimization 2 of 2')
			elif i == int(np.around(num_optimize*0.7,0)):
				print('70% done with optimization 2 of 2')
			elif i == int(np.around(num_optimize*0.8,0)):
				print('80% done with optimization 2 of 2')
			elif i == int(np.around(num_optimize*0.9,0)):
				print('90% done with optimization 2 of 2')
	if type_opt == 'R':
		print('100% done with optimization 2 of 4')
	elif type_opt == 'T':
		print('100% done with optimization 2 of 2')

	RC2_C1 = calc_R(rotworld.x)

	if type_opt == 'R':
		out = [Rb_a, RC2_C1]
		return out

	elif type_opt == 'T':
		x0 = [rand.uniform(-100.0,100.0), rand.uniform(-100.0,100.0), rand.uniform(-100.0,100.0)]
		trans = minimize(obj_fun_trans, x0, args = (Tai_aimin1, Tbi_bimin1, Rb_a), method = 'BFGS')
		for i in range(num_optimize):
			x0 = [rand.uniform(-100.0,100.0), rand.uniform(-100.0,100.0), rand.uniform(-100.0,100.0)]
			transi = minimize(obj_fun_trans, x0, args = (Tai_aimin1, Tbi_bimin1, Rb_a), method = 'BFGS')
			if transi.fun < trans.fun: #if the current error is less than previous error, update the answer
				trans = transi
			if i == int(np.around(num_optimize*0.1,0)):
				print('10% done with optimization 3 of 4')
			elif i == int(np.around(num_optimize*0.2,0)):
				print('20% done with optimization 3 of 4')
			elif i == int(np.around(num_optimize*0.3,0)):
				print('30% done with optimization 3 of 4')
			elif i == int(np.around(num_optimize*0.4,0)):
				print('40% done with optimization 3 of 4')
			elif i == int(np.around(num_optimize*0.5,0)):
				print('50% done with optimization 3 of 4')
			elif i == int(np.around(num_optimize*0.6,0)):
				print('60% done with optimization 3 of 4')
			elif i == int(np.around(num_optimize*0.7,0)):
				print('70% done with optimization 3 of 4')
			elif i == int(np.around(num_optimize*0.8,0)):
				print('80% done with optimization 3 of 4')
			elif i == int(np.around(num_optimize*0.9,0)):
				print('90% done with optimization 3 of 4')
		print('100% done with optimization 3 of 4')
		Tb_a = mat(np.zeros((4,4)))
		Tb_a[0:3, 0:3] = Rb_a
		Tb_a[0,3] = trans.x[0]
		Tb_a[1,3] = trans.x[1]
		Tb_a[2,3] = trans.x[2]
		Tb_a[3,3] = 1.0

		#now find TC2_C1
		x0 = [rand.uniform(-100.0,100.0), rand.uniform(-100.0,100.0), rand.uniform(-100.0,100.0)]
		transworld = minimize(obj_fun_trans_world, x0, args = (Tb_a, RC2_C1, Ta_C1, Tb_C2), method = 'BFGS')
		for i in range(num_optimize):
			x0 = [rand.uniform(-100.0,100.0), rand.uniform(-100.0,100.0), rand.uniform(-100.0,100.0)]
			transworldi = minimize(obj_fun_trans_world, x0, args = (Tb_a, RC2_C1, Ta_C1, Tb_C2), method = 'BFGS')
			if transworldi.fun < transworld.fun: #if the current error is less than previous error, update the answer
				transworld = transworldi
			if i == int(np.around(num_optimize*0.1,0)):
				print('10% done with optimization 4 of 4')
			elif i == int(np.around(num_optimize*0.2,0)):
				print('20% done with optimization 4 of 4')
			elif i == int(np.around(num_optimize*0.3,0)):
				print('30% done with optimization 4 of 4')
			elif i == int(np.around(num_optimize*0.4,0)):
				print('40% done with optimization 4 of 4')
			elif i == int(np.around(num_optimize*0.5,0)):
				print('50% done with optimization 4 of 4')
			elif i == int(np.around(num_optimize*0.6,0)):
				print('60% done with optimization 4 of 4')
			elif i == int(np.around(num_optimize*0.7,0)):
				print('70% done with optimization 4 of 4')
			elif i == int(np.around(num_optimize*0.8,0)):
				print('80% done with optimization 4 of 4')
			elif i == int(np.around(num_optimize*0.9,0)):
				print('90% done with optimization 4 of 4')
		print('100% done with optimization 4 of 4')
		TC2_C1 = mat(np.zeros((4,4)))
		TC2_C1[0:3,0:3] = RC2_C1
		TC2_C1[0,3] = transworld.x[0]
		TC2_C1[1,3] = transworld.x[1]
		TC2_C1[2,3] = transworld.x[2]
		TC2_C1[3,3] = 1.0
		out = [Tb_a, TC2_C1]
		return out

if __name__ == '__main__':
	#test optimization with example
	Tc2_d2_true = mat([[0.0, 0.0, 1.0, 1.0],[1.0, 0.0, 0.0, 2.0],[0.0, 1.0, 0.0, 3.0],[0.0, 0.0, 0.0, 1.0]])
	TCX_L1_true = mat([[1.0, 0.0, 0.0, 5.0],[0.0, 0.0, 1.0, 1.0],[0.0, -1.0, 0.0, -8.0],[0.0, 0.0, 0.0, 1.0]])
	Tc20_L1 = mat([[0.0, 0.0, 1.0, 6.0], [0.0, 1.0, 0.0, 1.0], [-1.0, 0.0, 0.0, 2.0], [0.0, 0.0, 0.0, 1.0]])
	Tc21_L1 = mat([[0.0,-1.0, 0.0, 3.0], [0.0, 0.0, 1.0, 0.2], [-1.0, 0.0, 0.0, 7.0], [0.0, 0.0, 0.0, 1.0]])
	Tc22_L1 = mat([[0.0, -1.0, 0.0, 5.0],[1.0, 0.0, 0.0, 1.1],[0.0, 0.0, 1.0, -6.0],[0.0, 0.0, 0.0, 1.0]])
	Tc23_L1 = mat([[0.0, -sqrt(2.0)/2.0, sqrt(2.0)/2.0, -0.2 ],[1.0, 0.0, 0.0, -0.1],[0.0, sqrt(2.0)/2.0, sqrt(2.0)/2.0, -2.0],[0.0, 0.0, 0.0, 1.0]])
	Td20_CX = mat([[1.0, 0.0, 0.0, 0.0],[0.0, 1.0, 0.0, -12.0],[0.0, 0.0, 1.0, -3.0],[0.0, 0.0, 0.0, 1.0]])
	Td21_CX = mat([[0.0, 0.0, -1.0, 1.0],[0.0, 1.0, 0.0, -17.0],[1.0, 0.0, 0.0, -1.8],[0.0, 0.0, 0.0, 1.0]])
	Td22_CX = mat([[0.0, 0.0, -1.0, 3.0],[-1.0, 0.0, 0.0, -1.0],[0.0, 1.0, 0.0, -1.9],[0.0, 0.0, 0.0, 1.0]])
	Td23_CX = np.linalg.inv(Tc2_d2_true*np.linalg.inv(Tc23_L1)*TCX_L1_true)

	T = [[Td20_CX, Td21_CX, Td22_CX, Td23_CX], [Tc20_L1, Tc21_L1, Tc22_L1, Tc23_L1]]
	type_opt = 'T'
	[Tc2_d2, TL1_CX] = calc_trans(type_opt, T)

	print('TRUE Tc2_d2: ', '\n', Tc2_d2_true)
	print('CALCULATED Tc2_d2: ', '\n', np.around(Tc2_d2,3))

	print('TRUE TCX_L1: ', '\n', np.linalg.inv(TCX_L1_true))
	print('CALCULATED TCX_L1: ', '\n', np.around(TL1_CX,3))
