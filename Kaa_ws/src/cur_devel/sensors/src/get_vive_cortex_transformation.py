#!/usr/bin/env python
import roslib
import rospy
import tf.transformations as tft
import numpy as np
from numpy import matrix as mat
from numpy import around
from tf2_msgs.msg import TFMessage
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
import scipy.io as sio
import transformation_optimization as tropt
import time
import datetime
from collections import deque
import os

class vive_cortex_transform():
    def __init__(self):            
        #initialize variables I'm using
        self.T_c1_wv = [] #stores transform data for controller 1 to world vive for optimization
        self.T_c2_wv = [] #stores transform data for controller 2 to world vive for optimization
        self.T_KLb_CX = [] #stores transform data from tracker dots at KL base to cortex for optimization (rigidly attached to vive controller 1)
        self.T_KLe_CX = [] #stores transform data from tracker dots at KL end effector to cortex for optimization (rigidly attached to vive controller 2)
        self.TrVc1 = mat(np.zeros((4,4))) #to hold the current transformation for the vive controller 1 to world vive
        self.TrVc2 = mat(np.zeros((4,4))) #to hold the current transformation for the vive controller 2    to world vive    
        self.TrCXb = mat(np.zeros((4,4))) #to hold the current transformation for KL base to cortex world
        self.Tr_KLb_bms = mat([[-1.0, 0.0, 0.0, 0.0],[0.0, 0.0, 1.0, 0.0],[0.0, 1.0, 0.0, 0.25],[0.0, 0.0, 0.0, 1.0]]) #transform from KL base DH frame at KL hip to cortex base marker set
        self.TrCXe = mat(np.zeros((4,4))) #to hold the current transformation for KL end effector to cortex world
        self.answer = [] #variable to store user answers to raw_input()
        self.num_points = [] #variable to store how many points to average at each location
        self.num_locations = [] #variable to store how many locations to collect transformations for for the optimization
        self.answer = input('Do you already have the transformation between your frames? (y/n):  ')

        #variables to be written as data
        self.time_arr = deque([])
        self.time0 = None
        self.time = 0    
    
        self.T_KLe_KLb_vive_x = deque([])
        self.T_KLe_KLb_vive_y = deque([])
        self.T_KLe_KLb_vive_z = deque([])
        self.T_KLe_KLb_vive_rx = deque([])
        self.T_KLe_KLb_vive_ry = deque([])
        self.T_KLe_KLb_vive_rz = deque([])
        self.T_KLe_KLb_CX_x = deque([])
        self.T_KLe_KLb_CX_y = deque([])
        self.T_KLe_KLb_CX_z = deque([])
        self.T_KLe_KLb_CX_rx = deque([])
        self.T_KLe_KLb_CX_ry = deque([])
        self.T_KLe_KLb_CX_rz = deque([])

        self.T_KLe_CX_x = deque([])
        self.T_KLe_CX_y = deque([])
        self.T_KLe_CX_z = deque([])
        self.T_KLe_CX_rx = deque([])
        self.T_KLe_CX_ry = deque([])
        self.T_KLe_CX_rz = deque([])
        self.T_KLe_CX_vive_x = deque([])
        self.T_KLe_CX_vive_y = deque([])
        self.T_KLe_CX_vive_z = deque([])
        self.T_KLe_CX_vive_rx = deque([])
        self.T_KLe_CX_vive_ry = deque([])
        self.T_KLe_CX_vive_rz = deque([])

        self.T_KLb_CX_x = deque([])
        self.T_KLb_CX_y = deque([])
        self.T_KLb_CX_z = deque([])
        self.T_KLb_CX_rx = deque([])
        self.T_KLb_CX_ry = deque([])
        self.T_KLb_CX_rz = deque([])
        self.T_KLb_CX_vive_x = deque([])
        self.T_KLb_CX_vive_y = deque([])
        self.T_KLb_CX_vive_z = deque([])
        self.T_KLb_CX_vive_rx = deque([])
        self.T_KLb_CX_vive_ry = deque([])
        self.T_KLb_CX_vive_rz = deque([])

        #Variables I find from optimization
        self.T_KLb_c1 = mat(np.zeros((4,4))) #transformation from the tracker dots at KL base to vive controller 1 (again, they are rigidly connected)
        self.T_c1_KLb = mat(np.zeros((4,4)))
        self.T_KLe_c2 = mat(np.zeros((4,4))) #transformation from the tracker dots at KL end effector to vive controller 2 (again, they are rigidly connected)
        self.T_wv_CX = mat(np.zeros((4,4))) #transformation from the vive world to cortex
        self.T_wv_CX_b = mat(np.zeros((4,4)))

        #Variables to help visualize error
        self.T_KLb_CX_fromvive = mat(np.zeros((4,4))) #the transformation from the King Louie Base tracker dots to cortex, using only data from the vive
        self.T_KLe_CX_fromvive = mat(np.zeros((4,4))) #the transformation from the King Louie end effector tracker dots to cortex, using only data from the vive
        self.T_KLe_KLb_fromvive = mat(np.zeros((4,4))) #the transformation from the King Louie end effector tracker dots to the King Louie base tracker dots using only data from the vive
        self.T_KLe_KLb = mat(np.zeros((4,4))) #the transformation from the King Louie end effector tracker dots to the King Louie base tracker dots using cortex
        # Set up subscribers: In this case, I'm subscribing to cortex and the vive
        rospy.init_node('vive_cortex_data', anonymous=True)
        rospy.Subscriber("tf", TFMessage, self.callbackVive) #vive subscriber
        rospy.Subscriber("KLworld/KLworld", PoseStamped, self.callbackCXworld) #cortex subscriber for King Louie base frame
        rospy.Subscriber("KLend/KLend", PoseStamped, self.callbackCXend) #cortex subscriber for King Louie end effeector        
    
    def callbackVive(self, msg): #callback function for the vive
        #vive has multiple transformations, so make sure to only pick off the ones I need
        transform_from = msg.transforms[0].child_frame_id #where I am transforming from (in this case controller 1 or controller 2)
        if msg.transforms[0].header.frame_id == "world_vive": #where I am transforming into
            if transform_from == "neo_tracker3": #change this based on what controllers I'm using but this is for the base
                transform = msg.transforms[0].transform #consists of translation and a quaternion
                quaternion = [transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w]
                self.TrVc1 = mat(tft.quaternion_matrix(quaternion)) #controller1 in world_vive frame
                self.TrVc1[0,3] = transform.translation.x
                self.TrVc1[1,3] = transform.translation.y
                self.TrVc1[2,3] = transform.translation.z
                self.T_KLb_CX_fromvive = self.T_wv_CX*self.TrVc1*self.T_KLb_c1
            if transform_from == "neo_tracker4": #change this based on what controllers I'm using, but this is for the end effector
                transform = msg.transforms[0].transform #consists of translation and a quaternion
                quaternion = [transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w]
                self.TrVc2 = mat(tft.quaternion_matrix(quaternion)) #controller2 in world_vive frame
                self.TrVc2[0,3] = transform.translation.x
                self.TrVc2[1,3] = transform.translation.y
                self.TrVc2[2,3] = transform.translation.z
                self.T_KLe_CX_fromvive = self.T_wv_CX*self.TrVc2*self.T_KLe_c2

    def callbackCXworld(self, msg): #callback function for KL base to Cortex world frame
        transform = msg.pose #consists of translation and a quaternion
        quaternion = [transform.orientation.x, transform.orientation.y, transform.orientation.z, transform.orientation.w]
        T_bms_CX = mat(tft.quaternion_matrix(quaternion))
        T_bms_CX[0,3] = transform.position.x
        T_bms_CX[1,3] = transform.position.y
        T_bms_CX[2,3] = transform.position.z
        self.TrCXb = T_bms_CX*self.Tr_KLb_bms        
        
    def callbackCXend(self, msg): #callback function for KL base to Cortex world frame
        transform = msg.pose #consists of translation and a quaternion
        quaternion = [transform.orientation.x, transform.orientation.y, transform.orientation.z, transform.orientation.w]
        translationx = transform.position.x
        translationy = transform.position.y
        translationz = transform.position.z
        self.TrCXe = mat(tft.quaternion_matrix(quaternion)) #KL base frame in cortex world frame
        self.TrCXe[0,3] = translationx
        self.TrCXe[1,3] = translationy
        self.TrCXe[2,3] = translationz
                
    def store_matrices1(self): #function to store n matrices for the optimization for the end effector transformations
        if self.answer == 'n':
            self.num_locations = input('How many locations do you want to collect for the optimization? ')
            self.num_points = input('How many data points do you want to average at each location? ')
            datetime_object = datetime.datetime #create a datetime object
            a = datetime_object.today() #get current year, month, day, hour, minute, second
            #create file name for data to be saved
            file_name = str(a.year) + '_' + str(a.month) + '_' + str(a.day) + '_' + str(a.hour) + '_' + str(a.minute) + '_' + str(a.second) + '_' + 'end_effector_trans_data.mat'
            ans = input('Move arm to steady position/orientation 0 and press enter or press "d" then enter when done: ') #must be at a steady position in case of lag between vive and cortex transformation updates: I found something that could help with this called message_filters::sync_policies::ApprocimateTime (filters messages and syncronizes based on msg time stamps)
            i = 0
            while i < int(self.num_locations):
            #while not ans == 'd':
                T_c2_wv_euler_toavg = [] #holds n data points at each position to average for controller 2 transformation to world vive
                T_KLe_CX_euler_toavg = [] #holds n data points at each position to average for KL base frame to cortex
                T_c2_wv_trans_toavg = [] #holds n data points at each position to average for controller 2 transformation to world vive
                T_KLe_CX_trans_toavg = [] #holds n data points at each position to average for KL base frame to cortex
                r = rospy.Rate(40) #I get data from the vive controller 1 at about 40 hz, so I want to make sure my loop doesn't run faster than this
                for j in range(int(self.num_points)): #collect num_points points at each position

                    T_c2_wv = self.TrVc2
                    T_c2_wv_euler = tft.euler_from_matrix(T_c2_wv,'rxyz') #get euler angles from matrix in a xyz sequence about rotated (not fixed) frames
                    T_c2_wv_trans = T_c2_wv[0:3,3] #get translation from matrix

                    T_KLe_CX = self.TrCXe
                    T_KLe_CX_euler = tft.euler_from_matrix(T_KLe_CX,'rxyz') #get euler angles from matrix in a xyz sequence about rotated (not fixed) frames
                    T_KLe_CX_trans = T_KLe_CX[0:3,3] #get translation from matrix

                    #append euler angles to average
                    T_c2_wv_euler_toavg.append(T_c2_wv_euler)
                    T_KLe_CX_euler_toavg.append(T_KLe_CX_euler)
                    #append translations to average
                    T_c2_wv_trans_toavg.append(T_c2_wv_trans)
                    T_KLe_CX_trans_toavg.append(T_KLe_CX_trans)

                    r.sleep() # keeps the loop running at 40 hz

                T_c2_wv_euler_avg = np.mean(T_c2_wv_euler_toavg,0) #average of euler angles at position i
                T_c2_wv_trans_avg = np.mean(T_c2_wv_trans_toavg,0) #average of translations at position i
                T_c2_wv_avg = mat(tft.euler_matrix(T_c2_wv_euler_avg[0],T_c2_wv_euler_avg[1],T_c2_wv_euler_avg[2],'rxyz'))
                T_c2_wv_avg[0,3] = T_c2_wv_trans_avg[0]
                T_c2_wv_avg[1,3] = T_c2_wv_trans_avg[1]
                T_c2_wv_avg[2,3] = T_c2_wv_trans_avg[2]
                print('T_c2_wv', '\n', T_c2_wv_avg)

                T_KLe_CX_euler_avg = np.mean(T_KLe_CX_euler_toavg,0) #average of euler angles at position i
                T_KLe_CX_trans_avg = np.mean(T_KLe_CX_trans_toavg,0) #average of translations at position i
                T_KLe_CX_avg = mat(tft.euler_matrix(T_KLe_CX_euler_avg[0],T_KLe_CX_euler_avg[1],T_KLe_CX_euler_avg[2],'rxyz'))
                T_KLe_CX_avg[0,3] = T_KLe_CX_trans_avg[0]
                T_KLe_CX_avg[1,3] = T_KLe_CX_trans_avg[1]
                T_KLe_CX_avg[2,3] = T_KLe_CX_trans_avg[2]
                print('T_KLe_CX', '\n', T_KLe_CX_avg)

                #append transformation matrix at point i
                self.T_c2_wv.append(T_c2_wv_avg)
                self.T_KLe_CX.append(T_KLe_CX_avg)    

                i = i + 1
                if i < int(self.num_locations):
                    print('You have 5 seconds to move to position %d and hold still: ' % i)
                    for j in range(5):
                        print(5 - j)
                        time.sleep(1)

            #write lists to a file (to analyze later if I need to)
            sio.savemat(file_name, {'T_c2_wv': self.T_c2_wv, 'T_KLe_CX': self.T_KLe_CX})
                #ans = raw_input('Move arm to steady position/orientation %d and press enter or press "d" then enter when done: ' % i) #must be at a steady position in case of lag between vive and cortex transformation updates: I found something that could help with this called message_filters::sync_policies::ApprocimateTime (filters messages and syncronizes based on msg time stamps)

    def store_matrices2(self):    #function to store n matrices for the optimization for the base frame transformations            
        if self.answer == 'n':
            ans = input('Move base to steady position/orientation 0 and press enter or press "d" then enter when done: ') #must be at a steady position in case of lag between vive and cortex transformation updates: I found something that could help with this called message_filters::sync_policies::ApprocimateTime (filters messages and syncronizes based on msg time stamps)
            i = 0
            datetime_object = datetime.datetime #create a datetime object
            a = datetime_object.today() #get current year, month, day, hour, minute, second
            #create file name for data to be saved
            file_name = str(a.year) + '_' + str(a.month) + '_' + str(a.day) + '_' + str(a.hour) + '_' + str(a.minute) + '_' + str(a.second) + '_' + 'base_trans_data.mat'

            while i < int(self.num_locations):
            #while not ans == 'd':
                T_c1_wv_euler_toavg = [] #holds n data points at each position to average for controller 1 transformation to world vive
                T_KLb_CX_euler_toavg = [] #holds n data points at each position to average for KL base frame to cortex
                T_c1_wv_trans_toavg = [] #holds n data points at each position to average for controller 1 transformation to world vive
                T_KLb_CX_trans_toavg = [] #holds n data points at each position to average for KL base frame to cortex
                r = rospy.Rate(40) #I get data from the vive controller 1 at about 40 hz, so I want to make sure my loop doesn't run faster than this
                for j in range(int(self.num_points)): #collect num_points points at each position
                    T_c1_wv = self.TrVc1
                    T_c1_wv_euler = tft.euler_from_matrix(T_c1_wv,'rxyz') #get euler angles from matrix in a xyz sequence about rotated (not fixed) frames
                    T_c1_wv_trans = T_c1_wv[0:3,3] #get translation from matrix                    

                    T_KLb_CX = self.TrCXb
                    T_KLb_CX_euler = tft.euler_from_matrix(T_KLb_CX,'rxyz') #get euler angles from matrix in a xyz sequence about rotated (not fixed) frames
                    T_KLb_CX_trans = T_KLb_CX[0:3,3] #get translation from matrix

                    #append euler angles to average
                    T_c1_wv_euler_toavg.append(T_c1_wv_euler)
                    T_KLb_CX_euler_toavg.append(T_KLb_CX_euler)
                    #append translations to average
                    T_c1_wv_trans_toavg.append(T_c1_wv_trans)
                    T_KLb_CX_trans_toavg.append(T_KLb_CX_trans)

                    r.sleep() # keeps the loop running at 40 hz
                T_c1_wv_euler_avg = np.mean(T_c1_wv_euler_toavg,0) #average of data points at position i
                T_c1_wv_trans_avg = np.mean(T_c1_wv_trans_toavg,0) #average of translations at position i
                T_c1_wv_avg = mat(tft.euler_matrix(T_c1_wv_euler_avg[0],T_c1_wv_euler_avg[1],T_c1_wv_euler_avg[2],'rxyz'))
                T_c1_wv_avg[0,3] = T_c1_wv_trans_avg[0]
                T_c1_wv_avg[1,3] = T_c1_wv_trans_avg[1]
                T_c1_wv_avg[2,3] = T_c1_wv_trans_avg[2]                
                print('T_c1_wv', '\n', T_c1_wv_avg)

                T_KLb_CX_euler_avg = np.mean(T_KLb_CX_euler_toavg,0) #average of euler angles at position i
                T_KLb_CX_trans_avg = np.mean(T_KLb_CX_trans_toavg,0) #average of translations at position i
                T_KLb_CX_avg = mat(tft.euler_matrix(T_KLb_CX_euler_avg[0],T_KLb_CX_euler_avg[1],T_KLb_CX_euler_avg[2],'rxyz'))
                T_KLb_CX_avg[0,3] = T_KLb_CX_trans_avg[0]
                T_KLb_CX_avg[1,3] = T_KLb_CX_trans_avg[1]
                T_KLb_CX_avg[2,3] = T_KLb_CX_trans_avg[2]
                print('T_KLb_CX', '\n', T_KLb_CX_avg)

                self.T_c1_wv.append(T_c1_wv_avg)
                self.T_KLb_CX.append(T_KLb_CX_avg)

                i = i + 1
                if i < int(self.num_locations):
                    print('You have 5 seconds to move to position %d and hold still: ' % i)
                    for j in range(5):
                        print(5 - j)
                        time.sleep(1)

            #write data points to a file (to analyze later if I need to)
            sio.savemat(file_name, {'T_c1_wv': self.T_c1_wv, 'T_KLb_CX': self.T_KLb_CX})

                #ans = raw_input('Move base to steady position/orientation %d and press enter or press "d" then enter when done: ' % i) #must be at a steady position in case of lag between vive and cortex transformation updates: I found something that could help with this called message_filters::sync_policies::ApprocimateTime (filters messages and syncronizes based on msg time stamps)

    def load_data(self):
        data_path_end = input('Enter path to data for end effector: ')
        data_path_base = input('Enter path to data for base: ')
        
        data_to_optimize_end = sio.loadmat(data_path_end)
        size_end = len(data_to_optimize_end['T_c2_wv'])
        data_to_optimize_base = sio.loadmat(data_path_base)
        size_base = len(data_to_optimize_base['T_c1_wv'])
        
        T_c2_wv = data_to_optimize_end['T_c2_wv']
        self.T_c2_wv = []
        T_KLe_CX = data_to_optimize_end['T_KLe_CX']
        self.T_KLe_CX = []
        for i in range(size_end):
            self.T_c2_wv.append(mat(T_c2_wv[i]))
            self.T_KLe_CX.append(mat(T_KLe_CX[i]))
        
        T_c1_wv = data_to_optimize_base['T_c1_wv']
        self.T_c1_wv = []
        T_KLb_CX = data_to_optimize_base['T_KLb_CX']
        self.T_KLb_CX = []
        for i in range(size_base):
            self.T_c1_wv.append(mat(T_c1_wv[i]))
            self.T_KLb_CX.append(mat(T_KLb_CX[i]))    
    
    def find_transformation(self): #runs an optimization using the n data points stored in store_matrices() to find the transformation between the different frames
        if self.answer == 'y':
            data_path = input('Specify path to .mat file containing transformations T_wv_CX, T_KLb_c1, and T_KLe_c2: ')
            transformations = sio.loadmat(data_path)
            self.T_wv_CX = mat(transformations['T_wv_CX'])
            self.T_KLb_c1 = mat(transformations['T_KLb_c1'])
            self.T_KLe_c2 = mat(transformations['T_KLe_c2'])
            print('T_wv_CX', '\n', self.T_wv_CX)
            print('T_KLb_c1', '\n', self.T_KLb_c1)
            print('T_KLe_c2', '\n', self.T_KLe_c2)
            
            self.T_c1_KLb[0:3, 0:3] = self.T_KLb_c1[0:3,0:3].T
            self.T_c1_KLb[0:3,3] = (-self.T_KLb_c1[0:3,0:3].T)*self.T_KLb_c1[0:3,3]
            self.T_c1_KLb[3,3] = 1.0
            
        elif self.answer == 'n':
            print('Running Optimization 1..........')
            [T_c2_KLe, self.T_wv_CX] = tropt.calc_trans('T', [self.T_KLe_CX, self.T_c2_wv])
            self.T_KLe_c2[0:3, 0:3] = T_c2_KLe[0:3,0:3].T
            self.T_KLe_c2[0:3,3] = (-T_c2_KLe[0:3,0:3].T)*T_c2_KLe[0:3,3]
            self.T_KLe_c2[3,3] = 1.0

            print('Running Optimization 2..........')
            [self.T_c1_KLb, self.T_wv_CX_b] = tropt.calc_trans('T', [self.T_KLb_CX, self.T_c1_wv])
            self.T_KLb_c1[0:3, 0:3] = self.T_c1_KLb[0:3,0:3].T
            self.T_KLb_c1[0:3,3] = (-self.T_c1_KLb[0:3,0:3].T)*self.T_c1_KLb[0:3,3]
            self.T_KLb_c1[3,3] = 1.0

            ans = input('Do you want to write these transformations to a file? (y/n):  ')
            if ans == 'y':
                file_name = input('Enter filename: ')
                sio.savemat(file_name, {'T_wv_CX': self.T_wv_CX, 'T_KLb_c1': self.T_KLb_c1, 'T_KLe_c2': self.T_KLe_c2})
                    
    def print_error(self):
        #this also stores the data to write to file
        #error = self.TrCXe-self.T_KLe_CX_fromvive
        #calculate T_KLe_KLb

        #for storing time
        t = time.time()
        if self.time0 == None:
            self.time0 = t
        self.time = t - self.time0
        self.time_arr.append(self.time)        

        TrCXb_inv = mat(np.zeros((4,4)))
        TrCXb_inv[0:3, 0:3] = self.TrCXb[0:3,0:3].T
        TrCXb_inv[0:3,3] = (-self.TrCXb[0:3,0:3].T)*self.TrCXb[0:3,3]
        TrCXb_inv[3,3] = 1.0
        self.T_KLe_KLb = TrCXb_inv*self.TrCXe
        
        #calculate T_KLe_KLb_fromvive
        TrVc1_inv = mat(np.zeros((4,4)))
        TrVc1_inv[0:3, 0:3] = self.TrVc1[0:3,0:3].T
        TrVc1_inv[0:3,3] = (-self.TrVc1[0:3,0:3].T)*self.TrVc1[0:3,3]
        TrVc1_inv[3,3] = 1.0
        self.T_KLe_KLb_fromvive = self.T_c1_KLb*TrVc1_inv*self.TrVc2*self.T_KLe_c2
        #print np.around(self.T_KLe_KLb_fromvive,3)
        #print np.around(self.T_KLe_KLb,3)
        error_e = self.TrCXe - self.T_KLe_CX_fromvive
        error_b = self.TrCXb - self.T_KLb_CX_fromvive
        error = self.T_KLe_KLb - self.T_KLe_KLb_fromvive
        #print 'x ', np.around(error[0,3]*100.0,3), ' cm'
        #print 'y ', np.around(error[1,3]*100.0,3), ' cm'
        #print 'z ', np.around(error[2,3]*100.0,3), ' cm'

        #for storing data
        T_KLe_KLb_vive_euler = tft.euler_from_matrix(self.T_KLe_KLb_fromvive,'rxyz')
        T_KLe_KLb_CX_euler = tft.euler_from_matrix(self.T_KLe_KLb,'rxyz')
        self.T_KLe_KLb_vive_x.append(self.T_KLe_KLb_fromvive[0,3])
        self.T_KLe_KLb_vive_y.append(self.T_KLe_KLb_fromvive[1,3])
        self.T_KLe_KLb_vive_z.append(self.T_KLe_KLb_fromvive[2,3])
        self.T_KLe_KLb_vive_rx.append(T_KLe_KLb_vive_euler[0])
        self.T_KLe_KLb_vive_ry.append(T_KLe_KLb_vive_euler[1])
        self.T_KLe_KLb_vive_rz.append(T_KLe_KLb_vive_euler[2])
        self.T_KLe_KLb_CX_x.append(self.T_KLe_KLb[0,3])
        self.T_KLe_KLb_CX_y.append(self.T_KLe_KLb[1,3])
        self.T_KLe_KLb_CX_z.append(self.T_KLe_KLb[2,3])
        self.T_KLe_KLb_CX_rx.append(T_KLe_KLb_CX_euler[0])
        self.T_KLe_KLb_CX_ry.append(T_KLe_KLb_CX_euler[1])
        self.T_KLe_KLb_CX_rz.append(T_KLe_KLb_CX_euler[2])

        T_KLe_CX_vive_euler = tft.euler_from_matrix(self.T_KLe_CX_fromvive,'rxyz')
        T_KLe_CX_euler = tft.euler_from_matrix(self.TrCXe,'rxyz')
        self.T_KLe_CX_x.append(self.TrCXe[0,3])
        self.T_KLe_CX_y.append(self.TrCXe[1,3])
        self.T_KLe_CX_z.append(self.TrCXe[2,3])
        self.T_KLe_CX_rx.append(T_KLe_CX_euler[0])
        self.T_KLe_CX_ry.append(T_KLe_CX_euler[1])
        self.T_KLe_CX_rz.append(T_KLe_CX_euler[2])
        self.T_KLe_CX_vive_x.append(self.T_KLe_CX_fromvive[0,3])
        self.T_KLe_CX_vive_y.append(self.T_KLe_CX_fromvive[1,3])
        self.T_KLe_CX_vive_z.append(self.T_KLe_CX_fromvive[2,3])
        self.T_KLe_CX_vive_rx.append(T_KLe_CX_vive_euler[0])
        self.T_KLe_CX_vive_ry.append(T_KLe_CX_vive_euler[1])
        self.T_KLe_CX_vive_rz.append(T_KLe_CX_vive_euler[2])

        T_KLb_CX_vive_euler = tft.euler_from_matrix(self.T_KLb_CX_fromvive,'rxyz')
        T_KLb_CX_euler = tft.euler_from_matrix(self.TrCXb,'rxyz')
        self.T_KLb_CX_x.append(self.TrCXb[0,3])
        self.T_KLb_CX_y.append(self.TrCXb[1,3])
        self.T_KLb_CX_z.append(self.TrCXb[2,3])
        self.T_KLb_CX_rx.append(T_KLb_CX_euler[0])
        self.T_KLb_CX_ry.append(T_KLb_CX_euler[1])
        self.T_KLb_CX_rz.append(T_KLb_CX_euler[2])
        self.T_KLb_CX_vive_x.append(self.T_KLb_CX_fromvive[0,3])
        self.T_KLb_CX_vive_y.append(self.T_KLb_CX_fromvive[1,3])
        self.T_KLb_CX_vive_z.append(self.T_KLb_CX_fromvive[2,3])
        self.T_KLb_CX_vive_rx.append(T_KLb_CX_vive_euler[0])
        self.T_KLb_CX_vive_ry.append(T_KLb_CX_vive_euler[1])
        self.T_KLb_CX_vive_rz.append(T_KLb_CX_vive_euler[2])

        print('euclidian total', np.around(((error[0,3]**2 + error[1,3]**2 + error[2,3]**2)**0.5)*100.0,3), ' cm')
        print('euclidian end', np.around(((error_e[0,3]**2 + error_e[1,3]**2 + error_e[2,3]**2)**0.5)*100.0,3), ' cm')
        print('euclidian base', np.around(((error_b[0,3]**2 + error_b[1,3]**2 + error_b[2,3]**2)**0.5)*100.0,3), ' cm', '\n')
    
    def answer_questions(self):
        if self.answer == 'n':
            have_data = input('Do you already have transform data to optimize (y/n)? ')
            if have_data == 'n':
                self.store_matrices1()
                self.store_matrices2()
            if have_data == 'y':
                self.load_data()
    
    def write_data(self):
        print("Writing data")
        savefile1 = os.environ['HOME'] + '/git/byu/development/kl_ws/src/vive_servoing/src/Data/transformation_data_end_base'
        savefile2 = os.environ['HOME'] + '/git/byu/development/kl_ws/src/vive_servoing/src/Data/transformation_data_end_CX'
        savefile3 = os.environ['HOME'] + '/git/byu/development/kl_ws/src/vive_servoing/src/Data/transformation_data_base_CX'
        sio.savemat(savefile1,mdict={'t':self.time_arr, 'T_KLe_KLb_vive_x': self.T_KLe_KLb_vive_x, 'T_KLe_KLb_vive_y': self.T_KLe_KLb_vive_y, 'T_KLe_KLb_vive_z': self.T_KLe_KLb_vive_z, 'T_KLe_KLb_vive_rx': self.T_KLe_KLb_vive_rx, 'T_KLe_KLb_vive_ry': self.T_KLe_KLb_vive_ry, 'T_KLe_KLb_vive_rz': self.T_KLe_KLb_vive_rz,    'T_KLe_KLb_CX_x': self.T_KLe_KLb_CX_x, 'T_KLe_KLb_CX_y': self.T_KLe_KLb_CX_y, 'T_KLe_KLb_CX_z': self.T_KLe_KLb_CX_z, 'T_KLe_KLb_CX_rx': self.T_KLe_KLb_CX_rx, 'T_KLe_KLb_CX_ry': self.T_KLe_KLb_CX_ry, 'T_KLe_KLb_CX_rz': self.T_KLe_KLb_CX_rz})

        sio.savemat(savefile2,mdict={'t':self.time_arr, 'T_KLe_CX_vive_x': self.T_KLe_CX_vive_x, 'T_KLe_CX_vive_y': self.T_KLe_CX_vive_y, 'T_KLe_CX_vive_z': self.T_KLe_CX_vive_z, 'T_KLe_CX_vive_rx': self.T_KLe_CX_vive_rx, 'T_KLe_CX_vive_ry': self.T_KLe_CX_vive_ry, 'T_KLe_CX_vive_rz': self.T_KLe_CX_vive_rz, 'T_KLe_CX_x': self.T_KLe_CX_x, 'T_KLe_CX_y': self.T_KLe_CX_y, 'T_KLe_CX_z': self.T_KLe_CX_z, 'T_KLe_CX_rx': self.T_KLe_CX_rx, 'T_KLe_CX_ry': self.T_KLe_CX_ry, 'T_KLe_CX_rz': self.T_KLe_CX_rz})

        sio.savemat(savefile3,mdict={'t':self.time_arr, 'T_KLb_CX_vive_x': self.T_KLb_CX_vive_x, 'T_KLb_CX_vive_y': self.T_KLb_CX_vive_y, 'T_KLb_CX_vive_z': self.T_KLb_CX_vive_z, 'T_KLb_CX_vive_rx': self.T_KLb_CX_vive_rx, 'T_KLb_CX_vive_ry': self.T_KLb_CX_vive_ry, 'T_KLb_CX_vive_rz': self.T_KLb_CX_vive_rz, 'T_KLb_CX_x': self.T_KLb_CX_x, 'T_KLb_CX_y': self.T_KLb_CX_y, 'T_KLb_CX_z': self.T_KLb_CX_z, 'T_KLb_CX_rx': self.T_KLb_CX_rx, 'T_KLb_CX_ry': self.T_KLb_CX_ry, 'T_KLb_CX_rz': self.T_KLb_CX_rz})    
        print("Data Written")

if __name__ == '__main__':
    vive_controller_transformation = vive_cortex_transform()
    vive_controller_transformation.answer_questions()        
    vive_controller_transformation.find_transformation()
    rospy.on_shutdown(vive_controller_transformation.write_data)
    while not rospy.is_shutdown():
        vive_controller_transformation.print_error()
        time.sleep(0.1)
