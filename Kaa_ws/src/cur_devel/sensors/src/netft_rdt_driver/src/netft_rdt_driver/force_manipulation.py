#!/usr/bin/env python

import rospy, math, time
from scipy import signal
from geometry_msgs.msg import WrenchStamped
import numpy as np
from copy import deepcopy
import matplotlib.pyplot as plt

class ForceManip():
    ''' Determines Forces and Torques applied to table for comanipulation control'''
    def __init__(self,limb,dist_x,dist_y,dist_z,dt):
        self.dx = dist_x
        self.dy = dist_y
        self.dz = dist_z
        self.dt = dt
        self.limb = limb
        if limb == 'right':
            self.sub = rospy.Subscriber('/transformed_world1',WrenchStamped,self.force_callback)
        if limb == 'left':
            self.sub = rospy.Subscriber('/transformed_world2',WrenchStamped,self.force_callback)
        self.F = np.zeros(6)
        self.prev_F = np.zeros(6)
        self.unfiltered =  np.zeros((6,10))
        self.dF = np.zeros(6)
        self.b,self.a = signal.butter(2,0.01)

    def force_callback(self,msg):
        self.F[0] = msg.wrench.force.x
        self.F[1] = msg.wrench.force.y
        self.F[2] = msg.wrench.force.z
        if self.limb == 'right':
            self.F[3] = msg.wrench.force.z * -self.dy
            self.F[4] = 0.0                        
            self.F[5] = msg.wrench.force.x * self.dy
        else:
            self.F[3] = msg.wrench.force.z * self.dy
            self.F[4] = 0.0                     
            self.F[5] = msg.wrench.force.x * -self.dy

        self.unfiltered = np.roll(self.unfiltered,-1,axis = 1)
        for i in range(len(self.F)):
            self.unfiltered[i][-1] = (self.F[i] - self.prev_F[i])/self.dt
        self.filter_data()
        self.prev_F = deepcopy(self.F)
            
    def filter_data(self):
        '''Filters data'''
        zi = signal.lfilter_zi(self.b,self.a)
        for i in range(np.shape(self.unfiltered)[0]):
            # temp_filt, _ = signal.lfilter(self.b,self.a,self.unfiltered[i,:],zi=zi*self.unfiltered[i,0])
            temp_filt = signal.lfilter(self.b,self.a,self.unfiltered[i,:])
            self.dF[i]= temp_filt[-1]


        
