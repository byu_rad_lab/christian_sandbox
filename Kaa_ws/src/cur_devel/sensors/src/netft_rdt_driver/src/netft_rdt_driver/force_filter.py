#!/usr/bin/env python


import rospy
from geometry_msgs.msg import WrenchStamped,Twist
import numpy as np
import math
from threading import RLock, Timer
import time

class ForceFilter():

#--------VARIABLES--------#

    def __init__(self):
        self.lock=RLock()

        self.ft_1=[[],[],[],[],[],[]]
        self.ft_2=[[],[],[],[],[],[]]

        self.ft_array_1=[[],[],[],[],[],[]]
        self.ft_array_2=[[],[],[],[],[],[]]

        self.filtered_array_1=[[],[],[],[],[],[]]
        self.filtered_array_2=[[],[],[],[],[],[]]

        self.bias_1=[[],[],[],[],[],[]]
        self.bias_2=[[],[],[],[],[],[]]
        
        #4th Order Filter Above 20 Hz
        # self.b=[.046582906636444,.186331626545775,.279497439818662,.186331626545775,.046582906636444]
        # self.a=[1,-.782095198023338,.679978526916300,-.183675697753033,.030118875043169]

        #4th Order Filter Above 10 Hz---Probably better, removes noise from sitting still, but might have more overshoot on step response
        self.b=[.0004165992044065994,.001666396817626,.00249959522644,.001666396817626,.0004165992044065994]
        self.a=[1,-3.180638548874719,3.861194348994214,-2.112155355110969,.43826514226198]

        self.sub1=rospy.Subscriber('/ftnode1/netft_data', WrenchStamped, self.callback_1)
        self.sub2=rospy.Subscriber('/ftnode2/netft_data', WrenchStamped, self.callback_2)

#--------CALLBACK--------#

    def callback_1(self,msg):
        self.ft_1[0]=msg.wrench.force.x
        self.ft_1[1]=msg.wrench.force.y
        self.ft_1[2]=msg.wrench.force.z
        self.ft_1[3]=msg.wrench.torque.x
        self.ft_1[4]=msg.wrench.torque.y
        self.ft_1[5]=msg.wrench.torque.z

    def callback_2(self,msg):
        self.ft_2[0]=msg.wrench.force.x
        self.ft_2[1]=msg.wrench.force.y
        self.ft_2[2]=msg.wrench.force.z
        self.ft_2[3]=msg.wrench.torque.x
        self.ft_2[4]=msg.wrench.torque.y
        self.ft_2[5]=msg.wrench.torque.z

#--------FORCE DEQUE--------#

    def force_array(self,ft1,ft2):
        self.lock.acquire()
        try:
            for i in range(len(self.ft_1)):
                self.ft_array_1[i].append(ft1[i]-self.bias_null(self.bias_1,self.bias_2)[i])
            for i in range(len(self.ft_2)):
                self.ft_array_2[i].append(ft2[i]-self.bias_null(self.bias_1,self.bias_2)[i+6])
            # print self.ft_array_1[2][len(self.ft_array_1[2])-1],'\t',self.ft_array_2[2][len(self.ft_array_2[2])-1]
        finally:
            self.lock.release()

#--------BUTTERWORTH LOW PASS FILTER--------#

    def aunt_jemima(self,ft1,ft2,filt1,filt2,a,b):
        n1=len(ft1[0])-1
        n2=len(ft2[0])-1
        output=[[],[],[],[],[],[],[],[],[],[],[],[]]
        temp_b1=[0,0,0,0,0,0]
        temp_a1=[0,0,0,0,0,0]
        temp_b2=[0,0,0,0,0,0]
        temp_a2=[0,0,0,0,0,0]
        if n1>len(a):
            for i in range(len(output)-6):
                for j in range(len(a)):
                    temp_b1[i]=temp_b1[i]+b[j]*ft1[i][n1-j]
                for k in range(len(a)-1):
                    temp_a1[i]=temp_a1[i]+a[k+1]*filt1[i][n1-(k+1)]
                output[i]=(1/a[0])*(temp_b1[i]-temp_a1[i])
        else:
            for i in range(len(output)-6):
                output[i]=ft1[i][n1]

        if n2>len(a):
            for i in range(len(output)-6):
                for j in range(len(a)):
                    temp_b2[i]=temp_b2[i]+b[j]*ft2[i][n2-j]
                for k in range(len(a)-1):
                    temp_a2[i]=temp_a2[i]+a[k+1]*filt2[i][n2-(k+1)]
                output[i+6]=(1/a[0])*(temp_b2[i]-temp_a2[i])
        else:
            for i in range(len(output)-6):
                output[i+6]=ft2[i][n2]

        return output
            
#--------FILTERED DEQUE--------#

    def get_filt_signal(self):
        self.force_array(self.ft_1,self.ft_2)
        
        filt=self.aunt_jemima(self.ft_array_1,self.ft_array_2,self.filtered_array_1,self.filtered_array_2,self.a,self.b)

        for i in range(len(self.ft_1)):
            self.filtered_array_1[i].append(filt[i])
            self.filtered_array_2[i].append(filt[i+6])
       
#--------BIAS NULLIFICATION--------#                                                                

    def get_bias(self):
        for i in range(len(self.bias_1)):
            self.bias_1[i].append(self.ft_1[i])
            self.bias_2[i].append(self.ft_2[i])
        
    def bias_null(self,bias1,bias2):
        output=[[],[],[],[],[],[],[],[],[],[],[],[]]
        n1=len(bias1[0])-1
        n2=len(bias2[0])-1
        for i in range(len(output)-6):
            output[i]=(bias1[i][n1]+bias1[i][n1-1]+bias1[i][n1-2]+bias1[i][n1-3])/4
        for i in range(len(output)-6):
            output[i+6]=(bias2[i][n2]+bias2[i][n2-1]+bias2[i][n2-2]+bias2[i][n2-3])/4
        return output
  
#--------MAIN FUNCTION--------#

if __name__=='__main__':
    rospy.init_node('Force_Filter', anonymous=True)
    hz=100
    rate=rospy.Rate(hz)
    start=time.time()
    F_mover=ForceFilter()
   
    #GET BIAS
    while not rospy.is_shutdown():
        rate.sleep()
        F_mover.get_bias()
        if time.time()>start+.15:
            break

    while not rospy.is_shutdown(): 
        F_mover.get_filt_signal()
        n=len(F_mover.filtered_array_1[2])
        # print 'Fx1=\t',F_mover.filtered_array_1[0][n-1],'Fy1=\t',F_mover.filtered_array_1[1][n-1],'Fz1=\t',F_mover.filtered_array_1[2][n-1],'Tx1=\t',F_mover.filtered_array_1[3][n-1],'Ty1=\t',F_mover.filtered_array_1[4][n-1],'Tz1=\t',F_mover.filtered_array_1[5][n-1]

        # print 'Fx2=\t',F_mover.filtered_array_2[0][n-1],'Fy2=\t',F_mover.filtered_array_2[1][n-1],'Fz2=\t',F_mover.filtered_array_2[2][n-1],'Tx2=\t',F_mover.filtered_array_2[3][n-1],'Ty2=\t',F_mover.filtered_array_2[4][n-1],'Tz2=\t',F_mover.filtered_array_2[5][n-1]

        # f=open('filtered_3.txt','a+')
        # for i in xrange(6):
        #     f.write(str(F_mover.filtered_array[i][n-1]))
        #     if i==5:
        #         f.write('\t')
        #     else:
        #         f.write(',\t')
        # f.write('\n')
        rate.sleep()
    # f.close()
