#!/usr/bin/env python

import tf.transformations as tft
import numpy as np

class Rotation():
    
    def __init__(self):
        
        self.Rx=tft.rotation_matrix(tft.math.pi,(1,0,0))[0:3,0:3]
        self.Rz=tft.rotation_matrix(tft.math.radians(-120),(0,0,1))[0:3,0:3]
        # self.R_ftg_r=Rx.dot(np.transpose(Rz))
        # self.R_ftg_l=Rx.dot(np.transpose(Rz))
        # Need to figure out what the rotation matrix will be for these and put it in there

        self.ft1=[]
        self.ft2=[]

    def rotate_forces(self,ft1,ft2,trans1,trans2):
        temp1=tft.quaternion_matrix(trans1)[0:3,0:3]
        temp2=tft.quaternion_matrix(trans2)[0:3,0:3]

        self.ft1.append(temp1.dot(self.Rx).dot(np.transpose(self.Rz)).dot(ft1))
        self.ft2.append(temp2.dot(self.Rx).dot(np.transpose(self.Rz)).dot(ft2))
        # self.ft1.append(self.R_ftg_r.dot(temp1).dot(ft1))
        # self.ft2.append(self.R_ftg_l.dot(temp2).dot(ft2))
