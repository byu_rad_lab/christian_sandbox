import numpy as np
import sys

class Integrator:

    def __init__(self, Ki, dt, x_length):
        self.Ki = Ki
        self.dt = dt
        self.x_length = x_length
        self.old_error = np.zeros(self.x_length)
        self.total_error = np.zeros(self.x_length)

    def getError(self, curr_err):
        self.total_error += self.dt*(curr_err + self.old_error)/2.0


    def getIntegralTerm(self, x_err):
        x_err = np.squeeze(np.asarray(x_err)).reshape(1,self.x_length).ravel()
        #print "Q_err inside getIntegralTerm", q_err
        #print "Q_err SHAPE inside getIntegralTerm", q_err.shape

        if x_err.size != self.x_length:
            print("Error: x_length and error vector do not match.")
            sys.exit()

        self.getError(x_err)
        self.old_error = x_err
        return self.Ki*self.total_error

if __name__ == "__main__":

    Ki = 1
    x_err = 5

    integ = Integrator(Ki)

    print((integ.getIntegralTerm(x_err)))
