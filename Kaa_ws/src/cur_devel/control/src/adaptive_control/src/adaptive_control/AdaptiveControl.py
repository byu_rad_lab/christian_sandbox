import numpy as np
from pdb import set_trace as pause
import sys
import rospy
from rospy.numpy_msg import numpy_msg
from rospy_tutorials.msg import Floats

class AdaptiveControl:
    def __init__(self, x_length, u_length, q_length, gamma, sigma, mask=None,
                 path_to_theta0='', should_save=False, alpha=1.):
        # x_length and u_length for preallocating theta in the correct size
        # mask is a boolean np array that specifies the significant Phi terms
        #   a Phi term is "not significant" if the corresponding Theta term is
        #   "small".
        # path_to_theta0 specifies the file path to where the nominal theta
        #   values are stored (if this file doesn't exist it will initialize
        #   theta to zeros)
        # should_save specifies whether or not to save final learned Theta
        #   values at "path_to_theta0".

        ############   Parameters that will likely need tuning #################
        # alpha determines the amount of smoothing between q_des and q_ref
        #   higher alpha is less smoothing/faster response
        # gamma is the rate of "learning"/"adapting"
        # sigma scales the opposing "spring" to maintain stability
        # For more alpha, gamma, sigma explanations see dt_mpc.py and Jon
        #   Terry's Thesis "Adaptive Control for Inflatable Soft Robotic
        #   Manipulators with Unknown Payloads"

        # calculate the number of rows and columns that Theta will have
        nrows = x_length
        ncols = x_length * (x_length + u_length) + x_length
        self.ncols = x_length * (x_length + u_length) + x_length
        self.x_length = x_length

        if path_to_theta0 is None and should_save is True:
            raise ValueError("File path not specified!: A save filepath needs"
                             + "to be specified if should_save is True")

        # Attempt to load Theta0, but initialize to zeros if it doesn't exist
        try:
            self.theta = np.load(path_to_theta0)
        except IOError:
            self.theta = np.zeros((nrows, ncols))

        # Mask theta based on mask. If mask is None, leave Theta as is
        if mask is not None:
            if mask.shape != (nrows, ncols):
                raise ValueError("Mask shape does not match expected theta"
                                 + " shape: make sure mask shape = (x_length, "
                                 + "x_length * (x_length + u_length)")
            self.theta = np.ma.array(self.theta, mask=mask)

        self.should_save = should_save
        self.path_to_theta0 = path_to_theta0
        # self.q_err_ref = 0.
        self.alpha = alpha
        try:
            self.gamma = gamma
        except:
            print("Gamma is the incorrect size")
            print("Gamma must have ",ncols," columns")
            sys.exit()

        try:
            self.sigma = sigma
        except:
            print("Sigma is the incorrect size")
            print("Sigma must have ",ncols," columns")
            sys.exit()

        self.thetaPub = rospy.Publisher("theta",numpy_msg(Floats),queue_size=10)



    def saveTheta0(self):
        np.save(self.path_to_theta0, self.theta)

    def calcPhi(self, A, B, g, x, u):
        # Expects A, B, x, and u as np arrays

        # Cast both vectors as numpy arrays
        x = np.asarray(x)
        u = np.asarray(u)
        A = np.asarray(A)
        B = np.asarray(B)

        #Reshape to the correct dimentions: [n,] and [u,]
        x = x.reshape([x.size,])
        u = u.reshape([u.size,])

        # elementwise  multiplication, with broadcasting results in arrays the
        # same shape as A and B respectively
        A_terms = A*x

        B_terms = B*u

        grav_terms = np.squeeze(np.asarray(g))

        # One row of Phi is a row vector of A_terms and B_terms flattened to a
        # row vector.
        Phi = np.hstack([A_terms.T.ravel(), B_terms.T.ravel(), grav_terms.ravel()])

        # Set any terms that are 0 to a small number so that they are allowed to
        # adapt if they end up having an effect
        Phi[Phi == 0] = 1e-10

        # Make Phi into an array that is the row vector Phi repeated for the
        # length of x.
        # Phi = [Phi for i in range(x.size)]
        Phi = np.array(Phi)

        # Returns an np array that is shape (n, m) where n is x.size and m is
        # the number of terms that could possibly be adapted.
        return Phi

    def calcOmega(self, A, B, g, x, u, x_err, dt, thresh):
        # Expects A as an n x n np array, B as an n x m np array, x as an n x 1
        # np array, u as an m x 1 np array,q and q_des as an n/4 x 1 np array,
        # and dt as a scalar

        Phi = self.calcPhi(A, B, g, x, u)

        # q_ref is a smoothing of q_des. If q_des is a step the abrupt change
        # can make the adaptive parameters unstable
        # q_err_ref_dot = self.alpha*(q_err-self.q_err_ref)

        # Integrate q_ref_dot to update q_ref
        # self.q_err_ref += q_err_ref_dot * dt

        # print "qerr ref: ",self.q_err_ref.T

        # Calculate theta dot using gains. See Eq. 4.11 in Jon Terry's thesis.
        theta_dot = np.zeros(self.theta.shape)
        k = np.shape(x_err)[0]
        for i in range(0,k):
            if(np.abs(x_err[i])>thresh):
                theta_dot[i,:] = (self.gamma*Phi*x_err[i]
                             - self.sigma*self.theta[i,:]*abs(x_err[i]))
            else:
                theta_dot[i,:] = 0.0

        self.theta += theta_dot * dt
        #print self.theta.shape
        #print Phi.shape
        self.saveTheta0()

        # thetafloats = np.array(self.theta,dtype=np.float32)
        # self.thetaPub.publish(thetafloats.flatten())
        # print "HERE: ",thetafloats.shape

        #Calculate omega by summing each of the rows (axis = 1 option)
        #   resulting from elementwise multiplication of theta*Phi.
        #   See Eq 4.12. Omega is size n.
        # Omega = np.sum(self.theta*Phi, axis = 1)
        Omega = np.dot(self.theta,Phi)

        dstrb = Omega
        # dstrb = np.zeros([self.x_length])
        # dstrb[self.x_length-self.q_length:self.x_length] = Omega
        # dstrb[self.x_length-2*self.q_length:self.x_length-self.q_length] = Omega/dt

        return dstrb

if __name__ == "__main__":
    A = np.arange(9).reshape(3,3)
    B = np.arange(6).reshape(3,2)
    x = np.arange(3).reshape(3,1)
    u = np.arange(2)
    g = np.arange(3)
    x_des = x.copy()
    x_des[1] = 2
    x_err = x_des - x

    ctrl = AdaptiveControl(x.size, u.size, 1, gamma = 1, sigma = 0.1, path_to_theta0 = "theta", should_save = True)

    print((ctrl.calcPhi(A, B, g, x, u)))
    print((ctrl.calcOmega(A, B, g, x, u, x_err, .02, .01)))

    test = np.load('theta.npy')
    print("THETA SAVE TEST", test)
