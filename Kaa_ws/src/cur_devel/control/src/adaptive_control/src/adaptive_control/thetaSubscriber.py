import rospy
import numpy as np
from rospy.numpy_msg import numpy_msg
from rospy_tutorials.msg import Floats

def getBigThetaTerms(theta_msg):
    threshold = .00000001
    bigTheta = theta_msg.data[abs(theta_msg.data)>threshold]
    print("Theta:", bigTheta)


def thetaListener():
    rospy.init_node('thetaListener', anonymous = True)
    rospy.Subscriber('theta',numpy_msg(Floats), getBigThetaTerms)    
    rospy.spin()
    
    
if __name__ == '__main__':
    thetaListener()
