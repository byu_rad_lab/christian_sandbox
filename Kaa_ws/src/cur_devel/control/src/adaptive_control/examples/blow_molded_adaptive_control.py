#!/usr/bin/env python
import numpy as np
import math
import rospy
import time
import sys
from copy import deepcopy
from rad_models.BellowsArm2 import BellowsArm
from osqp_mpc.OSQPMPC import OSQPMPC
from adaptive_control.AdaptiveControl import AdaptiveControl
from adaptive_control.simpleIntegrator import Integrator
from ros_srv.srv import uint32_srv #SetRobotCtrlMode
from ros_srv.srv import RequestMoveSrv #RequestMoveSinusoidal
from ros_msg.msg import SystemMinimalMsg
from std_msgs.msg import Float32MultiArray

np.set_printoptions(precision=2)


class BlowMoldedMPC:

    def __init__(self, horizon=10, dt=.02, run_option = ""):
        self.dt = dt
        self.q_goal = np.array([[0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],
                                dtype=np.double).T
        self.run_option = run_option
        self.save_q_error = []
        self.first_time = True
        self.start_time = 0
        
        
        #custom data publishers
        self.q_error_pub = rospy.Publisher('q_error', Float32MultiArray, queue_size=10)

        # Joint states and inputs
        self.q = 0.0*np.ones([6,1])
        self.qd = 0.0*np.ones([6,1])
        self.u = None
        self.pressures = []
        self.pscale = 1000.0

        self.arm = BellowsArm()
        self.arm.K_passive_damper = self.arm.K_passive_damper

        n = self.arm.numStates
        m = self.arm.numInputs
        k = 6

        if self.run_option == 'a':
            ################ Adaptive Control Section #######################
            mygamma = np.ones(n*(n+m)+n)
            mygamma[0:12*n*(n+m)] = mygamma[0:12*n*(n+m)]*1e-4
            mygamma[12*n*(n+m):18*n*(n+m)] = mygamma[12*n*(n+m):18*n*(n+m)]*1e-2
            mygamma[18*n*(n+m):24*n*(n+m)] = mygamma[18*n*(n+m):24*n*(n+m)]*1e-2
            mygamma[24*n*(n+m):24*n*(n+m)+n] = mygamma[24*n*(n+m):24*n*(n+m)+n]*1e-2

            mysigma = np.ones(n*(n+m)+n)
            mysigma[0:12*n*(n+m)] = mysigma[0:12*n*(n+m)]*1e-4
            mysigma[12*n*(n+m):18*n*(n+m)] = mysigma[12*n*(n+m):18*n*(n+m)]*1e-2
            mysigma[18*n*(n+m):24*n*(n+m)] = mysigma[18*n*(n+m):24*n*(n+m)]*1e-2
            mysigma[24*n*(n+m):24*n*(n+m)+n] = mysigma[24*n*(n+m):24*n*(n+m)+n]*1e-2



            self.adaptive_controller = AdaptiveControl(self.arm.numStates,
                                                       self.arm.numInputs,
                                                       k,
                                                       gamma=mygamma,
                                                       sigma=mysigma,
                                                       alpha=0.5, 
                                                       path_to_theta0="theta",
                                                       should_save = True)

        elif self.run_option == "i":
            ####################### Integrator Section #####################
            Ki = 0.5
            self.IntegralController = Integrator(Ki, self.dt, n)

        self.arm.K_passive_damper[2:4,2:4] = self.arm.K_passive_damper[2:4,2:4]
        self.arm.K_passive_spring = self.arm.K_passive_spring


        Q_vel = 0.0
        Q_pos = 1.0
        # Q = 10.0*np.diag      ([0.0,0,0,0,0,0,0,0,0,0,0,0,Q_vel,Q_vel,Q_vel,Q_vel,Q_vel,Q_vel,Q_pos,Q_pos,Q_pos,Q_pos,Q_pos,Q_pos])
        Q = 10.0*np.diag([0.0, 0 , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                          0, 0, 0, 0, Q_pos,Q_pos,Q_pos,Q_pos,Q_pos,Q_pos])
        Qf = 10.0*np.diag([0.0,0,0,0,0,0,0,0,0,0,0,0,
                           Q_vel,Q_vel,Q_vel,Q_vel,Q_vel,Q_vel,
                           Q_pos,Q_pos,Q_pos,Q_pos,Q_pos,Q_pos])
        R = 0.004*np.diag([1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0])
        # R = 0.0008*np.diag([1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0])

        pmin = 8.0
        pmax = 400.0
        qmin = -np.pi
        qmax = np.pi
        umin = np.array([pmin]*12)
        umax = np.array([pmax]*12)
        xmin = np.array([pmin,pmin,pmin,pmin,pmin,pmin,pmin,
                         pmin,pmin,pmin,pmin,pmin,-np.inf,-np.inf,
                         -np.inf,-np.inf,-np.inf,-np.inf,qmin,qmin,
                         qmin,qmin,qmin,qmin])
        xmax = np.array([pmax, pmax, pmax, pmax, pmax, pmax, pmax, pmax, pmax,
                         pmax, pmax, pmax, np.inf, np.inf, np.inf, np.inf, np.inf,
                         np.inf, qmax, qmax, qmax, qmax, qmax, qmax])
        # xmin = np.array([pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,-np.inf,-np.inf,-np.inf,-np.inf,-np.inf,-np.inf,-np.inf,-np.inf,-np.inf,-np.inf,-np.inf,-np.inf])
        # xmax = np.array([pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,np.inf,np.inf,np.inf,np.inf,np.inf,np.inf,np.inf,np.inf,np.inf,np.inf,np.inf,np.inf])

        self.OSQPMPCSolver = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax)

        # subscribers and service calls
        self.robot_state_sub = rospy.Subscriber('/atheris/system_minimal',
                                                SystemMinimalMsg,self.states_cb)

        self.got_initial_state = False
        rate = rospy.Rate(10)
        while not self.got_initial_state:
            rate.sleep()
            print("Waiting for initial robot state")

        self.first = True
        # self.u = deepcopy(self.pressures)
        # self.u_old = deepcopy(self.u)
        # self.x_old = np.vstack([self.pressures,self.qd,self.q])
        # self.Ad_old,self.Bd_old,self.wd_old = self.arm.calc_discrete_A_B_w(self.x_old,self.u,self.dt)
        self.setCtrl = rospy.ServiceProxy('/atheris/SetRobotCtrlMode',
                                            uint32_srv, persistent=True)
        try:
            worked = self.setCtrl(1) #1 for pressure 0 for position
        except rospy.ServiceException as e:
            print('Error :', e)

        self.req_move_sin_pressure = rospy.ServiceProxy(
                                    '/atheris/RequestMoveSinusoidalPressure',
                                    RequestMoveSrv, persistent=False)


    def states_cb(self,msg):
        q0 = np.array(deepcopy(msg.joints_est[0].q))
        q1 = np.array(deepcopy(msg.joints_est[1].q))
        q2 = np.array(deepcopy(msg.joints_est[2].q))
        q = np.reshape(np.hstack([q0,q1,q2]),[6,1])
        self.q = q

        qd0 = np.array(deepcopy(msg.joints_est[0].qd))
        qd1 = np.array(deepcopy(msg.joints_est[1].qd))
        qd2 = np.array(deepcopy(msg.joints_est[2].qd))
        qd = np.reshape(np.hstack([qd0,qd1,qd2]),[6,1])
        self.qd = qd

        p0 = np.array(deepcopy(msg.joints_est[0].prs_bel))
        p1 = np.array(deepcopy(msg.joints_est[1].prs_bel))
        p2 = np.array(deepcopy(msg.joints_est[2].prs_bel))
        p = np.reshape(np.hstack([p0,p1,p2]),[12,1])/self.pscale

        self.pressures = p
        self.got_initial_state=True

    def send_pressure_command(self,pressures,movetime):
        try:
            pascals = self.pscale*pressures
            result = self.req_move_sin_pressure.call(movetime, pascals)
        except:
            print("Invalid Pressures: ",pressures.T)


    def control(self):

        if(self.first==True):
            self.u = deepcopy(self.pressures)
            self.u_old = deepcopy(self.u)
            self.x_old = np.vstack([self.pressures,self.qd,self.q])
            self.Ad_old,self.Bd_old,self.wd_old = self.arm.calc_discrete_A_B_w(
                                                      self.x_old,self.u,self.dt)
            self.first=False

        x_pred = np.dot(self.Ad_old,self.x_old) + np.dot(self.Bd_old,self.u_old) + self.wd_old
        x = np.vstack([self.pressures,self.qd,self.q])

        if self.run_option == "a":
            ######### Adaptive Section ###############
            xerr = (x_pred - x)

            #print "x_pred: ",x_pred.T
            #print "x: ",x.T
            #print "xerr: ",xerr.T

            Omega = np.reshape(self.adaptive_controller.calcOmega
                (self.Ad_old, self.Bd_old, self.wd_old, self.x_old,
                 self.u_old, xerr, self.dt, 0.02),[24,1])

            #print "Adaptive Term: ", Omega

        elif self.run_option == "i":
            ################# Integral Section ##########################
            xerr = x_pred - x

            OmegaIntegralTerm = np.reshape(
                          self.IntegralController.getIntegralTerm(xerr), [24,1])


            #print "Integral Term: ", OmegaIntegralTerm

        Ad,Bd,wd = self.arm.calc_discrete_A_B_w(x,self.u,self.dt)

        if self.run_option == "a":
            ######### Adaptive Section ###############
            wd -= Omega
        elif self.run_option == "i":
            ######### Integral Section ##############
            #print 'wd: ', wd
            wd -= OmegaIntegralTerm

        xgoal = np.vstack([self.pressures,self.qd,self.q_goal])

        u = self.OSQPMPCSolver.solve_for_next_u(Ad, Bd, wd, x, xgoal,
                                                ugoal=self.u, use_c_solver=False)

        try:
            if u==None:
                print("MPC DIDN'T SOLVE!!!")
                u = self.u
        except:
            pass

       
        #if self.first_time == True:
        #    self.start_time = time.time()
        #    self.first_time = False
            
        # print "\nOmega: ", Omega.T
        #print "q error: ",(xgoal-x)[18:24].T
        #print xgoal[18:24].T
        temp = (xgoal-x)[18:24].T
        temp = temp.flatten()
        t = time.time() - self.start_time
        print(t)
        combined = np.hstack([t, temp])
        self.save_q_error.append(combined)
        
        
        self.q_error_pub.publish(data=temp)
        

        self.u_old = self.u
        self.x_old = x
        self.Ad_old = Ad
        self.Bd_old = Bd
        self.wd_old = wd

        self.u = np.reshape(u,[12,1])

        self.send_pressure_command(self.u,.001)




if __name__ == '__main__':
    option = input("Please enter run option: 'a' = adaptive control, 'i' = integral, [Enter] = none. \n")
    if option == 'a':
        print("Using adaptive control.")
    elif option == 'i':
        print("Using integrator.")
    else:
        print("Using nominal MPC.")

    if option != "a" and option != "i" and option != "":
        print("Invalid option")
        sys.exit()
    rospy.init_node('mpc_control', anonymous=True)
    horizon = 10
    mpc = BlowMoldedMPC(horizon,dt=.02, run_option = option)
    rate = rospy.Rate(50)

    mpc.send_pressure_command(200.*np.ones(12),1.0)

    # Time to get to E stop

    # mpc.q_goal = np.array([[0.0,0.0,0.0,0.0,0.0,0.0]],dtype=np.double).T
    # start = time.time()
    # mpc.control()
    # print "MPC time: ",time.time()-start
    t_settle = 20.0
    start = time.time()
    A = .4
    w = 0.5
    dynamic = False

    while not rospy.is_shutdown():
        if dynamic==False:
            if time.time() - start < t_settle:
                mpc.q_goal = np.array([[0.0,    0.0,     -0.0,    -0.0,  0.0,   0.0]]).T
            elif time.time() - start > t_settle and time.time() - start < t_settle*2:
                mpc.q_goal = np.array([0.5,     0.5,   -0.5,   0.5,   0.5, 0.5]).reshape(6,1)
            elif time.time() - start > t_settle*2 and time.time() - start < t_settle*3:
                mpc.q_goal = np.array([0.5,   0.5,   0.5,   -0.5,  -0.5,  0.5]).reshape(6,1)
            elif time.time() - start > t_settle*3 and time.time() - start < t_settle*4:
                mpc.q_goal = np.array([-0.5, 0.5, 0.0,     0.,    0.5,  0.5]).reshape(6,1)
            elif time.time() - start > t_settle*4 and time.time() - start < t_settle*5:
                mpc.q_goal = np.array([[0.0,    0.0,     -0.5,    -0.5,  0.0,   0.0]]).reshape(6,1)
            elif time.time() > t_settle*5:
                start = time.time()
        else:
            t = time.time()-start
            mpc.q_goal = np.ones(6).reshape(6,1)*A*np.sin(w*t)

        start2 = time.time()
        mpc.control()
        rate.sleep()
        
        
    #print "Saving error data..."
    #error_data = np.asarray(mpc.save_q_error)
    #np.savetxt('MPCA_qerror.txt', error_data)
    #print "Data saved successfully."
#    try:
#        error_data = np.asarray(mpc.save_q_error)
#        np.savetxt('qerror_data.txt', error_data)
#        print "Data saved successfully."
#    except:
#        print "Error saving data."
           
    mpc.send_pressure_command(0*np.ones(12),0.001)
