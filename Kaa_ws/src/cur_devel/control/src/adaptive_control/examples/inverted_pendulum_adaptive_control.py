import numpy as np
import time
import matplotlib.pyplot as plt
from osqp_mpc.SmallMatrixOSQPMPC import OSQPMPC
from rad_models.InvertedPendulum import *
from adaptive_control.AdaptiveControl import AdaptiveControl

if __name__=='__main__':

    ### TODO: need to add command line option for adaptive control.

    sys = InvertedPendulum(mass=.02)
    false_sys = InvertedPendulum(mass =.02*5)
    numStates = sys.numStates
    numInputs = sys.numInputs
    adaptive = AdaptiveControl(x_length=numStates, u_length=numInputs,
                q_length=1, gamma=0.01,sigma=0.1)

    Q = 1.0*np.diag([0,1.0])
    Qf = 10.0*np.diag([0,1.0])
    R = .0*np.diag([1.0])
    horizon = 100
    dt = .01

    mpc = OSQPMPC(Q,
                  Qf,
                  R,
                  horizon,
                  umin=[-sys.uMax]*sys.numInputs,
                  umax=[sys.uMax]*sys.numInputs,
                  xmin=[-np.inf,-np.pi],
                  xmax=[np.inf,np.pi])

    x = np.array([0,-np.pi]).reshape(sys.numStates,1)
    x_false = np.array([0,-np.pi]).reshape(sys.numStates,1)
    u = np.zeros([sys.numInputs,1])
    u_false = np.zeros([sys.numInputs,1])
    # xgoal = np.zeros([sys.numStates,1])
    xgoal = np.array([0,-np.pi/2]).reshape(sys.numStates,1)

    x_hist = np.zeros([sys.numStates,horizon])
    u_hist = np.zeros([sys.numInputs,horizon])
    x_hist_false = np.zeros([sys.numStates,horizon])
    u_hist_false = np.zeros([sys.numInputs,horizon])

    for i in range(0,horizon):
        [Ad,Bd,wd] = sys.calc_discrete_A_B_w(x,u,dt)
        [Ad_false,Bd_false,wd_false] = false_sys.calc_discrete_A_B_w(x_false,u_false,dt)

        #Calculate omega
        Omega = adaptive.calcOmega(Ad_false,Bd_false,wd_false,x_false,u,xgoal-x_false,dt,thresh=.0001)
        Omega = Omega.reshape([2,1])
        Omega[1] = 0 #is this allowed??
        #Subtract omega from disturbance terms
        #wd = wd.squeeze()
        # print wd_false - Omega, "\n"

        ## TODO: this line breaks the code, maybe because it makes the second entry
        # in wd non zero and also maybe because it makes the first >1?
        wd_false -= Omega

        u = mpc.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal=np.zeros([sys.numInputs,1]))
        u_false = mpc.solve_for_next_u(Ad_false,Bd_false,wd_false,x_false,xgoal,ugoal=np.zeros([sys.numInputs,1]))

        #forward simulate with true sys but with bad u
        x = sys.forward_simulate_dt(x,u,.01)
        x_false = sys.forward_simulate_dt(x_false,u_false,.01)

        plt.figure(1)
        sys.visualize(x)
        plt.figure(2)
        false_sys.visualize(x_false)

        u_hist[:,i] = u.flatten()
        x_hist[:,i] = x.flatten()
        u_hist_false[:,i] = u_false.flatten()
        x_hist_false[:,i] = x_false.flatten()

    plt.figure(3)
    plt.plot(x_hist[1,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Theta (rad)')
    plt.title('Position')
    plt.show()

    plt.figure(4)
    plt.plot(x_hist_false[1,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Theta (rad)')
    plt.title('Adaptive Position Bad Model')
    plt.show()

    plt.figure(5)
    plt.plot(u_hist.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Torque (Nm)')
    plt.title('Input')

    plt.figure(6)
    plt.plot(u_hist_false.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Torque (Nm)')
    plt.title('Adaptive Input Bad Model')

    plt.pause(1000)
