# Written by Curtis Johnson, 12/16/2019


import sympy as sp
from sympy.physics.vector import dynamicsymbols
import numpy as np
from copy import deepcopy
sp.init_printing(use_unicode=True)



#Define other symbolic parameters. Note that h defines where along the arc you want everything at. For example, if h is total length of center arc (H), you are at the tip of the grub. For our purposes, since I is defined below at COM, we approximate this to be at about h = H/2.
[h, m, r, t] = sp.symbols('h m r t')


#make necessary vars functions of time (see https://docs.sympy.org/latest/modules/physics/vector/api/functions.html) and define needed variables
# [u,v] = dynamicsymbols('u v') #returns u(t) and v(t)

u = sp.Function('u')(t)
v = sp.Function('v')(t)

phi = sp.sqrt(u**2+v**2)

u_tilde = u/phi
v_tilde = v/phi

sigma = sp.cos(phi) - 1

#Define inertia matrix. Grub is approximated as a cylinder
Icom = sp.Matrix([
    [(1.0/12.0)*m*(3*r**2 + h**2),0,0],
    [0,(1.0/12.0)*m*(3*r**2 + h**2),0],
    [0,0,(1.0/2.0)*m*r**2]])

q = sp.Matrix([[u],[v]])
qdot = sp.Matrix([[sp.diff(u,t)],[sp.diff(v,t)]])

#Define homogeneous transform between base frame and any point along spine (COM in our case)
g = sp.Matrix([
    [sigma*v_tilde**2 + 1, -sigma*u_tilde*v_tilde, v_tilde*sp.sin(phi), -sigma*h*v_tilde/phi],
    [-sigma*u_tilde*v_tilde, sigma*u_tilde**2 + 1, -u_tilde*sp.sin(phi), sigma*h*u_tilde/phi],
    [-v_tilde*sp.sin(phi), u_tilde*sp.sin(phi), sp.cos(phi), h*sp.sin(phi)/phi],
    [0,0,0,1]
    ])

#Rotation matrix from base to COM
R = g[0:3,0:3]

#translation from base to COM:
trans = g[0:3, 3]


#Define jacobian in terms of u, v, and phi.
J = sp.Matrix([
    [0, sigma*h/phi**2],
    [-sigma*h/phi**2, 0],
    [h*u_tilde*(phi-sp.sin(phi))/phi**2, h*v_tilde*(phi - sp.sin(phi))/phi**2],
    [u_tilde**4 + u_tilde**2*v_tilde**2 + v_tilde**2*sp.sin(phi)/phi, u_tilde*v_tilde*(phi - sp.sin(phi))/phi],
    [u_tilde*v_tilde*(phi - sp.sin(phi))/phi, v_tilde**4 + u_tilde**2*v_tilde**2 + v_tilde**2*sp.sin(phi)/phi],
    [sigma*v_tilde/phi, -sigma*u_tilde/phi]
    ])

#Define Jv (how rotation about u and v contribute to linear velocity) syntax is weird: 0:3 means rows 0,1, and 2 (up to 2)
Jv = J[0:3, :]

#Define Jw (how rotation about u and v contribute to angular velocity) -3 means third from the end or the 4th row
Jw = J[-3:, :]

#Kinetic energy
K = (1.0/2.0)*sp.transpose(qdot)*(m*sp.transpose(Jv)*Jv + sp.transpose(Jw)*R*Icom*sp.transpose(R)*Jw)*qdot

#Potential energy
grav = sp.Matrix([[0.0],[0.0],[9.81]])
P = m*sp.transpose(grav)*trans

#Lagrangian
L = sp.Matrix(K-P)

#EoM: take partials with respect to udot and u, vdot and v, then time
dLdu = sp.Matrix(sp.diff(L,q[0]))
dLdudot = sp.Matrix(sp.diff(L, qdot[0]))
dLdudot_dot = sp.Matrix(sp.diff(dLdudot, t))

dLdv = sp.Matrix(sp.diff(L,q[1]))
dLdvdot = sp.Matrix(sp.diff(L, qdot[1]))
dLdvdot_dot = sp.Matrix(sp.diff(dLdvdot, t))

tao = sp.Matrix([[dLdudot_dot - dLdu],[dLdvdot_dot - dLdv]])

#Get m from K.
M = m*sp.transpose(Jv)*Jv + sp.transpose(Jw)*R*Icom*sp.transpose(R)*Jw
#Get G from dPdq partial of potential wrt q, (u and v to give 2x1 vector).
G = sp.zeros(2,1)
G[0] = sp.diff(P, u)
G[1] = sp.diff(P, v)
#Get C*qdot from bellows_arm_gen_eqns commented out Coriolis stuff.

#use Phil's c code generation stuff:
print("Calculating Coriolis Matrix")
n = 2
C = sp.zeros(n,n)
for i in range(0,n):
    print("i: ",i)
    for j in range(0,n):
        print("j: ",j)
        Cij = 0
        for k in range(0,n):
            print("k: ",k)
            Cij += .5*(sp.diff(M[i,j],q[k]) + sp.diff(M[i,k],q[j]) - sp.diff(M[j,k],q[i]))*qdot[k]
        C[i,j] = deepcopy(Cij)
print("Calculated Coriolis Matrix")


# Get rid of time dependence within sympy
def dummify_undefined_functions(expr):
    mapping = {}
    for der in expr.atoms(u,v):
        f_name = der.func.__name__
        name = f_name
        mapping[der] = sp.Symbol(name)
    return expr.subs(mapping)

def print_sympy_for_python_wrapping(expr,cse=True):
    expr = dummify_undefined_functions(expr)
    code = ''
    n,m = expr.shape
    terms_list = []
    for i in range(0,n):
        for j in range(0,m):
            terms_list.append(expr[i,j])

    if(cse==True):
        print('doing cse')
        sub_exprs, terms_list = sp.cse(terms_list)

        for i in range(0,len(sub_exprs)):
            code += "\n\tdouble " + sp.ccode(sub_exprs[i][0])+" = "+sp.ccode((sub_exprs[i][1]))+";"

    for i in range(0,n):
        if(m>1):
            for j in range(0,m):
                code += "\n\t out["+str(i*n+j)+"] = "+sp.ccode((terms_list[i*n+j]))+";"
        else:
            code += "\n\t out["+str(i)+"] = "+sp.ccode((terms_list[i]))+";"
    return code



filename = 'bellowsgrubcylinderdynamics'
c_code = ''


print("Generating Mass matrix code")
c_code+='#include "'+'bellowsgrubintegraldynamics.h"\n#include <Eigen/LU>\n#include <math.h>\n\nnamespace py = pybind11;\n\n'

c_code+="py::array calc_M_py(std::vector<double> q,double h,double m,double r)\n{"
c_code+="\n\t double out["+str(n*n)+"];\n"
c_code+= print_sympy_for_python_wrapping(M)
c_code+="\n\treturn py::array("+str(n*n)+",out);\n"
c_code+="\n}\n\n"

print("Generating gravity vector code")
c_code+="py::array calc_grav_py(std::vector<double> q,double h,double m,double g)\n{"
c_code+="\n\t double out["+str(n)+"];\n"
c_code+= print_sympy_for_python_wrapping(G)
c_code+="\n\treturn py::array("+str(n)+",out);\n}\n\n"

print("Generating Coriolis Matrix code")
c_code+="py::array calc_C_py(std::vector<double> q,std::vector<double> qd,double h,double m,double r)\n{"
c_code+="\n\t double out["+str(n*n)+"];\n"
c_code+= print_sympy_for_python_wrapping(C)
c_code+="\n\treturn py::array("+str(n*n)+",out);\n}\n\n"

def print_fk_function(code,fk,link):
    code+="py::array fk"+str(link)+"_py(std::vector<double> q,double h)\n{"
    code+="\n\t double out[16];\n"

    code += print_sympy_for_python_wrapping(dummify_undefined_functions(fk))

    code+="\n\treturn py::array(16,out);\n"
    code+="\n}\n\n"
    return code

c_code = print_fk_function(c_code,g,'End')

print("Writing code to file")
cf = open(filename+".c",'w')

cf.write(c_code)
cf.close()
