import numpy as np
import time
import matplotlib.pyplot as plt

class MPPI():

    def __init__(self,
                 model,
                 cost_function,
                 numStates,
                 numInputs,
                 umin,
                 umax,
                 horizon=50,
                 numSims=500,
                 numKnotPoints=2,
                 dt=.01):

        self.model = model
        self.cost_function = cost_function
        self.n = numStates
        self.m = numInputs
        self.umin = np.array(umin).reshape(numInputs,1)
        self.umax = np.array(umax).reshape(numInputs,1)
        self.uRange = (np.array(umax)-np.array(umin)).reshape(numInputs,1)
        self.horizon = horizon
        self.numSims = numSims
        self.dt = dt
        self.numKnotPoints = numKnotPoints
        self.segmentLength = float(self.horizon)/(self.numKnotPoints-1)
        self.U = np.zeros([self.numKnotPoints,self.m,self.numSims])
        self.costs = np.zeros([1,self.numSims])        
        self.X = np.zeros([self.horizon,self.n,self.numSims])

    def perturb_trajectories(self,mutation_noise):
        self.U = self.U + np.random.randn(self.numKnotPoints,self.m,self.numSims)*mutation_noise
        self.U[self.U>self.umax[0]] = self.umax[0]
        self.U[self.U<self.umin[0]] = self.umin[0]
        return self.U

    def get_us_from_U(self,U,i,horizon,numKnotPoints):

        # Parameterized
        knot = int(i/self.segmentLength)
        ui = U[knot,:,:]*(1.0-float(i-knot)/self.segmentLength) + U[knot+1,:,:]*(float(i-knot)/self.segmentLength)

        # Not parameterized
        # ui = U[i,:,:]
        
        ui[ui>self.umax[0]] = self.umax[0]
        ui[ui<self.umin[0]] = self.umin[0]
        return ui

    def get_costs_from_trajectories(self,x0,xgoal,ugoal,U):
        self.costs = np.zeros([1,self.numSims])        
        self.X[0,:,:] = x0

        for i in range(0,self.horizon-1):
            ui = self.get_us_from_U(U,i,self.horizon,self.numKnotPoints)
            self.X[i+1,:,:] = self.model(self.X[i,:,:],ui,self.dt)
            self.costs += self.cost_function(self.X[i,:,:],ui,xgoal,ugoal)
        
        FinalCosts = self.cost_function(self.X[self.horizon-1,:,:],ui,xgoal,ugoal,final_timestep=True)
        self.costs += FinalCosts

        return self.costs

    def update_best_policy(self,U,costs):

        # Stupid sampling
        # which_sim = np.argmin(costs)
        # U_star = U[:,:,which_sim]

        # MPPI
        costs = costs/np.mean(costs)*100.0
        U_star = np.zeros([U.shape[0],U.shape[1]])        
        sum_of_exp_costs = np.sum(np.exp(-costs))
        for i in range(0,U.shape[0]):
            for j in range(0,U.shape[1]):
                U_star[i,j] = np.sum(U[i,j,:]*np.exp(-costs)/sum_of_exp_costs)

        return U_star
    

    def get_next_u_from_U_star(self,U_star):
        next_u = U_star[0,:]
        next_u[next_u>self.umax[0]] = self.umax[0]
        next_u[next_u<self.umin[0]] = self.umin[0]
        return next_u

    

    def solve_for_next_u(self,U0,x0,xgoal,ugoal,mutation_noise):

        self.U = np.repeat(U0[:,:,np.newaxis],self.numSims,axis=2)
        
        self.U = self.perturb_trajectories(mutation_noise)

        self.costs = self.get_costs_from_trajectories(x0,xgoal,ugoal,self.U)
        
        self.U_star = self.update_best_policy(self.U,self.costs)

        next_u = self.get_next_u_from_U_star(self.U_star)

        return next_u


if __name__=='__main__':

    from rad_models.LearnedThreelink import *
    from rad_models.Threelink import *

    import time

    learnedSys = LearnedThreelink()
    sys = Threelink()    
    numStates = sys.numStates
    numInputs = sys.numInputs
    Q = 1.0*np.diag([0,0,0,3,2,1.0])
    Qf = 10.0*np.diag([0,0,0,3,2,1.0])    
    R = 0.0001*np.eye(3)

    # This cost function must take in vectors of shapes:
    # x: [numStates,numSims]
    # u: [numInputs,numSims]
    # and return costs in the shape:
    # cost: [1,numSims]
    def myCostFcn(x,u,xgoal,ugoal,final_timestep=False):
        cost = np.zeros(u.shape)
        if final_timestep:
            Qx = np.abs(Qf.dot(x-xgoal)**2.0)
            cost = np.sum(Qx,axis=0)
        else:
            Qx = np.abs(Q.dot(x-xgoal)**2.0)
            Ru = np.abs(R.dot(u-ugoal))
            cost = np.sum(Qx,axis=0) + np.sum(Ru,axis=0)
        return cost

    numParams = 3
    controller = MPPI(learnedSys.forward_simulate_dt,
                      myCostFcn,
                      numStates,
                      numInputs,
                      umin=[-sys.uMax,-sys.uMax,-sys.uMax],
                      umax=[sys.uMax,sys.uMax,sys.uMax],
                      horizon=25,
                      numSims=500,
                      numKnotPoints=numParams,
                      dt=.01)
    
    x = np.array([0,0,0,-np.pi,0,0]).reshape(sys.numStates,1)
    xgoal = np.array([0,0,0,0,0,0.]).reshape(sys.numStates,1)
    U0 = np.zeros([numParams,sys.numInputs])

    fig = plt.figure(10)
    plt.ion()

    for i in range(0,1000):
        start = time.time()
        noise = np.sum(np.abs(xgoal-x))*.1
        # noise = .1
        u = controller.solve_for_next_u(U0,x,xgoal,np.zeros([sys.numInputs,1]),noise)
        x = sys.forward_simulate_dt(x,u,.01)

        plt.figure(10)
        print('x: ',x.T)        
        print('u: ',u.T)
        sys.visualize(x,u)
        
