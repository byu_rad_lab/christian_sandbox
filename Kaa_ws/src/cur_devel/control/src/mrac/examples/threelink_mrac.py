from rad_models.N3link import N3link
from rad_models.n3linkdynamics import calc_M, calc_grav, calc_C
import numpy as np
from mrac.MRAC import MRAC
import scipy
from copy import deepcopy
import matplotlib.pyplot as plt
plt.ion()    

if __name__=='__main__':

    dt = .01
    
    m = 1.0
    b = 1.5
    k = 3.0
    Kp = 10.0
    Kd = 3.0
    Aref = np.array([[-(b+Kd)/(m), 0, 0, -(k+Kp)/(m), 0, 0],
                     [0, -(b+Kd)/(m), 0, 0, -(k+Kp)/(m), 0],
                     [0, 0, -(b+Kd)/m, 0, 0, -(k+Kp)/m],
                     [1.0, 0, 0, 0, 0, 0],
                     [0, 1.0, 0, 0, 0, 0],
                     [0, 0, 1.0, 0, 0, 0]])                     
    Bref = np.array([[Kp/(m), 0, 0],
                     [0, Kp/(m), 0],
                     [0, 0, Kp/m],
                     [0, 0, 0],
                     [0, 0, 0],
                     [0, 0, 0]])

    Adref = scipy.linalg.expm(Aref*dt)
    Bdref = np.matmul(np.linalg.inv(Aref),np.matmul(Adref-np.eye(Adref.shape[0]),Bref))
    # Adref = np.linalg.inv(np.eye(6)-Aref*dt)
    # Bdref = Adref.dot(Bref)*dt

    # This is the vector of basis functions used to approximate a matched disturbance
    def regressor_func(x,r):
        x = np.matrix(x)
        r = np.matrix(r)        
        q = x[3:6].squeeze().tolist()[0]#+np.random.randn(3)*.1
        qd = x[0:3].squeeze().tolist()[0]#+np.random.randn(3)*.2
        M = calc_M(q,sys.l,sys.m,sys.I).squeeze().reshape(sys.numStates/2,sys.numStates/2)
        C = calc_C(q,qd,sys.l,sys.m,sys.I).squeeze().reshape(sys.numStates/2,sys.numStates/2)
        g = calc_grav(q,sys.l,sys.m,sys.g).squeeze().reshape(sys.numStates/2,1)

        Phi = np.vstack([g,
                         (C.dot(x[0:3])),
                         (-M.dot(x[0:3])*(b+Kd)/m),
                         (-M.dot(x[3:6])*(k+Kp)/m),
                         (M.dot(r)*Kp/m),
                         x])
        return Phi.reshape(-1,1)

    # This defines how my dynamics are
    sys = N3link(g=9.81,l=.5)
    sys.uMax = np.inf

    x = np.matrix([0,0,0,0,0,0.0]).reshape(sys.numStates,1)
    xref = deepcopy(x)

    qd = x[0:sys.numInputs]    
    q = x[sys.numInputs:]
    Ad,Bd,wd = sys.calc_discrete_A_B_w(q,qd,dt)
    
    controller = MRAC(Adref,Bdref,Bd,regressor_func,x,Gamma_x=0.0,Gamma_r=0.0,Gamma_theta=0.1)

    sim_length = 300000
    xref_hist = np.zeros([sys.numStates,sim_length])
    x_hist = np.zeros([sys.numStates,sim_length])
    u_hist = np.zeros([sys.numInputs,sim_length])
    r_hist = np.zeros([Bdref.shape[1],sim_length])
    Kx_hist = np.zeros([controller.n*controller.m,sim_length])
    Kr_hist = np.zeros([controller.m*controller.m,sim_length])
    Theta_hist = np.zeros([controller.N*controller.m,sim_length])
    e_hist = np.zeros([1,sim_length])    
    r = np.array([[1.0],
                  [-1.0],
                  [1.0]])*1.0

    d = 10

    for i in range(0,sim_length):

        if i%d==0:
            # r = -r*1.1 + np.random.randn(3,1)*1.0
            r = np.random.randn(3,1)*1.0
        if i%(10000)==0:
            d+=10

        r[abs(r)>5.0] = 0.5

        # This is Phil experimenting
        # q = x[3:6].squeeze().tolist()[0]
        # qd = x[0:3].squeeze().tolist()[0]
        # M = calc_M(q,sys.l,sys.m,sys.I).squeeze().reshape(sys.numStates/2,sys.numStates/2)
        # C = calc_C(q,qd,sys.l,sys.m,sys.I).squeeze().reshape(sys.numStates/2,sys.numStates/2)
        # g = calc_grav(q,sys.l,sys.m,sys.g).squeeze().reshape(sys.numStates/2,1)

        # ThetaPhi = g
        # ThetaPhi += C.dot(x[0:3])
        # ThetaPhi += -M.dot(x[0:3])*(b+Kd)/m
        # ThetaPhi += -M.dot(x[3:6])*(k+Kp)/m
        # ThetaPhi += M.dot(r)*Kp/m
        # ThetaPhi += np.eye(3).dot(x[3:6])*sys.damping
        

        # u = np.matrix(ThetaPhi)
        u = controller.solve_for_next_u(x,r)
        e = np.linalg.norm(x-controller.xref)
        x = sys.forward_simulate_dt(x,u,dt,wrapAngle=False)

        # controller.xref = Adref.dot(x) + Bdref.dot(r)
        # controller.update_estimates(x,r)        

        x_hist[:,i] = x.flatten()
        u_hist[:,i] = u.flatten()
        r_hist[:,i] = r.flatten()        
        xref_hist[:,i] = controller.xref.flatten()
        Kx_hist[:,i] = controller.Kx.flatten()
        Kr_hist[:,i] = controller.Kr.flatten()
        Theta_hist[:,i] = controller.Theta.flatten()
        e_hist[:,i] = e

        # if i%10==0:
        #     sys.visualize(x.reshape(-1,1))

        if i%10000==0:

            # Gradually change the learning rate
            if(i%50000==0):
                controller.Gamma_theta = controller.Gamma_theta*.8

            plt.figure(4)
            plt.clf()
            for j in range(0,sys.numStates/2):
                plt.subplot(sys.numStates/2,1,j+1)
                plt.plot(x_hist[j+3,0:i].T,'b')
                plt.plot(xref_hist[j+3,0:i].T,'r--')
                plt.legend(['x','x_ref'])
                plt.ylabel('Angle (rad)')
                plt.title('Angle')

            plt.figure(2)
            plt.clf()
            for j in range(0,sys.numInputs):
                plt.subplot(sys.numInputs,1,j+1)
                plt.plot(u_hist[j,0:i].T,'b')
                plt.plot(r_hist[j,0:i].T,'r--')
                plt.legend(['x','x_ref'])
                plt.ylabel('Input (Nm)')
                plt.title('Inputs')
                
            plt.figure(3)
            plt.clf()
            plt.plot(Theta_hist[:,0:i].T,'g')            
            plt.plot(Kx_hist[:,0:i].T,'b')
            plt.plot(Kr_hist[:,0:i].T,'r')
            plt.title('Adaptive Parameters')
            plt.show()
            plt.pause(.0001)
            
            plt.figure(5)
            plt.clf()
            plt.plot(np.log(e_hist[:,0:i]).T,'r')            
            plt.title('Log Reference Error')
            plt.show()
            plt.pause(.0001)


    plt.figure(4)
    plt.clf()
    for j in range(0,sys.numStates/2):
        plt.subplot(sys.numStates/2,1,j+1)
        plt.plot(x_hist[j+3,:].T,'b')
        plt.plot(xref_hist[j+3,:].T,'r--')
        plt.legend(['x','x_ref'])
        plt.ylabel('Angle (rad)')
        plt.title('Angle')
        
    plt.figure(2)
    plt.clf()
    for j in range(0,sys.numInputs):
        plt.subplot(sys.numInputs,1,j+1)
        plt.plot(u_hist[j,:].T,'b')
        plt.plot(r_hist[j,:].T,'r--')
        plt.legend(['x','x_ref'])
        plt.ylabel('Input (Nm)')
        plt.title('Inputs')
        
    plt.figure(3)
    plt.clf()    
    plt.plot(Theta_hist.T,'g')
    plt.plot(Kx_hist.T,'b')
    plt.plot(Kr_hist.T,'r')
    plt.title('Adaptive Parameters')
    plt.show()
    plt.pause(.0001)

    plt.figure(5)
    plt.clf()
    plt.plot(np.log(e_hist).T,'r')            
    plt.title('Reference Error')
    plt.show()
    plt.pause(.0001)
    
    
    plt.pause(100000)
