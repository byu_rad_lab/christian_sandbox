from rad_models.BellowsGrubIntegralNoPressures import BellowsGrub
import scipy
from copy import deepcopy
import matplotlib.pyplot as plt
from mrac.ManipulatorMRAC import ManipulatorMRAC
from osqp_mpc.ParameterizedSmallMatrixOSQPMPC import OSQPMPC
import numpy as np
import scipy.io as sio

def run_perfect_regressor_experiment(modeling_error,spring_offset):

    def u_to_pressures(u,pmean,umin,umax):
        pressures = np.matrix([[pmean+u[0,0]/2.],
                               [pmean-u[0,0]/2.],
                               [pmean+u[1,0]/2.],
                               [pmean-u[1,0]/2.]])
        
        pressures[pressures<umin] = umin
        pressures[pressures>umax] = umax
        
        return pressures

    def my_regressor(q,qd,qd_r,qdd_r):
        q = np.array(q)
        qd = np.array(qd)
        qd_r = np.array(qd_r)
        qdd_r = np.array(qdd_r)        
        reg = grub.calc_grub_regressor(q,qd,qd_r,qdd_r)

        reg = np.hstack([reg,q])
        reg = np.hstack([reg,qd])
        
        # reg = np.hstack([reg,np.eye(2)*qd[0],np.eye(2)*qd[1]])
        # reg = np.hstack([reg,np.eye(2)*q[0],np.eye(2)*q[1]])
        #reg = np.hstack([reg,np.eye(2)])
        
        # reg = np.hstack([reg,np.eye(2)*qd[0]*np.cos(q[0])**2,np.eye(2)*qd[1]*np.cos(q[1])**2])
        # reg = np.hstack([reg,np.eye(2)*q[0]*np.cos(qd[0])**2,np.eye(2)*q[1]*np.cos(qd[1])**2])
        # reg = np.hstack([reg,np.eye(2)*qd[0]*np.sin(q[0])**2,np.eye(2)*qd[1]*np.sin(q[1])**2])
        # reg = np.hstack([reg,np.eye(2)*q[0]*np.sin(qd[0])**2,np.eye(2)*q[1]*np.sin(qd[1])**2])
        # reg = np.hstack([reg,np.eye(2)])        

        
        return reg
    
    grub = BellowsGrub(spring_offset=spring_offset)
    wrong_grub = BellowsGrub(spring_offset=0.0)

    # Introduce Model Error
    wrong_grub.K_passive_damper = wrong_grub.K_passive_damper*modeling_error
    wrong_grub.K_passive_spring = wrong_grub.K_passive_spring*modeling_error
    wrong_grub.h = grub.h*modeling_error
    wrong_grub.m = grub.m*modeling_error
    wrong_grub.r = grub.r

    Lambda = 5.0
    Gamma = 10.0
    KD = 10.0
    sigma = 0.0
    
    mracmpc = ManipulatorMRAC(my_regressor,2,Lambda*2.0,Gamma*2.0,KD*0.0,sigma)
    mrac = ManipulatorMRAC(my_regressor,2,Lambda,Gamma,KD,sigma)
    # mrac.a[0] = wrong_grub.m/wrong_grub.prs_to_torque[0,0]
    # mrac.a[1-1] = wrong_grub.h**2/wrong_grub.prs_to_torque[0,0]
    # mrac.a[2-1] = wrong_grub.r**2/wrong_grub.prs_to_torque[0,0]
    # mrac.a[3-1] = wrong_grub.K_passive_spring[0,0]/wrong_grub.prs_to_torque[0,0]
    # mrac.a[4-1] = wrong_grub.K_passive_damper[0,0]/wrong_grub.prs_to_torque[0,0]
    
    # mrac.a[0] = wrong_grub.m/wrong_grub.prs_to_torque[0,0]
    # mrac.a[1] = wrong_grub.h**2/wrong_grub.prs_to_torque[0,0]
    # mrac.a[2] = wrong_grub.r**2/wrong_grub.prs_to_torque[0,0]
    # mrac.a[3] = wrong_grub.K_passive_damper[0,0]/wrong_grub.prs_to_torque[0,0]
    # mrac.a[6] = wrong_grub.K_passive_damper[0,0]/wrong_grub.prs_to_torque[0,0]
    # mrac.a[7] = wrong_grub.K_passive_spring[0,0]/wrong_grub.prs_to_torque[0,0]
    # mrac.a[10] = wrong_grub.K_passive_spring[0,0]/wrong_grub.prs_to_torque[0,0]
    
    # MPC setup
    Q = 1.0*np.diag([0.,0.,1,1.0])
    Qf = 1.0*np.diag([0.,0.,1,1.0])
    R = .0000001*np.eye(4)*0
    horizon = 50
    num_params = 5
    xmin = -np.inf*np.ones(4)
    xmax = np.inf*np.ones(4)
    umin = 0*np.ones(4)
    umax = 400*np.ones(4)
    # umin = -1000.0*np.ones(4)
    # umax = 1000.0*np.ones(4)
    mpc = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax,num_params)

    dt = .01
    sim_length = int(6*60.0/dt)
    # Create a reference system to track
    m = 3.0
    k = 100.0
    b = np.sqrt(4*m*k)
    Aref = np.matrix([[-b/m, 0, -k/m, 0],
                      [0,-b/m, 0, -k/m,],
                      [1, 0, 0, 0],
                      [0, 1, 0, 0]])
    Bref = np.matrix([[k/m, 0],
                      [0, k/m],
                      [0, 0],
                      [0, 0]])
    Adref = scipy.linalg.expm(Aref*dt)
    Bdref = np.matmul(np.linalg.inv(Aref),np.matmul(Adref-np.eye(Adref.shape[0]),Bref))    

    
    # For plotting All controllers
    references_from_file = False        
    try:
        import scipy.io as sio
        data = sio.loadmat('RandomReferences.mat')
        r_hist = data['r_hist']
        references_from_file = True
    except:
        print("Making up my own references this time around!")
        r_hist = np.zeros([Bdref.shape[1],sim_length])
        
    xref_hist = np.zeros([4,sim_length])

    # For plotting MPC    
    x_mpc = np.ones([grub.numStates,1]).reshape(-1,1)*0.01    
    x_hist_mpc = np.zeros([grub.numStates,sim_length])
    u_hist_mpc = np.zeros([4,sim_length])


    # For plotting MPRAC    
    x_mprac = deepcopy(x_mpc)
    x_hist_mprac = np.zeros([grub.numStates,sim_length])
    u_hist_mprac = np.zeros([4,sim_length])
    a_hist_mprac = np.zeros([mracmpc.a.shape[0],sim_length])
    
    # For plotting MRAC
    x_mrac = deepcopy(x_mpc)
    x_hist_mrac = np.zeros([grub.numStates,sim_length])
    u_hist_mrac = np.zeros([4,sim_length])
    a_hist_mrac = np.zeros([mrac.a.shape[0],sim_length])    
    
    r = np.array([[1.0],
                  [-1.0]])*1.0


    d = 100
    xref = deepcopy(x_mpc)
    mpc_integrated_error = 0
    mprac_integrated_error = 0
    mrac_integrated_error = 0    

    for i in range(0,sim_length):

        if i%d==0:
            if references_from_file == True:
                r = r_hist[:,i].reshape(-1,1)
            else:
                r = np.random.rand(2,1)*np.pi/np.sqrt(2) - np.pi/2.0/np.sqrt(2)

            b = np.sqrt(4*m*k)
            Aref = np.matrix([[-b/m, 0, -k/m, 0],
                              [0,-b/m, 0, -k/m,],
                              [1, 0, 0, 0],
                              [0, 1, 0, 0]])
            Bref = np.matrix([[k/m, 0],
                              [0, k/m],
                              [0, 0],
                              [0, 0]])
            Adref = scipy.linalg.expm(Aref*dt)
            Bdref = np.matmul(np.linalg.inv(Aref),np.matmul(Adref-np.eye(Adref.shape[0]),Bref))

                
        xdotref = Aref.dot(xref) + Bref.dot(r)
        xref = Adref.dot(xref) + Bdref.dot(r)
        pmean = (umax[0]-umin[0])/2. + umin[0]

        # Forward simulate MPRAC
        err = np.zeros([4,1])
        
        err[0:2] = mracmpc.solve_for_next_u(x_mprac[2:4],x_mprac[0:2],xref[2:4],xref[0:2],xdotref[0:2])
        A,B,w = wrong_grub.calc_continuous_A_B_w(x_mprac)
        Ad,Bd = wrong_grub.discretize_A_and_B(A,B,dt)

        wd = np.linalg.inv(A).dot(Ad-np.eye(Ad.shape[0])).dot(w-err)
        # wd = np.linalg.inv(A).dot(Ad-np.eye(Ad.shape[0])).dot(w) - err*dt
        #wd = np.linalg.inv(A).dot(Ad-np.eye(Ad.shape[0])).dot(w) - err
        # print "err: ",err.T
        # print "wd: ",wd.T
        xgoal = np.zeros([4,horizon])

        xref_future = deepcopy(xref)
        xgoal[:,0] = deepcopy(xref_future.flatten())
        for j in range(1,horizon):
            xref_future = Adref.dot(xref_future) + Bdref.dot(r)
            xgoal[:,j] = xref_future.flatten()


        u_mprac = mpc.solve_for_next_u(Ad,Bd,wd,x_mprac,xgoal,ugoal=np.ones([4,1])*pmean).reshape(4,1)
        x_mprac = grub.forward_simulate_dt(x_mprac,u_mprac,dt)

        # Forward simulate MPC
        A,B,w = wrong_grub.calc_continuous_A_B_w(x_mpc)
        Ad,Bd = wrong_grub.discretize_A_and_B(A,B,dt)
        wd = np.linalg.inv(A).dot(Ad-np.eye(Ad.shape[0])).dot(w)
        #wd = w*dt
        u_mpc = mpc.solve_for_next_u(Ad,Bd,wd,x_mpc,xgoal,ugoal=np.ones([4,1])*pmean).reshape(4,1)
        x_mpc = grub.forward_simulate_dt(x_mpc,u_mpc,dt)
        
        # Forward simulate MRAC
        u_mrac = mrac.solve_for_next_u(x_mrac[2:4],x_mrac[0:2],xref[2:4],xref[0:2],xdotref[0:2])
        u_mrac = u_to_pressures(u_mrac,pmean,umin[0],umax[0])
        x_mrac = grub.forward_simulate_dt(x_mrac,u_mrac,dt)
        
        mpc_integrated_error = mpc_integrated_error + np.linalg.norm(xref[2:4] - x_mpc[2:4])
        mprac_integrated_error = mprac_integrated_error + np.linalg.norm(xref[2:4] - x_mprac[2:4])
        mrac_integrated_error = mrac_integrated_error + np.linalg.norm(xref[2:4] - x_mrac[2:4])
        
        # Record stuff
        r_hist[:,i] = r.flatten()
        xref_hist[:,i] = xref.flatten()        
        
        x_hist_mpc[:,i] = x_mpc.flatten()
        u_hist_mpc[:,i] = u_mpc.flatten()

        x_hist_mprac[:,i] = x_mprac.flatten()
        u_hist_mprac[:,i] = u_mprac.flatten()
        a_hist_mprac[:,i] = mracmpc.a.flatten()        
        
        x_hist_mrac[:,i] = x_mrac.flatten()
        u_hist_mrac[:,i] = u_mrac.flatten()
        a_hist_mrac[:,i] = mrac.a.flatten()


        #if i%500==0 or i==sim_length-1:
        if False:
            # print "mpc integrated error: ",mpc_integrated_error
            # print "mrac integrated error: ",mrac_integrated_error
            # print "mprac integrated error: ",mprac_integrated_error    
            
            t = np.linspace(i-d,(i-d)/100.0,i)

            linewidth = 1.0
            plt.figure(1)
            plt.clf()
            for j in range(0,2):
                plt.subplot(2,1,j+1)
                plt.plot(x_hist_mpc[2+j,max([0,i-d]):i].T,'orange',lw=linewidth)
                plt.plot(x_hist_mrac[2+j,max([0,i-d]):i].T,'g',lw=linewidth)
                plt.plot(x_hist_mprac[2+j,max([0,i-d]):i].T,'b',lw=linewidth)
                plt.plot(xref_hist[2+j,max([0,i-d]):i].T,'r--',lw=linewidth)
                plt.legend(['MPC','MRAC','MPRAC','Reference'])
                # plt.legend(['MRAC','MPRAC','Reference'])                
                plt.ylabel('Positions')
                plt.title('Positions')
            plt.xlabel('Time (s)')

            t = np.linspace(0,i/100.0,i)
            

            # plt.figure(2)
            # plt.clf()
            # for j in xrange(0,2):
            #     plt.subplot(2,1,j+1)
            #     plt.plot(t,x_hist_mpc[j,0:i].T,'orange',lw=linewidth)
            #     plt.plot(t,x_hist_mrac[j,0:i].T,'g',lw=linewidth)
            #     plt.plot(t,x_hist_mprac[j,0:i].T,'b',lw=linewidth)            
            #     plt.plot(t,xref_hist[j,0:i].T,'r--',lw=linewidth)
            #     plt.legend(['MPC','MRAC','MPRAC','Reference'])
            #     # plt.legend(['MRAC','MPRAC','Reference'])                                
            #     plt.ylabel('Velocities')
            #     plt.title('Velocities')
            # plt.xlabel('Time (s)')
                
            #plt.figure(3)
            #plt.clf()
            #for j in xrange(0,2):
            #    plt.subplot(2,1,j+1)
            #    plt.plot(t,u_hist_mpc[j*2:(j+1)*2,0:i].T,'orange',lw=linewidth)
            #    plt.plot(t,u_hist_mrac[j*2:(j+1)*2,0:i].T,'g',lw=linewidth)
            #    plt.plot(t,u_hist_mprac[j*2:(j+1)*2,0:i].T,'b',lw=linewidth)            
            #    # plt.plot(t,r_hist[j,0:i].T,'r--',lw=linewidth)
            #    # plt.legend(['u','ref'])
            #    plt.ylabel('Inputs')
            #    plt.title('Inputs')
            #plt.xlabel('Time (s)')                

            plt.figure(4)
            plt.clf()
            plt.subplot(1,2,1)
            plt.plot(t,a_hist_mprac[:,0:i].T,lw=linewidth)
            plt.title('MPRAC')
            plt.xlabel('Time (s)')            
            plt.subplot(1,2,2)
            plt.plot(t,a_hist_mrac[:,0:i].T,lw=linewidth)
            plt.title('MRAC')
            plt.xlabel('Time (s)')

            plt.figure(5)
            plt.clf()
            plt.plot(t,np.linalg.norm(xref_hist[2:4,0:i].T-x_hist_mpc[2:4,0:i].T,axis=1),'orange',lw=linewidth)
            plt.plot(t,np.linalg.norm(xref_hist[2:4,0:i].T-x_hist_mrac[2:4,0:i].T,axis=1),'g',lw=linewidth)
            plt.plot(t,np.linalg.norm(xref_hist[2:4,0:i].T-x_hist_mprac[2:4,0:i].T,axis=1),'b',lw=linewidth)
            plt.legend(['MPC','MRAC','MPRAC'])
            plt.legend(['MPC','MPRAC'])                
            plt.ylabel('Position Error')
            plt.title('Position Error')
            plt.xlabel('Time (s)')
            
            plt.pause(.0001)            
    
    import scipy.io as sio
    import time
    data_dict = {'xref_hist':xref_hist,
                 'x_hist_mpc':x_hist_mpc,
                 'x_hist_mrac':x_hist_mrac,
                 'x_hist_mprac':x_hist_mprac,
                 'r_hist':r_hist,
                 'u_hist_mpc':u_hist_mpc,
                 'u_hist_mrac':u_hist_mrac,
                 'u_hist_mprac':u_hist_mprac,
                 'a_hist_mrac':a_hist_mrac,
                 'a_hist_mprac':a_hist_mprac,
                 'mpc_integrated_error':mpc_integrated_error,
                 'mprac_integrated_error':mprac_integrated_error,
                 'mrac_integrated_error':mrac_integrated_error}

    print("mpc integrated error: ",mpc_integrated_error)
    print("mrac integrated error: ",mrac_integrated_error)
    print("mprac integrated error: ",mprac_integrated_error)    
                 
    sio.savemat('spring_offset_'+str(spring_offset)+'_model_error_'+str(modeling_error)+'_data_NEW.mat',data_dict)
    #plt.pause(50)


if __name__=='__main__':
    errors = [.5,.6,.7,.8,.9,1.0,1.1,1.2,1.3,1.4,1.5]
    #errors = [.5,.8,1.0,1.2,1.5]    
    offsets = [0.0,.05,.1,.15,.2,.25]
    #offsets = [0.0,.1,.25]    
    for error in errors:
        for offset in offsets: 
            print("Error: ",error,"  Offset: ",offset)
            run_perfect_regressor_experiment(error,offset)
