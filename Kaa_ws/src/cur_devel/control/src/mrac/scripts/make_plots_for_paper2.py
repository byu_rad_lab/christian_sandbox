import scipy.io as sio
import matplotlib.pyplot as plt
import numpy as np

def integrate_position_error(error):
    total_error = np.linalg.norm(error[2:4,0])
    for i in range(1,error.shape[1]):
        total_error = total_error + np.linalg.norm(error[2:4,i])
    return total_error*.01

xref_hist = sio.loadmat('spring_offset_0.0_model_error_1.0_data_NEW.mat')['xref_hist']
#xref_hist = sio.loadmat('updated_regressor_spring_offset_0_model_error_1.0_data.mat')['xref_hist']

#errors = [.5,.75,1.0,1.25,1.5]
#offsets = [0.0,0.1,.25,0.5,.75]

#errors = [.5,.6,.7,.8,.9,1.0,1.1,1.2,1.3,1.4,1.5]
errors = [1.0,1.1,1.2,1.3,1.4,1.5]
offsets = [0,.05,.1,.2,.3,.4,.5]

errors = [.5,.8,1.0,1.2,1.5]    
offsets = [0.0,.1,.25]

errors = [.5,.6,.7,.8,.9,1.0,1.1,1.2,1.3,1.4,1.5]
offsets = [0.0,.05,.1,.15,.2,.25]



mpc_integrated_errors = {}
mrac_integrated_errors = {}
mrpac_integrated_errors = {}
begin = 6000*5
end = 6000*6
for error in errors:
    for offset in offsets:
        data = sio.loadmat('spring_offset_'+str(offset)+'_model_error_'+str(error)+'_data_NEW.mat')
        #data = sio.loadmat('updated_regressor_spring_offset_'+str(offset)+'_model_error_'+str(error)+'_data.mat')
        x_hist_mpc = data['x_hist_mpc']
        x_hist_mrac = data['x_hist_mrac']
        x_hist_mrpac = data['x_hist_mprac']
            
        mpc_error = integrate_position_error(xref_hist[:,begin:end]-x_hist_mpc[:,begin:end])
        mrac_error = integrate_position_error(xref_hist[:,begin:end]-x_hist_mrac[:,begin:end])
        mrpac_error = integrate_position_error(xref_hist[:,begin:end]-x_hist_mrpac[:,begin:end])

        mpc_integrated_errors['offset_'+str(offset)+'_error_'+str(error)] = mpc_error
        mrac_integrated_errors['offset_'+str(offset)+'_error_'+str(error)] = mrac_error
        mrpac_integrated_errors['offset_'+str(offset)+'_error_'+str(error)] = mrpac_error


            
for error in errors:
    mpc_errors = []
    mrac_errors = []
    mrpac_errors = []    
    for offset in offsets:
        mpc_errors.append(mpc_integrated_errors['offset_'+str(offset)+'_error_'+str(error)])
        mrac_errors.append(mrac_integrated_errors['offset_'+str(offset)+'_error_'+str(error)])
        mrpac_errors.append(mrpac_integrated_errors['offset_'+str(offset)+'_error_'+str(error)])

    if error == 1.0:            # added to generate just the figures needed for the paper
        
        plt.ion()
        plt.figure()
        plt.title('Error '+str(error))
        plt.plot(offsets,mpc_errors)
        plt.plot(offsets,mrac_errors)
        plt.plot(offsets,mrpac_errors)    
        leg = plt.legend(['MPC','MRAC','MRPAC'])
        leg.draggable()
        plt.xlabel('Spring offset')
        plt.ylabel('Integrated error over 60 s (rad)')
        plt.show()



for offset in offsets:
    mpc_errors = []
    mrac_errors = []
    mrpac_errors = []    
    for error in errors:
        mpc_errors.append(mpc_integrated_errors['offset_'+str(offset)+'_error_'+str(error)])
        mrac_errors.append(mrac_integrated_errors['offset_'+str(offset)+'_error_'+str(error)])
        mrpac_errors.append(mrpac_integrated_errors['offset_'+str(offset)+'_error_'+str(error)])

    if offset == 0.0: #added to generate just the figures needed for the paper.
        
        plt.ion()
        plt.figure()
        plt.title('Offset '+str(offset))
        plt.plot(errors,mpc_errors)
        plt.plot(errors,mrac_errors)
        plt.plot(errors,mrpac_errors)    
        leg = plt.legend(['MPC','MRAC','MRPAC'])
        leg.draggable()
        plt.xlabel('Model Error')
        plt.ylabel('Integrated error over 60 s (rad)')
        plt.show()

    

from mpl_toolkits.mplot3d import Axes3D
X,Y = np.meshgrid(errors,offsets)
Z_mpc = np.zeros([len(offsets),len(errors)])
Z_mrac = np.zeros([len(offsets),len(errors)])
Z_mrpac = np.zeros([len(offsets),len(errors)])
for i in range(0,len(offsets)):
    for j in range(0,len(errors)):
        error = errors[j]
        offset = offsets[i]
        Z_mpc[i,j] = mpc_integrated_errors['offset_'+str(offset)+'_error_'+str(error)]
        Z_mrac[i,j] = mrac_integrated_errors['offset_'+str(offset)+'_error_'+str(error)]
        Z_mrpac[i,j] = mrpac_integrated_errors['offset_'+str(offset)+'_error_'+str(error)]


fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(X,Y,Z_mrpac,color='#1f77b4')
surf._facecolors2d=surf._facecolors3d #these are needed as a workaround for a bug in matplotlib code, see https://stackoverflow.com/questions/54994600/pyplot-legend-poly3dcollection-object-has-no-attribute-edgecolors2d
surf._edgecolors2d=surf._edgecolors3d
ax = fig.gca(projection='3d')
surf = ax.plot_surface(X,Y,Z_mpc, color='#ff7f0e')
surf._facecolors2d=surf._facecolors3d
surf._edgecolors2d=surf._edgecolors3d
ax = fig.gca(projection='3d')
surf = ax.plot_surface(X,Y,Z_mrac, color='#2ca02c')
surf._facecolors2d=surf._facecolors3d
surf._edgecolors2d=surf._edgecolors3d


plt.xlabel('Model Error (known unknown)')
plt.ylabel('Spring Offset (unknown unknown)')
leg = plt.legend(['MRPAC','MPC','MRAC'])
leg.draggable()
            


plt.show()


linewidth = 1.0
t = np.linspace(0,(end-begin)/100,end-begin)
for error in errors:
    for offset in offsets:
        data = sio.loadmat('spring_offset_'+str(offset)+'_model_error_'+str(error)+'_data_NEW.mat')
        if offset==0.25 and error==1.0 or offset==0.0 and error==1.5:
            plt.figure()
        
            for j in range(0,2):
                plt.subplot(2,1,j+1)
                plt.plot(t,data['x_hist_mpc'][2+j,begin:end].T,'orange',lw=linewidth)
                plt.plot(t,data['x_hist_mrac'][2+j,begin:end].T,'g',lw=linewidth)
                plt.plot(t,data['x_hist_mprac'][2+j,begin:end].T,'b',lw=linewidth)
                plt.plot(t,data['xref_hist'][2+j,begin:end].T,'r--',lw=linewidth)
                leg = plt.legend(['MPC','MRAC','MRPAC','Reference'])
                leg.draggable()
                if(j==0):
                    plt.title('spring offset '+str(offset)+' model error '+str(error))
                    plt.ylabel('u (rad)')
                if(j==1):
                    plt.ylabel('v (rad)')
                    plt.xlabel('Time (s)')

plt.pause(10000)
