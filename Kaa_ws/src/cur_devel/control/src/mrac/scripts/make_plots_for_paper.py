import scipy.io as sio
import matplotlib.pyplot as plt
import numpy as np

def integrate_abs_error(error):
    total_error = np.linalg.norm(error[2:4,0])
    integrated_abs_error = []
    integrated_abs_error.append(np.linalg.norm(error[2:4,0]))
    for i in range(1,error.shape[1]):
        integrated_abs_error.append(integrated_abs_error[i-1] + np.linalg.norm(error[2:4,i]))
        total_error = total_error + np.linalg.norm(error[2:4,i])

    return integrated_abs_error,total_error
                
    

# Examine the effect of model error given a perfect regressor
error_05_data = sio.loadmat('perfect4_regressor_error_0.5_data.mat')
error_075_data = sio.loadmat('perfect4_regressor_error_0.75_data.mat')
error_09_data = sio.loadmat('perfect4_regressor_error_0.9_data.mat')
error_1_data = sio.loadmat('perfect4_regressor_error_1.0_data.mat')
error_11_data = sio.loadmat('perfect4_regressor_error_1.1_data.mat')
error_125_data = sio.loadmat('perfect4_regressor_error_1.25_data.mat')
error_15_data = sio.loadmat('perfect4_regressor_error_1.5_data.mat')

error_05_data = sio.loadmat('imperfect4_regressor_error_0.5_data.mat')
error_075_data = sio.loadmat('imperfect4_regressor_error_0.75_data.mat')
error_09_data = sio.loadmat('imperfect4_regressor_error_0.9_data.mat')
error_1_data = sio.loadmat('imperfect4_regressor_error_1.0_data.mat')
error_11_data = sio.loadmat('imperfect4_regressor_error_1.1_data.mat')
error_125_data = sio.loadmat('imperfect4_regressor_error_1.25_data.mat')
error_15_data = sio.loadmat('imperfect4_regressor_error_1.5_data.mat')

# error_05_data = sio.loadmat('imperfect_complex_regressor_error_0.5_data.mat')
# error_075_data = sio.loadmat('imperfect_complex_regressor_error_0.75_data.mat')
# error_09_data = sio.loadmat('imperfect_complex_regressor_error_0.9_data.mat')
# error_1_data = sio.loadmat('imperfect_complex_regressor_error_1.0_data.mat')
# error_11_data = sio.loadmat('imperfect_complex_regressor_error_1.1_data.mat')
# error_125_data = sio.loadmat('imperfect_complex_regressor_error_1.25_data.mat')
# error_15_data = sio.loadmat('imperfect_complex_regressor_error_1.5_data.mat')

# error_05_data = sio.loadmat('short_h_discrete_w_imperfect_regressor_error_0.5_data.mat')
# error_075_data = sio.loadmat('short_h_discrete_w_imperfect_regressor_error_0.75_data.mat')
# error_09_data = sio.loadmat('short_h_discrete_w_imperfect_regressor_error_0.9_data.mat')
# error_1_data = sio.loadmat('short_h_discrete_w_imperfect_regressor_error_1.0_data.mat')
# error_11_data = sio.loadmat('short_h_discrete_w_imperfect_regressor_error_1.1_data.mat')
# error_125_data = sio.loadmat('short_h_discrete_w_imperfect_regressor_error_1.25_data.mat')
# error_15_data = sio.loadmat('short_h_discrete_w_imperfect_regressor_error_1.5_data.mat')


errors = [0.5,0.75,0.9,1.0,1.1,1.25,1.5]

begin = 6000*5
end = 6000*6
mpc_error_05,mpc_total_error_05 = integrate_abs_error(error_05_data['xref_hist'][:,begin:end]-error_05_data['x_hist_mpc'][:,begin:end])
mpc_error_075,mpc_total_error_075 = integrate_abs_error(error_075_data['xref_hist'][:,begin:end]-error_075_data['x_hist_mpc'][:,begin:end])
mpc_error_09,mpc_total_error_09 = integrate_abs_error(error_09_data['xref_hist'][:,begin:end]-error_09_data['x_hist_mpc'][:,begin:end])
mpc_error_1,mpc_total_error_1 = integrate_abs_error(error_1_data['xref_hist'][:,begin:end]-error_1_data['x_hist_mpc'][:,begin:end])
mpc_error_11,mpc_total_error_11 = integrate_abs_error(error_11_data['xref_hist'][:,begin:end]-error_11_data['x_hist_mpc'][:,begin:end])
mpc_error_125,mpc_total_error_125 = integrate_abs_error(error_125_data['xref_hist'][:,begin:end]-error_125_data['x_hist_mpc'][:,begin:end])
mpc_error_15,mpc_total_error_15 = integrate_abs_error(error_15_data['xref_hist'][:,begin:end]-error_15_data['x_hist_mpc'][:,begin:end])

mrac_error_05,mrac_total_error_05 = integrate_abs_error(error_05_data['xref_hist'][:,begin:end]-error_05_data['x_hist_mrac'][:,begin:end])
mrac_error_075,mrac_total_error_075 = integrate_abs_error(error_075_data['xref_hist'][:,begin:end]-error_075_data['x_hist_mrac'][:,begin:end])
mrac_error_09,mrac_total_error_09 = integrate_abs_error(error_09_data['xref_hist'][:,begin:end]-error_09_data['x_hist_mrac'][:,begin:end])
mrac_error_1,mrac_total_error_1 = integrate_abs_error(error_1_data['xref_hist'][:,begin:end]-error_1_data['x_hist_mrac'][:,begin:end])
mrac_error_11,mrac_total_error_11 = integrate_abs_error(error_11_data['xref_hist'][:,begin:end]-error_11_data['x_hist_mrac'][:,begin:end])
mrac_error_125,mrac_total_error_125 = integrate_abs_error(error_125_data['xref_hist'][:,begin:end]-error_125_data['x_hist_mrac'][:,begin:end])
mrac_error_15,mrac_total_error_15 = integrate_abs_error(error_15_data['xref_hist'][:,begin:end]-error_15_data['x_hist_mrac'][:,begin:end])

mprac_error_05,mprac_total_error_05 = integrate_abs_error(error_05_data['xref_hist'][:,begin:end]-error_05_data['x_hist_mprac'][:,begin:end])
mprac_error_075,mprac_total_error_075 = integrate_abs_error(error_075_data['xref_hist'][:,begin:end]-error_075_data['x_hist_mprac'][:,begin:end])
mprac_error_09,mprac_total_error_09 = integrate_abs_error(error_09_data['xref_hist'][:,begin:end]-error_09_data['x_hist_mprac'][:,begin:end])
mprac_error_1,mprac_total_error_1 = integrate_abs_error(error_1_data['xref_hist'][:,begin:end]-error_1_data['x_hist_mprac'][:,begin:end])
mprac_error_11,mprac_total_error_11 = integrate_abs_error(error_11_data['xref_hist'][:,begin:end]-error_11_data['x_hist_mprac'][:,begin:end])
mprac_error_125,mprac_total_error_125 = integrate_abs_error(error_125_data['xref_hist'][:,begin:end]-error_125_data['x_hist_mprac'][:,begin:end])
mprac_error_15,mprac_total_error_15 = integrate_abs_error(error_15_data['xref_hist'][:,begin:end]-error_15_data['x_hist_mprac'][:,begin:end])

mpc_integrated_errors = [mpc_total_error_05,
                         mpc_total_error_075,
                         mpc_total_error_09,
                         mpc_total_error_1,
                         mpc_total_error_11,
                         mpc_total_error_125,
                         mpc_total_error_15]
mrac_integrated_errors = [mrac_total_error_05,
                         mrac_total_error_075,
                         mrac_total_error_09,
                         mrac_total_error_1,
                         mrac_total_error_11,
                         mrac_total_error_125,
                         mrac_total_error_15]
mprac_integrated_errors = [mprac_total_error_05,
                          mprac_total_error_075,
                          mprac_total_error_09,
                          mprac_total_error_1,
                          mprac_total_error_11,
                          mprac_total_error_125,
                          mprac_total_error_15]

plt.figure(1)
plt.ion()
plt.plot(errors,np.array(mpc_integrated_errors)*.01)
plt.plot(errors,np.array(mrac_integrated_errors)*.01)
plt.plot(errors,np.array(mprac_integrated_errors)*.01)
plt.legend(['MPC','MRAC','MPRAC'])
plt.title('Sensitivity to Model Parameter Error')
plt.xlabel('Error Multiplier')
plt.ylabel('Integrated error over 60 s (rad)')
plt.show()

datasets = [error_05_data,
            error_075_data,
            error_09_data,
            error_1_data,
            error_11_data,
            error_125_data,
            error_15_data]

linewidth = 1.0
t = np.linspace(0,(end-begin)/100,end-begin)
for dataset in datasets:
    plt.figure()
    for j in range(0,2):
        plt.subplot(2,1,j+1)
        plt.plot(t,dataset['x_hist_mpc'][2+j,begin:end].T,'orange',lw=linewidth)
        plt.plot(t,dataset['x_hist_mrac'][2+j,begin:end].T,'g',lw=linewidth)
        plt.plot(t,dataset['x_hist_mprac'][2+j,begin:end].T,'b',lw=linewidth)
        plt.plot(t,dataset['xref_hist'][2+j,begin:end].T,'r--',lw=linewidth)
        plt.legend(['MPC','MRAC','MPRAC','Reference'])
        plt.ylabel('Positions')
        plt.title('Positions')
    plt.xlabel('Time (s)')
            
plt.show()
plt.pause(1000)
