#!/usr/bin/env python
import rosbag
from std_msgs.msg import Float32, Float32MultiArray
import scipy.io as sio
import numpy as np

if __name__ == '__main__':
    controller = 'MRAC'
    bagpath = controller+'.bag'

    topic_list = ['/grubState','/ref_trajectory']

    bag = rosbag.Bag(bagpath)
    x_list = []
    xref_list = []    
    t_list = []
    tref_list = []
    started = False

    for topic_name, msg, t in bag.read_messages(topics=topic_list):
        t = t.to_sec()
        if topic_name == '/grubState':
            if started == True:
                t_list.append(t)
                x_list.append(np.array(msg.data).reshape(4,1))
        if topic_name == '/ref_trajectory':
            started = True
            tref_list.append(t)
            xref_list.append(np.array(msg.data).reshape(4,1))
            
    bag.close()
    print("Finished processing bag")

    x = np.squeeze(np.array(x_list))
    xref = np.squeeze(np.array(xref_list))

    data_dict = {'x':x,'xref':xref,'t':t_list,'tref':tref_list}

    sio.savemat(controller+'_data.mat',data_dict)

    
