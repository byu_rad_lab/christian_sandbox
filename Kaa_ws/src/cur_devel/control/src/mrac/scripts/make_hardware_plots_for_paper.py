import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio

def integrate_position_error(t,error):
    print(error.shape)
    total_error = 0
    for i in range(1,t.shape[0]):
        total_error = total_error + np.linalg.norm(error[i,2:4])*(t[i]-t[i-1])
    return total_error

x_mpc = sio.loadmat('MPC_data_NEW.mat')['x']
t_mpc = sio.loadmat('MPC_data_NEW.mat')['t'].flatten()
t_mpc = t_mpc - t_mpc[0]

x_ref = sio.loadmat('MRPAC_data_NEW.mat')['xref']
t_ref = sio.loadmat('MRPAC_data_NEW.mat')['tref'].flatten()
t_ref = t_ref - t_ref[0]

x_mrpac = sio.loadmat('MRPAC_data_NEW.mat')['x']
t_mrpac = sio.loadmat('MRPAC_data_NEW.mat')['t'].flatten()
t_mrpac = t_mrpac - t_mrpac[0]

x_mrac = sio.loadmat('MRAC_data_NEW.mat')['x']
t_mrac = sio.loadmat('MRAC_data_NEW.mat')['t'].flatten()
t_mrac = t_mrac - t_mrac[0]

begin = 905-150
end = 905

ref_begin_idx = np.argmin(np.abs(t_ref-begin))
ref_end_idx = np.argmin(np.abs(t_ref-end))
mpc_begin_idx = np.argmin(np.abs(t_mpc-begin))
mpc_end_idx = np.argmin(np.abs(t_mpc-end))
mrac_begin_idx = np.argmin(np.abs(t_mrac-begin))
mrac_end_idx = np.argmin(np.abs(t_mrac-end))
mrpac_begin_idx = np.argmin(np.abs(t_mrpac-begin))
mrpac_end_idx = np.argmin(np.abs(t_mrpac-end))

mpc_error = np.zeros([mpc_end_idx-mpc_begin_idx,4])
mrac_error = np.zeros([mrac_end_idx-mrac_begin_idx,4])
mrpac_error = np.zeros([mrpac_end_idx-mrpac_begin_idx,4])
for i in range(0,4):
    mpc_error[:,i] = np.interp(t_mpc[mpc_begin_idx:mpc_end_idx],t_ref,x_ref[:,i])-x_mpc[mpc_begin_idx:mpc_end_idx,i]
    mrac_error[:,i] = np.interp(t_mrac[mrac_begin_idx:mrac_end_idx],t_ref,x_ref[:,i])-x_mrac[mrac_begin_idx:mrac_end_idx,i]
    mrpac_error[:,i] = np.interp(t_mrpac[mrpac_begin_idx:mrpac_end_idx],t_ref,x_ref[:,i])-x_mrpac[mrpac_begin_idx:mrpac_end_idx,i]    

integrated_mpc_error = integrate_position_error(t_mpc[mpc_begin_idx:mpc_end_idx],mpc_error)
integrated_mrac_error = integrate_position_error(t_mrac[mrac_begin_idx:mrac_end_idx],mrac_error)
integrated_mrpac_error = integrate_position_error(t_mrpac[mrpac_begin_idx:mrpac_end_idx],mrpac_error)


print("\n --- Statistical Position Error Report for last 150s--- \n")
print("integrated mpc error: ",integrated_mpc_error)
print("integrated mrac error: ",integrated_mrac_error)
print("integrated mrpac error: ",integrated_mrpac_error)

print("mean mpc error: ",np.mean(mpc_error[:,2:4]))
print("mean mrac error: ",np.mean(mrac_error[:,2:4]))
print("mean mrpac error: ",np.mean(mrpac_error[:,2:4]))

print("median mpc error: ",np.median(mpc_error[:,2:4]))
print("median mrac error: ",np.median(mrac_error[:,2:4]))
print("median mrpac error: ",np.median(mrpac_error[:,2:4]))

print("stddev mpc error: ",np.std(mpc_error[:,2:4]))
print("stddev mrac error: ",np.std(mrac_error[:,2:4]))
print("stddev mrpac error: ",np.std(mrpac_error[:,2:4]))

linewidth=1
plt.figure()
for j in range(0,2):
    
    plt.subplot(2,1,j+1)
    plt.plot(t_mpc,x_mpc[:,2+j],'orange',lw=linewidth)
    plt.plot(t_mrac,x_mrac[:,2+j],'g',lw=linewidth)    
    plt.plot(t_mrpac,x_mrpac[:,2+j],'b',lw=linewidth)
    plt.plot(t_ref,x_ref[:,2+j],'r--',lw=linewidth)
    plt.legend(['MPC','MRAC','MPRAC','Reference'])    
    plt.ylabel('Positions')
    plt.xlabel('Time (s)')

plt.pause(10000)
