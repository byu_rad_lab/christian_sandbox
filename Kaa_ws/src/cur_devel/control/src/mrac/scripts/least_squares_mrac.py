from rad_models.BellowsGrubIntegral import BellowsGrub
import scipy
from copy import deepcopy
import matplotlib.pyplot as plt
from mrac.ManipulatorMRAC import ManipulatorMRAC
import numpy as np


def u_to_pressures(u,pmean,umin,umax):
    pressures = np.matrix([[pmean+u[0,0]/2.],
                           [pmean-u[0,0]/2.],
                           [pmean+u[1,0]/2.],
                           [pmean-u[1,0]/2.]])
    
    pressures[pressures<umin] = umin
    pressures[pressures>umax] = umax
    
    return pressures


def my_regressor(q,qd,qd_r,qdd_r):
    q = np.array(q)
    qd = np.array(qd)
    qd_r = np.array(qd_r)
    qdd_r = np.array(qdd_r)        
    
    reg = mrac_grub.calc_grub_regressor(q,qd,qd_r,qdd_r)

    # reg = np.hstack([reg,np.eye(2)*q[0],np.eye(2)*q[1]])
    # reg = np.hstack([reg,np.eye(2)*qd[0],np.eye(2)*qd[1]])    
    # reg = np.hstack([reg,q])
    # reg = np.hstack([reg,qd])

    reg = np.hstack([reg,q*np.cos(qd)**2])
    reg = np.hstack([reg,qd*np.cos(q)**2])
    reg = np.hstack([reg,q*np.sin(qd)**2])    
    reg = np.hstack([reg,qd*np.sin(q)**2])    
    
    return reg
    
mrac_grub = BellowsGrub()

Lambda = 10.0 #This is kinda like Jon's alpha...kinda...but not entirely
Gamma = 0.0
KD = 10.0

mrac = ManipulatorMRAC(my_regressor,2,Lambda,Gamma,KD)
# mrac.a[0] = -mrac_grub.m/mrac_grub.prs_to_torque[0,0]
# mrac.a[1] = mrac_grub.h**2/mrac_grub.prs_to_torque[0,0]
# mrac.a[2] = mrac_grub.r**2/mrac_grub.prs_to_torque[0,0]
# mrac.a[3] = mrac_grub.K_passive_spring[0,0]/mrac_grub.prs_to_torque[0,0]
# mrac.a[6] = mrac_grub.K_passive_spring[0,0]/mrac_grub.prs_to_torque[0,0]
# mrac.a[7] = mrac_grub.K_passive_damper[0,0]/mrac_grub.prs_to_torque[0,0]
# mrac.a[10] = mrac_grub.K_passive_damper[0,0]/mrac_grub.prs_to_torque[0,0]
# mrac.a[0] = -mrac_grub.m/mrac_grub.prs_to_torque[0,0]
# mrac.a[1] = mrac_grub.h**2/mrac_grub.prs_to_torque[0,0]
# mrac.a[2] = mrac_grub.r**2/mrac_grub.prs_to_torque[0,0]
# mrac.a[3] = mrac_grub.K_passive_spring[0,0]/mrac_grub.prs_to_torque[0,0]
# mrac.a[4] = mrac_grub.K_passive_damper[0,0]/mrac_grub.prs_to_torque[0,0]

sim_length = 10000

# Create a reference system to track
dt = .01
m = 1.0
k = 50.0
b = np.sqrt(4*m*k)
Aref = np.matrix([[-b/m, 0, -k/m, 0],
                  [0,-b/m, 0, -k/m,],
                  [1, 0, 0, 0],
                  [0, 1, 0, 0]])
Bref = np.matrix([[k/m, 0],
                  [0, k/m],
                  [0, 0],
                  [0, 0]])
Adref = scipy.linalg.expm(Aref*dt)
Bdref = np.matmul(np.linalg.inv(Aref),np.matmul(Adref-np.eye(Adref.shape[0]),Bref))    

# For plotting
r_hist = np.zeros([Bdref.shape[1],sim_length])
xref_hist = np.zeros([4,sim_length])
x_mrac = np.ones([8,1]).reshape(-1,1)*0.01

x_hist_mrac = np.zeros([mrac_grub.numStates,sim_length])
xdot_hist_mrac = np.zeros([mrac_grub.numStates,sim_length])    
u_hist_mrac = np.zeros([4,sim_length])
tau_hist_mrac = np.zeros([2,sim_length])    
Y_hist_mrac = np.zeros([mrac.Y.shape[0]*sim_length,mrac.Y.shape[1]])
a_hist_mrac = np.zeros([mrac.a.shape[0],sim_length])        

d = 100
xref = deepcopy(x_mrac[4:8])

for i in range(0,sim_length):

    if i%d==0:
        r = np.random.randn(2,1)*0.5
        
    xdotref = Aref.dot(xref) + Bref.dot(r)
    xref = Adref.dot(xref) + Bdref.dot(r)
    
    # Forward simulate MRAC
    pmean = 250
    tau_mrac = mrac.solve_for_next_u(x_mrac[6:8],x_mrac[4:6],xref[2:4],xref[0:2],xdotref[0:2],adapt=False)
    u_mrac = u_to_pressures(tau_mrac,pmean,0,500)
    
    x_mrac = mrac_grub.forward_simulate_dt(x_mrac,u_mrac,dt)
    
    xdot_mrac = (x_mrac.flatten() - x_hist_mrac[:,i-1]).flatten()/dt
    
    # Record stuff
    r_hist[:,i] = r.flatten()
    xref_hist[:,i] = xref.flatten()
    
    x_hist_mrac[:,i] = x_mrac.flatten()
    xdot_hist_mrac[:,i] = xdot_mrac.flatten()        
    u_hist_mrac[:,i] = u_mrac.flatten()
    tau_hist_mrac[:,i] = tau_mrac.flatten()        
    Y_hist_mrac[2*i:2*i+2,:] = my_regressor(x_mrac[6:8],x_mrac[4:6],x_mrac[4:6],xdot_mrac[4:6])
    a_hist_mrac[:,i] = mrac.a.flatten()

    # Use Least Squares to adapt parameters instead of MRAC's adaptive law
    if(i>=500 and i%500==0):
        taus = tau_hist_mrac[:,1:i].reshape(-1,1,order='F')
        regressors = Y_hist_mrac[2:2*i,:].reshape(-1,mrac.a.shape[0])
        mrac.a = np.linalg.pinv(regressors).dot(taus)

    if i%1000==0 and i>0:
        print("Changing Reference system!")
        print("MRAC a: ",mrac.a.T)
        m = m*.95
        k = k
        b = np.sqrt(4*m*k)
        Aref = np.matrix([[-b/m, 0, -k/m, 0],
                          [0,-b/m, 0, -k/m,],
                          [1, 0, 0, 0],
                          [0, 1, 0, 0]])
        Bref = np.matrix([[k/m, 0],
                          [0, k/m],
                          [0, 0],
                          [0, 0]])
        Adref = scipy.linalg.expm(Aref*dt)
        Bdref = np.matmul(np.linalg.inv(Aref),np.matmul(Adref-np.eye(Adref.shape[0]),Bref))    
        
    if i%100==0:
        
        plt.figure(1)
        plt.clf()
        for j in range(0,2):
            plt.subplot(2,1,j+1)
            plt.plot(x_hist_mrac[6+j,0:i].T,'g')
            plt.plot(xref_hist[2+j,0:i].T,'r--')
            plt.legend(['MRAC','Reference'])
            plt.ylabel('Positions')
            plt.title('Positions')
            
        plt.figure(2)
        plt.clf()
        for j in range(0,2):
            plt.subplot(2,1,j+1)
            plt.plot(x_hist_mrac[4+j,0:i].T,'g')                
            plt.plot(xref_hist[j,0:i].T,'r--')
            plt.legend(['MRAC','Reference'])                
            plt.ylabel('Velocities')
            plt.title('Velocities')
            
        plt.figure(3)
        plt.clf()
        for j in range(0,2):
            plt.subplot(2,1,j+1)
            plt.plot(u_hist_mrac[j*2:(j+1)*2,0:i].T,'g')                
            plt.plot(r_hist[j,0:i].T,'r--')
            plt.ylabel('Inputs')
            plt.title('Inputs')
            
        plt.figure(4)
        plt.clf()
        plt.subplot(1,2,2)
        plt.plot(a_hist_mrac[:,0:i].T)
        plt.title('MRAC')            
        plt.pause(.0001)
        
        
plt.pause(100000)
