import matplotlib.pyplot as plt
import scipy.io as sio
import numpy as np


# REFERENCE #######################
x_ref = sio.loadmat('MRPAC_data_NEW.mat')['xref']
t_ref = sio.loadmat('MRPAC_data_NEW.mat')['tref'].flatten()
t_ref = t_ref - t_ref[0]

#last minute
last_t_ref = t_ref[-1]
t_last_index_ref = np.where(t_ref > last_t_ref - 150)[0][0] #very first index that is in the last minute of data
t_last_ref = t_ref[t_last_index_ref:]
x_last_ref = x_ref[t_last_index_ref:]

#set x axis values back to 0
t_last_ref = t_last_ref - t_last_ref[0]

#MPC ##############################
x_mpc = sio.loadmat('MPC_data_NEW.mat')['x']
t_mpc = sio.loadmat('MPC_data_NEW.mat')['t'].flatten()
t_mpc = t_mpc - t_mpc[0]

#last minute
last_t_mpc = t_mpc[-1]
#set x axis value back to 0
t_last_index_mpc = np.where(t_mpc > last_t_ref - 150)[0][0] #very first index that is in the last minute of data
t_end_index = np.where(t_mpc > last_t_ref)[0][0]
t_last_mpc = t_mpc[t_last_index_mpc:t_end_index]
x_last_mpc = x_mpc[t_last_index_mpc:t_end_index]

#set x axis values back to 0
t_last_mpc = t_last_mpc - t_last_mpc[0]

# MRPAC #####################################
x_mrpac = sio.loadmat('MRPAC_data_NEW.mat')['x']
t_mrpac = sio.loadmat('MRPAC_data_NEW.mat')['t'].flatten()
t_mrpac = t_mrpac - t_mrpac[0]

#last minute
last_t_mrpac = t_mrpac[-1]
t_last_index_mrpac = np.where(t_mrpac > last_t_ref - 150)[0][0] #very first index that is in the last minute of data
t_end_index = np.where(t_mrpac > last_t_ref)[0][0]
t_last_mrpac = t_mrpac[t_last_index_mrpac:t_end_index]
x_last_mrpac = x_mrpac[t_last_index_mrpac:t_end_index]

#set x axis values back to 0
t_last_mrpac = t_last_mrpac - t_last_mrpac[0]

# MRAC #########################################
x_mrac = sio.loadmat('MRAC_data_NEW.mat')['x']
t_mrac = sio.loadmat('MRAC_data_NEW.mat')['t'].flatten()
t_mrac = t_mrac - t_mrac[0]

#last minutep
last_t_mrac = t_mrac[-1]
t_last_index_mrac = np.where(t_mrac > last_t_ref - 150)[0][0] #very first index that is in the last minute of data
t_end_index = np.where(t_mrac > last_t_ref)[0][0]
t_last_mrac = t_mrac[t_last_index_mrac:t_end_index]
x_last_mrac = x_mrac[t_last_index_mrac:t_end_index]

#set x axis values back to 0
t_last_mrac = t_last_mrac - t_last_mrac[0]


linewidth=1
#fig = plt.figure()
fig= plt.figure(figsize=(12,9))

j = 0
plt.subplot(2,1,1)
plt.plot(t_last_mpc,x_last_mpc[:,2+j],'orange',lw=linewidth)
plt.plot(t_last_mrac,x_last_mrac[:,2+j],'g',lw=linewidth)    
plt.plot(t_last_mrpac,x_last_mrpac[:,2+j],'b',lw=linewidth)
plt.plot(t_last_ref,x_last_ref[:,2+j],'r--',lw=linewidth)
plt.legend(['MPC','MRAC','MRPAC','Reference'], prop={'size':13}).set_draggable(True)
plt.ylabel('u (rad)')
plt.xlabel('Time (s)')

j = 1
plt.subplot(2,1,2)
plt.plot(t_last_mpc,x_last_mpc[:,2+j],'orange',lw=linewidth)
plt.plot(t_last_mrac,x_last_mrac[:,2+j],'g',lw=linewidth)    
plt.plot(t_last_mrpac,x_last_mrpac[:,2+j],'b',lw=linewidth)
plt.plot(t_last_ref,x_last_ref[:,2+j],'r--',lw=linewidth)
plt.legend(['MPC','MRAC','MRPAC','Reference'], prop={'size':13}).set_draggable(True)
plt.ylabel('v (rad)')
plt.xlabel('Time (s)')
   
plt.pause(10000)
