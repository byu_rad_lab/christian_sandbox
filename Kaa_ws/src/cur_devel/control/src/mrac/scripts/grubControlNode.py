#!/usr/bin/env python

from rad_models.BellowsGrubIntegralNoPressures import BellowsGrub #make sure c code in rad models is built, this won't work otherwise.
from mrac.ManipulatorMRAC import ManipulatorMRAC
# from osqp_mpc.ParameterizedSmallMatrixOSQPMPC import OSQPMPC # Currently Broken
# from osqp_mpc.ParameterizedLargeMatrixOSQPMPC import OSQPMPC
from osqp_mpc.SmallMatrixOSQPMPC import OSQPMPC
import numpy as np
import scipy
from copy import deepcopy
import scipy.io as sio
import time
import argparse
import rospy
from std_msgs.msg import Float32, Float32MultiArray

import falkor.srv as stmp #this is defined in the ego-pneubo repo in the falkor ws
import falkor.msg as msgs

def u_to_pressures(u,pmean,umin,umax):
    pressures = np.matrix([[pmean+u[0,0]/2.],
                           [pmean-u[0,0]/2.],
                           [pmean+u[1,0]/2.],
                           [pmean-u[1,0]/2.]])
    
    pressures[pressures<umin] = umin
    pressures[pressures>umax] = umax
    
    return pressures

def my_regressor(q,qd,qd_r,qdd_r):
    q = np.array(q)
    qd = np.array(qd)
    qd_r = np.array(qd_r)
    qdd_r = np.array(qdd_r)
      
    reg = grub.calc_grub_regressor(q,qd,qd_r,qdd_r)*0.01
    reg = np.hstack([reg,np.eye(2)*qd_r[0],np.eye(2)*qd_r[1]])*.01
    # reg = np.hstack([reg,np.eye(2)*qd[0],np.eye(2)*qd[1]])*.01
    reg = np.hstack([reg,np.eye(2)*q[0],np.eye(2)*q[1]])
    reg = np.hstack([reg,np.eye(2)])*1.0


        
    return reg

def u_callback(msg):
    x[2] = msg.data
def v_callback(msg):
    x[3] = msg.data
def udot_callback(msg):
    x[0] = msg.data
def vdot_callback(msg):
    x[1] = msg.data
def state_callback(msg):
    x[0] = msg.data[0]
    x[1] = msg.data[1]
    x[2] = msg.data[2]
    x[3] = msg.data[3]

if __name__=='__main__':

    #load references used for paper
    references = sio.loadmat('ReferenceInputs.mat')['reference_inputs']

    #adds functionality to parse command line arguments. 
    parser = argparse.ArgumentParser()
    parser.add_argument("--controller", 
                        choices=["MPC", "MRAC", "MRPAC"], required=True,
                        type=str,
                        help="Select the controller you would like to run.")
    #parse input arguments
    args = parser.parse_args()

    #store controller command line selection
    controller=args.controller

    # Create a model of the grub
    grub = BellowsGrub()
    
    x = np.ones([grub.numStates,1]).reshape(-1,1)*0.1
    # x = np.zeros([grub.numStates,1])

    #Initialize grub controll node
    rospy.init_node('grubController_%s' % controller)
    
    #Initialize subscribers to get state data from the grub
    #rospy.Subscriber("grubState/u", Float32, u_callback)
    #rospy.Subscriber("grubState/v", Float32, v_callback)
    #rospy.Subscriber("grubState/udot", Float32, udot_callback)
    #rospy.Subscriber("grubState/vdot", Float32, vdot_callback)
    rospy.Subscriber("grubState", Float32MultiArray, state_callback)

    #Initialize publisher to publish reference trajectory
    ref_traj_pub = rospy.Publisher('ref_trajectory', Float32MultiArray, queue_size = 10)

    #Initialize service that sends pressures to the grub
    rospy.wait_for_service('MoveJointPrs')
    move_joint_pressure_srv = rospy.ServiceProxy('MoveJointPrs', stmp.move_joint_prs, persistent=True)
    
    # Setup MRAC and MRPAC Machinery
    Lambda = 10.0
    Gamma = 10.0
    KD = 5.0

    sigma = 0.0
    mrpac = ManipulatorMRAC(my_regressor,2,Lambda*2.0,Gamma*2.0,KD*0.0,sigma)
    mrac = ManipulatorMRAC(my_regressor,2,Lambda,Gamma,KD,sigma)
    
    # MPC setup
    Q = 1.0*np.diag([.0,.0,1,1.0])
    Qf = 1.0*np.diag([0,0,1,1.0])
    R = .0005*np.eye(4)
    horizon = 10
    dt = .01
    xmin = -np.inf*np.ones(4)
    xmax = np.inf*np.ones(4)
    umin = 1.0*np.ones(4)
    umax = 200*np.ones(4)
    mpc = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax)
    pmean = (umax[0]-umin[0])/2. + umin[0]    
    
    # Create a reference system to track
    m = 3.0
    k = 100.0
    b = np.sqrt(4*m*k)
    Aref = np.matrix([[-b/m, 0, -k/m, 0],
                      [0,-b/m, 0, -k/m,],
                      [1, 0, 0, 0],
                      [0, 1, 0, 0]])
    Bref = np.matrix([[k/m, 0],
                      [0, k/m],
                      [0, 0],
                      [0, 0]])
    Adref = scipy.linalg.expm(Aref*dt)
    Bdref = np.matmul(np.linalg.inv(Aref),np.matmul(Adref-np.eye(Adref.shape[0]),Bref))

    reference_counter = 0
    r = np.array(references[:,reference_counter]).reshape(2,1)

    #initialize both systems in same spot
    u = np.ones([4,1])*pmean
    move_joint_pressure_srv(joint=0, target_position=u*1000.0, move_time=.000000001, move_type=msgs.robot_state.kMoveRefTypeSin)
    rospy.sleep(2.0)

    xref = deepcopy(x)  
    
    settle_time = 5.0
    last_time = time.time()
    rate = rospy.Rate(100)
    
    raw_input("Press enter to start")
    
    while not rospy.is_shutdown():

        start = time.time()
        
        if time.time()-last_time > settle_time:
            try:
                r = np.array(references[:,reference_counter]).reshape(2,1)
                reference_counter = reference_counter+1
                # Generate a new reference input
                # r = (np.random.rand(2,1)*np.pi/np.sqrt(2) - np.pi/2.0/np.sqrt(2))
                last_time = time.time()
            except:
                rospy.shutdown()

        xdotref = Aref.dot(xref) + Bref.dot(r)
        xref = Adref.dot(xref) + Bdref.dot(r) #this is what I want for reference trajectory


        # xref is a np.matrix and np.shape is (4,1)
        #Publish reference trajectory
        ref_traj_msg = Float32MultiArray()
        ref_traj_msg.data = xref
        ref_traj_pub.publish(ref_traj_msg)

        tic = time.time()
        if controller=='MRPAC':
            err = np.zeros([4,1])
            err[0:2] = mrpac.solve_for_next_u(x[2:4],x[0:2],xref[2:4],xref[0:2],xdotref[0:2])
            A,B,w = grub.calc_continuous_A_B_w(x)
            Ad,Bd = grub.discretize_A_and_B(A,B,dt)
            wd = np.linalg.inv(A).dot(Ad-np.eye(Ad.shape[0])).dot(w-err)
            
            xgoal = np.zeros([4,horizon])
            xref_future = deepcopy(xref)
            xgoal[:,0] = deepcopy(xref_future.flatten())
            for j in range(1,horizon):
                xref_future = Adref.dot(xref_future) + Bdref.dot(r)
                xgoal[:,j] = xref_future.flatten()

            u = mpc.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal=u).reshape(4,1)

        elif controller=='MPC':
            A,B,w = grub.calc_continuous_A_B_w(x)
            Ad,Bd = grub.discretize_A_and_B(A,B,dt)
            wd = np.linalg.inv(A).dot(Ad-np.eye(Ad.shape[0])).dot(w)

            xgoal = np.zeros([4,horizon])
            xref_future = deepcopy(xref)
            xgoal[:,0] = deepcopy(xref_future.flatten())
            for j in range(1,horizon):
                xref_future = Adref.dot(xref_future) + Bdref.dot(r)
                xgoal[:,j] = xref_future.flatten()
            
            u = mpc.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal=u).reshape(4,1)

        elif controller=='MRAC':
            u = mrac.solve_for_next_u(x[2:4],x[0:2],xref[2:4],xref[0:2],xdotref[0:2])
            u = u_to_pressures(u,pmean,umin[0],umax[0])

        toc = time.time()
        # print "find control pressures: ",toc-tic

        move_joint_pressure_srv(joint=0, target_position=u*1000.0, move_time=.000000001, move_type=1)

        # print "u: ", u.T
        # print "error:   ", xref[2:4].T - x[2:4].T
        #print "error norm:   ", np.linalg.norm(xref[2:4].T - x[2:4].T)
        # print "r:   ", r.T
        # print "x: ", x[2:4].T
        # print "xref: ", xref[2:4].T
        # print "params: ",mrpac.a.T

        rate.sleep()
        dt = time.time() - start
        #print dt
        

