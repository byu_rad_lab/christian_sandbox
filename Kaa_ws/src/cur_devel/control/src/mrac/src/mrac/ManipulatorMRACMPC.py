
if __name__=='__main__':

    def u_to_pressures(u,pmean):
        pressures = np.matrix([[pmean+u[0,0]/2*100.0],
                               [pmean-u[0,0]/2*100.0],
                               [pmean+u[1,0]/2*100.0],
                               [pmean-u[1,0]/2*100.0]])
        return pressures

    from rad_models.BellowsGrubIntegral import BellowsGrub
    import scipy
    from copy import deepcopy
    import matplotlib.pyplot as plt
    from ManipulatorMRAC import ManipulatorMRAC
    from osqp_mpc.ParameterizedSmallMatrixOSQPMPC import OSQPMPC
    import numpy as np

    def my_regressor(q,qd,qd_r,qdd_r):
        q = np.array(q)
        qd = np.array(qd)
        qd_r = np.array(qd_r)
        qdd_r = np.array(qdd_r)        
        
        reg = grub.calc_grub_regressor(q,qd,qd_r,qdd_r)

        reg = np.hstack([reg,np.eye(2)*q[0],np.eye(2)*q[1]])
        reg = np.hstack([reg,np.eye(2)*qd[0],np.eye(2)*qd[1]])        
        # reg = np.hstack([reg,np.eye(2)*qd_r[0],np.eye(2)*qd_r[1]])        
        # reg = np.hstack([reg,np.eye(2)*qdd_r[0],np.eye(2)*qdd_r[1]])        
        
        return reg
    

    grub = BellowsGrub()
    x = np.ones([8,1]).reshape(-1,1)*0.01
    Lambda = 5.0 #This is kinda like Jon's alpha...kinda...
    Gamma = 100.0
    KD = 0.0
    controller = ManipulatorMRAC(my_regressor,2,Lambda,Gamma,KD)

    Q = 1.0*np.diag([0,0,0,0,0,0,1,1.0])
    Qf = 1.0*np.diag([0,0,0,0,0,0,1,1.0])
    R = .00001*np.eye(4)
    horizon = 50
    num_params = 4
    xmin = -np.inf*np.ones(8)
    xmax = np.inf*np.ones(8)
    umin = 8*np.ones(4)
    umax = 500*np.ones(4)
    mpc = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax,num_params)

    # Create a reference system to track
    dt = .01
    m = 1.0
    b = 40.0
    k = 100.0
    Aref = np.matrix([[-b/m, 0, -k/m, 0],
                      [0,-b/m, 0, -k/m,],
                      [1, 0, 0, 0],
                      [0, 1, 0, 0]])
    Bref = np.matrix([[k/m, 0],
                      [0, k/m],
                      [0, 0],
                      [0, 0]])
    Adref = scipy.linalg.expm(Aref*dt)
    Bdref = np.matmul(np.linalg.inv(Aref),np.matmul(Adref-np.eye(Adref.shape[0]),Bref))
    xref = deepcopy(x[4:8])

    sim_length = 30000
    xref_hist = np.zeros([4,sim_length])
    x_hist = np.zeros([grub.numStates,sim_length])
    u_hist = np.zeros([4,sim_length])
    r_hist = np.zeros([Bdref.shape[1],sim_length])
    a_hist = np.zeros([controller.a.shape[0],sim_length])    
    r = np.array([[1.0],
                  [-1.0]])*1.0

    d = 250

    # plt.figure(5)
    # ax = plt.gca(projection='3d')
    # plt.ion()

    for i in range(0,sim_length):

        if i%d==0:
            r = np.random.randn(2,1)*0.5

        xdotref = Aref.dot(xref) + Bref.dot(r)
        xref = Adref.dot(xref) + Bdref.dot(r)

        err = np.zeros([8,1])
        
        # this is to push the output of MRAC into MPC
        err[4:6] = controller.solve_for_next_u(x[6:8],x[4:6],xref[2:4],xref[0:2],xdotref[0:2])

        pmean = 200
        A,B,w = grub.calc_continuous_A_B_w(x)

        #Introduce model error
        A = A*2.5
        B = B*2.0
        w = w*2.0        
                
        Ad,Bd = grub.discretize_A_and_B(A,B,dt)
        
        #this includes the control calculated from MRAC as a disturbance for MPC to use
        wd = np.linalg.inv(A).dot(Ad-np.eye(Ad.shape[0])).dot(w - err)

        xgoal = np.zeros([8,1])
        xgoal[4:8] = xref
        pmean = 250.*np.sin(i*.01) + 250.
        u = mpc.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal=np.ones([4,1])*pmean).reshape(4,1)
        # u = mpc.solve_for_next_u(Adref,Bdref,wd2,x,xgoal,ugoal=np.ones([4,1])*pmean).reshape(4,1)
        # print u
        # pressures = u_to_pressures(u,pmean)
        x = grub.forward_simulate_dt(x,u,dt)
        
        x_hist[:,i] = x.flatten()
        u_hist[:,i] = u.flatten()
        r_hist[:,i] = r.flatten()        
        xref_hist[:,i] = xref.flatten()
        a_hist[:,i] = controller.a.flatten()        

        # if i%100==0:
        #     grub.visualize(x,ax)

        if i%500==0:

            plt.figure(1)
            plt.clf()
            for j in range(0,2):
                plt.subplot(2,1,j+1)
                plt.plot(x_hist[6+j,0:i].T,'b')
                plt.plot(xref_hist[2+j,0:i].T,'r--')
                plt.legend(['x','x_ref'])
                plt.ylabel('Positions')
                plt.title('Positions')

            plt.figure(2)
            plt.clf()
            for j in range(0,2):
                plt.subplot(2,1,j+1)
                plt.plot(x_hist[4+j,0:i].T,'b')
                plt.plot(xref_hist[j,0:i].T,'r--')
                plt.legend(['x','x_ref'])
                plt.ylabel('Velocities')
                plt.title('Velocities')
                
            plt.figure(3)
            plt.clf()
            for j in range(0,2):
                plt.subplot(2,1,j+1)
                plt.plot(u_hist[j,0:i].T,'b')
                plt.plot(r_hist[j,0:i].T,'r--')
                plt.legend(['u','ref'])
                plt.ylabel('Inputs')
                plt.title('Inputs')

            plt.figure(4)
            plt.clf()
            p = controller.a.shape[0]
            plt.plot(a_hist[:,0:i].T)            
            # for j in xrange(0,p):
            #     plt.subplot(p,1,j+1)
            #     plt.plot(a_hist[j,0:i].T)            
            plt.ylabel('Params')                
            plt.pause(.0001)
    
    
    plt.pause(100000)
