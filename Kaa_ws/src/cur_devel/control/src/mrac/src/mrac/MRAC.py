import numpy as np

class MRAC():
    def __init__(self,Aref,Bref,B,regressor_func,x0,Gamma_x=100.1,Gamma_r=100.1,Gamma_theta=100.1):
        self.Aref = Aref
        self.Bref = Bref
        self.B = B
        self.regressor_func = regressor_func
        self.xref = x0
        self.Gamma_x = Gamma_x
        self.Gamma_r = Gamma_r
        self.Gamma_theta = Gamma_theta
        
        self.n = self.Aref.shape[0]
        self.m = self.Bref.shape[1]
        Phi = self.regressor_func(np.zeros([self.n,1]),np.zeros([self.m,1]))
        self.N = Phi.shape[0]

        self.Kx = np.zeros([self.n,self.m])
        self.Kr = np.zeros([self.m,self.m])
        self.Theta = np.zeros([self.N,self.m])
        # I assume Lambda is the identity, so I don't try to estimate it


    def calc_estimate_dots(self,x,r):
        e = x - self.xref
        Kxdot = -self.Gamma_x*x.dot(e.T).dot(self.B)
        Krdot = -self.Gamma_r*r.dot(e.T).dot(self.B)
        Phi = self.regressor_func(x,r)
        Thetadot = -self.Gamma_theta*Phi.dot(e.T).dot(self.B)
        return Kxdot,Krdot,Thetadot

    def update_xref(self,r,dt=.01):
        self.xref = (self.Aref.dot(self.xref) + self.Bref.dot(r)).reshape(-1,1)
    
    def update_estimates(self,x,r,dt=.01):
        
        Kxdot,Krdot,Thetadot = self.calc_estimate_dots(x,r)

        self.Kx = self.Kx + Kxdot*dt
        self.Kr = self.Kr + Krdot*dt
        self.Theta = self.Theta + Thetadot*dt
        

    def solve_for_next_u(self,x,r):

        Phi = self.regressor_func(x,r)
        u = self.Kx.T.dot(x) + self.Kr.T.dot(r) + self.Theta.T.dot(Phi)
        
        self.update_xref(r)        
        self.update_estimates(x,r)        

        return u
        










if __name__=='__main__':

    from rad_models.InvertedPendulum import InvertedPendulum
    import scipy
    from copy import deepcopy
    import matplotlib.pyplot as plt
    plt.ion()    

    dt = .01    

    # Define how I want my dynamics to be (my reference model)
    m = 1.0
    b = 1.5
    k = 3.0
    Aref = np.array([[-b/m, -k/m],
                     [1.0, 0]])
    Bref = np.array([[1.0/m],
                     [0]])

    Adref = scipy.linalg.expm(Aref*dt)
    Bdref = np.matmul(np.linalg.inv(Aref),np.matmul(Adref-np.eye(Adref.shape[0]),Bref))

    # This is the vector of basis functions used to approximate a matched disturbance
    def regressor_func(x,r):
        Phi = np.array([[x[1,0]],
                        [x[0,0]],
                        [np.sin(x[1,0])],
                        [np.cos(x[1,0])],
                        [1]]).reshape(-1,1)
        return Phi
    


    sys = InvertedPendulum(mass=1.0,damping=0.1,gravity=9.81)

    x = np.matrix([0,0.0]).reshape(sys.numStates,1)
    xref = deepcopy(x)

    Ad,Bd,wd = sys.calc_discrete_A_B_w(x,dt)
    
    controller = MRAC(Adref,Bdref,Bd,regressor_func,x)

    sim_length = 100000
    xref_hist = np.zeros([sys.numStates,sim_length])
    x_hist = np.zeros([sys.numStates,sim_length])
    u_hist = np.zeros([sys.numInputs,sim_length])
    r_hist = np.zeros([1,sim_length])
    Kx_hist = np.zeros([controller.n*controller.m,sim_length])
    Kr_hist = np.zeros([controller.m*controller.m,sim_length])
    Theta_hist = np.zeros([controller.N*controller.m,sim_length])
    r = np.array([[1.0]])    

    for i in range(0,sim_length):
        if i%1000==0:
            r = -r*1.0 + np.random.randn()*1.0

            if abs(r)>5.0:
                r = np.array([[1.0]])    

        u = np.matrix(controller.solve_for_next_u(x,r)).reshape(sys.numInputs,1)

        Ad,Bd,wd = sys.calc_discrete_A_B_w(x,dt)
        
        x = Ad*x + Bd*u + wd

        x_hist[:,i] = x.flatten()
        u_hist[:,i] = u.flatten()
        r_hist[:,i] = r.flatten()        
        xref_hist[:,i] = controller.xref.flatten()
        Kx_hist[:,i] = controller.Kx.flatten()
        Kr_hist[:,i] = controller.Kr.flatten()
        Theta_hist[:,i] = controller.Theta.flatten()        

        if i%1000==0:        

            plt.figure(4)
            plt.clf()
            plt.plot(x_hist[1,:].T,'b')
            plt.plot(xref_hist[1,:].T,'r--')
            plt.legend(['x','x_ref'])
            plt.ylabel('Angle (rad)')
            plt.title('Angle')
            
            plt.figure(2)
            plt.clf()            
            plt.plot(u_hist.T,'b')
            plt.plot(r_hist.T,'r--')
            plt.legend(['u','u_ref'])    
            plt.title('Inputs')        
            plt.ylabel('Input (Nm)')
            
            plt.figure(3)
            plt.clf()            
            plt.plot(Kr_hist.T,'r')
            plt.plot(Kx_hist.T,'b')            
            plt.plot(Theta_hist.T,'g')
            plt.title('Adaptive Parameters')
            plt.show()
            plt.pause(.0001)



    plt.figure(4)
    plt.clf()    
    plt.plot(x_hist[1,:].T,'b')
    plt.plot(xref_hist[1,:].T,'r--')
    plt.legend(['x','x_ref'])
    plt.ylabel('Angle (rad)')
    plt.title('Angle')
    
    plt.figure(2)
    plt.clf()    
    plt.plot(u_hist.T,'b')
    plt.plot(r_hist.T,'r--')
    plt.legend(['u','u_ref'])    
    plt.title('Inputs')        
    plt.ylabel('Input (Nm)')
    
    plt.figure(3)
    plt.clf()    
    plt.plot(Kx_hist.T,'b')
    plt.plot(Kr_hist.T,'r')
    plt.plot(Theta_hist.T,'g')
    plt.title('Adaptive Parameters')
    plt.show()
    plt.pause(.0001)
    
    plt.pause(1000)
    
    
