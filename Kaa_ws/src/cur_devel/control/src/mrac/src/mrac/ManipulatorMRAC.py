import numpy as np

# Most up to date version as of June 8, 2020 - Phil's graduation :)  - specific to robot arm manipulator
#
# This is based on the Slotine and Li MRAC paper. This implementation
# follows their algorithm which removes steady state position error
# and does not depend on measured joint accelerations.

# The regressor function passed into this class must follow the form
# outlined in Slotine and Li's paper:
# M(q) * qddot_d + C(q,qdot) * qdot_d + G(q) = Y(q, qdot, qdot_d, qddot_d) * a

class ManipulatorMRAC():
    def __init__(self,regressor_func,num_gen_coords,Lambda=1,Gamma=1,KD=1,sigma=0):
        
        self.regressor = regressor_func
        self.s = np.zeros([num_gen_coords,1])
        self.Y = self.regressor(self.s,self.s,self.s,self.s)
        p = self.Y.shape[1]
        self.a = np.zeros([p,1])
        self.Lambda = np.eye(num_gen_coords)*Lambda
        self.Gamma_inv = np.eye(p)*Gamma
        self.KD = np.eye(num_gen_coords)*KD
        self.sigma = sigma
        self.dt = .01

    def calc_s(self,q,qdot,q_d,qdot_d):
        s = qdot - qdot_d + self.Lambda.dot(q-q_d)
        return s

    def calc_refs(self,q,qdot,q_d,qdot_d,qddot_d):
        qddot_r = qddot_d - self.Lambda.dot(qdot-qdot_d)
        qdot_r = qdot_d - self.Lambda.dot(q-q_d)
        return qdot_r, qddot_r
        
    def update_s_Y_a(self,q,qdot,q_d,qdot_d,qddot_d):
        
        qdot_r, qddot_r = self.calc_refs(q,qdot,q_d,qdot_d,qddot_d)
        
        self.s = self.calc_s(q,qdot,q_d,qdot_d)
        self.Y = self.regressor(q,qdot,qdot_r,qddot_r)

        adot = -self.Gamma_inv.dot(self.Y.T.dot(self.s)) - self.sigma*self.a
        self.a = self.a + adot*self.dt
        
    def solve_for_next_u(self,q,qdot,q_d,qdot_d,qddot_d,adapt=True):

        if adapt==True:
            self.update_s_Y_a(q,qdot,q_d,qdot_d,qddot_d)
        else:
            qdot_r, qddot_r = self.calc_refs(q,qdot,q_d,qdot_d,qddot_d)
            self.s = self.calc_s(q,qdot,q_d,qdot_d)
            self.Y = self.regressor(q,qdot,qdot_r,qddot_r)

        tau = self.Y.dot(self.a) - self.KD.dot(self.s)

        return tau



if __name__=='__main__':

    def u_to_pressures(u,pmean):
        pressures = np.matrix([[pmean+u[0,0]/2*100.0],
                               [pmean-u[0,0]/2*100.0],
                               [pmean+u[1,0]/2*100.0],
                               [pmean-u[1,0]/2*100.0]])
        return pressures

    from rad_models.BellowsGrubIntegral import BellowsGrub
    import scipy
    from copy import deepcopy
    import matplotlib.pyplot as plt

    def my_regressor(q,qd,qd_r,qdd_r):
        q = np.array(q)
        qd = np.array(qd)
        qd_r = np.array(qd_r)
        qdd_r = np.array(qdd_r)        
        
        reg = grub.calc_grub_regressor(q,qd,qd_r,qdd_r)

        reg = np.hstack([reg,np.eye(2)*q[0],np.eye(2)*q[1]])
        reg = np.hstack([reg,np.eye(2)*qd[0],np.eye(2)*qd[1]])        
        # reg = np.hstack([reg,np.eye(2)*qd_r[0],np.eye(2)*qd_r[1]])        
        # reg = np.hstack([reg,np.eye(2)*qdd_r[0],np.eye(2)*qdd_r[1]])        
        
        return reg
    

    grub = BellowsGrub()
    x = np.ones([8,1]).reshape(-1,1)*0.01
    Lambda = 0.7 #This is kinda like Jon's alpha...kinda...
    Gamma = 0.5
    KD = 0.3
    controller = ManipulatorMRAC(my_regressor,2,Lambda,Gamma,KD)
    # controller.a[0] = grub.m
    # controller.a[1] = grub.h**2
    # controller.a[2] = grub.r**2

    # Create a reference system to track
    dt = .01
    m = 2.0
    b = 20.0
    k = 50.0
    Aref = np.matrix([[-b/m, 0, -k/m, 0],
                      [0,-b/m, 0, -k/m,],
                      [1, 0, 0, 0],
                      [0, 1, 0, 0]])
    Bref = np.matrix([[k/m, 0],
                      [0, k/m],
                      [0, 0],
                      [0, 0]])
    Adref = scipy.linalg.expm(Aref*dt)
    Bdref = np.matmul(np.linalg.inv(Aref),np.matmul(Adref-np.eye(Adref.shape[0]),Bref))
    xref = deepcopy(x[4:8])


    
    sim_length = 30000
    xref_hist = np.zeros([4,sim_length])
    x_hist = np.zeros([grub.numStates,sim_length])
    u_hist = np.zeros([2,sim_length])
    r_hist = np.zeros([Bdref.shape[1],sim_length])
    r = np.array([[1.0],
                  [-1.0]])*1.0

    d = 250

    # plt.figure(5)
    # ax = plt.gca(projection='3d')
    # plt.ion()

    for i in range(0,sim_length):

        if i%d==0:
            r = np.random.randn(2,1)*0.5

        xdotref = Aref.dot(xref) + Bref.dot(r)
        xref = Adref.dot(xref) + Bdref.dot(r)

        u = controller.solve_for_next_u(x[6:8],x[4:6],xref[2:4],xref[0:2],xdotref[0:2])

        pmean = 200
        pressures = u_to_pressures(u,pmean)

        x = grub.forward_simulate_dt(x,pressures,dt)

        x_hist[:,i] = x.flatten()
        u_hist[:,i] = u.flatten()
        r_hist[:,i] = r.flatten()        
        xref_hist[:,i] = xref.flatten()

        # if i%100==0:
        #     grub.visualize(x,ax)

        if i%500==0:

            plt.figure(1)
            plt.clf()
            for j in range(0,2):
                plt.subplot(2,1,j+1)
                plt.plot(x_hist[6+j,0:i].T,'b')
                plt.plot(xref_hist[2+j,0:i].T,'r--')
                plt.legend(['x','x_ref'])
                plt.ylabel('Positions')
                plt.title('Positions')

            plt.figure(2)
            plt.clf()
            for j in range(0,2):
                plt.subplot(2,1,j+1)
                plt.plot(x_hist[4+j,0:i].T,'b')
                plt.plot(xref_hist[j,0:i].T,'r--')
                plt.legend(['x','x_ref'])
                plt.ylabel('Velocities')
                plt.title('Velocities')
                
            plt.figure(3)
            plt.clf()
            for j in range(0,2):
                plt.subplot(2,1,j+1)
                plt.plot(u_hist[j,0:i].T,'b')
                plt.plot(r_hist[j,0:i].T,'r--')
                plt.legend(['u','ref'])
                plt.ylabel('Inputs')
                plt.title('Inputs')
                
            plt.pause(.0001)
    
    
    plt.pause(100000)
        
    
    
