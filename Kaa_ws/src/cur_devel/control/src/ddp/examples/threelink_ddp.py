from ddp.DDP import DDP
import numpy as np
from scipy.linalg import expm
import time
from rad_models.Threelink import *
from rad_models.LearnedThreelink import *

sys = Threelink()
learned_sys = Threelink()
# learned_sys = LearnedThreelink()

Q = 1.0*np.diag([0,0,0,3,2,1.])
Qf = 10.0*np.diag([0,0,0,3,2,1.])
R = .0001*np.diag([1,1,1])
# R = 0.001*np.eye(sys.numInputs)

def cost_function(x,u,xgoal,ugoal,final_timestep=False):
    if final_timestep==True:
        return 0.5*(xgoal.T-x).dot(Qf).dot(xgoal.T-x)
    else:
        return 0.5*(xgoal.T-x).dot(Q).dot(xgoal.T-x) + 0.5*u.T.dot(R).dot(u)            

def calc_cost_function_derivs(x,u,xgoal,ugoal,final_timestep=False):
    if final_timestep==True:
        return Qf.dot(x-xgoal.T), Qf
    else:
        return Q.dot(x-xgoal.T), R.dot(u), Q, R
        
horizon = 25
dt = .01
ddp = DDP(learned_sys.forward_simulate_dt,
          learned_sys.calc_A_B,
          cost_function,
          calc_cost_function_derivs,
          learned_sys.numStates,
          learned_sys.numInputs,
          dt,
          horizon)

    
x = np.array([[0],
               [0],
               [0],
               [-np.pi+.01],
               [0],
               [0.0]]).reshape([6,1])
xgoal = np.array([[0],
                   [0],
                   [0],
                   [0.0],
                   [0],
                   [0]]).reshape([6,1])

# U0 = 0.00*np.ones([learned_sys.numInputs,horizon])
U0 = 0.0*np.random.randn(learned_sys.numInputs,horizon)

# ddp.solve_for_u_trajectory(x,xgoal,U0=U)
# x_traj = ddp.X
# u_traj = ddp.U
# U0 = ddp.U

plt.figure(0)
# sys.visualize(x_traj[:,0],u_traj[:,0])

simulation_horizon = 300
x_hist = np.zeros([learned_sys.numStates,simulation_horizon])
u_hist = np.zeros([learned_sys.numInputs,simulation_horizon])
solve_time_hist = []
sim_cost = 0
num_ddp_iterations = 1

for i in range(0,simulation_horizon):

    tic = time.time()
    # ddp.solve_for_u_trajectory(x,xgoal,U0=U0)
    for j in range(0,num_ddp_iterations):
        ddp.do_single_iteration(x,xgoal,U0=U0)
        U0 = ddp.U    
    solve_time = time.time()-tic
    u = ddp.U[:,0]
    u = deepcopy(np.clip(u,-learned_sys.uMax,learned_sys.uMax)).reshape([sys.numInputs,-1])

    U0 = ddp.U
    U0 = np.append(U0,U0[:,-1].reshape(learned_sys.numInputs,1),axis=1)
    U0 = U0[:,1:]
    
    x = sys.forward_simulate_dt(x,u,.01)
    sys.visualize(x.flatten(),u.flatten())
    u_hist[:,i] = u.flatten()
    x_hist[:,i] = x.flatten()
    solve_time_hist.append(solve_time)

    sim_cost += (x-xgoal).T.dot(Q).dot(x-xgoal) + u.T.dot(R).dot(u)
    print("total cost: ",sim_cost)
    
    plt.pause(dt)
    time.sleep(dt)

data_dict = {'x':x_hist,
             'u':u_hist,
             'solve_times':solve_time_hist}
import scipy.io as sio
sio.savemat('ThreelinkiLQRTrajectoryV2.mat',data_dict)

    
plt.figure(1)
plt.plot(x_hist[3:6].T)
plt.xlabel('Timestep')
plt.ylabel('Theta (rad)')
plt.title('iLQR Position')
plt.show()

plt.figure(2)
plt.plot(u_hist.T)
plt.xlabel('Timestep')
plt.ylabel('Torque (Nm)')
plt.title('iLQR Input')
plt.show()


if(0):
    #-------------------------- Compare with LQR ---------------------#
    import scipy
    x = np.array([[0],
                  [0],
                  [0],
                  [-np.pi+.01],
                  [0],
                  [0.0]]).reshape([6,1])
    
    simulation_horizon = 100
    u = np.zeros(sys.numInputs)
    x_hist = np.zeros([sys.numStates,simulation_horizon])
    u_hist = np.zeros([sys.numInputs,simulation_horizon])
    Q = 1000.0*np.diag([0,0,0,3,2,2.])
    R = .10*np.diag([1,1,1.0])
    
    for i in range(0,simulation_horizon):
        
        A,B = sys.calc_A_B(x,u,dt)
        
        X = np.matrix(scipy.linalg.solve_discrete_are(A, B, Q, R))
        K = np.matrix(scipy.linalg.inv(B.T.dot(X).dot(B)+R)*(B.T.dot(X).dot(A)))
        
        u = np.array(K.dot(xgoal-x)).reshape([sys.numInputs,1])
        x = sys.forward_simulate_dt(x,u,dt)
        
        u_hist[:,i] = u.flatten()
        x_hist[:,i] = x.flatten()
        
    plt.figure(3)
    sys.visualize(x_hist[:,0].flatten(),u_hist[:,0].flatten())    
    
    for i in range(0,x_hist.shape[1]):
        sys.visualize(x_hist[:,i],u_hist[:,i])
        plt.pause(dt)
        time.sleep(dt)
    

    data_dict = {'x':x_hist,
                 'u':u_hist}
    import scipy.io as sio
    sio.savemat('ThreelinkLQRTrajectory.mat',data_dict)


    plt.figure(4)
    plt.plot(x_hist[3:6].T)
    plt.xlabel('Timestep')
    plt.ylabel('Theta (rad)')
    plt.title('LQR Position')
    plt.show()
    
    plt.figure(5)
    plt.plot(u_hist.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Torque (Nm)')
    plt.title('LQR Input')
    plt.ioff()
    
plt.pause(1000)

