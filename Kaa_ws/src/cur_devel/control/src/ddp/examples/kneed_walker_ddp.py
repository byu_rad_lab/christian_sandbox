from ddp.DDP import DDP
import numpy as np
from scipy.linalg import expm
import time
from copy import deepcopy

from rad_models.PDKneedWalker import *
from rad_models.KneedWalker import *

# sys = PDKneedWalker()
sys = KneedWalker()

Q = 10000.0*np.diag([10.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0])
# Qf = 100000.0*np.diag([10.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,10.0,0.,0.,0.,0.,0.])
Qf = Q
# Qf = 10.0*np.eye(sys.numStates)
# Q = 1000000.0*np.diag([0.0,0.0,0.0,0.0,0.0,0.0,0.0,0,1,0,0,0,0,0])
# Q = 1000.0*np.eye(sys.numStates)
#Qf = 1000.0*np.eye(sys.numStates)
# R = 1.001*np.eye(sys.numInputs)
R = .1*np.diag([1.0,1.0,1.0,1.0])

def cost_function(x,u,xgoal,ugoal,final_timestep=False):
    x = deepcopy(x)
    u = deepcopy(u)

    if final_timestep==True:
        return 0.5*(xgoal.T-x).dot(Qf).dot(xgoal.T-x)
    else:
        u = np.array(u).clip(-sys.tauMax,sys.tauMax)        
        return 0.5*(xgoal.T-x).dot(Q).dot(xgoal.T-x) + 0.5*u.T.dot(R).dot(u)            

def calc_cost_function_derivs(x,u,xgoal,ugoal,final_timestep=False):
    x = deepcopy(x)
    u = deepcopy(u)

    if final_timestep==True:
        return Qf.dot(x-xgoal.T), Qf
    else:
        return Q.dot(x-xgoal.T), R.dot(u), Q, R

def calc_dynamics_derivs(x,u,dt):
    x = deepcopy(x)
    u = deepcopy(u)
    A,B = sys.calc_A_B(x,u,dt)

    # A = A.clip(.01,np.inf)
    # A = A + np.eye(A.shape[0])*0.01

    # A = A*dt
    # B = B*dt

    # print "A cond before: ",np.linalg.cond(A)
    # u,s,v = np.linalg.svd(A)
    # sprime = np.linalg.inv(u).dot(A).dot(np.linalg.inv(v.T))
    # sprime = sprime + np.eye(sprime.shape[0])*10.1
    # A = u.dot(sprime).dot(v)
    # print "A cond after: ",np.linalg.cond(A)
    
    return A,B

def forward_simulate(x,u,dt):
    x = deepcopy(x)
    u = deepcopy(u)
    x = sys.forward_simulate_dt(x,u,dt)
    return x


        
horizon = 50
dt = .02
ddp = DDP(forward_simulate,
          calc_dynamics_derivs,
          cost_function,
          calc_cost_function_derivs,
          sys.numStates,
          sys.numInputs,
          dt,
          horizon)


theta = np.pi/6
x = np.matrix([[0.0,0,0,0,0,0,0,0,1.5+np.cos(theta),0,theta,-theta,-theta,theta]]).T
# x = np.matrix([[0.0,0,0,0,0,0,0,0,2.5,0,0,0,-np.pi/2.0,0]]).T
# theta = np.pi/6
# xgoal = np.matrix([[10.0,0,0,0,0,0,0,0.0,1.5+np.cos(theta),0,theta,-theta,-theta,theta]]).T
xgoal = np.matrix([[10.0,0,0,0,0,0,0,0,5.0,0,0,0,0,0]]).T
# xgoal = np.matrix([[0.0,0,0,0,0,0,0,0,2.5,0,0.0,0.0,-np.pi,0.0]]).T
# xgoal = np.matrix([[0.0,0,0,0,0,0,0,0,2.5,0,0,0,0,0]]).T

import scipy.io as sio
data = sio.loadmat('gooderU.mat')
U = data['U']

ddp.solve_for_u_trajectory(x,xgoal,U0=U)
# ddp.solve_for_u_trajectory(x,xgoal)
x_traj = ddp.X
u_traj = ddp.U

data_dict = {'U':u_traj,
             'X':x_traj}
sio.savemat('gooderU.mat',data_dict)

sys.visualize(x_traj[:,0])
plt.pause(10)

for i in range(0,x_traj.shape[1]):
    sys.visualize(x_traj[:,i])
    plt.pause(dt)
    time.sleep(dt)

plt.figure()
plt.plot(x_traj[7:14].T)
plt.xlabel('Timestep')
plt.ylabel('Theta (rad)')
plt.title('Position')
plt.show()

plt.figure()
plt.plot(x_traj[0:7].T)
plt.xlabel('Timestep')
plt.ylabel('Velocity (rad/s)')
plt.title('Velocity')
plt.show()

plt.figure()
plt.plot(u_traj.T)
plt.xlabel('Timestep')
plt.ylabel('Torque (Nm)')
plt.title('Input')
plt.show()
plt.ioff()

plt.pause(1000)
