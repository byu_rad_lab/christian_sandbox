from ddp.DDP import DDP
import numpy as np
from scipy.linalg import expm
import time

from rad_models.InvertedPendulum import *

sys = InvertedPendulum(mass=0.2,gravity=9.81)

Q = 100.0*np.diag([0,1])
Qf = 1000.0*np.diag([1,1])
R = 0.1*np.eye(sys.numInputs)

def cost_function(x,u,xgoal,ugoal,final_timestep=False):
    if final_timestep==True:
        return 0.5*(xgoal.T-x).dot(Qf).dot(xgoal.T-x)
    else:
        return 0.5*(xgoal.T-x).dot(Q).dot(xgoal.T-x) + 0.5*u.T.dot(R).dot(u)            

def calc_cost_function_derivs(x,u,xgoal,ugoal,final_timestep=False):
    if final_timestep==True:
        return Qf.dot(x-xgoal.T), Qf
    else:
        return Q.dot(x-xgoal.T), R.dot(u), Q, R
        
horizon = 50
dt = .01
ddp = DDP(sys.forward_simulate_dt,
          sys.calc_A_B,
          cost_function,
          calc_cost_function_derivs,
          sys.numStates,
          sys.numInputs,
          dt,
          horizon)

    
x = np.matrix([[0],
               [np.pi-.1]])
xgoal = np.matrix([[0],
                   [0]])
U = 0*np.ones([sys.numInputs,horizon])

ddp.solve_for_u_trajectory(x,xgoal,U0=U)
x_traj = ddp.X
u_traj = ddp.U

sys.visualize(x_traj[:,0])
# plt.pause(5)

for i in range(0,x_traj.shape[1]):
    sys.visualize(x_traj[:,i])
    plt.pause(dt)
    time.sleep(dt)

data_dict = {'x':x_traj,
             'u':u_traj}
import scipy.io as sio
sio.savemat('PendulumiLQRTrajectory.mat',data_dict)


plt.figure(2)
plt.plot(x_traj[1].T)
plt.xlabel('Timestep')
plt.ylabel('Theta (rad)')
plt.title('Position')
plt.ion()
plt.show()

plt.figure(3)
plt.plot(u_traj.T)
plt.show()
plt.xlabel('Timestep')
plt.ylabel('Torque (Nm)')
plt.title('Input')
#plt.hold(1)
plt.ion()

import scipy
horizon = 500
x = np.matrix([[0],
               [np.pi-.01]])

u = np.zeros(sys.numInputs)
x_hist = np.zeros([sys.numStates,horizon])
u_hist = np.zeros([sys.numInputs,horizon])
Q = 10.0*np.diag([0,1])
R = 0.1*np.eye(sys.numInputs)

for i in range(0,horizon):

    A,B = sys.calc_A_B(x,u,dt)

    X = np.matrix(scipy.linalg.solve_discrete_are(A, B, Q, R))
    K = np.matrix(scipy.linalg.inv(B.T.dot(X).dot(B)+R)*(B.T.dot(X).dot(A)))

    u = -K.dot(x)
    x = sys.forward_simulate_dt(x,u,dt)

    u_hist[:,i] = u.flatten()
    x_hist[:,i] = x.flatten()

plt.figure()
for i in range(0,x_hist.shape[1]):
    sys.visualize(x_hist[:,i])
    plt.pause(dt)
    time.sleep(dt)
    

data_dict = {'x':x_hist,
             'u':u_hist}
import scipy.io as sio
sio.savemat('PendulumLQRTrajectory.mat',data_dict)

print("plotting lqr states")
plt.figure(2)
plt.plot(x_hist[1].T)
plt.xlabel('Timestep')
plt.ylabel('Theta (rad)')
plt.title('Position')
plt.legend(['iLQR','LQR'])
plt.ion()
plt.show()
plt.hold(0)


print("plotting lqr inputs")
plt.figure(3)
plt.hold(1)
plt.plot(u_hist.T)
plt.show()
plt.xlabel('Timestep')
plt.ylabel('Torque (Nm)')
plt.title('Input')
plt.legend(['iLQR','LQR'])
plt.ioff()
plt.hold(0)
    
plt.pause(1000)

