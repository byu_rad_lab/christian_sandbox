import numpy as np
from scipy.linalg import expm
import time

'''
This class is designed to solve a trajectory optimization problem.
It must be given a function to calculate discrete dynamics matrices which fit
the discrete state space form x_{k+1} = A_k x_k + B_k u_k + w_k.
It must also be given a function to calculate cost based on state and input.
'''

np.set_printoptions(precision=1)

class DDP():

    def __init__(self,
                 forward_simulate_dt,
                 calc_dynamics_derivs,
                 cost_function,
                 calc_cost_function_derivs,
                 numStates,
                 numInputs,
                 dt,
                 horizon=10):
        
        self.forward_simulate_dt = forward_simulate_dt
        self.calc_dynamics_derivs = calc_dynamics_derivs        
        self.cost_function = cost_function
        self.calc_cost_function_derivs = calc_cost_function_derivs        
        self.dt = dt
        self.horizon = horizon
        self.numStates = numStates
        self.numInputs = numInputs
        self.maxDDPIters = 25

        self.X = np.zeros([self.numStates,self.horizon+1])
        self.U = None
        self.fx = np.zeros([self.numStates,self.numStates,self.horizon])
        self.fu = np.zeros([self.numStates,self.numInputs,self.horizon])
        # We ignore these in iLQG
        # fxx = np.zeros([self.numStates,self.numStates,self.numStates,self.horizon])
        # fuu = np.zeros([self.numStates,self.numInputs,self.numStates,self.horizon])
        # fux = np.zeros([self.numStates,self.numInputs,self.numStates,self.horizon])
        # fxu = np.zeros([self.numStates,self.numInputs,self.numStates,self.horizon])
        self.lx = np.zeros([self.numStates,self.horizon+1])
        self.lu = np.zeros([self.numInputs,self.horizon])
        self.lxx = np.zeros([self.numStates,self.numStates,self.horizon+1])
        self.luu = np.zeros([self.numInputs,self.numInputs,self.horizon])
        # I'm going to ignore these as well
        # lux = np.zeros([self.numInputs,self.numInputs,self.horizon+1])
        # lxu = np.zeros([self.numInputs,self.numInputs,self.horizon+1])

        self.Quu = np.zeros([self.numInputs,self.numInputs,self.horizon])
        self.Qu = np.zeros([self.numInputs,self.horizon])
        self.Qxu = np.zeros([self.numInputs,self.numStates,self.horizon])

    def calc_derivatives(self,xgoal,ugoal):
        
        k = self.horizon
        self.lx[:,k],self.lxx[:,:,k] = self.calc_cost_function_derivs(self.X[:,k],None,xgoal,ugoal,final_timestep=True)

        for k in range(0,self.horizon):
            self.lx[:,k],self.lu[:,k],self.lxx[:,:,k],self.luu[:,:,k] = self.calc_cost_function_derivs(self.X[:,k],self.U[:,k],xgoal,ugoal)


    def forward_pass(self,x0,xgoal,ugoal,first_pass=False,alpha=1.0):
        try:
            if self.U==None:
                print("Initial U is zero")
                self.U = np.zeros([self.numInputs,self.horizon])
        except:
            pass

        Xhat = np.zeros([self.numStates,self.horizon+1])
        Uhat = np.zeros([self.numInputs,self.horizon])
        newcost = 0
        Xhat[:,0] = x0.flatten()
        for k in range(0,self.horizon):
            if first_pass==True:
                Uhat[:,k] = self.U[:,k]
            else:
                try:
                    delta_uk = -np.linalg.inv(self.Quu[:,:,k]).dot(self.Qu[:,k]*alpha+self.Qxu[:,:,k].dot(Xhat[:,k]-self.X[:,k]))
                except:
                    delta_uk = -100.0*np.eye(self.Quu.shape[0]).dot(self.Qu[:,k]*alpha+self.Qxu[:,:,k].dot(Xhat[:,k]-self.X[:,k]))
                    
                Uhat[:,k] = self.U[:,k] + delta_uk

            ## Implementing squashing
            # Uhat[:,k] = 10.0*np.tanh(Uhat[:,k])            
            # u = 10.0*np.tanh(Uhat[:,k])

            ## Implementing clamping
            u = Uhat[:,k]
            u = np.clip(u,-10,10)
                
            Xhat[:,k+1] = self.forward_simulate_dt(Xhat[:,k],u,self.dt).flatten()
            
            self.fx[:,:,k],self.fu[:,:,k] = self.calc_dynamics_derivs(Xhat[:,k],Uhat[:,k],self.dt)
            newcost += self.cost_function(Xhat[:,k],Uhat[:,k],xgoal,ugoal)

        newcost += self.cost_function(Xhat[:,self.horizon],[],xgoal,ugoal,final_timestep=True)
        return Xhat,Uhat,newcost

    def backward_pass(self,xgoal,ugoal):

        # Boundary condition
        Vx = self.lx[:,self.horizon]
        Vxx = self.lxx[:,:,self.horizon]

        for k in range(self.horizon-1,-1,-1):

            # Pretty sure this is right
            Qx_ = self.lx[:,k] + self.fx[:,:,k].T.dot(Vx)
            Qu_ = self.lu[:,k] + self.fu[:,:,k].T.dot(Vx)
            Qxx_ = self.lxx[:,:,k] + self.fx[:,:,k].T.dot(Vxx).dot(self.fx[:,:,k])
            Quu_ = self.luu[:,:,k] + self.fu[:,:,k].T.dot(Vxx).dot(self.fu[:,:,k])
            Qxu_ = self.fu[:,:,k].T.dot(Vxx).dot(self.fx[:,:,k])

            # w,v = np.linalg.eig(Quu_)
            # alpha = np.mean(w)/1.0
            alpha = 1e-6
            # Quu_ = Quu_
            
            # Quu_inv = np.linalg.pinv(Quu_)
            Quu_inv = np.linalg.inv(Quu_.T.dot(Quu_) + alpha*np.eye(Quu_.shape[0])).dot(Quu_.T)
            

            Vx = Qx_ - Qu_.dot(Quu_inv).dot(Qxu_)
            Vxx = Qxx_ - Qxu_.T.dot(Quu_inv).dot(Qxu_)

            self.Quu[:,:,k] = Quu_
            self.Qu[:,k] = Qu_
            self.Qxu[:,:,k] = Qxu_

        # return Quu,Qu,Qxu
    

    def solve_for_u_trajectory(self,x0,xgoal,ugoal=None,U0=None):
        xgoal = np.array(xgoal).flatten()
        self.U = U0
        try:
            if ugoal==None:
                ugoal = np.zeros(self.numInputs)
        except:
            pass
            
        ugoal = np.array(ugoal).flatten()        

        self.X,self.U,cost = self.forward_pass(x0,xgoal,ugoal,first_pass=True)
        print("\n\n\n\n\n\n\n\n\n\n\n\n\nDID FIRST PASS!!!!!!!")
        print("cost: ",cost)
        
        converged=False
        iters = 0        
        while not converged:
            iters += 1
            print("iteration: ",iters)
            # tic = time.time()
            self.calc_derivatives(xgoal,ugoal)
            # print "Derivative time: ",time.time()-tic

            # tic = time.time()            
            self.backward_pass(xgoal,ugoal)
            # print "Backwards pass time: ",time.time()-tic

            X_hat,U_hat,cost_hat = self.forward_pass(x0,xgoal,ugoal)            

            # tic = time.time()                                    
            if cost_hat <= cost:
                print("Good step: ",cost_hat-cost)
                self.X = X_hat
                self.U = U_hat
                
                if np.abs(cost_hat-cost) < .001:
                    print("Converged: ",np.abs(cost_hat-cost))
                    converged=True
                else:
                    cost = cost_hat
            else:
                print("Bad step: ",cost_hat-cost)
                alpha = .01
                print(cost_hat)
                print(cost)
                while (cost_hat>cost or np.isnan(cost_hat)) and converged==True:
                    alpha = alpha*.01
                    
                    print(alpha)
                    X_hat,U_hat,cost_hat = self.forward_pass(x0,xgoal,ugoal,alpha=alpha)

                    if cost_hat < cost:
                        print("Did a good line search: ",cost_hat-cost)
                        self.X = X_hat
                        self.U = U_hat

                        if np.abs(cost_hat-cost) < .001:
                            print("Converged: ")
                            converged=True
                        
                        cost = cost_hat

                    if alpha < 1.0e-12:
                        print("Converged: ")
                        converged=True
                    if iters > self.maxDDPIters:
                        print("Converged: ")
                        converged=True
                    if abs(cost-cost_hat)<.1:
                        converged=True

            # print "Checking convergence and assigning time: ",time.time()-tic

    def solve_for_next_u(self,x0,xgoal,ugoal=None,U0=None):
        self.solve_for_u_trajectory(x0,xgoal,ugoal,U0)
        return self.U[:,0]

    def do_single_iteration(self,x0,xgoal,ugoal=None,U0=None):
        xgoal = np.array(xgoal).flatten()
        self.U = U0
        try:
            if ugoal==None:
                ugoal = np.zeros(self.numInputs)
        except:
            pass
            
        ugoal = np.array(ugoal).flatten()        

        self.X,self.U,cost = self.forward_pass(x0,xgoal,ugoal,first_pass=True)
        
        self.calc_derivatives(xgoal,ugoal)
        self.backward_pass(xgoal,ugoal)

        X_hat1,U_hat1,cost_hat1 = self.forward_pass(x0,xgoal,ugoal)
        X_hat2,U_hat2,cost_hat2 = self.forward_pass(x0,xgoal,ugoal,alpha=.01)
        X_hat3,U_hat3,cost_hat3 = self.forward_pass(x0,xgoal,ugoal,alpha=.0001)
        # X_hat4,U_hat4,cost_hat4 = self.forward_pass(x0,xgoal,ugoal,alpha=.01)        

        A = np.matrix([[1, 1, 1],
                       [.01**2, .01, 1],
                       [.0001**2, .0001, 1]])

        b = np.matrix([[cost_hat1],
                       [cost_hat2],
                       [cost_hat3]])
        x = np.linalg.pinv(A)*b
        alpha = (-x[1]/(2.0*x[0]))[0,0]
        if (alpha<0):
            alpha = 1e-4
        est_min_cost = -(x[1]**2)/(4*x[0]) + x[2]

        X_hat,U_hat,cost_hat = self.forward_pass(x0,xgoal,ugoal,alpha=alpha)

        A = np.vstack([A,
                       np.matrix([[alpha**2, alpha, 1]])])
        b = np.vstack([b,
                       np.matrix([[cost_hat]])])
        x = np.linalg.pinv(A)*b
        alpha = (-x[1]/(2.0*x[0]))[0,0]
        if (alpha<0):
            alpha = 1e-4
        
        est_min_cost = -(x[1]**2)/(4*x[0]) + x[2]        
        # print "x: ",x            
        # print "estimated minimum cost: ",est_min_cost
        # print "Actual cost: ",cost_hat
        # print "alpha: ",alpha
        
        while cost_hat > est_min_cost and abs(est_min_cost-cost_hat)>est_min_cost/2.0:
            X_hat,U_hat,cost_hat = self.forward_pass(x0,xgoal,ugoal,alpha=alpha)
            A = np.vstack([A,
                           np.matrix([[alpha**2, alpha, 1]])])
            b = np.vstack([b,
                           np.matrix([[cost_hat]])])
                
            x = np.linalg.pinv(A)*b
            if(abs(alpha-(-x[1]/(2.0*x[0]))[0,0]) < alpha*.001):
                alpha = (-x[1]/(2.0*x[0]))[0,0] + .1*np.random.rand()                
            else:
                alpha = (-x[1]/(2.0*x[0]))[0,0]
            if (alpha<0):
                alpha = 1e-4
            est_min_cost = -(x[1]**2)/(4*x[0]) + x[2]                    
            print("estimated minimum cost: ",est_min_cost)
            print("Actual cost: ",cost_hat)
            print("alpha: ",alpha)
            
        # print "Converged!"
        # print "estimated minimum cost: ",est_min_cost
        # print "Actual cost: ",cost_hat
        self.X = X_hat
        self.U = U_hat
                    

if __name__=='__main__':

    from rad_models.InvertedPendulum import *
    sys = InvertedPendulum()

    Q = 100.0*np.diag([0,1])
    Qf = 1000.0*np.diag([1,1])
    R = 0.001*np.eye(1)

    def cost_function(x,u,xgoal,ugoal,final_timestep=False):
        x = deepcopy(x)
        u = deepcopy(u)
        if final_timestep==True:
            return 0.5*(xgoal.T-x).dot(Qf).dot(xgoal.T-x)
        else:
            return 0.5*(xgoal.T-x).dot(Q).dot(xgoal.T-x) + 0.5*u.T.dot(R).dot(u)            

    def calc_cost_function_derivs(x,u,xgoal,ugoal,final_timestep=False):
        x = deepcopy(x)
        u = deepcopy(u)
        
        if final_timestep==True:
            return Qf.dot(x-xgoal.T), Qf
        else:
            return Q.dot(x-xgoal.T), R.dot(u), Q, R        
    horizon = 500
    dt = .01
    ddp = DDP(sys.forward_simulate_dt,
              sys.calc_A_B,
              cost_function,
              calc_cost_function_derivs,
              sys.numStates,
              sys.numInputs,
              dt,
              horizon)
    
    
    # Solve for a trajectory
    x = np.matrix([[0],
                   [np.pi/2-.01]])
    xgoal = np.matrix([[0],
                       [0]])

    ddp.solve_for_u_trajectory(x,xgoal)
    x_traj = ddp.X
    u_traj = ddp.U


    print(x_traj.shape)

    for i in range(0,x_traj.shape[1]):
        sys.visualize(x_traj[:,i])
        plt.pause(dt)
        time.sleep(dt)

