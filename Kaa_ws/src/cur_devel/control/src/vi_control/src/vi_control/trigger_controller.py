#!/usr/bin/env python


from scipy.linalg import expm
import numpy as np
import sys,rospy,jdagger_solver,time
from netft_utils.srv import SetBias

class SolveExpm():
    ''' Input A,B,x,u and rate to solve Matrix Exponential for next time step x'''
    def __init__(self,A,B,rate,x,u):
        self.x = []
        if np.linalg.cond(A) < 1/sys.float_info.epsilon:
            pass
        else:
            print('A matrix in continuous dynamics if very poorly conditioned')
            print('Exiting')
            sys.exit()

        I = np.eye(A.shape[0])
        A_d = np.matrix(expm(A*rate))
        B_d = (np.linalg.solve(A,(A_d - I) * B))

        self.x = A_d*x + B_d*u

class VITools():
    ''' This class starts out by determining difference in pose. Then, this class uses force, time derivative of force, damping, damping alpha, mass, rate, robot jacobian to calculate the next velocity to publish'''
    def __init__(self):
        self.xdot_next = [0.0,0.0,0.0,0.0,0.0,0.0]
        self.qdot_cmd = [0.0] * 10
        self.xdot_error_next = []
        self.state = 'stop'
        self.prev_state = 'stop'
        self.torque_lim = 3.0
        self.xtorq_lim = 1.5
        self.vel_min = 0.3
        self.check_time = time.time()

    def get_error_term(self,goal_position,goal_orientation,current_position, current_orientation,Kp,Ko,use_pos_err):
        ''' gets current pose and compares it to the "goal" pose to determine an xdot_next term to send to optimizer (orientations should be in quaternion form). The purpose of this is to make the robot pull towards a neutral position, and keep it from drifting.'''
        x_g = goal_position
        q_g = goal_orientation
        x_c = current_position
        q_c = current_orientation

        # Gets cartesian velocity error term
        if use_pos_err == True:
            xdot = np.multiply(np.add(x_g,-x_c),Kp)
        else:
            xdot = [0.0, 0.0, 0.0]

        # Gets orientation velocity error term
        p1 = np.dot(q_c[3],q_g[:3])
        p2 = np.dot(q_g[3],q_c[:3])
        p3 = np.dot([[0, -q_c[2], q_c[1]],[q_c[2], 0, -q_c[0]],[-q_c[1], q_c[0], 0]],q_g[:3])

        edot = np.multiply(np.add(np.add(p1,-p2),p3),Ko)

        self.xdot_error_next = [xdot[0], xdot[1], xdot[2], edot[0], edot[1], edot[2]]

    def solve_mat_exp(self,x,Fi, dFi, c, alpha, m, hz):
        ''' Take in velocity, force, time derivative of force, damping, alpha damping, mass, rate, and returns the variable impedance calculated next time step velocity using matrix exponential. x, Fi, and dFi should be 1x3 arrays in [x,y,z] form; c, alpha, m, and hz should be scalars.'''
        A = np.matrix([[ -c/m + alpha * dFi[0]/m, 0.0, 0.0], [0.0, -c/m + alpha * dFi[1]/m, 0.0],[0.0, 0.0, -c/m + alpha * dFi[2]/m]])
        B = np.eye(3,3) * [1/m, 1/m, 1/m]
        x = np.matrix(x)
        u = np.matrix([[Fi[0]],[Fi[1]],[Fi[2]]])
        # Solve matrix exponential
        mat = SolveExpm(A,B,1.0/hz,x,u)

        # Avoid using slow velocities?
        if np.linalg.norm([mat.x.item(0),mat.x.item(1),mat.x.item(2),0.0,0.0,0.0]) < 0.07:
            self.xdot_next = [0.0,0.0,0.0,0.0,0.0,0.0]
        else:
            # self.xdot_next = [mat.x.item(0),mat.x.item(1),mat.x.item(2),0.0,0.0,0.0]
            self.xdot_next[0] = mat.x.item(0)
            self.xdot_next[1] = mat.x.item(1)

    def solve_cvxgen_qdot(self,J):
        '''takes in xdot_next(next time step velocity predition) and a 6xN jacobian where N is number of joints. Then uses cvxgen optimizer to get qdot values to command to robot'''
        # self.qdot_cmd = jdagger_solver.runController(list(self.xdot_next),
        #                                          J.flatten('F').A1.tolist())

        # if len(self.qdot_cmd) > 2:
        #     self.qdot_cmd = self.qdot_cmd
        # else:
        #     self.qdot_cmd = self.qdot_cmd[0]
        self.qdot_cmd[0] = self.xdot_next[0]
        self.qdot_cmd[1] = self.xdot_next[1]
        self.qdot_cmd[2] = self.xdot_next[5]
        self.qdot_cmd[3:] = [0.0] * 7
        
    def check_prev_state(self,arm,Fx):
        '''If stopped for over 4 seconds, check previous state to see if we need to rebias'''
        if arm == 'left':
            b = 'bias2'
        else:
            b = 'bias1'

        if self.state == 'stop' and self.prev_state != 'stop':
            self.check_time = time.time()

        if self.state == 'stop' and self.prev_state == 'stop':
            if time.time() - self.check_time > 4.0:
                if abs(Fx) < 2.0:
                    self.check_time = time.time()
                    rospy.wait_for_service(b)
                    try:
                        print('Biasing',arm)
                        sp = rospy.ServiceProxy(b,SetBias)
                        sp(True,90,10)
                        self.check_time = time.time()
                    except rospy.ServiceException as e:
                        print('Service Failed')
                        self.check_time = time.time()

    def trigger_case(self,xtorq,ztorq,arm,Fx):
        ''' takes in a torque value, a torque value from X seconds ago, and a state and returns xdot_next V_y and W_z to use'''
        self.prev_state = self.state

        if ztorq > self.torque_lim and xtorq < -self.xtorq_lim:
            out_state = 'translating_right'
            self.xdot_next[1] = self.vel_min
            self.xdot_next[5] = 0.0
        elif ztorq > self.torque_lim and xtorq >= -self.xtorq_lim:
            out_state = 'rotating_right'
            self.xdot_next[1] = 0.0
            self.xdot_next[5] = self.vel_min * 5.0/8.0
        elif ztorq < -self.torque_lim and xtorq <= self.xtorq_lim:
            out_state = 'rotating_left'
            self.xdot_next[1] = 0.0
            self.xdot_next[5] = -self.vel_min * 5.0/8.0
        elif ztorq < -self.torque_lim and xtorq > self.xtorq_lim:
            out_state = 'translating_left'
            self.xdot_next[1] = -self.vel_min
            self.xdot_next[5] = 0.0
        else:
            out_state = 'stop'
            self.xdot_next[1] = 0.0
            self.xdot_next[5] = 0.0

        self.state = out_state
        # self.check_prev_state(arm,Fx)

    def other_trigger_case(self,xtorq,ztorq,arm,Fx):
        ''' takes in a torque value, a torque value from X seconds ago, and a state and returns xdot_next V_y and W_z to use'''
        self.prev_state = self.state

        if ztorq > self.torque_lim and xtorq > self.xtorq_lim:
            out_state = 'translating_right'
            self.xdot_next[1] = self.vel_min
            self.xdot_next[5] = 0.0
        elif ztorq > self.torque_lim and xtorq <= self.xtorq_lim:
            out_state = 'rotating_right'
            # self.xdot_next[0] = 0.0
            self.xdot_next[1] = 0.0
            self.xdot_next[5] = self.vel_min * 10.0/8.0
        elif ztorq < -self.torque_lim and xtorq >= -self.xtorq_lim:
            out_state = 'rotating_left'
            # self.xdot_next[0] = 0.0
            self.xdot_next[1] = 0.0
            self.xdot_next[5] = -self.vel_min * 10.0/8.0
        elif ztorq < -self.torque_lim and xtorq < -self.xtorq_lim:
            out_state = 'translating_left'
            self.xdot_next[1] = -self.vel_min
            self.xdot_next[5] = 0.0
        else:
            out_state = 'stop'
            self.xdot_next[1] = 0.0
            self.xdot_next[5] = 0.0

        self.state = out_state
        # self.check_prev_state(arm,Fx)
