#!/usr/bin/env python
import rospy,math,sys,time
import numpy as np
from netft_rdt_driver.force_manipulation import ForceManip
from megazord.current_vel import CurVel
from megazord.megazord_kdl import MegazordKDL
from sensor_msgs.msg import JointState
from baxter_core_msgs.msg import JointCommand
from geometry_msgs.msg import Twist, PoseWithCovarianceStamped
import tf.transformations as tft
import tf_conversions.posemath as pm
from scipy import signal
import matplotlib.pyplot as plt

if len(sys.argv) > 1:
    controller = str(sys.argv[1])
    if controller == 'with_torque_vi_control':
        from with_torque_vi_control import VITools
    elif controller == 'trigger_controller':
        from trigger_controller import VITools
else:
    from trigger_controller import VITools


class Subscribe2BaxterLimb():
    ''' Class will subscribe to baxter limb specified and extract joint position and velocities'''
    def __init__(self,limb):
        if limb == 'left':
            self.sub = rospy.Subscriber('/robot/joint_states',JointState,self.limb_callback_left)
        elif limb == 'right':
            self.sub = rospy.Subscriber('/robot/joint_states',JointState,self.limb_callback_right)

        self.q = [0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        self.qdot = [0.0,0.0,0.0,0.0,0.0,0.0,0.0]
            
    def limb_callback_left(self,msg):
        if len(msg.position) > 5:
            self.q = [msg.position[4], msg.position[5], msg.position[2], msg.position[3], msg.position[6], msg.position[7], msg.position[8]]
            self.qdot = [msg.velocity[4], msg.velocity[5], msg.velocity[2], msg.velocity[3], msg.velocity[6], msg.velocity[7], msg.velocity[8]]

    def limb_callback_right(self,msg):
        if len(msg.position) > 5:
            self.q = [msg.position[11], msg.position[12], msg.position[9], msg.position[10], msg.position[13], msg.position[14], msg.position[15]]
            self.qdot = [msg.velocity[11], msg.velocity[12], msg.velocity[9], msg.velocity[10], msg.velocity[13], msg.velocity[14], msg.velocity[15]]

class Subscribe2Base():
    '''Get Base velocities and positions'''
    def __init__(self):
        self.sub1 = rospy.Subscriber('/robot_pose_ekf/odom_combined', PoseWithCovarianceStamped, self.pose_callback)
        self.sub2 = rospy.Subscriber('/est_vel', Twist, self.vel_callback)
        self.V = [0.0,0.0,0.0]
        self.W = [0.0,0.0,0.0]
        self.P = [0.0,0.0,0.0]
        self.O = [0.0,0.0,0.0,1.0]

    def pose_callback(self,msg):
        self.P[0] = msg.pose.pose.position.x
        self.P[1] = msg.pose.pose.position.y
        self.P[2] = msg.pose.pose.position.z
        self.O[0] = msg.pose.pose.orientation.x
        self.O[1] = msg.pose.pose.orientation.y
        self.O[2] = msg.pose.pose.orientation.z
        self.O[3] = msg.pose.pose.orientation.w
        
    def vel_callback(self,msg):
        self.V[0] = msg.linear.x
        self.V[1] = msg.linear.y
        self.W[2] = msg.angular.z

class LimbAttributes():
    ''' gets attributes for left and right limbs, depending on input'''
    def __init__(self,limb):
        self.limb = limb
        if limb == 'left':
            self.SBL = Subscribe2BaxterLimb(limb)
            self.SBB = Subscribe2Base()
            self.arm_pub = rospy.Publisher('/robot/limb/left/joint_command',JointCommand,queue_size = 10)
            self.jnt_cmd = JointCommand()
            self.jnt_cmd.mode = 2
            self.jnt_cmd.command = np.resize(self.jnt_cmd.command,(7,1))
            self.jnt_cmd.names = ['left_s0','left_s1','left_e0','left_e1','left_w0','left_w1','left_w2']
        elif limb == 'right':
            self.SBL = Subscribe2BaxterLimb(limb)
            self.SBB = Subscribe2Base()
            self.arm_pub = rospy.Publisher('/robot/limb/right/joint_command',JointCommand,queue_size = 10)
            self.jnt_cmd = JointCommand()
            self.jnt_cmd.mode = 2
            self.jnt_cmd.command = np.resize(self.jnt_cmd.command,(7,1))
            self.jnt_cmd.names = ['right_s0','right_s1','right_e0','right_e1','right_w0','right_w1','right_w2']
        self.base_cmd = Twist()
        self.get_joint_angles()
        self.start_ang = self.jt_angles[3:]


    def get_joint_angles(self):
        ''' gets current joint angles for limb only '''
        angs = tft.euler_from_quaternion([self.SBB.O[0],self.SBB.O[1],self.SBB.O[2],self.SBB.O[3]])
        self.jt_angles = [self.SBB.P[0],self.SBB.P[1],angs[2]] + self.SBL.q

    def get_joint_velocities(self):
        ''' gets current joint velocities'''
        self.jt_velocities = [self.SBB.V[0], self.SBB.V[1], self.SBB.W[2]] + self.SBL.qdot

    def get_current_pose_and_vel(self):
        ''' get current velocities cartesian space, also jacobian '''
        self.Robot = CurVel(self.limb, self.jt_angles, self.jt_velocities)
        Frame = self.Robot.Frame
        R = pm.toMatrix(Frame)
        self.q_cur = tft.quaternion_from_matrix(R)
        self.x_cur = [self.Robot.pos[0],self.Robot.pos[1],self.Robot.pos[2]]
        self.x_dot = np.transpose(np.matrix([ self.Robot.xdot.item(0), self.Robot.xdot.item(1), self.Robot.xdot.item(2)]))
        

if __name__ == "__main__":
    rospy.init_node('vi_control',anonymous = True)

    hz = 100.0
    rate = rospy.Rate(hz)

    # Get parameters via rosparam
    if rospy.has_param('limb'):
        limb = str(rospy.get_param('limb'))
    else:
        limb = 'left'
    if rospy.has_param('c'):
        c = rospy.get_param('c')
    else:
        c = 5.0
    if rospy.has_param('alpha'):
        alpha = rospy.get_param('alpha')
    else:
        alpha = 0.3
    if rospy.has_param('Kp'):
        Kp = rospy.get_param('Kp')
    else:
        Kp = 0.0 
    if rospy.has_param('Kd'):
        Kd = rospy.get_param('Kd')
    else:
        Kd = 1.0 
    if rospy.has_param('goal_position'):
        goal_position = rospy.get_param('goal_position')
        gpl = goal_position[:3]
        gpr = goal_position[3:]
    else:
        goal_position = [0.577, 0.189, 0.29,0.577,-0.189,0.29]
        gpl = goal_position[:3]
        gpr = goal_position[3:]
    if rospy.has_param('goal_orientation'):
        goal_orientation = rospy.get_param('goal_orientation')
        gol = goal_orientation[:4]
        gor = goal_orientation[4:]
    else:
        goal_orientation = [0.145, 0.989, 0.006, 0.03,-0.145,0.991,-0.014,0.03]
        gol = goal_orientation[:4]
        gor = goal_orientation[4:]
    if rospy.has_param('m'):
        m = rospy.get_param('m')
    else:
        m = 10.0 
    if rospy.has_param('Q'):
        Q = rospy.get_param('Q')
    else:
        Q = [1.0,0.01,0.02,0.1,0.1,0.1,0.3,0.3,0.3,0.3]
    if rospy.has_param('use_pos_error'):
        use_pos_error = rospy.get_param('use_pos_error')
    else:
        use_pos_error = False

    # Call classes for limb
    L = LimbAttributes('left')
    Fl = ForceManip('left', 0.34, 0.17, 0.0, 1.0/hz)
    R = LimbAttributes('right')
    Fr = ForceManip('right', 0.34, 0.17, 0.0, 1.0/hz)
    vitoolsl = VITools()
    vitoolsr = VITools()

    # Start base publisher
    base_pub = rospy.Publisher('/des_vel',Twist,queue_size = 10)

    # Print output
    print('\nRunning Control on',limb,'arm(s).\n')
    print_time = time.time()

    # Loop of Control
    while not rospy.is_shutdown():
        # Update Torque Values
        xtorque = Fl.F[3] + Fr.F[3]
        ztorque = Fl.F[5] + Fr.F[5]

        # Update joint angles, velocities, positions, jacobians, etc.
        L.get_joint_angles()
        L.get_joint_velocities()
        L.get_current_pose_and_vel()
        R.get_joint_angles()
        R.get_joint_velocities()
        R.get_current_pose_and_vel()

        # Get current pose and compare to desired, calculate edot (xdot_next)
        vitoolsl.get_error_term(gpl,gol,L.x_cur,L.q_cur,Kp,Kd,use_pos_error)
        vitoolsr.get_error_term(gpr,gor,R.x_cur,R.q_cur,Kp,Kd,use_pos_error)

        if controller == 'trigger_controller':
            # IF USING Trigger CONTROLLER # # # # # # # # # # #
            # Calculate xdot_next using matrix exponential and variable impedance control
            vitoolsl.solve_mat_exp(L.x_dot,Fl.F[0:3],Fl.dF[0:3],c,alpha,m,hz)
            vitoolsr.solve_mat_exp(R.x_dot,Fr.F[0:3],Fr.dF[0:3],c,alpha,m,hz)
            
            vitoolsl.other_trigger_case(xtorque,ztorque,'left',Fl.F[0])
            vitoolsr.other_trigger_case(xtorque,ztorque,'right',Fr.F[0])
        elif controller == 'with_torque_vi_control':
            # IF USING VI Planar CONTROLLER # # # # # # # # # # #
            Tau = [xtorque,0.0,ztorque]
            dTau = [Fl.dF[3] + Fr.dF[3],0.0,Fl.dF[5] + Fr.dF[5]]
            w = [0.0,0.0,L.SBB.W[2]]
            vitoolsl.solve_mat_exp(L.x_dot,Fl.F[0:3],Fl.dF[0:3],c,alpha,m,hz,w,Tau,dTau,c,alpha,m*0.1)
            vitoolsr.solve_mat_exp(R.x_dot,Fr.F[0:3],Fr.dF[0:3],c,alpha,m,hz,w,Tau,dTau,c,alpha,m*0.1)


        # Get second Jacobian for use with edot
        Jl = np.matrix(L.Robot.J)
        Jel = Jl[:,3:]
        Jr = np.matrix(R.Robot.J)
        Jer = Jr[:,3:]
        
        vitoolsl.solve_cvxgen_qdot(Jl)
        vitoolsr.solve_cvxgen_qdot(Jr)
        
        # Publish
        pull_to_center_l = np.multiply(np.subtract(L.start_ang,L.jt_angles[3:]),0.77)
        pull_to_center_r = np.multiply(np.subtract(R.start_ang,R.jt_angles[3:]),0.77)
        
        L.jnt_cmd.command = np.add(vitoolsl.qdot_cmd[3:],pull_to_center_l)
        R.jnt_cmd.command = np.add(vitoolsr.qdot_cmd[3:],pull_to_center_r)
        
        Tot_base = Twist()
        
        Tot_base.linear.x = (vitoolsr.qdot_cmd[0] + vitoolsl.qdot_cmd[0])
        Tot_base.linear.y = (vitoolsr.qdot_cmd[1] + vitoolsl.qdot_cmd[1])
        Tot_base.angular.z = (vitoolsr.qdot_cmd[2] + vitoolsl.qdot_cmd[2])
        
        base_pub.publish(Tot_base)

        # L.arm_pub.publish(L.jnt_cmd)
        # R.arm_pub.publish(R.jnt_cmd)
        
        # # Printout
        # if time.time() - print_time > 0.5:
        #     # print ztorque,xtorque,vitoolsl.state
        #     prev_xvel = Tot_base.linear.x
        #     print_time = time.time()

        # Sleep rate
        rate.sleep()

