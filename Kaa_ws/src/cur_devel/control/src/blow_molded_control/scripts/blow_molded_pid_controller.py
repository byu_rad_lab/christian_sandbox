#!/usr/bin/env python
import blow_molded_joint_controller as jc
import numpy as np
import rospy

class BlowMoldedPID(jc.BlowMoldedJointController):

    def __init__(self, kp, ki, kd):

        self.kp = kp
        self.ki = ki
        self.kd = kd

        self.integrator = np.array([0.0,0.0,0.0,0.0])

        jc.BlowMoldedJointController.__init__(self)
        self.p_nom = 140000

    def find_pressures(self, q, q_cmd, qd, qd_cmd):

        pressures = []
        print("q: ", q)
        print("q_cmd: ", q_cmd)

        error = np.array(q_cmd) - np.array(q)
        print("error: ", error)
        print("kp: ", self.kp)
        print("time step: ", self.timestep)
        print("time step: ", self.timestep*error.T)

        self.integrator += self.timestep*error.T

        print("cow mania: ", np.dot(self.ki, self.integrator.T))

        pdiff = np.dot(self.kp, error.T) + np.dot(self.ki, self.integrator.T)

        print("pdiff: ", pdiff)

        for i in range(len(q)):
            pressures.append(self.p_nom + pdiff[i])
            pressures.append(self.p_nom - pdiff[i])

        return pressures

if __name__ == "__main__":

    rospy.init_node('blow_molded_pid', anonymous=True)

    kp = 10000.0*np.eye(4)
    ki = 10000.0*np.eye(4)

    controller = BlowMoldedPID(kp,ki,1.0)

    rate = rospy.Rate(controller.rate)

    while not rospy.is_shutdown():

        controller.run()

        rate.sleep()

