#!/usr/bin/env python
from abc import ABCMeta, abstractmethod
import numpy as np
from subscribers import system_minimal_msg_subscriber as smm_sub
from subscribers import wrench_stamped_subscriber as w_sub
import rospy
from ros_srv.srv import uint32_srv #SetRobotCtrlMode
from ros_srv.srv import RequestMoveSrv #RequestMoveLinear
from std_msgs.msg import Float32MultiArray
import sys
import time

class BlowMoldedForceController(metaclass=ABCMeta):
    def __init__(self):

        self.rate = 10.0
        self.timestep = 1.0/self.rate

        self.joint_sub = smm_sub.SystemMinimalMsgSubscriber('/shere_khan_uv/estimation_minimal/')
        self.prs_sub = smm_sub.SystemMinimalMsgSubscriber('/atheris/system_minimal/')
        self.force_sub = w_sub.WrenchStampedSubscriber('/ftnode1/netft_data')
        self.force_cmd_sub = w_sub.WrenchStampedSubscriber('/force_cmd')
        self.cmd_pub = rospy.Publisher('/pressure_cmds',Float32MultiArray)

        self.prs_max = 310000  
        self.prs_min = 8000  

        ####THIS IS SO BAD, GET RID OF IT SOON ##########################
        ####THIS IS SO BAD, GET RID OF IT SOON ##########################
        # start_time = time.time()
        # while time.time()-start_time < 30:
        #     self.joint_sub.getData()            
        #     self.q_nom = self.joint_sub.q_est
        #     print "q_nom is: ", self.q_nom
        #     rospy.sleep(0.1)
        # sys.exit()
        rospy.sleep(30) 
        # self.joint_sub.getData()
        # self.q_nom = self.joint_sub.q_est
        ####THIS IS SO BAD, GET RID OF IT SOON ##########################

        # set to pressure control through service
        self.setCtrl = rospy.ServiceProxy('/atheris/SetRobotCtrlMode', uint32_srv, persistent=True)
        try:
            worked = self.setCtrl(1)
        except rospy.ServiceException as e:
            print('Error :', e)

        self.req_move_sin_pressure = rospy.ServiceProxy('/atheris/RequestMoveSinusoidalPressure', RequestMoveSrv, persistent=True)


    def send_pressure_command(self,pressures,movetime):
        print(movetime," second sinusoidal pressure move to: ",pressures)
        cmd_msg = Float32MultiArray()
        cmd_msg.data = pressures
        self.cmd_pub.publish(cmd_msg)
        result = self.req_move_sin_pressure.call(movetime, pressures)

    def run(self):

        #get estimate data
        self.joint_sub.getData()
        self.prs_sub.getData()
        self.force_sub.getData()
        self.force_cmd_sub.getData()

        q = self.joint_sub.q_est
        #q = self.q_nom
        f = self.force_sub.wrench.force
        tau = self.force_sub.wrench.torque
        f_cmd = self.force_cmd_sub.wrench.force
        tau_cmd = self.force_cmd_sub.wrench.torque
        prs_cur = self.prs_sub.prs_bel_est
        print("reading pressures: ", prs_cur)

        wrench = [[f.x],[f.y],[f.z],[tau.x],[tau.y],[tau.z]]
        wrench_cmd = [[f_cmd.x],[f_cmd.y],[f_cmd.z],[tau_cmd.x],[tau_cmd.y],[tau_cmd.z]]
        print("f_cmd_from_pub: ", wrench_cmd)

        #wrench_cmd = [[0.0],[0.0],[-225.0],[tau.x],[tau.y],[tau.z]]
        #wrench_cmd = [[f.x],[f.y],[f.z],[tau.x],[tau.y],[tau.z]]
        print("q: ", q)
        #q = [[0.0],[0.0],[0.0],[0.0],[0.0],[0.0]]
        #print "not: ", not abs(f_cmd.z) < 1e-3

        if q and f and prs_cur and not abs(f_cmd.z) < 1e-3:
        #if q and f:
            #find pressures with abstract method that is defined by the controller you are using
            prs_delta = self.find_pressures(wrench, wrench_cmd, q)

            prs = []

            print("length of prs_delta: ", len(prs_delta))

            for i in range(len(prs_delta)):

                p0 = prs_cur[2*i] + prs_delta[i][0]/2.0
                p1 = prs_cur[2*i+1] - prs_delta[i][0]/2.0
                pdiff = p0 - p1

                if p0 > self.prs_max and p1 < self.prs_min:
                    p0 = self.prs_max
                    p1 = self.prs_min

                elif p1 > self.prs_max and p0 < self.prs_min:
                    p0 = self.prs_min
                    p1 = self.prs_max

                else:
                    if p0 > self.prs_max:
                        p0 = self.prs_max
                        p1 = p0 - pdiff
                        if p1 < self.prs_min:
                            p1 = self.prs_min

                    elif p1 > self.prs_max:
                        p1 = self.prs_max
                        p0 = p1 + pdiff
                        if p0 < self.prs_min:
                            p0 = self.prs_min

                    elif p0 < self.prs_min:
                        p0 = self.prs_min
                        p1 = self.prs_min - pdiff
                        if p1 > self.prs_max:
                            p1 = self.prs_max

                    elif p1 < self.prs_min:
                        p1 = self.prs_min
                        p0 = self.prs_min + pdiff
                        if p0 > self.prs_max:
                            p0 = self.prs_max

                prs.append(p0)
                prs.append(p1)
                

            self.send_pressure_command(prs, self.timestep)

            print("pressures: ", prs)
            print("")


    @abstractmethod
    def find_pressures(self, f, f_cmd, q):
        pass



