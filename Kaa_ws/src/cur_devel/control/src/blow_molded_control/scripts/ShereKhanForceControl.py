#!/usr/bin/env python
import blow_molded_force_controller as fc
from kinematics import arm_tools as at
import numpy as np
import rospy
import time
from tools import kin_tools as kt

class ShereKhanForceControl(fc.BlowMoldedForceController):

    def __init__(self, arm, k_torque_to_pressure):

        fc.BlowMoldedForceController.__init__(self)

        self.arm = arm
        self.k_t_p = k_torque_to_pressure
        self.hom_cmd = np.ones(4)
        self.integrator = np.array([[0.0],[0.0],[0.0],[0.0]])
        self.ki = 0.1

    def find_pressures(self, f, f_cmd, q):

        #See Vallan's thesis section 4.1.3 for the explanation of this math

        quadruped.setJointAngs(np.array(q))
        quadruped.calcJac()
        Jac = quadruped.getJacHybrid()[0:3]
        Jacbody = quadruped.getJacBody()[4:5]
        g_world_foot = quadruped.g_world_tool()
        # print "Jac: ", Jac
        # print "Jacbody: ", Jacbody
        # print "Jacbody: ", quadruped.getJacBody()
        Jac_Real_Hybrid = np.array([list(Jac[0]), list(Jac[1]), list(Jac[2]), list(Jacbody[0])])
        print("Jac real hybrid: ", Jac_Real_Hybrid)
        # print "g_world_tool: ", quadruped.g_world_tool()

       #k = np.eye(4)
       #k[0][0] = 136.0
       #k[1][1] = 136.0
       #k[2][2] = 27.0
       #k[3][3] = 27.0

       #f_side = np.array([[0.0], [0.0], [-200.0], [0.0], [0.0],[0.0]])
       #q_array = np.array([[q[0]],[q[1]],[q[2]],[q[3]]])
       #pressures_diff_side = np.dot(self.k_t_p,np.dot(Jac.T,f_side) + np.dot(k,q_array))
       #print "q: ", q_array
       #print "kq: ", np.dot(k,q_array)
       #print "pressures_diff_side: ", pressures_diff_side


        print("f: ", f)
        print("f_cmd: ", f_cmd)
        # print "self.hom_cmd: ", self.hom_cmd
        # print "g_world_foot: ", g_world_foot

        hom_error = np.dot(self.hom_cmd, np.linalg.inv(g_world_foot))
        error_rot = -1.0*kt.hom2ax_vec(hom_error)[0][1]
        # print "error_rot: ", error_rot


        error = np.array(f_cmd[0:3]) - np.array(f[0:3])
        # print "error: ", error
        error_in_leg_frame = np.array([error[0],-1.0*error[1],-1.0*error[2]])
        # print "error leg frame: ", error_in_leg_frame
        error_hybrid = np.array([list(error_in_leg_frame[0]), list(error_in_leg_frame[1]), list(error_in_leg_frame[2]), [500.0*error_rot]])
        # print "error_hybrid: ", error_hybrid
        press_delta = np.dot(0.25*self.k_t_p,np.dot(Jac.T,error_in_leg_frame))
        # print "press_delta: ", press_delta
        #error_hybrid = np.array([[0.0],[0.0],[0.0],[10.0]])
        press_delta_hybrid = np.dot(0.25*self.k_t_p,np.dot(Jac_Real_Hybrid.T,error_hybrid))
        # print "press_delta_hybrid: ", press_delta_hybrid

        self.integrator += self.timestep*error_hybrid

        print("with integrator: ", press_delta_hybrid + self.ki*self.integrator)
        press_delta_hybrid =  press_delta_hybrid + self.ki*self.integrator

        for i in range(4):
            if press_delta_hybrid[i] < 0:
                press_delta_hybrid[i] = 0.01

#       for i in range(len(q)):
#           pressures.append(self.p_nom + pdiff[i])
#           pressures.append(self.p_nom - pdiff[i])

        return press_delta_hybrid

if __name__ == "__main__":

    #define leg
    density = 4.980 #4.980 was measured empirically of the aluminium used in the adjustable links made for the leg
    min_lengths = [0.185, 0.263, 0.143, 0.15]
    R = 0.18 #radius of shoulder mount
    d = 0.145 #offset to beginning of joint from shoulder mount rotation center

    #robot 66
    # lengths = [0.185, 0.263, 0.143, 0.15]
    # angles = [1.199,0.0,90.0*np.pi/180.0,0.0]
    # x_rotation = -1.2146
    # base_hom = kt.ax_vec2homog(np.array([x_rotation, 0, 0]), np.array([0.0, R + d*np.sin(x_rotation), d*np.cos(x_rotation)]))
    # base_width = 1.1169
    # base_length = 0.4537
    # mount_angle = 0.826

    #robot 4
    lengths = [0.207, 0.371, 0.143, 0.234]
    angles = [57.0*np.pi/180.0,0.0,63.0*np.pi/180.0,0.0]
    x_rotation = -95.0*np.pi/180.0
    base_hom = kt.ax_vec2homog(np.array([x_rotation, 0, 0]), np.array([0.0, R + d*np.sin(x_rotation), d*np.cos(x_rotation)]))
    base_width = 1.2917
    base_length = 1.1814
    mount_angle = 0.895

    quadruped = at.quadruped(lengths,angles,base_hom,base_width,base_length,mount_angle,0.205, density, min_lengths)

    k_t_p = np.eye(4)
    k_t_p[0][0] = 2160.0
    k_t_p[1][1] = 2160.0
    k_t_p[2][2] = 6612.0
    k_t_p[3][3] = 6612.0

    g_cog_leg = quadruped.getGCoGLeg1()[0:3]
    g_leg_cog = g_cog_leg.T[0:3]
    print("Monkeys please", g_leg_cog)
    print(np.dot(g_leg_cog, np.array([[-1.0],[0.0],[0.0]])))

    g_cog_leg = quadruped.getGCoGLeg2()[0:3]
    g_leg_cog = g_cog_leg.T[0:3]
    print("Monkeys please", g_leg_cog)
    print(np.dot(g_leg_cog, np.array([[-1.0],[0.0],[0.0]])))

    g_cog_leg = quadruped.getGCoGLeg3()[0:3]
    g_leg_cog = g_cog_leg.T[0:3]
    print("Monkeys please", g_leg_cog)
    print(np.dot(g_leg_cog, np.array([[-1.0],[0.0],[0.0]])))

    g_cog_leg = quadruped.getGCoGLeg4()[0:3]
    g_leg_cog = g_cog_leg.T[0:3]
    print("Monkeys please", g_leg_cog)
    print(np.dot(g_leg_cog, np.array([[-1.0],[0.0],[0.0]])))


    rospy.init_node('Shere_Force', anonymous=True)

    controller = ShereKhanForceControl(quadruped, k_t_p)

    time.sleep(0.5)

    controller.joint_sub.getData()
    q = controller.joint_sub.q_est
    quadruped.setJointAngs(np.array(q))
    quadruped.calcFK()
    controller.hom_cmd = quadruped.g_world_tool()

    rate = rospy.Rate(controller.rate)

    while not rospy.is_shutdown():

        controller.run()

        rate.sleep()

