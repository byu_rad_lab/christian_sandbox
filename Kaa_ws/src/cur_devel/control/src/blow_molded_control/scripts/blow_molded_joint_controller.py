#!/usr/bin/env python
from abc import ABCMeta, abstractmethod
from subscribers import system_minimal_msg_subscriber as smm_sub
import rospy
from ros_srv.srv import uint32_srv #SetRobotCtrlMode
from ros_srv.srv import RequestMoveSrv #RequestMoveLinear

class BlowMoldedJointController(metaclass=ABCMeta):
    def __init__(self):

        #self.cmd_topic = cmd_topic
        self.torque_service_name = "/gazebo/apply_joint_effort"

        self.rate = 50.0
        self.timestep = 1.0/self.rate

        self.joint_sub = smm_sub.SystemMinimalMsgSubscriber('/shere_khan_uv/estimation_minimal')
        self.joint_cmd_sub = smm_sub.SystemMinimalMsgSubscriber("atheris/joint_angle_cmds/")

        # set to pressure control through service
        self.setCtrl = rospy.ServiceProxy('/atheris/SetRobotCtrlMode', uint32_srv, persistent=True)
        try:
            worked = self.setCtrl(1)
        except rospy.ServiceException as e:
            print('Error :', e)

        self.req_move_sin_pressure = rospy.ServiceProxy('/atheris/RequestMoveSinusoidalPressure', RequestMoveSrv, persistent=True)


    def send_pressure_command(self,pressures,movetime):
        print(movetime," second sinusoidal pressure move to: ",pressures)
        result = self.req_move_sin_pressure.call(movetime, pressures)

    def run(self):

        #get estimate data
        self.joint_sub.getData()

        q = self.joint_sub.q_est
        qd = self.joint_sub.qd_est

        #get commanded data
        #q_cmd = [0.0]*len(q)
        #q_cmd = [0.593412, -0.942478, 0.802851, -0.174533]
        #q_cmd = [-0.174533, -0.314159, 0.802851, 0.10472]
        #q_cmd = [-0.802851, -0.0349066, 0.733038, -0.174533]
        #q_cmd = [-1.36136, 0.663225, 0.942478, 0.383972]
        #q_cmd = [0.349066, -0.802851, 1.29154, 0.733038]
        #q_cmd = [-0.663225, -0.174533, 1.15192, 0.802851]
        #q_cmd = [0.244346, -0.663225, 0.593412, -0.942478]
        #q_cmd = [0.663225, -0.872665, 0.593412, -1.0825]
        q_cmd = [-0.593412, -0.523599, 1.22173, 0.733038]
        #q_cmd = [0.0]*len(q)
        #q_cmd = [-0.593412, -0.523599, 0.0, 0.0]
        qd_cmd = [0.0]*len(q)

        if q:
            #find pressures with abstract method that is defined by the controller you are using
            pressures = self.find_pressures(q, q_cmd, qd, qd_cmd)

            self.send_pressure_command(pressures, self.timestep)

            print("pressures: ", pressures)


    @abstractmethod
    def find_pressures(self, q, q_cmd, qd, qd_cmd):
        pass



