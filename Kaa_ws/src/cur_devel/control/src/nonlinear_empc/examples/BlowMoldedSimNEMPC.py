#!/usr/bin/env python
import numpy as np
import math
import time
from copy import deepcopy
from rad_models.BellowsArm2 import BellowsArm
# from rad_models.LearnedBellowsArm import LearnedBellowsArm
from rad_models.LearnedBellowsArmTRT import LearnedBellowsArm
from nonlinear_empc.NonlinearEMPC import NonlinearEMPC
import matplotlib.pyplot as plt

Q = 1.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1.0])        
Qf = 1.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1.0])
R = 0.0001*np.diag([1,1,1,1,1,1,1,1,1,1,1,1.0])

def myCostFcn(x,u,xgoal,ugoal,final_timestep=False):
    if final_timestep:
        Qx = Qf.dot(x-xgoal)**2.0
        cost = np.sum(Qx,axis=0)
    else:
        Qx = Q.dot(x-xgoal)**2.0
        Ru = R.dot(u-ugoal)**2.0
        cost = np.sum(Qx,axis=0) + np.sum(Ru,axis=0)
    return cost

if __name__ == '__main__':

    sys = BellowsArm()
    learnedSys = LearnedBellowsArm()
    
    numStates = sys.numStates
    numInputs = sys.numInputs

    horizon = 50
    dt = .02

    pmin = 8
    pmax = 400
    umin = [pmin]*12
    umax = [pmax]*12

    numSims = 100
    mpc = NonlinearEMPC(learnedSys.forward_simulate_dt,
                        #sys.forward_simulate_dt,
                        myCostFcn,
                        numStates,
                        numInputs,
                        umin,
                        umax,
                        horizon,
                        numSims,
                        numKnotPoints=2)
    
    x = np.array([100.0,100,100,100,100,100,100,100,100,100,100,100, 0,0,0,0,0,0, 0.0,0.0,0.0,0.0,0.0,0.0]).reshape(24,1)
    u = np.zeros([sys.numInputs,1])
    xgoal = np.array([0.0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0, 1,1,1,1,1,1]).reshape(24,1)

    sim_length = 500
    x_hist = np.zeros([sys.numStates,sim_length])
    u_hist = np.zeros([sys.numInputs,sim_length])
    solve_times = []

    for i in range(0,sim_length):
        noise = np.linalg.norm(xgoal[12:24]-x[12:24])*5.0
        start = time.time()
        u = mpc.solve_for_next_u(x,xgoal,ulast=u.flatten(),ugoal=u*0,mutation_noise=noise).reshape(12,1)
        solve_times.append(time.time()-start)
        print(time.time()-start)
        x = sys.forward_simulate_dt(x,u,dt)
        # x = learnedSys.forward_simulate_dt(x,u,dt)
        print(i, x[18:24].T)

        u_hist[:,i] = u.flatten()
        x_hist[:,i] = x.flatten()

    print("Average solve time: ",np.mean(solve_times))
    print("Median solve time: ",np.median(solve_times))
    print("Std. Dev.: ",np.std(solve_times))

    plt.figure(1)
    plt.ion()
    plt.plot(x_hist[0:12,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Pressure (KPa)')
    plt.title('Pressure')
    plt.show()
    
    plt.figure(2)
    plt.ion()
    plt.plot(x_hist[12:18,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Velocity (rad/s)')
    plt.title('Velocity')
    plt.show()

    plt.figure(4)
    plt.ion()
    plt.plot(x_hist[18:24,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Theta (rad)')
    plt.title('Position')
    plt.show()
        
    plt.figure(5)
    plt.plot(u_hist.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Torque (Nm)')
    plt.title('Input')

    plt.pause(1000)
