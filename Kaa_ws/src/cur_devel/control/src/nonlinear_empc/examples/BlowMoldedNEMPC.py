#!/usr/bin/env python
import numpy as np
import math
import rospy
import time
from copy import deepcopy
from rad_models.BellowsArm2 import BellowsArm
from rad_models.LearnedBellowsArmTRT import LearnedBellowsArm
from nonlinear_empc.NonlinearEMPC import NonlinearEMPC

from ros_srv.srv import uint32_srv #SetRobotCtrlMode
from ros_srv.srv import RequestMoveSrv #RequestMoveSinusoidal
from ros_msg.msg import SystemMinimalMsg

Q = 1.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,0.0,0.0,0,0,0,0,1,1,1,1,1,1.0])        
Qf = 1.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.0,0.0,1,1,1,1,1,1.0])
# Qf = 1.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1.0])
R = 0.00001*np.diag([3,3,3,3,3,3,3,3,1,1,1,1.0])
# R = 0.00005*np.diag([1,1,1,1,1,1,1,1,1,1,1,1.0])

def myCostFcn(x,u,xgoal,ugoal,final_timestep=False):
    cost = np.zeros(u.shape)
    if final_timestep:
        Qx = np.abs(Qf.dot(x-xgoal)**2.0)
        cost = np.sum(Qx,axis=0)
    else:
        Qx = np.abs(Q.dot(x-xgoal)**2.0)
        Ru = np.abs(R.dot(u-ugoal))
        cost = np.sum(Qx,axis=0) + np.sum(Ru,axis=0)
    return cost

class RealBellowsArm():

    def __init__(self):
        # Joint states and inputs
        self.q_goal = 0.0*np.ones([6,1])
        self.q = 0.0*np.ones([6,1])
        self.qd = 0.0*np.ones([6,1])
        self.pressures = []
        self.pscale = 1000.0

        pmin = 8
        pmax = 400
        umin = [pmin]*12
        umax = [pmax]*12

        # subscribers and service calls
        self.robot_state_sub = rospy.Subscriber('/atheris/system_minimal',SystemMinimalMsg,self.states_cb,queue_size=None)

        self.got_initial_state = False
        rate = rospy.Rate(10)
        while not self.got_initial_state:
            rate.sleep()
            print("Waiting for initial robot state")            
        
        self.setCtrl = rospy.ServiceProxy('/atheris/SetRobotCtrlMode', uint32_srv, persistent=True)
        try:
            worked = self.setCtrl(1) #1 for pressure 0 for position
        except rospy.ServiceException as e:
            print('Error :', e)
            
        self.req_move_sin_pressure = rospy.ServiceProxy('/atheris/RequestMoveSinusoidalPressure', RequestMoveSrv, persistent=True)
            

    def states_cb(self,msg):
        # q0 = np.array(deepcopy(msg.joints_est[0].q))
        # q1 = np.array(deepcopy(msg.joints_est[1].q))
        # q2 = np.array(deepcopy(msg.joints_est[2].q))
        q0 = np.array((msg.joints_est[0].q))
        q1 = np.array((msg.joints_est[1].q))
        q2 = np.array((msg.joints_est[2].q))
        q = np.reshape(np.hstack([q0,q1,q2]),[6,1])
        self.q = q

        # qd0 = np.array(deepcopy(msg.joints_est[0].qd))
        # qd1 = np.array(deepcopy(msg.joints_est[1].qd))
        # qd2 = np.array(deepcopy(msg.joints_est[2].qd))
        qd0 = np.array((msg.joints_est[0].qd))
        qd1 = np.array((msg.joints_est[1].qd))
        qd2 = np.array((msg.joints_est[2].qd))
        qd = np.reshape(np.hstack([qd0,qd1,qd2]),[6,1])
        self.qd = qd

        # p0 = np.array(deepcopy(msg.joints_est[0].prs_bel))
        # p1 = np.array(deepcopy(msg.joints_est[1].prs_bel))
        # p2 = np.array(deepcopy(msg.joints_est[2].prs_bel))
        p0 = np.array((msg.joints_est[0].prs_bel))
        p1 = np.array((msg.joints_est[1].prs_bel))
        p2 = np.array((msg.joints_est[2].prs_bel))
        p = np.reshape(np.hstack([p0,p1,p2]),[12,1])/self.pscale
        self.pressures = p
        
        self.got_initial_state=True
        
    def send_pressure_command(self,pressures,movetime):
        result = self.req_move_sin_pressure.call(movetime, self.pscale*pressures)

if __name__ == '__main__':

    rospy.init_node('mpc_control', anonymous=True)
    sys = RealBellowsArm()
    
    learnedSys = LearnedBellowsArm()
    
    numStates = learnedSys.numStates
    numInputs = learnedSys.numInputs

    horizon = 40
    dt = .02

    pmin = 8
    pmax = 400
    umin = [pmin]*12
    umax = [pmax]*12

    numSims = 100
    mpc = NonlinearEMPC(learnedSys.forward_simulate_dt,
                        myCostFcn,
                        numStates,
                        numInputs,
                        umin,
                        umax,
                        horizon,
                        numSims,
                        numKnotPoints=3)

    sys.send_pressure_command(100*np.ones(12),5)
    time.sleep(5.0)
    u = sys.pressures

    start = time.time()
    t_settle = 15.0

    ki = np.diag([.03,.03,.01,.01,.01,.01])*0
    integral_term = np.zeros([6,1])

    x = np.vstack([sys.pressures,sys.qd,sys.q])
    noise = 5.0
    xgoal = np.vstack([sys.pressures,sys.qd*0,sys.q])
    for i in range(0,200):
        u = mpc.solve_for_next_u(x,xgoal,ulast=u.flatten(),ugoal=sys.pressures*0,mutation_noise=noise).reshape(12,1)
    
    while not rospy.is_shutdown():
        if time.time() - start < t_settle:
            sys.q_goal = np.array([0.0,    0.0,     0.0,    0.0,  0.0,   0.0]).reshape(6,1)
        elif time.time() - start > t_settle and time.time() - start < t_settle*2:
            sys.q_goal = np.array([0.5,   0.5,   -.5,   -0.5,  0.5,  0.5]).reshape(6,1)            
        elif time.time() - start > t_settle and time.time() - start < t_settle*3:
            sys.q_goal = np.array([-0.5,     0.5,   0.5,   -0.5,   -0.5, 0.5]).reshape(6,1)
        elif time.time() - start > t_settle*2 and time.time() - start < t_settle*4:
            sys.q_goal = np.array([0.5,    -0.5,     -0.5,    0.5,  0.5,   -0.5]).reshape(6,1)
        elif time.time() - start > t_settle*3 and time.time() - start < t_settle*5:
            sys.q_goal = np.array([0.0,   0.0,   0.0,   0.0,  0.0,  0.0]).reshape(6,1)

        error = sys.q_goal - sys.q
        # for i in xrange(0,6):
        #     if np.linalg.norm(error[i]) >= .40:
        #         integral_term[i] = 0
        
        if np.linalg.norm(error) > 1.0:
            integral_term = 0
            
        if np.linalg.norm(sys.qd) < .1:
            integral_term += np.dot(ki,error)
            
        print("qd norm: ",np.linalg.norm(sys.qd))
        q_cmd = sys.q_goal + integral_term
        
        x = np.vstack([sys.pressures,sys.qd,sys.q])
        noise = np.linalg.norm(sys.qd)*0.0 + np.linalg.norm(sys.q_goal-sys.q)*1.0000
        xgoal = np.vstack([sys.pressures,sys.qd*0,q_cmd])

        begin = time.time()
        # print '\nulast: ',u.flatten()
        u = mpc.solve_for_next_u(x,xgoal,ulast=u.flatten(),ugoal=sys.pressures,mutation_noise=noise).reshape(12,1)
        print("solve time: ",time.time()-begin)        
        
        u = np.reshape(u,[12,1])
        # print 'u: ',u.T
        print('\nq: ',sys.q.T)
        print('q_goal: ',sys.q_goal.T)
        # print 'q_cmd: ',q_cmd.T
        # print 'error: ',np.linalg.norm(error)
        # print 'noise: ',noise

        sys.send_pressure_command(u,.01)
        # time.sleep(.5)
        # import sys as system
        # system.exit()
        

        
