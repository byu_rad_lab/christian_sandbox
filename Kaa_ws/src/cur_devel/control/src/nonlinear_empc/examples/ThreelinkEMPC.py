import numpy as np
import time
import matplotlib.pyplot as plt
from nonlinear_empc.NonlinearEMPC import NonlinearEMPC
from rad_models.LearnedThreelink import *
from rad_models.Threelink import *

if __name__=='__main__':

    learnedSys = LearnedThreelink()
    sys = Threelink()
    numStates = sys.numStates
    numInputs = sys.numInputs
    
    Q = 1.0*np.diag([0,0,0,3.0,2.0,1.0])
    Qf = 10.0*np.diag([0.0,0.0,0.0,3.0,2.0,1.0])
    # Q = 1.0*np.diag([0,0,0,1.0,1.0,1.0])
    # Qf = 10.0*np.diag([0,0,0,1.0,1.0,1.0])
    R = 0.0001*np.diag([1.0,1.0,1.0])

    def myCostFcn(x,u,xgoal,ugoal,final_timestep=False):
        cost = np.zeros(u.shape)
        if final_timestep:
            Qx = np.abs(Qf.dot(x-xgoal)**2.0)
            cost = np.sum(Qx,axis=0)
        else:
            Qx = np.abs(Q.dot(x-xgoal)**2.0)
            Ru = np.abs(R.dot(u-ugoal))
            cost = np.sum(Qx,axis=0) + np.sum(Ru,axis=0)
        return cost

    controller = NonlinearEMPC(learnedSys.forward_simulate_dt,
                               myCostFcn,
                               numStates,
                               numInputs,
                               horizon = 25,
                               numKnotPoints = 3,
                               numSims = 500,
                               numParents = 10,
                               numStrangers = 0,
                               umin=[-sys.uMax,-sys.uMax,-sys.uMax],
                               umax=[sys.uMax,sys.uMax,sys.uMax])

    x = np.array([0,0,0,np.pi,0,0.0]).reshape(6,1)
    u = np.zeros([3,1])
    xgoal = np.array([0,0,0,0,0,0.]).reshape(6,1)

    fig = plt.figure(10)
    plt.ion()

    simulation_horizon = 300
    x_hist = np.zeros([sys.numStates,simulation_horizon])
    u_hist = np.zeros([sys.numInputs,simulation_horizon])
    solve_time_hist = []
    sim_cost = 0

    for i in range(0,simulation_horizon):
        # noise = np.linalg.norm(xgoal-x)*1.0 + 0.2
        # noise = np.sum(np.abs(xgoal-x))*1.5 + 1.3 # I should use this
        noise = np.sum(np.abs(xgoal-x))*.3 + .1
        # noise = np.linalg.norm(xgoal-x)*1.0
        # noise = np.clip(noise,0,3)
        print(noise)
        start = time.time()
        u = controller.solve_for_next_u(x,xgoal,ulast=u.flatten(),ugoal=x[3:6,:]*0,mutation_noise=noise)
        solve_time_hist.append(time.time()-start)
        x = sys.forward_simulate_dt(x,u,.01)

        plt.figure(10)
        sys.visualize(x,u)

        u_hist[:,i] = u.flatten()
        x_hist[:,i] = x.flatten()

        sim_cost += (x-xgoal).T.dot(Q).dot(x-xgoal) + u.T.dot(R).dot(u)
        print("total cost: ",sim_cost)
        
    plt.figure(4)
    plt.plot(x_hist[3:6].T)
    plt.xlabel('Timestep')
    plt.ylabel('Theta (rad)')
    plt.title('Nonlinear EMPC Position')
    plt.show()
        
    plt.figure(5)
    plt.plot(u_hist.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Torque (Nm)')
    plt.title('Nonlinear EMPC Input')

    data_dict = {'x':x_hist,
                 'u':u_hist,
                 'solve_times':solve_time_hist}
    import scipy.io as sio
    sio.savemat('ThreelinkEMPCTrajectoryV2.mat',data_dict)
    
    plt.pause(1000)
