import numpy as np
import time
import matplotlib.pyplot as plt
from nonlinear_empc.NonlinearEMPC import NonlinearEMPC
from rad_models.LearnedInvertedPendulum import * #this is a nonlinear pytorch model right now 
from rad_models.InvertedPendulum import *  #just importing model for simulation, wouldn't normally do this for real system 

if __name__=='__main__':

    learnedSys = LearnedInvertedPendulum()
    sys = InvertedPendulum()
    numStates = sys.numStates
    numInputs = sys.numInputs
    
    Q = 1.0*np.diag([0,1.0])
    Qf = 5.0*np.diag([0,1.0])
    R = .0*np.diag([1.0])

    def myCostFcn(x,u,xgoal,ugoal,final_timestep=False):
        cost = np.zeros(u.shape)
        if final_timestep:
            Qx = np.abs(Qf.dot(x-xgoal))
            for i in range(u.shape[0]):
                cost[i,:] = Qx[i+u.shape[0]]
        else:
            Qx = np.abs(Q.dot(x-xgoal))
            Ru = np.abs(R.dot(u-ugoal))
            for i in range(u.shape[0]):
                cost[i,:] = Ru[i] + Qx[i+u.shape[0]]
        return cost

    controller = NonlinearEMPC(learnedSys.forward_simulate_dt,  #this is the neural net model that is used for forward simulation
                               myCostFcn,
                               numStates,
                               numInputs,
                               horizon = 50,
                               numSims = 100,
                               numParents = 10,    #this plus numSims tells us how many mates for each child
                               umin=[-sys.uMax]*sys.numInputs,
                               umax=[sys.uMax]*sys.numInputs)

    x = np.array([0,-np.pi]).reshape(sys.numStates,1)
    u = np.zeros([sys.numInputs,1])
    xgoal = np.zeros([sys.numInputs,1])

    fig = plt.figure(10)
    plt.ion()

    plt.pause(5)    

    horizon = 600
    x_hist = np.zeros([sys.numStates,horizon])
    u_hist = np.zeros([sys.numInputs,horizon])

    #simulate for time length of "horizon", not a control horizon or anything
    for i in range(0,horizon):
        start = time.time()
        u = controller.solve_for_next_u(x,xgoal,ulast=u,ugoal=np.zeros([sys.numInputs,1]))
        print("solve time: ",time.time()-start)
        x = sys.forward_simulate_dt(x,u,.01)

        plt.figure(10)
        sys.visualize(x)
        
        u_hist[:,i] = u.flatten()
        x_hist[:,i] = x.flatten()

    plt.figure(4)
    plt.plot(x_hist[1,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Theta (rad)')
    plt.title('Nonlinear EMPC Position')
    plt.show()
        
    plt.figure(5)
    plt.plot(u_hist.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Torque (Nm)')
    plt.title('Nonlinear EMPC Input')

    data_dict = {'x':x_hist,
                 'u':u_hist}
    import scipy.io as sio
    sio.savemat('InvertedPendulumEMPCTrajectory.mat',data_dict)

    
    plt.pause(1000)
        
