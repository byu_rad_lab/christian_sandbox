from rad_models.KneedWalker import *
from NonlinearEMPC import NonlinearEMPC
import numpy as np



if __name__=='__main__':

    import time

    walker = KneedWalker()
    numStates = 14
    numInputs = 7
    Q = 10.0
    R = 0.0
    xgoal = np.array([1.0,0,0,0,0,0,0,0,2.5,0,0,0,0,0]).reshape(14,1)
    ugoal = np.array([0,0,0,0,0,0,0.0]).reshape(7,1)
    

    def myCostFcn(x,u,xgoal,ugoal):
        # cost = Q*(100.0*np.abs(x[0,:]-xgoal[0]) + np.abs(x[8,:]-xgoal[8]))
        cost = Q*(100.0*np.abs(x[0,:]-xgoal[0]) + 1.0*np.abs(x[8,:]-xgoal[8]))        
        return cost

    tauMax = 1000.0
    umins = np.array([0,0,0,-tauMax,-tauMax,-tauMax,-tauMax])
    umaxes = np.array([0,0,0,tauMax,tauMax,tauMax,tauMax])
    dt = .01    
    
    controller = NonlinearEMPC(walker.forward_simulate_dt,myCostFcn,numStates,numInputs,umin=umins,umax=umaxes,horizon=100,numSims=1000,dt=dt,numKnotPoints=4)
    

    
    fig = plt.gca()
    plt.ion()
    plt.hlines(0,-15,15)
    b = .0001
    
    q = np.array([0,2.5,0.0,0.5,0,-0.5,0])
    qd = np.array([0.0,0,0,0,0,0,0.0])
    x = np.hstack([qd,q]).reshape(14,1)
    u = np.array([0.0,0,0,0,0,0,0])    

    for i in range(0,10000):
        start = time.time()
        u = controller.solve_for_next_u(x,xgoal,ugoal)
        # u[3] = 500.0*(-1.0*np.sin(i*.005+np.pi/4)-x[3+7]) - 100.0*x[3]
        # u[4] = 400.0*(-1.0*np.sin(i*.005+np.pi/4)-x[4+7]) - 100.0*x[4]
        # u[5] = 500.0*(0.75*np.sin(i*.005)-x[5+7]) - 100.0*x[5]
        # u[6] = 400.0*(0.75*np.sin(i*.005)-x[6+7]) - 100.0*x[6]
        
        print("solve time: ",time.time()-start)
        x = walker.forward_simulate_dt(x,u,dt)

        if i%1==0:
            plt.clf()
            plt.hlines(0,-15,15,linewidth=1.0)
            draw_robot(x[7:14,0],1.0)
            plt.axis(xmin=-12,xmax=12,ymin=-10,ymax=10)
            plt.axis(aspect='equal')
            plt.pause(.000001)
            
            print("x: ",x[8,0],"\nu: ",u[3:7])
        

