import numpy as np
import time
import matplotlib.pyplot as plt

class NonlinearEMPC():

    def __init__(self,
                 model,
                 cost_function,
                 numStates,
                 numInputs,
                 umin,
                 umax,
                 horizon=50,
                 numSims=500,
                 numParents=10,
                 numStrangers=0,
                 numKnotPoints=2,
                 dt=.01):

        self.model = model
        self.cost_function = cost_function
        self.n = numStates
        self.m = numInputs
        self.umin = np.array(umin).reshape(numInputs,1)
        self.umax = np.array(umax).reshape(numInputs,1)
        self.uRange = (np.array(umax)-np.array(umin)).reshape(numInputs,1)
        self.horizon = horizon
        self.numSims = numSims
        self.dt = dt
        self.numParents = numParents
        self.numStrangers = numStrangers        
        self.numKnotPoints = numKnotPoints
        self.segmentLength = float(self.horizon)/(self.numKnotPoints-1)
        self.U = np.zeros([self.numKnotPoints,self.m,self.numSims])
        self.costs = np.zeros([1,self.numSims])        
        self.X = np.zeros([self.horizon,self.n,self.numSims])
        self.warmStart = False

    def get_random_u_trajectories(self,numSims):
        trajectories = np.random.rand(self.numKnotPoints,self.m,numSims)*self.uRange + self.umin
        return trajectories

    def mate_parents(self,parent1,parent2):
        combinedParents = np.stack([parent1,parent2],axis=2)
        child = np.zeros(parent1.shape)
        coin_flips = np.random.randint(0,2,(self.m,self.numKnotPoints))
        for i in range(0,self.m):
            for j in range(0,self.numKnotPoints):
                child[j,i] = combinedParents[j,i,coin_flips[i,j]]
        return child
    
    def mate_and_mutate_parents(self,parents,x,xgoal,ulast,mutation_noise):
    
        #Mat has done tournament selection, and it seemed to help the speed a lot because of while loops
        
        self.U[:,:,0:self.numParents] = parents

        # Introduce random trajectories to discourage stagnation
        self.U[:,:,self.numParents:self.numParents+self.numStrangers] = self.get_random_u_trajectories(self.numStrangers)
        
        i = self.numParents + self.numStrangers
        # i = 0

        mates = np.random.randint(0,self.numParents,(2,self.numSims))
        while i<self.numSims:
            mate1 = mates[0,i]
            mate2 = mates[1,i]
            
            while mate1==mate2:
                mate2 = np.random.choice(range(0,self.numParents))

            self.U[:,:,i] = self.mate_parents(parents[:,:,mate1],parents[:,:,mate2])
            i += 1

        # Mutate
        self.U = self.U + np.random.randn(self.numKnotPoints,self.m,self.numSims)*mutation_noise
        self.U[self.U>self.umax[0]] = self.umax[0]
        self.U[self.U<self.umin[0]] = self.umin[0]
        
        return self.U

    def get_us_from_U(self,U,i,horizon,numKnotPoints):
        knot = int(i/self.segmentLength)
        ui = U[knot,:,:]*(1.0-float(i-knot)/self.segmentLength) + U[knot+1,:,:]*(float(i-knot)/self.segmentLength)        
        ui[ui>self.umax[0]] = self.umax[0]
        ui[ui<self.umin[0]] = self.umin[0]
        return ui

    def get_costs_from_trajectories(self,x0,xgoal,ugoal,U):
        self.costs = np.zeros([1,self.numSims])        
        self.X[0,:,:] = x0

        for i in range(0,self.horizon-1):
            ui = self.get_us_from_U(U,i,self.horizon,self.numKnotPoints)
            self.X[i+1,:,:] = self.model(self.X[i,:,:],ui,self.dt)
            self.costs += self.cost_function(self.X[i,:,:],ui,xgoal,ugoal)
        
        FinalCosts = self.cost_function(self.X[self.horizon-1,:,:],ui,xgoal,ugoal,final_timestep=True)
        self.costs += FinalCosts

        return self.costs

    def select_parents(self,U,costs):
        indices = costs.argsort().flatten()
        self.U_parents = U[:,:,indices[0:self.numParents]]
        self.X_parents = self.X[:,:,indices[0:self.numParents]]
        return self.U_parents

    def get_next_u_from_parents(self,U_parents):
        # This is taking the 2nd point (based on old restriction that 1st knot point be last input)
        # next_u = U_parents[0,:,0]*(1.0-1.0/self.segmentLength)+U_parents[1,:,0]*(1.0/self.segmentLength)
        
        next_u = U_parents[0,:,0]        
        next_u[next_u>self.umax[0]] = self.umax[0]
        next_u[next_u<self.umin[0]] = self.umin[0]
        return next_u

    

    def solve_for_next_u(self,x0,xgoal,ulast,ugoal,mutation_noise):
        if self.warmStart == False:
            self.U = self.get_random_u_trajectories(self.numSims)
            for i in range(0,self.numKnotPoints):
                self.U[i,:,0] = ulast
            self.warmStart = True
        else:
            self.U = self.mate_and_mutate_parents(self.U_parents,x0,xgoal,ulast,mutation_noise)

        # # This constrains the first point in the u trajectory to be the last commanded u
        # for i in xrange(0,self.numSims):
        #     self.U[0,:,i] = ulast
            
        self.costs = self.get_costs_from_trajectories(x0,xgoal,ugoal,self.U)
        self.U_parents = self.select_parents(self.U,self.costs)
        next_u = self.get_next_u_from_parents(self.U_parents)

        return next_u


if __name__=='__main__':

    from LearnedInvertedPendulum import *  #learned model
    from InvertedPendulum import *         #real platform or model based on physics

    import time

    learnedSys = LearnedInvertedPendulum(use_gpu=False) #would be faster if said "use_gpu=True"
    sys = InvertedPendulum(mass=.2)    
    numStates = sys.numStates
    numInputs = sys.numInputs
    Q = 1.0*np.diag([0,1.0])
    Qf = 100.0*np.diag([0,1.0])
    R = 0.0*np.diag([1.0])

    # This cost function must take in vectors of shapes:
    # x: [numStates,numSims]
    # u: [numInputs,numSims]
    # and return costs in the shape:
    # cost: [1,numSims]
    def myCostFcn(x,u,xgoal,ugoal,final_timestep=False):
        cost = np.zeros(u.shape)
        if final_timestep:
            Qx = np.abs(Qf.dot(x-xgoal))
            for i in range(u.shape[0]):
                cost[i,:] = Qx[i+u.shape[0]]
        else:
            Qx = np.abs(Q.dot(x-xgoal))
            Ru = np.abs(R.dot(u-ugoal))
            for i in range(u.shape[0]):
                cost[i,:] = Ru[i] + Qx[i+u.shape[0]]
        return cost

    controller = NonlinearEMPC(learnedSys.forward_simulate_dt,
                               myCostFcn,
                               numStates,
                               numInputs,
                               umin=[-sys.uMax],
                               umax=[sys.uMax])  # may take xmin and xmax, just need to check definition 

    x = np.array([0,-np.pi]).reshape(sys.numStates,1)
    u = np.zeros([1,1])
    xgoal = np.array([0,0.]).reshape(sys.numStates,1)

    fig = plt.figure(10)
    plt.ion()

    for i in range(0,1000):
        start = time.time()
        u = controller.solve_for_next_u(x,xgoal,ulast=u,ugoal=u*0)
        x = sys.forward_simulate_dt(x,u,.01)

        plt.figure(10)
        sys.visualize(x)
        print("x: ",x)
        print("u: ",u)
        
