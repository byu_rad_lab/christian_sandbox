import numpy as np
import time
import matplotlib.pyplot as plt
from osqp_mpc.ParameterizedSmallMatrixOSQPMPC import OSQPMPC as OSQPMPC
# from osqp_mpc.SmallMatrixOSQPMPC import OSQPMPC as OSQPMPC
# from osqp_mpc.LargeMatrixOSQPMPC import OSQPMPC as OSQPMPC
from rad_models.BaxterRight import *
from torque_controller.msg import q_cmd

if __name__=='__main__':

    rospy.init_node('BaxterOSQPMPC')
    qcmd_pub = rospy.Publisher('/right/joint_cmd',q_cmd,queue_size=10)

    sys = BaxterRight()
    numStates = sys.numStates
    numInputs = sys.numInputs

    Q = 1.0*np.diag([0,0,0,0,0,0,0,1.0,1.0,1.0,1.0,1.0,1.0,1.0])
    Qf = 1.0*np.diag([0,0,0,0,0,0,0,1.0,1.0,1.0,1.0,1.0,1.0,1.0])
    R = 2.0*np.diag([1.0,1.0,1.0,1.0,1.0,1.0,1.0])
    horizon = 50
    dt = .01

    umin = np.array([-10]*7)
    umax = np.array([10]*7)
    xmin = np.array([-np.inf]*14)
    xmax = np.array([np.inf]*14)    
    

    mpc = OSQPMPC(Q,
                  Qf,
                  R,
                  horizon,
                  umin=umin,
                  umax=umax,
                  xmin=xmin,
                  xmax=xmax,
                  num_params=3)

    x = np.array([0.0,0,0,0,0,0,0,0,0,0,0,0,0,0]).reshape(sys.numStates,1)
    u = np.zeros([sys.numInputs,1])
    xgoal = np.ones([sys.numStates,1])

    sim_length = 500
    x_hist = np.zeros([sys.numStates,sim_length])
    u_hist = np.zeros([sys.numInputs,sim_length])

    for i in range(0,sim_length):
        [Ad,Bd,wd] = sys.calc_discrete_A_B_w(x,u,dt)
        np.set_printoptions(precision=1)
        
        u = mpc.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal=x[7:14]).reshape(7,1)
        # u = np.ones([7,1])
        x = Ad.dot(x) + Bd.dot(u) + wd

        u_hist[:,i] = u.flatten()
        x_hist[:,i] = x.flatten()

        print(x[7:14].T)

    plt.figure(4)
    plt.ion()
    plt.plot(x_hist[7:14,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Theta (rad)')
    plt.title('Position')
    plt.show()
        
    plt.figure(5)
    plt.plot(u_hist.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Torque (Nm)')
    plt.title('Input')

    plt.pause(1000)
        
