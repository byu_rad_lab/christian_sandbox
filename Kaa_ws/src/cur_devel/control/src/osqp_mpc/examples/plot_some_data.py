import scipy.io as sio
from scipy.signal import savgol_filter
import matplotlib.pyplot as plt
import numpy as np

if __name__=='__main__':

    # data = sio.loadmat('kaa_parameterized_mpc_data_1570661366.22.mat')
    data = sio.loadmat('kaa_parameterized_mpc_data_1570826910.37.mat')
    lempc_data = sio.loadmat('kaa_lempc_data.mat')    

    t = np.squeeze(data['t'])
    p = np.squeeze(data['p'])    
    q = np.squeeze(data['q'])
    qd = np.squeeze(data['qd'])
    u = np.squeeze(data['u'])
    solve_times = np.squeeze(data['solve_times'])

    lempc_t = np.squeeze(lempc_data['t'])
    lempc_p = np.squeeze(lempc_data['p'])    
    lempc_q = np.squeeze(lempc_data['q'])
    lempc_qd = np.squeeze(lempc_data['qd'])
    lempc_u = np.squeeze(lempc_data['u'])
    lempc_solve_times = np.squeeze(lempc_data['solve_times'])
    

    # for i in xrange(0,6):
    #     q[:,i] = savgol_filter(q[:,i],51,2)

    plt.ion()
    plt.figure(1)
    for i in range(0,6):
        plt.subplot(3,2,(i+1))    
        plt.plot(t,q[:,i],'b')
        plt.plot(lempc_t,lempc_q[:,i],'r')        
        plt.ylim(-1.25,1.25)
        plt.show()

    plt.figure(2)
    for i in range(0,6):
        plt.subplot(3,2,(i+1))
        plt.plot(t,u[:,2*i+0],'b')
        plt.plot(t,u[:,2*i+1],'r')        
        plt.plot(lempc_t,lempc_u[:,2*i+0],'c')
        plt.plot(lempc_t,lempc_u[:,2*i+1],'k')        
        plt.show()

    plt.figure(3)
    for i in range(0,6):
        plt.subplot(3,2,(i+1))    
        plt.plot(t,qd[:,i],'b')
        plt.plot(lempc_t,lempc_qd[:,i],'r')        
        plt.show()

    plt.figure(4)
    for i in range(0,6):
        plt.subplot(3,2,(i+1))    
        plt.plot(t,p[:,2*i+0],'b')
        plt.plot(t,p[:,2*i+1],'r')        
        plt.plot(lempc_t,lempc_p[:,2*i+0],'c')
        plt.plot(lempc_t,lempc_p[:,2*i+1],'k')        
        plt.show()
        
    print("median osqp solve time: ",np.median(solve_times))
    print("mean osqp solve time: ",np.mean(solve_times))    
    print("median lempc solve time: ",np.median(lempc_solve_times))
    print("mean lempc solve time: ",np.mean(lempc_solve_times))    
    
    plt.pause(10000)
    
    
