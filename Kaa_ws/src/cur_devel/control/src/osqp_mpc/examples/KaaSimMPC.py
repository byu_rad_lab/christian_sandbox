#!/usr/bin/env python
import numpy as np
import math
import time
from copy import deepcopy
from rad_models.Kaa import Kaa
from osqp_mpc.ParameterizedSmallMatrixOSQPMPC import OSQPMPC as small_p_OSQPMPC
from osqp_mpc.ParameterizedLargeMatrixOSQPMPC import OSQPMPC as large_p_OSQPMPC
from osqp_mpc.SmallMatrixOSQPMPC import OSQPMPC as small_OSQPMPC
from osqp_mpc.LargeMatrixOSQPMPC import OSQPMPC as large_OSQPMPC
import matplotlib.pyplot as plt

if __name__ == '__main__':

    sys = Kaa()
    
    numStates = sys.numStates
    numInputs = sys.numInputs

    Q = 1.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1.0])        
    Qf = 1.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1.0])
    R = 0.00000001*np.diag([1,1,1,1,1,1,1,1,1,1,1,1.0])
    horizon = 50
    dt = .01

    pmin = 0
    pmax = 130
    umin = np.array([pmin]*12)
    umax = np.array([pmax]*12)
    xmin = np.array([-np.inf]*24)
    xmax = np.array([np.inf]*24)    

    small_mpc = small_OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax)
    large_mpc = large_OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax)    
    small_p_mpc = small_p_OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax,num_params=3)
    large_p_mpc = large_p_OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax,num_params=3)            

    x = np.array([50.0,50,50,50,50,50,50,50,50,50,50,50, 0,0,0,0,0,0, 0,0,0,0,0,0]).reshape(24,1)
    u = np.ones([sys.numInputs,1])*50.0
    xgoal = np.array([0.0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0, 1,1,1,1,1,1]).reshape(24,1)*.25

    sim_length = 750
    x_hist = np.zeros([sys.numStates,sim_length])
    u_hist = np.zeros([sys.numInputs,sim_length])

    ax = plt.gca(projection='3d')
    plt.ion()

    for i in range(0,sim_length):
        [Ad,Bd,wd] = sys.calc_discrete_A_B_w(x,u,dt)
        np.set_printoptions(precision=1)

        # try:
        # u = small_mpc.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal=u*0).reshape(12,1)
        # u = large_mpc.solve_for_next_u(Ad,Bd,wd*0,x,xgoal,ugoal=u).reshape(12,1)
        u = small_p_mpc.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal=u).reshape(12,1)
        # u = large_p_mpc.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal=u).reshape(12,1)
        # except:
        #     u = u

        x = Ad.dot(x) + Bd.dot(u) + wd

        print('u: ',u.T)
        print('x: ',x.T)

        u_hist[:,i] = u.flatten()
        x_hist[:,i] = x.flatten()

        # if(i%5==0):
        #     print i
        #     sys.visualize(x,ax)

    plt.figure(2)
    plt.ion()
    plt.plot(x_hist[0:12,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Pressure (KPa)')
    plt.title('Pressure')
    plt.show()
    
    # plt.figure(3)
    # plt.ion()
    # plt.plot(x_hist[12:18,:].T)
    # plt.xlabel('Timestep')
    # plt.ylabel('Velocity (rad/s)')
    # plt.title('Velocity')
    # plt.show()

    plt.figure(4)
    plt.ion()
    plt.plot(x_hist[18:24,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Theta (rad)')
    plt.title('Position')
    plt.show()
        
    plt.figure(5)
    plt.plot(u_hist.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Torque (Nm)')
    plt.title('Input')

    plt.pause(1000)
