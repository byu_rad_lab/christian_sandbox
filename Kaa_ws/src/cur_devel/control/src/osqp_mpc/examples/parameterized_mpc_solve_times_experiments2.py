#!/usr/bin/env python
import numpy as np
import math
import time
from copy import deepcopy

from osqp_mpc.ParameterizedSmallMatrixOSQPMPC import OSQPMPC as OSQPMPC_small_p
from osqp_mpc.ParameterizedLargeMatrixOSQPMPC import OSQPMPC as OSQPMPC_large_p
from osqp_mpc.SmallMatrixOSQPMPC import OSQPMPC as OSQPMPC_small
from osqp_mpc.LargeMatrixOSQPMPC import OSQPMPC as OSQPMPC_large
# from osqp_mpc.OSQPMPCcheck import OSQPMPC
from osqp_mpc.OSQPMPC import OSQPMPC
import matplotlib.pyplot as plt
import sys as system

def draw_boxplot(data,positions, edge_color, fill_color, outlier_sym='+'):

    if(outlier_sym == ''):
        bp = plt.boxplot(data,positions=positions,patch_artist=True,sym='')
    else:
        bp = plt.boxplot(data,positions=positions,patch_artist=True,sym=edge_color+outlier_sym)

    for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
        plt.setp(bp[element], color=edge_color)

    for patch in bp['boxes']:
        patch.set(facecolor=fill_color)

    return bp


if __name__ == '__main__':

    m = .25
    l = .25
    I = m*l*l/12.0
    gravity = 9.81*0

    horizon = 100
    num_trials = 100
    num_params = 5
    num_dofs = np.arange(1,14)

    small_mpc_solve_times = np.zeros([len(num_dofs),num_trials])
    large_mpc_solve_times = np.zeros([len(num_dofs),num_trials])
    small_mpc_p_solve_times = np.zeros([len(num_dofs),num_trials])
    large_mpc_p_solve_times = np.zeros([len(num_dofs),num_trials])
    small_mpc_total_solve_times = np.zeros([len(num_dofs),num_trials])
    large_mpc_total_solve_times = np.zeros([len(num_dofs),num_trials])
    small_mpc_p_total_solve_times = np.zeros([len(num_dofs),num_trials])
    large_mpc_p_total_solve_times = np.zeros([len(num_dofs),num_trials])    
    
    small_mpc_u_stars = np.zeros([len(num_dofs),num_trials])
    large_mpc_u_stars = np.zeros([len(num_dofs),num_trials])    
    small_mpc_p_u_stars = np.zeros([len(num_dofs),num_trials])
    large_mpc_p_u_stars = np.zeros([len(num_dofs),num_trials])    

    for j in range(0,len(num_dofs)):
        numLinks = num_dofs[j]
        print("numLinks: ",numLinks)
        
        damping = numLinks/15.0

        if(numLinks==1):
            from rad_models.N1link import N1link
            sys = N1link(l,m,I,gravity,damping)
        elif(numLinks==2):
            from rad_models.N2link import N2link
            sys = N2link(l,m,I,gravity,damping)
        elif(numLinks==3):
            from rad_models.N3link import N3link
            sys = N3link(l,m,I,gravity,damping)
        elif(numLinks==4):
            from rad_models.N4link import N4link
            sys = N4link(l,m,I,gravity,damping)
        elif(numLinks==5):
            from rad_models.N5link import N5link
            sys = N5link(l,m,I,gravity,damping)
        elif(numLinks==6):
            from rad_models.N6link import N6link
            sys = N6link(l,m,I,gravity,damping)
        elif(numLinks==7):
            from rad_models.N7link import N7link
            sys = N7link(l,m,I,gravity,damping)
        elif(numLinks==8):
            from rad_models.N8link import N8link
            sys = N8link(l,m,I,gravity,damping)
        elif(numLinks==9):
            from rad_models.N9link import N9link
            sys = N9link(l,m,I,gravity,damping)
        elif(numLinks==10):
            from rad_models.N10link import N10link
            sys = N10link(l,m,I,gravity,damping)
        elif(numLinks==11):
            from rad_models.N11link import N11link
            sys = N11link(l,m,I,gravity,damping)
        elif(numLinks==12):
            from rad_models.N12link import N12link
            sys = N12link(l,m,I,gravity,damping)
        elif(numLinks==13):
            from rad_models.N13link import N13link
            sys = N13link(l,m,I,gravity,damping)
            
        x = np.zeros([sys.numStates,1])
        u = np.zeros([sys.numInputs,1])
        ugoal = np.zeros([sys.numInputs,1])
        xgoal = np.zeros([sys.numStates,1])
        xgoal[sys.numInputs] = 1
        
        n = sys.numInputs
        numStates = sys.numStates
        numInputs = sys.numInputs
        
        Q = 0.0*np.eye(sys.numStates)
        for i in range(sys.numLinks,sys.numLinks*2):
            Q[i,i] = 1.0/(5.0*(i+1-sys.numLinks))
        Qf = 1.0*Q
            
        R = 0.0*np.eye(sys.numInputs)            
        for i in range(0,sys.numLinks):
            R[i,i] = .01
            
        taumin = -50
        taumax = 50
        umin = np.array([taumin]*sys.numInputs)
        umax = np.array([taumax]*sys.numInputs)
        xmin = np.array([-np.inf]*sys.numStates)
        xmax = np.array([np.inf]*sys.numStates)
        dt = .01    
        
        
        large_mpc = OSQPMPC_large(Q,Qf,R,horizon,umin,umax,xmin,xmax,warm_start=False)
        small_mpc = OSQPMPC_small(Q,Qf,R,horizon,umin,umax,xmin,xmax,warm_start=False)        
        large_mpc_p = OSQPMPC_large_p(Q,Qf,R,horizon,umin,umax,xmin,xmax,num_params,warm_start=False)
        small_mpc_p = OSQPMPC_small_p(Q,Qf,R,horizon,umin,umax,xmin,xmax,num_params,warm_start=False)        
        
        
        for i in range(0,num_trials):
            x = np.random.randn(sys.numStates,1)
            xgoal = np.random.randn(sys.numStates,1)        
        
            [mpcAd,mpcBd,mpcwd] = sys.calc_discrete_A_B_w(x,u,dt)

            tic = time.time()
            small_mpc_p_u_star = small_mpc_p.solve_for_next_u(mpcAd,mpcBd,mpcwd,x,xgoal,ugoal)
            small_mpc_p_total_solve_times[j,i] = time.time()-tic
            tic = time.time()            
            small_mpc_u_star = small_mpc.solve_for_next_u(mpcAd,mpcBd,mpcwd,x,xgoal,ugoal)
            small_mpc_total_solve_times[j,i] = time.time()-tic            
            tic = time.time()
            large_mpc_u_star = large_mpc.solve_for_next_u(mpcAd,mpcBd,mpcwd,x,xgoal,ugoal)
            large_mpc_total_solve_times[j,i] = time.time()-tic                        
            tic = time.time()            
            large_mpc_p_u_star = large_mpc_p.solve_for_next_u(mpcAd,mpcBd,mpcwd,x,xgoal,ugoal)
            large_mpc_p_total_solve_times[j,i] = time.time()-tic            

            small_mpc_p_u_stars[j,i] = np.linalg.norm(small_mpc_p_u_star - large_mpc_u_star)
            small_mpc_u_stars[j,i] = np.linalg.norm(small_mpc_u_star - large_mpc_u_star)
            large_mpc_u_stars[j,i] = np.linalg.norm(large_mpc_p_u_star - large_mpc_u_star)
            large_mpc_p_u_stars[j,i] = np.linalg.norm(large_mpc_u_star - large_mpc_u_stars[j,i])

            # print 'small mpc p: ',small_mpc_p_u_stars[j,i]
            # print 'small mpc: ',small_mpc_u_stars[j,i]
            # print 'large mpc p: ',large_mpc_p_u_stars[j,i]
            # print 'large mpc: ',large_mpc_u_stars[j,i]            
            # raw_input('')
            
            # print "small MPC: ",small_mpc.solve_time
            # print "large MPC: ",large_mpc.solve_time        
            # print "Parameterized small MPC: ",small_mpc_p.solve_time
            # print "Parameterized large MPC: ",large_mpc_p.solve_time
            
            small_mpc_solve_times[j,i] = small_mpc.solve_time
            large_mpc_solve_times[j,i] = large_mpc.solve_time
            small_mpc_p_solve_times[j,i] = small_mpc_p.solve_time
            large_mpc_p_solve_times[j,i] = large_mpc_p.solve_time
            
    data = {'small_mpc_solve_times':small_mpc_solve_times,
            'large_mpc_solve_times':large_mpc_solve_times,
            'parameterized_small_mpc_solve_times':small_mpc_p_solve_times,
            'parameterized_large_mpc_solve_times':large_mpc_p_solve_times,
            'small_mpc_u_stars':small_mpc_u_stars,
            'large_mpc_u_stars':large_mpc_u_stars,
            'parameterized_small_mpc_u_stars':small_mpc_p_u_stars,
            'parameterized_large_mpc_u_stars':large_mpc_p_u_stars,
            'num_dofs':num_dofs}

    # import scipy.io as sio
    # sio.savemat(str(numLinks)+'link_num_dof_'+str(num_dof),data)

    plt.figure(1)
    bp1 = draw_boxplot(large_mpc_solve_times.T,num_dofs-1.5,'r','white',outlier_sym='')
    bp2 = draw_boxplot(small_mpc_solve_times.T,num_dofs-.5,'b','white',outlier_sym='')
    bp3 = draw_boxplot(large_mpc_p_solve_times.T,num_dofs+.5,'g','white',outlier_sym='')
    bp4 = draw_boxplot(small_mpc_p_solve_times.T,num_dofs+1.5,'k','white',outlier_sym='')
    plt.legend([bp1['medians'][0],bp2['medians'][0],bp3['medians'][0],bp4['medians'][0]],
               ['Large Matrix MPC','Small Matrix MPC','Large Matrix Parameterized MPC','Small Matrix Parameterized MPC'])
    num_dof_strings = []
    for num_dof in num_dofs:
        num_dof_strings.append(str(num_dof))
    plt.xticks(num_dofs,num_dof_strings)
    plt.xlim([5,105])
    plt.ion()
    plt.xlabel('Number of Links')
    plt.ylabel('Optimization Solve Time (s)')
    plt.show()

    plt.figure(2)
    plt.plot(num_dofs,np.median(large_mpc_solve_times,axis=1),'r')    
    plt.plot(num_dofs,np.median(small_mpc_solve_times,axis=1),'b')
    plt.plot(num_dofs,np.median(large_mpc_p_solve_times,axis=1),'g')
    plt.plot(num_dofs,np.median(small_mpc_p_solve_times,axis=1),'k')    
    plt.legend(['Large Matrix MPC','Small Matrix MPC','Large Matrix Parameterized MPC','Small Matrix Parameterized MPC'])
    plt.xlabel('Number of Links')
    plt.ylabel('Optimization Solve Time (s)')
    plt.show()

    plt.figure(3)
    bp1 = draw_boxplot(large_mpc_total_solve_times.T,num_dofs-1.5,'r','white',outlier_sym='')
    bp2 = draw_boxplot(small_mpc_total_solve_times.T,num_dofs-.5,'b','white',outlier_sym='')
    bp3 = draw_boxplot(large_mpc_p_total_solve_times.T,num_dofs+.5,'g','white',outlier_sym='')
    bp4 = draw_boxplot(small_mpc_p_total_solve_times.T,num_dofs+1.5,'k','white',outlier_sym='')
    plt.legend([bp1['medians'][0],bp2['medians'][0],bp3['medians'][0],bp4['medians'][0]],
               ['Large Matrix MPC','Small Matrix MPC','Large Matrix Parameterized MPC','Small Matrix Parameterized MPC'])
    num_dof_strings = []
    for num_dof in num_dofs:
        num_dof_strings.append(str(num_dof))
    plt.xticks(num_dofs,num_dof_strings)
    plt.xlim([0,14])
    plt.ion()
    plt.xlabel('Number of Links')
    plt.ylabel('Optimization Solve Time (s)')
    plt.show()
    

    plt.figure(4)
    plt.plot(num_dofs,np.median(large_mpc_total_solve_times,axis=1),'r')    
    plt.plot(num_dofs,np.median(small_mpc_total_solve_times,axis=1),'b')
    plt.plot(num_dofs,np.median(large_mpc_p_total_solve_times,axis=1),'g')
    plt.plot(num_dofs,np.median(small_mpc_p_total_solve_times,axis=1),'k')    
    plt.legend(['Large Matrix MPC','Small Matrix MPC','Large Matrix Parameterized MPC','Small Matrix Parameterized MPC'])
    plt.xlabel('Number of Links')
    plt.ylabel('MPC Solve Time (s)')
    plt.show()
    
    plt.pause(100000)

