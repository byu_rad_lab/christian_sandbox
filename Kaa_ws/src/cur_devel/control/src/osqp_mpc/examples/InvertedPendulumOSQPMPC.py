import numpy as np
import time
import matplotlib.pyplot as plt
from osqp_mpc.OSQPMPC import OSQPMPC
from rad_models.InvertedPendulum import *

if __name__=='__main__':

    sys = InvertedPendulum(mass=.02)
    numStates = sys.numStates
    numInputs = sys.numInputs
    
    Q = 1.0*np.diag([0,1.0])
    Qf = 10.0*np.diag([0,1.0])
    R = .0*np.diag([1.0])
    horizon = 100
    dt = .01

    mpc = OSQPMPC(Q,
                  Qf,
                  R,
                  horizon,
                  umin=[-sys.uMax]*sys.numInputs,
                  umax=[sys.uMax]*sys.numInputs,
                  xmin=[-np.inf,-np.pi],
                  xmax=[np.inf,np.pi])    

    x = np.array([0,-np.pi]).reshape(sys.numStates,1)
    u = np.zeros([sys.numInputs,1])
    xgoal = np.zeros([sys.numStates,1])

    fig = plt.figure(10)
    plt.ion()

    horizon = 100
    x_hist = np.zeros([sys.numStates,horizon])
    u_hist = np.zeros([sys.numInputs,horizon])

    for i in range(0,horizon):
        [Ad,Bd,wd] = sys.calc_discrete_A_B_w(x,u,dt)
        
        u = mpc.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal=np.zeros([sys.numInputs,1]))
        
        x = sys.forward_simulate_dt(x,u,.01)

        plt.figure(10)
        sys.visualize(x)
        
        u_hist[:,i] = u.flatten()
        x_hist[:,i] = x.flatten()

    plt.figure(4)
    plt.plot(x_hist[1,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Theta (rad)')
    plt.title('Position')
    plt.show()
        
    plt.figure(5)
    plt.plot(u_hist.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Torque (Nm)')
    plt.title('Input')

    plt.pause(1000)
        
