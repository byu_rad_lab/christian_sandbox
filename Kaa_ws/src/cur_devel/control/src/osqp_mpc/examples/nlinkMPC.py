#!/usr/bin/env python
import numpy as np
import math
import time
from copy import deepcopy

from osqp_mpc.LargeMatrixOSQPMPC import OSQPMPC
from osqp_mpc.ParameterizedSmallMatrixOSQPMPC import OSQPMPC as small_OSQPMPC
import matplotlib.pyplot as plt
import sys as system
import scipy.io as sio

if __name__ == '__main__':

    m = 1.0
    l = .25
    I = m*l*l/12.0
    gravity = 9.81*0

    numLinks = 5
    trial = str(0)
    argc = len(system.argv)
    if(argc>1):
        numLinks = eval(system.argv[1])
    if(argc>2):
        trial = system.argv[2]

    damping = 1/10.0

    error = 1.0
    if(argc>2*numLinks+3):
        error = eval(system.argv[2*numLinks+3])
        print("error: ",error)
        
    if(numLinks==1):
        from rad_models.N1link import N1link
        sys = N1link(l,m,I,gravity,damping)
        mpc_sys = N1link(l*error,m*error,I*error**3,gravity,damping)        
    elif(numLinks==2):
        from rad_models.N2link import N2link
        sys = N2link(l,m,I,gravity,damping)
        mpc_sys = N2link(l*error,m*error,I*error**3,gravity,damping)
    elif(numLinks==3):
        from rad_models.N3link import N3link
        sys = N3link(l,m,I,gravity,damping)
        mpc_sys = N3link(l*error,m*error,I*error**3,gravity,damping)
    elif(numLinks==4):
        from rad_models.N4link import N4link
        sys = N4link(l,m,I,gravity,damping)
        mpc_sys = N4link(l*error,m*error,I*error**3,gravity,damping)
    elif(numLinks==5):
        from rad_models.N5link import N5link
        sys = N5link(l,m,I,gravity,damping)
        mpc_sys = N5link(l*error,m*error,I*error**3,gravity,damping)
    elif(numLinks==6):
        from rad_models.N6link import N6link
        sys = N6link(l,m,I,gravity,damping)
        mpc_sys = N6link(l*error,m*error,I*error**3,gravity,damping)
    elif(numLinks==7):
        from rad_models.N7link import N7link
        sys = N7link(l,m,I,gravity,damping)
        mpc_sys = N7link(l*error,m*error,I*error**3,gravity,damping)
    elif(numLinks==8):
        from rad_models.N8link import N8link
        sys = N8link(l,m,I,gravity,damping)
        mpc_sys = N8link(l*error,m*error,I*error**3,gravity,damping)
    elif(numLinks==9):
        from rad_models.N9link import N9link
        sys = N9link(l,m,I,gravity,damping)
        mpc_sys = N9link(l*error,m*error,I*error**3,gravity,damping)
    elif(numLinks==10):
        from rad_models.N10link import N10link
        sys = N10link(l,m,I,gravity,damping)
        mpc_sys = N10link(l*error,m*error,I*error**3,gravity,damping)
    elif(numLinks==11):
        from rad_models.N11link import N11link
        sys = N11link(l,m,I,gravity,damping)
        mpc_sys = N11link(l*error,m*error,I*error**3,gravity,damping)
    elif(numLinks==12):
        from rad_models.N12link import N12link
        sys = N12link(l,m,I,gravity,damping)
        mpc_sys = N12link(l*error,m*error,I*error**3,gravity,damping)
    elif(numLinks==13):
        from rad_models.N13link import N13link
        sys = N13link(l,m,I,gravity,damping)
        mpc_sys = N13link(l*error,m*error,I*error**3,gravity,damping)
        

    x = np.zeros([sys.numStates,1])
    u = np.zeros([sys.numInputs,1])
    ugoal = np.zeros([sys.numInputs,1])
    xgoal = np.zeros([sys.numStates,1])
    xgoal[sys.numInputs] = 1
    
    n = sys.numInputs
    if(argc>3):
        for i in range(0,n):
            x[n+i] = eval(system.argv[i+3])
            xgoal[n+i] = eval(system.argv[i+n+3])
            
        

    numStates = sys.numStates
    numInputs = sys.numInputs
    
    Q = 0.0*np.eye(sys.numStates)
    for i in range(sys.numLinks,sys.numLinks*2):
        # Q[i,i] = 1.0/(5.0*(i+1-sys.numLinks))
        Q[i,i] = 1.0
    Qf = Q
    # Qf = 1.0*np.eye(sys.numStates)
    
    R = 0.001*np.eye(sys.numInputs)
    
    horizon = 100
    dt = .01

    taumax = 0
    for i in range(0,n):
        taumax += m*gravity*l*(i+.5)
    taumax = 2.0
    taumin = -taumax
    print(taumax)
    
    umin = np.array([taumin]*sys.numInputs)
    umax = np.array([taumax]*sys.numInputs)
    xmin = np.array([-np.inf]*sys.numStates)
    xmax = np.array([np.inf]*sys.numStates)    

    mpc = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax)
    small_mpc = small_OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax,num_params=3)            

    sim_length = 500
    x_hist = np.zeros([sys.numStates,sim_length])
    u_hist = np.zeros([sys.numInputs,sim_length])
    solve_time_hist = np.zeros([1,sim_length])
    total_solve_time_hist = np.zeros([1,sim_length])        

    small_mpc_x_hist = np.zeros([sys.numStates,sim_length])
    small_mpc_u_hist = np.zeros([sys.numInputs,sim_length])
    small_mpc_solve_time_hist = np.zeros([1,sim_length])        
    small_mpc_total_solve_time_hist = np.zeros([1,sim_length])    

    actual_cost = 0

    for i in range(0,sim_length):
        x = x.reshape([sys.numStates,-1])
        qd = x[0:numLinks,:]
        q = x[numLinks:sys.numStates,:]
        [Ad,Bd,wd] = sys.calc_discrete_A_B_w(q,qd,dt)
        [mpcAd,mpcBd,mpcwd] = mpc_sys.calc_discrete_A_B_w(q,qd,dt)
        # np.set_printoptions(precision=1)

        tic = time.time()
        u_star = mpc.solve_for_next_u(mpcAd,mpcBd,mpcwd,x,xgoal,ugoal=u*0).reshape(sys.numInputs,1)
        total_solve_time = time.time()-tic
        
        u = u_star.reshape(sys.numInputs,1)

        x_next = Ad.dot(x) + Bd.dot(u) + wd
        x = deepcopy(x_next)

        actual_cost += x.T.dot(Q).dot(x) + u.T.dot(R).dot(u)

        u_hist[:,i] = u.flatten()
        x_hist[:,i] = x.flatten()
        solve_time_hist[0,i] = mpc.solve_time
        total_solve_time_hist[0,i] = total_solve_time

    # plt.figure(1)
    # plt.ion()
    # plt.plot(x_hist[sys.numLinks:sys.numStates,:].T)
    # plt.xlabel('Timestep')
    # plt.ylabel('Position (rad)')
    # plt.title('OSQP Position')
    # plt.show()
    
    # plt.figure(5)
    # plt.plot(u_hist.T)
    # plt.show()
    # plt.xlabel('Timestep')
    # plt.ylabel('Torque (Nm)')
    # plt.title('OSQP Input')

    # plt.pause(5)

    data = {'x':x_hist.T,
            'xgoal':xgoal,
            'u':u_hist.T,
            'solve_times':solve_time_hist,
            'total_solve_times':total_solve_time_hist}
    sio.savemat(str(n)+'link_osqp_data_error_'+str(error)+'_'+trial,data)

    small_mpc_actual_cost = 0
    x = np.zeros([sys.numStates,1])
    u = np.zeros([sys.numInputs,1])
    ugoal = np.zeros([sys.numInputs,1])
    xgoal = np.zeros([sys.numStates,1])
    xgoal[sys.numInputs] = 1
    
    n = sys.numInputs
    if(argc>3):
        for i in range(0,n):
            x[n+i] = eval(system.argv[i+3])
            xgoal[n+i] = eval(system.argv[i+n+3])

    for i in range(0,sim_length):
        x = x.reshape([sys.numStates,-1])
        qd = x[0:numLinks,:]
        q = x[numLinks:sys.numStates,:]
        [Ad,Bd,wd] = sys.calc_discrete_A_B_w(q,qd,dt)
        [mpcAd,mpcBd,mpcwd] = mpc_sys.calc_discrete_A_B_w(q,qd,dt)
        # np.set_printoptions(precision=1)

        tic = time.time()
        u_star = small_mpc.solve_for_next_u(mpcAd,mpcBd,mpcwd,x,xgoal,ugoal=u*0).reshape(sys.numInputs,1)
        total_solve_time = time.time()-tic
        if(u_star[0] == None):
            print("failed to solve")
        else:
            u = u_star

        x_next = Ad.dot(x) + Bd.dot(u) + wd
        x = deepcopy(x_next)

        small_mpc_actual_cost += x.T.dot(Q).dot(x) + u.T.dot(R).dot(u)

        small_mpc_u_hist[:,i] = u.flatten()
        small_mpc_x_hist[:,i] = x.flatten()
        small_mpc_solve_time_hist[0,i] = small_mpc.solve_time
        small_mpc_total_solve_time_hist[0,i] = total_solve_time
        

    # plt.figure(1)
    # plt.ion()
    # plt.plot(small_mpc_x_hist[sys.numLinks:sys.numStates,:].T)
    # plt.xlabel('Timestep')
    # plt.ylabel('Position (rad)')
    # plt.title('OSQP Position')
    # plt.show()
    
    # plt.figure(5)
    # plt.plot(small_mpc_u_hist.T)
    # plt.show()
    # plt.xlabel('Timestep')
    # plt.ylabel('Torque (Nm)')
    # plt.title('OSQP Input')

    data = {'x':small_mpc_x_hist.T,
            'xgoal':xgoal,
            'u':small_mpc_u_hist.T,
            'solve_times':small_mpc_solve_time_hist,
            'total_solve_times':small_mpc_total_solve_time_hist}
    sio.savemat(str(n)+'link_small_osqp_data_error_'+str(error)+'_'+trial,data)

    # plt.pause(5)
