close all; clear all; clc;

%% Plot OSQPMPC data
load('OSQPMPC_data.mat')
dt = .01;
t = linspace(0,dt*size(x,2),size(x,2));

figure(1)
for jt=1:7
    subplot(4,2,jt)
    plot(t,x(jt*2-1,:),'r')
    hold on
    plot(t,x(jt*2,:),'b')
    
    plot(t,u(jt*2-1,:),'r--')
    plot(t,u(jt*2,:),'b--')
    ylabel(strcat('Jt ',int2str(jt),' Prs (kPa)'))
end

figure(2)
for jt=1:7
    subplot(4,2,jt)
    plot(t,x(21+jt,:))
    ylabel(strcat('Jt ',int2str(jt),' Angle (rad)'))
end


%% Plot EMPC data
load('../../../data1.mat')
datas = csvmatrix;
load('../../../data2.mat')
datas = cat(3,datas,csvmatrix);
load('../../../data3.mat')
datas = cat(3,datas,csvmatrix);
load('../../../data4.mat')
datas = cat(3,datas,csvmatrix);
load('../../../data5.mat')
datas = cat(3,datas,csvmatrix);
load('../../../data6.mat')
datas = cat(3,datas,csvmatrix);
load('../../../data7.mat')
datas = cat(3,datas,csvmatrix);
load('../../../data8.mat')
datas = cat(3,datas,csvmatrix);
load('../../../data9.mat')
datas = cat(3,datas,csvmatrix);
load('../../../data10.mat')
datas = cat(3,datas,csvmatrix);

mean_data = mean(datas,3);
max_data = max(datas,[],3);
min_data = min(datas,[],3);

x = mean_data(:,1:28)';
xmax = max_data(:,1:28)';
xmin = min_data(:,1:28)';
u = mean_data(:,29:42)';

dt = .01;
t = linspace(0,dt*size(x,2),size(x,2));

figure(1)
for jt=1:7
    subplot(4,2,jt)
    hold on
    plot(t,x(jt*2-1,:),'m')
    plot(t,x(jt*2,:),'c')
    
    plot(t,u(jt*2-1,:),'m--')
    plot(t,u(jt*2,:),'c--')
end
legend('OSQPMPC Prs+','OSQPMPC Prs-','OSQPMPC Ref Prs+','OSQPMPC Ref Prs-','EMPC Prs+','EMPC Prs-','EMPC Ref Prs+','EMPC Ref Prs-')

figure(2)
for jt=1:7
    subplot(4,2,jt)
    hold on
    plot(t,x(21+jt,:),'r')
    plot(t,xmax(21+jt,:),'r--')
    plot(t,xmin(21+jt,:),'r--')
end

legend('OSQPMPC','EMPC Mean','EMPC Max','EMPC Min')

%% Plot solve time data
figure(3)
load('data.mat')
empc_solve_times = csvmatrix(:,43)';
solve_times = [solve_time,empc_solve_times]';
g1 = repmat({'OSQPMPC'},1000,1);
g2 = repmat({'EMPC'},1000,1);
g = [g1; g2];


title('Solve Times')
boxplot(solve_times,g)
ylabel('Log Solve Time (s)')
set(gca, 'YScale', 'log')



