import numpy as np
import time
import matplotlib.pyplot as plt
from osqp_mpc.ParameterizedSmallMatrixOSQPMPC import OSQPMPC as OSQPMPC
# from osqp_mpc.SmallMatrixOSQPMPC import OSQPMPC as OSQPMPC
# from osqp_mpc.LargeMatrixOSQPMPC import OSQPMPC as OSQPMPC
from rad_models.BaxterRight import *
from torque_controller.msg import q_cmd
from sensor_msgs.msg import JointState
import rospy


class HardwareBaxterOSQPMPC():

    def __init__(self):
        self.q = np.zeros((7,1)) # initialize q and q dot
        self.qd = np.zeros((7,1))

        rospy.init_node('BaxterOSQPMPC') # initializes ROS node for the process
        # creates subscriber object
        self.joint_states_sub = rospy.Subscriber('/robot/joint_states',
                                                 JointState,self.joint_state_cb)
        # creates publisher object, q_cmd comes from torque_controller.msg
        self.qcmd_pub = rospy.Publisher('/right/joint_cmd',q_cmd,queue_size=10)

        self.sys = BaxterRight() # makes system object
        numStates = self.sys.numStates
        numInputs = self.sys.numInputs

        Q = 1.0*np.diag([0,0,0,0,0,0,0,1.0,1.0,1.0,1.0,1.0,1.0,1.0])
        Qf = 1.0*np.diag([0,0,0,0,0,0,0,1.0,1.0,1.0,1.0,1.0,1.0,1.0])
        # Qf = 10.0*np.diag([1,1,1,1,1,1,1,1.0,1.0,1.0,1.0,1.0,1.0,1.0])
        R = 2.0*np.diag([1.0,1.0,1.0,1.0,1.0,1.0,1.0])

        self.xgoal = np.ones([self.sys.numStates,1])*-0.
        horizon = 100
        self.dt = .01
        
        umin = np.array([-10]*7) # makes an array that has [-10] ten times
        umax = np.array([10]*7)
        xmin = np.array([-np.inf]*14) # makes an array of 14 [-inf] values
        xmax = np.array([np.inf]*14)    
        
        self.mpc = OSQPMPC(Q,   # imported from osqp_mpc.ParameterizedSmallMatrixOSQPMPC
                           Qf,
                           R,
                           horizon,
                           umin=umin,
                           umax=umax,
                           xmin=xmin,
                           xmax=xmax,
                           num_params=4)

        self.x = np.array([0.0,0,0,0,0,0,0,0,0,0,0,0,0,0]).reshape(self.sys.numStates,1)
        self.u = np.zeros([self.sys.numInputs,1])


    # I think this deconstructs the message from /robot/joint_states and puts values
    # into self.q and self.qd
    def joint_state_cb(self,msg): 
        pos = msg.position
        vel = msg.velocity
        self.q = np.array([pos[11],pos[12],pos[9],pos[10],pos[13],pos[14],pos[15]])
        self.qd = np.array([vel[11],vel[12],vel[9],vel[10],vel[13],vel[14],vel[15]])

    def control(self):
        # Reconstructs a single x vector
        self.x = np.vstack([self.qd.flatten(),self.q.flatten()]).reshape(14,1)

        # calculates descrete A B and w
        [ad,Bd,wd] = self.sys.calc_discrete_A_B_w(self.x,self.u,self.dt)

        start_solve = time.time() # for calculating solve time

        # mpc magic using osqp_mpc.ParameterizedSmallMatricOSPMPC
        self.u = self.mpc.solve_for_next_u(Ad,Bd,wd,self.x,self.xgoal,
                                           ugoal=self.x[7:14]).reshape(7,1)
        
        self.solve_time = time.time()-start_solve # solve time stuff

        # For a step input
        # self.u = self.xgoal[7:14]

        # magic from q_cmd from the torque control msg thing
        msg = q_cmd()
        msg.q_array = self.u
        self.qcmd_pub.publish(msg) # publishes the message to Baxter

        # print "u: ",self.u.T
        # print "q: ",self.q.T


if __name__=='__main__':

    controller = HardwareBaxterOSQPMPC() # Calls OSQPMPC as the controller for system
                                         # also starts ROS node BaxterOSQPMPC
    x_hist = []                          # creates histories for plotting
    u_hist = []
    t_hist = []    
    solve_time_hist = []

    t_settle = 10.0             # settling time between commands
    start = time.time()         # takes current time as a reference
    while not rospy.is_shutdown():                                  # recommended for running ROS
        if(time.time()-start < t_settle and time.time()-start > 0): # set xgoals at each time
            controller.xgoal = np.ones([controller.sys.numStates,1])*0
        if(time.time()-start < 2*t_settle and time.time()-start > t_settle):
            controller.xgoal = np.ones([controller.sys.numStates,1])*.5
        if(time.time()-start < 3*t_settle and time.time()-start > 2*t_settle):
            controller.xgoal = np.ones([controller.sys.numStates,1])*-.5
        if(time.time()-start < 4*t_settle and time.time()-start > 3*t_settle):
            controller.xgoal = np.ones([controller.sys.numStates,1])*0
        if(time.time()-start > 4*t_settle):
            break
            
        controller.control()    # runs the controller with new xgoal

        t_hist.append(time.time()-start) # records time since start for plotting
        x_hist.append(controller.x)      # adds the measured state for plotting
        u_hist.append(controller.u)      # adds the current commanded torques for plotting
        solve_time_hist.append(controller.solve_time) # adds current solve time for plotting

    x_hist = np.array(x_hist).squeeze() # makes x_hist a 2d np array
    u_hist = np.array(u_hist).squeeze() # same for u
    solve_time_hist = np.array(solve_time_hist).squeeze() # and time

    plt.figure(4)               # plots 
    plt.ion()
    plt.plot(x_hist[:,7:14])
    plt.xlabel('Timestep')
    plt.ylabel('Theta (rad)')
    plt.title('Position')
    plt.show()
        
    plt.figure(6)
    plt.plot(u_hist)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('qdes (Nm)')
    plt.title('Input')

    plt.figure(5)
    plt.hist(solve_time_hist,100)
    plt.show()
    plt.xlabel('Solve Time (s)')
    plt.title('Solve Times')

    data_mat = {'x':x_hist,
                'u':u_hist,
                't':t_hist,
                'solve_times':solve_time_hist}

    import scipy.io as sio
    sio.savemat('baxter_osqpmpc_data',data_mat) # saves off x, u, t, and times as a .mat
    
    plt.pause(1000)
        
