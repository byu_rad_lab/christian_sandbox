#!/usr/bin/env python
import numpy as np
import math
import rospy
import time
from copy import deepcopy
# from rad_models.BellowsArm import BellowsArm
from rad_models.BellowsArm2 import BellowsArm
from osqp_mpc.LargeMatrixOSQPMPC import OSQPMPC
# from osqp_mpc.SmallMatrixOSQPMPC import OSQPMPC
# from osqp_mpc.ParameterizedSmallMatrixOSQPMPC import OSQPMPC
from ros_srv.srv import uint32_srv #SetRobotCtrlMode
from ros_srv.srv import RequestMoveSrv #RequestMoveSinusoidal
from ros_msg.msg import SystemMinimalMsg

class BlowMoldedMPC():

    def __init__(self,horizon=20,dt=.01):
        self.dt = dt
        self.q_goal = np.array([[0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],dtype=np.double).T

        # Joint states and inputs
        self.q = 0.0*np.ones([6,1])
        self.qd = 0.0*np.ones([6,1])
        self.pressures = []
        self.pscale = 1000.0

        self.arm = BellowsArm()

        Q_vel = 0.0
        Q_pos = 1.0
        Q = 1.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,Q_vel,Q_vel,Q_vel,Q_vel,Q_vel,Q_vel,Q_pos,Q_pos,Q_pos,Q_pos,Q_pos,Q_pos])

        Q_vel = 0.0
        Qf = 10.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,Q_vel,Q_vel,Q_vel,Q_vel,Q_vel,Q_vel,Q_pos,Q_pos,Q_pos,Q_pos,Q_pos,Q_pos])
        
        R = 0.000001*np.diag([1,1,1,1,1,1,1,1,1,1,1,1.0])        

        pmin = 8
        pmax = 400
        umin = np.array([pmin]*12)
        umax = np.array([pmax]*12)
        xmin = np.ones(24)*-np.inf
        xmax = np.ones(24)*np.inf        
        
        self.OSQPMPCSolver = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax)
        # self.OSQPMPCSolver = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax,num_params=2)        
        
        # subscribers and service calls
        self.robot_state_sub = rospy.Subscriber('/atheris/system_minimal',SystemMinimalMsg,self.states_cb)

        self.got_initial_state = False
        rate = rospy.Rate(10)
        while not self.got_initial_state:
            rate.sleep()
            print("Waiting for initial robot state")            
        
        self.setCtrl = rospy.ServiceProxy('/atheris/SetRobotCtrlMode', uint32_srv, persistent=True)
        try:
            worked = self.setCtrl(1) #1 for pressure 0 for position
        except rospy.ServiceException as e:
            print('Error :', e)
            
        self.req_move_sin_pressure = rospy.ServiceProxy('/atheris/RequestMoveSinusoidalPressure', RequestMoveSrv, persistent=True)
            

    def states_cb(self,msg):
        q0 = np.array(deepcopy(msg.joints_est[0].q))
        q1 = np.array(deepcopy(msg.joints_est[1].q))
        q2 = np.array(deepcopy(msg.joints_est[2].q))
        q = np.reshape(np.hstack([q0,q1,q2]),[6,1])
        self.q = q

        qd0 = np.array(deepcopy(msg.joints_est[0].qd))
        qd1 = np.array(deepcopy(msg.joints_est[1].qd))
        qd2 = np.array(deepcopy(msg.joints_est[2].qd))
        qd = np.reshape(np.hstack([qd0,qd1,qd2]),[6,1])
        self.qd = qd

        p0 = np.array(deepcopy(msg.joints_est[0].prs_bel))
        p1 = np.array(deepcopy(msg.joints_est[1].prs_bel))
        p2 = np.array(deepcopy(msg.joints_est[2].prs_bel))
        p = np.reshape(np.hstack([p0,p1,p2]),[12,1])/self.pscale

        self.pressures = p
        self.got_initial_state=True
        
    def send_pressure_command(self,pressures,movetime):
        result = self.req_move_sin_pressure.call(movetime, self.pscale*pressures)

    def control(self):
        x = np.vstack([self.pressures,self.qd,self.q])
        u = self.pressures
        
        Ad,Bd,wd = self.arm.calc_discrete_A_B_w(x,u,self.dt)
        xgoal = np.vstack([self.pressures,self.qd*0,self.q_goal])

        tic = time.time()
        print('x: ',x.T)
        u = self.OSQPMPCSolver.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal=self.pressures)
        print(time.time()-tic)
        # u = 0.5*np.reshape(u,[12,1]) + 0.5*self.pressures.reshape(12,1)
        u = np.reshape(u,[12,1])

        self.send_pressure_command(u,.01)

if __name__ == '__main__':

    rospy.init_node('mpc_control', anonymous=True)
    horizon = 10
    mpc = BlowMoldedMPC(horizon,dt=.02)
    rate = rospy.Rate(50)        

    mpc.send_pressure_command(100*np.ones(12),10)
    time.sleep(10.0)    

    start = time.time()
    # mpc.q_goal = np.array([[0,0,0,0,0,0]],dtype=np.double).T
    # mpc.control()
    t_settle = 20.0
    start = time.time()
    while not rospy.is_shutdown():
        if time.time() - start < t_settle:
            mpc.q_goal = 0.5*np.array([[0.5,    -0.5,     -0.5,    0.5,  0.5,   -0.5]],dtype=np.double).T
        elif time.time() - start > t_settle and time.time() - start < t_settle*2:
            mpc.q_goal = np.array([-0.5,     0.5,   0.5,   -0.5,   -0.5, 0.5]).reshape(6,1)
        elif time.time() - start > t_settle*2 and time.time() - start < t_settle*3:
            mpc.q_goal = np.array([0.5,   0.5,   0.0,   0.0,  0.5,  0.5]).reshape(6,1)
        elif time.time() - start > t_settle*3 and time.time() - start < t_settle*4:
            mpc.q_goal = np.array([0.0,   0.0,   0.5,   0.5,  0.0,  0.0]).reshape(6,1)            

        mpc.control()
        rate.sleep()
            

        
