#!/usr/bin/env python
import numpy as np
import math
import rospy
import time
from copy import deepcopy
from rad_models.Kaa import Kaa
from osqp_mpc.ParameterizedSmallMatrixOSQPMPC import OSQPMPC

from falkor.srv import set_robot_ctrl_mode
from falkor.srv import move_joint_prs
from byu_robot.msg import states

np.set_printoptions(precision=2)

class KaaMPC():

    def __init__(self,horizon=20,dt=.01):
        self.dt = dt
        self.q_goal = np.array([[0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],dtype=np.double).T

        # Joint states and inputs
        self.q = 0.0*np.ones([6,1])
        self.qd = 0.0*np.ones([6,1])
        self.pressures = 0.0*np.ones([12,1])
        self.u = 0.0*np.ones([12,1])

        self.ki = .00
        self.error = np.zeros([6,1])

        self.arm = Kaa()

        Q_vel = 0.0
        Q_pos = 1.0
        Q = 1.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,Q_vel,Q_vel,Q_vel,Q_vel,Q_vel,Q_vel,Q_pos,Q_pos,Q_pos,Q_pos,Q_pos,Q_pos])

        Q_vel = 0.0
        Qf = 1.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,Q_vel,Q_vel,Q_vel,Q_vel,Q_vel,Q_vel,Q_pos,Q_pos,Q_pos,Q_pos,Q_pos,Q_pos])
        
        # R = 0.00005*np.diag([1,1,1,1,1,1,1,1,1,1,1,1.0])
        R = 0.0001*np.diag([1,1,1,1,1,1,1,1,2,2,2,2.0])                

        pmin = 0
        pmax = 130
        umin = np.array([pmin]*12)
        umax = np.array([pmax]*12)
        xmin = np.ones(24)*-np.inf
        xmax = np.ones(24)*np.inf        
        
        # self.OSQPMPCSolver = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax)
        self.OSQPMPCSolver = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax,num_params=3)        
        
        # subscribers and service calls
        self.robot_state_sub = rospy.Subscriber('/states',states,self.states_cb)

        self.got_initial_state = False
        rate = rospy.Rate(10)
        while not self.got_initial_state:
            rate.sleep()
            print("Waiting for initial robot state")            

        self.setCtrl = rospy.ServiceProxy('/SetRobotCtrlMode', set_robot_ctrl_mode, persistent=True)            
        try:
            worked = self.setCtrl(2)
        except rospy.ServiceException as e:
            print('Error :', e)
            
        self.sendPrsCmd = rospy.ServiceProxy('MoveJointPrs', move_joint_prs, persistent=True)
            

    def states_cb(self,msg):
        q = np.array(msg.q).reshape(6,1)
        qd = np.array(msg.qdot).reshape(6,1)
        qd[np.abs(qd)>1] = 0
        # if self.got_initial_state==False:
        #     self.q = q
        #     qd = np.array(msg.qdot).reshape(6,1)
        #     self.qd = qd
        
        self.q = deepcopy(q)
        self.qd = deepcopy(qd)

        # p = np.array(zip(msg.pPlus,msg.pMinus)).reshape(12,1)
        p = np.array(list(zip(msg.pMinus,msg.pPlus))).reshape(12,1)        

        ptemp = deepcopy(p[0])
        p[0] = deepcopy(p[1])
        p[1] = ptemp

        ptemp = deepcopy(p[11])
        p[11] = deepcopy(p[10])
        p[10] = ptemp

        self.pressures = deepcopy(p)/1000.0
        self.got_initial_state=True
        
    def send_pressure_command(self,pressures,move_time):
        pressures[pressures<0] = 0
        # print 'pref: ',pressures.flatten().tolist()
        pressures = (pressures)*1000.0
        move_type = 1
        self.sendPrsCmd(0,[pressures[0],pressures[1],pressures[3],pressures[2]],move_time,move_type)
        self.sendPrsCmd(1,[pressures[5],pressures[4],pressures[7],pressures[6]],move_time,move_type)
        self.sendPrsCmd(2,[pressures[9],pressures[8],pressures[11],pressures[10]],move_time,move_type)
        
    def control(self):
        x = np.vstack([self.pressures,self.qd,self.q])

        # self.q_goal[4] = 0
        # self.q[4] = 0        
        self.error += self.q_goal-self.q
        max_error = 3.0
        for i in range(0,6):
            if(np.abs(self.error[i]) > max_error):
                self.error[i] = np.sign(self.error[i])*max_error
                
        # print np.linalg.norm(self.q_goal[0:4]-self.q[0:4])        
        # if(np.linalg.norm(self.q_goal[0:4]-self.q[0:4])>.25):
        #     self.error = self.error*0

        self.q_cmd = self.q_goal + self.error*self.ki
        # print "q_cmd: ",self.q_cmd.T
        u = self.pressures
        
        Ad,Bd,wd = self.arm.calc_discrete_A_B_w(x,u,self.dt)
        xgoal = np.vstack([self.pressures,self.qd*0,self.q_cmd])

        tic = time.time()
        print('q: ',self.q.T)
        # print 'pressures: ',self.pressures.T
        self.u = self.OSQPMPCSolver.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal=self.u)
        self.solve_time = time.time()-tic
        # print self.solve_time        
        self.u = np.reshape(self.u,[12,1])

        self.send_pressure_command(self.u,.0001)

if __name__ == '__main__':

    rospy.init_node('mpc_control', anonymous=True)
    horizon = 50
    mpc = KaaMPC(horizon,dt=.02)
    rate = rospy.Rate(50)  

    mpc.send_pressure_command(0*np.ones(12),1)
    time.sleep(10.0)    

    start = time.time()
    t_settle = 30.0
    start = time.time()
    solve_times = []
    t_hist = []    
    q_hist = []
    qd_hist = []
    p_hist = []
    u_hist = []
    while not rospy.is_shutdown():
        if time.time() - start < t_settle:
            mpc.q_goal = 1.0*np.array([[1.0,-1,-1,1,0,0]],dtype=np.double).T
        elif time.time() - start > t_settle and time.time() - start < t_settle*2:
            mpc.q_goal = 1.0*np.array([[-1.0,1,1,-1,0,0]],dtype=np.double).T
        elif time.time() - start > t_settle*2 and time.time() - start < t_settle*3:
            mpc.q_goal = 1.0*np.array([[0.0,0,-1,0,0,0]],dtype=np.double).T
        elif time.time() - start > t_settle*3 and time.time() - start < t_settle*4:
            mpc.q_goal = 1.0*np.array([[0.0,1,0,-1,0,0]],dtype=np.double).T
        elif time.time() - start > t_settle*4 and time.time() - start < t_settle*5:
            mpc.q_goal = 0.0*np.array([[0.0,0,0,0,0,0]],dtype=np.double).T
        # elif time.time() - start > t_settle*5 and time.time() - start < t_settle*6:
        #     mpc.q_goal = -0.5*np.array([[0.0,0,1,0,0,0]],dtype=np.double).T
        # elif time.time() - start > t_settle*6 and time.time() - start < t_settle*7:
        #     mpc.q_goal = np.array([[0.0,0,0,0,0,0]],dtype=np.double).T

        else:
            break

        mpc.control()
        t_hist.append(time.time()-start)        
        solve_times.append(mpc.solve_time)
        q_hist.append(mpc.q)
        qd_hist.append(mpc.qd)
        p_hist.append(mpc.pressures)
        u_hist.append(mpc.u)
        
        rate.sleep()

    import scipy.io as sio

    data_dict = {'t':t_hist,
                 'q':q_hist,
                 'qd':qd_hist,
                 'p':p_hist,
                 'u':u_hist,
                 'solve_times':solve_times}

    sio.savemat('kaa_parameterized_mpc_data_'+str(time.time()),data_dict)
