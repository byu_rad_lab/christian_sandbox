#!/usr/bin/env python
import numpy as np
import math
import time
from copy import deepcopy
from rad_models.BellowsArm import BellowsArm
from osqp_mpc.OSQPMPC import OSQPMPC
import matplotlib.pyplot as plt

if __name__ == '__main__':

    sys = BellowsArm()
    
    numStates = sys.numStates
    numInputs = sys.numInputs

    Q = 1.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1.0])        
    Qf = 1.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1.0])
    R = 0.0000*np.diag([1,1,1,1,1,1,1,1,1,1,1,1.0])        
    horizon = 100
    dt = .01

    pmin = 8
    pmax = 300
    umin = np.array([pmin]*12)
    umax = np.array([pmax]*12)
    xmin = np.array([pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,-np.inf,-np.inf,-np.inf,-np.inf,-np.inf,-np.inf,-np.pi,-np.pi,-np.pi,-np.pi,-np.pi,-np.pi])
    xmax = np.array([pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,np.inf,np.inf,np.inf,np.inf,np.inf,np.inf,np.pi,np.pi,np.pi,np.pi,np.pi,np.pi])

    mpc = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax)    

    x = np.array([100.0,100,100,100,100,100,100,100,100,100,100,100, 0,0,0,0,0,0, 0,0,0,0,0,0]).reshape(24,1)
    u = np.zeros([sys.numInputs,1])
    xgoal = np.array([0.0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0, 1,1,1,1,1,1]).reshape(24,1)

    sim_length = 1000
    x_hist = np.zeros([sys.numStates,sim_length])
    u_hist = np.zeros([sys.numInputs,sim_length])

    [Ad,Bd,wd] = sys.calc_discrete_A_B_w(x,u,dt)    
    for i in range(0,sim_length):
        # [Ad,Bd,wd] = sys.calc_discrete_A_B_w(x,u,dt)
        np.set_printoptions(precision=1)
        
        u = mpc.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal=u).reshape(12,1)
        x = Ad.dot(x) + Bd.dot(u) + wd

        u_hist[:,i] = u.flatten()
        x_hist[:,i] = x.flatten()

    plt.figure(1)
    plt.ion()
    plt.plot(x_hist[0:12,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Pressure (KPa)')
    plt.title('Pressure')
    plt.show()
    
    plt.figure(2)
    plt.ion()
    plt.plot(x_hist[12:18,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Velocity (rad/s)')
    plt.title('Velocity')
    plt.show()

    plt.figure(4)
    plt.ion()
    plt.plot(x_hist[18:24,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Theta (rad)')
    plt.title('Position')
    plt.show()
        
    plt.figure(5)
    plt.plot(u_hist.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Torque (Nm)')
    plt.title('Input')

    plt.pause(1000)
