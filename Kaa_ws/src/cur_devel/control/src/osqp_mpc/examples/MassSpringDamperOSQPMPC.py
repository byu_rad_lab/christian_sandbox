import numpy as np
import time
import matplotlib.pyplot as plt
from osqp_mpc.SmallMatrixOSQPMPC import OSQPMPC
from rad_models.MassSpringDamper import *

if __name__=='__main__':

    sys = MassSpringDamper(mass = 10.0, spring = 3.0, damping = 0.1, friction = 0.02, uMax = 150.0)
    numStates = sys.numStates
    numInputs = sys.numInputs

    Q = 1.0*np.diag([10.0,0.0])
    Qf = 10.0*np.diag([10.0,0.0])
    R = 0.0*np.diag([0.0])
    horizon = 100
    dt = 0.01

    mpc = OSQPMPC(Q, Qf, R, horizon,
                  umin=[-sys.uMax]*sys.numInputs,
                  umax=[sys.uMax]*sys.numInputs,
                  xmin=[-np.inf,-np.inf],
                  xmax=[np.inf,np.inf])

    x = np.array([0.0, 0.0]).reshape(sys.numStates, 1)
    u = np.zeros([sys.numInputs,1]).reshape(sys.numInputs, 1)
    xgoal = np.array([1.0, 0]).reshape(sys.numStates, 1)

    fig = plt.figure(10)
    plt.ion()

    n_steps = 100
    x_hist = np.zeros([sys.numStates, horizon])
    u_hist = np.zeros([sys.numInputs, horizon])

    for i in range(0, n_steps):
        # print(x)
        # print(u)
        # print(dt)
        [Ad, Bd, wd] = sys.calc_discrete_A_B_w(x,u,dt)
        # print((Ad))
        # print((Bd))
        # print((wd))

        u = mpc.solve_for_next_u(Ad, Bd, wd, x, xgoal, ugoal = np.zeros([sys.numInputs,1]))
        # print(u)                # Pretty sure mpc.solve_for_next_u is spitting out weird u
        u = u.reshape(sys.numInputs, 1)

        x = sys.forward_simulate_dt(x,u,dt)

        plt.figure(10)
        sys.visualize(x, u)

        u_hist[:,i] = u.flatten()
        x_hist[:,i] = x.flatten()

    plt.figure(4)
    plt.plot(x_hist[0,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('x (m)')
    plt.title('Position')
    plt.show()
        
    plt.figure(5)
    plt.plot(u_hist.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Force (N)')
    plt.title('Input')

    plt.pause(1000)
        
