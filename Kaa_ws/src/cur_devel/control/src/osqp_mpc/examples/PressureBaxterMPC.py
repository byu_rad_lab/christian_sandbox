#!/usr/bin/env python
import numpy as np
import math
import time
from copy import deepcopy
from rad_models.PressureBaxter import PressureBaxter
from osqp_mpc.OSQPMPC import OSQPMPC
import matplotlib.pyplot as plt

if __name__ == '__main__':

    sys = PressureBaxter()
    
    numStates = sys.numStates
    numInputs = sys.numInputs

    Q = 1.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1.0])
    Qf = 1.0*np.diag([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1.0])
    R = 0.000001*np.diag([1,1,1,1,1,1,1,1,1,1,1,1,1,1.0])        
    horizon = 100
    dt = .01

    pmin = 8
    pmax = 300
    umin = np.array([pmin]*14)
    umax = np.array([pmax]*14)
    xmin = np.array([pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,pmin,-np.inf,-np.inf,-np.inf,-np.inf,-np.inf,-np.inf,-np.inf,-np.pi,-np.pi,-np.pi,-np.pi,-np.pi,-np.pi,-np.pi])
    xmax = np.array([pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,pmax,np.inf,np.inf,np.inf,np.inf,np.inf,np.inf,np.inf,np.pi,np.pi,np.pi,np.pi,np.pi,np.pi,np.pi])

    mpc = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax)

    x = np.array([200.0,200,200,200,200,200,200,200,200,200,200,200,200,200, 0,0,0,0,0,0,0, 0,0,0,0,0,0,0]).reshape(28,1)
    u = np.zeros([sys.numInputs,1])
    uGoal = np.ones([sys.numInputs,1])*100.0
    xgoal = np.array([0.0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0, 1,1,1,1,1,1,1]).reshape(28,1)

    sim_length = 1000
    x_hist = np.zeros([sys.numStates,sim_length])
    u_hist = np.zeros([sys.numInputs,sim_length])

    [Ad,Bd,wd] = sys.calc_discrete_A_B_w(x,u,dt)
    np.set_printoptions(precision=1)    
    for i in range(0,sim_length):
        [newAd,newBd,newwd] = sys.calc_discrete_A_B_w(x,u,dt)
        Ad = newAd
        Bd = newBd

        # u = mpc.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal=u).reshape(14,1)
        u = mpc.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal=uGoal).reshape(14,1)
        xnext = Ad.dot(x) + Bd.dot(u) + wd
        # for j in xrange(0,14):
        #     if(xnext[j]<x[j]):
        #         xnext[j] = x[j] + (xnext[j]-x[j])*10.0
        x = xnext


        u_hist[:,i] = u.flatten()
        x_hist[:,i] = x.flatten()

    plt.figure(1)
    plt.ion()
    plt.plot(x_hist[0:14,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Pressure (KPa)')
    plt.title('Pressure')
    plt.show()
    
    plt.figure(2)
    plt.ion()
    plt.plot(x_hist[14:21,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Velocity (rad/s)')
    plt.title('Velocity')
    plt.show()

    plt.figure(4)
    plt.ion()
    plt.plot(x_hist[21:28,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Theta (rad)')
    plt.title('Position')
    plt.show()
        
    plt.figure(5)
    plt.plot(u_hist.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Torque (Nm)')
    plt.title('Input')

    import scipy.io as sio
    data_dict = {'x':x_hist,
                 'u':u_hist}
    sio.savemat('OSQPMPC_data.mat',data_dict)

    plt.pause(1000)
