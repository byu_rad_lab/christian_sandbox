import scipy.io as sio
import matplotlib.pyplot as plt
import numpy as np

if __name__=='__main__':

    data_ki = sio.loadmat('kaa_parameterized_osqp_with_ki_data.mat')
    data = sio.loadmat('kaa_parameterized_osqp_without_ki_data.mat')                
    data_ki = sio.loadmat('kaa_parameterized_mpc_data_1570660578.74.mat')
    data = sio.loadmat('kaa_parameterized_mpc_data_1570660785.46.mat')    

    t = np.squeeze(data['t'])
    p = np.squeeze(data['p'])    
    q = np.squeeze(data['q'])
    qd = np.squeeze(data['qd'])
    u = np.squeeze(data['u'])
    solve_times = np.squeeze(data['solve_times'])
    
    t_ki = np.squeeze(data_ki['t'])
    p_ki = np.squeeze(data_ki['p'])    
    q_ki = np.squeeze(data_ki['q'])
    qd_ki = np.squeeze(data_ki['qd'])
    u_ki = np.squeeze(data_ki['u'])
    solve_times_ki = np.squeeze(data_ki['solve_times'])

    plt.ion()
    plt.figure(1)
    for i in range(0,6):
        plt.subplot(3,2,(i+1))    
        plt.plot(t,q[:,i],'b')
        plt.plot(t_ki,q_ki[:,i],'r')
        plt.ylim(-1.25,1.25)
        plt.ylabel('Jt '+str(i)+' Angle (rad)')
        plt.show()

    plt.subplot(3,2,4)
    plt.xlabel('Time (s)')
    plt.subplot(3,2,5)
    plt.xlabel('Time (s)')

    plt.legend(['Parameterized OSQP w/o integrator','Parameterized OSQP w integrator'])

    plt.figure(2)
    for i in range(0,6):
        plt.subplot(3,2,(i+1))    
        plt.plot(t,u[:,(2*i):(2*i)+2],'b')
        plt.plot(t_ki,u_ki[:,(2*i):(2*i)+2],'r')        
        plt.ylabel('Jt '+str(i)+' Input (kPa)')        
        plt.show()
    plt.legend(['Parameterized OSQP w/o integrator','Parameterized OSQP w integrator'])

    plt.subplot(3,2,4)
    plt.xlabel('Time (s)')
    plt.subplot(3,2,5)
    plt.xlabel('Time (s)')
    
        
    print("median solve time: ",np.median(solve_times))
    print("mean solve time: ",np.mean(solve_times))    
    
    plt.pause(10000)
    
    
