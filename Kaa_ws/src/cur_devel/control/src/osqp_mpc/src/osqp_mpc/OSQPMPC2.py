import osqp
import numpy as np
import scipy as sp
import scipy.sparse as sparse
from scipy.linalg import expm
import time
try:
    import emosqp
except:
    print("No Pre-compiled solver found")

class OSQPMPC():
    def __init__(self,Q,Qf,R,Horizon,umin=[],umax=[],xmin=[],xmax=[]):
        self.Q = sparse.diags(Q.diagonal().tolist())
        self.Qf = sparse.diags(Qf.diagonal().tolist())
        self.R = sparse.diags(R.diagonal().tolist())
        self.numInputs = R.shape[0]
        self.numStates = Q.shape[0]
        self.Horizon = Horizon
        self.initialized_OSQP=False
        if umin==[]:
            self.umin = -np.inf*np.ones(self.numInputs)
        else:
            self.umin = np.reshape(umin,self.numInputs)
        if umax==[]:
            self.umax = np.inf*np.ones(self.numInputs)
        else:
            self.umax = np.reshape(umax,self.numInputs)
            
        if xmin==[]:
            self.xmin = -np.inf*np.ones(self.numStates)
        else:
            self.xmin = np.reshape(xmin,self.numStates)
        if xmax==[]:
            self.xmax = np.inf*np.ones(self.numStates)
        else:
            self.xmax = np.reshape(xmax,self.numStates)

        # Input and state inequality constraints
        self.Aineq = sparse.eye((self.Horizon+1)*self.numStates + self.Horizon*self.numInputs + 1)
        self.lineq = np.hstack([np.kron(np.ones(self.Horizon+1), self.xmin), np.kron(np.ones(self.Horizon), self.umin), 1.0])
        self.uineq = np.hstack([np.kron(np.ones(self.Horizon+1), self.xmax), np.kron(np.ones(self.Horizon), self.umax), 1.0])
            

    def generate_c_code(self,directory='./osqp_c_code'):
        if self.initialized_OSQP==False:
            print("You must first setup a problem before generating code")
        else:
            print("Generating C code...")            
            self.prob.codegen(directory,
                              parameters='matrices',
                              python_ext_name='emosqp',
                              force_rewrite=True)
            print("Finished generating C code")

    def solve_optimization(self,Ad,Bd,wd,x,xgoal,ugoal=[],use_c_solver=False):
        x = np.array(x).squeeze()
        if ugoal==[]:
            ugoal = np.array([0]*self.numInputs)
        else:
            ugoal = np.array(ugoal).flatten()
            
        xgoal = np.array(xgoal).flatten()
        self.A, self.l, self.u = self.get_linear_constraint_matrices(Ad,Bd,wd,x)
        self.P, self.q = self.get_objective_function_matrices(x,xgoal,ugoal)
        if self.initialized_OSQP==False:
            # Create an OSQP object
            self.prob = osqp.OSQP()
            
            self.prob.setup(self.P, self.q, self.A, self.l, self.u, warm_start=True, verbose=True)
            self.initialized_OSQP=True
        else:
            if(use_c_solver==True):
                emosqp.update_lin_cost(self.q)
                emosqp.update_bounds(self.l,self.u)
                emosqp.update_P_A(self.P.data,None,0,self.A.data,None,0)

            else:
                self.prob.update(l=self.l,u=self.u,q=self.q)
                self.prob.update(Ax=self.A.data,Px=self.P.data)
                
        if(use_c_solver==True):
            res = emosqp.solve()
            xstar = res[0]
        else:
            res = self.prob.solve()
            xstar = res.x[0:len(res.x)-1]

        return xstar
                
    def solve_for_u_trajectory(self,Ad,Bd,wd,x,xgoal,ugoal=[],use_c_solver=False):
        xstar = self.solve_optimization(Ad,Bd,wd,x,xgoal,ugoal,use_c_solver)

        x_traj = np.zeros([self.numStates,self.Horizon+1])
        u_traj = np.zeros([self.numInputs,self.Horizon])
        for i in range(0,self.numStates):
            x_traj[i] = xstar[i:self.numStates*(self.Horizon+1):self.numStates]
        for i in range(0,self.numInputs):
            u_traj[i] = xstar[self.numStates*(self.Horizon+1)+i:len(xstar):self.numInputs]

        return x_traj,u_traj

    def solve_for_next_u(self,Ad,Bd,wd,x,xgoal,ugoal=[],use_c_solver=False):
        xstar = self.solve_optimization(Ad,Bd,wd,x,xgoal,ugoal,use_c_solver)        

        return xstar[self.numStates*(self.Horizon+1):self.numStates*(self.Horizon+1)+self.numInputs]
    
    def get_objective_function_matrices(self,x,xgoal,ugoal):

        # Cast MPC problem to a QP: x = (x(0),x(1),...,x(N),u(0),...,u(N-1))
        # Quadratic objective
        P = sparse.block_diag([sparse.kron(sparse.eye(self.Horizon), self.Q),
                               self.Qf,
                               sparse.kron(sparse.eye(self.Horizon), self.R),0]).tocsc()
        # Linear objective
        q = np.hstack([np.kron(np.ones(self.Horizon), -self.Q.dot(xgoal)),
                       -self.Qf.dot(xgoal),
                       np.kron(np.ones(self.Horizon), -self.R.dot(ugoal)),0])
        return P,q

    def get_linear_constraint_matrices(self,Ad,Bd,wd,x):
        
        # Linear dynamics equality constraints
        self.Ax = sparse.kron(sparse.eye(self.Horizon+1),-sparse.eye(self.numStates)) + sparse.kron(sparse.eye(self.Horizon+1, k=-1), Ad)
        self.Bu = sparse.kron(sparse.vstack([sparse.csc_matrix((1, self.Horizon)), sparse.eye(self.Horizon)]), Bd)
        self.wd = sparse.kron(np.vstack([0,np.ones([self.Horizon,1])]), wd)
        self.Aeq = sparse.hstack([self.Ax, self.Bu, self.wd])
        self.leq = np.hstack([-x, np.zeros(self.Horizon*self.numStates)])
        self.ueq = self.leq
        
        # - OSQP constraints
        A = sparse.vstack([self.Aeq, self.Aineq]).tocsc()
        l = np.hstack([self.leq, self.lineq])
        u = np.hstack([self.ueq, self.uineq])

        return A, l, u

if __name__=='__main__':

    ###--------------------------DEFINING SYSTEM-------------------------------###
    # Mass Spring Damper system
    m = 2.5
    k = 1.
    b = .1
    A = np.matrix([[-b/m, -k/m],
                   [1., 0]])
    B = np.matrix([[1./m],
                   [0]])
    dt = .1
    Ad = expm(A*dt)
    Bd = np.matmul(np.linalg.inv(A),np.matmul(Ad-np.eye(Ad.shape[0]),B))
    wd = np.array([[2.0],[0.0]])*dt
    ###--------------------------DEFINING SYSTEM-------------------------------###

    
    ###--------------------------SETTING UP MPC-------------------------------###
    n = 2
    m = 1
    Q = 100.0*np.diag([1]*n)
    Qf = 1000.0*np.diag([1]*n)
    R = 0.0*np.eye(m)
    horizon = 100
    xmin = -10*np.ones(n)
    xmax = 10*np.ones(n)
    umin = -10*np.ones(m)
    umax = 10*np.ones(m)
    
    mpc = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax)
    ###--------------------------SETTING UP MPC-------------------------------###


    # ###--------------------------SOLVING MPC-------------------------------###    
    x = np.matrix([[0],
                   [10]])
    xgoal = np.matrix([[0],
                       [0]])
    solve_times = []
    for i in range(0,1):
        tic = time.time()
        x_traj1,u_traj1 = mpc.solve_for_u_trajectory(Ad,Bd,wd,x,xgoal)
        solve_times.append(time.time() - tic)

    print("Mean solve time: ",np.mean(solve_times))
    # x_traj1,u_traj1 = mpc.solve_for_u_trajectory(Ad,Bd,wd,x,xgoal)
    ###--------------------------SOLVING MPC-------------------------------###    


    ###--------------------------PLOTTING-------------------------------###        
    import matplotlib.pyplot as plt
    plt.figure(1)
    plt.plot(u_traj1[0,:])
    plt.title('Input force vs time')    
    plt.ion()    
    plt.show()

    plt.figure(2)
    plt.ion()
    plt.plot(np.array(x_traj1)[1,:])
    plt.title('Position vs time')
    plt.show()
    plt.pause(1000)
    ###--------------------------PLOTTING-------------------------------###            
