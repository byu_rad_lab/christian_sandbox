import osqp
import numpy as np
import scipy as sp
import scipy.sparse as sparse
from scipy.linalg import expm
import time
from copy import deepcopy
try:
    import emosqp
except:
    print("No Pre-compiled solver found")

class OSQPMPC():
    def __init__(self,Q,Qf,R,Horizon,numSegments=2,umin=[],umax=[],xmin=[],xmax=[]):
        self.Q = sparse.diags(Q.diagonal().tolist())
        self.Qf = sparse.diags(Qf.diagonal().tolist())
        self.R = sparse.diags(R.diagonal().tolist())
        self.numInputs = R.shape[0]
        self.numStates = Q.shape[0]
        self.Horizon = Horizon
        self.initialized_OSQP=False
        if umin==[]:
            self.umin = -np.inf*np.ones(self.numInputs)
        else:
            self.umin = np.reshape(umin,self.numInputs)
        if umax==[]:
            self.umax = np.inf*np.ones(self.numInputs)
        else:
            self.umax = np.reshape(umax,self.numInputs)
            
        if xmin==[]:
            self.xmin = -np.inf*np.ones(self.numStates)
        else:
            self.xmin = np.reshape(xmin,self.numStates)
        if xmax==[]:
            self.xmax = np.inf*np.ones(self.numStates)
        else:
            self.xmax = np.reshape(xmax,self.numStates)

        # Parameterized stuff
        self.numSegments = numSegments
        self.numKnotPoints = numSegments+1
        self.segmentLength = self.Horizon/self.numSegments
        self.coeffs = np.zeros([self.segmentLength,2])
        for i in range(0,self.segmentLength):
            self.coeffs[i,0] = 1.0 - float(i)/self.segmentLength
            self.coeffs[i,1] = float(i)/self.segmentLength

        # Input and state inequality constraints
        self.Aineq = sparse.block_diag([np.eye(self.numStates),np.eye(self.Horizon*self.numInputs)])
        self.lineq = np.hstack([np.zeros(self.numStates),np.kron(np.ones(self.Horizon), self.umin)])
        self.uineq = np.hstack([np.zeros(self.numStates),np.kron(np.ones(self.Horizon), self.umax)])
            
    def solve_optimization(self,Ad,Bd,wd,x,xgoal,ugoal=[],use_c_solver=False):
        x = np.array(x).squeeze()
        if ugoal==[]:
            ugoal = np.array([0.0]*self.numInputs)
        else:
            ugoal = np.array(ugoal)
            
        xgoal = np.array(xgoal)
        self.A, self.l, self.u = self.get_linear_constraint_matrices(Ad,Bd,wd,x)
        self.P, self.q = self.get_objective_function_matrices(Ad,Bd,wd,x,xgoal,ugoal)

        # Find nonzero entries of P
        # tol = 5e-1
        # self.P = np.array(self.P.todense())
        # print self.P
        # self.P[np.abs(self.P)<tol] = 0
        # print self.P        
        self.P = sparse.csc_matrix(self.P)
        print("\nP: \n",self.P.data)
        print("\nA: \n",self.A.data)        
        print("nnz P ",sparse.tril(self.P).getnnz())
        print("nnz A ",self.A.getnnz())
        print("total nnz: ",sparse.tril(self.P).getnnz()+self.A.getnnz())

        # Alist = np.squeeze(np.array(self.A.todense()).flatten())
        # Plist = np.squeeze(np.array(sparse.tril(self.P).todense()).flatten())
        # print "Entries in P: ",Plist.shape
        # print "Entries in A: ",Alist.shape        
        # Plist[Plist<tol] = 0
        # Alist[Alist<tol] = 0        
        # print "Nonzero entries in P: ",np.shape(np.nonzero(Plist))
        # print "Nonzero entries in A: ",np.shape(np.nonzero(Alist))
        # print "Nonzero entries in A+P: ",np.shape(np.nonzero(Alist))+np.shape(np.nonzero(Plist))
        
        if self.initialized_OSQP==False:
            self.prob = osqp.OSQP()
            self.prob.setup(self.P, self.q, self.A, self.l, self.u, warm_start=True, verbose=True)
            self.initialized_OSQP=True
        else:
            self.prob.update(l=self.l,u=self.u,q=self.q)
            self.prob.update(Ax=self.A.data,Px=self.P.data)

        res = self.prob.solve()
        xstar = res.x[0:len(res.x)]
        
        return xstar
                
    def solve_for_u_trajectory(self,Ad,Bd,wd,x,xgoal,ugoal=[],use_c_solver=False):
        xstar = self.solve_optimization(Ad,Bd,wd,x,xgoal,ugoal,use_c_solver)

        u_traj = np.zeros([self.numInputs,self.Horizon])
        for i in range(0,self.numInputs):
            u_traj[i] = xstar[self.numStates+i:len(xstar):self.numInputs]

        return u_traj

    def solve_for_next_u(self,Ad,Bd,wd,x,xgoal,ugoal=[],use_c_solver=False):
        xstar = self.solve_optimization(Ad,Bd,wd,x,xgoal,ugoal,use_c_solver)        

        return xstar[self.numStates*(self.Horizon+1):self.numStates*(self.Horizon+1)+self.numInputs]

    def get_M(self,A,B):
        self.M = np.zeros([(self.numInputs+self.numStates)*self.Horizon, self.numInputs*self.Horizon + self.numStates])
        self.M[self.numStates*self.Horizon:self.M.shape[0],self.numStates:self.M.shape[1]] = np.eye(self.numInputs*self.Horizon)
        self.M[0:self.numStates*self.Horizon,self.numStates:self.M.shape[1]] = np.kron(np.tril(np.ones([self.numInputs*self.Horizon,self.numInputs*self.Horizon])),B)

        # print "M: \n",self.M
        Aj = np.eye(A.shape[0])
        for j in range(0,self.Horizon*self.numStates,self.numStates):
            Aj = Aj.dot(A)
            self.M[j:j+self.numStates,0:self.numStates] = Aj

        for i in range(self.numStates,self.numInputs*self.Horizon+self.numStates,self.numInputs):
            Aj = np.eye(A.shape[0])
            for j in range((i-1)*self.numStates,self.Horizon*self.numStates,self.numStates):
                Aj = Aj.dot(A)
                self.M[j:j+self.numStates,i:i+self.numInputs] = Aj.dot(self.M[j:j+self.numStates,i:i+self.numInputs])
                # print "i = ",i,"j = ",j," M: \n",self.M
        return self.M
    
    def get_objective_function_matrices(self,Ad,Bd,wd,x,xgoal,ugoal):

        M = self.get_M(Ad,Bd)
        # print "A: ",Ad
        # print "B: ",Bd        
        # print "M: ",M
        M = sparse.csc_matrix(M)

        # Quadratic objective
        Qs = sparse.kron(sparse.eye(self.Horizon), self.Q)
        Rs = sparse.kron(sparse.eye(self.Horizon), self.R)

        P = sparse.block_diag([Qs,
                               Rs])
        P = M.T*P*M
        # Linear objective
        ugoals = np.kron(np.ones(self.Horizon), ugoal.T)
        xgoals = np.kron(np.ones(self.Horizon), xgoal.T)
        
        goals = sparse.hstack([xgoals,ugoals])

        q = sparse.block_diag([Qs,
                               Rs])
        q = q.todense()
        q = goals.dot(q)
        q = -q.dot(M.todense())
        return P,q.T

    def get_linear_constraint_matrices(self,Ad,Bd,wd,x):

        self.lineq[0:self.numStates] = x.flatten()
        self.uineq[0:self.numStates] = x.flatten()
        
        # - OSQP constraints
        
        A = self.Aineq
        l = np.array(self.lineq)
        u = np.array(self.uineq)

        return A, l, u

if __name__=='__main__':

    ###--------------------------DEFINING SYSTEM-------------------------------###
    # Mass Spring Damper system
    m = 2.5
    k = 1.
    b = 0.1
    A = np.matrix([[-b/m, -k/m],
                   [1., 0]])
    B = np.matrix([[1./m],
                   [0]])
    dt = .1
    Ad = expm(A*dt)
    Bd = np.matmul(np.linalg.inv(A),np.matmul(Ad-np.eye(Ad.shape[0]),B))
    wd = np.array([[0.0],[0.0]])*dt
    ###--------------------------DEFINING SYSTEM-------------------------------###

    
    ###--------------------------SETTING UP MPC-------------------------------###
    n = 2
    m = 1
    Q = 100.0*np.diag([0.0,1.0])
    Qf = 100.0*np.diag([0.0,1.0])
    R = 0.0*np.eye(m)
    horizon = 50
    numSegments = 1
    xmin = -10*np.ones(n)
    xmax = 10*np.ones(n)
    umin = -10*np.ones(m)
    umax = 10*np.ones(m)
    mpc = OSQPMPC(Q,Qf,R,horizon,numSegments,umin,umax,xmin,xmax)
    # mpc = OSQPMPC(Q,Qf,R,horizon,umin,umax)
    ###--------------------------SETTING UP MPC-------------------------------###


    ###--------------------------SOLVING MPC-------------------------------###    
    x = np.matrix([[0],
                   [0]])
    xgoal = np.matrix([[0],
                       [5]])
    
    u_traj = mpc.solve_for_u_trajectory(Ad,Bd,wd,x,xgoal)
    ###--------------------------SOLVING MPC-------------------------------###    


    ###--------------------------PLOTTING-------------------------------###
    x_traj = np.zeros([n,horizon+1])
    x_traj[:,0] = x.flatten()
    for i in range(0,horizon):
        x = Ad*x + Bd*np.matrix(u_traj[:,i].T)
        x_traj[:,i+1] = x.flatten()
        
    import matplotlib.pyplot as plt
    plt.figure(1)
    plt.plot(u_traj[0,:])
    plt.title('Input force vs time')    
    plt.ion()    
    plt.show()

    plt.figure(2)
    plt.ion()
    plt.plot(np.array(x_traj)[1,:])
    plt.title('Position vs time')
    plt.show()
    plt.pause(1000)
    ###--------------------------PLOTTING-------------------------------###            
