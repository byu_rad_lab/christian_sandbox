import osqp
import numpy as np
import scipy as sp
import scipy.sparse as sparse
from scipy.linalg import expm
from control_trajectory_parameterization.OSQPMPC import OSQPMPC
import time

try:
    import emosqp
except:
    print("No Pre-compiled solver found")

class ParameterizedOSQPMPC(OSQPMPC):
    def __init__(self,Q,Qf,R,Horizon,numSegments=4,umin=None,umax=None,xmin=None,xmax=None):
        self.Q = sparse.diags(Q.diagonal().tolist())
        self.Qf = sparse.diags(Qf.diagonal().tolist())
        self.R = sparse.diags(R.diagonal().tolist())
        self.numInputs = R.shape[0]
        self.numStates = Q.shape[0]
        self.Horizon = Horizon
        
        

        self.initialized_OSQP=False
        if umin==[]:
            self.umin = -np.inf*np.ones(self.numInputs)
        else:
            self.umin = np.reshape(umin,self.numInputs)
        if umax==[]:
            self.umax = np.inf*np.ones(self.numInputs)
        else:
            self.umax = np.reshape(umax,self.numInputs)
            
        if xmin==[]:
            self.xmin = -np.inf*np.ones(self.numStates)
        else:
            self.xmin = np.reshape(xmin,self.numStates)
        if xmax==[]:
            self.xmax = np.inf*np.ones(self.numStates)
        else:
            self.xmax = np.reshape(xmax,self.numStates)

        # Parameterized stuff
        self.numSegments = numSegments
        self.numKnotPoints = numSegments+1
        self.segmentLength = self.Horizon/self.numSegments

        self.coeffs = np.zeros([self.segmentLength,2])
        for i in range(0,self.segmentLength):
            self.coeffs[i,0] = 1.0 - float(i)/self.segmentLength
            self.coeffs[i,1] = float(i)/self.segmentLength

        self.Bu = sparse.lil_matrix(((self.Horizon+1)*self.numStates,self.numKnotPoints*self.numInputs))
        # Input and state inequality constraints
        self.Aineq = sparse.eye((self.Horizon+1)*self.numStates + self.numKnotPoints*self.numInputs)
        self.lineq = np.hstack([np.kron(np.ones(self.Horizon+1), self.xmin), np.kron(np.ones(self.numKnotPoints), self.umin)])
        self.uineq = np.hstack([np.kron(np.ones(self.Horizon+1), self.xmax), np.kron(np.ones(self.numKnotPoints), self.umax)])
            

    def get_objective_function_matrices(self,x,xgoal,ugoal):

        # Cast MPC problem to a QP: x = (x(0),x(1),...,x(N),u(0),...,u(N-1))
        # Quadratic objective
        P = sparse.block_diag([sparse.kron(sparse.eye(self.Horizon), self.Q),
                               self.Qf,
                               sparse.kron(sparse.eye(self.numKnotPoints), self.R)]).tocsc()
        # Linear objective
        q = np.hstack([np.kron(np.ones(self.Horizon), -self.Q.dot(xgoal)),
                       -self.Qf.dot(xgoal),
                       np.kron(np.ones(self.numKnotPoints), -self.R.dot(ugoal))])
        return P,q

    def get_linear_constraint_matrices(self,Ad,Bd,x):
        
        # Linear dynamics equality constraints
        self.Ax = sparse.kron(sparse.eye(self.Horizon+1),-sparse.eye(self.numStates)) + sparse.kron(sparse.eye(self.Horizon+1, k=-1), Ad)

        self.subB = sparse.kron(self.coeffs,Bd)

        for i in range(0,self.numSegments):
            self.Bu[self.numStates + self.segmentLength*self.numStates*i:self.numStates + self.segmentLength*self.numStates*(i+1), self.numInputs*i:self.numInputs*(i+2)] = self.subB
            
        self.Aeq = sparse.hstack([self.Ax, self.Bu.tocsc()])
        self.leq = np.hstack([-x, np.zeros(self.Horizon*self.numStates)])
        self.ueq = self.leq
        
        # - OSQP constraints
        A = sparse.vstack([self.Aeq, self.Aineq]).tocsc()
        l = np.hstack([self.leq, self.lineq])
        u = np.hstack([self.ueq, self.uineq])

        return A, l, u

    def solve_for_u_trajectory(self,Ad,Bd,x,xgoal,ugoal=[],use_c_solver=False):
        xstar = self.solve_optimization(Ad,Bd,x,xgoal,ugoal,use_c_solver)

        x_traj = np.zeros([self.numStates,self.Horizon+1])
        u_traj = np.zeros([self.numInputs,self.numKnotPoints])
        for i in range(0,self.numStates):
            x_traj[i] = xstar[i:self.numStates*(self.Horizon+1):self.numStates]
        for i in range(0,self.numInputs):
            u_traj[i] = xstar[self.numStates*(self.Horizon+1)+i : len(xstar):self.numInputs]
                
        return x_traj,u_traj
    

if __name__=='__main__':

    ###--------------------------DEFINING SYSTEM-------------------------------###
    # Mass Spring Damper system
    mass = 2.5
    k = 1.
    b = .1
    A = np.matrix([[-b/mass, -k/mass],
                   [1., 0]])
    B = np.matrix([[1./mass],
                   [0]])
    dt = .1
    Ad = expm(A*dt)
    Bd = np.matmul(np.linalg.inv(A),np.matmul(Ad-np.eye(Ad.shape[0]),B))
    n = 2
    m = 1
    ###--------------------------DEFINING SYSTEM-------------------------------###

    # ###--------------------------DEFINING SYSTEM-------------------------------###
    # # Mass Spring Damper system
    # n = 14
    # m = 14
    # A = -np.eye(n) + np.random.randn(n,n)*.1
                   
    # B = np.random.randn(n,m)
                   
    # dt = .1
    # Ad = expm(A*dt)
    # Bd = np.matmul(np.linalg.inv(A),np.matmul(Ad-np.eye(Ad.shape[0]),B))
    # ###--------------------------DEFINING SYSTEM-------------------------------###    
    

    
    
    ###--------------------------SETTING UP MPC-------------------------------###
    mydiag = []
    for i in range(0,n/2):
        mydiag.append(0)
    for i in range(0,n/2):
        mydiag.append(1)
    Q = 100.0*np.diag(mydiag)
    Qf = 100.0*np.diag(mydiag)
    R = 1.0*np.eye(m)*0
    horizon = 50
    xmin = -10*np.ones(n)
    xmax = 10*np.ones(n)
    umin = -10*np.ones(m)
    umax = 10*np.ones(m)

    #numSegments = 
    
    # Set up MPC object
    mpc = ParameterizedOSQPMPC(Q,Qf,R,horizon,numSegments,umin,umax,xmin,xmax)
    ###--------------------------SETTING UP MPC-------------------------------###


    
    ###--------------------------SOLVING MPC-------------------------------###
    x = -1*np.ones([n,1])
    xgoal = 0*np.ones([n,1])
    
    solve_times = []
    for i in range(0,1):
        xgoal[1,0] = np.random.randn()*3.0*0
        tic = time.time()
        x_traj1,u_traj1 = mpc.solve_for_u_trajectory(Ad,Bd,x,xgoal)
        solve_times.append(time.time() - tic)

    print("Mean solve time: ",np.mean(solve_times))
    ###--------------------------SOLVING MPC-------------------------------###    


    ###--------------------------PLOTTING-------------------------------###        
    import matplotlib.pyplot as plt
    plt.figure(1)
    plt.plot(u_traj1[0,:])
    plt.title('Input force vs time')    
    plt.ion()    
    plt.show()

    plt.figure(2)
    plt.ion()
    plt.plot(np.array(x_traj1)[1,:])
    plt.title('Position vs time')
    plt.show()
    plt.pause(1000)
    ###--------------------------PLOTTING-------------------------------###            
