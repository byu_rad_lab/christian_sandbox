import osqp
import numpy as np
import scipy
import scipy.sparse as sparse
from scipy.linalg import expm
import time

class OSQPMPC():
    def __init__(self,Q,Qf,R,Horizon,umin,umax,xmin,xmax,num_params=None,warm_start=True):
        
        self.Q = Q
        self.Qf = Qf
        self.R = R
        self.numInputs = R.shape[0]
        self.numStates = Q.shape[0]
        self.Horizon = Horizon
        if(num_params==None):
            self.num_params = Horizon + 1
        else:
            self.num_params = num_params
        self.initialized_OSQP=False
        self.umin = np.reshape(umin,[self.numInputs,1])
        self.umax = np.reshape(umax,[self.numInputs,1])
        self.xmin = np.reshape(xmin,[self.numStates,1])
        self.xmax = np.reshape(xmax,[self.numStates,1])
        self.warm_start = warm_start
            
    def solve_optimization(self,Ad,Bd,wd,x,xgoal,ugoal=[]):
        x = np.array(x).squeeze()
        if ugoal==[]:
            ugoal = np.array([0]*self.numInputs)
        else:
            ugoal = np.array(ugoal).flatten()
            
        xgoal = np.array(xgoal).flatten()

        P, q = self.get_objective_function_matrices(xgoal,ugoal)
        A, l, u = self.get_linear_constraint_matrices(Ad,Bd,wd,x,)

        if self.initialized_OSQP==False:
            P = sparse.csc_matrix(P)
            A = sparse.csc_matrix(A)            
            self.prob = osqp.OSQP()
            self.prob.setup(P, q, A, l, u, warm_start=self.warm_start, verbose=False)
            self.initialized_OSQP=True
        else:
            P = sparse.csc_matrix(np.triu(P))
            A = sparse.csc_matrix(A)                        
            self.prob.update(q=q,l=l,u=u)
            self.prob.update(Ax=A.data,Px=P.data)

        self.res = self.prob.solve()
        # print "Parameterized Large Matrix solve: ",time.time()-tic        
        # tic = time.time()
        
        xstar = self.res.x[0:len(self.res.x)]
        status_val = self.res.info.status_val            
        
        if(status_val != 1 and status_val != 2):
        # if(0):
            print("Failed to solve.")
            print(self.res.info.status)
            solved=False
        else:
            solved=True
            self.solve_time = self.res.info.run_time

        # print "Parameterized Large Matrix rest: ",time.time()-tic        
            
        return xstar,solved
                
    def solve_for_u_trajectory(self,Ad,Bd,wd,x,xgoal,ugoal=[]):
        xstar,solved = self.solve_optimization(Ad,Bd,wd,x,xgoal,ugoal)

        x_traj = np.zeros([self.numStates,self.Horizon+1])
        u_traj = np.zeros([self.numInputs,self.num_params])
        for i in range(0,self.numStates):
            x_traj[i] = xstar[i:self.numStates*(self.Horizon+1):self.numStates]
        for i in range(0,self.numInputs):
            u_traj[i] = xstar[self.numStates*(self.Horizon+1)+i:-1:self.numInputs]

        if solved:
            return x_traj,u_traj
        else:
            return None

    def solve_for_next_u(self,Ad,Bd,wd,x,xgoal,ugoal=[]):
        xstar,solved = self.solve_optimization(Ad,Bd,wd,x,xgoal,ugoal)        
        if solved:
            return xstar[self.numStates*(self.Horizon+1):self.numStates*(self.Horizon+1)+self.numInputs]
        else:
            return None

    def get_objective_function_matrices(self,xgoal,ugoal):

        P = scipy.linalg.block_diag(np.zeros([self.numStates,self.numStates]),
                                    np.kron(np.eye(self.Horizon-1), self.Q),
                                    self.Qf,
                                    np.kron(np.eye(self.num_params), self.R),0)
        
        q = np.hstack([np.zeros(self.numStates),
                       np.kron(np.ones(self.Horizon-1), -self.Q.dot(xgoal)),
                       -self.Qf.dot(xgoal),
                       np.kron(np.ones(self.num_params), -self.R.dot(ugoal)),
                       0])
        
        return P,q

    def get_linear_constraint_matrices(self,Ad,Bd,wd,x0):

        # Dynamics constraints
        Aleft = np.kron(np.eye(self.Horizon+1),-np.eye(self.numStates))
        Aleft += np.kron(np.eye(self.Horizon+1,k=-1),Ad)
        Aleft = Aleft[self.numStates:,:]

        delta_T = self.Horizon/(self.num_params-1.0)
        Amiddle = np.zeros([self.Horizon*self.numStates,self.num_params*self.numInputs])
        Amiddle[0:self.numStates,0:self.numInputs] = Bd
        for k in range(1,self.Horizon):
            idx = int(np.floor(k/delta_T))
            Amiddle[self.numStates*k:self.numStates*(k+1),self.numInputs*idx:self.numInputs*(idx+1)] = (1.0-k/delta_T + float(idx))*Bd
            Amiddle[self.numStates*k:self.numStates*(k+1),self.numInputs*(idx+1):self.numInputs*(idx+2)] = (k/delta_T - float(idx))*Bd

        Aright = np.kron(np.ones([self.Horizon,1]),wd)

        A_dynamics = np.hstack([Aleft,Amiddle,Aright])
        b_dynamics = np.zeros([self.numStates*self.Horizon,1])

        A_initial_condition = np.hstack([-np.eye(self.numStates),np.zeros([self.numStates,A_dynamics.shape[1]-self.numStates])])
        b_initial_condition = -np.array(x0).reshape(self.numStates,1)
        
        A_disturbance = np.hstack([np.zeros([1,A_dynamics.shape[1]-1]),np.ones([1,1])])
        b_disturbance = np.ones([1,1])
                                  

        # State and input constraints
        A_constraints = np.eye((self.Horizon+1)*self.numStates+self.num_params*self.numInputs)
        A_constraints = np.hstack([A_constraints,np.zeros([A_constraints.shape[0],1])])

        Bigxmin = np.kron(np.ones([self.Horizon+1,1]), self.xmin)
        Bigxmax = np.kron(np.ones([self.Horizon+1,1]), self.xmax)
        Bigumin = np.kron(np.ones([self.num_params,1]), self.umin)
        Bigumax = np.kron(np.ones([self.num_params,1]), self.umax)
        l_constraints = np.vstack([Bigxmin,Bigumin])
        u_constraints = np.vstack([Bigxmax,Bigumax])        

        A = np.vstack([A_dynamics,A_initial_condition,A_disturbance,A_constraints])
        l = np.vstack([b_dynamics,b_initial_condition,b_disturbance,l_constraints])
        u = np.vstack([b_dynamics,b_initial_condition,b_disturbance,u_constraints])

        return A, l, u

if __name__=='__main__':

    ###--------------------------DEFINING SYSTEM-------------------------------###
    # Mass Spring Damper system
    m = 1.0
    k = 1.0
    b = .05
    A = np.matrix([[-b/m, -k/m],
                   [1., 0]])
    B = np.matrix([[1./m],
                   [0]])
    dt = .01
    Ad = expm(A*dt)
    Bd = np.matmul(np.linalg.inv(A),np.matmul(Ad-np.eye(Ad.shape[0]),B))
    wd = np.array([[0.0],[0.0]])*dt
    ###--------------------------DEFINING SYSTEM-------------------------------###

    
    ###--------------------------SETTING UP MPC-------------------------------###
    n = 2
    m = 1
    Q = 1.0*np.diag([0.0,1.0])
    Qf = 1.0*np.diag([0.0,1.0])
    R = .0*np.eye(m)
    horizon = 100
    num_params = 2
    xmin = -10*np.ones(n)*np.inf
    xmax = 10*np.ones(n)*np.inf
    umin = -5*np.ones(m)
    umax = 5*np.ones(m)
    
    mpc = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax,num_params)
    ###--------------------------SETTING UP MPC-------------------------------###


    # ###--------------------------SOLVING MPC-------------------------------###    
    x = np.matrix([[0],
                   [-1.0]])
    xgoal = np.matrix([[0],
                       [2]])

    sim_length = 1000
    x_traj = np.zeros([n,sim_length+1])
    x_traj[:,0] = x.flatten()
    u_traj = np.zeros([m,sim_length])
    for i in range(0,sim_length):
        tic = time.time()
        # x_traj1,u_traj1 = mpc.solve_for_u_trajectory(Ad,Bd,wd,x,xgoal)
        u_star = mpc.solve_for_next_u(Ad,Bd,wd,x,xgoal)
        
        x = Ad.dot(x).reshape(n,1) + Bd.dot(u_star).reshape(n,1) + wd.reshape(n,1)

        print("x after: ",x.T)                
        print(u_star)
        x_traj[:,i+1] = x.flatten()
        u_traj[:,i] = u_star.flatten()        

    # print "Mean solve time: ",np.mean(solve_times)
    x_traj1,u_traj1 = mpc.solve_for_u_trajectory(Ad,Bd,wd,x,xgoal)
    ###--------------------------SOLVING MPC-------------------------------###    


    ###--------------------------PLOTTING-------------------------------###        
    import matplotlib.pyplot as plt
    plt.figure(1)
    plt.plot(u_traj.T)
    plt.title('Input force vs time')    
    plt.ion()    
    plt.show()

    plt.figure(2)
    plt.ion()
    plt.plot(x_traj.T)
    plt.title('Position vs time')
    plt.show()
    plt.pause(1000)
    ###--------------------------PLOTTING-------------------------------###            
