import osqp
import numpy as np
import scipy
from scipy.linalg import expm
import time

class OSQPMPC():
    def __init__(self,Q,Qf,R,Horizon,umin=None,umax=None,xmin=None,xmax=None):
        self.Q = Q
        self.Qf = Qf
        self.R = R
        self.numInputs = R.shape[0]
        self.numStates = Q.shape[0]
        self.Horizon = Horizon
        self.initialized_OSQP=False
        if umin==[]:
            self.umin = -np.inf*np.ones(self.numInputs)
        else:
            self.umin = np.reshape(umin,self.numInputs)
        if umax==[]:
            self.umax = np.inf*np.ones(self.numInputs)
        else:
            self.umax = np.reshape(umax,self.numInputs)
            
        if xmin==[]:
            self.xmin = -np.inf*np.ones(self.numStates)
        else:
            self.xmin = np.reshape(xmin,self.numStates)
        if xmax==[]:
            self.xmax = np.inf*np.ones(self.numStates)
        else:
            self.xmax = np.reshape(xmax,self.numStates)

        # Input and state inequality constraints
        self.Aineq = np.eye((self.Horizon+1)*self.numStates + self.Horizon*self.numInputs + 1)
        self.lineq = np.hstack([np.kron(np.ones(self.Horizon+1), self.xmin), np.kron(np.ones(self.Horizon), self.umin), 1.0])
        self.uineq = np.hstack([np.kron(np.ones(self.Horizon+1), self.xmax), np.kron(np.ones(self.Horizon), self.umax), 1.0])
            

    def generate_c_code(self,directory='./osqp_c_code'):
        if self.initialized_OSQP==False:
            print("You must first setup a problem before generating code")
        else:
            print("Generating C code...")            
            self.prob.codegen(directory,
                              parameters='matrices',
                              python_ext_name='emosqp',
                              force_rewrite=True)
            print("Finished generating C code")

    def solve_optimization(self,Ad,Bd,wd,x,xgoal,ugoal=[],use_c_solver=False):
        x = np.array(x).squeeze()
        if ugoal==[]:
            ugoal = np.array([0]*self.numInputs)
        else:
            ugoal = np.array(ugoal).flatten()
            
        xgoal = np.array(xgoal).flatten()
        self.A, self.l, self.u = self.get_linear_constraint_matrices(Ad,Bd,wd,x)
        self.P, self.q = self.get_objective_function_matrices(xgoal,ugoal)        
        self.A = scipy.sparse.csc_matrix(self.A)
        self.P = scipy.sparse.csc_matrix(self.P)        

        if self.initialized_OSQP==False:
            # Create an OSQP object
            self.prob = osqp.OSQP()
            
            self.prob.setup(self.P, self.q, self.A, self.l, self.u, warm_start=True, verbose=False)
            self.initialized_OSQP=True
        else:
            self.P = scipy.sparse.triu(self.P)
            # self.prob.update_settings(max_iter=500)
            # self.prob.update_settings(time_limit=.01)
            if(use_c_solver==True):
                emosqp.update_lin_cost(self.q)
                emosqp.update_bounds(self.l,self.u)
                emosqp.update_P_A(self.P.data,None,0,self.A.data,None,0)

            else:
                self.prob.update(l=self.l,u=self.u,q=self.q)
                self.prob.update(Ax=self.A.data,Px=self.P.data)
                
                
        if(use_c_solver==True):
            res = emosqp.solve()
            xstar = res[0]
            status_val = res.info.status_val
        else:
            res = self.prob.solve()
            xstar = res.x[0:len(res.x)-1]
            status_val = res.info.status_val            

        # if(status_val != 1 and status_val != 2):
        if(0):
            print("Failed to solve.")
            print(res.info.status)
            solved=False
        else:
            solved=True
            self.solve_time = res.info.run_time
            # print "OSQP Total time: ",self.solve_time
            
        return xstar,solved
                
    def solve_for_u_trajectory(self,Ad,Bd,wd,x,xgoal,ugoal=[],use_c_solver=False):
        xstar,solved = self.solve_optimization(Ad,Bd,wd,x,xgoal,ugoal,use_c_solver)

        x_traj = np.zeros([self.numStates,self.Horizon+1])
        u_traj = np.zeros([self.numInputs,self.Horizon])
        for i in range(0,self.numStates):
            x_traj[i] = xstar[i:self.numStates*(self.Horizon+1):self.numStates]
        for i in range(0,self.numInputs):
            u_traj[i] = xstar[self.numStates*(self.Horizon+1)+i:len(xstar):self.numInputs]

        if solved:
            return x_traj,u_traj
        else:
            return None

    def solve_for_next_u(self,Ad,Bd,wd,x,xgoal,ugoal=[],use_c_solver=False):
        xstar,solved = self.solve_optimization(Ad,Bd,wd,x,xgoal,ugoal,use_c_solver)        
        if solved:
            return xstar[self.numStates*(self.Horizon+1):self.numStates*(self.Horizon+1)+self.numInputs]
        else:
            return None
    
    def get_objective_function_matrices(self,xgoal,ugoal):

        # Cast MPC problem to a QP: x = (x(0),x(1),...,x(N),u(0),...,u(N-1))
        # Quadratic objective
        self.P = scipy.linalg.block_diag(np.zeros([self.numStates,self.numStates]),
                                    np.kron(np.eye(self.Horizon-1), self.Q),
                                    self.Qf,
                                    np.kron(np.eye(self.Horizon), self.R),0)
        # Linear objective
        self.q = np.hstack([np.zeros(self.numStates),
                       np.kron(np.ones(self.Horizon-1), -self.Q.dot(xgoal)),
                       -self.Qf.dot(xgoal),
                       np.kron(np.ones(self.Horizon), -self.R.dot(ugoal)),
                       0])
        return self.P,self.q

    def get_linear_constraint_matrices(self,Ad,Bd,wd,x):
        
        # Linear dynamics equality constraints
        self.Ax = np.kron(np.eye(self.Horizon+1),-np.eye(self.numStates)) + np.kron(np.eye(self.Horizon+1, k=-1), Ad)
        self.Bu = np.kron(np.vstack([np.zeros((1, self.Horizon)), np.eye(self.Horizon)]), Bd)
        self.wd = np.kron(np.vstack([0,np.ones([self.Horizon,1])]), wd)
        self.Aeq = np.hstack([self.Ax, self.Bu, self.wd])
        self.leq = np.hstack([-x, np.zeros(self.Horizon*self.numStates)])
        self.ueq = self.leq
        
        # - OSQP constraints
        self.A = np.vstack([self.Aeq, self.Aineq])
        self.l = np.hstack([self.leq, self.lineq])
        self.u = np.hstack([self.ueq, self.uineq])

        return self.A, self.l, self.u

if __name__=='__main__':

    ###--------------------------DEFINING SYSTEM-------------------------------###
    # Mass Spring Damper system
    m = 1.0
    k = 1.
    b = .05
    A = np.matrix([[-b/m, -k/m],
                   [1., 0]])
    B = np.matrix([[1./m],
                   [0]])
    dt = .01
    Ad = expm(A*dt)
    Bd = np.matmul(np.linalg.inv(A),np.matmul(Ad-np.eye(Ad.shape[0]),B))
    # Ad = np.linalg.inv(np.eye(A.shape[0]) - A*dt)
    # Bd = np.matmul(Ad,B)*dt
    wd = np.array([[0.0],[0.0]])*dt
    ###--------------------------DEFINING SYSTEM-------------------------------###

    
    ###--------------------------SETTING UP MPC-------------------------------###
    n = 2
    m = 1
    Q = 1.0*np.diag([0.0,1.0])
    Qf = 1.0*np.diag([1.0,1.0])
    R = 0.0*np.eye(m)
    horizon = 500
    xmin = -10*np.ones(n)*np.inf
    xmax = 10*np.ones(n)*np.inf
    umin = -5*np.ones(m)
    umax = 5*np.ones(m)
    
    mpc = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax)
    ###--------------------------SETTING UP MPC-------------------------------###


    # ###--------------------------SOLVING MPC-------------------------------###    
    x = np.matrix([[0],
                   [-1]])
    xgoal = np.matrix([[0],
                       [2]])
    # solve_times = []
    # for i in xrange(0,1):
    #     tic = time.time()
    #     x_traj1,u_traj1 = mpc.solve_for_u_trajectory(Ad,Bd,wd,x,xgoal)
    #     solve_times.append(time.time() - tic)

    # print "Mean solve time: ",np.mean(solve_times)
    x_traj1,u_traj1 = mpc.solve_for_u_trajectory(Ad,Bd,wd,x,xgoal)
    ###--------------------------SOLVING MPC-------------------------------###    


    ###--------------------------PLOTTING-------------------------------###        
    import matplotlib.pyplot as plt
    plt.figure(1)
    plt.plot(u_traj1.T)
    plt.title('Input force vs time')    
    plt.ion()    
    plt.show()

    plt.figure(2)
    plt.ion()
    plt.plot(x_traj1.T)
    plt.title('Position vs time')
    plt.show()
    plt.pause(1000)
    ###--------------------------PLOTTING-------------------------------###            
