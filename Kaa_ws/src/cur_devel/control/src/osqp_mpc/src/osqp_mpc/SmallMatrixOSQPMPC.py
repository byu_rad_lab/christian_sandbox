import osqp
import numpy as np
import scipy as sp
import scipy.sparse as sparse
from scipy.linalg import expm
import time

class OSQPMPC():
    def __init__(self,Q,Qf,R,Horizon,umin,umax,xmin,xmax,warm_start=True):
        
        self.Q = Q
        self.Qf = Qf
        self.R = R
        self.numInputs = R.shape[0]
        self.numStates = Q.shape[0]
        self.Horizon = Horizon
        self.initialized_OSQP=False
        self.umin = np.reshape(umin,self.numInputs)
        self.umax = np.reshape(umax,self.numInputs)
        self.xmin = np.reshape(xmin,self.numStates)
        self.xmax = np.reshape(xmax,self.numStates)
        self.warm_start = warm_start
            
    def solve_optimization(self,Ad,Bd,wd,x,xgoal,ugoal=[]):
        x = np.array(x).squeeze()
        if ugoal==[]:
            ugoal = np.array([0]*self.numInputs)
        else:
            ugoal = np.array(ugoal).flatten()
            
        P, q = self.get_objective_function_matrices(Ad,Bd,wd,x,xgoal,ugoal)
        A, l, u = self.get_linear_constraint_matrices()

        if self.initialized_OSQP==False:
            P = sparse.csc_matrix(P)
            A = sparse.csc_matrix(A)            
            self.prob = osqp.OSQP()
            self.prob.setup(P, q, A, l, u, warm_start=self.warm_start, verbose=False)
            self.initialized_OSQP=True
        else:
            P = sparse.csc_matrix(np.triu(P))
            A = sparse.csc_matrix(A)
            self.prob.update(q=q,l=l,u=u)
            self.prob.update(Ax=A.data,Px=P.data)

            
        self.res = self.prob.solve()
        xstar = self.res.x[0:len(self.res.x)]
        status_val = self.res.info.status_val            
        
        if(status_val != 1 and status_val != 2):
        # if(0):
            print("Failed to solve.")
            print(self.res.info.status)
            solved=False
        else:
            solved=True
            self.solve_time = self.res.info.run_time
            
        return xstar,solved
                
    def solve_for_u_trajectory(self,Ad,Bd,wd,x,xgoal,ugoal=[]):
        xstar,solved = self.solve_optimization(Ad,Bd,wd,x,xgoal,ugoal)

        x_traj = np.zeros([self.numStates,self.Horizon+1])
        u_traj = np.zeros([self.numInputs,self.Horizon])
        for i in range(0,self.numInputs):
            u_traj[i] = xstar[i:len(xstar):self.numInputs]

        if solved:
            return x_traj,u_traj
        else:
            return None

    def solve_for_next_u(self,Ad,Bd,wd,x,xgoal,ugoal=[]):
        xstar,solved = self.solve_optimization(Ad,Bd,wd,x,xgoal,ugoal)        
        if solved:
            return xstar[0:self.numInputs]
        else:
            return None

    def calculate_S_and_V(self,A,B,w,x0):
        S = np.zeros([self.Horizon*self.numStates,self.Horizon*self.numInputs])
        V = np.zeros([self.Horizon*self.numStates,1])
        
        S[0:self.numStates,0:self.numInputs] = B
        V[0:self.numStates,:] = A.dot(x0).reshape(self.numStates,1) + w.reshape(self.numStates,1)

        for k in range(1,self.Horizon):
            S[self.numStates*k:self.numStates*(k+1),self.numInputs*(k):self.numInputs*(k+1)] = B
            S[self.numStates*k:self.numStates*(k+1),:] += A.dot(S[self.numStates*(k-1):self.numStates*(k),:])

            V[self.numStates*k:self.numStates*(k+1),:] = A.dot(V[self.numStates*(k-1):self.numStates*(k),:]) + w

        self.S = S
        self.V = V
    
    def get_objective_function_matrices(self,A,B,w,x0,xgoal,ugoal):

        self.calculate_S_and_V(A,B,w,x0)
        BigQ = np.kron(np.eye(self.Horizon), self.Q)
        BigQ[-self.numStates:,-self.numStates:] = self.Qf
        BigR = np.kron(np.eye(self.Horizon), self.R)
        
        P = self.S.T.dot(BigQ).dot(self.S) + BigR

        if(xgoal.size==self.numStates):
            xgoal = xgoal.flatten()            
            BigQxgoal = np.kron(np.ones(self.Horizon), self.Q.dot(xgoal)).reshape(self.numStates*self.Horizon,1)
        else:
            BigQxgoal = np.zeros([self.numStates*self.Horizon,1])
            for i in range(0,self.Horizon):
                BigQxgoal[self.numStates*i:self.numStates*(i+1)] = self.Q.dot(xgoal[:,i]).reshape(-1,1)
        
        BigRugoal = np.kron(np.ones(self.Horizon), self.R.dot(ugoal)).reshape(self.Horizon*self.numInputs,1)

        q = self.S.T.dot(BigQ).dot(self.V) - self.S.T.dot(BigQxgoal) - BigRugoal

        return P,np.squeeze(q)

    def get_linear_constraint_matrices(self):

        # TODO - This is ignoring state constraints
        Bigumin = np.kron(np.ones(self.Horizon), self.umin.flatten())
        Bigumax = np.kron(np.ones(self.Horizon), self.umax.flatten())
        A = np.eye((self.Horizon)*self.numInputs)
        l = Bigumin
        u = Bigumax

        return A, l, u

if __name__=='__main__':

    ###--------------------------DEFINING SYSTEM-------------------------------###
    # Mass Spring Damper system
    m = 1.0
    k = 1.0
    b = .05
    A = np.matrix([[-b/m, -k/m],
                   [1., 0]])
    B = np.matrix([[1./m],
                   [0]])
    dt = .01
    Ad = expm(A*dt)
    Bd = np.matmul(np.linalg.inv(A),np.matmul(Ad-np.eye(Ad.shape[0]),B))
    wd = np.array([[0.0],[0.0]])*dt
    ###--------------------------DEFINING SYSTEM-------------------------------###

    
    ###--------------------------SETTING UP MPC-------------------------------###
    n = 2
    m = 1
    Q = 1.0*np.diag([0.0,1.0])
    Qf = 1.0*np.diag([1.0,1.0])
    R = 0.0*np.eye(m)
    horizon = 1000
    xmin = -10*np.ones(n)*np.inf
    xmax = 10*np.ones(n)*np.inf
    umin = -5*np.ones(m)
    umax = 5*np.ones(m)
    
    mpc = OSQPMPC(Q,Qf,R,horizon,umin,umax,xmin,xmax)
    ###--------------------------SETTING UP MPC-------------------------------###


    # ###--------------------------SOLVING MPC-------------------------------###    
    x = np.matrix([[0],
                   [1.0]])
    xgoal = np.matrix([[0],
                       [1]])
    ugoal = np.array([0])

    # sim_length = 1000
    # x_traj = np.zeros([n,sim_length+1])
    # x_traj[:,0] = x.flatten()
    # u_traj = np.zeros([m,sim_length])
    # for i in xrange(0,sim_length):
    #     tic = time.time()
    #     # x_traj1,u_traj1 = mpc.solve_for_u_trajectory(Ad,Bd,wd,x,xgoal)
    #     u_star = mpc.solve_for_next_u(Ad,Bd,wd,x,xgoal,ugoal)
        
    #     x = Ad.dot(x).reshape(n,1) + Bd.dot(u_star).reshape(n,1) + wd.reshape(n,1)

    #     print "x after: ",x.T                
    #     print u_star
    #     x_traj[:,i+1] = x.flatten()
    #     u_traj[:,i] = u_star.flatten()        

    # print "Mean solve time: ",np.mean(solve_times)
    x_traj1,u_traj1 = mpc.solve_for_u_trajectory(Ad,Bd,wd,x,xgoal)
    ###--------------------------SOLVING MPC-------------------------------###    


    ###--------------------------PLOTTING-------------------------------###        
    import matplotlib.pyplot as plt
    plt.figure(1)
    plt.plot(u_traj1.T)
    plt.title('Input force vs time')    
    plt.ion()    
    plt.show()

    plt.figure(2)
    plt.ion()
    plt.plot(x_traj1.T)
    plt.title('Position vs time')
    plt.show()
    plt.pause(1000)
    ###--------------------------PLOTTING-------------------------------###            
