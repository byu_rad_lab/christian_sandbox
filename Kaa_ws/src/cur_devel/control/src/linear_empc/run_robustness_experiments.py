import os
import numpy as np

num_trials = 100
max_links = 13
errors = [.25,.5,.75,1,1.5,2.0,4.0]

for trial in range(1000,1000+num_trials):

    for n in range(1,max_links+1):
        x0 = np.random.rand(n,1)*np.pi*2 - np.pi
        xgoal = np.random.rand(n,1)*np.pi*2 - np.pi
        
        x0_xgoal_string = ''
        xgoal_string = ''
        for i in range(0,n):
            x0_xgoal_string += str(x0[i,0])+'  '
        for i in range(0,n):
            x0_xgoal_string += str(xgoal[i,0])+'  '
            xgoal_string += str(xgoal[i,0])+'  '

        for error in errors:            
            
            print("Trial: ",trial,"Error: ",error,"  Number of links: ",n)
            
            # Run LEMPC and record the trajectory, solve times, and xgoal
            os.system('rosrun linear_empc n'+str(n)+'link_empc '+x0_xgoal_string+' '+str(error))
            # os.system('rosrun linear_empc n8link_empc '+x0_xgoal_string)        
            
            os.system('python csv2mat.py '+str(n)+' '+str(trial)+' '+xgoal_string+' '+str(error))
            
            # Run OSQPMPC and record the trajectory, solve times, and xgoal
            os.system('python ../../src/osqp_mpc/examples/nlinkMPC.py '+str(n)+' '+str(trial)+' '+x0_xgoal_string+' '+str(error))
