import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio

if __name__=='__main__':
    n = 6
    trial = '1044'
    lempc_data = sio.loadmat(str(n)+'link_lempc_data_error_1_'+trial+'.mat')
    osqp_data = sio.loadmat(str(n)+'link_osqp_data_error_1.0_'+trial+'.mat')
    osqp_p_data = sio.loadmat(str(n)+'link_small_osqp_data_error_1.0_'+trial+'.mat')

    x_lempc = lempc_data['x']
    u_lempc = lempc_data['u']
    x_osqp = osqp_data['x']
    u_osqp = osqp_data['u']    
    x_osqp_p = osqp_p_data['x']
    u_osqp_p = osqp_p_data['u']
    t = np.linspace(0,5,500)

    plt.figure(1)
    for i in range(0,n):
        plt.subplot(n/2,2,i+1)
        plt.plot(t,x_lempc[:,n+i],'g')
        plt.plot(t,x_osqp[:,n+i],'k')
        plt.plot(t,x_osqp_p[:,n+i],'r')
        plt.ylabel('Jt'+str(i)+' Angle (rad)')
        # plt.ylim([-np.pi,np.pi])
    plt.xlabel('Time (s)')
    plt.ion()
    plt.show()
    plt.legend(['EMPC','OSQPMPC','Parameterized OSQPMPC'],loc='upper right')

    plt.figure(2)
    for i in range(0,n):
        plt.subplot(n/2,2,i+1)
        plt.plot(t,u_lempc[:,i])
        plt.plot(t,u_osqp[:,i])
        plt.plot(t,u_osqp_p[:,i])
        plt.ylabel('Jt'+str(i)+' Torque (Nm)')        
    plt.xlabel('Time (s)')        
    plt.ion()
    plt.show()
    plt.legend(['EMPC','OSQPMPC','Parameterized OSQPMPC'],loc='lower right')

    plt.pause(50000)
