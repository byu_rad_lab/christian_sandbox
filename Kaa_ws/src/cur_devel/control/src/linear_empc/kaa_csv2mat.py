import csv  
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
import time
import sys

if __name__ == "__main__":
    data = [ ]
    with open('kaa_lempc_data.txt') as f:        
        reader = csv.reader(f)
        for row in reader:
            rowData = [ float(elem) for elem in row ]
            data.append(rowData)

    matrix = np.array(data)
    t = matrix[247:,37]/1000.0-30.0
    p = matrix[247:,0:12]
    qd = matrix[247:,12:18]
    q = matrix[247:,18:24]    
    u = matrix[247:,24:36]
    solve_times = matrix[247:,36]/1000.0

    sio.savemat('kaa_lempc_data',
                {'t':t,
                 'p':p,
                 'qd':qd,
                 'q':q,
                 'u':u,
                 'solve_times':solve_times})

    plt.ion()
    plt.figure(1)
    for i in range(0,6):
        plt.subplot(3,2,(i+1))    
        plt.plot(t,q[:,i],'b')
        plt.ylim(-1.25,1.25)
        plt.show()

    plt.figure(2)
    for i in range(0,6):
        plt.subplot(3,2,(i+1))
        plt.plot(t,u[:,2*i+0],'b')
        plt.plot(t,u[:,2*i+1],'r')        
        plt.show()

    plt.figure(3)
    for i in range(0,6):
        plt.subplot(3,2,(i+1))    
        plt.plot(t,qd[:,i],'b')
        plt.show()

    plt.figure(4)
    for i in range(0,6):
        plt.subplot(3,2,(i+1))    
        plt.plot(t,p[:,2*i+0],'b')
        plt.plot(t,p[:,2*i+1],'r')        
        plt.show()
        
    print("median solve time: ",np.median(solve_times))
    print("mean solve time: ",np.mean(solve_times))    
    
    plt.pause(10000)
