import csv  
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
import time
import sys

if __name__ == "__main__":
    n = 8
    argc = len(sys.argv)
    if(argc > 1):
        n = eval(sys.argv[1])
    if(argc > 2):
        trial = sys.argv[2]

    xgoal = np.zeros([2*n,1])
    if(argc > 3):
        for i in range(0,n):
            xgoal[n+i] = eval(sys.argv[3+i])

    error = 1
    if(argc > n+3):
        error = eval(sys.argv[n+3])

    data = [ ]
    with open('nlinkdata.txt') as f:
        reader = csv.reader(f)
        for row in reader:
            rowData = [ float(elem) for elem in row ]
            data.append(rowData)

    matrix = np.array(data)
    x = matrix[:,0:n*2]
    u = matrix[:,n*2:n*3]
    solve_times = matrix[:,n*3]
    sio.savemat(str(n)+'link_lempc_data_error_'+str(error)+'_'+trial,
                {'x':x,
                 'xgoal':xgoal,
                 'u':u,
                 'solve_times':solve_times})

    # # plt.figure(1)
    # # plt.plot(matrix[:,0:n])
    # # plt.ion()
    # # plt.show()

    # plt.figure(2)
    # plt.plot(matrix[:,n:n*2])
    # plt.ion()
    # plt.show()
    # plt.title('LEMPC Positions')
    
    # plt.figure(3)
    # plt.plot(matrix[:,n*2:n*3])
    # plt.ion()
    # plt.show()
    # plt.title('LEMPC Inputs')    
    
    # # plt.figure(4)
    # # plt.plot(matrix[:,n*4:n*6])
    # # plt.ion()
    # # plt.show()

    # plt.pause(5)
    

    
