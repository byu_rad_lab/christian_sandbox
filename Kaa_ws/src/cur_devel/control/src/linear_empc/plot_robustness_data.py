import os
import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt

def get_num_links_from_file(myfile):
    data = sio.loadmat(myfile)
    u_traj = data['u']
    n = u_traj.shape[1]
    return n

def get_solve_times_from_file(myfile):
    data = sio.loadmat(myfile)
    solve_times = data['solve_times'].flatten().tolist()
    return solve_times

def get_error_from_file(myfile):
    return eval(myfile[myfile.find('error')+6 : myfile.find('_' , myfile.find('error')+6)])

def get_total_cost_from_file(myfile):
    data = sio.loadmat(myfile)
    xgoal = data['xgoal'].T
    x_traj = data['x']
    u_traj = data['u']

    sim_length = u_traj.shape[0]
    n = u_traj.shape[1]
    
    Q = 0.0*np.eye(n*2)
    for i in range(n,n*2):
        Q[i,i] = 1.0/(5.0*(i+1-n))
    
    R = 0.0*np.eye(n)
    for i in range(0,n):
        R[i,i] = .001

    total_cost = 0
    for i in range(0,sim_length):
        total_cost += (x_traj[i,:]-xgoal).dot(Q).dot((x_traj[i,:]-xgoal).T) + u_traj[i,:].T.dot(R).dot(u_traj[i,:])

    return total_cost.flatten()[0]

def draw_boxplot(data,positions, edge_color, fill_color):
    
    bp = plt.boxplot(data,positions=positions,patch_artist=True,sym=edge_color+'+')

    for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
        plt.setp(bp[element], color=edge_color)

    for patch in bp['boxes']:
        patch.set(facecolor=fill_color)   


if __name__ == "__main__":

    errors = [.25, .5, .75, 1.0, 1.5, 2.0, 4.0]

    osqp_costs = np.zeros([13,len(errors),1000])
    lempc_costs = np.zeros([13,len(errors),1000])    

    all_files = os.listdir('./')
    osqp_files = []
    lempc_files = []    
        
    for thisfile in all_files:
        if thisfile[-4:] == '.mat':
            if(thisfile.find('lempc') != -1):
                lempc_files.append(thisfile)
            elif(thisfile.find('osqp') != -1):
                osqp_files.append(thisfile)

    osqp_files.sort()
    lempc_files.sort()

    num_trials = 0
    for i in range(0,len(lempc_files)):
        print("Finding match for ",lempc_files[i])
        match = 0
        lempc_trial = lempc_files[i][-8:-4]
        lempc_n = get_num_links_from_file(lempc_files[i])
        lempc_error = get_error_from_file(lempc_files[i])
        
        for j in range(0,len(osqp_files)):
            osqp_trial = osqp_files[j][-8:-4]
            osqp_n = get_num_links_from_file(osqp_files[j])
            osqp_error = get_error_from_file(osqp_files[j])

            if(lempc_trial == osqp_trial and lempc_n == osqp_n and lempc_error == osqp_error):
                match=1
                trial = eval(lempc_trial)-1000
                n = lempc_n
                error = lempc_error
                if(trial > num_trials):
                    num_trials = trial
                break
            
        if(match==1):
            lempc_cost = get_total_cost_from_file(lempc_files[i])
            osqp_cost = get_total_cost_from_file(osqp_files[j])

            error_idx = 0
            for k in range(0,len(errors)):
                if lempc_error == errors[k]:
                    error_idx = k
                    break
            osqp_costs[n-1,error_idx,trial] = osqp_cost
            lempc_costs[n-1,error_idx,trial] = lempc_cost

    num_trials =+ 1 
    osqp_costs = osqp_costs[:,:,0:num_trials]
    lempc_costs = lempc_costs[:,:,0:num_trials]    
            
    # Normalize the costs
    print("Normalizing...")
    for i in range(0,13):
        for j in range(0,len(errors)):
            for k in range(0,len(osqp_costs[i][len(errors)-1])):
                osqp_costs[i][j][k] = osqp_costs[i][j][k]/osqp_costs[i][3][k]
                lempc_costs[i][j][k] = lempc_costs[i][j][k]/lempc_costs[i][3][k]                
            
    for i in range(0,13):
        plt.figure(i)
        draw_boxplot(osqp_costs[i],np.arange(0,len(errors))-.2,'r','tan')
        draw_boxplot(lempc_costs[i],np.arange(0,len(errors)),'b','cyan')
        plt.axis([-1,len(errors),.75,1.5])

    plt.ion()
    plt.show()
    plt.pause(10000)

    
