import os
import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt

def draw_boxplot(data,positions, edge_color, fill_color, outlier_sym='+'):

    if(outlier_sym == ''):
        bp = plt.boxplot(data,positions=positions,patch_artist=True,sym='')
    else:
        bp = plt.boxplot(data,positions=positions,patch_artist=True,sym=edge_color+outlier_sym)

    for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
        plt.setp(bp[element], color=edge_color)

    for patch in bp['boxes']:
        patch.set(facecolor=fill_color)


if __name__ == "__main__":

    data = sio.loadmat('good_1_iteration_data_for_plotting')

    osqp_solve_times = data['osqp_solve_times'][0]
    osqp_total_solve_times = data['osqp_total_solve_times'][0]
    lempc_solve_times = data['lempc_solve_times'][0]
    # big_gpu_solve_times = data['big_gpu_solve_times'][0]
    small_osqp_solve_times = data['small_osqp_solve_times'][0]
    small_osqp_total_solve_times = data['small_osqp_total_solve_times'][0]
    lempc_relative_costs = data['lempc_relative_costs'][0]
    small_osqp_relative_costs = data['small_osqp_relative_costs'][0]

    data2 = sio.loadmat('good_3_iteration_data_for_plotting')        
    lempc_solve_times_3_iter = data2['lempc_solve_times'][0]
    lempc_relative_costs_3_iter = data2['lempc_relative_costs'][0]
    small_osqp_relative_costs_3_iter = data2['small_osqp_relative_costs'][0]

    big_gpu_solve_times = sio.loadmat('./big_gpu_data_more_damping/big_gpu_lempc_solve_times.mat')['big_gpu_solve_times'].flatten()    

    import matplotlib.patches as mpatches

    black = mpatches.Patch(color='black', label='OSQPMPC')    
    red = mpatches.Patch(color='red', label='Parameterized OSQPMPC')
    blue = mpatches.Patch(color='blue', label='EMPC - 750 Ti')
    green = mpatches.Patch(color='green', label='EMPC - Titan X')
    purple = mpatches.Patch(color='purple', label='3 Iter EMPC - 750 Ti')        

    plt.figure(1)
    draw_boxplot(osqp_solve_times,np.arange(1,14)*5-1.0,'k','tan','')
    draw_boxplot(lempc_solve_times,np.arange(1,14)*5-.5,'b','cyan','')
    draw_boxplot(small_osqp_solve_times,np.arange(1,14)*5+0,'r','tan','')
    draw_boxplot(big_gpu_solve_times,np.arange(1,14)*5+.5,'g','tan','')
    draw_boxplot(lempc_solve_times_3_iter,np.arange(1,14)*5+1.0,'purple','gray','')        
    plt.ylabel('Optimization Solve Times (s)')
    plt.xlabel('Number of Links')
    plt.xlim(-.5,14*5)    
    # plt.axis([0,14*4,0,.25])
    plt.xticks((np.arange(1,14)*5).tolist(),['1','2','3','4','5','6','7','8','9','10','11','12','13'])    
    plt.legend(handles=[black,red,blue,green,purple])

    plt.figure(2)
    draw_boxplot(osqp_total_solve_times,np.arange(1,14)*5-1.0,'k','tan','')
    draw_boxplot(lempc_solve_times,np.arange(1,14)*5-.5,'b','cyan','')
    draw_boxplot(small_osqp_total_solve_times,np.arange(1,14)*5+0,'r','tan','')
    draw_boxplot(big_gpu_solve_times,np.arange(1,14)*5+.5,'g','tan','')
    draw_boxplot(lempc_solve_times_3_iter,np.arange(1,14)*5+1.5,'purple','gray','')    
    plt.ylabel('MPC Solve Times (s)')
    plt.xlabel('Number of Links')
    plt.xlim(-.5,14*5)
    # plt.ylim(0,.25)
    plt.xticks((np.arange(1,14)*5).tolist(),['1','2','3','4','5','6','7','8','9','10','11','12','13'])
    plt.legend(handles=[black,red,blue,green,purple])    

    plt.figure(5)
    draw_boxplot(osqp_total_solve_times,np.arange(1,14)*5-1.0,'k','tan','')
    draw_boxplot(lempc_solve_times,np.arange(1,14)*5-.5,'b','cyan','')
    draw_boxplot(small_osqp_total_solve_times,np.arange(1,14)*5+0,'r','tan','')
    draw_boxplot(big_gpu_solve_times,np.arange(1,14)*5+.5,'g','tan','')
    draw_boxplot(lempc_solve_times_3_iter,np.arange(1,14)*5+1.0,'purple','gray','')        
    plt.ylabel('MPC Solve Times (s)')
    plt.xlabel('Number of Links')
    plt.xlim(-.5,14*5)    
    plt.ylim(0,.15)
    plt.xticks((np.arange(1,14)*5).tolist(),['1','2','3','4','5','6','7','8','9','10','11','12','13'])
    plt.legend(handles=[black,red,blue,green,purple],loc='upper left')        
    

    red = mpatches.Patch(color='red', label='Parameterized OSQPMPC')
    blue = mpatches.Patch(color='blue', label='EMPC')
    purple = mpatches.Patch(color='purple', label='3 Iter EMPC')        
    
    plt.figure(3)
    # draw_boxplot(small_gpu_relative_costs,np.arange(1,14),'b','cyan')
    draw_boxplot(lempc_relative_costs,np.arange(1,14)*3-.6,'b','cyan','')
    draw_boxplot(small_osqp_relative_costs,np.arange(1,14)*3+.6,'r','tan','')
    draw_boxplot(lempc_relative_costs_3_iter,np.arange(1,14)*3,'purple','gray','')    
    plt.hlines(1,1,13*3,'k','dashed')
    plt.xlabel('Number of Links')
    plt.ylabel('Actual Cost Ratio (EMPC / OSQPMPC)')
    plt.xticks((np.arange(1,14)*3).tolist(),['1','2','3','4','5','6','7','8','9','10','11','12','13'])
    plt.xlim(0,14*3)


    plt.legend(handles=[red,blue,purple])
    plt.ion()
    plt.show()
    plt.pause(10000)

    plt.ylabel('Solve Times (s)')
    plt.xlabel('Number of Links')
    plt.axis([0,14,0,.05])
    plt.legend(['OSQPMPC','LEMPC'])

    plt.ion()

    plt.show()

    plt.pause(10000)

    
