import os
import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt

def get_num_links_from_file(myfile):
    data = sio.loadmat(myfile)
    u_traj = data['u']
    n = u_traj.shape[1]
    return n

def get_solve_times_from_file(myfile):
    data = sio.loadmat(myfile)
    solve_times = data['solve_times'].flatten().tolist()        
    total_solve_times = []
    try:
        total_solve_times = data['total_solve_times'].flatten().tolist()
    except:
        pass

    return solve_times,total_solve_times

def get_total_cost_from_file(myfile):
    data = sio.loadmat(myfile)
    xgoal = data['xgoal'].T
    x_traj = data['x']
    u_traj = data['u']

    sim_length = u_traj.shape[0]
    n = u_traj.shape[1]
    
    Q = 0.0*np.eye(n*2)
    for i in range(n,n*2):
        Q[i,i] = 1.0
    
    R = 0.001*np.eye(n)

    total_cost = 0
    for i in range(0,sim_length):
        total_cost += (x_traj[i,:]-xgoal).dot(Q).dot((x_traj[i,:]-xgoal).T) + u_traj[i,:].T.dot(R).dot(u_traj[i,:])

    return total_cost.flatten()[0]

def draw_boxplot(data,positions, edge_color, fill_color, outlier_sym='+'):

    if(outlier_sym == ''):
        bp = plt.boxplot(data,positions=positions,patch_artist=True,sym='')
    else:
        bp = plt.boxplot(data,positions=positions,patch_artist=True,sym=edge_color+outlier_sym)

    for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
        plt.setp(bp[element], color=edge_color)

    for patch in bp['boxes']:
        patch.set(facecolor=fill_color)

def get_solve_times_and_relative_costs(dirname):

    lempc_relative_costs = []
    small_osqp_relative_costs = []    
    osqp_solve_times = []
    osqp_total_solve_times = []    
    lempc_solve_times = []
    lempc_solve_times = []    
    small_osqp_solve_times = []
    small_osqp_total_solve_times = []    
    for i in range(0,13):
        lempc_relative_costs.append([])
        small_osqp_relative_costs.append([])
        osqp_solve_times.append([])
        lempc_solve_times.append([])
        small_osqp_solve_times.append([])
        osqp_total_solve_times.append([])
        small_osqp_total_solve_times.append([])
    
    all_files = os.listdir(dirname)
    osqp_files = []
    small_osqp_files = []    
    lempc_files = []    
        
    for thisfile in all_files:
        if thisfile[-4:] == '.mat':
            if(thisfile.find('lempc') != -1):
                lempc_files.append(dirname+thisfile)
            elif(thisfile.find('osqp') != -1):
                if(thisfile.find('small') != -1):
                    small_osqp_files.append(dirname+thisfile)
                else:
                    osqp_files.append(dirname+thisfile)


    for i in range(0,len(lempc_files)):
        match = 0
        lempc_trial = lempc_files[i][-8:-4]
        lempc_n = get_num_links_from_file(lempc_files[i])
        for j in range(0,len(osqp_files)):
            osqp_trial = osqp_files[j][-8:-4]
            osqp_n = get_num_links_from_file(osqp_files[j])

            if(lempc_trial == osqp_trial and lempc_n == osqp_n):
                match=1
                n = lempc_n
                for k in range(0,len(small_osqp_files)):
                    small_osqp_trial = small_osqp_files[k][-8:-4]
                    small_osqp_n = get_num_links_from_file(small_osqp_files[k])
                    if(lempc_trial == small_osqp_trial and lempc_n == small_osqp_n):
                        match=1
                        n = lempc_n
                        break
                    else:
                        match=0
                break
            
        if(match==1):
            lempc_cost = get_total_cost_from_file(lempc_files[i])
            osqp_cost = get_total_cost_from_file(osqp_files[j])
            small_osqp_cost = get_total_cost_from_file(small_osqp_files[k])
            lempc_relative_cost = lempc_cost/osqp_cost
            small_osqp_relative_cost = small_osqp_cost/osqp_cost            

            solve_times, total_solve_times = get_solve_times_from_file(lempc_files[i])
            for l in range(0,len(solve_times)):
                lempc_solve_times[n-1].append(solve_times[l])

            solve_times, total_solve_times = get_solve_times_from_file(osqp_files[j])
            for l in range(0,len(solve_times)):
                osqp_solve_times[n-1].append(solve_times[l])
                osqp_total_solve_times[n-1].append(total_solve_times[l])

            solve_times, total_solve_times = get_solve_times_from_file(small_osqp_files[k])
            for l in range(0,len(solve_times)):
                small_osqp_solve_times[n-1].append(solve_times[l])
                small_osqp_total_solve_times[n-1].append(total_solve_times[l])
                
            lempc_relative_costs[n-1].append(lempc_relative_cost)
            small_osqp_relative_costs[n-1].append(small_osqp_relative_cost)            
                
            print("lempc relative cost: ",lempc_relative_cost)
            print("small_osqp relative cost: ",small_osqp_relative_cost)            
            
    return osqp_solve_times,osqp_total_solve_times,lempc_solve_times,small_osqp_solve_times,small_osqp_total_solve_times,lempc_relative_costs,small_osqp_relative_costs
    


if __name__ == "__main__":

    small_gpu_osqp_solve_times,small_gpu_lempc_solve_times,small_gpu_relative_costs = get_solve_times_and_relative_costs('./small_gpu_data_less_damping/')

    big_gpu_osqp_solve_times,big_gpu_lempc_solve_times,big_gpu_relative_costs = get_solve_times_and_relative_costs('./big_gpu_data_less_damping/')

    big_gpu_solve_times = sio.loadmat('./big_gpu_data_more_damping/big_gpu_lempc_solve_times.mat')['big_gpu_solve_times'].flatten()
    
    osqp_solve_times,osqp_total_solve_times,lempc_solve_times,small_osqp_solve_times,small_osqp_total_solve_times,lempc_relative_costs,small_osqp_relative_costs = get_solve_times_and_relative_costs('./')
    
    data_dict = {'osqp_solve_times':osqp_solve_times,
                 'osqp_total_solve_times':osqp_total_solve_times,
                 'lempc_solve_times':lempc_solve_times,
                 'big_gpu_solve_times':big_gpu_solve_times,
                 'small_osqp_solve_times':small_osqp_solve_times,
                 'small_osqp_total_solve_times':small_osqp_total_solve_times,
                 'lempc_relative_costs':lempc_relative_costs,
                 'small_osqp_relative_costs':small_osqp_relative_costs}

    # sio.savemat('good_1_iteration_data_for_plotting',data_dict)

    # data = sio.loadmat('good_1_iteration_data_for_plotting')

    # osqp_solve_times = data['osqp_solve_times'][0]
    # osqp_total_solve_times = data['osqp_total_solve_times'][0]
    # lempc_solve_times = data['lempc_solve_times'][0]
    # # big_gpu_solve_times = data['big_gpu_solve_times'][0]
    # small_osqp_solve_times = data['small_osqp_solve_times'][0]
    # small_osqp_total_solve_times = data['small_osqp_total_solve_times'][0]
    # lempc_relative_costs = data['lempc_relative_costs'][0]
    # small_osqp_relative_costs = data['small_osqp_relative_costs'][0]

    # data2 = sio.loadmat('good_3_iteration_data_for_plotting')        
    # lempc_solve_times_3_iter = data2['lempc_solve_times'][0]
    # lempc_relative_costs_3_iter = data2['lempc_relative_costs'][0]
    # small_osqp_relative_costs_3_iter = data2['small_osqp_relative_costs'][0]

    # big_gpu_solve_times = sio.loadmat('./big_gpu_data_more_damping/big_gpu_lempc_solve_times.mat')['big_gpu_solve_times'].flatten()    

    import matplotlib.patches as mpatches

    black = mpatches.Patch(color='black', label='OSQPMPC')    
    red = mpatches.Patch(color='red', label='Parameterized OSQPMPC')
    blue = mpatches.Patch(color='blue', label='EMPC - 750 Ti')
    green = mpatches.Patch(color='green', label='EMPC - Titan X')
    purple = mpatches.Patch(color='purple', label='3 Iter EMPC - 750 Ti')        

    plt.figure(1)
    draw_boxplot(osqp_solve_times,np.arange(1,14)*5-1.0,'k','tan','')
    draw_boxplot(lempc_solve_times,np.arange(1,14)*5-.5,'b','cyan','')
    draw_boxplot(small_osqp_solve_times,np.arange(1,14)*5+0,'r','tan','')
    # draw_boxplot(big_gpu_solve_times,np.arange(1,14)*5+.5,'g','tan','')
    # draw_boxplot(lempc_solve_times_3_iter,np.arange(1,14)*5+1.0,'purple','gray','')        
    plt.ylabel('Optimization Solve Times (s)')
    plt.xlabel('Number of Links')
    plt.xlim(-.5,14*5)    
    # plt.axis([0,14*4,0,.25])
    plt.xticks((np.arange(1,14)*5).tolist(),['1','2','3','4','5','6','7','8','9','10','11','12','13'])    
    plt.legend(handles=[black,red,blue,green,purple])

    plt.figure(2)
    draw_boxplot(osqp_total_solve_times,np.arange(1,14)*5-1.0,'k','tan','')
    draw_boxplot(lempc_solve_times,np.arange(1,14)*5-.5,'b','cyan','')
    draw_boxplot(small_osqp_total_solve_times,np.arange(1,14)*5+0,'r','tan','')
    # draw_boxplot(big_gpu_solve_times,np.arange(1,14)*5+.5,'g','tan','')
    # draw_boxplot(lempc_solve_times_3_iter,np.arange(1,14)*5+1.5,'purple','gray','')    
    plt.ylabel('MPC Solve Times (s)')
    plt.xlabel('Number of Links')
    plt.xlim(-.5,14*5)
    # plt.ylim(0,.25)
    plt.xticks((np.arange(1,14)*5).tolist(),['1','2','3','4','5','6','7','8','9','10','11','12','13'])
    plt.legend(handles=[black,red,blue,green,purple])    

    plt.figure(5)
    draw_boxplot(osqp_total_solve_times,np.arange(1,14)*5-1.0,'k','tan','')
    draw_boxplot(lempc_solve_times,np.arange(1,14)*5-.5,'b','cyan','')
    draw_boxplot(small_osqp_total_solve_times,np.arange(1,14)*5+0,'r','tan','')
    # draw_boxplot(big_gpu_solve_times,np.arange(1,14)*5+.5,'g','tan','')
    # draw_boxplot(lempc_solve_times_3_iter,np.arange(1,14)*5+1.0,'purple','gray','')        
    plt.ylabel('MPC Solve Times (s)')
    plt.xlabel('Number of Links')
    plt.xlim(-.5,14*5)    
    plt.ylim(0,.15)
    plt.xticks((np.arange(1,14)*5).tolist(),['1','2','3','4','5','6','7','8','9','10','11','12','13'])
    plt.legend(handles=[black,red,blue,green,purple],loc='upper left')        
    

    red = mpatches.Patch(color='red', label='Parameterized OSQPMPC')
    blue = mpatches.Patch(color='blue', label='EMPC')
    purple = mpatches.Patch(color='purple', label='3 Iter EMPC')        
    
    plt.figure(3)
    # draw_boxplot(small_gpu_relative_costs,np.arange(1,14),'b','cyan')
    draw_boxplot(lempc_relative_costs,np.arange(1,14)*3-.6,'b','cyan','')
    draw_boxplot(small_osqp_relative_costs,np.arange(1,14)*3+.6,'r','tan','')
    # draw_boxplot(lempc_relative_costs_3_iter,np.arange(1,14)*3,'purple','gray','')    
    plt.hlines(1,1,13*3,'k','dashed')
    plt.xlabel('Number of Links')
    plt.ylabel('Actual Cost Ratio (EMPC / OSQPMPC)')
    plt.xticks((np.arange(1,14)*3).tolist(),['1','2','3','4','5','6','7','8','9','10','11','12','13'])
    plt.xlim(0,14*3)


    plt.legend(handles=[red,blue,purple])
    plt.ion()
    plt.show()
    plt.pause(10000)

    plt.ylabel('Solve Times (s)')
    plt.xlabel('Number of Links')
    plt.axis([0,14,0,.05])
    plt.legend(['OSQPMPC','LEMPC'])

    plt.ion()

    plt.show()

    plt.pause(10000)

    
