import os
import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt

def get_num_links_from_file(myfile):
    data = sio.loadmat(myfile)
    u_traj = data['u']
    n = u_traj.shape[1]
    return n

def get_solve_times_from_file(myfile):
    data = sio.loadmat(myfile)
    solve_times = data['solve_times'].flatten().tolist()
    return solve_times

def get_total_cost_from_file(myfile):
    data = sio.loadmat(myfile)
    xgoal = data['xgoal'].T
    x_traj = data['x']
    u_traj = data['u']

    sim_length = u_traj.shape[0]
    n = u_traj.shape[1]
    
    Q = 0.0*np.eye(n*2)
    for i in range(n,n*2):
        Q[i,i] = 1.0/(5.0*(i+1-n))
    
    R = 0.0*np.eye(n)
    for i in range(0,n):
        R[i,i] = .001

    total_cost = 0
    for i in range(0,sim_length):
        total_cost += (x_traj[i,:]-xgoal).dot(Q).dot((x_traj[i,:]-xgoal).T) + u_traj[i,:].T.dot(R).dot(u_traj[i,:])

    return total_cost.flatten()[0]

def draw_boxplot(data,positions, edge_color, fill_color, outlier_symbol='+'):

    if(outlier_symbol == ''):
        bp = plt.boxplot(data,positions=positions,patch_artist=True,sym='')
    else:
        bp = plt.boxplot(data,positions=positions,patch_artist=True,sym=edge_color+outlier_symbol)

    for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
        plt.setp(bp[element], color=edge_color)

    for patch in bp['boxes']:
        patch.set(facecolor=fill_color)   


if __name__ == "__main__":

    relative_costs = []    
    osqp_solve_times = []
    lempc_solve_times = []
    for i in range(0,13):
        relative_costs.append([])        
        osqp_solve_times.append([])
        lempc_solve_times.append([])

    all_files = os.listdir('.')
    osqp_files = []
    lempc_files = []    
        
    for thisfile in all_files:
        if thisfile[-4:] == '.mat':
            if(thisfile.find('lempc') != -1):
                lempc_files.append(thisfile)
            elif(thisfile.find('osqp') != -1):
                osqp_files.append(thisfile)


    for i in range(0,len(lempc_files)):
        match = 0
        lempc_trial = lempc_files[i][-8:-4]
        lempc_n = get_num_links_from_file(lempc_files[i])
        for j in range(0,len(osqp_files)):
            osqp_trial = osqp_files[j][-8:-4]
            osqp_n = get_num_links_from_file(osqp_files[j])

            if(lempc_trial == osqp_trial and lempc_n == osqp_n):
                match=1
                n = lempc_n
                break
            
        if(match==1):
            lempc_cost = get_total_cost_from_file(lempc_files[i])
            osqp_cost = get_total_cost_from_file(osqp_files[j])
            relative_cost = lempc_cost/osqp_cost

            this_lempc_solve_times = get_solve_times_from_file(lempc_files[i])
            this_osqp_solve_times = get_solve_times_from_file(osqp_files[j])                
        
            relative_costs[n-1].append(relative_cost)

            for k in range(0,len(this_lempc_solve_times)):
                lempc_solve_times[n-1].append(this_lempc_solve_times[k])
            for k in range(0,len(this_osqp_solve_times)):
                osqp_solve_times[n-1].append(this_osqp_solve_times[k])
                
            print("relative cost: ",relative_cost)
    

            
    np.set_printoptions(precision=3)
    print("\nmedian relative cost for links")     
    for i in range(0,13):
        print(i,": ",np.median(relative_costs[i]))
    print("\nmean relative cost for links")     
    for i in range(0,13):
        print(i,": ",np.mean(relative_costs[i]))
        
    # print "\nmean solve times"
    # for i in xrange(0,13):
    #     print i,"lempc: ",np.median(lempc_solve_times[i]),"\t\tosqp: ",np.mean(osqp_solve_times[i])

    plt.figure(1)
    draw_boxplot(osqp_solve_times,np.arange(1,14)-.2,'r','tan')
    draw_boxplot(lempc_solve_times,np.arange(1,14),'b','cyan')
    plt.ylabel('Solve Times (s)')
    plt.xlabel('Number of Links')
    plt.axis([0,14,0,.25])
    plt.legend(['OSQPMPC','LEMPC'])

    plt.figure(2)
    draw_boxplot(osqp_solve_times,np.arange(1,14)-.2,'r','tan','')
    draw_boxplot(lempc_solve_times,np.arange(1,14),'b','cyan','')
    plt.ylabel('Solve Times (s)')
    plt.xlabel('Number of Links')
    plt.axis([0,14,0,.025])
    plt.legend(['OSQPMPC','LEMPC'])
    
    plt.figure(3)
    draw_boxplot(relative_costs,np.arange(1,14),'b','cyan')

    plt.ion()

    plt.show()

    plt.pause(10000)

    
