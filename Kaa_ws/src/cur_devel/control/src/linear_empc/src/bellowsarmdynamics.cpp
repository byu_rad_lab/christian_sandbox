#include "kinematics/arm.h"
#include "kinematics/joint_ccc.h"

kinematics::Arm BELLOWSARM;
double height;

kinematics::JointCCC joint1;
kinematics::Link link1;
Eigen::Matrix3d rot1;
Eigen::Vector3d vec1;
Eigen::Matrix4d hom1;
double m1;
Eigen::Vector3d I1;
Eigen::Vector3d pos1;

kinematics::JointCCC joint2;
kinematics::Link link2;
Eigen::Matrix3d rot2;
Eigen::Vector3d vec2;
Eigen::Matrix4d hom2;
double m2;
Eigen::Vector3d I2;
Eigen::Vector3d pos2;

kinematics::JointCCC joint3;
kinematics::Link link3;
Eigen::Matrix3d rot3;
Eigen::Vector3d vec3;
Eigen::Matrix4d hom3;
double m3;
Eigen::Vector3d I3;
Eigen::Vector3d pos3;


void calc_M(kinematics::Arm * BELLOWSARM, std::vector<double> qvec, Eigen::MatrixXd * M)
{
  Eigen::VectorXd q = Eigen::Map<Eigen::Matrix<double,6,1>>(qvec.data());
  BELLOWSARM->setJointAngsReal(q);
  BELLOWSARM->calcInertiaMat();
  
  *M = BELLOWSARM->getInertiaMat();
  // while((*M).rows()==0)
  //   {
  //     std::cout<<"Recalculating Mass Matrix\n";
  //     BELLOWSARM->setJointAngs(q);
  //     BELLOWSARM->setJointVels(q);
  //     BELLOWSARM->calcDynamicsMats();
  //     *M = BELLOWSARM->getInertiaMat();
  //   }
}
void calc_C(kinematics::Arm * BELLOWSARM,std::vector<double> qvec,std::vector<double> qdvec, Eigen::MatrixXd * C)
{
  Eigen::VectorXd q = Eigen::Map<Eigen::Matrix<double,6,1>>(qvec.data());
  Eigen::VectorXd qd = Eigen::Map<Eigen::Matrix<double,6,1>>(qdvec.data());
  BELLOWSARM->setJointAngs(q);
  BELLOWSARM->setJointVels(qd);
  BELLOWSARM->calcDynamicsMats();
  *C = BELLOWSARM->coriolis_mat_;
}
void calc_grav_torque(kinematics::Arm * BELLOWSARM,std::vector<double> qvec, Eigen::MatrixXd * grav_torque)
{
  Eigen::VectorXd q = Eigen::Map<Eigen::Matrix<double,6,1>>(qvec.data());
  BELLOWSARM->setJointAngsReal(q);
  BELLOWSARM->setGravity(9.81);
  BELLOWSARM->calcTorqueGravity();
  // *grav_torque = BELLOWSARM->getTorqueGravity();
  (*grav_torque).setZero();
}

void calc_A_B_w(kinematics::Arm * BELLOWSARM,std::vector<double> qvec,std::vector<double> qdvec, Eigen::MatrixXd * A, Eigen::MatrixXd * B, Eigen::MatrixXd * w)
{
  Eigen::MatrixXd KPassiveSpring(6,6);
  KPassiveSpring<< 136.336,0,0,0,0,0,
    0,136.336,0,0,0,0,
    0,0,22.119,0,0,0,
    0,0,0,22.119,0,0,
    0,0,0,0,22.119,0,
    0,0,0,0,0,22.119;
  
  Eigen::MatrixXd KPassiveDamper(6,6);
  KPassiveDamper<< 35.5,0,0,0,0,0,
    0,35.5,0,0,0,0,
    0,0,6.3,0,0,0,
    0,0,0,6.3,0,0,
    0,0,0,0,6.3,0,
    0,0,0,0,0,6.3;
  
  Eigen::MatrixXd Prs2Torque(6,12);
  Prs2Torque<< 4.629,-4.629,0,0,0,0,0,0,0,0,0,0,
    0,0,4.629,-4.629,0,0,0,0,0,0,0,0,
    0,0,0,0,1.512,-1.512,0,0,0,0,0,0,
    0,0,0,0,0,0,1.512,-1.512,0,0,0,0,
    0,0,0,0,0,0,0,0,1.512,-1.512,0,0,
    0,0,0,0,0,0,0,0,0,0,1.512,-1.512;
  
  Eigen::MatrixXd alphas(12,12);
  alphas.setIdentity();
  
  Eigen::MatrixXd M(6,6);
  Eigen::MatrixXd Minv(6,6);
  Eigen::MatrixXd C(6,6);
  Eigen::MatrixXd grav_torque(6,1);
  calc_M(BELLOWSARM,qvec,&M);
  std::cout<<"M:\n"<<M<<std::endl;
  for(int i=0; i<6; i++)
    {
      std::cout<<"\n";
      for(int j=0; j<6; j++)
	{
	  std::cout<<M(i,j)<<"  ";
	}
    }
  calc_C(BELLOWSARM,qvec,qdvec,&C);
  std::cout<<"C:\n"<<C<<std::endl;
  for(int i=0; i<6; i++)
    {
      std::cout<<"\n";
      for(int j=0; j<6; j++)
	{
	  std::cout<<C(i,j)<<"  ";
	}
    }
  
  // calc_grav_torque(qvec,&grav_torque);
  grav_torque.setZero();
  
  Minv = M.inverse();
  std::cout<<"Minv:\n"<<Minv<<std::endl;
  
  Eigen::MatrixXd eye(6,6);
  eye.setIdentity();
  Eigen::MatrixXd Azeros(6,6);
  Azeros.setZero();
  
  Eigen::MatrixXd A1(12,24);
  A1<< -alphas,Azeros,Azeros;
  Eigen::MatrixXd A2(6,24);
  A2<< Minv*Prs2Torque, -Minv*(KPassiveDamper+C), -Minv*KPassiveSpring;
  Eigen::MatrixXd A3(6,24);
  A3<< Azeros, eye, Azeros;
  Eigen::MatrixXd Acont(24,24);
  Acont<<A1,A2,A3;
  
  Eigen::MatrixXd Bzeros(12,12);
  Bzeros.setZero();
  
  Eigen::MatrixXd Bcont(24,12);
  Bcont<< alphas,Bzeros;
  
  Eigen::MatrixXd wzeros(6,1);
  wzeros.setZero();
  Eigen::MatrixXd wcont(24,1);
  wcont<< wzeros, grav_torque, wzeros;
  
  *A = Acont;
  *B = Bcont;
  *w = wcont;
}

void calc_discrete_A_B_w(kinematics::Arm * BELLOWSARM,std::vector<double> q,std::vector<double> qd, double dt, Eigen::MatrixXd * A_d, Eigen::MatrixXd * B_d, Eigen::MatrixXd * w_d)
{
  int NUM_INPUTS = 12;
  int NUM_STATES = 24;
  Eigen::MatrixXd A(NUM_STATES,NUM_STATES);
  Eigen::MatrixXd B(NUM_STATES,NUM_INPUTS);
  Eigen::MatrixXd w(NUM_STATES,1);
  
  calc_A_B_w(BELLOWSARM,q,qd,&A,&B,&w);

  // std::cout<<"A:\n"<<A<<std::endl;
  // std::cout<<"B:\n"<<B<<std::endl;
  // std::cout<<"w:\n"<<w<<std::endl;
  
  
  Eigen::MatrixXd eye(A.rows(),A.cols());
  eye.setIdentity();
  *A_d = (eye - A*dt).inverse();
  *B_d = (eye - A*dt).inverse()*B*dt;
  *w_d = w*dt;
}
