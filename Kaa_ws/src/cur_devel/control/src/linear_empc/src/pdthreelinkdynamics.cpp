#include <Eigen/LU>
#include <math.h>

const double default_l = 0.5;
const double default_m = 1.0;
const double default_grav = 9.81;

void calc_M(std::vector<double> q,Eigen::MatrixXd * M,double l=default_l,double m=default_m)
{
  double I = 1.0/12.0*m*l*l;
  (*M)(0,0) = 3.0*I + 3.0*pow(l, 2)*m*cos(q[1]) + 1.0*pow(l, 2)*m*cos(q[2]) + 1.0*pow(l, 2)*m*cos(q[1] + q[2]) + 3.75*pow(l, 2)*m;
  (*M)(0,1) = 2.0*I + 1.5*pow(l, 2)*m*cos(q[1]) + 1.0*pow(l, 2)*m*cos(q[2]) + 0.5*pow(l, 2)*m*cos(q[1] + q[2]) + 1.5*pow(l, 2)*m;
  (*M)(0,2) = 1.0*I + 0.5*pow(l, 2)*m*cos(q[2]) + 0.5*pow(l, 2)*m*cos(q[1] + q[2]) + 0.25*pow(l, 2)*m;
  (*M)(1,0) = 2.0*I + 1.5*pow(l, 2)*m*cos(q[1]) + 1.0*pow(l, 2)*m*cos(q[2]) + 0.5*pow(l, 2)*m*cos(q[1] + q[2]) + 1.5*pow(l, 2)*m;
  (*M)(1,1) = 2.0*I + 1.0*pow(l, 2)*m*cos(q[2]) + 1.5*pow(l, 2)*m;
  (*M)(1,2) = 1.0*I + 0.5*pow(l, 2)*m*cos(q[2]) + 0.25*pow(l, 2)*m;
  (*M)(2,0) = 1.0*I + 0.5*pow(l, 2)*m*cos(q[2]) + 0.5*pow(l, 2)*m*cos(q[1] + q[2]) + 0.25*pow(l, 2)*m;
  (*M)(2,1) = 1.0*I + 0.5*pow(l, 2)*m*cos(q[2]) + 0.25*pow(l, 2)*m;
  (*M)(2,2) = 1.0*I + 0.25*pow(l, 2)*m;
}

void calc_g(std::vector<double> q,Eigen::MatrixXd * g,double l=default_l,double m=default_m,double gravity=default_grav)
{
  (*g)(0) = -1.0/2.0*gravity*l*m*(5*sin(q[0]) + 3*sin(q[0] + q[1]) + sin(q[0] + q[1] + q[2]));
  (*g)(1) = -1.0/2.0*gravity*l*m*(3*sin(q[0] + q[1]) + sin(q[0] + q[1] + q[2]));
  (*g)(2) = -1.0/2.0*gravity*l*m*sin(q[0] + q[1] + q[2]);
}

void calc_c(std::vector<double> q,std::vector<double> qd,Eigen::MatrixXd * c,double l=default_l,double m=default_m)
{
  (*c)(0) = -pow(l, 2)*m*(3.0*qd[0]*qd[1]*sin(q[1]) + 1.0*qd[0]*qd[1]*sin(q[1] + q[2]) + 1.0*qd[0]*qd[2]*sin(q[2]) + 1.0*qd[0]*qd[2]*sin(q[1] + q[2]) + 1.5*pow(qd[1], 2)*sin(q[1]) + 0.5*pow(qd[1], 2)*sin(q[1] + q[2]) + 1.0*qd[1]*qd[2]*sin(q[2]) + 1.0*qd[1]*qd[2]*sin(q[1] + q[2]) + 0.5*pow(qd[2], 2)*sin(q[2]) + 0.5*pow(qd[2], 2)*sin(q[1] + q[2]));
  (*c)(1) = pow(l, 2)*m*(1.5*pow(qd[0], 2)*sin(q[1]) + 0.5*pow(qd[0], 2)*sin(q[1] + q[2]) - 1.0*qd[0]*qd[2]*sin(q[2]) - 1.0*qd[1]*qd[2]*sin(q[2]) - 0.5*pow(qd[2], 2)*sin(q[2]));
  (*c)(2) = pow(l, 2)*m*(0.5*pow(qd[0], 2)*sin(q[2]) + 0.5*pow(qd[0], 2)*sin(q[1] + q[2]) + 1.0*qd[0]*qd[1]*sin(q[2]) + 0.5*pow(qd[1], 2)*sin(q[2]));
}

void calc_C(std::vector<double> q,std::vector<double> qd,Eigen::MatrixXd * C,double l=default_l,double m=default_m)
{
  (*C)(0,0) = -pow(l, 2)*m*(3.0*qd[1]*sin(q[1]) + 1.0*qd[1]*sin(q[1] + q[2]) + 1.0*qd[2]*sin(q[2]) + 1.0*qd[2]*sin(q[1] + q[2]));
  (*C)(0,1) = -pow(l, 2)*m*(1.5*qd[1]*sin(q[1]) + 0.5*qd[1]*sin(q[1] + q[2]) + 1.0*qd[2]*sin(q[2]) + 1.0*qd[2]*sin(q[1] + q[2]));
  (*C)(0,2) = -pow(l, 2)*m*(0.5*qd[2]*sin(q[2]) + 0.5*qd[2]*sin(q[1] + q[2]));
  
  (*C)(1,0) = pow(l, 2)*m*(1.5*qd[0]*sin(q[1]) + 0.5*qd[0]*sin(q[1] + q[2]) - 1.0*qd[2]*sin(q[2]));
  (*C)(1,1) = pow(l, 2)*m*(-1.0*qd[2]*sin(q[2]));
  (*C)(1,2) = pow(l, 2)*m*(-0.5*qd[2]*sin(q[2]));
  
  (*C)(2,0) = pow(l, 2)*m*(0.5*qd[0]*sin(q[2]) + 0.5*qd[0]*sin(q[1] + q[2]));
  (*C)(2,1) = pow(l, 2)*m*(1.0*qd[0]*sin(q[2]) + 0.5*qd[1]*sin(q[2]));
  (*C)(2,2) = 0;
}

void calc_A_B_w(std::vector<double> q,std::vector<double> qd,Eigen::MatrixXd * A, Eigen::MatrixXd * B, Eigen::MatrixXd * w,double l=default_l,double m=default_m,double gravity=default_grav)
{
  
  Eigen::MatrixXd M(3,3);
  Eigen::MatrixXd C(3,3);
  Eigen::MatrixXd g(3,1);

  Eigen::MatrixXd Kp(3,3);
  Kp<<20,0,0,
    0,10,0,
    0,0,6;
  Eigen::MatrixXd Kd(3,3);
  Kd<<5,0,0,
    0,3,0,
    0,0,1;
  
  calc_M(q,&M);
  calc_C(q,qd,&C);
  calc_g(q,&g);

  Eigen::MatrixXd M_inv;
  M_inv = M.inverse();
  Eigen::MatrixXd M_inv_Kp(3,3);
  M_inv_Kp = M_inv*Kp;

  (*A).topLeftCorner(3,3) = -M_inv*(Kd+C);
  (*A).topRightCorner(3,3) = -M_inv_Kp;
  (*A).bottomLeftCorner(3,3).setIdentity();
  (*A).bottomRightCorner(3,3).setZero();

  for(int i=0; i<M_inv.rows(); i++)
    {
      for(int j=0; j<M_inv.cols(); j++)
	{
	  (*B)(i,j) = M_inv_Kp(i,j);
	}
    }
  for(int i=3; i<6; i++)
    {
      for(int j=0; j<3; j++)
	{
	  (*B)(i,j) = 0.0;
	}
    }

  Eigen::MatrixXd b(3,1);
  b = M_inv*(-g);
  for(int i=0; i<g.rows(); i++)
    {
      (*w)(i) = b(i);
    }
  for(int i=g.rows(); i<6; i++)
    {
      (*w)(i) = 0;
    }
  
}

void calc_discrete_A_B_w(std::vector<double> q,std::vector<double> qd, double dt, Eigen::MatrixXd * A_d, Eigen::MatrixXd * B_d, Eigen::MatrixXd * w_d,double l=default_l,double m=default_m,double gravity=default_grav)
{
  Eigen::MatrixXd A(6,6);
  Eigen::MatrixXd B(6,3);
  Eigen::MatrixXd w(6,1);
  calc_A_B_w(q,qd,&A,&B,&w);
  
  Eigen::MatrixXd eye(A.rows(),A.cols());
  eye.setIdentity();
  *A_d = (eye - A*dt).inverse();
  *B_d = (eye - A*dt).inverse()*B*dt;
  *w_d = w*dt;
}
