#include "n8linkdynamics.h"
#include "LinearEMPC.cu"
#include <Eigen/LU>
#include <ctime>
#include <fstream>

const int n = 8;

double hwrapAngle(double x)
{
  return remainder(x, 2*M_PI);
}

__device__ void my_cost_function(double * state, double * goal_state, double * u, double * goal_input, double * cost, int state_id, bool last_time_step)
{
  // If this is a position state
  if(state_id>=n)
    {
      *cost += 1.0*(*goal_state-*state)*(*goal_state-*state)/(5.0*(state_id+1.0-n));
    }
  // If this is a velocity state
  else if(state_id<n)
    {
      *cost += 0.0*(*goal_state-*state)*(*goal_state-*state);
    }
  // If this is an input
  if(state_id<n)
    {
      *cost += .001*(*goal_input-u[state_id])*(*goal_input-u[state_id])*(5.0*state_id);
    }
}


__device__ costfunc d_cost_function_pointer = my_cost_function;


int main(int argc, char * argv[])
{
  const int num_states = 2*n;
  const int num_inputs = n;

  Eigen::MatrixXd x(num_states,1);
  Eigen::MatrixXd xGoal(num_states,1);
  x.setZero();
  xGoal.setZero();
  if(argc>1)
    {
      for(int i=0; i<n; i++)
	{
	  x(n+i) = atof(argv[i+1]);
	  xGoal(n+i) = atof(argv[i+n+1]);
	}
    }

  costfunc h_cost_function_pointer;
  cudaMemcpyFromSymbol(&h_cost_function_pointer, d_cost_function_pointer, sizeof(costfunc));


  std::vector<double> umin;
  std::vector<double> umax;
  std::vector<double> xmin;
  std::vector<double> xmax;
  double taumin = -5.0;
  double taumax = 5.0;
  for(int i=0; i<num_inputs; i++)
    {
      umin.push_back(taumin);
      umax.push_back(taumax);
    }
  for(int i=0; i<num_states; i++)
    {
      xmin.push_back(-9999999.0);
      xmax.push_back(9999999.0);
    }
  
  Eigen::MatrixXd u(num_inputs,1);
  Eigen::MatrixXd uGoal(num_inputs,1);  
  for(int i=0; i<num_inputs; i++)
    {
      u(i) = 0.0;
      uGoal(i) = 0.0;
    }
  
  Eigen::MatrixXd A(num_states,num_states);
  Eigen::MatrixXd B(num_states,num_inputs);
  Eigen::MatrixXd w(num_states,1);
  Eigen::MatrixXd Q(num_states,num_states);
  Eigen::MatrixXd R(num_inputs,num_inputs);
  Q.setZero();
  for(int i=0; i<num_inputs; i++)
    {
      Q(i,i) = 1.0/(5.0*(i+1));
    }
  for(int i=0; i<num_inputs; i++)
    {
      R(i,i) = .001*5.0*(i);
    }
  

  
  std::vector<double> Avec;
  std::vector<double> Bvec;
  std::vector<double> wvec;
  Avec.resize(num_states*num_states);
  Bvec.resize(num_states*num_inputs);
  wvec.resize(num_states);
  
  LinearEMPC controller(num_states,
			num_inputs,
			h_cost_function_pointer,
			umin,
			umax,
			xmin,
			xmax,
		        100, //Horizon
			25, //NumParents
			20); //NumMates

  std::ofstream myfile;
  myfile.open("nlinkdata.txt");

  double actual_cost = 0;  

  double dt = .01;
  double l = 0.25;
  double m = 0.25;
  double I = m*l*l/12.0;
  double gravity = 9.81*0;
  double damping = 0.02;  
  for(int i=0; i<1000; i++)
    {
      std::vector<double> xvec(x.data(), x.data() + x.size());
      std::vector<double> uvec(u.data(), u.data() + u.size());

      std::vector<double> q;
      std::vector<double> qd;
      for(int j=0; j<num_inputs; j++)
	{
	  qd.push_back(x(j));
	  q.push_back(x(j+num_inputs));
	}

      calc_discrete_A_B_w(q,qd,dt,&A,&B,&w,l,m,I,gravity,damping);

      for(int j=0; j<num_states; j++)
	{
	  for(int k=0; k<num_states; k++)
	    {
	      Avec[j*num_states+k] = A(j,k);
	    }
	  for(int k=0; k<num_inputs; k++)
	    {
	      Bvec[j*num_inputs+k] = B(j,k);
	    }
	  wvec[j] = w(j);
	}

      std::vector<double> xGoalvec(xGoal.data(), xGoal.data() + xGoal.size());
      std::vector<double> uGoalvec(uGoal.data(), uGoal.data() + uGoal.size());
      
      double noise = 0.1*(x.block(num_inputs,0,num_inputs,1)-xGoal.block(num_inputs,0,num_inputs,1)).lpNorm<Eigen::Infinity>();
      // double noise = 0.5*(x.block(num_inputs,0,num_inputs,1)-xGoal.block(num_inputs,0,num_inputs,1)).lpNorm<2>();
      // double noise = 1.0*(x.block(num_inputs,0,1,1)-xGoal.block(num_inputs,0,1,1)).lpNorm<1>();
      
      //For velocity
      noise += 0.1*(x.block(0,0,num_inputs,1)-xGoal.block(0,0,num_inputs,1)).lpNorm<Eigen::Infinity>();
      

      clock_t begin = clock();
      uvec = controller.run_empc(xvec,xGoalvec,uvec,uGoalvec,Avec,Bvec,wvec,noise);
      clock_t end = clock();

      u = Eigen::Map<Eigen::Matrix<double,num_inputs,1>>(uvec.data());

      Eigen::MatrixXd x_next(num_states,1);      
      x_next = A*x + B*u + w;
      x = x_next;

      Eigen::MatrixXd tmp = x.transpose()*Q*x;// + u.transpose()*R*u;
      actual_cost += tmp(0,0);


      // Display and record stuff
      // std::cout<<"\nNoise: "<<noise<<std::endl;      
      double solve_time = double(end-begin)/CLOCKS_PER_SEC;
      // std::cout<<"\nEMPC Solve Time: "<<solve_time;
      
      // std::cout<<"\nu: "<<std::endl;
      // for(int j=0; j<num_inputs; j++)
      // 	{
      // 	  std::cout<<u(j)<<"  ";
      // 	}
      
      // std::cout<<"\nx: "<<std::endl;
      // for(int j=num_inputs; j<num_states; j++)
      // 	{
      // 	  std::cout<<x(j)<<"  ";
      // 	}

      for(int j=0; j<num_states; j++)
	{
	  myfile<<x(j)<<", ";
	}
      for(int j=0; j<num_inputs; j++)
	{
	  myfile<<u(j)<<", ";
	}
      myfile<<solve_time<<", ";
      myfile<<i<<"\n";
      
    }

  std::cout<<"\nactual_cost "<<actual_cost<<std::endl;
  myfile.close();
  return 0;
}
