#include "baxter_right_dynamics.cpp"
#include "LinearEMPC.cu"
#include <Eigen/LU>
#include <ctime>
#include "ros/ros.h"
#include "sensor_msgs/JointState.h"
#include "torque_controller/q_cmd.h"
#include <chrono>


// These will be updated by a callback
std::vector<double> x;
std::vector<double> xGoal;


void callback(const sensor_msgs::JointState msg)
{
  // Extract joint angles and velocities
  x[0] = msg.velocity[11];
  x[1] = msg.velocity[12];
  x[2] = msg.velocity[9];
  x[3] = msg.velocity[10];
  x[4] = msg.velocity[13];
  x[5] = msg.velocity[14];
  x[6] = msg.velocity[15];

  x[7] = msg.position[11];
  x[8] = msg.position[12];
  x[9] = msg.position[9];
  x[10] = msg.position[10];
  x[11] = msg.position[13];
  x[12] = msg.position[14];
  x[13] = msg.position[15];  
}

__device__ void my_cost_function(double * state, double * goal_state, double * u, double * goal_input, double * cost, int state_id, bool last_time_step)
  
{
  if(last_time_step)
    {
      // If this is a velocity state
      if(state_id>=0 and state_id<7)
  	{
  	  *cost += 0.0*(*goal_state-*state)*(*goal_state-*state);
  	}
      // If this is a position state
      if(state_id>=7)
  	{
  	  *cost += 1.0*(*goal_state-*state)*(*goal_state-*state);
  	}
    }
  // If this is a position state
  if(state_id>=7)
    {
      *cost += 1.0*(*goal_state-*state)*(*goal_state-*state);
    }
  
  // If this is an input
  if(state_id<7)
    {
      *cost += 0.20000*(*goal_input-*u)*(*goal_input-*u);
    }
}

__device__ costfunc d_cost_function_pointer = my_cost_function;

void phil_sleep(double duration)
{
  auto start = std::chrono::high_resolution_clock::now();
  auto now = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> elapsed_time = now-start;
  while(elapsed_time.count() <= duration)
    {
      ros::spinOnce();
      now = std::chrono::high_resolution_clock::now();      
      elapsed_time = now-start;      
    }
}

int main(int argc, char * argv[])
{
  x.resize(14);
  xGoal.resize(14);
  
  std::vector<double> u;
  std::vector<double> uNext;
  u.resize(7);
  uNext.resize(7);  
  for(int i=0; i<7; i++)
    {
      u[i] = 0.0;
    }
  costfunc h_cost_function_pointer;

  cudaMemcpyFromSymbol(&h_cost_function_pointer, d_cost_function_pointer, sizeof(costfunc));

  const int num_states = 14;
  const int num_inputs = 7;

  std::vector<double> umin;
  std::vector<double> umax;
  std::vector<double> xmin;
  std::vector<double> xmax;
  double tmin = -1.0;
  double tmax = 1.0;
  for(int i=0; i<num_inputs; i++)
    {
      umin.push_back(tmin);
      umax.push_back(tmax);
    }
  for(int i=0; i<num_states; i++)
    {
      xmin.push_back(-9999999.0);
      xmax.push_back(9999999.0);
    }

  int horizon = 100;
  double dt = .01;

  LinearEMPC controller(num_states,
			num_inputs,
			h_cost_function_pointer,
			umin,
			umax,
			xmin,
			xmax,
		        horizon,
			20,
			25);

  // Initialize ROS node
  ros::init(argc, argv, "controller_node");
  ros::NodeHandle n;
  ros::Rate loop_rate(int(1./dt));
  ros::Subscriber sub = n.subscribe("/robot/joint_states/", 1000, callback);
  ros::Publisher pcmd_pub = n.advertise<torque_controller::q_cmd>("/right/joint_cmd/",1000);

  float goal_states_array[14] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  for(int i=0;i<14;i++)
    {
      xGoal[i] = goal_states_array[i];
    }

  while(x[0]==0)
    {
      // std::cout<<"Waiting for states from robot..."<<std::endl;
      ros::spinOnce();
    }

  Eigen::MatrixXd A(num_states,num_states);
  Eigen::MatrixXd B(num_states,num_inputs);
  Eigen::MatrixXd w(num_states,1);
  std::vector<double> Avec;
  std::vector<double> Bvec;
  std::vector<double> wvec;
  Avec.resize(num_states*num_states);
  Bvec.resize(num_states*num_inputs);
  wvec.resize(num_states);

  auto start = std::chrono::high_resolution_clock::now();
  auto now = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> elapsed_time = now-start;
  double t_settle = 10.0;
  int first_time = 1;

  std::ofstream myfile;
  myfile.open("baxter_lempc_data.txt");

  while(ros::ok())
    {
      now = std::chrono::high_resolution_clock::now();
      elapsed_time = now-start;
      if(elapsed_time.count() >= 0*t_settle*1000.0)
      	{
	  std::cout<<"Goal 1"<<std::endl;
      	  xGoal[7] = 0;
      	  xGoal[8] = 0;
      	  xGoal[9] = 0;
      	  xGoal[10] = 0;
      	  xGoal[11] = 0;
      	  xGoal[12] = 0;
      	  xGoal[13] = 0;
      	}
      if(elapsed_time.count() >= 1*t_settle*1000.0)
      	{
	  std::cout<<"Goal 1"<<std::endl;
      	  xGoal[7] = 1*.5;
      	  xGoal[8] = 1*.5;
      	  xGoal[9] = 1*.5;
      	  xGoal[10] = 1*.5;
      	  xGoal[11] = 1*.5;
      	  xGoal[12] = 1*.5;
      	  xGoal[13] = 1*.5;
      	}
      if(elapsed_time.count() >= 2*t_settle*1000.0)      
      	{
	  std::cout<<"Goal 2"<<std::endl;
      	  xGoal[7] = -1*.5;
      	  xGoal[8] = -1*.5;
      	  xGoal[9] = -1*.5;
      	  xGoal[10] = -1*.5;
      	  xGoal[11] = -1*.5;
      	  xGoal[12] = -1*.5;
      	  xGoal[13] = -1*.5;
      	}
      if(elapsed_time.count() >= 3*t_settle*1000.0)
      	{
	  std::cout<<"Goal 3"<<std::endl;
      	  xGoal[7] = 0;
      	  xGoal[8] = 0;
      	  xGoal[9] = 0;
      	  xGoal[10] = 0;
      	  xGoal[11] = 0;
      	  xGoal[12] = 0;
      	  xGoal[13] = 0;
      	}

      ros::spinOnce();
      double noise = 0;
      for(int i=0; i<7; i++)
	{
	  noise += 0.01*abs(xGoal[7+i] - x[7+i]); // Good for horizon=100
	  noise += 0.001*abs(xGoal[0+i] - x[0+i]); // Good for horizon=100	  
	}

      std::vector<double> q;
      std::vector<double> qd;
      for(int j=0; j<7; j++)
	{
	  qd.push_back(x[j+0]);
	  q.push_back(x[j+7]);
	}

      calc_discrete_A_B_w(q,qd,dt,&A,&B,&w);

      for(int j=0; j<num_states; j++)
	{
	  for(int k=0; k<num_states; k++)
	    {
	      Avec[j*num_states+k] = A(j,k);
	    }
	  for(int k=0; k<num_inputs; k++)
	    {
	      Bvec[j*num_inputs+k] = B(j,k);
	    }
	  wvec[j] = w(j);
	}

      std::vector<double> uGoal;
      for(int j=0; j<7; j++)
	{
	  // uGoal.push_back(u[j+7]);
	  uGoal.push_back(x[j+7]);
	}

      // if(first_time)
      // 	{
      // 	  noise = 1.0;
      // 	  uNext = controller.run_empc(x,xGoal,u,uGoal,Avec,Bvec,wvec,noise,-1,100);
      // 	  first_time=0;
      // 	}


      auto t1 = std::chrono::high_resolution_clock::now();
      u = controller.run_empc(x,xGoal,u,uGoal,Avec,Bvec,wvec,noise,-1,2);
      auto t2 = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double, std::milli> solve_time = t2-t1;
      std::cout<<"Solve time: "<<solve_time.count()/1000.0<<std::endl;

      // Publish the inputs
      torque_controller::q_cmd msg;
      msg.q_array = u;
      // msg.q_array.resize(7);
      // for(int k=0; k<7; k++)
      // 	{
      // 	  msg.q_array[k] = u[k];	  
      // 	}
      pcmd_pub.publish(msg);

      std::cout<<"\nNoise: "<<noise<<std::endl;      

      std::cout<<"X: "<<std::endl;
      for(int i=7; i<14; i++)
      	{
	  std::cout<<"  "<<x[i];
      	}
      
      std::cout<<std::endl;
      
      std::cout<<"u: "<<std::endl;
      for(int i=0; i<7; i++)
      	{
      	  std::cout<<"  "<<u[i];
      	}
      std::cout<<std::endl;

      
      for(int j=0; j<num_states; j++)
	{
	  myfile<<x[j]<<", ";
	}
      for(int j=0; j<num_inputs; j++)
	{
	  myfile<<u[j]<<", ";
	}
      myfile<<solve_time.count()<<", ";
      myfile<<elapsed_time.count()<<"\n";
      
      // loop_rate.sleep();

      if(elapsed_time.count()>t_settle*4*1000.0)
	{
	  ros::shutdown();
	}
    }

  return 0;
}
