#include "baxter_right_dynamics.cpp"
#include "LinearEMPC.cu"
#include <Eigen/LU>
#include <ctime>

#include<fstream>

void my_calc_A_and_B_matrices(thrust::host_vector<double> * states, thrust::host_vector<double> * A_mat, thrust::host_vector<double> * B_mat, thrust::host_vector<double> * disturbance)
{
  const int NUM_INPUTS = 7;
  const int NUM_STATES = 14;

  Eigen::MatrixXd A_d(NUM_STATES, NUM_STATES);
  Eigen::MatrixXd B_d(NUM_STATES, NUM_INPUTS);
  Eigen::MatrixXd w_d(NUM_STATES,1);
  double dt = .01;

  std::vector<double> q;
  std::vector<double> qd;
  for(int i=0; i<NUM_INPUTS; i++)
    {
      qd.push_back((*states)[i]);
      q.push_back((*states)[i+NUM_INPUTS]);
    }

  calc_discrete_A_B_w(q,qd,dt,&A_d,&B_d,&w_d);

  // Load A_d and B_d back into A_mat and B_mat
  for(int i=0; i<NUM_STATES; i++)
    {
      for(int j=0; j<NUM_STATES; j++)
  	{
  	  (*A_mat)[i*NUM_STATES+j] = A_d(i,j);
  	}
      for(int j=0; j<NUM_INPUTS; j++)
  	{
  	  (*B_mat)[i*NUM_INPUTS+j] = B_d(i,j);
  	}
      (*disturbance)[i] = w_d(i);
    }
}

void calc_A_B_w(Eigen::MatrixXd * states, Eigen::MatrixXd * A_d, Eigen::MatrixXd * B_d, Eigen::MatrixXd * disturbance)
{
  std::vector<double> q;
  std::vector<double> qd;
  for(int i=0; i<7; i++)
    {
      qd.push_back((*states)(i));
      q.push_back((*states)(i+7));
    }

  double dt = .01;
  calc_discrete_A_B_w(q,qd,dt,A_d,B_d,disturbance);
}

__device__ void my_cost_function(double * state, double * goal_state, double * u, double * cost, int state_id, bool last_time_step)
{
  // If this is a position state
  if(state_id>6)
    {
      *cost += 10.0*(*goal_state-*state)*(*goal_state-*state);
    }
  else
    {
      *cost += 0*(*goal_state-*state)*(*goal_state-*state);
    }
}

__device__ costfunc d_cost_function_pointer = my_cost_function;



int main(int argc, char * argv[])
{
  costfunc h_cost_function_pointer;

  cudaMemcpyFromSymbol(&h_cost_function_pointer, d_cost_function_pointer, sizeof(costfunc));

  const int num_states = 14;
  const int num_inputs = 7;

  Eigen::MatrixXd x(num_states,1);
  Eigen::MatrixXd x_next(num_states,1);
  x << 0,0,0,0,0,0,0, 0,0,0,0,0,0,0;
  Eigen::MatrixXd xGoal(num_states,1);
  xGoal << 0,0,0,0,0,0,0, 1.0,1.0,1.0,1.0,1.0,1.0,1.0;
  Eigen::MatrixXd u(num_inputs,1);
  u << 0,0,0,0,0,0,0;
  Eigen::MatrixXd uSteadyState(num_inputs,1);
  uSteadyState << 1.0,1.0,1.0,1.0,1.0,1.0,1.0;

  Eigen::MatrixXd A(num_states,num_states);
  Eigen::MatrixXd B(num_states,num_inputs);
  Eigen::MatrixXd w(num_states,1);
  
  bool warmStart = false;  

  LinearEMPC controller(num_states,
			num_inputs,
			my_calc_A_and_B_matrices,
			h_cost_function_pointer,
		        50,
			250,
			4,
			0.05);

  std::ofstream myfile;
  myfile.open("data.txt");
  // for(int i=0; i<7; i++)
  //   {
  //     myfile<<"q"<<i<<", ";
  //   }
  // for(int i=0; i<7; i++)
  //   {
  //     myfile<<"u"<<i<<", ";
  //   }
  // myfile<<"t\n";

  for(int i=0; i<1000; i++)
    {
      std::vector<double> xvec(x.data(), x.data() + x.size());
      std::vector<double> xGoalvec(xGoal.data(), xGoal.data() + xGoal.size());;
      std::vector<double> uvec(u.data(), u.data() + u.size());
      std::vector<double> uSteadyStatevec(uSteadyState.data(), uSteadyState.data() + uSteadyState.size());

      clock_t begin = clock();
      uvec = controller.run_empc(xvec,xGoalvec,uvec,uSteadyStatevec,warmStart);
      clock_t end = clock();

      double elapsed_time = double(end-begin)/CLOCKS_PER_SEC;
      // std::cout<<"\nEMPC Solve Time: "<<elapsed_time;
      
      warmStart = true;

      u = Eigen::Map<Eigen::Matrix<double,num_inputs,1>>(uvec.data());

      calc_A_B_w(&x,&A,&B,&w);

      x_next = A*x + B*u + w;
      x = x_next;

      std::cout<<"\nu: "<<std::endl;
      for(int j=0; j<num_inputs; j++)
      	{
      	  std::cout<<u(j)<<"  ";
      	}
      
      std::cout<<"\nx: "<<std::endl;
      for(int j=7; j<num_states; j++)
      	{
      	  std::cout<<x(j)<<"  ";
      	}

      for(int j=0; j<7; j++)
	{
	  myfile<<x(j+7)<<", ";
	}
      for(int j=0; j<7; j++)
	{
	  myfile<<u(j)<<", ";
	}
      myfile<<i<<"\n";
      
    }

  myfile.close();
  return 0;
}
