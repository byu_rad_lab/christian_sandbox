#include "n3linkdynamics.cpp"
#include "LinearEMPC.cu"
#include <Eigen/LU>
#include <ctime>
#include <fstream>

double hwrapAngle(double x)
{
  return remainder(x, 2*M_PI);
}

__device__ void my_cost_function(double * state, double * goal_state, double * u, double * goal_input, double * cost, int state_id, bool last_time_step)
{
  // If this is a position state
  if(state_id>=3)
    {
      *cost += 1.0*(*goal_state-*state)*(*goal_state-*state);
    }
  // If this is a velocity state
  else if(state_id<3)
    {
      *cost += 0.0*(*goal_state-*state)*(*goal_state-*state);
    }
  // If this is an input
  if(state_id<3)
    {
      *cost += .0000001*(*goal_input-u[state_id])*(*goal_input-u[state_id]);
    }
}


__device__ costfunc d_cost_function_pointer = my_cost_function;


int main(int argc, char * argv[])
{
  costfunc h_cost_function_pointer;
  cudaMemcpyFromSymbol(&h_cost_function_pointer, d_cost_function_pointer, sizeof(costfunc));

  const int num_states = 6;
  const int num_inputs = 3;

  std::vector<double> umin;
  std::vector<double> umax;
  std::vector<double> xmin;
  std::vector<double> xmax;
  double taumin = -10.0;
  double taumax = 10.0;
  for(int i=0; i<num_inputs; i++)
    {
      umin.push_back(taumin);
      umax.push_back(taumax);
    }
  for(int i=0; i<num_states; i++)
    {
      xmin.push_back(-9999999.0);
      xmax.push_back(9999999.0);
    }

  Eigen::MatrixXd x(num_states,1);
  x << 0,0,0,3.14,0,0;
  Eigen::MatrixXd xGoal(num_states,1);
  xGoal << 0,0.0,0,0,0,0;
  
  Eigen::MatrixXd u(num_inputs,1);
  u << 0,0,0;
  Eigen::MatrixXd uGoal(num_inputs,1);
  uGoal << 0,0,0;

  
  Eigen::MatrixXd A(num_states,num_states);
  Eigen::MatrixXd B(num_states,num_inputs);
  Eigen::MatrixXd w(num_states,1);
  std::vector<double> Avec;
  std::vector<double> Bvec;
  std::vector<double> wvec;
  Avec.resize(num_states*num_states);
  Bvec.resize(num_states*num_inputs);
  wvec.resize(num_states);
  
  LinearEMPC controller(num_states,
			num_inputs,
			h_cost_function_pointer,
			umin,
			umax,
			xmin,
			xmax,
		        100, //Horizon
			20, //NumParents
			50); //NumMates

  std::ofstream myfile;
  myfile.open("threelinkdata.txt");

  double dt = .01;
  double l = 0.5;
  double m = 0.5;
  double I = 0.05;
  double gravity = 9.81;
  double damping = .02;  
  for(int i=0; i<10000; i++)
    {
      std::vector<double> xvec(x.data(), x.data() + x.size());
      std::vector<double> uvec(u.data(), u.data() + u.size());

      std::vector<double> q;
      std::vector<double> qd;
      for(int j=0; j<num_inputs; j++)
	{
	  qd.push_back(x(j));
	  q.push_back(x(j+num_inputs));
	}

      calc_discrete_A_B_w(q,qd,dt,&A,&B,&w,l,m,I,gravity,damping);

      for(int j=0; j<num_states; j++)
	{
	  for(int k=0; k<num_states; k++)
	    {
	      Avec[j*num_states+k] = A(j,k);
	    }
	  for(int k=0; k<num_inputs; k++)
	    {
	      Bvec[j*num_inputs+k] = B(j,k);
	    }
	  wvec[j] = w(j);
	}

      std::vector<double> xGoalvec(xGoal.data(), xGoal.data() + xGoal.size());
      std::vector<double> uGoalvec(uGoal.data(), uGoal.data() + uGoal.size());
      
      double noise = 1.0*(x.block(num_inputs,0,num_inputs,1)-xGoal.block(num_inputs,0,num_inputs,1)).norm();
      
      clock_t begin = clock();
      uvec = controller.run_empc(xvec,xGoalvec,uvec,uGoalvec,Avec,Bvec,wvec,noise);
      clock_t end = clock();

      u = Eigen::Map<Eigen::Matrix<double,num_inputs,1>>(uvec.data());

      Eigen::MatrixXd x_next(num_states,1);      
      x_next = A*x + B*u + w;
      x = x_next;


      // Display and record stuff
      // std::cout<<"\nNoise: "<<noise<<std::endl;      
      // double elapsed_time = double(end-begin)/CLOCKS_PER_SEC;
      // std::cout<<"\nEMPC Solve Time: "<<elapsed_time;
      
      std::cout<<"\nu: "<<std::endl;
      for(int j=0; j<num_inputs; j++)
      	{
      	  std::cout<<u(j)<<"  ";
      	}
      
      std::cout<<"\nx: "<<std::endl;
      for(int j=0; j<num_states; j++)
      	{
      	  std::cout<<x(j)<<"  ";
      	}

      for(int j=0; j<num_states; j++)
	{
	  myfile<<x(j)<<", ";
	}
      for(int j=0; j<num_inputs; j++)
	{
	  myfile<<u(j)<<", ";
	}
      myfile<<i<<"\n";
      
    }

  myfile.close();
  return 0;
}
