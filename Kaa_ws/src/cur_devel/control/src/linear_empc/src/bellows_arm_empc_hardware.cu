#include "bellows_arm_dynamics.cpp"
#include "LinearEMPC.cu"
#include <Eigen/LU>
#include <ctime>
#include "ros/ros.h"
#include "ros_msg/SystemMinimalMsg.h"
#include "ros_srv/uint32_srv.h"
#include "ros_srv/RequestMoveSrv.h"


// These will be updated by a callback
std::vector<double> x;
std::vector<double> xGoal;

void callback(const ros_msg::SystemMinimalMsg msg)
{
  for(int jt=0;jt<3;jt++)
    {
      for(int i=0; i<4; i++)
	{
	  x[4*jt + i] = msg.joints_est[jt].prs_bel[i]/1000.0;
	}
      for(int i=0; i<2; i++)
	{
	  x[12 + 2*jt + i] = msg.joints_est[jt].qd[i];
	}
      for(int i=0; i<2; i++)
	{
	  x[18 + 2*jt + i] = msg.joints_est[jt].q[i];
	}
    }
}

__device__ void my_cost_function(double * state, double * goal_state, double * u, double * goal_input, double * cost, int state_id, bool last_time_step)
{
  if(last_time_step)
    {
      // If this is a velocity state
      if(state_id>=12 and state_id<18)
  	{
  	  *cost += 0.0*(*goal_state-*state)*(*goal_state-*state);
  	}
      // If this is a position state
      if(state_id>=18)
  	{
  	  *cost += 10.0*(*goal_state-*state)*(*goal_state-*state);
  	}
    }
  // If this is a position state
  if(state_id>=18)
    {
      *cost += 1.0*(*goal_state-*state)*(*goal_state-*state);
    }
  // If this is an input
  if(state_id<12)
    {
      *cost += 0.000001*(*goal_input-*u)*(*goal_input-*u);
    }
}

__device__ costfunc d_cost_function_pointer = my_cost_function;

void phil_sleep(double duration)
{
  clock_t begin = clock();
  while(double(clock()-begin)/CLOCKS_PER_SEC <= duration)
    {
      ros::spinOnce();
    }
}



int main(int argc, char * argv[])
{
  x.resize(24);
  xGoal.resize(24);
  std::vector<double> u;
  std::vector<double> uNext;
  u.resize(12);
  uNext.resize(12);  
  for(int i=0; i<12; i++)
    {
      u[i] = 100;
    }
  costfunc h_cost_function_pointer;

  cudaMemcpyFromSymbol(&h_cost_function_pointer, d_cost_function_pointer, sizeof(costfunc));

  const int num_states = 24;
  const int num_inputs = 12;

  std::vector<double> umin;
  std::vector<double> umax;
  std::vector<double> xmin;
  std::vector<double> xmax;
  double pmin = 8.0;
  double pmax = 400.0;
  for(int i=0; i<num_inputs; i++)
    {
      umin.push_back(pmin);
      umax.push_back(pmax);
      xmin.push_back(pmin);
      xmax.push_back(pmax);
    }
  for(int i=num_inputs; i<num_states; i++)
    {
      xmin.push_back(-9999999.0);
      xmax.push_back(9999999.0);
    }


  bool warmStart = false;
  int horizon = 100;
  double dt = .02;


  LinearEMPC controller(num_states,
			num_inputs,
			h_cost_function_pointer,
			umin,
			umax,
			xmin,
			xmax,
		        horizon,
			10,
			50);

  // Initialize ROS node
  ros::init(argc, argv, "controller_node");
  ros::NodeHandle n;
  ros::Rate loop_rate(int(1./dt));
  ros::Subscriber sub = n.subscribe("/atheris/system_minimal", 1000, callback);
  ros::ServiceClient modeService = n.serviceClient<ros_srv::uint32_srv>("/atheris/SetRobotCtrlMode");
  ros::ServiceClient pressureService= n.serviceClient<ros_srv::RequestMoveSrv>("/atheris/RequestMoveSinusoidalPressure");

  ros_srv::uint32_srv modeRequest;
  modeRequest.request.req = 1;
  modeService.call(modeRequest);

  ros_srv::RequestMoveSrv pressureRequest;
  pressureRequest.request.time = 5;
  for(int i=0; i<12; i++)
    {
      pressureRequest.request.q.push_back(100000);
    }
  pressureService.call(pressureRequest);
  phil_sleep(5);
  
  float goal_states_array[24] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,.5,-.5,-.5,.5,.5,-.5};
  for(int i=0;i<24;i++)
    {
      xGoal[i] = goal_states_array[i];
    }

  while(x[0]==0)
    {
      ros::spinOnce();
    }

  Eigen::MatrixXd A(num_states,num_states);
  Eigen::MatrixXd B(num_states,num_inputs);
  Eigen::MatrixXd w(num_states,1);
  std::vector<double> Avec;
  std::vector<double> Bvec;
  std::vector<double> wvec;
  std::vector<double> integral_term;
  Avec.resize(num_states*num_states);
  Bvec.resize(num_states*num_inputs);
  wvec.resize(num_states);
  integral_term.resize(num_states);
  double ki = .0;

  clock_t start_time = clock();
  while(ros::ok())
    {
      if(double(clock()-start_time)/CLOCKS_PER_SEC >= 10)
	{
	  xGoal[18] = -.5;
	  xGoal[19] = .5;
	  xGoal[20] = .5;
	  xGoal[21] = -.5;
	  xGoal[22] = -.5;
	  xGoal[23] = .5;
	}
      if(double(clock()-start_time)/CLOCKS_PER_SEC >= 20)
	{
	  xGoal[18] = .5;
	  xGoal[19] = .5;
	  xGoal[20] = 0;
	  xGoal[21] = 0;
	  xGoal[22] = .5;
	  xGoal[23] = .5;
	}
      // if(double(clock()-start_time)/CLOCKS_PER_SEC >= 15)
      // 	{
      // 	  xGoal[18] = -.5;
      // 	  xGoal[19] = -.5;
      // 	  xGoal[20] = .5;
      // 	  xGoal[21] = .5;
      // 	}
      if(double(clock()-start_time)/CLOCKS_PER_SEC >= 30)
	{
	  xGoal[18] = 0.001;
	  xGoal[19] = 0.001;
	  xGoal[20] = 0.001;
	  xGoal[21] = 0.001;
	  xGoal[22] = 0.001;
	  xGoal[23] = 0.001;
	}
      
      ros::spinOnce();
      double noise = 0;
      for(int i=0; i<4; i++)
	{
	  noise += 50.0*sqrt(pow(xGoal[18+i] - x[18+i],2.0)); // Good for horizon=100
	  // noise += .50*sqrt(pow(xGoal[18+i] - x[18+i],2.0)); // Good for horizon=10
	  // noise += .50*sqrt(pow(xGoal[18+i] - x[18+i],2.0));
	}
      for(int i=0; i<4; i++)
	{
	  noise += 5.*sqrt(pow(xGoal[12+i] - x[12+i],2.0)); // Good for horizon=100
	  // noise += .05*sqrt(pow(xGoal[12+i] - x[12+i],2.0)); // Good for horizon=10
	  // noise += .10*sqrt(pow(xGoal[12+i] - x[12+i],2.0));
	}

      std::vector<double> q;
      std::vector<double> qd;
      for(int j=0; j<6; j++)
	{
	  qd.push_back(x[j+12]);
	  q.push_back(x[j+18]);
	}

      calc_discrete_A_B_w(q,qd,dt,&A,&B,&w);

      for(int j=0; j<num_states; j++)
	{
	  for(int k=0; k<num_states; k++)
	    {
	      Avec[j*num_states+k] = A(j,k);
	    }
	  for(int k=0; k<num_inputs; k++)
	    {
	      Bvec[j*num_inputs+k] = B(j,k);
	    }
	  wvec[j] = w(j) + integral_term[j]*dt;
	}

      for(int j=18; j<24; j++)
	{
	  if(abs(x[j-6])<.1)
	    {
	      integral_term[j] += ki*(x[j]-xGoal[j]);
	    }
	  double integral_max = 3.0;
	  if(integral_term[j]>integral_max)
	    {
	      integral_term[j] = integral_max;
	    }
	  if(integral_term[j]<-integral_max)
	    {
	      integral_term[j] = -integral_max;
	    }
	  
	}

      std::vector<double> uGoal;
      for(int j=0; j<12; j++)
	{
	  uGoal.push_back(x[j]);
	}
      
      uNext = controller.run_empc(x,xGoal,u,uGoal,Avec,Bvec,wvec,warmStart,noise);

      std::cout<<"\nNoise: "<<noise<<std::endl;      

      warmStart = true;
      
      std::cout<<"X: "<<std::endl;
      for(int i=18; i<24; i++)
      	{
	  std::cout<<"  "<<x[i];
      	}
      std::cout<<"\nIntegral Term: "<<std::endl;
      for(int i=18; i<24; i++)
	{
	    {
	      std::cout<<"  "<<integral_term[i];
	    }
	}
      
      std::cout<<std::endl;
      
      std::cout<<"u: "<<std::endl;
      for(int i=0; i<12; i++)
      	{
      	  std::cout<<"  "<<u[i];
      	}
      std::cout<<std::endl;

      pressureRequest.request.time = 0.000001;
      for(int i=0; i<12; i++)
	{
	  // Moving average filtering
	  // pressureRequest.request.q[i] = 0.5*(uNext[i] + u[i])*1000.0;
	  // u[i] = 0.5*(uNext[i] + u[i]);

	  // No filtering
	  pressureRequest.request.q[i] = uNext[i]*1000.0;
	  u[i] = uNext[i];
	}
      pressureService.call(pressureRequest);
      loop_rate.sleep();
    }

  return 0;
}
