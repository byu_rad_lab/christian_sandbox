#include "kaa_6_dof_dynamics.cpp"
#include "LinearEMPC.cu"
#include <Eigen/LU>
#include <ctime>
#include "ros/ros.h"
#include "byu_robot/states.h"
#include "falkor/set_robot_ctrl_mode.h"
#include "falkor/move_joint_prs.h"
#include <chrono>


// These will be updated by a callback
std::vector<double> x;
std::vector<double> xGoal;

void callback(const byu_robot::states msg)
{
  for(int jt=0;jt<6;jt++)
    {
      x[18 + jt] = msg.q[jt];      
      x[12 + jt] = msg.qdot[jt];
      x[2*jt + 0] = msg.pMinus[jt]/1000.0;
      x[2*jt + 1] = msg.pPlus[jt]/1000.0;
    }
  // Todo - Swap the needed indices because Kaa is messed up
  double p0 = x[0];
  double p1 = x[1];
  x[0] = p1;
  x[1] = p0;

  double p10 = x[10];
  double p11 = x[11];
  x[10] = p11;
  x[11] = p10;
  
}

__device__ void my_cost_function(double * state, double * goal_state, double * u, double * goal_input, double * cost, int state_id, bool last_time_step)
  
{
  if(last_time_step)
    {
      // If this is a velocity state
      if(state_id>=12 and state_id<18)
  	{
  	  *cost += 0.0*(*goal_state-*state)*(*goal_state-*state);
  	}
      // If this is a position state
      if(state_id>=18)
  	{
  	  *cost += 1.0*(*goal_state-*state)*(*goal_state-*state);
  	}
    }
  // If this is a position state
  if(state_id>=18)
    {
      *cost += 1.0*(*goal_state-*state)*(*goal_state-*state);
    }
  // If this is an input
  if(state_id<12 && state_id>7)
    {
      *cost += 0.000005*(*goal_input-*u)*(*goal_input-*u);
    }
  if(state_id<12 && state_id<=7)
    {
      *cost += 0.000001*(*goal_input-*u)*(*goal_input-*u);
    }
  
}

__device__ costfunc d_cost_function_pointer = my_cost_function;

void phil_sleep(double duration)
{
  auto start = std::chrono::high_resolution_clock::now();
  auto now = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> elapsed_time = now-start;
  while(elapsed_time.count() <= duration)
    {
      ros::spinOnce();
      now = std::chrono::high_resolution_clock::now();      
      elapsed_time = now-start;      
    }
}

int main(int argc, char * argv[])
{
  x.resize(24);
  xGoal.resize(24);
  std::vector<double> u;
  std::vector<double> uNext;
  u.resize(12);
  uNext.resize(12);  
  for(int i=0; i<12; i++)
    {
      u[i] = 0.0;
    }
  costfunc h_cost_function_pointer;

  cudaMemcpyFromSymbol(&h_cost_function_pointer, d_cost_function_pointer, sizeof(costfunc));

  const int num_states = 24;
  const int num_inputs = 12;

  std::vector<double> umin;
  std::vector<double> umax;
  std::vector<double> xmin;
  std::vector<double> xmax;
  double pmin = 0.0;
  double pmax = 130.0;
  for(int i=0; i<num_inputs; i++)
    {
      umin.push_back(pmin);
      umax.push_back(pmax);
    }
  for(int i=0; i<num_states; i++)
    {
      xmin.push_back(-9999999.0);
      xmax.push_back(9999999.0);
    }

  int horizon = 50;
  double dt = .02;

  LinearEMPC controller(num_states,
			num_inputs,
			h_cost_function_pointer,
			umin,
			umax,
			xmin,
			xmax,
		        horizon,
			20,
			25);

  // Initialize ROS node
  ros::init(argc, argv, "controller_node");
  ros::NodeHandle n;
  ros::Rate loop_rate(int(1./dt));
  ros::Subscriber sub = n.subscribe("/states", 1000, callback);
  ros::ServiceClient modeService = n.serviceClient<falkor::set_robot_ctrl_mode>("/SetRobotCtrlMode");
  ros::ServiceClient pressureService= n.serviceClient<falkor::move_joint_prs>("/MoveJointPrs");

  falkor::set_robot_ctrl_mode modeRequest;
  modeRequest.request.mode = 2;
  modeService.call(modeRequest);

  std::cout<<"Sending initial pressure command 1..."<<std::endl;  
  
  falkor::move_joint_prs pressureRequest;
  for(int t=0; t<100; t++)
    {
      for(int i=0; i<3; i++)
	{
	  pressureRequest.request.joint = i;
	  pressureRequest.request.move_time = .0;
	  pressureRequest.request.move_type = 1;
	  for(int j=0; j<2; j++)
	    {
	      pressureRequest.request.target_position[2*j] = 0.0*1000.0;
	      pressureRequest.request.target_position[2*j+1] = 0.0*1000.0;	      
	    }
	  pressureService.call(pressureRequest);
	  phil_sleep(1);
	}
      std::cout<<"t: "<<t<<std::endl;
    }

  std::cout<<"Sent initial pressure command..."<<std::endl;


  
  float goal_states_array[24] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  for(int i=0;i<24;i++)
    {
      xGoal[i] = goal_states_array[i];
    }

  while(x[0]==0)
    {
      ros::spinOnce();
    }

  Eigen::MatrixXd A(num_states,num_states);
  Eigen::MatrixXd B(num_states,num_inputs);
  Eigen::MatrixXd w(num_states,1);
  std::vector<double> Avec;
  std::vector<double> Bvec;
  std::vector<double> wvec;
  std::vector<double> integral_term;
  Avec.resize(num_states*num_states);
  Bvec.resize(num_states*num_inputs);
  wvec.resize(num_states);
  integral_term.resize(num_states);
  double ki = .00;

  auto start = std::chrono::high_resolution_clock::now();
  auto now = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> elapsed_time = now-start;
  double t_settle = 30.0;
  int first_time = 1;

  std::ofstream myfile;
  myfile.open("kaa_lempc_data.txt");
  
  while(ros::ok())
    {
      now = std::chrono::high_resolution_clock::now();
      elapsed_time = now-start;      
      if(elapsed_time.count() >= t_settle*1000.0)
      	{
	  std::cout<<"Goal 1"<<std::endl;
      	  xGoal[18] = 1;
      	  xGoal[19] = -1;
      	  xGoal[20] = -1;
      	  xGoal[21] = 1;
      	  xGoal[22] = 0;
      	  xGoal[23] = 0;
      	}
      if(elapsed_time.count() >= 2*t_settle*1000.0)      
      	{
	  std::cout<<"Goal 2"<<std::endl;	  
      	  xGoal[18] = -1;
      	  xGoal[19] = 1;
      	  xGoal[20] = 1;
      	  xGoal[21] = -1;
      	  xGoal[22] = 0;
      	  xGoal[23] = 0;
      	}
      if(elapsed_time.count() >= 3*t_settle*1000.0)
      	{
	  std::cout<<"Goal 3"<<std::endl;	  	  
      	  xGoal[18] = 0;
      	  xGoal[19] = 0;
      	  xGoal[20] = -1;
      	  xGoal[21] = 0;
      	  xGoal[22] = 0;
      	  xGoal[23] = 0;
      	}
      if(elapsed_time.count() >= 4*t_settle*1000.0)
      	{
	  std::cout<<"Goal 4"<<std::endl;	  	  
      	  xGoal[18] = 0;
      	  xGoal[19] = 1;
      	  xGoal[20] = 0;
      	  xGoal[21] = -1;
      	  xGoal[22] = 0;
      	  xGoal[23] = 0;
      	}
      if(elapsed_time.count() >= 5*t_settle*1000.0)      
      	{
	  std::cout<<"Goal 5"<<std::endl;	  	  
      	  xGoal[18] = 0;
      	  xGoal[19] = 0;
      	  xGoal[20] = 0;
      	  xGoal[21] = 0;
      	  xGoal[22] = 0;
      	  xGoal[23] = 0;
      	}
      
      ros::spinOnce();
      double noise = 0;
      for(int i=0; i<6; i++)
	{
	  noise += 1.1*sqrt(pow(xGoal[18+i] - x[18+i],2.0)); // Good for horizon=100
	  // noise += .50*sqrt(pow(xGoal[18+i] - x[18+i],2.0)); // Good for horizon=10
	  // noise += .50*sqrt(pow(xGoal[18+i] - x[18+i],2.0));
	}
      for(int i=0; i<6; i++)
	{
	  noise += 0.*sqrt(pow(xGoal[12+i] - x[12+i],2.0)); // Good for horizon=100
	  // noise += .05*sqrt(pow(xGoal[12+i] - x[12+i],2.0)); // Good for horizon=10
	  // noise += .10*sqrt(pow(xGoal[12+i] - x[12+i],2.0));
	}

      std::vector<double> q;
      std::vector<double> qd;
      for(int j=0; j<6; j++)
	{
	  qd.push_back(x[j+12]);
	  q.push_back(x[j+18]);
	}

      calc_discrete_A_B_w(q,qd,dt,&A,&B,&w);

      for(int j=0; j<num_states; j++)
	{
	  for(int k=0; k<num_states; k++)
	    {
	      Avec[j*num_states+k] = A(j,k);
	    }
	  for(int k=0; k<num_inputs; k++)
	    {
	      Bvec[j*num_inputs+k] = B(j,k);
	    }
	  wvec[j] = w(j) + integral_term[j]*dt;
	}

      for(int j=18; j<24; j++)
	{
	  if(abs(x[j-6])<.1)
	    {
	      integral_term[j] += ki*(x[j]-xGoal[j]);
	    }
	  double integral_max = 3.0;
	  if(integral_term[j]>integral_max)
	    {
	      integral_term[j] = integral_max;
	    }
	  if(integral_term[j]<-integral_max)
	    {
	      integral_term[j] = -integral_max;
	    }
	  
	}

      std::vector<double> uGoal;
      for(int j=0; j<12; j++)
	{
	  uGoal.push_back(u[j]);
	}

      if(first_time)
	{
	  noise = 1.0;
	  uNext = controller.run_empc(x,xGoal,u,uGoal,Avec,Bvec,wvec,noise,-1,100);
	  first_time=0;
	}
      
      auto t1 = std::chrono::high_resolution_clock::now();
      uNext = controller.run_empc(x,xGoal,u,uGoal,Avec,Bvec,wvec,noise);
      auto t2 = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double, std::milli> solve_time = t2-t1;
      std::cout<<"Solve time: "<<solve_time.count()/1000.0<<std::endl;

      std::cout<<"\nNoise: "<<noise<<std::endl;      

      std::cout<<"X: "<<std::endl;
      for(int i=18; i<24; i++)
      	{
	  std::cout<<"  "<<x[i];
      	}
      /* std::cout<<"\nIntegral Term: "<<std::endl; */
      /* for(int i=18; i<24; i++) */
      /* 	{ */
      /* 	    { */
      /* 	      std::cout<<"  "<<integral_term[i]; */
      /* 	    } */
      /* 	} */
      
      std::cout<<std::endl;
      
      std::cout<<"u: "<<std::endl;
      for(int i=0; i<12; i++)
      	{
      	  std::cout<<"  "<<u[i];
      	}
      std::cout<<std::endl;

      pressureRequest.request.move_time = 0.0;
      pressureRequest.request.move_time = 0;
      pressureRequest.request.move_type = 1;
      
      pressureRequest.request.joint = 0;	  
      pressureRequest.request.target_position[0] = u[0]*1000.0;
      pressureRequest.request.target_position[1] = u[1]*1000.0;
      pressureRequest.request.target_position[2] = u[3]*1000.0;
      pressureRequest.request.target_position[3] = u[2]*1000.0;
      pressureService.call(pressureRequest);
      
      pressureRequest.request.joint = 1;	  	  
      pressureRequest.request.target_position[0] = u[5]*1000.0;
      pressureRequest.request.target_position[1] = u[4]*1000.0;
      pressureRequest.request.target_position[2] = u[7]*1000.0;
      pressureRequest.request.target_position[3] = u[6]*1000.0;
      pressureService.call(pressureRequest);
      
      pressureRequest.request.joint = 2;
      pressureRequest.request.target_position[0] = u[9]*1000.0;
      pressureRequest.request.target_position[1] = u[8]*1000.0;
      pressureRequest.request.target_position[2] = u[11]*1000.0;
      pressureRequest.request.target_position[3] = u[10]*1000.0;
      pressureService.call(pressureRequest);

      for(int i=0; i<12; i++)
	{
	  u[i] = uNext[i];
	}
      
      for(int j=0; j<num_states; j++)
	{
	  myfile<<x[j]<<", ";
	}
      for(int j=0; j<num_inputs; j++)
	{
	  myfile<<u[j]<<", ";
	}
      myfile<<solve_time.count()<<", ";
      myfile<<elapsed_time.count()<<"\n";
      
      loop_rate.sleep();

      if(elapsed_time.count()>t_settle*6*1000.0)
	{
	  ros::shutdown();
	}
    }

  return 0;
}
