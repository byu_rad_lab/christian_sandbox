#include "LinearEMPC.cu"
#include <Eigen/LU>
#include <ctime>
#include <fstream>

typedef void (*dynamicsfunc)(std::vector<double> q,std::vector<double> qd, double dt, Eigen::MatrixXd * A_d, Eigen::MatrixXd * B_d, Eigen::MatrixXd * w_d,double l,double m,double I,double gravity,double damping);

void run_empc_sim(const int n, const int num_states, const int num_inputs, Eigen::MatrixXd x,Eigen::MatrixXd xGoal, costfunc d_cost_function_pointer, dynamicsfunc calc_discrete_A_B_w);
