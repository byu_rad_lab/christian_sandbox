#include "LinearEMPC.cu"
#include <Eigen/LU>
#include <ctime>
#include <fstream>

typedef void (*dynamicsfunc)(std::vector<double> q,std::vector<double> qd, double dt, Eigen::MatrixXd * A_d, Eigen::MatrixXd * B_d, Eigen::MatrixXd * w_d,double l,double m,double I,double gravity,double damping);

void run_empc_sim(const int n, const int num_states, const int num_inputs, Eigen::MatrixXd x,Eigen::MatrixXd xGoal, costfunc d_cost_function_pointer, dynamicsfunc calc_discrete_A_B_w)  
{
  double dt = .01;
  double l = 0.125;
  double m = 0.125;
  double I = m*l*l/12.0;
  double gravity = 9.81;
  double damping = n/10.0;
  double mpc_m = m;
  double mpc_l = l;
  double mpc_I = I;

  costfunc h_cost_function_pointer;
  cudaMemcpyFromSymbol(&h_cost_function_pointer, d_cost_function_pointer, sizeof(costfunc));


  std::vector<double> umin;
  std::vector<double> umax;
  std::vector<double> xmin;
  std::vector<double> xmax;
  double taumax = 0;
  for(int i=0; i<n; i++)
    {
      taumax += m*gravity*l*((double)i+.5);
    }
  double taumin = -taumax;
  std::cout<<"Tau max: "<<taumax;
  for(int i=0; i<num_inputs; i++)
    {
      umin.push_back(taumin);
      umax.push_back(taumax);
    }
  for(int i=0; i<num_states; i++)
    {
      xmin.push_back(-9999999.0);
      xmax.push_back(9999999.0);
    }
  
  Eigen::MatrixXd u(num_inputs,1);
  Eigen::MatrixXd uGoal(num_inputs,1);  
  for(int i=0; i<num_inputs; i++)
    {
      u(i) = 0.0;
      uGoal(i) = 0.0;
    }
  
  Eigen::MatrixXd A(num_states,num_states);
  Eigen::MatrixXd B(num_states,num_inputs);
  Eigen::MatrixXd w(num_states,1);
  Eigen::MatrixXd mpcA(num_states,num_states);
  Eigen::MatrixXd mpcB(num_states,num_inputs);
  Eigen::MatrixXd mpcw(num_states,1);  
  Eigen::MatrixXd Q(num_states,num_states);
  Eigen::MatrixXd R(num_inputs,num_inputs);
  Q.setZero();
  for(int i=0; i<num_inputs; i++)
    {
      Q(i,i) = 1.0/(5.0*(i+1));
    }
  for(int i=0; i<num_inputs; i++)
    {
      R(i,i) = .001*5.0*(i);
    }
  

  
  std::vector<double> Avec;
  std::vector<double> Bvec;
  std::vector<double> wvec;
  Avec.resize(num_states*num_states);
  Bvec.resize(num_states*num_inputs);
  wvec.resize(num_states);
  
  LinearEMPC controller(num_states,
			num_inputs,
			h_cost_function_pointer,
			umin,
			umax,
			xmin,
			xmax,
		        100, //Horizon
			25, //NumParents
			20); //NumMates

  std::ofstream myfile;
  myfile.open("nlinkdata.txt");

  double actual_cost = 0;  

  for(int i=0; i<500; i++)
    {
      std::vector<double> xvec(x.data(), x.data() + x.size());
      std::vector<double> uvec(u.data(), u.data() + u.size());

      std::vector<double> q;
      std::vector<double> qd;
      for(int j=0; j<num_inputs; j++)
	{
	  qd.push_back(x(j));
	  q.push_back(x(j+num_inputs));
	}

      calc_discrete_A_B_w(q,qd,dt,&A,&B,&w,l,m,I,gravity,damping);
      calc_discrete_A_B_w(q,qd,dt,&mpcA,&mpcB,&mpcw,mpc_l,mpc_m,mpc_I,gravity,damping);

      for(int j=0; j<num_states; j++)
	{
	  for(int k=0; k<num_states; k++)
	    {
	      Avec[j*num_states+k] = mpcA(j,k);
	    }
	  for(int k=0; k<num_inputs; k++)
	    {
	      Bvec[j*num_inputs+k] = mpcB(j,k);
	    }
	  wvec[j] = mpcw(j);
	}

      std::vector<double> xGoalvec(xGoal.data(), xGoal.data() + xGoal.size());
      std::vector<double> uGoalvec(uGoal.data(), uGoal.data() + uGoal.size());
      
      double noise = 0.1*(x.block(num_inputs,0,num_inputs,1)-xGoal.block(num_inputs,0,num_inputs,1)).lpNorm<1>();
      // double noise = 0.5*(x.block(num_inputs,0,num_inputs,1)-xGoal.block(num_inputs,0,num_inputs,1)).lpNorm<2>();
      // double noise = 1.0*(x.block(num_inputs,0,1,1)-xGoal.block(num_inputs,0,1,1)).lpNorm<1>();
      
      //For velocity
      noise += 0.1*(x.block(0,0,num_inputs,1)-xGoal.block(0,0,num_inputs,1)).lpNorm<Eigen::Infinity>();
      

      clock_t begin = clock();
      uvec = controller.run_empc(xvec,xGoalvec,uvec,uGoalvec,Avec,Bvec,wvec,noise,-1,3);
      clock_t end = clock();

      // u = Eigen::Map<Eigen::Matrix<double,num_inputs,1>>(uvec.data());

      Eigen::MatrixXd x_next(num_states,1);      
      x_next = A*x + B*u + w;
      x = x_next;

      Eigen::MatrixXd tmp = x.transpose()*Q*x;// + u.transpose()*R*u;
      actual_cost += tmp(0,0);


      // Display and record stuff
      // std::cout<<"\nNoise: "<<noise<<std::endl;      
      double solve_time = double(end-begin)/CLOCKS_PER_SEC;
      // std::cout<<"\nEMPC Solve Time: "<<solve_time;
      
      // std::cout<<"\nu: "<<std::endl;
      // for(int j=0; j<num_inputs; j++)
      // 	{
      // 	  std::cout<<u(j)<<"  ";
      // 	}
      
      // std::cout<<"\nx: "<<std::endl;
      // for(int j=num_inputs; j<num_states; j++)
      // 	{
      // 	  std::cout<<x(j)<<"  ";
      // 	}

      for(int j=0; j<num_states; j++)
	{
	  myfile<<x(j)<<", ";
	}
      for(int j=0; j<num_inputs; j++)
	{
	  myfile<<u(j)<<", ";
	}
      myfile<<solve_time<<", ";
      myfile<<i<<"\n";
      
    }

  std::cout<<"\nactual_cost "<<actual_cost<<std::endl;
  myfile.close();
}
