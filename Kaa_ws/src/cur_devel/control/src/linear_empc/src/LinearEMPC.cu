#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/device_ptr.h>
#include <curand.h>
#include <curand_kernel.h>
#include <math.h>
#include <ctime>

const int NUM_POINTS = 3;

//syntax here was a little wonky, the typedef is strange, but Phil spent a lot of time getting this working ... are there other ways? 
//idea is to make a cost function that takes certain arguments and returns nothing, but runs on GPU (since one of doubles is the cost)
//Mat found this link to explain it - https://stackoverflow.com/questions/4295432/typedef-function-pointer
typedef void (*costfunc)(double*, double*, double*, double*, double*, int, bool);


void make_histogram(thrust::host_vector<double> vec,int num_bins)
{
  std::vector<int> bins;
  bins.resize(num_bins);
  int num_elements = vec.size();
  auto min_it = thrust::min_element(vec.begin(),vec.end());
  auto max_it = thrust::max_element(vec.begin(),vec.end());
  double min_cost = *min_it;
  double max_cost = *max_it;
  double cost_range = max_cost-min_cost;
  double bin_range = cost_range/(double)num_bins;

  for(int k=0; k<num_elements; k++)
    {
      for(int k2=0; k2<num_bins; k2++)
	{
	  if(vec[k] >= (double)k2*bin_range+min_cost && vec[k] <= (double)(k2+1)*bin_range+min_cost)
	    {
	      bins[k2]++;
	    }
	}
    }

  std::cout<<std::endl;
  for(int k=0; k<num_bins; k++)
    {
      std::cout<<(double)k*bin_range+min_cost<<": ";
      for(int k2=0; k2<bins[k]; k2++)
	{
	  std::cout<<"*";
	}
      std::cout<<std::endl;
    }
}

//each thread has it's own state (row of A and B and w) and forward simulates given the commanded control
__device__ void simulate_discrete(int NUM_STATES, int NUM_INPUTS, int STATE_ID, double * Ad, double * Bd, double * wd, double * xt, double * xt_plus_one, double * u)
{
  (*xt_plus_one) = wd[STATE_ID];

  for(int i=0; i<NUM_STATES; i++)
    {
      (*xt_plus_one) = (*xt_plus_one) + Ad[STATE_ID*NUM_STATES+i]*xt[i];
    }
  
  for(int i=0; i<NUM_INPUTS; i++)
    {
      (*xt_plus_one) = (*xt_plus_one) + Bd[STATE_ID*NUM_INPUTS+i]*u[i];
    }
}


__global__ void total_sim_cost(int * NUM_STATES,
			       int * NUM_SIMS,
			       double * d_out_costs,
			       double * d_sim_costs)
{
  int SIM_ID = threadIdx.x;
  double sim_cost = 0.0;
  for(int i=0; i<(*NUM_STATES); i++)
    {
      sim_cost += abs(d_out_costs[(*NUM_STATES)*SIM_ID + i]);
    }

  d_sim_costs[SIM_ID] = sim_cost;
}


__device__ void randomly_mate_parents(int *d_NUM_INPUTS,
				      double * parent1_points,
				      double * parent2_points,
				      curandState_t * state,
				      double * child_points)
{

  // for(int i=0;i<NUM_POINTS;i++)
  //   {
  //     if(curand_normal(state)<0.0)
  // 	{
  // 	  child_points[i] = parent1_points[i];
  // 	}
  //     else
  // 	{
  // 	  child_points[i] = parent2_points[i];
  // 	}
  //   }
  if(curand_normal(state)<0)
    {
      for(int i=0;i<NUM_POINTS;i++) 
  	{
  	  child_points[i] = parent1_points[i];
  	}
    }
  else
    {
      for(int i=0;i<NUM_POINTS;i++) 
  	{
  	  child_points[i] = parent2_points[i];
  	}
    }
}


__device__ void mutate_child(double * child_points,
			     curandState_t * state,
			     double noise,
			     double * d_umin,
			     double * d_umax,
			     int STATE_ID)
{
  for(int i=0; i<NUM_POINTS; i++)
    {
      child_points[i] = child_points[i] + curand_normal(state)*noise;
      child_points[i] = max(min(d_umax[STATE_ID],child_points[i]),d_umin[STATE_ID]);
    }
}

__device__ void get_cold_start_points(double * child_points,
				      curandState_t * state,
				      double *d_umin,
				      double *d_umax,
				      int STATE_ID)
{
  double uRange = (d_umax[STATE_ID]-d_umin[STATE_ID])*.5;
  for(int i=0; i<NUM_POINTS; i++)
    {
      child_points[i] = curand_uniform(state)*uRange + d_umin[STATE_ID];
    }
}

__device__ void fitness_function(int *d_NUM_STATES,
				 int *d_NUM_INPUTS,
				 double *d_umin,
				 double *d_umax,
				 double *d_xmin,
				 double *d_xmax,
				 int *d_HORIZON,
				 double * child_points,
				 costfunc cost_fcn,
				 double *d_xinit,
				 double *d_xgoal,
				 double *d_ugoal,
				 double *d_A_matrix,
				 double *d_B_matrix,
				 double *d_disturbance,
				 double *d_out_points,
				 double *d_out_costs)
{
  int SIM_ID = blockIdx.x*gridDim.y + blockIdx.y;
  int STATE_ID = threadIdx.x;
  int NUM_STATES = *d_NUM_STATES;
  int NUM_INPUTS = *d_NUM_INPUTS;  
  
  int HORIZON = *d_HORIZON;
  double cost = 0.0;
  
  double x_goal = d_xgoal[STATE_ID];
  double u_goal = d_ugoal[STATE_ID];

  //each thread can't have it's own state vector, because it couldn't see the states from the other threads. 
  extern __shared__ double smem[];
  double * x = &smem[0];
  double * u = &smem[NUM_STATES];
  x[STATE_ID] = d_xinit[STATE_ID];

  double xt_plus_one;
  double interp_delta_t = (1.0*NUM_POINTS-1)/(1.0*HORIZON);
  double interp_t = interp_delta_t;
  int idx = 0;

  __syncthreads();

  for(int k=0; k<HORIZON; k++)
    {
      if (interp_t>=1)
  	{
  	  interp_t = 0.0;
  	  idx += 1;
  	}

      __syncthreads();
      
      \\ just getting what inputs should be applied to what state at what time
      if(STATE_ID<NUM_INPUTS)  // 14 states for Baxter for example, but only 7 inputs, assumes a specific order for control 
      	{
	  u[STATE_ID] = (1.0-interp_t)*child_points[idx] + interp_t*child_points[idx+1];
	}
      interp_t = interp_t + interp_delta_t;

      __syncthreads();
      simulate_discrete(NUM_STATES,NUM_INPUTS,STATE_ID,d_A_matrix,d_B_matrix,d_disturbance,x,&xt_plus_one,u);
      __syncthreads(); 
      
      x[STATE_ID] = xt_plus_one;
      
      \\two different functions are just because there are states with inputs and states without inputs
      if(STATE_ID<NUM_INPUTS)
      	{
      	  cost_fcn(&x[STATE_ID],&x_goal,&u[STATE_ID],&u_goal,&cost, STATE_ID,false);
      	}
      else
      	{
      	  double zero = 0.0;
      	  cost_fcn(&x[STATE_ID],&x_goal,&zero,&zero,&cost, STATE_ID,false);
      	}
      __syncthreads();
    }
      
  //terminal cost calculation
  if(STATE_ID<NUM_INPUTS)
    {
      cost_fcn(&x[STATE_ID],&x_goal,&u[STATE_ID],&u_goal,&cost, STATE_ID,true);
    }
  else
    {
      double zero = 0.0;
      cost_fcn(&x[STATE_ID],&x_goal,&zero,&zero,&cost, STATE_ID,true);
    }
  __syncthreads();


  //array of all of the knot points for all of the simulations for all of the degrees of freedom 
  if(STATE_ID<NUM_INPUTS)
    {
      for(int j=0;j<NUM_POINTS;j++)
  	{
      //what simulation, then what input, then what point in time or in the trajectory
  	  d_out_points[SIM_ID*NUM_INPUTS*NUM_POINTS + NUM_POINTS*STATE_ID + j] = child_points[j];
  	}
    }
    
  //array of all costs for the correct groupings of knot points (since 4 knot points for a single sim, they have the same cost), 
  //also assigns a cost for states with no input (like position)
  d_out_costs[SIM_ID*NUM_STATES + STATE_ID] = cost;
  __syncthreads();
}






__global__ void evolution(int *d_NUM_STATES,
			  int *d_NUM_INPUTS,
			  int *d_mating_order,
			  double *d_umin,
			  double *d_umax,
			  double *d_xmin,
			  double *d_xmax,
			  int *d_HORIZON,
			  double *d_exploration_noise,
			  double *d_u_last,
			  int *d_warm_start,			  
			  int *d_time,
			  costfunc cost_fcn,
			  double *d_xinit,
			  double *d_xgoal,
			  double *d_ugoal,
			  double *d_A_matrix,
			  double *d_B_matrix,
			  double *d_disturbance,
			  double *d_parents_points,
			  double *d_out_points,
			  double *d_out_costs)
{
  int SIM_ID = blockIdx.x*gridDim.y + blockIdx.y;
  int PARENT1_ID = blockIdx.x;
  int PARENT2_ID = d_mating_order[SIM_ID];
  int STATE_ID = threadIdx.x;
  int NUM_INPUTS = *d_NUM_INPUTS;
  bool warm_start = *d_warm_start;
  
  double child_points[NUM_POINTS] = {};


  if(STATE_ID<NUM_INPUTS)
    {
      __syncthreads();

      curandState_t state;
      curand_init(*d_time,NUM_POINTS*NUM_INPUTS*SIM_ID + NUM_POINTS*STATE_ID + STATE_ID,0,&state);

      if(warm_start==1)
	{
	  double * parent1_points = &d_parents_points[PARENT1_ID*NUM_INPUTS*NUM_POINTS + NUM_POINTS*STATE_ID];
	  double * parent2_points = &d_parents_points[PARENT2_ID*NUM_INPUTS*NUM_POINTS + NUM_POINTS*STATE_ID];
	  randomly_mate_parents(d_NUM_INPUTS,parent1_points,parent2_points,&state,child_points);
	  if(curand_uniform(&state)<0.5)
	    {
	      mutate_child(child_points,&state,*d_exploration_noise,d_umin,d_umax,STATE_ID);
	    }
	}

      else if(warm_start==0)
	{
	  get_cold_start_points(child_points,&state,d_umin,d_umax,STATE_ID);
	  if(SIM_ID==0)
	    {
	      for(int i=0; i<NUM_POINTS; i++)
	  	{
	  	  // child_points[i] = (d_umax[STATE_ID]+d_umin[STATE_ID])*.5;
	  	  child_points[i] = d_ugoal[STATE_ID]; //at least one of the population should be the actual goal 
                                               // Mat Haskell also suggested that one of the children could be an equilibrium input 
	  	}
	    }
	}

      //   we used to do this to smooth out performance (which it did) and would apply 2nd input instead
      //   but not necessary, and performance got better if we didn't, also required more explanation for why we did it in papers
      // THIS CONSTRAINS THE FIRST POINT
      // child_points[0] = d_u_last[STATE_ID];
    }

  fitness_function(d_NUM_STATES,
  		   d_NUM_INPUTS,
  		   d_umin,
  		   d_umax,
  		   d_xmin,
  		   d_xmax,
  		   d_HORIZON,
  		   child_points,
  		   cost_fcn,
  		   d_xinit,
  		   d_xgoal,
  		   d_ugoal,
  		   d_A_matrix,
  		   d_B_matrix,
  		   d_disturbance,
  		   d_out_points,
  		   d_out_costs);  
}




class LinearEMPC
{
public:
  int h_NUM_STATES;
  int h_NUM_INPUTS;  
  int h_NUM_PARENTS;
  int h_NUM_MATES;
  int h_NUM_SIMS;
  double h_exploration_noise;  

  //reminder that h_ variables are host and d_ is device(GPU)
  //thrust is a nice library for doing a standard vector on CPU and GPU with sorting functions, etc.
  thrust::host_vector<double> h_umin;
  thrust::host_vector<double> h_umax;
  thrust::host_vector<double> h_xmin;
  thrust::host_vector<double> h_xmax;
  thrust::host_vector<double> h_xinit;
  thrust::host_vector<double> h_xgoal;
  thrust::host_vector<double> h_ugoal;  
  thrust::host_vector<double> h_A_matrix;
  thrust::host_vector<double> h_B_matrix;
  thrust::host_vector<double> h_disturbance;
  thrust::host_vector<double> h_parents_points;
  thrust::host_vector<int> h_mating_order;
  thrust::host_vector<double> h_u_last;
  thrust::host_vector<double> h_out_points;
  thrust::host_vector<double> h_out_costs;
  thrust::host_vector<double> h_sim_costs;  
  thrust::host_vector<int> h_sim_cost_keys;
  int h_HORIZON;
  int h_warm_start;
  costfunc h_cost_fcn_ptr;    
  int h_time[1];
  
  dim3 GridSize;
  dim3 BlockSize;
  int SHARED_MEMORY_BYTES;

  thrust::device_vector<double> d_umin;
  thrust::device_vector<double> d_umax;
  thrust::device_vector<double> d_xmin;
  thrust::device_vector<double> d_xmax;
  thrust::device_vector<double> d_xinit;
  thrust::device_vector<double> d_xgoal;
  thrust::device_vector<double> d_ugoal;
  thrust::device_vector<double> d_A_matrix;
  thrust::device_vector<double> d_B_matrix;
  thrust::device_vector<double> d_disturbance;  
  thrust::device_vector<double> d_parents_points;
  thrust::device_vector<int> d_mating_order;
  thrust::device_vector<double> d_u_last;
  thrust::device_vector<double> d_out_points;
  thrust::device_vector<double> d_out_costs;
  thrust::device_vector<double> d_sim_costs;  
  thrust::device_vector<int> d_sim_cost_keys;

  int *d_NUM_STATES;  
  int *d_NUM_INPUTS;
  int *d_NUM_SIMS;
  int *d_warm_start;  
  int *d_HORIZON;
  int *d_time;
  double *d_exploration_noise;    

  ~LinearEMPC()
  {
    cudaFree((void**)&d_NUM_SIMS);
    cudaFree((void**)&d_warm_start);
    cudaFree((void**)&d_HORIZON);
    cudaFree((void**)&d_time);
  }
  
  LinearEMPC(int num_states,
	     int num_inputs,
	     costfunc cost_fcn,
	     std::vector<double> umin,
	     std::vector<double> umax,
	     std::vector<double> xmin,
	     std::vector<double> xmax,
	     int horizon=100,
	     int num_parents=10,
	     int num_mates=35)
  {
    h_warm_start = 0;
    h_NUM_STATES = num_states;
    h_NUM_INPUTS = num_inputs;
    for(int i=0; i<umax.size(); i++)
      {
	h_umin.push_back(umin[i]);
	h_umax.push_back(umax[i]);
      }
    for(int i=0; i<xmax.size(); i++)
      {
	h_xmin.push_back(xmin[i]);
	h_xmax.push_back(xmax[i]);
      }
    h_HORIZON = horizon;	
    h_NUM_PARENTS = num_parents;
    h_NUM_MATES = num_mates;
    h_cost_fcn_ptr = cost_fcn;
    h_NUM_SIMS = h_NUM_PARENTS*h_NUM_MATES;
    GridSize = dim3( h_NUM_PARENTS, h_NUM_MATES, 1 );
    BlockSize = dim3(h_NUM_STATES, 1, 1);
    SHARED_MEMORY_BYTES = (h_NUM_STATES+h_NUM_INPUTS)*sizeof(double);
    
    h_xinit.resize(h_NUM_STATES);
    h_xgoal.resize(h_NUM_STATES);
    h_ugoal.resize(h_NUM_INPUTS);
    h_A_matrix.resize(h_NUM_STATES*h_NUM_STATES);
    h_B_matrix.resize(h_NUM_STATES*h_NUM_INPUTS);
    h_disturbance.resize(h_NUM_STATES);
    h_parents_points.resize(h_NUM_PARENTS*h_NUM_INPUTS*NUM_POINTS);
    h_mating_order.resize(h_NUM_SIMS);
    for(int i=0; i<h_NUM_MATES; i++)
      {
	for(int j=0; j<h_NUM_PARENTS; j++)
	  {
	    h_mating_order.push_back(j);
	  }
      }
    h_u_last.resize(h_NUM_INPUTS);
    h_out_points.resize(h_NUM_SIMS*h_NUM_INPUTS*NUM_POINTS);
    h_out_costs.resize(h_NUM_SIMS*h_NUM_STATES);
    h_sim_costs.resize(h_NUM_SIMS);    
    h_sim_cost_keys.resize(h_NUM_SIMS);
    for(int i=0; i<h_NUM_SIMS; i++)
      {
    	h_sim_cost_keys[i] = i;
      }

    //copying all of these vectors from CPU to GPU once is intentional because copying data is expensive
    d_umin = h_umin;  // just copying vectors from GPU to CPU, if don't use "thrust" library, then you have to do the cudaMalloc as shown above
    d_umax = h_umax;
    d_xmin = h_xmin;
    d_xmax = h_xmax;
    d_xinit = h_xinit;
    d_xgoal = h_xgoal;
    d_ugoal = h_ugoal;        
    d_A_matrix = h_A_matrix;
    d_B_matrix = h_B_matrix;
    d_disturbance = h_disturbance;
    d_parents_points = h_parents_points;
    d_mating_order = h_mating_order;
    d_u_last = h_u_last;
    d_out_points = h_out_points;
    d_out_costs = h_out_costs;
    d_sim_costs = h_sim_costs;    
    d_sim_cost_keys = h_sim_cost_keys;    
    cudaMalloc( (void**)&d_NUM_STATES,sizeof(int));
    cudaMalloc( (void**)&d_NUM_INPUTS,sizeof(int));	
    cudaMalloc( (void**)&d_NUM_SIMS,sizeof(int));
    cudaMalloc( (void**)&d_warm_start,sizeof(int));
    cudaMalloc( (void**)&d_HORIZON,sizeof(int));
    cudaMalloc( (void**)&d_time,sizeof(int));
    cudaMalloc( (void**)&d_exploration_noise,sizeof(double));
    
    cudaMemcpy( d_HORIZON, &h_HORIZON, sizeof(int), cudaMemcpyHostToDevice );
    cudaMemcpy( d_NUM_STATES, &h_NUM_STATES, sizeof(int), cudaMemcpyHostToDevice );
    cudaMemcpy( d_NUM_INPUTS, &h_NUM_INPUTS, sizeof(int), cudaMemcpyHostToDevice );	
    cudaMemcpy( d_NUM_SIMS, &h_NUM_SIMS, sizeof(int), cudaMemcpyHostToDevice );
    cudaDeviceSynchronize();
  }

  std::vector<double> best_points2u()
  {
    std::vector<double> u;
    u.resize(h_NUM_INPUTS);

    for(int i=0; i<h_NUM_INPUTS; i++)
      {
	int best_idx = NUM_POINTS*i;

	// WHEN CONSTRAINING FIRST POINT	
	// double delta_t = 1.0*(1.0*NUM_POINTS-1)/(1.0*h_HORIZON);
	// u[i] = delta_t*h_parents_points[best_idx+1] + (1.0-delta_t)*h_parents_points[best_idx];

	// WHEN NOT CONSTRAINING FIRST POINT
	u[i] = h_parents_points[best_idx];
      }
    return u;
  }
  
  std::vector<double> run_empc(std::vector<double> states,
			       std::vector<double> goalStates,
			       std::vector<double> uLast,
			       std::vector<double> uGoal,
			       std::vector<double> A_matrix,
			       std::vector<double> B_matrix,
			       std::vector<double> disturbance,
			       double noise,
			       int warmStart=-1,
			       int iterations=1)
  {
    if(warmStart>-1)
      {
	h_warm_start = warmStart;
      }
      
    //this is a CPU to CPU copy, but not sure if necessary (it was std::vector to thrust vector)
    h_xinit = states; 
    h_xgoal = goalStates;
    h_ugoal = uGoal;
    h_u_last = uLast;
    h_A_matrix = A_matrix;
    h_B_matrix = B_matrix;
    h_disturbance = disturbance;

    d_xinit = h_xinit;
    d_xgoal = h_xgoal;
    d_ugoal = h_ugoal;
    d_A_matrix = h_A_matrix;
    d_B_matrix = h_B_matrix;
    d_disturbance = h_disturbance;

    d_u_last = h_u_last;
    cudaMemcpy( d_exploration_noise, &noise, sizeof(double), cudaMemcpyHostToDevice );

    for(int i=0; i<iterations; i++)
      {
	cudaMemcpy( d_warm_start, &h_warm_start, sizeof(int), cudaMemcpyHostToDevice );
	
    // not sure why Phil did this, is it hard to get random numbers? not sure he doesn't remember
    // just can't remember what mating_order is or how he uses it
    // might be a better way to pick mates and do mating ... 
	std::random_shuffle (h_mating_order.begin(), h_mating_order.end());
	d_mating_order = h_mating_order;
    
	d_parents_points = h_parents_points;
	clock_t now_time = clock();
	h_time[0] = (int)(now_time);
	cudaMemcpy( d_time, h_time, sizeof(int), cudaMemcpyHostToDevice );
	cudaDeviceSynchronize();

    //calling a GPU function on the GPU, all stuff needed has already been copied to GPU
    // all arguments are pointers to data/functions on GPU
	evolution<<<GridSize,BlockSize,SHARED_MEMORY_BYTES>>>(d_NUM_STATES,
							      d_NUM_INPUTS,
							      thrust::raw_pointer_cast(&d_mating_order[0]),
							      thrust::raw_pointer_cast(&d_umin[0]),
							      thrust::raw_pointer_cast(&d_umax[0]),
							      thrust::raw_pointer_cast(&d_xmin[0]),
							      thrust::raw_pointer_cast(&d_xmax[0]),
							      d_HORIZON,
							      d_exploration_noise,
							      thrust::raw_pointer_cast(&d_u_last[0]),
							      d_warm_start,
							      d_time,
							      h_cost_fcn_ptr,
							      thrust::raw_pointer_cast(&d_xinit[0]),
							      thrust::raw_pointer_cast(&d_xgoal[0]),
							      thrust::raw_pointer_cast(&d_ugoal[0]),
							      thrust::raw_pointer_cast(&d_A_matrix[0]),
							      thrust::raw_pointer_cast(&d_B_matrix[0]),
							      thrust::raw_pointer_cast(&d_disturbance[0]),
							      thrust::raw_pointer_cast(&d_parents_points[0]),
							      thrust::raw_pointer_cast(&d_out_points[0]),
							      thrust::raw_pointer_cast(&d_out_costs[0]));
    
	cudaDeviceSynchronize();

    //only have a cost for each state, not for each simulation, so need to sum across states
	int num_blocks = h_NUM_SIMS/1024 + 1;
	total_sim_cost<<<num_blocks,h_NUM_SIMS%1024>>>(d_NUM_STATES,
						       d_NUM_SIMS,
						       thrust::raw_pointer_cast(&d_out_costs[0]),
						       thrust::raw_pointer_cast(&d_sim_costs[0]));
	cudaDeviceSynchronize();

    // this sorts sims by cost so that very best 
	// Sequence the keys from 1 to NUM_SIMS
	thrust::sequence(d_sim_cost_keys.begin(),d_sim_cost_keys.end());
	cudaDeviceSynchronize();    

	// Sort the costs and keys
	thrust::sort_by_key(d_sim_costs.begin(),d_sim_costs.end(),d_sim_cost_keys.begin());
	cudaDeviceSynchronize();    

	//Copy sorted costs and keys back onto the host
	h_out_points = d_out_points;
	h_sim_cost_keys = d_sim_cost_keys;
	cudaDeviceSynchronize();

    //copying parents back to host - if 50 parents, find 50 best parents, and send them back
    // in future work, we could try to not send all parents back to CPU, but instead keep it locally on GPU and 
	for(int parent=0; parent<h_NUM_PARENTS; parent++)
	  {
	    for(int input=0; input<h_NUM_INPUTS; input++)
	      {
		for(int point=0; point<NUM_POINTS; point++)
		  {
		    h_parents_points[NUM_POINTS*h_NUM_INPUTS*parent + NUM_POINTS*input + point] = h_out_points[h_sim_cost_keys[parent]*h_NUM_INPUTS*NUM_POINTS + NUM_POINTS*input + point];
		  }
	      }
	  }

	// Visualize the costs and points
	if(0)
	  {
	    h_sim_costs = d_sim_costs;
	    int num_bins = 20;

	    make_histogram(h_sim_costs,num_bins);

	    thrust::host_vector<thrust::host_vector<thrust::host_vector<double>>> pt_vecs;
	    pt_vecs.resize(h_NUM_INPUTS);
	    for(int k=0; k<h_NUM_INPUTS; k++)
	      {
		pt_vecs[k].resize(NUM_POINTS);
		for(int k2=0; k2<NUM_POINTS; k2++)
		  {
		    pt_vecs[k][k2].resize(h_NUM_SIMS);
		    for(int k3=0; k3<h_NUM_SIMS; k3++)
		      {
			pt_vecs[k][k2][k3] = h_parents_points[NUM_POINTS*h_NUM_INPUTS*k3 + NUM_POINTS*k + k2];
		      }
		  }
	      }
	
	    std::cout<<"\n\n1\n";
	    make_histogram(pt_vecs[0][0],num_bins);
	    // std::cout<<"\n\n2\n";
	    // make_histogram(pt_vecs[0][1],num_bins);
	    // std::cout<<"\n\n3\n";
	    // make_histogram(pt_vecs[0][2],num_bins);	
	
	    std::cin.get();
	  }
	h_warm_start = 1;	
      }
    std::vector<double> u;
    u.resize(h_NUM_INPUTS);
    u = best_points2u();

    return u;
  }
};
