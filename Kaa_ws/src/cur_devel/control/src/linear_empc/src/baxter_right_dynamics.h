
void tau( double* tau_out, double* parms, const double* q, const double* dq, const double* ddq );

void M( double* M_out, double* parms, double* q );

void c( double* c_out, double* parms, const double* q, const double* dq );

void C( double* C_out, double* parms, double* q, double* dq );

void g( double* g_out, double* parms, const double* q );

void get_parms(double * parms);

void regressor( double* regressor_out, const double* q, const double* dq, const double* ddq );

