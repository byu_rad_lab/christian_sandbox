#include "bellows_arm_dynamics.cpp"
#include "LinearEMPC.cu"
#include <Eigen/LU>
#include <ctime>
#include <fstream>

__device__ void my_cost_function(double * state, double * goal_state, double * u, double * goal_input, double * cost, int state_id, bool last_time_step)
{
  // If this is a position state
  if(state_id>=18)
    {
      *cost += 1.0*(*goal_state-*state)*(*goal_state-*state);
    }
  // If this is a velocity state
  else if(state_id>=12 and state_id<18)
    {
      *cost += 0.0*(*goal_state-*state)*(*goal_state-*state);
    }
  // If this is an input
  if(state_id<12)
    {
      *cost += .0000001*(*goal_input-u[state_id])*(*goal_input-u[state_id]);
    }
}




__device__ costfunc d_cost_function_pointer = my_cost_function;


int main(int argc, char * argv[])
{
  costfunc h_cost_function_pointer;
  cudaMemcpyFromSymbol(&h_cost_function_pointer, d_cost_function_pointer, sizeof(costfunc));

  const int num_states = 24;
  const int num_inputs = 12;

  std::vector<double> umin;
  std::vector<double> umax;
  std::vector<double> xmin;
  std::vector<double> xmax;
  double pmin = 8.0;
  double pmax = 400.0;
  for(int i=0; i<num_inputs; i++)
    {
      umin.push_back(pmin);
      umax.push_back(pmax);
      xmin.push_back(pmin);
      xmax.push_back(pmax);
    }
  for(int i=num_inputs; i<num_states; i++)
    {
      xmin.push_back(-9999999.0);
      xmax.push_back(9999999.0);
    }

  Eigen::MatrixXd x(num_states,1);
  x<<200,200,200,200,200,200,200,200,200,200,200,200, 0,0,0,0,0,0, 0,0,0,0,0,0;
  Eigen::MatrixXd xGoal(num_states,1);
  xGoal << 200,200,200,200,200,200,200,200,200,200,200,200, 0,0,0,0,0,0, 0.5,0.5,0,0,0,0;
  
  Eigen::MatrixXd u(num_inputs,1);
  u << 200,200,200,200,200,200,200,200,200,200,200,200;
  Eigen::MatrixXd uGoal(num_inputs,1);
  uGoal << 400,400,400,400,100,100,100,100,10,10,10,10;

  
  Eigen::MatrixXd A(num_states,num_states);
  Eigen::MatrixXd B(num_states,num_inputs);
  Eigen::MatrixXd w(num_states,1);
  std::vector<double> Avec;
  std::vector<double> Bvec;
  std::vector<double> wvec;
  Avec.resize(num_states*num_states);
  Bvec.resize(num_states*num_inputs);
  wvec.resize(num_states);
  
  LinearEMPC controller(num_states,
			num_inputs,
			h_cost_function_pointer,
			umin,
			umax,
			xmin,
			xmax);
		        // 100, //Horizon
			// 10, //NumParents
			// 50); //NumMates

  std::ofstream myfile;
  myfile.open("data.txt");

  double dt = .01;
  for(int i=0; i<1000; i++)
    {
      std::vector<double> xvec(x.data(), x.data() + x.size());
      std::vector<double> uvec(u.data(), u.data() + u.size());

      std::vector<double> q;
      std::vector<double> qd;
      for(int j=0; j<6; j++)
	{
	  qd.push_back(x(j+12));
	  q.push_back(x(j+18));
	}

      calc_discrete_A_B_w(q,qd,dt,&A,&B,&w);

      for(int j=0; j<num_states; j++)
	{
	  for(int k=0; k<num_states; k++)
	    {
	      Avec[j*num_states+k] = A(j,k);
	    }
	  for(int k=0; k<num_inputs; k++)
	    {
	      Bvec[j*num_inputs+k] = B(j,k);
	    }
	  wvec[j] = w(j);
	}

      std::vector<double> xGoalvec(xGoal.data(), xGoal.data() + xGoal.size());
      std::vector<double> uGoalvec(uGoal.data(), uGoal.data() + uGoal.size());
      
      double noise = 0.0*(x.block(12,0,6,1)-xGoal.block(12,0,6,1)).norm() + 30.0*(x.block(18,0,6,1)-xGoal.block(18,0,6,1)).norm();
      
      clock_t begin = clock();
      uvec = controller.run_empc(xvec,xGoalvec,uvec,uGoalvec,Avec,Bvec,wvec,noise);
      clock_t end = clock();

      u = Eigen::Map<Eigen::Matrix<double,num_inputs,1>>(uvec.data());

      Eigen::MatrixXd x_next(num_states,1);      
      x_next = A*x + B*u + w;
      x = x_next;


      // Display and record stuff
      std::cout<<"\nNoise: "<<noise<<std::endl;      
      double elapsed_time = double(end-begin)/CLOCKS_PER_SEC;
      std::cout<<"\nEMPC Solve Time: "<<elapsed_time;
      
      std::cout<<"\nu: "<<std::endl;
      for(int j=0; j<num_inputs; j++)
      	{
      	  std::cout<<u(j)<<"  ";
      	}
      
      std::cout<<"\nx: "<<std::endl;
      for(int j=18; j<num_states; j++)
      	{
      	  std::cout<<x(j)<<"  ";
      	}

      for(int j=0; j<num_states; j++)
	{
	  myfile<<x(j)<<", ";
	}
      for(int j=0; j<num_inputs; j++)
	{
	  myfile<<u(j)<<", ";
	}
      myfile<<i<<"\n";
      
    }

  myfile.close();
  return 0;
}
