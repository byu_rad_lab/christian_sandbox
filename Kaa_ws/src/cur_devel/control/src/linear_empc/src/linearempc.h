#ifndef LINEAREMPC_H
#define LINEAREMPC_H

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/device_ptr.h>
#include <curand.h>
#include <curand_kernel.h>
#include <math.h>

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

/* typedef void (*costfunc)(float*, float*, float*, float*, int); */
typedef void (*costfunc)();

typedef void (*calc_A_B_func)(thrust::host_vector<float> * states, thrust::host_vector<float> * A_mat, thrust::host_vector<float> * B_mat);

class LinearEMPC
{
public:

  LinearEMPC(int num_states,
	     int num_inputs,
	     void (&this_calc_A_and_B_matrices)(thrust::host_vector<float> * states, thrust::host_vector<float> * A_mat, thrust::host_vector<float> * B_mat),
	     /* calc_A_B_func this_calc_A_and_B_matrices, */
	     costfunc cost_fcn,
	     int horizon=25,
	     int num_parents=5,
	     int num_matings=1,
	     int num_parents_per_child=3,
	     int num_mutations=10,
	     float exploration_noise = 2.0);

  std::vector<float> best_points2u();
  std::vector<float> run_empc(std::vector<float> states,std::vector<float> goalStates,std::vector<float> uLast,std::vector<float> uSteadyState,bool warmStart=true);
		
  
  const int NUM_POINTS = 4;

  // These variables are going to be copied from CPU to GPU
  int h_NUM_STATES;
  int h_NUM_INPUTS;  
  int h_NUM_PARENTS; //This must be lower than blockdim_y*NUM_BLOCKS_INIT
  int h_NUM_PARENTS_PER_CHILD;
  int h_NUM_MATINGS;
  int h_NUM_MUTATIONS; //How many variants (mutations) of each child
  int h_NUM_SIMS;
  float h_exploration_noise;  
  
  thrust::host_vector<float> h_xinit;
  thrust::host_vector<float> h_xgoal;  
  thrust::host_vector<float> h_A_matrix;
  thrust::host_vector<float> h_B_matrix;
  thrust::host_vector<float> h_parents_points;
  thrust::host_vector<float> h_parents_costs;
  thrust::host_vector<float> h_u_last;
  thrust::host_vector<float> h_u_steady_state;  
  thrust::host_vector<float> h_out_points;
  thrust::host_vector<float> h_out_costs;
  thrust::host_vector<float> h_out_sim_costs;  
  thrust::host_vector<int> h_out_sim_cost_keys;

  int h_HORIZON;
  int h_warm_start;
  costfunc h_cost_fcn_ptr;
  int h_time[1];

  // These variables will be calculated from the above parameters
  int NUM_MATINGS;
  
  dim3 GridSize;
  dim3 BlockSize;
  int SHARED_MEMORY_BYTES;

  // device variables
  thrust::device_vector<float> d_xinit;
  thrust::device_vector<float> d_xgoal;
  thrust::device_vector<float> d_A_matrix;
  thrust::device_vector<float> d_B_matrix;
  thrust::device_vector<float> d_parents_points;
  thrust::device_vector<float> d_parents_costs;
  thrust::device_vector<float> d_u_last;
  thrust::device_vector<float> d_u_steady_state;  
  thrust::device_vector<float> d_out_points;
  thrust::device_vector<float> d_out_costs;
  thrust::device_vector<float> d_out_sim_costs;  
  thrust::device_vector<int> d_out_sim_cost_keys;

  int *d_NUM_STATES;  
  int *d_NUM_INPUTS;
  int *d_NUM_SIMS;
  int *d_NUM_PARENTS_PER_CHILD;
  int *d_warm_start;  
  int *d_HORIZON;
  int *d_time;
  float *d_exploration_noise;    

  // Function pointer to the calc_A_and_B_matrices function
  calc_A_B_func calc_A_and_B_matrices;

};

#endif
