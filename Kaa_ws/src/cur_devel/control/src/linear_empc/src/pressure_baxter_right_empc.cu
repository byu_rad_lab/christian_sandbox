#include "pressure_baxter_right_dynamics.cpp"
#include "LinearEMPC.cu"
#include <Eigen/LU>
#include <ctime>

#include<fstream>

void my_calc_A_and_B_matrices(thrust::host_vector<double> * states, thrust::host_vector<double> * A_mat, thrust::host_vector<double> * B_mat, thrust::host_vector<double> * disturbance)
{
  const int NUM_INPUTS = 14;
  const int NUM_STATES = 28;

  Eigen::MatrixXd A_d(NUM_STATES, NUM_STATES);
  Eigen::MatrixXd B_d(NUM_STATES, NUM_INPUTS);
  Eigen::MatrixXd w_d(NUM_STATES,1);
  double dt = .01;

  std::vector<double> q;
  std::vector<double> qd;
  for(int i=0; i<NUM_INPUTS; i++)
    {
      qd.push_back((*states)[14+i]);
      q.push_back((*states)[21+i]);
    }

  calc_discrete_A_B_w(q,qd,dt,&A_d,&B_d,&w_d);

  // Load A_d and B_d back into A_mat and B_mat
  for(int i=0; i<NUM_STATES; i++)
    {
      for(int j=0; j<NUM_STATES; j++)
  	{
  	  (*A_mat)[i*NUM_STATES+j] = A_d(i,j);
  	}
      for(int j=0; j<NUM_INPUTS; j++)
  	{
  	  (*B_mat)[i*NUM_INPUTS+j] = B_d(i,j);
  	}
      (*disturbance)[i] = w_d(i);
    }
}

__device__ void my_cost_function(double * state, double * goal_state, double * u, double * cost, int state_id, bool last_time_step)
{
  // If this is a position state
  if(state_id>=21)
    {
      *cost += 1.0*(*goal_state-*state)*(*goal_state-*state);
    }
  // If this is an input
  if(state_id<14)
    {
      *cost += .000001*(100-u[state_id])*(100-u[state_id]);
    }
  
  else
    {
      *cost += 0*(*goal_state-*state)*(*goal_state-*state);
    }
}

__device__ costfunc d_cost_function_pointer = my_cost_function;



int main(int argc, char * argv[])
{
  costfunc h_cost_function_pointer;

  cudaMemcpyFromSymbol(&h_cost_function_pointer, d_cost_function_pointer, sizeof(costfunc));

  const int num_states = 28;
  const int num_inputs = 14;

  std::vector<double> umin;
  std::vector<double> umax;
  std::vector<double> xmin;
  std::vector<double> xmax;
  double pmin = 8.0;
  double pmax = 300.0;
  for(int i=0; i<num_inputs; i++)
    {
      umin.push_back(pmin);
      umax.push_back(pmax);
      xmin.push_back(pmin);
      xmax.push_back(pmax);
    }
  for(int i=num_inputs; i<num_states; i++)
    {
      xmin.push_back(-9999999.0);
      xmax.push_back(9999999.0);
    }

  Eigen::MatrixXd x(num_states,1);
  Eigen::MatrixXd x_next(num_states,1);
  x << 200,200,200,200,200,200,200,200,200,200,200,200,200,200, 0,0,0,0,0,0,0, 0,0,0,0,0,0,0;
  Eigen::MatrixXd xGoal(num_states,1);
  xGoal << 200,200,200,200,200,200,200,200,200,200,200,200,200,200, 0,0,0,0,0,0,0, 1,1,1,1,1,1,1;
  Eigen::MatrixXd u(num_inputs,1);
  u << 200,200,200,200,200,200,200,200,200,200,200,200,200,200;
  Eigen::MatrixXd uSteadyState(num_inputs,1);
  uSteadyState << 200,200,200,200,200,200,200,200,200,200,200,200,200,200;

  Eigen::MatrixXd A(num_states,num_states);
  Eigen::MatrixXd B(num_states,num_inputs);
  Eigen::MatrixXd w(num_states,1);
  
  bool warmStart = false;
  double dt = .01;

  LinearEMPC controller(num_states,
			num_inputs,
			my_calc_A_and_B_matrices,
			h_cost_function_pointer,
			umin,
			umax,
			xmin,
			xmax,
		        100,
			20,
			50);

  std::ofstream myfile;
  myfile.open("data.txt");

  for(int i=0; i<1000; i++)
    {
      std::vector<double> xvec(x.data(), x.data() + x.size());
      std::vector<double> xGoalvec(xGoal.data(), xGoal.data() + xGoal.size());;
      std::vector<double> uvec(u.data(), u.data() + u.size());
      std::vector<double> uSteadyStatevec(uSteadyState.data(), uSteadyState.data() + uSteadyState.size());

      // Use this when constraining u(0)
      // double noise = 10.0*(x.block(14,0,7,1)-xGoal.block(14,0,7,1)).norm() + 30.0*(x.block(21,0,7,1)-xGoal.block(21,0,7,1)).norm();

      // Use this when not constraining u(0)
      double noise = 0.0*(x.block(0,0,14,1)-xGoal.block(0,0,14,1)).norm() + 0.0*(x.block(14,0,7,1)-xGoal.block(14,0,7,1)).norm() + 50.0*(x.block(21,0,7,1)-xGoal.block(21,0,7,1)).norm();
      
      std::cout<<"\nNoise: "<<noise<<std::endl;
      
      clock_t begin = clock();
      uvec = controller.run_empc(xvec,xGoalvec,uvec,uSteadyStatevec,warmStart,noise);
      clock_t end = clock();

      double elapsed_time = double(end-begin)/CLOCKS_PER_SEC;
      std::cout<<"\nEMPC Solve Time: "<<elapsed_time;
      
      warmStart = true;

      u = Eigen::Map<Eigen::Matrix<double,num_inputs,1>>(uvec.data());

      // for(int j=0; j<num_inputs; j++)
      // 	{
      // 	  u(j) = 200.0;
      // 	}

      std::vector<double> q;
      std::vector<double> qd;
      for(int j=0; j<7; j++)
	{
	  qd.push_back(x(j+14));
	  q.push_back(x(j+21));
	}

      calc_discrete_A_B_w(q,qd,dt,&A,&B,&w);
      
      x_next = A*x + B*u + w;
      // for(int j=0; j<14; j++)
      // 	{
      // 	  if(x_next(j)<x(j))
      // 	    {
      // 	      x_next(j) = x(j) + (x_next(j) - x(j))*10.0;
      // 	    }
      // 	}
      x = x_next;

      std::cout<<"\nu: "<<std::endl;
      for(int j=0; j<num_inputs; j++)
      	{
      	  std::cout<<u(j)<<"  ";
      	}
      
      std::cout<<"\nx: "<<std::endl;
      for(int j=21; j<num_states; j++)
      	{
      	  std::cout<<x(j)<<"  ";
      	}

      for(int j=0; j<num_states; j++)
	{
	  myfile<<x(j)<<", ";
	}
      for(int j=0; j<num_inputs; j++)
	{
	  myfile<<u(j)<<", ";
	}
      myfile<<i<<"\n";
      
    }

  myfile.close();
  return 0;
}
