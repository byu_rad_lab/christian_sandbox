#include "n3linkdynamics.h"

namespace py = pybind11;

py::array calc_M_py(std::vector<double> q,double l,double m,double I)
{
	 double out[9];

	double x0 = sin(q[0]);
	double x1 = pow(x0, 2);
	double x2 = cos(q[0]);
	double x3 = pow(x2, 2);
	double x4 = cos(q[1]);
	double x5 = x0*x4;
	double x6 = sin(q[1]);
	double x7 = x2*x6;
	double x8 = x5 + x7;
	double x9 = (1.0/2.0)*l;
	double x10 = -x5*x9 - x7*x9;
	double x11 = -l*x0 + x10;
	double x12 = x2*x4;
	double x13 = x0*x6;
	double x14 = x12 - x13;
	double x15 = -x12*x9 + x13*x9;
	double x16 = -l*x2 + x15;
	double x17 = x11*x8 + x14*x16;
	double x18 = -x5 - x7;
	double x19 = x11*x14 + x16*x18;
	double x20 = sin(q[2]);
	double x21 = q[0] + q[1];
	double x22 = cos(x21);
	double x23 = x20*x22;
	double x24 = cos(q[2]);
	double x25 = sin(x21);
	double x26 = x24*x25;
	double x27 = -x23 - x26;
	double x28 = x20*x25;
	double x29 = x22*x24;
	double x30 = x28*x9 - x29*x9;
	double x31 = -l*(x2 + x22) + x30;
	double x32 = -x28 + x29;
	double x33 = -x23*x9 - x26*x9;
	double x34 = l*(-x0 - x25) + x33;
	double x35 = x27*x31 + x32*x34;
	double x36 = x23 + x26;
	double x37 = x31*x32 + x34*x36;
	double x38 = I*pow(pow(x27, 2) + pow(x32, 2), 2);
	double x39 = I*pow(pow(x14, 2) + pow(x18, 2), 2) + x38;
	double x40 = x10*x8 + x14*x15;
	double x41 = x10*x14 + x15*x18;
	double x42 = -l*x25 + x33;
	double x43 = -l*x22 + x30;
	double x44 = x27*x43 + x32*x42;
	double x45 = m*x35;
	double x46 = x32*x43 + x36*x42;
	double x47 = m*x37;
	double x48 = m*x17*x40 + m*x19*x41 + x39 + x44*x45 + x46*x47;
	double x49 = x30*x32 + x33*x36;
	double x50 = x27*x30 + x32*x33;
	double x51 = x38 + x45*x50 + x47*x49;
	double x52 = m*x44*x50 + m*x46*x49 + x38;
	 out[0] = I*pow(x1 + x3, 2) + m*pow(x17, 2) + m*pow(x19, 2) + m*pow(x35, 2) + m*pow(x37, 2) + m*pow(-x1*x9 - x3*x9, 2) + x39;
	 out[1] = x48;
	 out[2] = x51;
	 out[3] = x48;
	 out[4] = m*pow(x40, 2) + m*pow(x41, 2) + m*pow(x44, 2) + m*pow(x46, 2) + x39;
	 out[5] = x52;
	 out[6] = x51;
	 out[7] = x52;
	 out[8] = m*pow(x49, 2) + m*pow(x50, 2) + x38;
	return py::array(9,out);

}

void calc_M(std::vector<double> q,Eigen::MatrixXd * M,double l,double m,double I)
{

	double x0 = sin(q[0]);
	double x1 = pow(x0, 2);
	double x2 = cos(q[0]);
	double x3 = pow(x2, 2);
	double x4 = cos(q[1]);
	double x5 = x0*x4;
	double x6 = sin(q[1]);
	double x7 = x2*x6;
	double x8 = x5 + x7;
	double x9 = (1.0/2.0)*l;
	double x10 = -x5*x9 - x7*x9;
	double x11 = -l*x0 + x10;
	double x12 = x2*x4;
	double x13 = x0*x6;
	double x14 = x12 - x13;
	double x15 = -x12*x9 + x13*x9;
	double x16 = -l*x2 + x15;
	double x17 = x11*x8 + x14*x16;
	double x18 = -x5 - x7;
	double x19 = x11*x14 + x16*x18;
	double x20 = sin(q[2]);
	double x21 = q[0] + q[1];
	double x22 = cos(x21);
	double x23 = x20*x22;
	double x24 = cos(q[2]);
	double x25 = sin(x21);
	double x26 = x24*x25;
	double x27 = -x23 - x26;
	double x28 = x20*x25;
	double x29 = x22*x24;
	double x30 = x28*x9 - x29*x9;
	double x31 = -l*(x2 + x22) + x30;
	double x32 = -x28 + x29;
	double x33 = -x23*x9 - x26*x9;
	double x34 = l*(-x0 - x25) + x33;
	double x35 = x27*x31 + x32*x34;
	double x36 = x23 + x26;
	double x37 = x31*x32 + x34*x36;
	double x38 = I*pow(pow(x27, 2) + pow(x32, 2), 2);
	double x39 = I*pow(pow(x14, 2) + pow(x18, 2), 2) + x38;
	double x40 = x10*x8 + x14*x15;
	double x41 = x10*x14 + x15*x18;
	double x42 = -l*x25 + x33;
	double x43 = -l*x22 + x30;
	double x44 = x27*x43 + x32*x42;
	double x45 = m*x35;
	double x46 = x32*x43 + x36*x42;
	double x47 = m*x37;
	double x48 = m*x17*x40 + m*x19*x41 + x39 + x44*x45 + x46*x47;
	double x49 = x30*x32 + x33*x36;
	double x50 = x27*x30 + x32*x33;
	double x51 = x38 + x45*x50 + x47*x49;
	double x52 = m*x44*x50 + m*x46*x49 + x38;
	(*M)(0,0) = I*pow(x1 + x3, 2) + m*pow(x17, 2) + m*pow(x19, 2) + m*pow(x35, 2) + m*pow(x37, 2) + m*pow(-x1*x9 - x3*x9, 2) + x39;
	(*M)(0,1) = x48;
	(*M)(0,2) = x51;
	(*M)(1,0) = x48;
	(*M)(1,1) = m*pow(x40, 2) + m*pow(x41, 2) + m*pow(x44, 2) + m*pow(x46, 2) + x39;
	(*M)(1,2) = x52;
	(*M)(2,0) = x51;
	(*M)(2,1) = x52;
	(*M)(2,2) = m*pow(x49, 2) + m*pow(x50, 2) + x38;
}

py::array calc_grav_py(std::vector<double> q,double l,double m,double g)
{
	 double out[3];

	double x0 = g*m;
	double x1 = sin(q[0]);
	double x2 = l*x1;
	double x3 = (1.0/2.0)*x2;
	double x4 = (1.0/2.0)*l;
	double x5 = -x3*cos(q[1]) - x4*sin(q[1])*cos(q[0]);
	double x6 = q[0] + q[1];
	double x7 = sin(x6);
	double x8 = l*x7;
	double x9 = -x4*sin(q[2])*cos(x6) - 1.0/2.0*x8*cos(q[2]);
	 out[0] = -x0*x3 + x0*(-x2 + x5) + x0*(l*(-x1 - x7) + x9);
	 out[1] = x0*x5 + x0*(-x8 + x9);
	 out[2] = x0*x9;
	return py::array(3,out);
}

void calc_grav(std::vector<double> q,Eigen::MatrixXd * grav,double l,double m,double g)
{

	double x0 = g*m;
	double x1 = sin(q[0]);
	double x2 = l*x1;
	double x3 = (1.0/2.0)*x2;
	double x4 = (1.0/2.0)*l;
	double x5 = -x3*cos(q[1]) - x4*sin(q[1])*cos(q[0]);
	double x6 = q[0] + q[1];
	double x7 = sin(x6);
	double x8 = l*x7;
	double x9 = -x4*sin(q[2])*cos(x6) - 1.0/2.0*x8*cos(q[2]);
	(*grav)(0,0) = -x0*x3 + x0*(-x2 + x5) + x0*(l*(-x1 - x7) + x9);
	(*grav)(1,0) = x0*x5 + x0*(-x8 + x9);
	(*grav)(2,0) = x0*x9;
}

py::array calc_C_py(std::vector<double> q,std::vector<double> qd,double l,double m,double I)
{
	 double out[9];

	double x0 = cos(q[2]);
	double x1 = q[0] + q[1];
	double x2 = cos(x1);
	double x3 = x0*x2;
	double x4 = sin(q[2]);
	double x5 = sin(x1);
	double x6 = x4*x5;
	double x7 = x3 - x6;
	double x8 = x2*x4;
	double x9 = x0*x5;
	double x10 = -x8 - x9;
	double x11 = 0.5*I;
	double x12 = x11*(pow(x10, 2) + pow(x7, 2))*(2*x10*(-2*x3 + 2*x6) + 2*x7*(-2*x8 - 2*x9));
	double x13 = sin(q[0]);
	double x14 = l*(-x13 - x5);
	double x15 = (1.0/2.0)*l;
	double x16 = x15*x8;
	double x17 = x15*x9;
	double x18 = -x16 - x17;
	double x19 = x14 + x18;
	double x20 = x19*x7;
	double x21 = cos(q[0]);
	double x22 = -x15*x3 + x15*x6;
	double x23 = -l*(x2 + x21) + x22;
	double x24 = x10*x23;
	double x25 = 2*x20 + 2*x24;
	double x26 = x16 + x17;
	double x27 = x26*x7;
	double x28 = x8 + x9;
	double x29 = x22*x28;
	double x30 = 2*x27 + 2*x29;
	double x31 = x19*x28 + x23*x7;
	double x32 = 0.5*m;
	double x33 = x31*x32;
	double x34 = x33*(x25 + x30);
	double x35 = -x3 + x6;
	double x36 = x23*x35;
	double x37 = x10*x19;
	double x38 = 2*x36 + 2*x37;
	double x39 = x22*x7;
	double x40 = x10*x26;
	double x41 = 2*x39 + 2*x40;
	double x42 = x20 + x24;
	double x43 = x32*x42;
	double x44 = x43*(x38 + x41);
	double x45 = x12 + x34 + x44;
	double x46 = cos(q[1]);
	double x47 = x21*x46;
	double x48 = sin(q[1]);
	double x49 = x13*x48;
	double x50 = x47 - x49;
	double x51 = l*x13;
	double x52 = x13*x46;
	double x53 = x15*x52;
	double x54 = x21*x48;
	double x55 = x15*x54;
	double x56 = -x53 - x55;
	double x57 = -x51 + x56;
	double x58 = x50*x57;
	double x59 = -x52 - x54;
	double x60 = -x15*x47 + x15*x49;
	double x61 = -l*x21 + x60;
	double x62 = x59*x61;
	double x63 = 2*x58 + 2*x62;
	double x64 = x53 + x55;
	double x65 = x50*x64;
	double x66 = x52 + x54;
	double x67 = x60*x66;
	double x68 = 2*x65 + 2*x67;
	double x69 = x50*x61;
	double x70 = x57*x66 + x69;
	double x71 = x32*x70;
	double x72 = x71*(x63 + x68);
	double x73 = -x47 + x49;
	double x74 = x61*x73;
	double x75 = x57*x59;
	double x76 = 2*x74 + 2*x75;
	double x77 = x50*x60;
	double x78 = x59*x64;
	double x79 = 2*x77 + 2*x78;
	double x80 = x58 + x62;
	double x81 = x32*x80;
	double x82 = x81*(x76 + x79);
	double x83 = -l*x2 + x22;
	double x84 = x7*x83;
	double x85 = l*x5;
	double x86 = x26 + x85;
	double x87 = x10*x86;
	double x88 = 2*x84 + 2*x87;
	double x89 = x43*(x38 + x88);
	double x90 = x7*x86;
	double x91 = x28*x83;
	double x92 = 2*x90 + 2*x91;
	double x93 = x33*(x25 + x92);
	double x94 = x11*(pow(x50, 2) + pow(x59, 2))*(2*x50*(-2*x52 - 2*x54) + 2*x59*(-2*x47 + 2*x49)) + x12;
	double x95 = x72 + x82 + x89 + x93 + x94;
	double x96 = x51 + x64;
	double x97 = x50*x96;
	double x98 = x61*x66;
	double x99 = x59*x96;
	double x100 = l*(-x2 - x21) + x22;
	double x101 = x100*x7;
	double x102 = -x14 + x26;
	double x103 = x10*x102;
	double x104 = x102*x7;
	double x105 = x100*x28;
	double x106 = x65 + x67;
	double x107 = x56*x66 + x77;
	double x108 = 1.0*m;
	double x109 = x107*x108;
	double x110 = x77 + x78;
	double x111 = x74 + x75;
	double x112 = x50*x56;
	double x113 = x59*x60;
	double x114 = x112 + x113;
	double x115 = x108*x114;
	double x116 = x90 + x91;
	double x117 = x116 + x42;
	double x118 = x18 - x85;
	double x119 = x118*x28 + x84;
	double x120 = x108*x119;
	double x121 = x84 + x87;
	double x122 = x36 + x37;
	double x123 = x121 + x122;
	double x124 = x118*x7;
	double x125 = x10*x83;
	double x126 = x124 + x125;
	double x127 = x108*x126;
	double x128 = x107*x32*(2*x112 + 2*x113 + x68);
	double x129 = x60*x73;
	double x130 = x56*x59;
	double x131 = x114*x32*(2*x129 + 2*x130 + x79);
	double x132 = 2*x124 + 2*x125;
	double x133 = x119*x32;
	double x134 = x133*(x132 + x92);
	double x135 = x35*x83;
	double x136 = x10*x118;
	double x137 = 2*x135 + 2*x136;
	double x138 = x126*x32;
	double x139 = x138*(x137 + x88);
	double x140 = x135 + x136;
	double x141 = x121 + x140;
	double x142 = x108*x42;
	double x143 = x116 + x126;
	double x144 = x108*x31;
	double x145 = x108*x70*(x106 + x114) + x108*x80*(x110 + x129 + x130) + x141*x142 + x143*x144 + x94;
	double x146 = x18*x28 + x39;
	double x147 = x146*x32;
	double x148 = x143*x147;
	double x149 = x18*x7;
	double x150 = x10*x22;
	double x151 = x149 + x150;
	double x152 = x151*x32;
	double x153 = x141*x152;
	double x154 = x27 + x29;
	double x155 = x151 + x154;
	double x156 = x133*x155;
	double x157 = x22*x35;
	double x158 = x10*x18;
	double x159 = x39 + x40;
	double x160 = x157 + x158 + x159;
	double x161 = x138*x160;
	double x162 = x140 + x159;
	double x163 = x162*x43;
	double x164 = x126 + x154;
	double x165 = x164*x33;
	double x166 = x154 + x42;
	double x167 = x133*x166;
	double x168 = x122 + x159;
	double x169 = x138*x168;
	double x170 = x12 + x163 + x165 + x167 + x169;
	double x171 = x117*x147;
	double x172 = x123*x152;
	double x173 = x160*x43;
	double x174 = x155*x33;
	double x175 = x171 + x172 + x173 + x174;
	double x176 = -x148 - x153 - x156 - x161 + x170 + x175;
	double x177 = x108*x146;
	double x178 = x108*x151;
	double x179 = x147*(2*x149 + 2*x150 + x30);
	double x180 = x152*(2*x157 + 2*x158 + x41);
	double x181 = x12 - x179 - x180;
	double x182 = x142*x160 + x144*x155;
	double x183 = x128 + x131 + x134 + x139 + x94;
	double x184 = qd[1]*x183;
	double x185 = x104 + x105 + x42;
	double x186 = x101 + x103 + x122;
	double x187 = x148 + x153 + x156 + x161;
	double x188 = x170 - x171 - x172 - x173 - x174 + x187;
	double x189 = x133*(x132 + x30);
	double x190 = x138*(x137 + x41);
	double x191 = x12 + x189 + x190;
	double x192 = x120*x155 + x127*x160;
	double x193 = x12 + x179 + x180;
	double x194 = qd[2]*x193;
	double x195 = x12 - x163 - x165 - x167 - x169 + x175 + x187;
	 out[0] = qd[0]*(x33*(2*x104 + 2*x105 + x25) + x43*(2*x101 + 2*x103 + x38) + x71*(x63 + 2*x97 + 2*x98) + x81*(2*x69 + x76 + 2*x99) + x94) + qd[1]*x95 + qd[2]*x45;
	 out[1] = qd[0]*x95 + qd[1]*(x109*(x106 + x80) + x115*(x110 + x111) + x117*x120 + x123*x127 - x128 - x131 - x134 - x139 + x145) + qd[2]*x176;
	 out[2] = qd[0]*x45 + qd[1]*x176 + qd[2]*(x166*x177 + x168*x178 + x181 + x182);
	 out[3] = qd[0]*(x109*(x80 + x97 + x98) + x115*(x111 + x69 + x99) + x120*x185 + x127*x186 + x145 - x72 - x82 - x89 - x93) + qd[2]*x188 + x184;
	 out[4] = qd[0]*x183 + qd[2]*x191 + x184;
	 out[5] = qd[0]*x188 + qd[1]*x191 + qd[2]*(x162*x178 + x164*x177 + x181 + x192);
	 out[6] = qd[0]*(x12 + x177*x185 + x178*x186 + x182 - x34 - x44) + qd[1]*x195 + x194;
	 out[7] = qd[0]*x195 + qd[1]*(x12 + x141*x178 + x143*x177 - x189 - x190 + x192) + x194;
	 out[8] = qd[0]*x193 + qd[1]*x193 + x194;
	return py::array(9,out);
}

void calc_C(std::vector<double> q,std::vector<double> qd,Eigen::MatrixXd * C,double l,double m,double I)
{

	double x0 = cos(q[2]);
	double x1 = q[0] + q[1];
	double x2 = cos(x1);
	double x3 = x0*x2;
	double x4 = sin(q[2]);
	double x5 = sin(x1);
	double x6 = x4*x5;
	double x7 = x3 - x6;
	double x8 = x2*x4;
	double x9 = x0*x5;
	double x10 = -x8 - x9;
	double x11 = 0.5*I;
	double x12 = x11*(pow(x10, 2) + pow(x7, 2))*(2*x10*(-2*x3 + 2*x6) + 2*x7*(-2*x8 - 2*x9));
	double x13 = sin(q[0]);
	double x14 = l*(-x13 - x5);
	double x15 = (1.0/2.0)*l;
	double x16 = x15*x8;
	double x17 = x15*x9;
	double x18 = -x16 - x17;
	double x19 = x14 + x18;
	double x20 = x19*x7;
	double x21 = cos(q[0]);
	double x22 = -x15*x3 + x15*x6;
	double x23 = -l*(x2 + x21) + x22;
	double x24 = x10*x23;
	double x25 = 2*x20 + 2*x24;
	double x26 = x16 + x17;
	double x27 = x26*x7;
	double x28 = x8 + x9;
	double x29 = x22*x28;
	double x30 = 2*x27 + 2*x29;
	double x31 = x19*x28 + x23*x7;
	double x32 = 0.5*m;
	double x33 = x31*x32;
	double x34 = x33*(x25 + x30);
	double x35 = -x3 + x6;
	double x36 = x23*x35;
	double x37 = x10*x19;
	double x38 = 2*x36 + 2*x37;
	double x39 = x22*x7;
	double x40 = x10*x26;
	double x41 = 2*x39 + 2*x40;
	double x42 = x20 + x24;
	double x43 = x32*x42;
	double x44 = x43*(x38 + x41);
	double x45 = x12 + x34 + x44;
	double x46 = cos(q[1]);
	double x47 = x21*x46;
	double x48 = sin(q[1]);
	double x49 = x13*x48;
	double x50 = x47 - x49;
	double x51 = l*x13;
	double x52 = x13*x46;
	double x53 = x15*x52;
	double x54 = x21*x48;
	double x55 = x15*x54;
	double x56 = -x53 - x55;
	double x57 = -x51 + x56;
	double x58 = x50*x57;
	double x59 = -x52 - x54;
	double x60 = -x15*x47 + x15*x49;
	double x61 = -l*x21 + x60;
	double x62 = x59*x61;
	double x63 = 2*x58 + 2*x62;
	double x64 = x53 + x55;
	double x65 = x50*x64;
	double x66 = x52 + x54;
	double x67 = x60*x66;
	double x68 = 2*x65 + 2*x67;
	double x69 = x50*x61;
	double x70 = x57*x66 + x69;
	double x71 = x32*x70;
	double x72 = x71*(x63 + x68);
	double x73 = -x47 + x49;
	double x74 = x61*x73;
	double x75 = x57*x59;
	double x76 = 2*x74 + 2*x75;
	double x77 = x50*x60;
	double x78 = x59*x64;
	double x79 = 2*x77 + 2*x78;
	double x80 = x58 + x62;
	double x81 = x32*x80;
	double x82 = x81*(x76 + x79);
	double x83 = -l*x2 + x22;
	double x84 = x7*x83;
	double x85 = l*x5;
	double x86 = x26 + x85;
	double x87 = x10*x86;
	double x88 = 2*x84 + 2*x87;
	double x89 = x43*(x38 + x88);
	double x90 = x7*x86;
	double x91 = x28*x83;
	double x92 = 2*x90 + 2*x91;
	double x93 = x33*(x25 + x92);
	double x94 = x11*(pow(x50, 2) + pow(x59, 2))*(2*x50*(-2*x52 - 2*x54) + 2*x59*(-2*x47 + 2*x49)) + x12;
	double x95 = x72 + x82 + x89 + x93 + x94;
	double x96 = x51 + x64;
	double x97 = x50*x96;
	double x98 = x61*x66;
	double x99 = x59*x96;
	double x100 = l*(-x2 - x21) + x22;
	double x101 = x100*x7;
	double x102 = -x14 + x26;
	double x103 = x10*x102;
	double x104 = x102*x7;
	double x105 = x100*x28;
	double x106 = x65 + x67;
	double x107 = x56*x66 + x77;
	double x108 = 1.0*m;
	double x109 = x107*x108;
	double x110 = x77 + x78;
	double x111 = x74 + x75;
	double x112 = x50*x56;
	double x113 = x59*x60;
	double x114 = x112 + x113;
	double x115 = x108*x114;
	double x116 = x90 + x91;
	double x117 = x116 + x42;
	double x118 = x18 - x85;
	double x119 = x118*x28 + x84;
	double x120 = x108*x119;
	double x121 = x84 + x87;
	double x122 = x36 + x37;
	double x123 = x121 + x122;
	double x124 = x118*x7;
	double x125 = x10*x83;
	double x126 = x124 + x125;
	double x127 = x108*x126;
	double x128 = x107*x32*(2*x112 + 2*x113 + x68);
	double x129 = x60*x73;
	double x130 = x56*x59;
	double x131 = x114*x32*(2*x129 + 2*x130 + x79);
	double x132 = 2*x124 + 2*x125;
	double x133 = x119*x32;
	double x134 = x133*(x132 + x92);
	double x135 = x35*x83;
	double x136 = x10*x118;
	double x137 = 2*x135 + 2*x136;
	double x138 = x126*x32;
	double x139 = x138*(x137 + x88);
	double x140 = x135 + x136;
	double x141 = x121 + x140;
	double x142 = x108*x42;
	double x143 = x116 + x126;
	double x144 = x108*x31;
	double x145 = x108*x70*(x106 + x114) + x108*x80*(x110 + x129 + x130) + x141*x142 + x143*x144 + x94;
	double x146 = x18*x28 + x39;
	double x147 = x146*x32;
	double x148 = x143*x147;
	double x149 = x18*x7;
	double x150 = x10*x22;
	double x151 = x149 + x150;
	double x152 = x151*x32;
	double x153 = x141*x152;
	double x154 = x27 + x29;
	double x155 = x151 + x154;
	double x156 = x133*x155;
	double x157 = x22*x35;
	double x158 = x10*x18;
	double x159 = x39 + x40;
	double x160 = x157 + x158 + x159;
	double x161 = x138*x160;
	double x162 = x140 + x159;
	double x163 = x162*x43;
	double x164 = x126 + x154;
	double x165 = x164*x33;
	double x166 = x154 + x42;
	double x167 = x133*x166;
	double x168 = x122 + x159;
	double x169 = x138*x168;
	double x170 = x12 + x163 + x165 + x167 + x169;
	double x171 = x117*x147;
	double x172 = x123*x152;
	double x173 = x160*x43;
	double x174 = x155*x33;
	double x175 = x171 + x172 + x173 + x174;
	double x176 = -x148 - x153 - x156 - x161 + x170 + x175;
	double x177 = x108*x146;
	double x178 = x108*x151;
	double x179 = x147*(2*x149 + 2*x150 + x30);
	double x180 = x152*(2*x157 + 2*x158 + x41);
	double x181 = x12 - x179 - x180;
	double x182 = x142*x160 + x144*x155;
	double x183 = x128 + x131 + x134 + x139 + x94;
	double x184 = qd[1]*x183;
	double x185 = x104 + x105 + x42;
	double x186 = x101 + x103 + x122;
	double x187 = x148 + x153 + x156 + x161;
	double x188 = x170 - x171 - x172 - x173 - x174 + x187;
	double x189 = x133*(x132 + x30);
	double x190 = x138*(x137 + x41);
	double x191 = x12 + x189 + x190;
	double x192 = x120*x155 + x127*x160;
	double x193 = x12 + x179 + x180;
	double x194 = qd[2]*x193;
	double x195 = x12 - x163 - x165 - x167 - x169 + x175 + x187;
	(*C)(0,0) = qd[0]*(x33*(2*x104 + 2*x105 + x25) + x43*(2*x101 + 2*x103 + x38) + x71*(x63 + 2*x97 + 2*x98) + x81*(2*x69 + x76 + 2*x99) + x94) + qd[1]*x95 + qd[2]*x45;
	(*C)(0,1) = qd[0]*x95 + qd[1]*(x109*(x106 + x80) + x115*(x110 + x111) + x117*x120 + x123*x127 - x128 - x131 - x134 - x139 + x145) + qd[2]*x176;
	(*C)(0,2) = qd[0]*x45 + qd[1]*x176 + qd[2]*(x166*x177 + x168*x178 + x181 + x182);
	(*C)(1,0) = qd[0]*(x109*(x80 + x97 + x98) + x115*(x111 + x69 + x99) + x120*x185 + x127*x186 + x145 - x72 - x82 - x89 - x93) + qd[2]*x188 + x184;
	(*C)(1,1) = qd[0]*x183 + qd[2]*x191 + x184;
	(*C)(1,2) = qd[0]*x188 + qd[1]*x191 + qd[2]*(x162*x178 + x164*x177 + x181 + x192);
	(*C)(2,0) = qd[0]*(x12 + x177*x185 + x178*x186 + x182 - x34 - x44) + qd[1]*x195 + x194;
	(*C)(2,1) = qd[0]*x195 + qd[1]*(x12 + x141*x178 + x143*x177 - x189 - x190 + x192) + x194;
	(*C)(2,2) = qd[0]*x193 + qd[1]*x193 + x194;
}

py::array fkCoM0_py(std::vector<double> q,double l)
{
	 double out[16];

	double x0 = cos(q[0]);
	double x1 = sin(q[0]);
	double x2 = (1.0/2.0)*l;
	 out[0] = x0;
	 out[1] = -x1;
	 out[2] = 0;
	 out[3] = -x1*x2;
	 out[4] = x1;
	 out[5] = x0;
	 out[6] = 0;
	 out[7] = x0*x2;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = 0;
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 1;
	return py::array(16,out);

}

py::array fkCoM1_py(std::vector<double> q,double l)
{
	 double out[16];

	double x0 = cos(q[0]);
	double x1 = cos(q[1]);
	double x2 = x0*x1;
	double x3 = sin(q[0]);
	double x4 = sin(q[1]);
	double x5 = x3*x4;
	double x6 = x2 - x5;
	double x7 = x1*x3;
	double x8 = x0*x4;
	double x9 = (1.0/2.0)*l;
	 out[0] = x6;
	 out[1] = -x7 - x8;
	 out[2] = 0;
	 out[3] = -l*x3 - x7*x9 - x8*x9;
	 out[4] = x7 + x8;
	 out[5] = x6;
	 out[6] = 0;
	 out[7] = l*x0 + x2*x9 - x5*x9;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = 0;
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 1;
	return py::array(16,out);

}

py::array fkCoM2_py(std::vector<double> q,double l)
{
	 double out[16];

	double x0 = cos(q[2]);
	double x1 = q[0] + q[1];
	double x2 = cos(x1);
	double x3 = x0*x2;
	double x4 = sin(q[2]);
	double x5 = sin(x1);
	double x6 = x4*x5;
	double x7 = x3 - x6;
	double x8 = x2*x4;
	double x9 = x0*x5;
	double x10 = (1.0/2.0)*l;
	 out[0] = x7;
	 out[1] = -x8 - x9;
	 out[2] = 0;
	 out[3] = -l*(x5 + sin(q[0])) - x10*x8 - x10*x9;
	 out[4] = x8 + x9;
	 out[5] = x7;
	 out[6] = 0;
	 out[7] = l*(x2 + cos(q[0])) + x10*x3 - x10*x6;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = 0;
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 1;
	return py::array(16,out);

}

void calc_A_B_w(std::vector<double> q,std::vector<double> qd,Eigen::MatrixXd * A, Eigen::MatrixXd * B, Eigen::MatrixXd * w,double l=1.0,double m=1.0,double I=.1,double gravity=9.81,double damping=.01)
{
  int n = 3;
  Eigen::MatrixXd M(n,n);
  Eigen::MatrixXd C(n,n);
  Eigen::MatrixXd F(n,n);
  F.setIdentity();
  F = F*damping;
  Eigen::MatrixXd g(n,1);

  calc_M(q,&M,l,m,I);
  calc_C(q,qd,&C,l,m,I);
  calc_grav(q,&g,l,m,gravity);

  Eigen::MatrixXd M_inv;
  M_inv = M.inverse();

  (*A).topLeftCorner(n,n) = -M_inv*(C+F);
  (*A).topRightCorner(n,n).setZero();
  (*A).bottomLeftCorner(n,n).setIdentity();
  (*A).bottomRightCorner(n,n).setZero();

  for(int i=0; i<M_inv.rows(); i++)
    {
      for(int j=0; j<M_inv.cols(); j++)
	{
	  (*B)(i,j) = M_inv(i,j);
	}
    }
  for(int i=n; i<2*n; i++)
    {
      for(int j=0; j<n; j++)
	{
	  (*B)(i,j) = 0.0;
	}
    }

  Eigen::MatrixXd b(n,1);
  b = M_inv*(-g);  
  for(int i=0; i<g.rows(); i++)
    {
      (*w)(i) = b(i);
    }
  for(int i=g.rows(); i<2*n; i++)
    {
      (*w)(i) = 0;
    }
  
}

void calc_discrete_A_B_w(std::vector<double> q,std::vector<double> qd, double dt, Eigen::MatrixXd * A_d, Eigen::MatrixXd * B_d, Eigen::MatrixXd * w_d,double l=1.0,double m=1.0,double I=.1,double gravity=9.81,double damping=.01)
{
  int n = 3;
  Eigen::MatrixXd A(2*n,2*n);
  Eigen::MatrixXd B(2*n,n);
  Eigen::MatrixXd w(2*n,1);
  calc_A_B_w(q,qd,&A,&B,&w,l,m,I,gravity,damping);
  
  Eigen::MatrixXd eye(A.rows(),A.cols());
  eye.setIdentity();
  *A_d = (eye - A*dt).inverse();
  *B_d = (eye - A*dt).inverse()*B*dt;
  *w_d = (eye - A*dt).inverse()*w*dt;
}

py::array calc_discrete_A_B_w_py(std::vector<double> q,std::vector<double> qd, double dt,double l=1.0,double m=1.0,double I=.1,double gravity=9.81,double damping=.01)
{
  int n = 3;
  int num_states = n*2;
  int num_inputs = n;
  double out[num_states*num_states + num_states*num_inputs + num_states];
  
  Eigen::MatrixXd A(num_states,num_states);
  Eigen::MatrixXd B(num_states,num_inputs);
  Eigen::MatrixXd w(num_states,1);
  Eigen::MatrixXd A_d(num_states,num_states);
  Eigen::MatrixXd B_d(num_states,num_inputs);
  Eigen::MatrixXd w_d(num_states,1);

  calc_A_B_w(q,qd,&A,&B,&w,l,m,I,gravity,damping);
  
  Eigen::MatrixXd eye(A.rows(),A.cols());
  eye.setIdentity();
  A_d = (eye - A*dt).inverse();
  B_d = (eye - A*dt).inverse()*B*dt;

  w_d = (eye - A*dt).inverse()*w*dt;

  for(int i=0; i<num_states; i++)
    {
      for(int j=0; j<num_states; j++)
	{
	  out[i*num_states+j] = A_d(i,j);
	}
      for(int j=0; j<num_inputs; j++)
	{
	  out[num_states*num_states + i*num_inputs+j] = B_d(i,j);
	}
      out[num_states*num_states + num_states*num_inputs + i] = w_d(i);
    }
  return py::array(num_states*num_states + num_states*num_inputs + num_states,out);
}


PYBIND11_MODULE(n3linkdynamics,m) {
  m.def("calc_discrete_A_B_w_py",&calc_discrete_A_B_w_py,"a function description");
  m.def("calc_M",&calc_M_py,"a function description");
  m.def("calc_grav",&calc_grav_py,"another function description");
  m.def("calc_C",&calc_C_py,"yet another function description");
  m.def("fkCoM0",&fkCoM0_py,"a function description");
  m.def("fkCoM1",&fkCoM1_py,"a function description");
  m.def("fkCoM2",&fkCoM2_py,"a function description");
}
