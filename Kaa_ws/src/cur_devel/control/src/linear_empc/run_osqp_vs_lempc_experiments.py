import os
import numpy as np

num_trials = 1000
max_links = 13

for trial in range(1010,1000+num_trials):
    
    for n in range(1,max_links+1):
    # for n in xrange(1,5):        
        print("Trial: ",trial,"  Number of links: ",n)

        x0 = np.random.rand(n,1)*np.pi*2 - np.pi
        xgoal = np.random.rand(n,1)*np.pi*2 - np.pi
        
        x0_xgoal_string = ''
        xgoal_string = ''
        for i in range(0,n):
            x0_xgoal_string += str(x0[i,0])+'  '
        for i in range(0,n):
            x0_xgoal_string += str(xgoal[i,0])+'  '
            xgoal_string += str(xgoal[i,0])+'  '

        # Run LEMPC and record the trajectory, solve times, and xgoal
        os.system('rosrun linear_empc n'+str(n)+'link_empc '+x0_xgoal_string)

        os.system('python csv2mat.py '+str(n)+' '+str(trial)+' '+xgoal_string)
        
        # Run OSQPMPC and record the trajectory, solve times, and xgoal
        os.system('python ../../src/osqp_mpc/examples/nlinkMPC.py '+str(n)+' '+str(trial)+' '+x0_xgoal_string)
