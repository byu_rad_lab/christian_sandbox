import numpy as np
import time
import matplotlib.pyplot as plt

class GPU_EMPC():

    def __init__(self, model, cost_function, numStates, numInputs, xmin, xmax, umin, umax,
                 horizon = 50, numSims = 500, numKnotPoints=2, dt=0.01, elitism=False):

        # form: x_t+dt = model(x_t,u,dt=0.01)
        self.model = model
        
        # form: cost = cost_function(x, u, xgoal, ugoal, final_timestep=False) for a single timestep
        self.cost_function = cost_function
        
        self.numStates = numStates
        self.numInputs = numInputs
        self.xmin = np.array(xmin).reshape(numStates,1)
        self.xmax = np.array(xmax).reshape(numStates,1)
        self.umin = np.array(umin).reshape(numInputs,1)
        self.umax = np.array(umax).reshape(numInputs,1)
        self.uRange = (np.array(umax)-np.array(umin)).reshape(numInputs,1)
        self.horizon = horizon
        self.numSims = numSims
        self.dt = dt
        self.numKnotPoints = numKnotPoints
        self.segmentLength = float(self.horizon)/(self.numKnotPoints-1)
        self.U = np.zeros([self.numSims,self.numInputs,self.numKnotPoints])
        self.costs = np.zeros([self.numSims])
        self.x_traj = np.zeros([self.numSims, self.numStates, self.horizon]) # for updating costs after shift
        self.cost_traj = np.zeros([self.numSims, self.horizon]) # records the cost accrued at each timestep
        
        
    def initialize_U(self):
        diff = self.umax - self.umin
        diff = np.tile(diff[None,:,None], (self.numSims, 1, self.numKnotPoints))
        rands = diff*np.random.random([self.numSims,self.numInputs,self.numKnotPoints]) - diff/2.0
        self.U = rands.squeeze(0)
        # print(self.U.shape)

    def random_u(self, umin, umax):
        diff = (umax - umin).reshape([self.numInputs, 1])
        diff = np.tile(diff[:,None], (1, self.numKnotPoints))
        rand = diff*np.random.random([1,self.numKnotPoints]) - diff/2.0
        return rand.squeeze(0)

    def assign_costs(self, x_in, xgoal, ugoal):
        """Assigns costs to each input trajectory in the population"""
        
        for member in range(self.numSims):
            total_cost = 0
            u = self.get_u(member)
            x = x_in

            for step in range(self.horizon-1):
                # print(u)
                seg = int(step/self.segmentLength)
                low = u[:,seg]
                high = u[:,seg+1]
                diff = high - low
                input = low + diff*(step % self.segmentLength)/float(self.segmentLength)
               
                step_cost = self.cost_function(x,input,xgoal,ugoal,final_timestep=False)
                # print(seg)
                # print(input)
                self.x_traj[member, :, step] = x.squeeze()
                self.cost_traj[member,step] = step_cost
                
                x = self.model(x,input,self.dt)

            step += 1
            seg = int(step/self.segmentLength)
            low = u[:,seg]
            high = u[:,seg+1]
            diff = high - low
            input = low + diff*(step % self.segmentLength)/float(self.segmentLength)
            # print(seg)
            # print(input)
            
            step_cost = self.cost_function(x,input,xgoal,ugoal,final_timestep=True)
            self.x_traj[member, :, step] = x.squeeze()
            self.cost_traj[member,step] = step_cost
            
            
            
        self.costs = np.sum(self.cost_traj,axis=1) 


    def new_generation(self, num_select=2, selection='tournament',
                       elitism='false', mating='single_point', mutation='gaussian',
                       strangers='true', mut_prob=0.3):
        """Advances the population by one generation, assumes assign_costs() has already been called"""

        # self.shift_U()

        best = self.get_best_u()
        
        num_matings = self.numSims / num_select
        # print(num_matings)
        last_gen_kids = self.numSims % num_select
        # print(last_gen_kids)
        pos = 0
        U_new = np.zeros(self.U.shape)
        # print(num_matings)
        
        for i in range(num_matings):
            parents = self.select(num_select = num_select, method = selection)
            children = self.mate(parents, method = mating)
            # need to stuff children into U, and check for elitism(TODO)
            for num in range(num_select):
                U_new[num_select*i+num,:,:] = children[num]
                # print(U_new)

        # If there's any
        if last_gen_kids > 0:
            parents = self.select(num_select = num_select, method = selection)
            children = self.mate(parents, method=mating)
            for num in range(last_gen_kids):
                U_new[num_select*(i+1)+num,:,:] = children[num]
                # print(U_new)

        # print(U_new)
        self.U = U_new
        
        self.mutate(method=mutation,strangers=strangers, probability=mut_prob)
        # self.U[-1,:,:] = best
    

    def select(self, num_select=2, method='tournament'):

        parents = []
        
        if method == 'roulette':
            print('Roulette selection not yet implemented')

            
        elif method == 'tournament':
            for parent in range(num_select):
                n_accepted = 0
                accepted = []

                # tournament size can be adjusted to raise/lower fitness-based pressure            
                tournament_size = min(self.numSims, 15)
                p_best = 0.8
                a_costs = np.zeros(tournament_size)
                
                while n_accepted < tournament_size:
                    num = np.random.randint(self.numSims)
                    if num not in accepted:
                        accepted.append(num)
                        a_costs[n_accepted] = self.costs[num]
                        n_accepted += 1

                # select a parent - tournament style
                winner = False

                while not(winner):
                    pos = np.argmin(a_costs)
                    rand = np.random.random(1)
                    if rand > p_best:
                        a_costs[pos] = np.inf
                    else:
                        winner = True

                parents.append(self.get_u(pos))

                
                
        elif method == 'e_greedy':
            print('E_greedy selection not yet implemented')

            
        elif method == 'stochastic_acceptance':
            n_accepted = 0
            c_min = min(self.costs)
            
            while n_accepted < num_select:
                # select random candidate
                num = np.random.randint(self.numSims)
                # accept with probability
                c = self.costs[num]
                prob = c_min/c
                if np.random.random() < prob:
                    parents.append(self.get_u(num))
                    n_accepted += 1

        else:
            print('Please specify a valid selection method')

        return parents
            

    def mate(self, parents, method='single_point'):

        n_parents = len(parents)
        
        if method == 'single_point':
            if n_parents != 2:
                print("Single point crossover only works for 2 parents")
                
            u_1 = parents[0]
            u_2 = parents[1]
            horizon = self.numKnotPoints
            cross = np.random.randint(0, horizon)

            child_1 = np.concatenate((u_1[:,list(range(cross))],u_2[:,list(range(cross,horizon))]),axis=1)
            child_2 = np.concatenate((u_2[:,list(range(cross))],u_1[:,list(range(cross,horizon))]),axis=1)
            children = [child_1, child_2]

        elif method == 'multi_point':
            print('Multi_point crossover not yet implemented')

        elif method == 'blend':
            print('Blend crossover not yet implemented')

        elif method == 'extrapolate':
            print('Extrapolation not yet implemented')

        elif method == 'coin_flip':
            prob = 1.0/n_parents

            # shape = (n_parents,) + parents[0].shape

            children = []
            
            for child in range(n_parents):
                random = np.random.random(parents[0].shape)
                child_val = np.zeros(parents[0].shape)
                
                for parent in range(n_parents):
                    low = parent*prob
                    high = (parent + 1)*prob

                    mask = (random > low) & (random < high)
                    child_val += parents[parent]*mask

                children.append(child_val)
                

        elif method == 'proportional_inheritance':
            print('Proportional inheritance not yet implemented')

        else:
            print('Invalid mating method')

        return children
    

    def mutate(self, probability=0.2, method='gaussian', strangers='true'):

        if method == 'random_gene':
            probs = np.random.random(self.U.shape)
            mask = probs < probability

            rands = np.zeros(self.U.shape)

            for i in range(self.numInputs):
                diff = self.umax[i] - self.umin[i]
                values = diff*np.random.random([self.numSims,1,self.numKnotPoints]) - diff/2.0
                rands[:,i,:] = values.squeeze()
                
            self.U[mask] = rands[mask]

        elif method == 'gaussian':
            st_dev = (self.umax - self.umin)/4.0
            
            modifier = np.random.normal(0, st_dev, self.U.shape)

            probs = np.random.random(self.U.shape)
            mask = probs < probability
            
            proposed = self.U + modifier*mask

            too_high = (proposed > self.umax)
            proposed[too_high] = (self.umax * too_high)[too_high]
            
            too_low = (proposed < self.umin)
            proposed[too_low] = (self.umin * too_low)[too_low]

            
        elif method == 'invert':
            print('Inversion mutation not yet implemented')

        else:
            print('Invalid mutation method, no mutation performed')


        if strangers == True:
            n_strangers = 1
            for i in range(n_strangers):
                pos = np.random.randint(self.numSims)
                stranger = self.random_u(self.umin, self.umax)
                self.U[pos,:,:] = stranger
            


    def shift_U(self):
        """Pulls off the first input in the time series of each member of the population.
        Adds a new randomized last point of the trajectory. Also augments the cost of each
        trajectory to match the new points. Doesn't work unless you fully reevaluate costs after 
        shifting"""

        # Subtract 1st and last step costs (depends on Qf, which is bad)
        mod_costs = self.costs - self.cost_traj[:,0] - self.cost_traj[:,-1]

        # Shift all trajectories forward in time
        self.U = np.roll(self.U, -1, axis=2)
        self.x_traj = np.roll(self.x_traj, -1, axis=2)
        self.cost_traj = np.roll(self.cost_traj, -1, axis=1)

        # Add a new random last step
        for i in range(self.numInputs):
            diff = self.umax[i] - self.umin[i]
            rand = diff*np.random.random() - diff/2.0
            self.U[:,i,-1] = rand.squeeze()

        # Add on cost of last two steps
        # Get xgoal (TODO)
        # get ugoal (TODO)
        # This is all bogus, remove it (TODO)
        
        for sim in range(self.numSims):         

            u = get_u(sim)
            x = x_traj[sim,:,-2]
            # print(u)      
            step_cost = self.cost_function(x,u[:,-2],xgoal,ugoal,final_timestep=False)
            self.x_traj[member, :, i] = x
            self.cost_traj[member,i] = step_cost
                
            x = self.model(x,u[:,i],self.dt)

            self.x_traj[member,:,-1] = x
            step_cost = self.cost_function(x,u[:,-1],xgoal,ugoal,final_timestep=True)
            self.cost_traj[member,-1] = step_cost
            
            
        self.costs = np.sum(self.cost_traj,axis=1)

    def get_u(self, pos):
        u = self.U[pos,:,:]
        return u


    def get_best_u(self):

        pos = np.argmin(self.costs)
        u = self.get_u(pos)

        return u
    

if __name__=='__main__':

    from rad_models.InvertedPendulum import *
    # from rad_models.LearnedInvertedPendulum import *
    import numpy as np
    import time
    
    dt = .050
    
    sys = InvertedPendulum(uMax=0.5)
    # sys = LearnedInvertedPendulum(use_gpu=False)
    # sys = InvertedPendulum(uMax=2.0)
    numStates = sys.numStates
    numInputs = sys.numInputs
    
    Q = 1.0*np.diag([0,1.0])
    Qf = 0.0*np.diag([0,1.0])
    R = .0*np.diag([1.0])

    def myCostFcn(x,u,xgoal,ugoal,final_timestep=False):
        cost = np.zeros(u.shape)
        if final_timestep:
            Qx = np.abs(Qf.dot(x-xgoal)**2.0)
            cost = np.sum(Qx,axis=0)
        else:
            Qx = np.abs(Q.dot(x-xgoal)**2.0)
            Ru = np.abs(R.dot(u-ugoal))
            cost = np.sum(Qx,axis=0) + np.sum(Ru,axis=0)
        return cost

    numKnotPoints = 4
    horizon = 50
    simLength = 300
    numSims = 100
    gen_per_action = 1
    

    mpc = CPU_EMPC(sys.forward_simulate_dt,
                   myCostFcn,
                   numStates,
                   numInputs,
                   xmin=sys.xMin,
                   xmax=sys.xMax,
                   umin=[-sys.uMax],
                   umax=[sys.uMax],
                   horizon=horizon,
                   numSims=numSims,
                   numKnotPoints=numKnotPoints,
                   dt=dt,
                   elitism=False)    

    x = np.array([0,-np.pi]).reshape(sys.numStates,1)
    u = np.zeros([sys.numInputs,1])
    xgoal = np.zeros([sys.numStates,1])
    ugoal = np.zeros([sys.numInputs,1])

    

    fig = plt.figure(10)
    plt.ion()

    x_hist = np.zeros([sys.numStates,simLength])
    x_goal_hist = np.zeros([sys.numStates,simLength])
    u_hist = np.zeros([sys.numInputs,simLength])
    cost_hist = np.zeros([simLength])

    mut_prob = 0.3
    proportional_mutate = False
    mut_prob_min = 0.1
    mut_range = mut_prob-mut_prob_min
    err_0 = np.sum((xgoal - x)**2)
    
    mpc.initialize_U()
    mpc.assign_costs(x, xgoal, ugoal)

    actual_cost = 0

    # print(sys.uMax)
    # raw_input("Enter")
    
    for i in range(0,simLength):
        [Ad,Bd,wd] = sys.calc_discrete_A_B_w(x,u,dt)

        for n in range(gen_per_action):

            # print(mpc.U)

            if proportional_mutate == True:
                error = np.sum((xgoal - x)**2)
                mut_prob = error/err_0*mut_range + mut_range
            
            mpc.new_generation(num_select=3, selection='stochastic_acceptance',
                               mating='coin_flip', mutation='random_gene', strangers=True,
                               mut_prob = mut_prob)
            
            mpc.assign_costs(x,xgoal,ugoal)
            best_cost = np.min(mpc.costs)
            print(best_cost)

        u = mpc.get_best_u()

        u = u.flatten()[0]
        
        x = sys.forward_simulate_dt(x,u,dt)

        step_cost = mpc.cost_function(x, u, xgoal, ugoal)
        actual_cost += step_cost

        plt.figure(10)
        sys.visualize(x, u)
        
        u_hist[:,i] = u.flatten()
        x_hist[:,i] = x.flatten()
        x_goal_hist[:,i] = xgoal.flatten()
        cost_hist[i] = best_cost

    print(actual_cost)
        
    plt.figure(4)
    plt.plot(x_hist[1,:].T)
    plt.plot(x_goal_hist[1,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Theta (rad)')
    plt.title('Position')
    plt.show()
        
    plt.figure(5)
    plt.plot(u_hist.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Torque (Nm)')
    plt.title('Input')

    plt.figure(6)
    plt.plot(cost_hist.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Cost')
    plt.title('Cost')

    plt.pause(1000)
