import numpy as np
import time
import matplotlib.pyplot as plt
from cpu_empc.CPU_EMPC import *
from rad_models.InvertedPendulum import *
# from rad_models.LearnedInvertedPendulum import *
import numpy as np
import time

dt = .050

sys = InvertedPendulum(uMax=0.5)
# sys = LearnedInvertedPendulum(use_gpu=False)
# sys = InvertedPendulum(uMax=2.0)
numStates = sys.numStates
numInputs = sys.numInputs

Q = 1.0*np.diag([0,1.0])
Qf = 0.0*np.diag([0,1.0])
R = .0*np.diag([1.0])

def myCostFcn(x,u,xgoal,ugoal,final_timestep=False):
    cost = np.zeros(u.shape)
    if final_timestep:
        x_wrap_up = x + np.array([0, 2*np.pi]).reshape(x.shape)
        x_wrap_down = x + np.array([0, -2*np.pi]).reshape(x.shape)
        Qx = np.abs(Qf.dot(x-xgoal)**2.0)
        Qx_wrap_up = np.abs(Qf.dot(x_wrap_up-xgoal)**2.0)
        Qx_wrap_down = np.abs(Qf.dot(x_wrap_down-xgoal)**2.0)
        cost_norm = np.sum(Qx,axis=0)
        cost_up = np.sum(Qx_wrap_up,axis=0)
        cost_down = np.sum(Qx_wrap_down,axis=0)
        cost = np.min([cost_norm, cost_up, cost_down])

    else:
        Qx = np.abs(Q.dot(x-xgoal)**2.0)
        Ru = np.abs(R.dot(u-ugoal))
        cost = np.sum(Qx,axis=0) + np.sum(Ru,axis=0)
    return cost

numKnotPoints = 4
horizon = 50
simLength = 200
numSims = 100
gen_per_action = 1

# change_goal = False
# goal1_time = 200


mpc = CPU_EMPC(sys.forward_simulate_dt,
               myCostFcn,
               numStates,
               numInputs,
               xmin=sys.xMin,
               xmax=sys.xMax,
               umin=[-sys.uMax],
               umax=[sys.uMax],
               horizon=horizon,
               numSims=numSims,
               numKnotPoints=numKnotPoints,
               dt=dt,
               elitism=False)    

x = np.array([0,-np.pi]).reshape(sys.numStates,1)
u = np.zeros([sys.numInputs,1])
xgoal = np.zeros([sys.numStates,1])
ugoal = np.zeros([sys.numInputs,1])
xgoal1 = np.array([0,np.pi]).reshape(sys.numStates,1)
ugoal1 = np.zeros([sys.numInputs,1])



fig = plt.figure(10)
plt.ion()

x_hist = np.zeros([sys.numStates,simLength])
x_goal_hist = np.zeros([sys.numStates,simLength])
u_hist = np.zeros([sys.numInputs,simLength])
cost_hist = np.zeros([simLength])

mut_prob = 0.3
proportional_mutate = False
mut_prob_min = 0.1
mut_range = mut_prob-mut_prob_min
err_0 = np.sum((xgoal - x)**2)

mpc.initialize_U()
mpc.assign_costs(x, xgoal, ugoal)

actual_cost = 0

# print(sys.uMax)
# raw_input("Enter")

for i in range(0,simLength):
    [Ad,Bd,wd] = sys.calc_discrete_A_B_w(x,u,dt)

    # if (change_goal == True) & (i >= goal1_time):
    #     xgoal = xgoal1
    #     ugoal = ugoal1
    #     change_goal = False
    #     mpc.initialize_U()
    #     mpc.assign_costs(x, xgoal, ugoal)
    #     print('goal changed')


    for n in range(gen_per_action):

        # print(mpc.U)

        if proportional_mutate == True:
            error = np.sum((xgoal - x)**2)
            mut_prob = error/err_0*mut_range + mut_range

        mpc.new_generation(num_select=3, selection='stochastic_acceptance',
                           mating='coin_flip', mutation='random_gene', strangers=True,
                           mut_prob = mut_prob)

        mpc.assign_costs(x,xgoal,ugoal)
        best_cost = np.min(mpc.costs)
        print(best_cost)

    u = mpc.get_best_u()

    u = u.flatten()[0]

    x = sys.forward_simulate_dt(x,u,dt)

    step_cost = mpc.cost_function(x, u, xgoal, ugoal)
    actual_cost += step_cost

    plt.figure(10)
    sys.visualize(x, u)

    u_hist[:,i] = u.flatten()
    x_hist[:,i] = x.flatten()
    x_goal_hist[:,i] = xgoal.flatten()
    cost_hist[i] = best_cost

print(actual_cost)

plt.figure(4)
plt.plot(x_hist[1,:].T)
plt.plot(x_goal_hist[1,:].T)
plt.xlabel('Timestep')
plt.ylabel('Theta (rad)')
plt.title('Position')
plt.show()

plt.figure(5)
plt.plot(u_hist.T)
plt.show()
plt.xlabel('Timestep')
plt.ylabel('Torque (Nm)')
plt.title('Input')

plt.figure(6)
plt.plot(cost_hist.T)
plt.show()
plt.xlabel('Timestep')
plt.ylabel('Cost')
plt.title('Cost')

plt.pause(1000)
