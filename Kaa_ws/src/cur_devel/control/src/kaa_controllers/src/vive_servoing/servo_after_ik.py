#!/usr/bin/env python
from pdb import set_trace as pause
import rospy
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Float64MultiArray as pose_ax_ang
# from kaa_jacobian import jacobian
from kaa_6_dof_kinematics import jacobian05 as jacobian
import numpy as np
from collections import deque as deq
import roslib
import tf.transformations as tft
from numpy import matrix as mat
from tf2_msgs.msg import TFMessage
import scipy.io as sio
import datetime
# from byu_robot.msg import states as states_msg
from state_estimator.msg import states as states_msg
# from state_estimator.msg import commands as commands_msg
from byu_robot.msg import commands as commands_msg
from threading import RLock
from copy import deepcopy, copy
import math
import time
from ego_byu.tools import constructors
from tools import kin_tools as kt
# from ego_byu.tools import kin_tools as kt
from ego_core.kinematics import kinematics as kin
# from kinematics import kinematics as kin
from scripts.path_planner.QPIK import PlanQPIK
from numpy import fromstring

# STEPS:
# subscribe to X_des, q_des (from inverse kinematics of Xdes) (or just load them in here), the vive to get X_act, and q_act (actual joint angles)
# store qdes and Xdes
# calculate and store Xact in a deque size 700
# maybe check that q_des is reached (check steady state)
# Once reached, calculate del_x (xdes-avg(xact))
# calculate jacobian
# del_q = Ja'*(K*(del_x)-Kd*(derivative (numerical) of del_x))
# calculate q_cmd = q_cmd + del_q (initial q_cmd = q_des)
# publish q_cmd
# repeat loop

# Transformation notation:
#kb = Kaa base frame at the first joint
#ke = Kaa end effector
#ve = vive tracker on the link on the end effector
#vb = vive tracker attached to the wooden base
#vw = vive world frame
# T_b_a = frame b expressed in frame a s.t. T_b_a*p_b = p_a (b is the subscript and a is the superscript)

class calc_q_cmd():
    def __init__(self):
        self.lock = RLock()
        #Initialize ROS node
        rospy.init_node('vive_servo_to_position', anonymous=True)

        #Load desired positions
        filepath = '/home/kraudust/git/byu/cur_devel/design/src/evolutionary_optimization/src/optimization/multi_arm_data/test_hardware_new_floor/data_file.mat'
        ja_filepath = '/home/kraudust/git/byu/cur_devel/design/src/evolutionary_optimization/src/optimization/multi_arm_data/test_hardware_new_floor/paths.mat'
        opt_param = sio.loadmat(filepath)
        ja_path = sio.loadmat(ja_filepath)
        self.des_joint_path_ = ja_path['ja_paths'][0]
        # for test_hardware
        self.des_joint_path = np.array([self.des_joint_path_[0], self.des_joint_path_[1765], self.des_joint_path_[4999], self.des_joint_path_[6032], self.des_joint_path_[6498], self.des_joint_path_[6975], self.des_joint_path_[7437], self.des_joint_path_[7868], self.des_joint_path_[8541], self.des_joint_path_[9168], self.des_joint_path_[9999]])
        # for test_hardware_horizontal_new_floor
        # self.des_joint_path = np.array([self.des_joint_path_[0], self.des_joint_path_[636], self.des_joint_path_[960], self.des_joint_path_[1262], self.des_joint_path_[1650], self.des_joint_path_[2709], self.des_joint_path_[3324], self.des_joint_path_[3871], self.des_joint_path_[5050], self.des_joint_path_[6101], self.des_joint_path_[6416], self.des_joint_path_[6711], self.des_joint_path_[7163], self.des_joint_path_[8352], self.des_joint_path_[8843], self.des_joint_path_[9262], self.des_joint_path_[9999]])

        self.desired_transforms_ = opt_param['desired_robot_transforms'][:,0,:,:]
        # for test_hardware
        self.desired_transforms = np.array([self.desired_transforms_[0], self.desired_transforms_[5], self.desired_transforms_[10], self.desired_transforms_[15], self.desired_transforms_[20], self.desired_transforms_[25], self.desired_transforms_[30], self.desired_transforms_[35], self.desired_transforms_[40], self.desired_transforms_[45], self.desired_transforms_[50]])
        # for test_hardware_horizontal_new_floor
        # self.desired_transforms = np.array([self.desired_transforms_[0], self.desired_transforms_[4], self.desired_transforms_[9], self.desired_transforms_[14], self.desired_transforms_[19], self.desired_transforms_[24], self.desired_transforms_[29], self.desired_transforms_[34], self.desired_transforms_[39], self.desired_transforms_[44], self.desired_transforms_[49], self.desired_transforms_[54], self.desired_transforms_[59], self.desired_transforms_[64], self.desired_transforms_[69], self.desired_transforms_[74], self.desired_transforms_[79]])
        #fix a mistake in the data
        # for i in xrange(len(self.desired_transforms)):
        #     self.desired_transforms[i][1][3] = 0.5

        # Timing stuff
        self.Ts = 0.1
        self.rate = rospy.Rate(1.0/self.Ts)
        self.time0 = None
        self.time = None
        desired_trajectory_time = 10. # in seconds
        self.time_btw_steps = desired_trajectory_time/len(self.desired_transforms) # time between new positions in seconds
        self.first_point = True
        self.time0_servo = None
        self.time_servo_prev = None
        self.des_index = 0

        #set up publishers
        #TODO: test out various queue sizes?
        self.pub_q_cmd = rospy.Publisher('/SetJointGoal', commands_msg ,queue_size=1)
        self.pub_x_des = rospy.Publisher('/desired_pose', pose_ax_ang, queue_size = 1)
        self.pub_x_act = rospy.Publisher('/act_pose', pose_ax_ang, queue_size = 1)
        self.pub_table_pose = rospy.Publisher('/table_pose', pose_ax_ang, queue_size = 1)

        #define variables
        self.x_act = deq([],maxlen=1) # deque to hold actual end effector poses,  was len 700 before
        self.x_avg_ = mat([[None], [None], [None], [None], [None], [None]]) # average of self.x_act (temp)
        self.x_avg = mat([[None], [None], [None], [None], [None], [None]]) # average of self.x_act (used for calc)
        self.q_des = mat([[None], [None], [None], [None], [None], [None]]) # average of self.x_act (used for calc)
        self.del_x = mat([[None], [None], [None], [None], [None], [None]]) # average of self.x_act (used for calc)
        #do inverse kinematics to get first desired joint angles --------------------------------------
        arm_base_poses = opt_param['arm_base_poses']
        rots = arm_base_poses[0][0:3]
        transl = arm_base_poses[0][3:6]
        g_world_base = kt.eulxzx_trans2homog(rots, transl)
        # g_base_arm = np.array([[1.,0.,0.,0.5], [0.,1.,0.,0.], [0.,0.,1.,0.], [0.,0.,0.,1.]])
        # base = constructors.BaseFixed(g_world_base, g_base_arm)
        # narms = 50
        # seed_angs = np.array([0., 0., 0., 0., 0., 0.])
        # arms = constructors.KaaArms(narms, base, seed_angs)

        # path = {}
        # path['pos_wts'] = [[1., 1., 1.]]
        # path['rot_wts'] = [[.1, .1, .1]]
        # path['targets'] = [self.desired_transforms[0]]
        # constraints = []
        # HLP = PlanQPIK.Planner(path, arms, base, constraints, True)
        # HLP.Toplevel.max_iter = 70
        # HLP.smoothpts = 1
        # HLP.Plan()
        # self.q_des_ = mat(HLP.FinalPathConfigs).T

        #----------------------------------------------------------------------------------------------
        # get desired transforms in right frames
        self.q_des_ = (mat(self.des_joint_path[0]).T)
        T_w_b = np.linalg.inv(g_world_base)
        for i in range(len(self.desired_transforms)):
            self.desired_transforms[i] = np.matmul(T_w_b, self.desired_transforms[i])
        self.q_act_ = mat([[0.0],[0.0],[0.0],[0.0],[0.0],[0.0]]) # actual joint angles (temp)
        self.q_act = None # actual joint angles (used in calculations)
        self.x_des_ = mat([[None],[None],[None],[None],[None],[None]]) # desired cartesian end effector pose (temp)
        self.get_new_x_des()
        self.x_des = mat([[None],[None],[None],[None],[None],[None]]) # desired cartesian end effector pose (used in calculations)
        self.q_cmd = mat(np.zeros(6)).T # commanded joint angles to be published
        self.del_x = None #error in pose (i.e. desired position minus actual position)
        self.del_x_tm1 = None # previous error in pose used to calculate velocity for derivative control
        self.del_x_dot = mat([[0],[0],[0],[0],[0],[0]]) #derivative of delta x
        self.jacob = None #jacobian

        # stuff used to make sure variables are properly filled before servoing
        self.check = [0, 0] # checks to see if vive controller's have been registered in callback
        self.first_time_through = 0 # used to fill tm1 for derivative control

        # joint limits
        # self.joint_limits_low = np.array([-np.pi/2., -np.pi/2., -np.pi/2., -np.pi/2., -np.pi/2., -np.pi/2.])
        # self.joint_limits_high = np.array([np.pi/2., np.pi/2., np.pi/2., np.pi/2., np.pi/2., np.pi/2.])
        self.joint_limits_low = np.array([-.68, -.81, -1., -1.3, -1.4, -1.2])
        self.joint_limits_high = np.array([.6, .87, 1., 1.1, 1.3, .98])

        # Tunable Gains
        kpx = 0.005 #servoing proportional gain on x
        kpy = 0.005 #servoing proportional gain on y
        kpz = 0.005 #servoing proportional gain on z
        kpox = 0.0001 #servoing proportional gain on x orientation (axis angle)
        kpoy = 0.0001 #servoing proportional gain on y orientation (axis angle)
        kpoz = 0.0001 #servoing proportional gain on z orientation (axis angle)
        # kpx = 0.#servoing proportional gain on x
        # kpy = 0.#servoing proportional gain on y
        # kpz = 0.#servoing proportional gain on z
        # kpox = 0.#servoing proportional gain on x orientation (axis angle)
        # kpoy = 0.#servoing proportional gain on y orientation (axis angle)
        # kpoz = 0.#servoing proportional gain on z orientation (axis angle)
        self.Kp = mat(np.diag([kpx, kpy, kpz, kpox, kpoy, kpoz]))

        kdx = 0.005 #servoing proportional gain on x
        kdy = 0.005 #servoing proportional gain on y
        kdz = 0.005 #servoing proportional gain on z
        kdox = 0.0001 #servoing proportional gain on x orientation (axis angle)
        kdoy = 0.0001 #servoing proportional gain on y orientation (axis angle)
        kdoz = 0.0001 #servoing proportional gain on z orientation (axis angle)
        # kdx = 0.#servoing proportional gain on x
        # kdy = 0.#servoing proportional gain on y
        # kdz = 0.#servoing proportional gain on z
        # kdox = 0.#servoing proportional gain on x orientation (axis angle)
        # kdoy = 0.#servoing proportional gain on y orientation (axis angle)
        # kdoz = 0.#servoing proportional gain on z orientation (axis angle)
        self.Kd = mat(np.diag([kdx, kdy, kdz, kdox, kdoy, kdoz]))

        k_j1 = 1. #how much we want to change the 1st joint
        k_j2 = 1. #how much we want to change the 2nd joint
        k_j3 = 1. #how much we want to change the 3rd joint
        k_j4 = 1. #how much we want to change the 4th joint
        k_j5 = 1. #how much we want to change the 5th joint
        k_j6 = 1. #how much we want to change the 6th joint
        self.Kq = mat(np.diag([k_j1, k_j2, k_j3, k_j4, k_j5, k_j6]))

        self.vb = '/neo_tracker1' #change this to the vive tracker on the wooden base
        self.ve = '/neo_tracker4' #change this to the vive tracker name on the end effector
        self.vw = '/neo_tracker5' # world vive puck for multi arm manipulation
        self.vt = '/neo_tracker6'  # vive puck on the table

        #Transformations
        self.T_vb_b = mat([[1.0, 0.0, 0.0, 0.4],[0.0, -1.0, 0.0, 0.0], [0.0, 0.0, -1.0, .28], [0.0, 0.0, 0.0, 1.0]]) # this is with the base 0.5 meters back from the joint
        self.T_vw_vb = mat(np.zeros((4,4)))
        self.T_ve_vw = mat(np.zeros((4,4)))
        self.T_e_ve = mat([[0., 1., 0., 0.], [-1., 0., 0., 0.04], [0.0, 0.0, 1.0, .105], [0.0, 0.0, 0.0, 1.0]])

        #table stuff
        self.T_vwp_w = mat([[0., 1., 0., 0.], [1., 0., 0., 0.], [0., 0., -1., 0.], [0., 0., 0., 1.]])
        self.T_t_vt = mat([[0., 1., 0., 0.], [1., 0., 0., 0.], [0., 0., -1., 0.], [0., 0., 0., 1.]])
        self.T_vw_vwp_ = mat(np.zeros((4,4)))
        self.T_vt_vw_ = mat(np.zeros((4,4)))
        # self.T_vw_vwp = mat(np.zeros((4,4)))
        # self.T_vt_vw = mat(np.zeros((4,4)))
        self.T_t_w = mat(np.zeros((4,4)))

        #set up subscribers
        # rospy.Subscriber('tf', TFMessage, self.vive_callback) #subcribe to vive data to get actual pose of end effector
        rospy.Subscriber(self.vb, PoseStamped, self.Tracker_b_Callback)
        rospy.Subscriber(self.ve, PoseStamped, self.Tracker_e_Callback)
        rospy.Subscriber(self.vw, PoseStamped, self.Tracker_w_Callback)
        rospy.Subscriber(self.vt, PoseStamped, self.Tracker_t_Callback)
        rospy.Subscriber('/states',states_msg,self.q_act_callback) #joint angle subscriber

    def Tracker_b_Callback(self, msg):
        quaternion = [msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w]
        Tr = mat(tft.quaternion_matrix(quaternion))
        Tr[0,3] = msg.pose.position.x
        Tr[1,3] = msg.pose.position.y
        Tr[2,3] = msg.pose.position.z
        self.T_vw_vb = self.homog_trans_inv(Tr)
        self.check[0] = 1

    def Tracker_e_Callback(self, msg):
        quaternion = [msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w]
        Tr = mat(tft.quaternion_matrix(quaternion))
        Tr[0,3] = msg.pose.position.x
        Tr[1,3] = msg.pose.position.y
        Tr[2,3] = msg.pose.position.z
        self.T_ve_vw = deepcopy(Tr)
        self.check[1] = 1
        if all(i > 0 for i in self.check):
            # Calculate end effector pose
            T_e_b  = self.T_vb_b * self.T_vw_vb * self.T_ve_vw * self.T_e_ve
            R_e_b = copy(T_e_b[0:3, 0:3])
            ax = kin.AxFromRot(R_e_b) # gets axis angle representation of end effector orientation 1
            ox = ax[0][0] # extract x portion of axis angle representation
            oy = ax[1][0] # extract y portion of axis angle representation
            oz = ax[2][0] # extract z portion of axis angle representation
            self.x_act.append(mat([[T_e_b[0,3]], [T_e_b[1,3]], [T_e_b[2,3]], [ox], [oy], [oz]]))
            x_act_mat = np.concatenate(self.x_act,axis=1)
            self.x_avg_ = x_act_mat.mean(1)

    def Tracker_w_Callback(self, msg):
        quaternion = [msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w]
        Tr = mat(tft.quaternion_matrix(quaternion))
        Tr[0,3] = msg.pose.position.x
        Tr[1,3] = msg.pose.position.y
        Tr[2,3] = msg.pose.position.z
        self.T_vw_vwp_ = self.homog_trans_inv(Tr)

    def Tracker_t_Callback(self, msg):
        quaternion = [msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w]
        self.T_vt_vw_ = mat(tft.quaternion_matrix(quaternion))
        self.T_vt_vw_[0,3] = msg.pose.position.x
        self.T_vt_vw_[1,3] = msg.pose.position.y
        self.T_vt_vw_[2,3] = msg.pose.position.z
        self.T_t_w = self.T_vwp_w*self.T_vw_vwp_*self.T_vt_vw_*self.T_t_vt

    def q_act_callback(self,data):
        q_act_ = list(data.q)
        self.q_act_ = mat([[q_act_[0]],[q_act_[1]],[q_act_[2]],[q_act_[3]],[q_act_[4]],[q_act_[5]]]) # actual joint angles (temp)

    def get_new_x_des(self):
        if self.des_index < len(self.desired_transforms):
            x = self.desired_transforms[self.des_index][0][3]
            y = self.desired_transforms[self.des_index][1][3]
            z = self.desired_transforms[self.des_index][2][3]
            R_des = copy(self.desired_transforms[self.des_index][0:3,0:3])
            ax = kin.AxFromRot(R_des) # gets axis angle representation of end effector orientation 1
            ox = ax[0][0] # extract x portion of axis angle representation
            oy = ax[1][0] # extract y portion of axis angle representation
            oz = ax[2][0] # extract z portion of axis angle representation
            self.q_cmd = (mat(self.des_joint_path[self.des_index]).T)
        else:
            x = self.x_des_[0,0]
            y = self.x_des_[1,0]
            z = self.x_des_[2,0]
            ox = self.x_des_[3,0]
            oy = self.x_des_[4,0]
            oz = self.x_des_[5,0]
        self.x_des_ = mat([[x],[y],[z],[ox],[oy],[oz]])

    def homog_trans_inv(self,Tr):
        Tinv = mat(np.zeros((4,4)))
        Tinv[0:3,0:3] = Tr[0:3,0:3].T
        Tinv[0:3,3] = (-Tr[0:3,0:3].T)*(Tr[0:3,3])
        Tinv[3,3] = 1.0
        return Tinv

    def calc_del_q(self):
        self.lock_data()
        q = np.array(self.q_act)
        self.jacob = mat(jacobian(q))

        # calculate del_x and del_x_dot
        self.del_x = self.x_des-self.x_avg

        # wrap del_x angles from -pi to pi
        for i in range(4,6):
            if self.del_x[i] < -np.pi:
                while self.del_x[i] < -np.pi:
                    self.del_x[i] = self.del_x[i] + 2.*np.pi
            elif self.del_x[i] > np.pi:
                while self.del_x[i] > np.pi:
                    self.del_x[i] = self.del_x[i] - 2.*np.pi


        if self.first_time_through == 0:
            self.del_x_tm1 = deepcopy(self.del_x)
            self.first_time_through = 1
        self.del_x_dot = (self.del_x - self.del_x_tm1)/self.Ts
        # print 'x: ',np.around(self.x_avg[0,0],3), '    x_des', self.x_des[0,0]
        # print 'y: ',np.around(self.x_avg[1,0],3), '    y_des', self.x_des[1,0]
        # print 'z: ',np.around(self.x_avg[2,0],3), '    z_des', self.x_des[2,0]

        del_q = self.Kq*self.jacob.T *(self.Kp * self.del_x - self.Kd * self.del_x_dot)

        self.q_cmd = self.q_cmd + del_q # in radians

        # limit commanded values to joint limits to avoid windup
        for i in range(6):
            if self.q_cmd[i] < self.joint_limits_low[i]:
                self.q_cmd[i] = self.joint_limits_low[i]
            elif self.q_cmd[i] > self.joint_limits_high[i]:
                self.q_cmd[i] = self.joint_limits_high[i]

        self.del_x_tm1 = deepcopy(self.del_x)

    def lock_data(self):
        self.lock.acquire()
        try:
            self.x_avg = deepcopy(self.x_avg_)
            self.x_des = deepcopy(self.x_des_)
            self.q_act = deepcopy(self.q_act_)
            self.q_des = deepcopy(self.q_des_)
            # self.T_vw_vwp = deepcopy(self.T_vw_vwp_)
            # self.T_vt_vw = deepcopy(self.T_vt_vw_)
        finally:
            self.lock.release()

    def calc_table_pose(self):
        self.T_t_w = self.T_vwp_w*self.T_vw_vwp*self.T_vt_vw*self.T_t_vt
        # self.T_t_w = self.T_vwp_w*self.T_vw_vwp_*self.T_vt_vw_*self.T_t_vt

    def run(self):
        while self.x_avg[1,0] == None or self.x_des[1,0] == None or self.q_des[1,0] == None:
            print('x_act not yet recording')
            print(self.x_avg.T)
            print(self.x_des.T)
            print(self.q_des.T)
            self.lock_data()
            self.rate.sleep()

        # Set up the message for the joint angles
        q_cmd_pub = commands_msg()
        q_cmd_pub.qdot = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

        # Set up message for desired position
        x_des_pub = pose_ax_ang()
        x_des_pub.data = [None]*7

        # Set up message for actual position
        x_act_pub = pose_ax_ang()
        x_act_pub.data = [None]*7

        # Set up message for the table pose
        table_pub = pose_ax_ang()
        table_pub.data = [None]*7

        self.q_cmd = deepcopy(self.q_des)
        # for servoing data --------------------------------------------------
        # position 1
        # self.q_cmd[0,0] = 30.*np.pi/180.
        # self.q_cmd[1,0] = -30.*np.pi/180.
        # self.q_cmd[2,0] = -30.*np.pi/180.
        # self.q_cmd[3,0] = 30.*np.pi/180.
        # self.q_cmd[4,0] = 30.*np.pi/180.
        # self.q_cmd[5,0] = 30.*np.pi/180.
        # x = 1.879
        # y = .375
        # z = -.256
        # ox = 1.18
        # oy = -.094
        # oz = 0.962
        # self.x_des_ = mat([[x],[y],[z],[ox],[oy],[oz]])

        # # position 2
        # self.q_cmd[0,0] = -30.*np.pi/180.
        # self.q_cmd[1,0] = 30.*np.pi/180.
        # self.q_cmd[2,0] = 30.*np.pi/180.
        # self.q_cmd[3,0] = -30.*np.pi/180.
        # self.q_cmd[4,0] = -30.*np.pi/180.
        # self.q_cmd[5,0] = -30.*np.pi/180.
        # x = 1.742
        # y = -.642
        # z = .511
        # ox = 1.274
        # oy = -.376
        # oz = -.927
        # self.x_des_ = mat([[x],[y],[z],[ox],[oy],[oz]])
        #---------------------------------------------------------------------
        self.time0 = time.time()
        while not rospy.is_shutdown():
            self.lock_data()

            # get to desired joint angles before servoing only for first point though------------------
            if self.first_point:
                self.lock_data()
                q_error = np.mean(np.absolute(self.q_act - self.q_des), axis=0)[0,0]
                q_cmd_pub.q = (np.array(self.q_cmd.T)[0])
                while q_error > 0.05: #while the average joint angle error is greater than 0.05 rad keep doing traditional joint angle control
                    self.pub_q_cmd.publish(q_cmd_pub)
                    self.lock_data()
                    # self.calc_table_pose()
                    self.time = time.time() - self.time0
                    x_des_pub.data[0] = self.time
                    x_des_pub.data[1] = self.x_des[0,0]
                    x_des_pub.data[2] = self.x_des[1,0]
                    x_des_pub.data[3] = self.x_des[2,0]
                    x_des_pub.data[4] = self.x_des[3,0]
                    x_des_pub.data[5] = self.x_des[4,0]
                    x_des_pub.data[6] = self.x_des[5,0]

                    x_act_pub.data[0] = self.time
                    x_act_pub.data[1] = self.x_avg[0,0]
                    x_act_pub.data[2] = self.x_avg[1,0]
                    x_act_pub.data[3] = self.x_avg[2,0]
                    x_act_pub.data[4] = self.x_avg[3,0]
                    x_act_pub.data[5] = self.x_avg[4,0]
                    x_act_pub.data[6] = self.x_avg[5,0]

                    R_t_w = copy(self.T_t_w[0:3, 0:3])
                    ax = kin.AxFromRot(R_t_w)
                    table_pub.data[0] = self.time
                    table_pub.data[1] = self.T_t_w[0,3]
                    table_pub.data[2] = self.T_t_w[1,3]
                    table_pub.data[3] = self.T_t_w[2,3]
                    table_pub.data[4] = ax[0][0]
                    table_pub.data[5] = ax[1][0]
                    table_pub.data[6] = ax[2][0]

                    self.pub_x_des.publish(x_des_pub)
                    self.pub_x_act.publish(x_act_pub)
                    self.pub_table_pose.publish(table_pub)
                    print('q_cmd: ', np.around(q_cmd_pub.q,3))
                    print('q_act: ', np.around(np.array(self.q_act.T)[0], 3))
                    print(q_error)
                    # q_error = np.mean(np.absolute(self.q_act - self.q_des), axis=0)[0,0] 
                    q_error = q_error/1.03 #TODO: Remove this
                    # self.calc_del_q()
                    self.rate.sleep()
                self.first_point = False
                print("Initial position reached")
                input('Press enter when you are ready to start')
                time.sleep(5) #becasue baxter has a weird delay
            #------------------------------------------------------------------------------------------
            if self.time0_servo == None:
                self.time0_servo = time.time()
                self.time_servo_prev = time.time() - self.time0_servo

            # self.calc_table_pose()
            self.time = time.time() - self.time0
            x_des_pub.data[0] = self.time
            x_des_pub.data[1] = self.x_des[0,0]
            x_des_pub.data[2] = self.x_des[1,0]
            x_des_pub.data[3] = self.x_des[2,0]
            x_des_pub.data[4] = self.x_des[3,0]
            x_des_pub.data[5] = self.x_des[4,0]
            x_des_pub.data[6] = self.x_des[5,0]

            x_act_pub.data[0] = self.time
            x_act_pub.data[1] = self.x_avg[0,0]
            x_act_pub.data[2] = self.x_avg[1,0]
            x_act_pub.data[3] = self.x_avg[2,0]
            x_act_pub.data[4] = self.x_avg[3,0]
            x_act_pub.data[5] = self.x_avg[4,0]
            x_act_pub.data[6] = self.x_avg[5,0]

            R_t_w = copy(self.T_t_w[0:3, 0:3])
            ax = kin.AxFromRot(R_t_w)
            table_pub.data[0] = self.time
            table_pub.data[1] = self.T_t_w[0,3]
            table_pub.data[2] = self.T_t_w[1,3]
            table_pub.data[3] = self.T_t_w[2,3]
            table_pub.data[4] = ax[0][0]
            table_pub.data[5] = ax[1][0]
            table_pub.data[6] = ax[2][0]

            self.calc_del_q()
            q_cmd_pub.q = (np.array(self.q_cmd.T)[0])

            self.pub_q_cmd.publish(q_cmd_pub)
            self.pub_x_des.publish(x_des_pub)
            self.pub_x_act.publish(x_act_pub)
            self.pub_table_pose.publish(table_pub)

            print('q_cmd: ', np.around(q_cmd_pub.q,3))
            print('q_act: ', np.around(np.array(self.q_act.T)[0],3))
            print('x_des: ', np.around(self.x_des.T, 3))
            print('x_act: ', np.around(self.x_avg.T, 3))
            print('cartesian error: ', np.around(math.sqrt(self.del_x[0,0]**2 + self.del_x[1,0]**2 + self.del_x[2,0]**2), 3))
            print('\n')
            self.rate.sleep()
            time_servo = time.time() - self.time0_servo
            if time_servo > self.time_servo_prev + self.time_btw_steps:
                self.des_index = self.des_index + 1
                self.get_new_x_des()
                self.time_servo_prev = time.time() - self.time0_servo
            # print self.q_cmd.T
    def pub_q_des_user(self):
        # Set up the message for the joint angles
        q_cmd_pub = commands_msg()
        q_cmd_pub.qdot = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        q_user = input('Enter desired joint angles: ')
        q_user_ = fromstring(q_user, dtype = float, count = 6, sep = ',')
        q_cmd_pub.q = q_user_
        self.pub_q_cmd.publish(q_cmd_pub)

if __name__=='__main__':
    vive_servo_fun = calc_q_cmd()
    vive_servo_fun.run()
    # while not rospy.is_shutdown():
    #     vive_servo_fun.pub_q_des_user()
