import sympy
import sympybotics
import numpy as np

import math
# has dh paramaters for right and left arms from shoulder to fingers with sh1, sh2, w1, w2
#             alpha     a       d       theta
a = [0.06, 0.527, 0.346, 0.224, 0.27, 0.03]
alpha = [np.pi/2, -np.pi/2, np.pi/2, -np.pi/2, np.pi/2, 0.]
d = [0., 0., 0., 0., 0., 0.]
theta = [0., 0., 0., 0., 0., 0.]
dh_params = [(alpha[0], a[0], d[0], 'q+'+str(theta[0])),
			(alpha[1], a[1], d[1], 'q+'+str(theta[1])),
			(alpha[2], a[2], d[2], 'q+'+str(theta[2])),
			(alpha[3], a[3], d[3], 'q+'+str(theta[3])),
			(alpha[4], a[4], d[4], 'q+'+str(theta[4])),
			(alpha[5], a[5], d[5], 'q+'+str(theta[5]))]

rbtdef = sympybotics.RobotDef('kaa', dh_params , 'standard')
rbt = sympybotics.RobotDynCode(rbtdef)

kaa_jacobian = open('./' + 'kaa_jacobian.py', 'w+')
print("from math import sin, cos", file=kaa_jacobian)
print("import numpy as np", file=kaa_jacobian)
print("pi = np.pi\n\n\n", file=kaa_jacobian)

joint_jac_code = sympy.cse(rbt.kin.J[-1])
jac_string = sympybotics.robotcodegen.robot_code_to_func('python', joint_jac_code, 'jacob_out', 'jacobian' , rbtdef)

jac_list_string = jac_string.split('\n')
jac_list_string.insert(-1, '    jacob_out = np.array(jacob_out).reshape(6,'+str(rbt.dof)+')')
jac_final = "\n".join(jac_list_string)
print(jac_final + '\n\n\n', file=kaa_jacobian)

kaa_jacobian.close()
