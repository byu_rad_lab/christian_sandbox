#!/usr/bin/env python
import numpy as np
import rospy
import time
from king_louie.msg import mpc_msg

rospy.init_node('rand_pressure_profile', anonymous=True)
pressure_pub_lsh1 = rospy.Publisher('/Joint5DesiredPressures', mpc_msg, queue_size = 1, tcp_nodelay = True)
pressure_pub_lsh2 = rospy.Publisher('/Joint6DesiredPressures', mpc_msg, queue_size = 1, tcp_nodelay = True)
pressure_pub_lw1 = rospy.Publisher('/Joint8DesiredPressures', mpc_msg, queue_size = 1, tcp_nodelay = True)
pressure_pub_lw2 = rospy.Publisher('/Joint9DesiredPressures', mpc_msg, queue_size = 1, tcp_nodelay = True)

poff = 14.6959488 # atmospheric pressure (we command the pressure controller in absolute for some reason)
max_gauge_pressure = 20.0 # maximum pressure to command to
max_time_btw_steps = 5.0
while not rospy.is_shutdown():
    # get random desired pressures
    p_d = max_gauge_pressure*np.random.rand(4,2)

    # put desired pressures into ros msg
    msg1 = mpc_msg([p_d[0,0]+poff, p_d[0,1]+poff], 1)
    # msg1 = mpc_msg([5.0+poff, 20.0+poff], 1)
    msg2 = mpc_msg([p_d[1,0]+poff, p_d[1,1]+poff], 1)
    # msg2 = mpc_msg([5.0+poff, 20.0+poff], 1)
    msg3 = mpc_msg([p_d[2,0]+poff, p_d[2,1]+poff], 1)
    msg4 = mpc_msg([p_d[3,0]+poff, p_d[3,1]+poff], 1)

    # publish desired pressures
    pressure_pub_lsh1.publish(msg1)
    pressure_pub_lsh2.publish(msg2)
    pressure_pub_lw1.publish(msg3)
    pressure_pub_lw2.publish(msg4)
    
    
    
    # sleep for a random amount of time (10 seconds or less)
    sleep_time = max_time_btw_steps*np.random.rand()
    print("Pressures")
    print('L_sh1', '\t', p_d[0,:])
    print('L_sh2', '\t', p_d[1,:])
    print('L_w1', '\t', p_d[2,:])
    print('L_w2', '\t', p_d[3,:])
    print("Time: ", sleep_time, '\n')
    time.sleep(sleep_time)

