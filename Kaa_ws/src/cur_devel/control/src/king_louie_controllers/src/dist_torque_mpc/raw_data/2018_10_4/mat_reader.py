import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt

file = input('enter the .mat file to read in: ')
print(file)
try:
	data = sio.loadmat(file)

	time = data['time']
	time = time[0]

	p1 = data['p1']
	p1 = p1[0]

	p0 = data['p0']
	p0 = p0[0]

	q_goal = data['q_goal']
	q_goal = q_goal[0]

	q_ref = data['q_ref']
	q_ref = q_ref[0]

	fpi = data['fpi']
	fpi = fpi[0]
    
	q = data['q']
	q = q[0]

	phi = data['Phi']

	theta = data['theta']

	plt.figure(1)
	plt.plot(time, phi)
	legend = []
	for x in range(len(phi[0])):
		legend.append(str(x))
	plt.legend(legend)

	plt.figure(2)
	plt.plot(time, theta)
	legend = []
	for x in range(len(theta[0])):
		legend.append(str(x))
	plt.legend(legend)

	plt.figure(3)
	plt.plot(time, q)
	plt.plot(time, q_goal)
	plt.plot(time, q_ref)
	legend = []
	legend.append('q')
	legend.append('q_goal')
	legend.append('q_ref')
	plt.legend(legend)

	plt.figure(4)
	plt.plot(time, fpi)
	legend = []
	legend.append('fpi')
	plt.legend(legend)

	plt.show()

except IOError:
	print('incorrect file')

