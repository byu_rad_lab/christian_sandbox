clear all; clc;
deg = 180/pi;

jointList = [8,9];

figure

for i = 1:2
joint = jointList(i);
    load(strcat('mpc_data_j', int2str(joint), '.mat'))

    subplot(2,1,i), hold on
    title(['Link ' num2str(i+1)])
    plot(time, p0, 'linewidth',2)
    plot(time, p1, 'linewidth',2)
    xlim([0 100])
    ylabel('Pressure (psig)')
end
xlabel('Time (s)')
legend('p^{-}', 'p^{+}')