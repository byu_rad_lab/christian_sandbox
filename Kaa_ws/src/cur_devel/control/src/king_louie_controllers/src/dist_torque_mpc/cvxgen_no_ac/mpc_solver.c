#include <boost/python.hpp> 
#include "solver.h" 
#include <iostream>

Vars vars; 
Params params; 
Workspace work; 
Settings settings; 

int flags = 0; 

boost::python::list runController(      // these are all the paramters that get sent to the optimizer 
                                  double Q,
                                  double S,
                                  boost::python::list V,
                                  boost::python::list A,
                                  boost::python::list B,
                                  boost::python::list x_0,
                                  boost::python::list u_last,
                                  double theta_max,
                                  double p_max,
                                  double p_min,
                                  double p_target,
                                  double p_slew,
                                  double x_goal,
                                  double dint,
                                  boost::python::list dstrb,
								  boost::python::list tP,
                                  int n_inputs)
{ 
  
    if (flags == 0) 
    { 
        set_defaults(); 
        setup_indexing(); 
        flags = 1; 
        settings.verbose = 0; 
        settings.eps = 1e-4; // this could be changed to e-6 to be more accurate if necessary 
    } 

    params.Q[0] = Q;
    params.S[0] = S;
    params.p_max[0] = p_max;
    params.p_min[0] = p_min;
    params.p_target[0] = p_target;
    params.x_goal[0] = x_goal;
    params.dint[0] = dint;

    int i;

    for (i = 0; i < boost::python::len(V); i++)
        params.V[i] = boost::python::extract<double>(V[i]);

    for (i = 0; i < boost::python::len(A); i++)
        params.A[i] = boost::python::extract<double>(A[i]);

    for (i = 0; i < boost::python::len(B); i++)
        params.B[i] = boost::python::extract<double>(B[i]);

    for (i = 0; i < boost::python::len(x_0); i++)
        params.x_0[i] = boost::python::extract<double>(x_0[i]);

    for (i = 0; i < boost::python::len(u_last); i++)
        params.u_last[i] = boost::python::extract<double>(u_last[i]);

    for (i = 0; i < boost::python::len(dstrb); i++)
        params.dstrb[i] = boost::python::extract<double>(dstrb[i]);

    for (i = 0; i < boost::python::len(tP); i++)
        params.tP[i] = boost::python::extract<double>(tP[i]);

    solve(); 


    boost::python::list P_cmd; 
    boost::python::list all_data; 
    boost::python::list P0cmd; 
    boost::python::list P1cmd; 
    boost::python::list theta_dot; 
    boost::python::list theta; 
    boost::python::list P_0; 
    boost::python::list P_1; 


    if (work.converged == 1) 
    { 
        for (int j=0; j < 20; j++)  // the max j can be is the mpc horizon length
        {   
            P0cmd.append(vars.u[j][0]); 
            P1cmd.append(vars.u[j][1]); 
            theta_dot.append(vars.x[j+1][0]); 
            theta.append(vars.x[j+1][1]); 
            P_0.append(vars.x[j+1][2]); 
            P_1.append(vars.x[j+1][3]); 
        } 
    } 
    else 
    { 
        std::cout << std::endl << "FAILED TO CONVERGE" << 
                     std::endl << "FAILED TO CONVERGE" <<
                     std::endl << "FAILED TO CONVERGE" <<
                     std::endl << "FAILED TO CONVERGE" << std::endl << std::endl;

        for (i=0; i< n_inputs; i++) 
        { 
            P0cmd.append(0.0); 
            P1cmd.append(0.0); 
        } 
    } 

    all_data.append(P0cmd); 
    all_data.append(P1cmd); 
    all_data.append(theta_dot); 
    all_data.append(theta); 
    all_data.append(P_0); 
    all_data.append(P_1); 

    return all_data; 

} 

char const* greet() 
{ 
    printf("got into greet"); 
    return "hello, world (and you)"; 
} 

// this is what tells boost what the function names and definitions should be (DON'T DELETE) 
BOOST_PYTHON_MODULE(mpc_solver) 
{ 
    using namespace boost::python; 
    def("greet", greet); 
    def("runController", runController); 
} 
