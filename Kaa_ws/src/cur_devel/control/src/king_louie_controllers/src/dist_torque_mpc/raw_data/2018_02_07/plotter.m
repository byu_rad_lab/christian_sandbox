clear all; clc;
deg = 180/pi;

% jointList = [0,1,3,4];
jointList = [5,6,8,9];

figure
% for i = 1:4
%     joint = jointList(i);
%     load(strcat('mpc_data_j', int2str(joint), '.mat'))
%     
%     p = 2 * i - 1;
%     
%     subplot(4,2,p), hold on
%     plot(time, q_goal*deg)
% %     plot(time, q_ref*deg)
%     plot(time, q*deg)
%     
%     subplot(4,2,p+1), hold on
%     plot(time, theta)
% end


for i = 1:3
    joint = jointList(i+1);
    load(strcat('mpc_data_j', int2str(joint), '.mat'))
    
    subplot(3,1,i), hold on
    plot(time, theta)
    xlim([0 700])
    ylabel('\theta')
    title(['Link ' num2str(i+1)])
end


xlabel('Time (s)')


