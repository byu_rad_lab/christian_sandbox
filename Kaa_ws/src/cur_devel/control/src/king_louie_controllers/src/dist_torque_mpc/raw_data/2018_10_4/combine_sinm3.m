clear all; clc;

deg = 180/pi;

jointList = [6,8,9];

% dt = '2017_12_14';
dt = '2018_01_31';
% type = 'm0_m3_m0';
% type = 'm0';
type = 'sin_m3';

figure

for i = 1:3
    joint = jointList(i);
    load(strcat(dt, '/ac_', type, '/mpc_data_j', int2str(joint), '.mat'))
    
    subplot(3,1,i), hold on
    plot(time, q_goal*deg, 'linewidth',2, 'color',[0.088, 0.61, 0.29])
    plot(time, q*deg, 'linewidth',2, 'color','r', 'linestyle','-.')
    ylabel('Joing Angle (deg)')
    title(['Link ' num2str(i+1)])
    
    if strcmp(type, 'm0_m3_m0')==1
        xlim([0 299])
    else
        xlim([0 70])
    end
    
    if contains(type, 'sin')==0
        if i==1
            ylim([-30 30])
        elseif i==2
            ylim([-30 30])
        elseif i==3
            ylim([-70 30])
        end
    else
        if i==1
            ylim([-70 70])
        elseif i==2
            ylim([-35 50])
        elseif i==3
            ylim([-70 50])
        end
    end
end

dt = '2017_12_14';

for i = 1:3
    joint = jointList(i);
    load(strcat(dt, '/int_', type, '/mpc_data_j', int2str(joint), '.mat'))
    
    subplot(3,1,i), hold on
    plot(time, q*deg, 'linewidth',2, 'color','m', 'linestyle',':')
    ylabel('Joing Angle (deg)')
end

for i = 1:3
    joint = jointList(i);
    load(strcat(dt, '/nom_', type, '/mpc_data_j', int2str(joint), '.mat'))
    
    subplot(3,1,i), hold on
    plot(time, q*deg, 'linewidth',2, 'color','b', 'linestyle','--')
    ylabel('Joint Angle (deg)')
end

xlabel('Time (s)')
legend('q_{goal}', 'q_{AC}', 'q_{Int}', 'q_{Nom}', 'orientation', 'horizontal', 'location', 'southeast')