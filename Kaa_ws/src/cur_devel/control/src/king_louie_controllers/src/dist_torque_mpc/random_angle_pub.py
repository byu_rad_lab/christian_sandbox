import rospy
from byu_robot.msg import commands
import numpy as np
from pdb import set_trace as pause


class JointAnglePublisher:

    def __init__(self, num_joints, period, angle):
        rospy.init_node('step_joint_angles', anonymous=True)
        self.pub_qdes = rospy.Publisher('/SetJointGoal', commands, queue_size = 1)

        self.rate = rospy.Rate(50) # how fast to run the publisher

        self.Ts1 = period/2 # period of switching in seconds
        self.Ts2 = self.Ts1 * 2
        self.Ts3 = self.Ts1 * 3
        self.Ts4 = self.Ts1 * 4

        self.q_des = commands()
        self.r2d = 180.0/np.pi
        self.angle = angle/self.r2d
        # angle1 = np.random.uniform(-self.angle, self.angle)
        # angle2 = np.random.uniform(-self.angle, self.angle)
        # self.j_angles = np.array([[-50.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        #                     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        #                     [0.0, 60.0, 0.0, 0.0, 0.0, 0.0],
        #                     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]])
        self.j_angles = np.random.uniform(low=-self.angle, high=self.angle, size=(2, num_joints))
        # self.j_angles[0] *= angle1/self.r2d
        # self.j_angles[1] *= -angle2/self.r2d
        # self.j_angles[2] = self.j_angles[0]
        # self.j_angles[3] = self.j_angles[1]
        self.q_des.qdot = np.zeros(num_joints)
        # print(self.j_angles)

    def publish(self, position_number):
        # print(self.j_angles[position_number])
        # print(self.q_des.q)
        self.q_des.q = self.j_angles[position_number]
        # print(self.q_des.q)
        # print(self.q_des.qdot)
        self.pub_qdes.publish(self.q_des)
        
    def run(self):
        start_time = rospy.get_time()
        while not rospy.is_shutdown():	
            self.rate.sleep()
            elapsed_time = rospy.get_time() - start_time
            if elapsed_time < self.Ts1:
                self.publish(0)

            elif elapsed_time < self.Ts2:
                self.publish(1)

            # elif elapsed_time > self.Ts2 and elapsed_time < self.Ts3:
            #    self.publish(2)

            # elif elapsed_time > self.Ts3 and elapsed_time < self.Ts4:
            #    self.publish(3)

            else:
                self.j_angles = np.random.uniform(low=-self.angle, high=self.angle, size=self.j_angles.shape)
                # self.j_angles[0] *= angle1/self.r2d
                # self.j_angles[1] *= -angle2/self.r2d
                start_time = rospy.get_time()

if __name__=='__main__':
    publisher = JointAnglePublisher(6, 30, 45)
    publisher.run()

