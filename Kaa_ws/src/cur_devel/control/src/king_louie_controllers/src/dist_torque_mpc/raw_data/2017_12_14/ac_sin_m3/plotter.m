clear all; clc;
deg = 180/pi;

jointList = [5,6,8,9];

figure
for i = 1:4
    joint = jointList(i);
    load(strcat('mpc_data_j', int2str(joint), '.mat'))
    
    p = 2 * i - 1;
    
    q_err = cumsum(abs(q - q_ref));
    
    subplot(4,2,p), hold on
    plot(time, q_goal*deg)
    plot(time, q_err*deg)
    plot(time, q*deg)
    xlim([0 100])
    
    subplot(4,2,p+1), hold on
    plot(time, theta)
    xlim([0 100])
end

legend('q_{goal}', 'q_{ref}', 'q')


