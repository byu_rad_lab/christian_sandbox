#!/usr/bin/env python

import sys
import os
import signal
import rospy
import numpy as np
import math
import yaml
import time
from scipy import linalg

import cvxgen_dt.mpc_solver as mpc_solver
import time
from math import pi
import copy
import scipy.io as spicy
from scipy.linalg import expm
import scipy.signal as spicycigs
import optparse



from king_louie.msg import sensors as sensor
# from king_louie.msg import data_msg as data
from king_louie.msg import joint_angles_msg_array as j_a_msg
from king_louie.msg import mpc_msg
from king_louie.msg import param_msg
from king_louie.msg import joint_angles_msg

from byu_robot.msg import states as states_msg
from byu_robot.msg import commands as commands_msg
from byu_robot.msg import send_pressures

from copy import deepcopy
from threading import RLock, Timer
from collections import deque
from pdb import set_trace as pause

class MPC_control():

    def __init__(self, jt_num, no_ac, update, robot_name='kaa'):
        # jt_num should be 1-6 for kaa

        self.lock = RLock()
        self.robot_name = robot_name # options are kl and kaa
        self.d2r = math.pi/180
        
        # TODO: should we have all the kl things in one if and all the kaa things in another,
        #       or does it make more sense to have them split up by task like this?

        if self.robot_name == 'kl':
            self.joint_idx =  jt_num
            from king_louie_models.kl_dyn_params import params as dyn_params
            from king_louie_models.kl_left_dynamics import M as M_left
            from king_louie_models.kl_left_dynamics import c as c_left
            from king_louie_models.kl_left_dynamics import g as g_left
            from king_louie_models.kl_right_dynamics import M as M_right
            from king_louie_models.kl_right_dynamics import c as c_right
            from king_louie_models.kl_right_dynamics import g as g_right
            self.njts = 4
            self.goal_list1 = np.array([15, -20, -10, -40]) * self.d2r
            self.goal_list2 = np.array([-30, 20, 10, 20]) * self.d2r
            self.arm = ""
            if jt_num < 5:
                    self.arm = "Right"
            else:
                    self.arm = "Left"

            if self.arm == "Right":
                self.MassFunc = M_right
                self.c = c_right
                self.joint = self.joint_idx
                if self.joint > 2: self.joint = self.joint - 1  # elbow removed, index back one spot
                th_i = self.joint_idx + 5
            elif self.arm == "Left":
                self.MassFunc = M_left
                self.c = c_left
                self.joint = self.joint_idx - 5
                if self.joint > 2: self.joint = self.joint - 1  # elbow removed, index back one spot
                th_i = self.joint_idx

        elif self.robot_name == 'kaa':
            from kaa_dyn_params import DynParams as kaaDynParamsClass
            kaaDynParams = kaaDynParamsClass()
            self.dyn_params = kaaDynParams.params
            from kaa_6_dof_dynamics import M, c, g
            self.joint_idx = jt_num - 1 #switches the indexing here to be from 0-5 for easier indexing
            self.njts = 6
            self.MassFunc = M
            self.c = c
            self.gravcalc = g
            self.joint = self.joint_idx # TODO:do i need this??
            self.goal_list1 = np.array([15, -20, -10, -40, 30, 30]) * self.d2r
            self.goal_list2 = np.array([-30, 20, 10, 20, 30, 30]) * self.d2r
        else:
            print('Enter a correct robot name, either kaa or kl')
        self.no_ac = no_ac
        self.update = update

        self.rate = 300.0 #25.0
        self.dt = 1.0/self.rate
        self.paps = 0.000145037738 # conversion from pascals to psi
        self.poff = 14.6959488 # atmospheric pressure in psi

        self.p0_all = []
        self.p1_all = []
        self.q_all = []
        # self.q_all_ = [0., 0., 0., 0., 0., 0.]
        self.q_dot_all = []
        # self.q_dot_all_ = [0., 0., 0., 0., 0., 0.]

        self.p0 = 0
        self.p1 = 0
        self.q = 0
        self.q_dot = 0
        self.q_ref = 0

        self.time = 0
        self.time_0 = None

        I = np.eye(self.njts)

        
        if self.robot_name == 'kl':
            # mpc tuning params ------------------------------------------
            Qr = [10.0, 10.0, 5.0, 10.0]    # Right arm Error cost 
            Ql = [25.0, 10.0, 12.0, 10.0]    # Left arm Error cost 
            self.R = 0.0                          # Velocity cost at end of horizon
            self.S = 0.00005                      # Stiffness cost
            self.V = [0.7, 0.7, 0.7, 0.7]       # cost on changing pdes
            self.slew_rate = 2.5*self.dt          # contraint on difference between pdes at each time step
            self.ki = 0.005                       # integrator magnitude
            self.expscale = 30                    # scales integrator with q_dot
            self.Kd       = I * 1.229
            self.Ks       = I * 12.2844
            self.p_target = 15
            # -------------------------------------------------------
            ############## these were found from pressure dynamics data
            self.gammaPlus  = I * 1.3512
            self.gammaMinus = I * 0.9465
            self.alphaPlus  = np.diag(np.array([1.2023, 1.3066, 2.8084, 3.5161]))
            self.alphaMinus = deepcopy(self.alphaPlus)
            self.betaPlus   = np.diag(np.array([1.2015, 1.3183, 2.8043, 3.5069]))
            self.betaMinus  = deepcopy(self.betaPlus)
            ############## these were found from pressure dynamics data

            if self.arm == "Right":
                self.Q = Qr
            elif self.arm == "Left":
                self.Q = Ql
        elif self.robot_name == 'kaa':
            self.time_to_control=0.
            self.time_to_publish=0.
            self.callback_time=0.
            self.callback_iters=0.
            self.iters=0.
            self.loop_time=0.
            # mpc tuning params ------------------------------------------
            # self.Q = [1., 1., 1., 1., 1., 1.]    # Error cost 
            self.Q = [25.0, 25.0, 25.0, 10.0, 10., 10.]    # Error cost
            # self.Q = [20., 20., 10., 5., 5., 5.]    # Error cost
            self.R = 0.                             # Velocity cost at end of horizon  #NOT USED!!!!!!!
            self.S = 0.00005                        # Stiffness cost
            #self.V = [0.001]*6 #[.7, 0.7, .7, .7, .7, .7]       # cost on changing pdes
            self.V = [.9, .9, .9, .9, .9, .9]       # cost on changing pdes
            self.slew_rate = 2.5*self.dt            # constraint on difference between pdes at each time step   #NOT USED!!!!!!!
            self.ki = 0.005 * 0.                      # integrator magnitude
            self.expscale = 30                      # scales integrator with q_dot
            self.Kd       = I * 1.229
            self.Ks       = I * 12.2844
            # self.p_target = 103421.
            # self.p_target = 10. #using psi for control to avoid having to retune too drastically
            self.p_target = 5. #using psi for control to avoid having to retune too drastically
            # -------------------------------------------------------
            ############## these were found from pressure dynamics data--- pulled these from Levi's controller
            # self.gammaPlus  = I * 1.5*.2799*1.5/self.paps #need to do this since using psi
            # self.gammaMinus = I * 1.5*.2799*1.5/self.paps #need to do this since using psi
            self.gammaPlus  = I * 1.3512
            self.gammaMinus = I * 1.3512
            self.gammaPlus  = I * .9465     # TODO: These gammaPlus and gammaMinus are getting overwritten, why?
            self.gammaMinus = I * .9465
            # self.alphaPlus  = np.diag(np.array([16., 16., 16., 16., 16., 16.]))
            # self.alphaMinus = deepcopy(self.alphaPlus)
            # self.betaPlus   = deepcopy(np.diag(np.array([16., 16., 16., 16., 16., 16.])))
            # self.betaMinus  = deepcopy(self.betaPlus)
            self.alphaPlus  = np.diag(np.array([1.2023, 1.3066, 2.8084, 3.5161, 3.5, 3.5]))
            self.alphaMinus = deepcopy(self.alphaPlus)
            self.betaPlus   = np.diag(np.array([1.2015, 1.3183, 2.8043, 3.5069, 3.5, 3.5]))
            self.betaMinus  = deepcopy(self.betaPlus)
            ############## these were found from pressure dynamics data

        # joint limits
        self.q_max = 120*self.d2r #TODO: Should this be 90??

        # All pressure values used in this MPC controller are in psig.
        # The pressure controller operates in psia for king louie, so all 
        # published / subscribed pressure values are in psia for king louie.
        # Kaa is controlled in gauge pascals
        if self.robot_name == 'kl':
            self.p_max = 20
            self.p_min = 0.0
        elif self.robot_name == 'kaa':
            # self.p_max = 137895.
            # self.p_min = 10000.
            self.p_max = 20. #using psi for mpc
            self.p_min = 1.45 #using psi for mpc
        self.u_prev = [self.p_target, self.p_target] 
        self.hor_len = 35
        self.q_des_ = 0. * np.ones(self.hor_len)
        self.q_des = deepcopy(self.q_des_)
        self.fpilast = 0.0
        self.q_ref = self.goal_list1[self.joint]
        if not self.no_ac:
            if self.robot_name == 'kl':
                self.theta = np.zeros(10)
            else:
                self.theta = np.zeros(12)
        ############## record data in the arrays/deques ########################
        self.q_arr = deque([])
        self.q_ref_arr = deque([])
        self.time_arr = deque([])
        self.q_arr = deque([])
        self.p0_arr = deque([])
        self.p1_arr = deque([])
        self.p0d_arr = deque([])
        self.p1d_arr = deque([])
        self.q_dot_arr = deque([])
        self.fpi_arr = deque([])
        self.p_target_arr = deque([])
        self.q_goal_arr = deque([])
        if not self.no_ac:
            self.theta_arr = deque([])
            self.Phi_arr = deque([])
        ########################################################################
        self.loadedParams = False

        if self.robot_name == 'kl':
            self.paramPath = os.environ['HOME'] + '/git/byu/cur_devel/control/src/king_louie_controllers/src/dist_torque_mpc/adaptive_params/king_louie/theta_' + str(th_i) + '.yaml'
        else:
            self.paramPath = os.environ['HOME'] + '/git/byu/cur_devel/control/src/king_louie_controllers/src/dist_torque_mpc/adaptive_params/kaa/theta_' + str(self.joint_idx) + '.yaml'

        self.pdes_all = []
        if self.robot_name == 'kl':
            self.pdes_all_ = np.ones(8)*14.7 
        elif self.robot_name == 'kaa':
            # self.pdes_all_ = np.ones(12)*10000.
            self.pdes_all_ = np.ones(12)*14.7 #using psi for mpc
        else:
            print('enter either kaa or kl for robot_name')


        if self.robot_name =='kl':
            publisher_name = '/Joint' + str(self.joint_idx) + 'DesiredPressures'
            rospy.Subscriber("/PressureData", sensor, self.pressure_callback, tcp_nodelay=True)
            # rospy.Subscriber("/KLJointAngles",j_a_msg,self.angle_callback, tcp_nodelay = True, queue_size=1)
            rospy.Subscriber("/statesFiltered", states_msg, self.states_callback)

            if self.arm == "Right":
                rospy.Subscriber('/Joint0DesiredPressures', mpc_msg, self.pdes_callback0, tcp_nodelay=True)
                rospy.Subscriber('/Joint1DesiredPressures', mpc_msg, self.pdes_callback1, tcp_nodelay=True)
                rospy.Subscriber('/Joint3DesiredPressures', mpc_msg, self.pdes_callback3, tcp_nodelay=True)
                rospy.Subscriber('/Joint4DesiredPressures', mpc_msg, self.pdes_callback4, tcp_nodelay=True)
            else:
                rospy.Subscriber('/Joint5DesiredPressures', mpc_msg, self.pdes_callback5, tcp_nodelay=True)
                rospy.Subscriber('/Joint6DesiredPressures', mpc_msg, self.pdes_callback6, tcp_nodelay=True)
                rospy.Subscriber('/Joint8DesiredPressures', mpc_msg, self.pdes_callback8, tcp_nodelay=True)
                rospy.Subscriber('/Joint9DesiredPressures', mpc_msg, self.pdes_callback9, tcp_nodelay=True) 

            rospy.Subscriber("/SetJointGoal", j_a_msg, self.joint_goal_callback) # TODO: does this work for kl?

            self.pressure_pub = rospy.Publisher(publisher_name, mpc_msg, queue_size = 1, tcp_nodelay = True)

        elif self.robot_name == 'kaa':
            # desired pressure publisher
            self.pressure_pub = rospy.Publisher('/pressure_cmd_' + str(self.joint_idx+1), send_pressures, tcp_nodelay = True, queue_size = 1) # note that the joint should be 1-6, not 0-5

            # actual pressure subscriber and q, qdot subscriber
            rospy.Subscriber("/states", states_msg, self.kaa_callback) # used for q, qdot, and actual pressures

            # desired joint angle subscriber
            rospy.Subscriber("/SetJointGoal", commands_msg, self.kaa_joint_goal_callback, tcp_nodelay=True)
            # rospy.Subscriber("/commands", commands_msg, self.kaa_joint_goal_callback, tcp_nodelay=True)

            # desired pressure subscriber for all joints
            rospy.Subscriber("/pressure_cmd_1", send_pressures, self.kaa_pdes_callback1, tcp_nodelay=True)
            rospy.Subscriber("/pressure_cmd_2", send_pressures, self.kaa_pdes_callback2, tcp_nodelay=True)
            rospy.Subscriber("/pressure_cmd_3", send_pressures, self.kaa_pdes_callback3, tcp_nodelay=True)
            rospy.Subscriber("/pressure_cmd_4", send_pressures, self.kaa_pdes_callback4, tcp_nodelay=True)
            rospy.Subscriber("/pressure_cmd_5", send_pressures, self.kaa_pdes_callback5, tcp_nodelay=True)
            rospy.Subscriber("/pressure_cmd_6", send_pressures, self.kaa_pdes_callback6, tcp_nodelay=True)

        print "Init cleared for ", self.joint_idx+1

    def kaa_callback(self, arm_data):
        
        # this should store joint angles, joint velocities, and pressures
        self.lock.acquire()
        try:
            # self.p0_all = (self.paps*np.array(arm_data.pPlus)).tolist()
            # self.p1_all = (self.paps*np.array(arm_data.pMinus)).tolist()
            # this gets the pressures right so that pPlus causes positive torque in line with DH params
            #p0 is p+ and p1 is p-
            # TODO: is there a way to do the following in one step/more readably?
            #       maybe a list comprehension?
            pPlus_temp = (self.paps*np.array(arm_data.pPlus)).tolist()
            pMinus_temp = (self.paps*np.array(arm_data.pMinus)).tolist()
            self.p0_all = [pPlus_temp[0], pMinus_temp[1], pMinus_temp[2], pMinus_temp[3], pMinus_temp[4], pMinus_temp[5]]
            self.p1_all = [pMinus_temp[0], pPlus_temp[1], pPlus_temp[2], pPlus_temp[3], pPlus_temp[4], pPlus_temp[5]]
            self.q_all_ = list(arm_data.q)
            self.q_dot_all_ = list(arm_data.qdot)
        finally:
            self.lock.release()
            

    def pressure_callback(self, data):
        self.lock.acquire()
        try:
            self.p0_all = []
            self.p1_all = []
            
            tj = self.initTJ()+1

            for i in range(self.njts):
                if self.arm == "Left" and i == 2:  # this joint is indexed backwards from the rest
                    self.p0_all.append( data.p[int(2*tj+1)]*self.paps - self.poff )
                    self.p1_all.append( data.p[int(2*tj)]*self.paps - self.poff )
                else:
                    self.p0_all.append( data.p[int(2*tj)]*self.paps - self.poff )
                    self.p1_all.append( data.p[int(2*tj+1)]*self.paps - self.poff )
                
                tj = self.iterateAndSkipElbow(tj)

        finally:
            self.lock.release()

    def initTJ(self):
        if self.arm == 'Right':
            tj = 0
        elif self.arm == 'Left':
            tj = 5

        return tj

    def iterateAndSkipElbow(self, tj):
        if tj == 1 or tj == 6: 
            tj = tj + 2
        else: 
            tj = tj + 1

        return tj

    def states_callback(self, data):
        self.lock.acquire()
        try:
            self.q_all_ = list(data.q)
            self.q_dot_all_ = list(data.qdot)

            if self.arm == "Left":
                self.q_all_[0] = -self.q_all_[0]
                self.q_dot_all_[0] = -self.q_dot_all_[0]

            self.q_all_[3] = -self.q_all_[3]
            self.q_dot_all_[3] = -self.q_dot_all_[3]
        finally:
            self.lock.release()

    def angle_callback(self, data):
        self.lock.acquire()
        tj = self.initTJ()
        self.q_all_ = []
        self.q_dot_all_ = []

        try:
            for i in range(0,len(data.joint_angles)):  # loops through arms (right, left, hip)
                if data.joint_angles[i].arm == self.arm:
                    for j in range(self.njts):  # loop through joints in the arm
                        
                        self.q_all_.append( data.joint_angles[i].q[tj] )
                        self.q_dot_all_.append( data.joint_angles[i].q_dot[tj] )

                        tj = iterateAndSkipElbow(tj)
        finally:
            self.lock.release()

    def get_pdes_data(self, data, jt):
        self.lock.acquire()
        try:
            # arrange self.pdes_all_ for copying into input u vector
            # which has grav vals first, then p0 vals, then p1 vals
            i1 = jt
            i2 = jt + self.njts
            if data.flag == 1:
                self.pdes_all_[i1] = data.p_d[0] - self.poff
                self.pdes_all_[i2] = data.p_d[1] - self.poff
            else:
                self.pdes_all_[i1] = self.p_target
                self.pdes_all_[i2] = self.p_target
        finally:
            self.lock.release()

    def get_kaa_pdes_data(self, data, jt):
        
        self.lock.acquire()
        try:
            # arrange self.pdes_all_ for copying into input u vector
            # which has grav vals first, then p0 vals, then p1 vals
            i1 = jt
            i2 = jt + self.njts
            self.pdes_all_[i1] = data.pPlus*self.paps
            self.pdes_all_[i2] = data.pMinus*self.paps
  
        finally:
            
            self.lock.release()

    def pdes_callback0(self, data):
        self.get_pdes_data(data, 0)

    def pdes_callback1(self, data):
        self.get_pdes_data(data, 1)

    def pdes_callback3(self, data):
        self.get_pdes_data(data, 2)

    def pdes_callback4(self, data):        
        self.get_pdes_data(data, 3)
        
    def pdes_callback5(self, data):
        self.get_pdes_data(data, 0)

    def pdes_callback6(self, data):
        self.get_pdes_data(data, 1)
        
    def pdes_callback8(self, data):
        self.get_pdes_data(data, 2)

    def pdes_callback9(self, data):
        self.get_pdes_data(data, 3)
    
    def kaa_pdes_callback1(self, data):
        self.get_kaa_pdes_data(data, 0)
    def kaa_pdes_callback2(self, data):
        self.get_kaa_pdes_data(data, 1)
    def kaa_pdes_callback3(self, data):
        self.get_kaa_pdes_data(data, 2)
    def kaa_pdes_callback4(self, data):
        self.get_kaa_pdes_data(data, 3)
    def kaa_pdes_callback5(self, data):
        self.get_kaa_pdes_data(data, 4)
    def kaa_pdes_callback6(self, data):
        self.get_kaa_pdes_data(data, 5)

    def joint_goal_callback(self, data):
        if self.arm == "Right":
            j = self.joint_idx
        elif self.arm == "Left":
            j = self.joint_idx - 5

        for i in range(0,len(data.joint_angles)):
                if data.joint_angles[i].arm == self.arm:
                    if data.joint_angles[i].flag != 1:
                        print("No Joint Data")
                        return
                    self.q_des_ = data.joint_angles[i].q[j] * np.ones(self.hor_len)

    def kaa_joint_goal_callback(self, data):
        self.q_des_ = (data.q[self.joint_idx]) * np.ones(self.hor_len)
        #print(self.q_des_)

    def get_data(self):
        
        self.lock.acquire()
        try:
            # self.q_all = self.q_all_
            # self.q_dot_all = self.q_dot_all_
            # self.q = self.q_all[self.joint]
            # self.q_dot = self.q_dot_all[self.joint]
            # self.p0 = self.p0_all[self.joint]
            # self.p1 = self.p1_all[self.joint]
            # self.pdes_all = self.pdes_all_

            self.q_all = deepcopy(self.q_all_)
            self.q_dot_all = deepcopy(self.q_dot_all_)
            self.q = deepcopy(self.q_all[self.joint])
            self.q_dot = deepcopy(self.q_dot_all[self.joint])
            self.p0 = deepcopy(self.p0_all[self.joint])
            self.p1 = deepcopy(self.p1_all[self.joint])
            self.pdes_all = deepcopy(self.pdes_all_)
            self.q_des = deepcopy(self.q_des_)

            self.x = np.hstack([self.q_dot_all, self.q_all, self.p0_all, self.p1_all])
            self.x = np.matrix(self.x)
            self.x = np.transpose(self.x)

            t = time.time()
            
            if self.time_0 is None:
                self.time_0 = t
            self.timePrev = self.time
            self.time = t - self.time_0

        finally:
            
            self.lock.release()
            

    def control(self, event=None):
        
        if self.time > 800:
            rospy.signal_shutdown("")
            return
        
        self.get_data()
        
        #print "time for 'get_data' \t", t_0a-t_0

        
        # TODO: is there a reason for changing from q_des to q_goal?
        # q_goal = self.step_inputs()   
        # q_goal = self.sin_trajectory() 
        if self.robot_name =='kl':
            q_goal = self.q_des * self.d2r
        elif self.robot_name == 'kaa':
            q_goal = deepcopy(self.q_des)

        # note that p0 = p+ and p1 = p-
        
        x0 = np.array([self.q_dot, self.q, self.p0, self.p1]).flatten('F').tolist()
        

        #print "time for 'deepcopy' \t", t_1a-t_1
        ##Discretize is computationally expensive
        
        A, B = self.build_model()
        tls=time.time()
        Ad, Bd = self.discretize(A, B, 2)
        tle=time.time()
        self.loop_time+=tle-tls
        
        #Ad, Bd = self.discretize(A, B, 1)
        #print "EIGENVALUES are :", (np.linalg.eig(Ad))[0]
        
        Act, Bct, dstrb, u_all = self.build_disturbance_model(Ad, Bd)
        
        
        # choose between running adaptive control or the fancy pants integrator. they tend to fight if your run them both simultaneously.
        # if self.no_ac == True:
        if self.no_ac:
        	fpi = self.integrator(q_goal[0])
        	thetaPhi, Phi = [0, 0]
        else:
            # TODO: should we pass in A and B or A and B discretized?
        	thetaPhi, Phi = self.getAdaptiveParams(q_goal[0], A, B, u_all)
        	fpi = 0

        dstrb[0] = dstrb[0] - thetaPhi

        V = self.V[self.joint] * np.eye(2)
        V = V.flatten('F').tolist()
        q_goal = q_goal.flatten('F').tolist()
#        print "goal for joint ", self.joint, "is :", q_goal 
        
        #print "Controller setup time: ", t_4-t_2 
        #print ("For joint ", self.joint, "\tCurrent Angle = ",round(self.q/self.d2r,5), "\tGoal = ",round(q_goal[0]/self.d2r,5))
        #print "For joint ", self.joint, "\tCurrent Angle = ",round(self.q/self.d2r,5), "\tGoal = ",round(q_goal[0]/self.d2r,5)

        tl1=time.time()
        result = mpc_solver.runController(
                                           self.Q[self.joint],
                                           self.S,
                                           V,
                                           Act,
                                           Bct,
                                           x0,
                                           self.u_prev,
                                           self.q_max,
                                           self.p_max,
                                           self.p_min,
                                           self.p_target,
                                           self.slew_rate,
                                           q_goal,
                                           fpi,
                                           dstrb,
                                           2)

        
        tl2=time.time()
        tdisc=tl2-tl1
        #print "For joint ", self.joint_idx+1, " time to control ", tdisc
        self.time_to_control+=tdisc
        pdesPlus = result[0][0] 
        pdesMinus = result[1][0]
        # q_dot_troubleshoot = result[2]
        # q_troubleshoot = result[2]
        
        tl2=time.time()
        
        u = [pdesPlus, pdesMinus] # in psi

        if self.robot_name == 'kl':
            msg = mpc_msg([u[0]+self.poff, u[1]+self.poff], 1)

        elif self.robot_name == 'kaa':
            msg = send_pressures()
            msg.pPlus = (pdesPlus/self.paps)/1000. #convert back to kPa to publish
            msg.pMinus = (pdesMinus/self.paps)/1000. #convert back to kPa to publish
            msg.jointNum = self.joint+1
            '''print("message to publish:")
            print(msg)'''

        # msg.pPlus = 50000.
        # msg.pMinus = 25000.
        tl1=time.time()
        
        self.pressure_pub.publish(msg)
        tl2=time.time()
        tdisc=tl2-tl1
        self.time_to_publish += tdisc
        self.iters+=1
        #rospy.sleep(.00005) 

        self.append_data(q_goal[0], u, fpi, Phi)
        self.q_last = self.q
        self.u_prev = u
        
        
        

    def step_inputs(self):
        period = 40
        if math.sin(2*math.pi*self.time/period) > 0:
            q_goal = self.goal_list1[self.joint]
        else:
            q_goal = self.goal_list2[self.joint]

        return q_goal * np.ones(self.hor_len)

    def sin_trajectory(self):
    	period = 20

    	t = self.time
    	q_goal = np.zeros(self.hor_len)

        if self.joint==0:
            offset = -15
            amp = .2
        elif self.joint==1:
            offset = 0
            amp = 1
        elif self.joint==2:
            offset = 10
            amp = 0.63
        elif self.joint==3:
            offset = 0
            amp = -0.7

        #print(self.joint)

    	for i in range(self.hor_len):
    		t_i = t + i*self.dt
    		q_goal[i] = math.sin(2*math.pi*t_i/period)*amp + offset*self.d2r

    	return q_goal

    def integrator(self, q_goal):
        interror = q_goal - self.q
        errord = self.q_dot       
        fpisum = self.ki*math.exp(-self.expscale*abs(errord))*interror + self.fpilast 
        limit = 1.0
        fpi = min(limit, max(-limit, fpisum))
        self.fpilast = fpi

        return fpi

    def build_model(self):

        # tau = M*qdd + (C + Kd)*qd + Ks*q + G 
        # tau = gammaPlus*p0 - gammaMinus*p1
        # pd = alpha*p + beta*pdes
        #
        # qdd = -Minv*((C+Kd)*qd - Ks*q + gammaPlus*p0 - gammaMinus*p1 - G)
        
        if self.robot_name == 'kaa':
            dyn_params = deepcopy(self.dyn_params)
        z = np.zeros([self.njts, self.njts])
        I = np.eye(self.njts)

        # TODO: self.MactInv doesn't seem to be used, are these necessary?
        
        ### Christian Commented these lines out to improve performance
        #self.Mact = np.matrix(self.MassFunc(dyn_params, self.q_all))
        #self.Mact = self.Mact.reshape(self.njts, self.njts)
        
        #self.MactInv = np.linalg.inv(self.Mact)
        
        self.M = I
        
        cor = np.matrix(self.c(dyn_params, self.q_all, self.q_dot_all))
        cor = np.diag(cor)

        cKd = self.Kd + cor
        
        if self.robot_name == 'kl':
            G = np.diag(g_right(dyn_params, self.q_all))
        elif self.robot_name == 'kaa':
            G = np.diag(self.gravcalc(dyn_params,self.q_all))
        
        Minv_cKd = linalg.solve(self.M, cKd)
        Minv_Ks = linalg.solve(self.M, self.Ks)
        Minv_gp = linalg.solve(self.M, self.gammaPlus)
        Minv_gm = linalg.solve(self.M, self.gammaMinus)
        Minv_G = linalg.solve(self.M, G)
        
        A1 = np.hstack([-Minv_cKd, -Minv_Ks, Minv_gp, -Minv_gm])
        A2 = np.hstack([I, z, z, z])
        A3 = np.hstack([z, z, -self.alphaPlus, z])
        A4 = np.hstack([z, z, z, -self.alphaMinus])
        A = np.vstack([A1, A2, A3, A4])
        
        B1 = np.hstack([-Minv_G, z, z])
        B2 = np.hstack([z, z, z])
        B3 = np.hstack([z, self.betaPlus, z])
        B4 = np.hstack([z, z, self.betaMinus])
        B = np.vstack([B1, B2, B3, B4])
        
        
        
        return A, B

    def discretize(self, A, B, mthd):
        

        if mthd == 1: # Matrix Exponential
            Ad = linalg.expm2(A*self.dt)
            eye = np.eye(4)
            Asinv = linalg.solve(A,eye)
            Bd = np.dot(np.dot(Asinv,(Ad-eye)),B)
        
        elif mthd == 2:  # Zero order hold, allows A to be singular, needed for model 3
            C = np.eye(self.njts*4)
            D = 0
            #Ad, Bd, Cd, Dd, dt = spicycigs.cont2discrete((A, B, C, D),self.dt, method='zoh')
            Ad, Bd, Cd, Dd, dt = spicycigs.cont2discrete((A, B, C, D),self.dt, method='bilinear')
            #Ad, Bd, Cd, Dd, dt = spicycigs.cont2discrete((A, B, C, D),self.dt, method='backward_diff')
            
        else:
            rospy.signal_shutdown('No discretization method selected')

        Ad = np.matrix(Ad)
        Bd = np.matrix(Bd)
        
        
        return Ad, Bd

    def build_disturbance_model(self, Ad, Bd):
        i = self.joint
        i2 = i + self.njts
        i3 = i + self.njts*2
        i4 = i + self.njts*3

        onesies = np.transpose(np.matrix(np.ones(self.njts)))
        pdes_temp = np.transpose(np.matrix(self.pdes_all))

        u_all = np.vstack([onesies, pdes_temp])
        u_all = np.matrix(u_all)

        x1 = Ad*self.x + Bd*u_all

        dstrb1 = x1[i,0] - Ad[i,i]*self.x[i,0] - Ad[i,i2]*self.x[i2,0] - \
                           Ad[i,i3]*self.x[i3,0] - Ad[i,i4]*self.x[i4,0] - \
                           Bd[i,i2]*u_all[i2,0] - Bd[i,i3]*u_all[i3,0]
                         # Bd[i,i]*u_all[i] is gravity for link i, which is treated as a distrubance
        dstrb2 = x1[i2,0] - Ad[i2,i]*self.x[i,0] - Ad[i2,i2]*self.x[i2,0] - \
                            Ad[i2,i3]*self.x[i3,0] - Ad[i2,i4]*self.x[i4,0] - \
                            Bd[i2,i2]*u_all[i2,0] - Bd[i2,i3]*u_all[i3,0]
        dstrb = np.array([dstrb1, dstrb2, 0, 0]).flatten('F').tolist()
        # dstrb = np.array([0, 0, 0, 0]).flatten('F').tolist()

        A1 = np.hstack([Ad[i,i], Ad[i,i2], Ad[i,i3], Ad[i,i4]])
        A2 = np.hstack([Ad[i2,i], Ad[i2,i2], Ad[i2,i3], Ad[i2,i4]])
        A3 = np.hstack([Ad[i3,i], Ad[i3,i2], Ad[i3,i3], Ad[i3,i4]])
        A4 = np.hstack([Ad[i4,i], Ad[i4,i2], Ad[i4,i3], Ad[i4,i4]])
        A = np.vstack([A1, A2, A3, A4])
        A = A.flatten('F').tolist()

        B1 = np.hstack([Bd[i,i2], Bd[i,i3]])  # gravity is all in dstrb, so don't need it in B
        B2 = np.hstack([Bd[i2,i2], Bd[i2,i3]])
        B3 = np.hstack([Bd[i3,i2], Bd[i3,i3]])
        B4 = np.hstack([Bd[i4,i2], Bd[i4,i3]])
        B = np.vstack([B1, B2, B3, B4])
        B = B.flatten('F').tolist()

        return A, B, dstrb, u_all

    def getAdaptiveParams(self, q_goal, A, B, u_all):

        if self.loadedParams == False:
            self.loadedParams = True
            with open(self.paramPath) as stream:
                self.theta = np.array(yaml.load(stream))

        alpha = 1
        ke = 0
        # q_ref smooths the transition between step changes in q_goal. if alpha is too large (alpha = inf approximates a step input) then the sudden change in q_goal can cause adaptive paramters to go unstable
        # ke > 0 helps with  adaptive controller's transient behavior, but makes it more dificult to settle at the goal
        q_ref_dot = alpha*(q_goal - self.q_ref) + ke*(self.q - self.q_ref)  # TODO: why the ke term? it's not in thesis/paper
        q_err = self.q - self.q_ref     # TODO: this is swapped from in thesis/paper

        # Phi contains the dynamic parameters to be adapted. I started by putting all elements of A and B into Phi, and removed the elements that didn't have any impact on control performance.
        # L (upside down capital Gamma) is the tuning weight for how aggressively to change adaptive parameter theta
        # s is a spring weight that pulls theta towards 0, to keep it from growing too aggressively
        # There is one L, s and theta element for each Phi element
        #
        # Tuning L and s:
        # theta values are exported with all other control data in the write_data function
        # theta is easily plotted in matlab - just use the plot(time, theta) command and it'll plot all of the theta's on the same plot
        # I tuned it so L was large enough to settle to significant value, and s large enough to prevent theta from growing forever.
        if self.robot_name == 'kl':
            if self.joint==0 or self.joint==5:
                Phi = np.array([A[0,4]*self.x[4,0], A[0,8]*self.x[8,0], A[0,12]*self.x[12,0], A[3,7]*self.x[7,0], B[1,1]*u_all[1,0], B[2,2]*u_all[2,0], 0, 0, 0, 0])
                L = np.array([0.01, 0.005, 0.005, 0.02, 0.005, 0.01, 0, 0, 0, 0])*2
                s = np.array([0.05, 0.06, 0.01, 0.07, 0.05, 0.05, 0, 0, 0, 0])

            if self.joint==1 or self.joint==6:
                Phi = np.array([A[1,5]*self.x[5,0], A[1,9]*self.x[9,0], A[1,13]*self.x[13,0], B[1,1]*u_all[1,0], 0, 0, 0, 0, 0, 0])
                L = np.array([2, 0, 0, 1, 0, 0, 0, 0, 0, 0])
                s = np.array([0.2, 0, 0, 0.02, 0, 0, 0, 0, 0, 0])*1.1

            elif self.joint==2 or self.joint==8:
                Phi = np.array([A[0,4]*self.x[4,0], A[0,8]*self.x[8,0], A[2,6]*self.x[6,0], A[2,10]*self.x[10,0], A[2,14]*self.x[14,0], A[3,7]*self.x[7,0], A[3,11]*self.x[11,0], A[3,15]*self.x[15,0], B[1,1]*u_all[1,0], B[2,2]*u_all[2,0]])
                L = 0.005*np.ones(len(Phi))
                s = 0.09*np.ones(len(Phi))

            elif self.joint==3 or self.joint==9:
                Phi = np.array([A[0,4]*self.x[4,0], A[0,8]*self.x[8,0], A[2,6]*self.x[6,0], A[2,10]*self.x[10,0], A[2,14]*self.x[14,0], A[3,7]*self.x[7,0], A[3,11]*self.x[11,0], A[3,15]*self.x[15,0], B[1,1]*u_all[1,0], B[2,2]*u_all[2,0]])
                L = 0.0005*np.ones(len(Phi))
                s = 0.03*np.ones(len(Phi))
        
        elif self.robot_name == 'kaa':
            x = self.x
            eye = np.identity(self.njts)
            A_I = np.hstack([eye, eye, eye, eye])
            A_terms = (A[:self.njts] * A_I).sum(0)
            B_terms = (B[:self.njts, :self.njts] * eye).sum(0)
            dyn_terms = np.hstack([A_terms, B_terms])
            input_terms = np.vstack([x[:,0], u_all[:self.njts,0]]).ravel()
            input_terms = np.squeeze(np.asarray(input_terms))
            Phi = dyn_terms * input_terms
            L = 0.01*np.ones(len(Phi))
            s = 0.003*np.ones(len(Phi))

        # pause()
        # uncomment these lines if you want to experiment with adding more parameters to Phi
        # Phi = self.addTermsToPhi(A, B, u_all)
        # Phi = np.array(Phi)
        # L = 0.01*np.ones(len(Phi))
        # s = 0.003*np.ones(len(Phi))
        
        if len(self.theta) != len(Phi):
            self.theta = np.zeros(len(Phi))

        if abs(q_err) < 1*self.d2r:
        	# don't change adaptive parameters when q_err is small, to avoid adapting to noise
            theta_dot = 0
        else:
            theta_dot = -L*Phi*q_err - s*abs(q_err)*self.theta

        
        delTime = self.time - self.timePrev
        self.q_ref = self.q_ref + q_ref_dot * delTime
        # self.q_ref = q_goal # TODO: why set q_ref to q_goal here? doesn't that defeat the purpose?
        self.theta = self.theta + theta_dot * delTime

        # thetaPhi is the total adaptive component which is sent in with the model to MPC
        thetaPhi = 0
        thetaPhi = np.dot(self.theta, Phi) * self.dt #/self.rate
        # for i in range(len(Phi)):
        	# divide by rate to discretize
        #    thetaPhi = thetaPhi + self.theta[i] * Phi[i] / self.rate 

        return thetaPhi, Phi

    def addTermsToPhi(self, A, B, u_all):
    	# Use this function to add all of the A and B terms to Phi. This way you're adapting every element in the state space model and can see which elements are the most influential.
        # only adding elements from the model that aren't 0 or 1

        # Old way of doing it, updated to use np for speed/understandability
        # cols = A.shape[1]

        # for i in range(self.njts):
        #     for j in range(cols):
        #         val = A[i,j] * self.x[j,0]
        #         Phi.append(val)

        # for i in range(self.njts):
        #     for j in range(self.njts):
        #         val = B[i,j] * u_all[j,0]
        #         Phi.append(val)
        x = np.squeeze(np.asarray(self.x[:,0]))
        u = np.squeeze(np.asarray(u_all))
        
        A_terms = A * x
        B_terms = B * u
        Phi = np.hstack([A_terms[0:self.njts].ravel(), B_terms[0:self.njts,0:self.njts].ravel()])
        # Phi = np.hstack([A_terms.ravel(), B_terms.ravel()])
        Phi[Phi == 0] = 0.001
        return Phi.tolist()
    
    def append_data(self, q_goal, u, fpi, Phi):
        self.q_arr.append(self.q)
        self.q_ref_arr.append(self.q_ref)
        self.q_dot_arr.append(self.q_dot)
        self.q_goal_arr.append(q_goal)
        self.time_arr.append(self.time)
        self.p0_arr.append(self.p0)
        self.p1_arr.append(self.p1)
        self.p0d_arr.append(u[0])
        self.p1d_arr.append(u[1])
        self.fpi_arr.append(fpi)
        self.p_target_arr.append(self.p_target)
        if not self.no_ac:
            self.theta_arr.append(self.theta)
            self.Phi_arr.append(Phi)

    def write_data(self):
        
        print("\n\n\nWriting data")
        print "For Joint ", self.joint_idx, " : " 
        print "Average Time to solve MPC: ", self.time_to_control/self.iters
        print "Average Time to Publish: ", self.time_to_publish/self.iters
        print "Average Time in Discretize Model: ", self.loop_time/self.iters
        
        '''path = os.environ['HOME'] + '/git/byu/cur_devel/control/src/king_louie_controllers/src/dist_torque_mpc/raw_data/mpc_data_j' + str(self.joint_idx)
        
        if self.update == True:
            with open(self.paramPath, 'w') as outfile:
                yaml.dump(self.theta.tolist(), outfile)

        if self.no_ac:
            spicy.savemat(path,mdict={
                          'time':self.time_arr,
                          'q':self.q_arr,
                          'q_ref':self.q_ref_arr,
                          'q_dot':self.q_dot_arr,
                          'q_goal':self.q_goal_arr,
                          'p0':self.p0_arr,
                          'p1':self.p1_arr,
                          'p0d':self.p0d_arr,
                          'p1d':self.p1d_arr,
                          'rate_k':self.rate,
                          'fpi':self.fpi_arr,
                          'p_target':self.p_target_arr})
        else:

            spicy.savemat(path,mdict={
                          'time':self.time_arr,
                          'q':self.q_arr,
                          'q_ref':self.q_ref_arr,
                          'q_dot':self.q_dot_arr,
                          'q_goal':self.q_goal_arr,
                          'p0':self.p0_arr,
                          'p1':self.p1_arr,
                          'p0d':self.p0d_arr,
                          'p1d':self.p1d_arr,
                          'rate_k':self.rate,
                          'fpi':self.fpi_arr,
                          'p_target':self.p_target_arr,
                          'theta':self.theta_arr,
                          'Phi':self.Phi_arr})
                          # 'set_params':self.set_params})


        print("Written to File\n\n\n")'''

    def drainChambers(self):
        if self.robot_name == 'kl':
            msg = mpc_msg([15.0, 15.0], 1)
        elif self.robot_name == 'kaa':
            msg = send_pressures()
            msg.pPlus = 5.
            msg.pMinus = 5.
            msg.jointNum = self.joint+1
        #rate = rospy.Rate(self.rate)
        for i in range(10):
            self.pressure_pub.publish(msg)
            rospy.sleep(self.dt)
            #rate.sleep()


if __name__ == '__main__':

    parser = optparse.OptionParser()
    parser.add_option('--jt', action="store", default=5, dest="jt_num", type="int") # pick joint from 0-9, except 2 and 7 (elbows)
    parser.add_option('--no_ac', action="store_true", default=False, dest='dont_run_adaptive_control')
    parser.add_option('--u', action="store_true", default=False, dest='update_adaptive_params')
    parser.add_option('--robot_name', action="store", default='kl', dest="robot_name", type="string") # options are 'kaa' or 'kl'
    # using the -u flag will overwrite the theta.yaml files stored in dist_torque_mpc/adaptive_params with whatever theta evolves to during the current controller run
    (options, args) = parser.parse_args()

    mpc = MPC_control(options.jt_num, options.dont_run_adaptive_control, options.update_adaptive_params, robot_name=options.robot_name)

    rospy.init_node('mpc' + str(mpc.joint) + '_control', anonymous=True, disable_signals=True)
    
    rate = rospy.Rate(mpc.rate)
    rate = rospy.Rate(100)
    
    import signal
    
    def sigint(signum, frame):
        mpc.write_data()
        mpc.drainChambers()
        rospy.signal_shutdown(signum)
        
    def sigterm(signum, frame):
        rospy.signal_shutdown(signum)
        
    signal.signal(signal.SIGINT, sigint)
    signal.signal(signal.SIGTERM, sigterm)

    time.sleep(1.)
    ttot=0.
    loops=0.
    while not rospy.is_shutdown():
        tl1=time.time()
        mpc.control()
        tl2=time.time()
        tloop=tl2-tl1
        ttot+=tloop
        loops+=1
        print "Running Average Loop Time for Joint ", options.jt_num, " : ", ttot/loops 
        rate.sleep()
        

    # rospy.Timer(rospy.Duration(1/mpc.rate), mpc.control)
        
    # rospy.on_shutdown(mpc.write_data)
    # rospy.on_shutdown(mpc.drainChambers)

    # rospy.spin()

    # mpc.write_data()
    # mpc.drainChambers()
