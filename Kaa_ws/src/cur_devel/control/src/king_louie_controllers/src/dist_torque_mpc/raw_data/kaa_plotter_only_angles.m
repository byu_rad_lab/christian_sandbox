clear all; clc;
deg = 180/pi;

jointList = [0,1,2,3,4,5];
% jointList = [5,6,8,9];

figure
for i = 1:6
    if i ~= 7
        joint = jointList(i);
%         load(strcat('2019_3_18/int_m3/One_bad_spike/mpc_data_j', int2str(joint), '.mat'))
        load(strcat('mpc_data_j', int2str(joint), '.mat'))

%         p = 2 * i - 1;
        p = i;

%         subplot(6,2,p), hold on
        subplot(6,1,p), hold on
        plot(time, q_goal*deg)
%         plot(time, q_ref*deg)
        plot(time, q*deg)
        xlim([0 300])

%         subplot(6,2,p+1), hold on
%         plot(time, theta(1:length(time),:))
%         xlim([0 300])
    end
end