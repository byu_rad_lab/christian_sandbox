clear all
close all
clc

linewidth = 2;
%----------------Load Data and Store Variables----------------------------%
data = load('project_data_gain_0_01.txt');
t0 = data(1,1);
t = data(:,1) - t0;
x_d = data(:,2);
y_d = data(:,3);
z_d = data(:,4);
x = data(:,5);
y = data(:,6);
z = data(:,7);
xs = x;
ys = y;
zs = z;
ts = t;
%--------------------------Plot Data--------------------------------------%
figure()
subplot(3,1,1)
plot(t,x_d,'--','LineWidth',linewidth)
hold on
plot(t,x,'LineWidth',linewidth)
xlabel('Time (sec)')
ylabel('x (m)')
legend('x_d', 'x')
title('Response with Servoing')

subplot(3,1,2)
plot(t,y_d,'--','LineWidth',linewidth)
hold on
plot(t,y,'LineWidth',linewidth)
xlabel('Time (sec)')
ylabel('t (m)')
legend('y_d', 'd')

subplot(3,1,3)
plot(t,z_d,'--','LineWidth',linewidth)
hold on
plot(t,z,'LineWidth',linewidth)
xlabel('Time (sec)')
ylabel('z (m)')
legend('z_d', 'z')

%----------------Load Data and Store Variables----------------------------%
data = load('project_data_no_servo.txt');
t0 = data(1,1);
t = data(:,1) - t0;
x_d = data(:,2);
y_d = data(:,3);
z_d = data(:,4);
x = data(:,5);
y = data(:,6);
z = data(:,7);

%--------------------------Plot Data--------------------------------------%
figure()
subplot(3,1,1)
plot(t,x_d,'--','LineWidth',linewidth)
hold on
plot(t,x,':','LineWidth',linewidth)
plot(ts,xs,'LineWidth',linewidth)
xlabel('Time (sec)')
ylabel('x (m)')
legend('x_d', 'x', 'x_s')
title('End Effector Position')

subplot(3,1,2)
plot(t,y_d,'--','LineWidth',linewidth)
hold on
plot(t,y,':','LineWidth',linewidth)
plot(ts,ys,'LineWidth',linewidth)
xlabel('Time (sec)')
ylabel('y (m)')
legend('y_d', 'y', 'y_s')

subplot(3,1,3)
plot(t,z_d,'--','LineWidth',linewidth)
hold on
plot(t,z,':','LineWidth',linewidth)
plot(ts,zs,'LineWidth',linewidth)
xlabel('Time (sec)')
ylabel('z (m)')
legend('z_d', 'z', 'z_s')
