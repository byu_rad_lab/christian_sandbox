clear all
close all
clc
run ~/Desktop/rvctools/startup_rvc.m    
deg = pi/180;

L(1) = Revolute('d', 0.0, 'a', -0.44, 'alpha', -pi/2, 'offset', -pi/2,'standard', 'qlim', [0 0]*deg );
L(2) = Revolute('d', 0.35, 'a', 0.0, 'alpha', -pi/2, 'offset', 0.0,'standard', 'qlim', [0 0]*deg );
L(3) = Revolute('d', 0.0, 'a', 0.185, 'alpha', -pi/2, 'offset', 0.0,'standard', 'qlim', [-90 0]*deg );
L(4) = Revolute('d', 0.0, 'a', 0.39, 'alpha', 0, 'offset', 0.0,'standard', 'qlim', [-90 90]*deg );
L(5) = Revolute('d', 0.0, 'a', 0.155, 'alpha', pi/2, 'offset', 0.0,'standard', 'qlim', [-90 90]*deg );
L(6) = Revolute('d', 0.0, 'a', 0.38, 'alpha', pi, 'offset', 0.0,'standard', 'qlim', [-90 90]*deg );

arm = SerialLink(L, 'name', ' ');

% arm.base = trotx(90, 'deg')*troty(90, 'deg')*trotz(90, 'deg');
% arm.base = trotx(180, 'deg')*trotz(180,'deg');
q = [0 0 -45 30 40 -30]*deg;
arm.plot(q)
arm.fkine(q)
pause()
q = [0 0 -30 0 15 0]*deg;
arm.plot(q)
arm.fkine(q)
pause()
q = [0 0 -15 -30 -15 -15]*deg;
arm.plot(q)
arm.fkine(q)
pause()




