#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Pose
from king_louie.msg import joint_angles_msg_array as j_a_msg
from king_louie.msg import joint_angles_msg as j_a
import numpy as np

rospy.init_node('des_pos', anonymous=True)
pub_xdes = rospy.Publisher('/desired_position', Pose, queue_size = 1)
pub_qdes = rospy.Publisher('/SetJointGoal', j_a_msg, queue_size = 1)

Ts1 = 5 # how often to switch positions in seconds
Ts2 = 10
Ts3 = 15
Ts4 = 20
rate = rospy.Rate(50) # how fast to run the publisher

positions = np.array([[1,2,3], [1,2,3], [1,2,3], [1,2,3]])
pos = Pose()
pos.orientation.x = 0
pos.orientation.y = 0
pos.orientation.z = 0
pos.orientation.w = 1	

q_des = j_a()
q_des.appendage = 'Right'
conv_rad_to_deg = 180.0/np.pi
#shoulder 1, shoulder 2, elbow, wrist 1, wrist 2
j_angles = np.array([[-50.0, 0.0, 0.0, 0.0, 0.0],[0.0, 0.0, 0.0, 0.0, 0.0],[0.0, 60.0, 0.0, 0.0, 0.0],[0.0, 0.0, 0.0, 0.0, 0.0]])
q_des.flag = 1
q_des.q_dot = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
q_des_arr = j_a_msg()

def publisher(position_number):
    pos.position.x = positions[position_number, 0]
    pos.position.y = positions[position_number, 1]
    pos.position.z = positions[position_number, 2]
    pub_xdes.publish(pos)

    q_des.q = j_angles[position_number]
    q_des_arr.joint_angles = [q_des]
    pub_qdes.publish(q_des_arr)

if __name__=='__main__':
    start_time = rospy.get_time()
    while not rospy.is_shutdown():	
        rate.sleep()
        elapsed_time = rospy.get_time() - start_time
        if elapsed_time < Ts1:
            publisher(0)

        elif elapsed_time > Ts1 and elapsed_time < Ts2:
            publisher(1)

        elif elapsed_time > Ts2 and elapsed_time < Ts3:
            publisher(2)

        elif elapsed_time > Ts3 and elapsed_time < Ts4:
            publisher(3)

        else:
            start_time = rospy.get_time()

