close all
clear all
clc

linewidth = 2;
%load transformation_data_base_CX.mat
load transformation_data_end_CX.mat
%load transformation_data_end_base.mat

transformations = who('T_*');
% t(200) = [];
% t(201) = [];
% t(202) = [];
% t(203) = [];
for i = 1:6:length(transformations)
    figure()
    for j = 1:6
        Tr(j,:) = eval(transformations{i+j-1});
        plot(t,Tr(j,:),'LineWidth',linewidth)
        hold on
    end
end
figure(1)
lgd = legend('Rx Cortex', 'Ry Cortex', 'Rz Cortex', 'Rx Vive', 'Ry Vive', 'Rz, Vive');
xlabel('Time (sec)','FontSize',16)
ylabel('Radians','FontSize',16)
title('Euler Angles','FontSize', 16)
lgd.FontSize = 14;


figure(2)
lgd = legend('x Vive', 'y Vive', 'z Vive', 'x Cortex', 'y Cortex', 'z Cortex');
xlabel('Time (sec)', 'FontSize', 16)
ylabel('Meters', 'FontSize', 16)
title('Cartesian Position', 'FontSize', 16)
lgd.FontSize = 14;

figure(3)
plot(t,T_KLe_CX_rx - T_KLe_CX_vive_rx, 'LineWidth',linewidth)
hold on 
plot(t,T_KLe_CX_ry - T_KLe_CX_vive_ry, 'LineWidth',linewidth)
plot(t,T_KLe_CX_rz - T_KLe_CX_vive_rz, 'LineWidth',linewidth)
xlabel('Time (sec)')
ylabel('Radians')
title('Euler Angle Error')
legend('Rx error', 'Ry error', 'Rz error')

figure(4)
plot(t,T_KLe_CX_x - T_KLe_CX_vive_x, 'LineWidth',linewidth)
hold on 
plot(t,T_KLe_CX_y - T_KLe_CX_vive_y, 'LineWidth',linewidth)
plot(t,T_KLe_CX_z - T_KLe_CX_vive_z, 'LineWidth',linewidth)
xlabel('Time (sec)')
ylabel('Meters')
title('Cartesian Position Error')
legend('x error', 'y error', 'z error')


%-------------------------------------------------------------------------%
%Calculate Error
cartesian_error = ((T_KLe_CX_x -T_KLe_CX_vive_x).^2 + (T_KLe_CX_y -T_KLe_CX_vive_y).^2 + (T_KLe_CX_z -T_KLe_CX_vive_z).^2).^0.5;
Average_cartesian_error = mean(cartesian_error)
Rx = mean(T_KLe_CX_rx - T_KLe_CX_vive_rx)
Ry = mean(T_KLe_CX_ry - T_KLe_CX_vive_ry)
Rz = mean(T_KLe_CX_rz - T_KLe_CX_vive_rz)
Average_euler_angle_error = mean([Rx,Ry,Rz])
Average_euler_angle_error_deg = Average_euler_angle_error*180/pi
