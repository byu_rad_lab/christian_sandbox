#!/usr/bin/env python
import rospy
from king_louie.msg import joint_angles_msg_array as j_a_msg
from king_louie.msg import joint_angles_msg as j_a
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
from kl_jacob_right import jacobian
import numpy as np
from collections import deque as deq
import roslib
import tf.transformations as tft
from numpy import matrix as mat
from tf2_msgs.msg import TFMessage
import scipy.io as sio
import datetime
from byu_robot.msg import states as states_msg
from threading import RLock
from pdb import set_trace as stop
from copy import deepcopy

# STEPS:
# subscribe to X_des, q_des (from inverse kinematics of Xdes), the vive to get X_act, and q_act (actual KL joint angles)
# store qdes and Xdes
# calculate and store Xact in a deque size 700
# check steady state (norm of xact-xdes: error of 0.05)
# Once reached, calculate del_x (xdes-avg(xact))
# calculate jacobian, and sudo inverse (maybe)
# del_q = Ja'*K*(del_x) - Ja is first three rows of Jacobian
#del_q = Ja'*(K*(del_x)-Kd*(derivative (numerical) of del_x))
# calculate q_cmd = q_cmd + del_q (initial q_cmd = q_des)
# publish q_cmd
# repeat loop

# KLJointAngles order = sh1,sh2, elb, wr1, wr2

# Transformation notation:
#KLb = King Louie base frame at his hip (z is out the front, x is to the left, and y up)
#KLe = King Louie end effector
#ve = vive tracker on the link before the end effector
#vb = vive tracker attached to the wooden base
#vw = vive world frame
#vdp = vive tracker that sets the desired pose
# T_b_a = frame b expressed in frame a s.t. T_b_a*p_b = p_a (b is the subscript and a is the superscript)

class calc_q_cmd():

    def __init__(self):
        self.lock = RLock()
        #Initialize ROS node
        rospy.init_node('vive_servo_to_position', anonymous=True)
        self.Ts = 0.02
        self.rate = rospy.Rate(1.0/self.Ts)

        #set up subscribers
        rospy.Subscriber('tf', TFMessage, self.vive_callback) #subcribe to vive data to get actual position of end effector
        rospy.Subscriber('/statesFiltered',states_msg,self.q_act_callback)

        #set up publishers
        self.pub_q_cmd = rospy.Publisher('/SetJointGoal',j_a_msg,queue_size=1)
        self.pub_x_des = rospy.Publisher('/desired_position', Pose, queue_size = 1)
        self.pub_x_act = rospy.Publisher('/act_position', Pose, queue_size = 1)

        #define variables
        self.deq_length = 1 #how many end effector positions I want to average
        self.x_act = deq([],maxlen=self.deq_length) # deque to hold actual end effector positions
        self.x_avg_ = mat([[None],[None],[None]]) # average of self.x_act (temp)
        self.x_avg = mat([[None],[None],[None]]) # average of self.x_act (used in calculations)
        self.q_des = mat(np.zeros(5)).T # initial desired joint angles
        self.q_act_ = None # actual joint angles (temp)
        self.q_act = None # actual joint angles (used in calculations)
        self.x_des_ = mat([[None],[None],[None]]) # desired cartesian end effector position (temp)
        self.x_des = mat([[None],[None],[None]]) # desired cartesian end effector position (used in calculations)
        self.check = [0, 0, 0] # to check if all topics have been received before starting calculations
        self.first_time_through = 0
        self.q_cmd = mat(np.zeros(5)).T # commanded joint angles (sh1, sh2, elb, wr1, wr2) to be published
        self.del_x = None #error in position (i.e. desired position minus actual position)
        self.del_x_tm1 = None # previous error in position used to calculate velocity for derivative control
        self.del_x_dot = mat([[0],[0],[0]]) #derivative of delta x
        self.del_q = mat(np.zeros(5)).T # change in joint angles (computed from numerical inverse kinematics algorithm) to add to q_des and get q_cmd
        self.jacob = None #jacobian
        self.deg_2_rad = np.pi/180.0 # to convert from degrees to radians

        kpx = 0.01 #servoing proportional gain
        kpy = 0.01 #servoing proportional gain
        kpz = 0.01 #servoing proportional gain
        self.Kp = mat([[kpx, 0.0, 0.0], [0.0, kpy, 0.0], [0.0, 0.0, kpz]])

        kdx = 0.0 #servoing derivative gain
        kdy = 0.0 #servoing derivative gain
        kdz = 0.0 #servoing derivative gain
        self.Kd = mat([[kdx, 0.0, 0.0], [0.0, kdy, 0.0], [0.0, 0.0, kdz]])

        k_sh1 = 1.0# weight on the delta q for the shoulder 1 joint
        k_sh2 = 1.0# weight on the delta q for the shoulder 2 joint
        k_w1 = 1.0 # weight on the delta q for the wrist 1 joint
        k_w2 = 1.0 # weight on the delta q for the wrist 2 joint
        self.Kq = mat([[k_sh1, 0.0, 0.0, 0.0], [0.0, k_sh2, 0.0, 0.0], [0.0, 0.0, k_w1, 0.0], [0.0, 0.0, 0.0, k_w2]])

        self.ve = 'neo_tracker5'  # name of the vive tracker name on the end effector
        self.vb = 'neo_tracker4'  # name of the vive tracker on king louie's wooden base
        self.vdp = 'neo_tracker2' # name of the vive tracker that you want to get desired positions from

        #Transformations
        self.T_vb_KLb = mat([[0.0, 1.0, 0.0, -0.425],[0.0, 0.0, 1.0, -0.835],[1.0, 0.0, 0.0, 0.43],[0.0, 0.0, 0.0, 1.0]]) # vb frame expressed in king louie base frame (at his hip) (measured)
        # self.T_KLe_ve = mat([[0.0, 0.0, 1.0, 0.14],[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, -0.075], [0.0, 0.0, 0.0, 1.0]]) # KLe frame expressed in ve frame (measured) I think this is wrong
        self.T_KLe_ve = mat([[1.0, 0.0, 0.0, 0.27],[0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 1.0, -0.075], [0.0, 0.0, 0.0, 1.0]]) # KLe frame expressed in ve frame (measured)
        self.T_vw_vb = mat(np.zeros((4,4))) # wv frame expressed in vb frame
        self.T_ve_vw = mat(np.zeros((4,4))) # ve frame expressed in vw frame
        self.T_vdp_vw = mat(np.zeros((4,4))) # vdp frame expressed in vw frame
        self.R_KLb_sh1 = mat([[0.0, 1.0, 0.0], [-1.0, 0.0, 0.0], [0.0, 0.0, 1.0]]) # used to put the delta x in the shoulder frame (where jacobian is wrt)

    def vive_callback(self, msg): #calculates the actual and desired end effector positions using the vive
        transform_from = msg.transforms[0].child_frame_id
        if msg.transforms[0].header.frame_id == "world_vive":
            if transform_from == self.vb:
                transform = msg.transforms[0].transform #consists of translation and a quaternion
                quaternion = [transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w]
                Tr = mat(tft.quaternion_matrix(quaternion)) #controller1 in world vive frame
                Tr[0,3] = transform.translation.x
                Tr[1,3] = transform.translation.y
                Tr[2,3] = transform.translation.z
                self.T_vw_vb = self.homog_trans_inv(Tr)
                self.check[0] = 1

            elif transform_from == self.ve:
                transform = msg.transforms[0].transform #consists of translation and a quaternion
                quaternion = [transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w]
                Tr = mat(tft.quaternion_matrix(quaternion)) #controller2 in world vive frame
                Tr[0,3] = transform.translation.x
                Tr[1,3] = transform.translation.y
                Tr[2,3] = transform.translation.z
                self.T_ve_vw = Tr
                self.check[1] = 1

            elif transform_from == self.vdp:
            # elif True:
                transform = msg.transforms[0].transform #consists of translation and a quaternion
                quaternion = [transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w]
                Tr = mat(tft.quaternion_matrix(quaternion))
                Tr[0,3] = transform.translation.x
                Tr[1,3] = transform.translation.y
                Tr[2,3] = transform.translation.z
                self.T_vdp_vw = Tr
                self.check[2] = 1

        if all(i > 0 for i in self.check):
            # Calculate end effector position
            T_KLe_KLb = self.T_vb_KLb * self.T_vw_vb * self.T_ve_vw * self.T_KLe_ve
            self.x_act.append(mat([[T_KLe_KLb[0,3]], [T_KLe_KLb[1,3]], [T_KLe_KLb[2,3]]]))
            x_act_mat = np.concatenate(self.x_act,axis=1)
            self.x_avg_ = x_act_mat.mean(1)

            # Calculate desired position
            T_vdp_KLb = self.T_vb_KLb*self.T_vw_vb*self.T_vdp_vw
            self.x_des_ = mat([[T_vdp_KLb[0,3]], [T_vdp_KLb[1,3]], [T_vdp_KLb[2,3]]])

    def q_act_callback(self,data): # get joint angles
        q_all_ = list(data.q)
        q_all_[3] = -q_all_[3]
        self.q_act_ = q_all_

    def homog_trans_inv(self,Tr): # for computing the inverse of a homogeneous transform w/o taking an inverse
        Tinv = mat(np.zeros((4,4)))
        Tinv[0:3,0:3] = Tr[0:3,0:3].T
        Tinv[0:3,3] = (-Tr[0:3,0:3].T)*(Tr[0:3,3])
        Tinv[3,3] = 1.0
        return Tinv

    def calc_del_q(self): # calculate the change in joint angles to move closer to cartesian goal
        self.lock_data()
        q = np.array(self.q_act)
        self.jacob = mat(jacobian(q)) # this is a 6x4
        J_ana = self.jacob[0:3,:] # this is the first three rows of the analytical jacobian

        # calculate del_x and del_x_dot
        self.del_x = self.x_des-self.x_avg

        # convert del_x to be expressed in the sh1 frame (the jacobian is from the end effector to here)
        self.del_x = self.R_KLb_sh1*self.del_x

        if self.first_time_through == 0:
            self.del_x_tm1 = deepcopy(self.del_x)
            self.first_time_through = 1
        else:
            self.del_x_dot = (self.del_x - self.del_x_tm1)/self.Ts
            print('x: ',np.around(self.x_avg[0,0],3), '    x_des', self.x_des[0,0])
            print('y: ',np.around(self.x_avg[1,0],3), '    y_des', self.x_des[1,0])
            print('z: ',np.around(self.x_avg[2,0],3), '    z_des', self.x_des[2,0])


            del_q_4 = self.Kq*J_ana.T *(self.Kp * self.del_x - self.Kd * self.del_x_dot) #this is currently length 4

            #insert the elbow to make it length 5
            self.del_q[0:2] = del_q_4[0:2]
            self.del_q[3:5] = del_q_4[2:4]

            # self.q_cmd contains 5 joint angles (sh1, sh2, elb, wr1, wr2)
            self.q_cmd = self.q_cmd + self.del_q # in radians

            # limit commanded values to joint limits to avoid windup (sh1, sh2, el, w1, w2)
            joint_limits_low = np.array([-np.pi/2, -np.pi/2, 0.0, -np.pi/2, -np.pi/2])
            joint_limits_high = np.array([0.0, np.pi/2, 0.0, np.pi/2, np.pi/2])

            for i in range(5):
                if self.q_cmd[i] < joint_limits_low[i]:
                    self.q_cmd[i] = joint_limits_low[i]
                elif self.q_cmd[i] > joint_limits_high[i]:
                    self.q_cmd[i] = joint_limits_high[i]

            self.del_x_tm1 = deepcopy(self.del_x)

    def lock_data(self):
        self.lock.acquire()
        try:
            self.x_avg = deepcopy(self.x_avg_)
            self.x_des = deepcopy(self.x_des_)
            self.q_act = deepcopy(self.q_act_)
        finally:
            self.lock.release()

    def run(self):
        while self.x_avg_[1,0] == None or self.x_des_[1,0] == None:
            if not rospy.is_shutdown():
                print('x_act not yet assigned')
                self.lock_data()

        self.q_cmd = deepcopy(self.q_des)

        #set up the custom messages of the joint angles
        q_cmd_pub = j_a()
        q_cmd_pub.appendage = "Right"
        q_cmd_pub.q_dot = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        q_cmd_pub.flag = 1
        q_cmd_arr_pub = j_a_msg()

        #Desired and actual position publishers
        x_des_pub = Pose()
        x_des_pub.orientation.x = 0
        x_des_pub.orientation.y = 0
        x_des_pub.orientation.z = 0
        x_des_pub.orientation.w = 1

        x_act_pub = Pose()
        x_act_pub.orientation.x = 0
        x_act_pub.orientation.y = 0
        x_act_pub.orientation.z = 0
        x_act_pub.orientation.w = 1

        while not rospy.is_shutdown():
            x_des_pub.position.x = self.x_des[0,0]
            x_des_pub.position.y = self.x_des[1,0]
            x_des_pub.position.z = self.x_des[2,0]
            x_act_pub.position.x = self.x_avg[0,0]
            x_act_pub.position.y = self.x_avg[1,0]
            x_act_pub.position.z = self.x_avg[2,0]
            self.calc_del_q()
            q_cmd_pub.q = np.array(self.q_cmd.T*(180.0/np.pi))[0]
            q_cmd_arr_pub.joint_angles = [q_cmd_pub]

            self.pub_q_cmd.publish(q_cmd_arr_pub)
            self.pub_x_des.publish(x_des_pub)
            self.pub_x_act.publish(x_act_pub)

            print('q_cmd: ', np.around(q_cmd_arr_pub.joint_angles[0].q,3))
            print('\n')
            self.rate.sleep()

if __name__=='__main__':
    vive_servo_fun = calc_q_cmd()
    vive_servo_fun.run()
