
import sympy
import sympybotics
import numpy as np

import math
# has dh paramaters for right and left arms from shoulder to fingers with sh1, sh2, w1, w2
#             alpha     a       d       theta
dh_params_r = [(np.pi/2.0,    0.185,    0.0,    'q+pi'),
			(0.0,           0.39,     0.0,    'q+0.0'),
			(-np.pi/2.0,    0.155,    0.0,    'q+0.0'),
			(0.0,           0.38,     0.0,    'q+0.0')]

rbtdef_r = sympybotics.RobotDef('kl_right', dh_params_r , 'standard')
rbt_r = sympybotics.RobotDynCode(rbtdef_r)

kl_right_jacobian = open('./' + 'kl_jacob_right.py', 'w+')
print("from math import sin, cos", file=kl_right_jacobian)
print("import numpy as np", file=kl_right_jacobian)
print("pi = np.pi\n\n\n", file=kl_right_jacobian)

joint_jac_code_r = sympy.cse(rbt_r.kin.J[-1])
jac_string_r = sympybotics.robotcodegen.robot_code_to_func('python', joint_jac_code_r, 'jacob_out', 'jacobian_r' , rbtdef_r)

jac_list_string_r = jac_string_r.split('\n')
jac_list_string_r.insert(-1, '    jacob_out = np.array(jacob_out).reshape(6,'+str(rbt_r.dof)+')')
jac_final_r = "\n".join(jac_list_string_r)
print(jac_final_r + '\n\n\n', file=kl_right_jacobian)

kl_right_jacobian.close()

dh_params_l = [(-np.pi/2.0,    0.185,    0.0,    'q+0.0'),
			(0.0,           0.39,     0.0,    'q+0.0'),
			(-np.pi/2.0,    0.155,    0.0,    'q+0.0'),
			(0.0,           0.38,     0.0,    'q+0.0')]

rbtdef_l = sympybotics.RobotDef('kl_left', dh_params_l , 'standard')
rbt_l = sympybotics.RobotDynCode(rbtdef_l)

kl_left_jacobian = open('./' + 'kl_jacob_left.py', 'w+')
print("from math import sin, cos", file=kl_left_jacobian)
print("import numpy as np", file=kl_left_jacobian)
print("pi = np.pi\n\n\n", file=kl_left_jacobian)

joint_jac_code_l = sympy.cse(rbt_l.kin.J[-1])
jac_string_l = sympybotics.robotcodegen.robot_code_to_func('python', joint_jac_code_l, 'jacob_out', 'jacobian_l' , rbtdef_l)

jac_list_string_l = jac_string_l.split('\n')
jac_list_string_l.insert(-1, '    jacob_out = np.array(jacob_out).reshape(6,'+str(rbt_l.dof)+')')
jac_final_l = "\n".join(jac_list_string_l)
print(jac_final_l + '\n\n\n', file=kl_left_jacobian)

kl_left_jacobian.close()
