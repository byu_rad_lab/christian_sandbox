#!/usr/bin/env python
import tensorflow as tf
import numpy as np
import time,rospy
import matplotlib.pyplot as plt
from vi_control.control_megazord import LimbAttributes, Subscribe2Base
from scipy import signal
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Wrench
from cortex_to_velocity import Cortex2Velocity
from vive_tracker_velocity import ViveTrackerVel
from copy import deepcopy

class NNControl():
    def __init__(self,using_cortex = True):
        # Initialize node and set loop rate
        rospy.init_node('nn_control', anonymous = True)
        self.hz = 200.0
        self.rate = rospy.Rate(self.hz)
        
        # Get Base tools and Cortex velocity and arm tools
        self.SBB = Subscribe2Base()

        # Check if using cortex or vive for object velocity estimation
        self.cortex = using_cortex
        if self.cortex == True:
            print('Starting NN Control using Cortex')
            self.C2V = Cortex2Velocity()
        else:
            print('Starting NN Control using Vive')

        self.left = LimbAttributes('left')
        self.right = LimbAttributes('right')

        # Start base velocity publisher
        self.base_velocity_pub = rospy.Publisher('/des_vel',Twist,queue_size = 10)

        # Start base velocity subscriber
        self.base_velocity_sub = rospy.Subscriber('/est_vel',Twist,self.est_vel_cb)

        # Start vive tracker subscriber
        self.base_velocity_sub = rospy.Subscriber('/neo_tracker1_vel',Twist,self.vive_cb)

        # Filter stuff
        self.b, self.a = signal.butter(2, .06)
        self.zi = signal.lfilter_zi(self.b, self.a)

        # For plotting
        self.time_array = []
        self.object_vel = []
        self.object_vely = []
        self.object_velz = []
        self.pred_vel = []
        self.pred_velz = []
        self.pred_vely = []
        self.est_vel = []
        self.est_vely = []
        self.est_velz = []

        # Velocity
        self.vive_V = Twist()
        self.V = Twist()
        self.velocity_command = Twist()
        self.kp = 0.5

        # for now, hard coded
        self.n_steps = 45
        self.n_inputs = 3
        self.for_steps = 10

        # Initialize vectors
        self.vel_data = np.zeros((1,self.n_steps,6))
        self.filt_vel = np.zeros((1,self.n_steps,12))
        self.input_data = np.zeros((1,self.n_steps,self.n_inputs))
        self.pred = np.zeros((self.for_steps,self.n_inputs))

        # Tools to speed up prediction
        self.calc_counter = 0
        self.which_pred = self.for_steps/5
        self.pred_steps = 3

        # Restore Model
        self.restore_model()

    # vive Tracker Callback
    def vive_cb(self,msg):
        self.vive_V = msg

    # Velocity Callback
    def est_vel_cb(self,msg):
        self.V = msg

    # For Scaling inputs
    def scale(self,vec,mx,mn):
        ''' scales down input for NN input'''
        vec = np.divide(np.subtract(vec,mn),np.subtract(mx,mn))
        return vec

    def unscale(self,vec,mx,mn):
        ''' scales up input for publishing'''
        vec = np.add(np.multiply(vec,np.subtract(mx,mn)),mn)
        return vec

    # Restore Model from File
    def restore_model(self):
        ''' Restores model from files'''
        with tf.device('/cpu:0'):
            self.sess = tf.Session()
            saver = tf.train.import_meta_graph('latest.meta')
            saver.restore(self.sess,'latest')
            graph = tf.get_default_graph()
            self.xx = graph.get_tensor_by_name('x:0') # This is where you will put inputs
            self.predict = tf.get_collection('prediction')[0] # This gets the prediction
            print('Model Restored')

    # Get Baxter Arm Velocity
    def get_arm_velocities(self):
        ''' Gets arm velocities, jacobians, etc from pykdl'''
        self.left.get_joint_angles()
        self.left.get_joint_velocities()
        self.left.get_current_vel()
        self.right.get_joint_angles()
        self.right.get_joint_velocities()
        self.right.get_current_vel()

    # Get base velocity
    def get_base_velocity(self):
        ''' Updates arrays with velocity of base (currently from Cortex), then filters and differentiates to get acceleration as well'''
        # Update sensed velocity data
        self.vel_data = np.roll(self.vel_data,-1,axis = 1)

        # If using Cortex
        if self.cortex == True:
            self.vel_data[:,-1,0] = self.C2V.V_rot[0]
            self.vel_data[:,-1,1] = self.C2V.V_rot[1]
            self.vel_data[:,-1,2] = self.C2V.V_rot[2]
            self.vel_data[:,-1,3] = self.C2V.W[0]
            self.vel_data[:,-1,4] = self.C2V.W[1]
            self.vel_data[:,-1,5] = self.C2V.W[2]
        else:
            self.vel_data[:,-1,0] = self.vive_V.linear.x
            self.vel_data[:,-1,1] = self.vive_V.linear.y
            self.vel_data[:,-1,2] = self.vive_V.linear.z
            self.vel_data[:,-1,3] = self.vive_V.angular.x
            self.vel_data[:,-1,4] = self.vive_V.angular.y
            self.vel_data[:,-1,5] = self.vive_V.angular.z

        # Filter velocity data, get acceleration data
        for j in range(6):
            self.filt_vel[:,:,j] , _ = signal.lfilter(self.b, self.a, self.vel_data[:,:,j][0], zi = self.zi * self.vel_data[:,0,j])
            temp_deriv = np.gradient(self.filt_vel[:,:,j][0]) * self.hz
            self.filt_vel[:,:,j + self.n_inputs/2], _ = signal.lfilter(self.b, self.a, temp_deriv, zi = self.zi*temp_deriv[0])
        # for j in xrange(self.n_inputs/2):
        #     self.filt_data[:,:,j] = self.sensed_data[:,:,j][0]
        #     self.filt_data[:,:,j + self.n_inputs/2] = np.gradient(self.sensed_data[:,:,j][0]) * self.hz

        # Scale
        if self.cortex == True:
            self.filt_vel[:,:,0] = self.scale(self.filt_vel[:,:,0],0.6,-0.6)
            self.filt_vel[:,:,1] = self.scale(self.filt_vel[:,:,1],0.5,-0.5)
            self.filt_vel[:,:,2] = self.scale(self.filt_vel[:,:,2],1.0,-1.0)
            self.filt_vel[:,:,3] = self.scale(self.filt_vel[:,:,3],0.3,-0.3)
            self.filt_vel[:,:,4] = self.scale(self.filt_vel[:,:,4],0.6,-0.6)
            self.filt_vel[:,:,5] = self.scale(self.filt_vel[:,:,5],0.7,-0.7)
        else:
            self.filt_vel[:,:,0] = self.scale(self.vel_data[:,:,0],0.6,-0.6)
            self.filt_vel[:,:,1] = self.scale(self.vel_data[:,:,1],0.5,-0.5)
            self.filt_vel[:,:,2] = self.scale(self.vel_data[:,:,2],1.0,-1.0)
            self.filt_vel[:,:,3] = self.scale(self.vel_data[:,:,3],0.3,-0.3)
            self.filt_vel[:,:,4] = self.scale(self.vel_data[:,:,4],0.6,-0.6)
            self.filt_vel[:,:,5] = self.scale(self.vel_data[:,:,5],0.7,-0.7)
        self.filt_vel[:,:,6] = self.scale(self.filt_vel[:,:,6],0.5,-0.5)
        self.filt_vel[:,:,7] = self.scale(self.filt_vel[:,:,7],0.5,-0.5)
        self.filt_vel[:,:,8] = self.scale(self.filt_vel[:,:,8],1.5,-1.5)
        self.filt_vel[:,:,9] = self.scale(self.filt_vel[:,:,9],0.5,-0.5)
        self.filt_vel[:,:,10] = self.scale(self.filt_vel[:,:,10],1.3,-1.3)
        self.filt_vel[:,:,11] = self.scale(self.filt_vel[:,:,11],0.9,-0.9)

    # Input Data Generation
    def input_data_generation(self):
        ''' puts appropriate section of data into input_data array for use in prediction'''
        self.input_data[:,:,0] = self.filt_vel[:,:,0]
        self.input_data[:,:,1] = self.filt_vel[:,:,1]
        self.input_data[:,:,2] = self.filt_vel[:,:,5]
        
    # Predict Velocity with NN
    def predict_velocity(self):
        '''Predicts the velocity to command '''
        with tf.device('/cpu:0'): # run on cpu
            if self.calc_counter > self.which_pred: # run every self.which_pred times
                for j in range(self.pred_steps): # only get self.pred_steps predictions
                    feed_dict = {self.xx:self.input_data[:,:,:]} # feed dictionary
                    self.pred[j,:] = self.sess.run(self.predict,feed_dict) # run nn pred
                    self.input_data = np.roll(self.input_data,-1,axis = 1) # roll back
                    self.input_data[:,-1,:] = self.pred[j,:] # replace latest entry

                # scale up velocities for publishing
                self.pred[j,0] = self.unscale(self.pred[j,0],1.0,-1.0)
                self.pred[j,1] = self.unscale(self.pred[j,1],0.8,-0.8)
                self.pred[j,2] = self.unscale(self.pred[j,2],0.8,-0.8)

                # apply predicted velocities    
                if abs(self.pred[j,0]) < 0.05:
                    self.velocity_command.linear.x = 0.0
                else:
                    self.velocity_command.linear.x = self.pred[j,0]
                if abs(self.pred[j,1]) < 0.05:
                    self.velocity_command.linear.y = 0.0
                else:
                    self.velocity_command.linear.y = self.pred[j,1] - (self.pred[j,2] * 2.0)
                if abs(self.pred[j,2]) < 0.05:
                    self.velocity_command.angular.z = 0.0
                else:
                    self.velocity_command.angular.z = self.pred[j,2]

                # Reset Counter
                self.calc_counter = 0

    # publish velocity to base
    def publish_velocity(self):
        ''' Publishes velocity'''
        self.base_velocity_pub.publish(self.velocity_command)

    # publish arm velocity
    def publish_arm_velocities(self):
        ''' Publishes arm velocities, based on desired pose'''
        self.left.jnt_cmd.command = np.multiply(np.subtract(self.left.start_ang,self.left.jt_angles[3:]),self.kp)
        self.right.jnt_cmd.command = np.multiply(np.subtract(self.right.start_ang,self.right.jt_angles[3:]),self.kp)
            

#-----------------------------------------------------------------------------------------------#
# Setup NN control
# NN = NNControl(using_cortex = False)
NN = NNControl()

# Start time
start_time = time.time()
print('Beginning Loop')

# Loop
try:
    while not rospy.is_shutdown():
        # Get cortex velocity
        if NN.cortex == True:
            NN.C2V.calc_velocity(1/NN.hz)
        
        # # Get arm velocity
        # NN.get_arm_velocities()
        
        # Update sensed velocities, filter, and derive for accelerations
        NN.get_base_velocity()
        NN.input_data_generation()
        
        # Predict velocity
        NN.predict_velocity()
        
        # Publish predicted velocities
        NN.publish_velocity()
        
        # Plotting Stuff
        NN.time_array.append(time.time() - start_time)
        if NN.cortex == True:
            NN.object_vel.append(NN.C2V.V_rot[0])
            NN.object_vely.append(NN.C2V.V_rot[1])
            NN.object_velz.append(NN.C2V.W[2])
        else:
            NN.object_vel.append(NN.vive_V.linear.x)
            NN.object_vely.append(NN.vive_V.linear.y)
            NN.object_velz.append(NN.vive_V.angular.z)
        NN.pred_vel.append(NN.pred[NN.pred_steps - 1,0])
        NN.pred_vely.append(NN.pred[NN.pred_steps - 1,1])
        NN.pred_velz.append(NN.pred[NN.pred_steps - 1,2])
        NN.est_vel.append(NN.V.linear.x)
        NN.est_vely.append(NN.V.linear.y)
        NN.est_velz.append(NN.V.angular.z)
        
        # Update counter
        NN.calc_counter += 1
        
        NN.rate.sleep()
        
    # Plot comparisons
    plt.figure()
    plt.plot(NN.time_array,NN.object_vel)
    plt.plot(NN.time_array,NN.pred_vel)
    plt.plot(NN.time_array,NN.est_vel)
    plt.figure()
    plt.plot(NN.time_array,NN.object_vely)
    plt.plot(NN.time_array,NN.pred_vely)
    plt.plot(NN.time_array,NN.est_vely)
    plt.figure()
    plt.plot(NN.time_array,NN.object_velz)
    plt.plot(NN.time_array,NN.pred_velz)
    plt.plot(NN.time_array,NN.est_velz)
    plt.show()

except KeyboardInterrupt:
    pass





