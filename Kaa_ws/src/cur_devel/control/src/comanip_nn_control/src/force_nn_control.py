#!/usr/bin/env python
import tensorflow as tf
import numpy as np
import time,rospy
import matplotlib.pyplot as plt
from vi_control.control_megazord import LimbAttributes, Subscribe2Base
from vi_control.force_manipulation import ForceManip
from scipy import signal
from geometry_msgs.msg import Twist, WrenchStamped
from cortex_to_velocity import Cortex2Velocity
from copy import deepcopy

class NNControl():
    def __init__(self):
        # Initialize node and set loop rate
        rospy.init_node('nn_control', anonymous = True)
        self.hz = 100.0
        self.rate = rospy.Rate(self.hz)
        
        # Get Base tools and Cortex velocity and arm tools
        self.SBB = Subscribe2Base()
        self.C2V = Cortex2Velocity()
        self.left = LimbAttributes('left')
        self.right = LimbAttributes('right')

        # Get Forces
        self.Fl = ForceManip('left', 0.34, 0.17, 0.0, 1.0/self.hz)
        self.Fr = ForceManip('right', 0.34, 0.17, 0.0, 1.0/self.hz)

        # Start base velocity publisher
        self.base_velocity_pub = rospy.Publisher('/des_vel',Twist,queue_size = 10)

        # Base Velocity Subscriber
        self.base_velocity_sub = rospy.Subscriber('/est_vel',Twist,self.est_vel_cb)

        # Filter stuff
        self.b, self.a = signal.butter(2, .06)
        self.zi = signal.lfilter_zi(self.b, self.a)

        # For plotting
        self.time_array = []
        self.cortex_vel = []
        self.cortex_vely = []
        self.cortex_velz = []
        self.pred_vel = []
        self.pred_velz = []
        self.pred_vely = []
        self.est_vel = []
        self.est_vely = []
        self.est_velz = []

        # Velocity
        self.V = Twist()
        self.velocity_command = Twist()
        self.kp = 0.5

        # for now, hard coded
        self.n_steps = 25
        self.n_inputs = 6
        self.for_steps = 5

        # Initialize vectors
        self.vel_data = np.zeros((1,self.n_steps,6))
        self.force_data = np.zeros((1,self.n_steps,6))
        self.filt_vel = np.zeros((1,self.n_steps,12))
        self.input_data = np.zeros((1,self.n_steps,self.n_inputs))
        self.pred = np.zeros((self.for_steps,self.n_inputs))

        # Tools to speed up prediction
        self.calc_counter = 0
        self.which_pred = self.for_steps/2
        self.pred_steps = 3

        # Restore Model
        self.restore_model()
        
    # Velocity Callback
    def est_vel_cb(self,msg):
        self.V = msg

    # For Scaling inputs
    def scale(self,vec,mx,mn):
        ''' scales down input for NN input'''
        vec = np.divide(np.subtract(vec,mn),np.subtract(mx,mn))
        return vec

    def unscale(self,vec,mx,mn):
        '''scales up input for publishing'''
        vec = np.add(np.multiply(vec,np.subtract(mx,mn)),mn)
        return vec

    # Restore Model from File
    def restore_model(self):
        ''' Restores model from files'''
        with tf.device('/cpu:0'):
            self.sess = tf.Session()
            saver = tf.train.import_meta_graph('force.meta')
            saver.restore(self.sess,'force')
            graph = tf.get_default_graph()
            self.xx = graph.get_tensor_by_name('x:0') # This is where you will put inputs
            self.predict = tf.get_collection('prediction')[0] # This gets the prediction
            print('Model Restored')

    # Get Baxter Arm Velocity
    def get_arm_velocities(self):
        ''' Gets arm velocities, jacobians, etc from pykdl'''
        self.left.get_joint_angles()
        self.left.get_joint_velocities()
        self.left.get_current_vel()
        self.right.get_joint_angles()
        self.right.get_joint_velocities()
        self.right.get_current_vel()

    # Get Base Velocity
    def get_base_velocity(self):
        ''' Updates arrays with velocity of base (currently from Cortex), then filters and differentiates to get acceleration as well'''
        # Update sensed velocity data
        self.vel_data = np.roll(self.vel_data,-1,axis = 1)
        self.vel_data[:,-1,0] = self.C2V.V_rot[0]
        self.vel_data[:,-1,1] = self.C2V.V_rot[1]
        self.vel_data[:,-1,2] = self.C2V.V_rot[2]
        self.vel_data[:,-1,3] = self.C2V.W[0]
        self.vel_data[:,-1,4] = self.C2V.W[1]
        self.vel_data[:,-1,5] = self.C2V.W[2]

        # Filter velocity data, get acceleration data
        for j in range(6):
            self.filt_vel[:,:,j] , _ = signal.lfilter(self.b, self.a, self.vel_data[:,:,j][0], zi = self.zi * self.vel_data[:,0,j])
            temp_deriv = np.gradient(self.filt_vel[:,:,j][0]) * self.hz
            self.filt_vel[:,:,j + 6], _ = signal.lfilter(self.b, self.a, temp_deriv, zi = self.zi*temp_deriv[0])
        # for j in xrange(self.n_inputs/2):
        #     self.filt_vel[:,:,j] = self.vel_data[:,:,j][0]
        #     self.filt_vel[:,:,j + self.n_inputs/2] = np.gradient(self.vel_data[:,:,j][0]) * self.hz

        # Scale
        self.filt_vel[:,:,0] = self.scale(self.filt_vel[:,:,0],0.3,-0.3)
        self.filt_vel[:,:,1] = self.scale(self.filt_vel[:,:,1],0.5,-0.5)
        self.filt_vel[:,:,2] = self.scale(self.filt_vel[:,:,2],1.0,-1.0)
        self.filt_vel[:,:,3] = self.scale(self.filt_vel[:,:,3],0.3,-0.3)
        self.filt_vel[:,:,4] = self.scale(self.filt_vel[:,:,4],0.6,-0.6)
        self.filt_vel[:,:,5] = self.scale(self.filt_vel[:,:,5],0.5,-0.5)
        self.filt_vel[:,:,6] = self.scale(self.filt_vel[:,:,6],0.5,-0.5)
        self.filt_vel[:,:,7] = self.scale(self.filt_vel[:,:,7],0.5,-0.5)
        self.filt_vel[:,:,8] = self.scale(self.filt_vel[:,:,8],1.5,-1.5)
        self.filt_vel[:,:,9] = self.scale(self.filt_vel[:,:,9],0.5,-0.5)
        self.filt_vel[:,:,10] = self.scale(self.filt_vel[:,:,10],1.3,-1.3)
        self.filt_vel[:,:,11] = self.scale(self.filt_vel[:,:,11],0.9,-0.9)

    # Stack Forces/Torques
    def stack_ft(self):
        ''' Stacks force data into force_data array and scales it'''
        # Update Force Data
        self.force_data = np.roll(self.force_data, -1, axis = 1)
        self.force_data[:,-1,0] = self.Fl.F[0] + self.Fr.F[0]
        self.force_data[:,-1,1] = self.Fl.F[3] + self.Fr.F[3]
        self.force_data[:,-1,2] = self.Fl.F[5] + self.Fr.F[5]
        self.force_data[:,-1,3] = self.Fl.dF[0] + self.Fr.dF[0]
        self.force_data[:,-1,4] = self.Fl.dF[3] + self.Fr.dF[3]
        self.force_data[:,-1,5] = self.Fl.dF[5] + self.Fr.dF[5]
        # Scale
        self.force_data[:,:,0] = self.scale(self.force_data[:,:,0],30.0,-30.0)
        self.force_data[:,:,1] = self.scale(self.force_data[:,:,1],10.0,-10.0)
        self.force_data[:,:,2] = self.scale(self.force_data[:,:,2],30.0,-30.0)
        self.force_data[:,:,3] = self.scale(self.force_data[:,:,3],35.0,-35.0)
        self.force_data[:,:,4] = self.scale(self.force_data[:,:,4],17.0,-17.0)
        self.force_data[:,:,5] = self.scale(self.force_data[:,:,5],48.0,-48.0)
        
    def input_data_generation(self):
        ''' Puts appropriate section of data into input_data array for use in prediction'''
        # Put into appropriate part of input data
        self.input_data[:,:,:3] = self.force_data[:,:,:3]
        # self.input_data[:,:,9:12] = self.force_data[:,:,3:]

        # Put into appropriate part of input_data
        # self.input_data[:,:,3:self.n_inputs] = self.filt_vel[:,:,:6]
        # self.input_data[:,:,12:] = self.filt_vel[:,:,6:]
        self.input_data[:,:,3] = self.filt_vel[:,:,0]
        self.input_data[:,:,4] = self.filt_vel[:,:,1]
        self.input_data[:,:,5] = self.filt_vel[:,:,5]


    # Use NN for Predicting future velocities
    def predict_velocity(self):
        '''Predicts the velocity to command '''
        with tf.device('/cpu:0'): # run on cpu
            if self.calc_counter > self.which_pred: # run only every self.which_pred times through
                for j in range(self.pred_steps): # only get self.pred_steps predictions
                    feed_dict = {self.xx: self.input_data} # input data into feed_dict
                    self.pred[j,:] = self.sess.run(self.predict,feed_dict) # run nn prediction
                    self.input_data = np.roll(self.input_data, -1, axis = 1) # roll back
                    self.input_data[:,-1,:] = self.pred[j,:] # replace latest entry

                # Scale up velocities for publishing
                self.pred[j,0] = self.unscale(self.pred[j,0],30.0,-30.0)
                self.pred[j,1] = self.unscale(self.pred[j,1],10.0,-10.0)
                self.pred[j,2] = self.unscale(self.pred[j,2],30.0,-30.0)
                self.pred[j,3] = self.unscale(self.pred[j,3],0.3,-0.3)
                self.pred[j,4] = self.unscale(self.pred[j,4],0.5,-0.5)
                self.pred[j,5] = self.unscale(self.pred[j,5],0.5,-0.5)

                # apply predicted velocities    
                if abs(self.pred[j,3]) < 0.05:
                    self.velocity_command.linear.x = 0.0
                else:
                    self.velocity_command.linear.x = self.pred[j,3]
                if abs(self.pred[j,4]) < 0.05:
                    self.velocity_command.linear.y = 0.0
                else:
                    self.velocity_command.linear.y = self.pred[j,4]
                if abs(self.pred[j,5]) < 0.05:
                    self.velocity_command.angular.z = 0.0
                else:
                    self.velocity_command.angular.z = self.pred[j,5]



                # Reset Counter
                self.calc_counter = 0

    # Publish Velocity to Base
    def publish_velocity(self):
        ''' Publishes velocity'''
        self.base_velocity_pub.publish(self.velocity_command)

    def publish_arm_velocities(self):
        ''' Publishes arm velocities, based on desired pose'''
        self.left.jnt_cmd.command = np.multiply(np.subtract(self.left.start_ang,self.left.jt_angles[3:]),self.kp)
        self.right.jnt_cmd.command = np.multiply(np.subtract(self.right.start_ang,self.right.jt_angles[3:]),self.kp)

# Setup NN control
NN = NNControl()

# Start time
start_time = time.time()
print('Beginning Loop')

# Loop
try:
    while not rospy.is_shutdown():
        # Get cortex velocity
        NN.C2V.calc_velocity(1/NN.hz)
        
        # Get arm velocity
        # NN.get_arm_velocities()
        
        # Update sensed velocities, filter, and derive for accelerations, put into input vector
        NN.get_base_velocity()
        NN.stack_ft()
        NN.input_data_generation()
        
        # Predict velocity
        NN.predict_velocity()
        
        # Publish predicted velocities
        NN.publish_velocity()
        
        # Plotting Stuff
        NN.time_array.append(time.time() - start_time)
        NN.cortex_vel.append(NN.C2V.V_rot[0])
        NN.cortex_vely.append(NN.C2V.V_rot[1])
        NN.cortex_velz.append(NN.C2V.W[2])
        NN.pred_vel.append(NN.pred[NN.pred_steps - 1,3])
        NN.pred_vely.append(NN.pred[NN.pred_steps - 1,4])
        NN.pred_velz.append(NN.pred[NN.pred_steps - 1,5])
        NN.est_vel.append(NN.V.linear.x)
        NN.est_vely.append(NN.V.linear.y)
        NN.est_velz.append(NN.V.angular.z)
        
        # Update counter
        NN.calc_counter += 1
        
        NN.rate.sleep()
        
    # Plot comparisons
    plt.figure()
    plt.plot(NN.time_array,NN.cortex_vel)
    plt.plot(NN.time_array,NN.pred_vel)
    plt.plot(NN.time_array,NN.est_vel)
    plt.figure()
    plt.plot(NN.time_array,NN.cortex_vely)
    plt.plot(NN.time_array,NN.pred_vely)
    plt.plot(NN.time_array,NN.est_vely)
    plt.figure()
    plt.plot(NN.time_array,NN.cortex_velz)
    plt.plot(NN.time_array,NN.pred_velz)
    plt.plot(NN.time_array,NN.est_velz)
    plt.show()

except KeyboardInterrupt:
    pass





