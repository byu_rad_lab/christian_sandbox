#!/usr/bin/env python

import rospy, time
import tf.transformations as tft
import numpy as np
from geometry_msgs.msg import Twist,PoseStamped
from scipy import signal

class Cortex2Velocity():
    def __init__(self):
        # self.sub = rospy.Subscriber('/Hand/Hand',PoseStamped,self.pose_callback)
        self.sub = rospy.Subscriber('/Table/Table',PoseStamped,self.pose_callback)
        # self.sub = rospy.Subscriber('/SmallTable/SmallTable',PoseStamped,self.pose_callback)
        self.sub2 = rospy.Subscriber('/AMPI/AMPI',PoseStamped,self.base_callback)
        self.pub = rospy.Publisher('hand_velocity',Twist,queue_size = 10)
        self.P = []
        self.Pbase = []
        self.V_rot = [0.0,0.0,0.0]
        self.O = []
        self.Obase = []
        self.R = []
        self.V = [0.0,0.0,0.0]
        self.Vbase = [0.0,0.0,0.0]
        self.W = [0.0,0.0,0.0]
        self.num_to_keep = 10
        self.b,self.a = signal.butter(2,0.03)
        self.zi = signal.lfilter_zi(self.b,self.a)
        self.tw = Twist()

    def pose_callback(self,data):
        if abs(data.pose.position.x) < 5.0:
            if len(self.P) <= self.num_to_keep:
                self.P.append([data.pose.position.x,data.pose.position.y,data.pose.position.z])
                self.O.append([data.pose.orientation.x,data.pose.orientation.y,data.pose.orientation.z,data.pose.orientation.w])
            else:
                self.P = np.roll(self.P,-3)
                self.P[self.num_to_keep - 1] = [data.pose.position.x,data.pose.position.y,data.pose.position.z]
                self.O = np.roll(self.O,-4)
                self.O[self.num_to_keep - 1] = [data.pose.orientation.x,data.pose.orientation.y,data.pose.orientation.z,data.pose.orientation.w]
        else:
            if len(self.P) <= self.num_to_keep:
                if len(self.P) == 0:
                    self.P.append([0.0,0.0,0.0])
                    self.O.append([0.0,0.0,0.0,1.0])
                else:
                    self.P.append(self.P[-1])
                    self.O.append(self.O[-1])
            else:
                self.P = np.roll(self.P,-3)
                self.P[self.num_to_keep - 1] = self.P[-2]
                self.O = np.roll(self.O,-4)
                self.O[self.num_to_keep - 1] = self.O[-2]

    def base_callback(self,data):
        if abs(data.pose.position.x) < 5.0:
            if len(self.Pbase) <= self.num_to_keep:
                self.Pbase.append([data.pose.position.x,data.pose.position.y,data.pose.position.z])
                self.Obase.append([data.pose.orientation.x,data.pose.orientation.y,data.pose.orientation.z,data.pose.orientation.w])
            else:
                self.Pbase = np.roll(self.Pbase,-3)
                self.Pbase[self.num_to_keep - 1] = [data.pose.position.x,data.pose.position.y,data.pose.position.z]
                self.Obase = np.roll(self.Obase,-4)
                self.Obase[self.num_to_keep - 1] = [data.pose.orientation.x,data.pose.orientation.y,data.pose.orientation.z,data.pose.orientation.w]
        else:
            if len(self.Pbase) <= self.num_to_keep:
                if len(self.Pbase) == 0:
                    self.Pbase.append([0.0,0.0,0.0])
                    self.Obase.append([0.0,0.0,0.0,1.0])
                else:
                    self.Pbase.append(self.P[-1])
                    self.Obase.append(self.O[-1])
            else:
                self.Pbase = np.roll(self.Pbase,-3)
                self.Pbase[self.num_to_keep - 1] = self.Pbase[-2]
                self.Obase = np.roll(self.Obase,-4)
                self.Obase[self.num_to_keep - 1] = self.Obase[-2]


    def rotate_to_hand_frame(self):
        # if backwards check here.....
        self.R = tft.inverse_matrix(tft.quaternion_matrix(self.O[-1]))[:3,:3]
        self.V_rot = self.R.dot(self.V)

    def all_rotations(self):
        euler = list(tft.euler_from_quaternion((tft.quaternion_inverse(self.O[-1])),axes = 'rxyz'))
        R1 = tft.euler_matrix(euler[0],-euler[1],-euler[2],'rxyz')[:3,:3]
        R2 = tft.euler_matrix(0,-euler[1],-euler[2],'rxyz')[:3,:3]
        R3 = tft.euler_matrix(0,0,-euler[2],'rxyz')[:3,:3]
        return R1,R2,R3

    def calc_ang_velocity(self,dt):
        th = []
        thdot = []
        R1,R2,R3 = self.all_rotations()
        if len(self.O) > 1:
            for i in range(len(self.O)):
                th.append(np.asarray(tft.euler_from_quaternion(self.O[i])).tolist())
            th = np.transpose(th)
            for i in range(3):
                temp_thdot, _ = signal.lfilter(self.b, self.a, th[i,:], zi = self.zi * th[i,0])
                # if backwards, check here.....
                thdot.append((temp_thdot[-1] - temp_thdot[-2])/dt)
            w1 = R1.dot([thdot[0],0,0]).tolist()
            w2 = R2.dot([0,thdot[1],0]).tolist()
            w3 = R3.dot([0,0,thdot[2]]).tolist()
            w = np.add(w1,np.add(w2,w3)).tolist()

            self.W = [w[0],w[1],w[2]]
        else:
            self.W = [0.0,0.0,0.0]

    def calc_velocity(self,dt):
        if len(self.P) <= self.num_to_keep:
            self.V = [0.0,0.0,0.0]
        else:
            for i in range(3):
                temp_p = np.transpose(self.P)[i].tolist()
                temp_filt,_ = signal.lfilter(self.b,self.a,temp_p,zi = self.zi*temp_p[0])
                self.V[i] = (temp_filt[-1] - temp_filt[-2])/dt
            # self.V = np.divide(np.divide(np.subtract(self.P[-1],self.P[-2]),dt),-4.0)
        if len(self.Pbase) <= self.num_to_keep:
            self.Vbase = [0.0,0.0,0.0]
        else:
            for i in range(3):
                temp_p = np.transpose(self.Pbase)[i].tolist()
                temp_filt,_ = signal.lfilter(self.b,self.a,temp_p,zi = self.zi*temp_p[0])
                self.Vbase[i] = (temp_filt[-1] - temp_filt[-2])/dt
            # self.Vbase = np.divide(np.divide(np.subtract(self.Pbase[-1],self.Pbase[-2]),dt),4.0)
        self.rotate_to_hand_frame()
        self.calc_ang_velocity(dt)
        
        # self.tw.linear.x = self.V_rot[0]
        # self.tw.linear.y = self.V_rot[1]
        # self.tw.linear.z = self.V_rot[2]

        # self.pub.publish(self.tw)

