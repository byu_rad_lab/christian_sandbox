#!/usr/bin/env python
import numpy as np
import tf.transformations as tft
from geometry_msgs.msg import Twist
from numpy import matrix as mat
from tf2_msgs.msg import TFMessage
from scipy import signal
import sys, rospy, time
import matplotlib.pyplot as plt

class ViveTrackerVel():
    def __init__(self,tracker_id):
        # Define Publisher
        topic = str(tracker_id) + '_vel'
        self.pub = rospy.Publisher(topic,Twist,queue_size = 10)
            
        # Subscribe to transforms
        rospy.Subscriber('tf', TFMessage, self.vive_callback)

        # Remember tracker_id name
        self.tracker_id = tracker_id

        # Iniitalize Variables needed
        self.R = np.zeros((3,3)) # Rotation Matrix
        self.t = np.zeros((3,1)) # Global Frame Position
        self.T = np.zeros((3,1)) # Local Frame Position
        self.P = [] # Local Frame Position History
        self.euler = [] # Euler Angles History
        self.euler_dot = [0,0] * 3 # Euler Angle Rates
        self.V = [0.0] * 3 # Local Frame Velocity
        self.W = [0.0] * 3 # Angular Velocity

        # Publisher Messages
        self.tw = Twist()

        # Filter Information
        self.b,self.a = signal.butter(2,0.00055) 
        self.zi = signal.lfilter_zi(self.b,self.a)
        self.num_to_filt = 300

    def vive_callback(self, msg): 
        # Locate wrt world_vive frame
        if msg.transforms[0].header.frame_id == "world_vive":
            if msg.transforms[0].child_frame_id == self.tracker_id:
                # Calculate translation and rotations
                t = msg.transforms[0].transform
                q = [t.rotation.x, t.rotation.y, t.rotation.z, t.rotation.w]
                self.R = tft.quaternion_matrix(q)[0:3,0:3]
                self.t = [t.translation.x,t.translation.y,t.translation.z]
                                        
                # Rotate to local frame
                self.T = np.dot(np.transpose(self.R),(self.t))

                # Update History
                if len(self.P) <= self.num_to_filt:
                    self.P.append(self.T)
                    self.euler.append(list(tft.euler_from_quaternion(q,axes='rzyx')))
                else:
                    self.P = np.roll(self.P,-1,axis = 0)
                    self.P[-1] = self.T
                    self.euler = np.roll(self.euler,-1,axis = 0)
                    self.euler[-1] = list(tft.euler_from_quaternion(q,axes='rzyx'))

    def calc_vel(self,dt):
        # If History large enough
        if len(self.P) >= self.num_to_filt:
            # Loop for X,Y,Z
            for i in range(3):
                # Calculate Velocities
                temp_p = np.transpose(self.P)[i].tolist()
                temp_filt, _ = signal.lfilter(self.b, self.a, temp_p, zi = self.zi*temp_p[0])
                self.V[i] = (temp_filt[-1] - temp_filt[-2])/(dt)

                # Calculate euler angle rates
                temp_eul = np.transpose(self.euler)[i].tolist()
                temp_filt, _ = signal.lfilter(self.b, self.a, temp_eul, zi = self.zi*temp_eul[0])
                self.euler_dot[i] = (temp_filt[-1] - temp_filt[-2])/(dt)
                        
                # Calculate Angular Velocities using 3-2-1 standard
                self.W[0] = -self.euler_dot[0] * np.sin(self.euler[-1][1]) + self.euler_dot[2]
                self.W[1] = self.euler_dot[0] * np.cos(self.euler[-1][1]) * np.sin(self.euler[-1][2]) + self.euler_dot[1] * np.cos(self.euler[-1][2])
                self.W[2] = self.euler_dot[0] * np.cos(self.euler[-1][1]) * np.cos(self.euler[-1][2]) - self.euler_dot[1] * np.sin(self.euler[-1][2])

            self.tw.linear.x = self.V[0]
            self.tw.linear.y = self.V[1]
            self.tw.linear.z = self.V[2]
            self.tw.angular.x = self.W[0]
            self.tw.angular.y = self.W[1]
            self.tw.angular.z = self.W[2]

			
if __name__=='__main__':
    rospy.init_node('tracker_vel',anonymous = True)
    if len(sys.argv[0]) > 1:
        tracker = sys.argv[1]
    V2V = ViveTrackerVel(tracker)
    hz = 2000.0
    rate = rospy.Rate(hz)
    print('Publishing',sys.argv[1],'Velocity(s)')

    # start = time.time()
    # t = []
    # v = []
    while not rospy.is_shutdown():
        V2V.calc_vel(1.0/hz)
        V2V.pub.publish(V2V.tw)
        # t.append(time.time()-start)
        # v.append(V2V.tw.linear.x)
        rate.sleep()

    # plt.plot(t,v)
    # plt.show()
