import csv  
import numpy
import scipy.io as sio
import matplotlib.pyplot as plt
import time
import sys

if __name__ == "__main__":
    n = 7
    if(len(sys.argv) > 1):
        n = eval(sys.argv[1])

    data = [ ]
    with open('baxter_lempc_data.txt') as f:
        reader = csv.reader(f)
        for row in reader:
            rowData = [ float(elem) for elem in row ]
            data.append(rowData)

    matrix = numpy.array(data)
    x = matrix[:,0:n*2]
    u = matrix[:,n*2:n*3]
    solve_times = matrix[:,n*3]
    t = matrix[:,n*3+1]    
    sio.savemat('baxter_lempc_data',
                {'x':x,
                 'u':u,
                 't':t,
                 'solve_times':solve_times})

    plt.figure(1)
    plt.plot(matrix[:,0:n])
    plt.ion()
    plt.title('Velocities')
    plt.show()

    plt.figure(2)
    plt.plot(matrix[:,n:n*2])
    plt.ion()
    plt.title('Positions')    
    plt.show()
    
    plt.figure(3)
    plt.plot(matrix[:,n*2:n*3])
    plt.ion()
    plt.title('Inputs')        
    plt.show()
    
    # plt.figure(4)
    # plt.plot(matrix[:,n*4:n*6])
    # plt.ion()
    # plt.show()

    plt.pause(1100)
    

    
