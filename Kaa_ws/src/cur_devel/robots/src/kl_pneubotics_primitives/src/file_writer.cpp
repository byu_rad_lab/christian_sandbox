/*
 * @file    file_writer.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Provides basic low level file writing functionality.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "kl_pneubotics_primitives/file_writer.h"
#include "kl_pneubotics_primitives/exception.h"

#include <boost/filesystem.hpp>

#include <string>
#include <iostream>
#include <fstream>

namespace pneubotics
{
namespace file_io
{
FileWriter::FileWriter() :
        Object(), ofstream_(NULL), openmode_(std::ios::out | std::ios::trunc), filepath_("./")
{
}

FileWriter::FileWriter(const std::string &name) :
        Object(name), ofstream_(NULL), openmode_(std::ios::out | std::ios::trunc), filepath_("./")
{
    ofstream_ = new std::ofstream;
}

FileWriter::FileWriter(const std::string &name, const std::ios::openmode openmode) :
        Object(name), ofstream_(NULL), openmode_(openmode), filepath_("./")
{
    ofstream_ = new std::ofstream;
}

FileWriter::FileWriter(const std::string &name, const std::string &filepath) :
        Object(name), ofstream_(NULL), openmode_(std::ios::out | std::ios::trunc), filepath_(filepath)
{
    ofstream_ = new std::ofstream;
}

FileWriter::FileWriter(const std::string &name, const std::ios::openmode openmode, const std::string &filepath) :
        Object(name), ofstream_(NULL), openmode_(openmode), filepath_(filepath)
{
    ofstream_ = new std::ofstream;
}

FileWriter::~FileWriter()
{
    if (is_open())
    {
        close();
    }
    if (ofstream_)
    {
        delete ofstream_;
    }
}

void FileWriter::openmodeSet(const std::ios::openmode openmode)
{
    openmode_ = openmode;
}

std::ios::openmode FileWriter::getOpenMode(void) const
        {
    return openmode_;
}

void FileWriter::setFilePath(const std::string &filepath)
{
    filepath_ = filepath;
}

std::string FileWriter::getFilePath(void) const
        {
    return filepath_;
}

void FileWriter::open(const std::string &name)
{
    setName(name);
    open();
}

void FileWriter::open(void)
{
    boost::filesystem::path dir(filepath_ + "/");
    boost::filesystem::path dir2 = boost::filesystem::canonical(dir);
    if (!boost::filesystem::exists(dir))
    {
        if (!boost::filesystem::create_directory(dir))
        {
            throw exception::FileException("Failed to open file. Could not create path: " + filepath_);
        }
    }
    ofstream_->open((filepath_ + "/" + name_).c_str(), std::ios::app);
    if (!ofstream_->is_open())
    {
        throw exception::FileException(
                "Failed to open file. Path or file name may be invalid: " + filepath_ + "/" + name_);
    }
}

void FileWriter::open(const std::string &name, const std::ios::openmode openmode)
{
    openmodeSet(openmode);
    setName(name);
    open();
}

void FileWriter::open(const std::ios::openmode openmode)
{
    openmodeSet(openmode);
    open();
}

void FileWriter::close(void)
{
    if (is_open())
    {
        ofstream_->close();
    }
}

bool FileWriter::is_open(void)
{
    return ofstream_->is_open();
}

void FileWriter::write(const std::string &msg)
{
    if (!is_open())
    {
        throw exception::FileException("Cannot write to file. File not open.");
    }
    (*ofstream_) << msg;
}

void FileWriter::write(const std::string &msg, bool addLineEnding)
{
    if (!is_open())
    {
        throw exception::FileException("Cannot write to file. File not open.");
    }
    (*ofstream_) << msg;

    if (addLineEnding)
    {
        (*ofstream_) << std::endl;
    }
}

}  // namespace file_io
}  // namespace pneubotics
