/*
 * @file    loop.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Provides ability to create a new thread for executing some work on a periodic basis
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "kl_pneubotics_primitives/loop.h"

namespace pneubotics
{
namespace loop
{
Loop::Loop() :
        thread::Thread("Loop"), time_(time::Time::now()), timeDiff_(time::Duration()), dataLogger_(), logging_(false), period_(
                time::Duration()), timeNext_(time_)
{
}

Loop::Loop(const std::string &name) :
        thread::Thread("name"), time_(time::Time::now()), timeDiff_(time::Duration()), dataLogger_(), logging_(false), period_(
                time::Duration()), timeNext_(time_)
{
}

Loop::Loop(const time::Duration &period) :
        thread::Thread("Loop"), time_(time::Time::now()), timeDiff_(time::Duration()), dataLogger_(), logging_(false), period_(
                period), timeNext_(time_ + period_)
{
}

Loop::Loop(const std::string &name, const time::Duration &period) :
        thread::Thread(name), time_(time::Time::now()), timeDiff_(time::Duration()), dataLogger_(), logging_(false), period_(
                period), timeNext_(time_ + period_)
{
}

Loop::Loop(thread::thread_prio_t prio) :
        thread::Thread("Loop", prio), time_(time::Time::now()), timeDiff_(time::Duration()), dataLogger_(), logging_(
                false), period_(
                time::Duration()), timeNext_(time_)
{
}

Loop::Loop(const std::string &name, thread::thread_prio_t prio) :
        thread::Thread("name", prio), time_(time::Time::now()), timeDiff_(time::Duration()), dataLogger_(), logging_(
                false), period_(
                time::Duration()), timeNext_(time_)
{
}

Loop::Loop(const time::Duration &period, thread::thread_prio_t prio) :
        thread::Thread("Loop", prio), time_(time::Time::now()), timeDiff_(time::Duration()), dataLogger_(), logging_(
                false), period_(
                period), timeNext_(time_ + period_)
{
}

Loop::Loop(const std::string &name, const time::Duration &period, thread::thread_prio_t prio) :
        thread::Thread(name, prio), time_(time::Time::now()), timeDiff_(time::Duration()), dataLogger_(), logging_(
                false), period_(
                period), timeNext_(time_ + period_)
{
}

Loop::~Loop()
{
}

void Loop::setLogging(const bool ena)
{
    logging_ = ena;
}

bool Loop::getLogging(void)
{
    return logging_;
}

void Loop::ownWork(void)
{
    /*
     * Calculate loop timing
     */
    timeDiff_ = time::Time::now() - time_;
    time_ = time::Time::now();
    timeNext_ = time_ + period_;

    /*
     * Do all loop work
     */
    doOwnWorkPre();
    doOwnWork();
    doOwnWorkPost();

    /*
     * Do data logging
     */
    if (logging_)
    {
        doLogging();
    }
}

void *Loop::mainLoop(void)
{
    setPriority();

    do
    {
        /*
         * Do your own work
         */
        ownWork();

        /*
         * Do work for any other loops that have been bound to this one
         */
        for (std::vector<void *>::iterator it = boundThreads_.begin(); it != boundThreads_.end(); ++it)
        {
            boundWorkCaller(*it);
        }

        /*
         * Sleep until next loop iteration
         */
        time::Time now(time::Time::now());
        if (!period_.isZero())
        {
            /*
             * If now is after the next scheduled time we should run then we overran our alloted loop execution time
             */
            if (timeNext_ > now)
            {
                time::Time::sleepUntil(timeNext_);
            }
            else
            {
                std::stringstream ss;
                //ss << name_ << " overran loop time by " << (now - time_ - period_).toUSec() << "us, now = " << now << " scheduled time = " << timeNext_;
                ss << name_ << " overran loop time by " << (now - timeNext_).toUSec() << "us, now = " << now
                        << ", scheduled time = " << timeNext_;
                warn(ss.str());
            }
        }
    }
    while (running_);

    return 0;
}

}  // namespace loop
}  // namespace pneubotics
