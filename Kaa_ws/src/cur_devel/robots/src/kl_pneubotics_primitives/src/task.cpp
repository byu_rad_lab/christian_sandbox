/*
 * @file    task.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Some unit of work to be done
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "kl_pneubotics_primitives/task.h"
#include <iostream>

namespace pneubotics
{
namespace task
{
Task::Task() :
        Object("Task"), initialized_(false)
{
}

Task::Task(const std::string &name) :
        Object(name), initialized_(false)
{
}

Task::~Task(void)
{

}

void Task::init(void)
{
    doInit();

    initialized_ = true;
}

void Task::run(void)
{
    if (!initialized_)
    {
        throw exception::TaskInitException("Cannot run " + name_ + " instance of Task object before initialization");
    }

    doRun();
}

bool Task::isInitialized(void) const
        {
    return initialized_;
}

}  // namespace task
}  // namespace pneubotics
