/*
 * @file    mutex.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Mutex for shared resource protection
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "kl_pneubotics_primitives/mutex.h"

namespace pneubotics
{
namespace mutex
{

Mutex::Mutex()
{
    if (pthread_mutex_init(&mutexHandle_, NULL))
    {
        throw exception::MutexException("Failed to initialize mutex");
    }
}

Mutex::~Mutex()
{
    if (pthread_mutex_destroy (&mutexHandle_))
    {
        throw exception::MutexException("Failed to destroy mutex");
    }
}

void Mutex::lock(void)
{
    pthread_mutex_lock (&mutexHandle_);
}

void Mutex::unlock(void)
{
    pthread_mutex_unlock (&mutexHandle_);
}

}  // namespace mutex
}  // namespace pneubotics
