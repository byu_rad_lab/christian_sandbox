/*
 * @file    data_logger.c
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Provides basic functionality for logging data to a file.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "kl_pneubotics_primitives/data_logger.h"

#include <algorithm>

namespace pneubotics
{
namespace file_io
{

LoggableObject::LoggableObject() :
        logPath_("/data")
{
}

LoggableObject::LoggableObject(const std::string &logPath) :
        logPath_(logPath)
{
}

LoggableObject::LoggableObject(const std::string &logName, const std::string &logPath) :
        Object(logName), logPath_(logPath)
{
}

LoggableObject::~LoggableObject()
{
}

std::string LoggableObject::getPath(void) const
        {
    return logPath_;
}

DataLogger::DataLogger()
{
    fileWriter_ = NULL;
}

std::string DataLogger::getLogFilePath(void) const
        {
    if (fileWriter_)
    {
        return fileWriter_->getFilePath();
    }
    return "";
}

DataLogger::~DataLogger()
{
    if (fileWriter_)
    {
        if (fileWriter_->is_open())
        {
            fileWriter_->close();
        }
        delete fileWriter_;
    }
}

void DataLogger::end(void)
{
    fileWriterLock_.lock();
    if (fileWriter_)
    {
        if (fileWriter_->is_open())
        {
            fileWriter_->close();
        }
        delete fileWriter_;
        fileWriter_ = NULL;
    }
    fileWriterLock_.unlock();
}

void DataLogger::init(const LoggableObject &loggableObject)
{
    fileWriterLock_.lock();
    if (fileWriter_)
    {
        if (fileWriter_->is_open())
        {
            fileWriter_->close();
        }
        delete fileWriter_;
        fileWriter_ = NULL;
    }

    startDateTime_ = time::WallTime::isoDateTimeLocal();

    fileWriter_ = new FileWriter(startDateTime_ + loggableObject.getName() + ".log", std::ios::trunc,
            loggableObject.getPath());

    if (fileWriter_)
    {
        fileWriter_->open();

        fileWriter_->write("# Pneubotics Data Log", true);
        fileWriter_->write("# Start Time: " + startDateTime_, true);
        fileWriter_->write("# Column Definitions:", true);
        fileWriter_->write(loggableObject.getColumnDefinitions(0), true);
        fileWriter_->write(loggableObject.getColumnHeaders(), true);
    }
    else
    {
        fileWriterLock_.unlock();
        throw pneubotics::exception::FileException("Data logger failed to create instance of file writer");
    }
    fileWriterLock_.unlock();
}

void DataLogger::writeLogEntry(const LoggableObject &loggableObject)
{
    fileWriterLock_.lock();
    if (fileWriter_)
    {
        if (fileWriter_->is_open())
        {
            fileWriter_->write(loggableObject.getColumnData(), true);
        }
    }
    else
    {
        fileWriterLock_.unlock();
        throw pneubotics::exception::FileException("Data logger attempting to write data before initialization");
    }
    fileWriterLock_.unlock();
}

}  // namespace file_io
}  // namespace pneubotics
