/*
 * @file    duration.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Class for representing time durations
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "kl_pneubotics_primitives/duration.h"

#include <ostream>
#include <string>
#include <string>

namespace pneubotics
{
namespace time
{

std::ostream& operator<<(std::ostream& os, const Duration& rhs)
{
    boost::io::ios_all_saver s(os);
    os << rhs.rep_;
    return os;
}

}  // namespace time
}  // namespace pneubotics
