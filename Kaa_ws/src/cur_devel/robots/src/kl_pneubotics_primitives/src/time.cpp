/*
 * @file    time.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Class for representing points in time
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "kl_pneubotics_primitives/time.h"

#include <boost/date_time/posix_time/posix_time.hpp>

#include <locale>
#include <sstream>
#include <ostream>
#include <string>

namespace pneubotics
{
namespace time
{

const std::string WallTime::dateTimeDelim_ = "T";

std::ostream& operator<<(std::ostream& os, const Time& rhs)
{
    boost::io::ios_all_saver s(os);
    os << rhs.rep_;
    return os;
}

WallTime::WallTime()
{
}

WallTime::~WallTime()
{
}

std::string WallTime::isoTimeLocal(void)
{
    std::string dateTime = boost::posix_time::to_iso_string(boost::posix_time::microsec_clock::local_time());
    size_t pos = dateTime.find(dateTimeDelim_);
    std::string time = dateTime.substr(pos + dateTimeDelim_.length(),
            dateTime.length() - pos - dateTimeDelim_.length());

    return time;
}

std::string WallTime::isoTimeUTC(void)
{
    std::string dateTime = boost::posix_time::to_iso_string(boost::posix_time::microsec_clock::universal_time());
    size_t pos = dateTime.find(dateTimeDelim_);
    std::string time = dateTime.substr(pos + dateTimeDelim_.length(),
            dateTime.length() - pos - dateTimeDelim_.length());

    return time;
}

std::string WallTime::isoDateLocal(void)
{
    std::string dateTime = boost::posix_time::to_iso_string(boost::posix_time::microsec_clock::local_time());
    size_t pos = dateTime.find(dateTimeDelim_);
    std::string date = dateTime.substr(0, pos);

    return date;
}

std::string WallTime::isoDateUTC(void)
{
    std::string dateTime = boost::posix_time::to_iso_string(boost::posix_time::microsec_clock::universal_time());
    size_t pos = dateTime.find(dateTimeDelim_);
    std::string date = dateTime.substr(0, pos);

    return date;
}

std::string WallTime::isoDateTimeLocal(void)
{
    return boost::posix_time::to_iso_string(boost::posix_time::microsec_clock::local_time());
}

std::string WallTime::isoDateTimeUTC(void)
{
    return boost::posix_time::to_iso_string(boost::posix_time::microsec_clock::universal_time());
}

}  // namespace time
}  // namespace pneubotics

