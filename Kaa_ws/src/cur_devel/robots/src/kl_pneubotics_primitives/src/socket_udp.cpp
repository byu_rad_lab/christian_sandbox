/*
 * @file    socket_udp.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Provides basic low level utilities for reading writing UPD packets.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "kl_pneubotics_primitives/socket_udp.h"

#include "kl_pneubotics_primitives/socket_exception.h"

#include <boost/bind.hpp>
#include <iostream>

namespace pneubotics
{
namespace socket
{

SocketUdp::SocketUdp(void) :
        socket_(ioService_), open_(false), port_(-1), timer_(ioService_), bytesTransferred_(0),
                result_(RESULT_ERROR)
{
    ip_ = boost::asio::ip::address_v4::from_string("", lastError_);
}

SocketUdp::~SocketUdp(void)
{
    close();
}

void SocketUdp::open(std::string host, int port)
{
    ip_ = boost::asio::ip::address_v4::from_string(host, lastError_);
    port_ = port;

    if (lastError_ != boost::system::errc::success)
    {
        throw exception::SocketBindException("Failed to open socket: " + lastError_.message());
    }

    endpoint_.address(ip_);
    endpoint_.port(port_);

    socket_.open(endpoint_.protocol(), lastError_);

    if (lastError_ != boost::system::errc::success)
    {
        throw exception::SocketBindException("Failed to open socket: " + lastError_.message());
    }

    open_ = true;
}

void SocketUdp::close(void)
{
    socket_.close(lastError_);

    if (lastError_ != boost::system::errc::success)
    {
        throw exception::SocketCloseException("Failed to close socket: " + lastError_.message());
    }

    open_ = false;
}

int SocketUdp::recv(char *pData, int size)
{
    return recv(pData, size, 0);
}

int SocketUdp::recv(char *pData, int size, int timeoutUs)
{
    if (!open_)
    {
        throw exception::SocketRecvException("Socket not open");
    }

    boost::posix_time::time_duration timeout = boost::posix_time::microsec(timeoutUs);

    // Pull out any data already in the buffer
    if (rxBuffer_.size() > 0)
    {
        std::istream is(&rxBuffer_);
        // Read everything in the buffer or 'size', whichever is less
        size_t toRead = ((rxBuffer_.size() < (size_t) size) ? (rxBuffer_.size()) : (size));  //How many bytes to read?
        is.read(pData, toRead);
        pData += toRead;
        size -= toRead;

        if (size == 0)
        {
            return size;  //If read data was enough, just return
        }
    }

    // Setup async read
    socket_.async_receive_from(boost::asio::buffer(pData, size), endpoint_,
            boost::bind(&SocketUdp::readComplete, this,  //shared_from_this(),
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));

    // Zero timeout is interpreted as requesting a blocking read
    if (timeout != boost::posix_time::seconds(0))
    {
        timer_.expires_from_now(timeout);
    }
    else
    {
        timer_.expires_from_now(boost::posix_time::hours(100000));
    }

    // Setup timeout callback
    timer_.async_wait(
            boost::bind(&SocketUdp::readTimedOut, this,  //shared_from_this(),
                    boost::asio::placeholders::error));

    // Spin until read finishes or we timeout
    result_ = RESULT_IN_PROGRESS;
    bytesTransferred_ = 0;
    for (;;)
    {
        ioService_.run_one();
        switch (result_)
        {
        case RESULT_SUCCESS:
            timer_.cancel();
            return bytesTransferred_;

        case RESULT_TIMED_OUT:
            timer_.cancel();
            socket_.cancel();
            throw exception::SocketTimeout("Timed out while reading socket");

        case RESULT_ERROR:
            timer_.cancel();
            socket_.cancel();
            throw exception::SocketRecvException(
                    "Error while reading socket: " + boost::system::error_code().message());

        case RESULT_IN_PROGRESS:
            default:
            // Keep waiting
            break;
        }
    }
    std::stringstream ss;
    ss << "Unexpected receive exception in file " << __FILE__ << " line " << __LINE__;
    throw exception::SocketRecvException(ss.str());
}

void SocketUdp::readTimedOut(const boost::system::error_code& error)
{
    if (!error && (result_ == RESULT_IN_PROGRESS))
    {
        result_ = RESULT_TIMED_OUT;
    }
    else if (error != boost::system::errc::operation_canceled)
    {
        result_ = RESULT_ERROR;
    }
}

void SocketUdp::readComplete(const boost::system::error_code& error, const size_t bytesTransferred)
{
    if (!error)
    {
        result_ = RESULT_SUCCESS;
        this->bytesTransferred_ = bytesTransferred;
        return;
    }
    else if (error != boost::system::errc::operation_canceled)
    {
        result_ = RESULT_ERROR;
    }
}

int SocketUdp::send(const char *pData, int size)
{
    if (!open_)
    {
        throw exception::SocketSendException("Socket not open");
    }

    return socket_.send_to(boost::asio::buffer(pData, size), endpoint_);
}

bool SocketUdp::isOpen(void)
{
    return open_;
}

void SocketUdp::bind(void)
{
    if (!open_)
    {
        throw exception::SocketBindException("Socket not open");
    }

    socket_.bind(endpoint_, lastError_);

    if (lastError_ != boost::system::errc::success)
    {
        throw exception::SocketBindException("Failed to bind socket: " + lastError_.message());
    }
}

}  // namespace socket
}  // namespace pneubotics
