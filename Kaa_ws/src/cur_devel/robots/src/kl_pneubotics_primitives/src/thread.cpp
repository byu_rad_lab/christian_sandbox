/*
 * @file    thread.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Provide ability to create new execution thread
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "kl_pneubotics_primitives/thread.h"
#include "kl_pneubotics_primitives/exception.h"
#include "kl_pneubotics_primitives/pneubotics_object.h"

#include <errno.h>

namespace pneubotics
{
namespace thread
{

Thread::Thread() :
        Object(), running_(false), initalized_(false), threadHandle_(0), threadPrio_(THREAD_PRIO_NORMAL)
{
}
Thread::Thread(std::string name) :
        Object(name), running_(false), initalized_(false), threadHandle_(0), threadPrio_(THREAD_PRIO_NORMAL)
{
}

Thread::Thread(thread_prio_t prio) :
        Object(), running_(false), initalized_(false), threadHandle_(0), threadPrio_(prio)
{
}
Thread::Thread(std::string name, thread_prio_t prio) :
        Object(name), running_(false), initalized_(false), threadHandle_(0), threadPrio_(prio)
{
}

Thread::~Thread()
{
}

void Thread::doStart(void)
{
    if (initalized_)
    {
        throw exception::ThreadException("A thread instance can only ever be started once. Cannot start again.");
    }

    // Do initialization
    init();

    // Start the thread
    int rc = pthread_create(&threadHandle_, NULL, mainLoopCaller, this);
    if (rc)
    {
        std::stringstream ss;
        ss << "Failed to create thread with error code " << rc;
        switch (rc)
        {
        case EAGAIN:
            ss << ", EAGAIN The  system  lacked  the  necessary  resources to create another thread, or "
                    "the system-imposed  limit  on  the  total  number  of threads in a process "
                    "{PTHREAD_THREADS_MAX} would be exceeded.";
            break;
        case EINVAL:
            ss << ", EINVAL The value specified by attr is invalid.";
            break;
        case EPERM:
            ss << ", EPERM The  caller  does  not  have  appropriate  permission to set the required scheduling"
                    " parameters or scheduling policy.";
            break;
        default:
            break;
        }
        throw exception::ThreadException(ss.str());
    }
}

void Thread::bind(void *arg)
{
    boundThreads_.push_back(arg);
}

void Thread::start(void)
{
    running_ = true;
    doStart();
}

bool Thread::isAlive(void)
{
    return running_;
}

void Thread::join(void)
{
    if (!initalized_)
    {
        throw exception::ThreadException("Cannot join uninitialized thread.");
    }

    int rc = pthread_join(threadHandle_, NULL);
    if (rc)
    {
        std::stringstream ss;
        ss << "Failed to join thread with error code " << rc;
        switch (rc)
        {
        case EINVAL:
            ss << ", EINVAL The implementation has detected  that  the  value  specified  by thread does not "
                    "refer to a joinable thread.";
            break;
        case ESRCH:
            ss << ", ESRCH No thread could be found corresponding to that specified by the given thread ID.";
            break;
        case EDEADLK:
            ss << ", EDEADLK A deadlock was detected or the value  of  thread  specifies  the calling thread.";
            break;
        default:
            break;
        }
        throw exception::ThreadException(ss.str());
    }
}

void Thread::stop(void)
{
    running_ = false;
    join();
}

void Thread::cancel(void)
{
    if (!initalized_)
    {
        throw exception::ThreadException("Cannot cancel uninitialized thread.");
    }

    if (!running_)
    {
        throw exception::ThreadException("Thread not running. Cannot cancel.");
    }

    int rc = pthread_cancel(threadHandle_);
    if (rc)
    {
        std::stringstream ss;
        ss << "Failed to cancel thread with error code " << rc;
        switch (rc)
        {
        case ESRCH:
            ss << ", ESRCH No thread could be found corresponding to that specified by the given thread ID.";
            break;
        }
        throw exception::ThreadException(ss.str());
    }
    running_ = false;
}

void Thread::init(void)
{
    /*
     * Do your own init
     */
    doOwnInit();
    initalized_ = true;

    /*
     * Do init for any other threads that have been bound to this one
     */
    for (std::vector<void *>::iterator it = boundThreads_.begin(); it != boundThreads_.end(); ++it)
    {
        boundInitCaller(*it);
    }
}

void Thread::setPriority(void)
{
    if (threadPrio_ == THREAD_PRIO_HIGH)
    {
        int ret;
        pthread_t this_thread = pthread_self();
        struct sched_param params;

        params.sched_priority = sched_get_priority_max(SCHED_FIFO);
        info(name_ + " attempting to set thread priority to real-time");

        // Attempt to set thread real-time priority to the SCHED_FIFO policy
        ret = pthread_setschedparam(this_thread, SCHED_FIFO, &params);
        if (ret != 0)
        {
            error(name_ + " unsuccessful in setting thread priority to real-time");
        }
        else
        {
            info(name_ + " successfully set thread priority to real-time");
        }

        // Now verify the change in thread priority
        int policy = 0;
        ret = pthread_getschedparam(this_thread, &policy, &params);
        if (ret != 0)
        {
            warn(name_ + " couldn't retrieve real-time scheduling parameters");
        }

        // Check the correct policy was applied
        if (policy != SCHED_FIFO)
        {
            error(name_ + " thread scheduling is not set to SCHED_FIFO");
        }
    }
    else
    {
        info(name_ + " running with default priority");
    }
}

void Thread::ownWork(void)
{
    doOwnWorkPre();
    doOwnWork();
    doOwnWorkPost();
}

void Thread::allWork(void)
{
    {
        /*
         * Do your own work
         */
        ownWork();

        /*
         * Do work for any other threads that have been bound to this one
         */
        for (std::vector<void *>::iterator it = boundThreads_.begin(); it != boundThreads_.end(); ++it)
        {
            boundWorkCaller(*it);
        }
    }
}
void Thread::boundWork(void)
{
    ownWork();
}

void Thread::boundWorkCaller(void *context)
{
    ((Thread *) context)->boundWork();
}

void Thread::boundInitCaller(void *context)
{
    ((Thread *) context)->doOwnInit();
}

void *Thread::mainLoopCaller(void *context)
{
    // Run main loop
    return ((Thread *) context)->mainLoop();
}

void *Thread::mainLoop(void)
{
    setPriority();

    do
    {
        allWork();
    }
    while (running_);

    return 0;
}

}  // namespace thread
}  // namespace pneubotics

