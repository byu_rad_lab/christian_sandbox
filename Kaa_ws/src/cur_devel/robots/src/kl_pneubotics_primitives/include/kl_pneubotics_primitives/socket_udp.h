/*
 * @file    socket_udp.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Provides basic low level utilities for reading writing UPD packets.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _PNEUBOTICS_SOCKET_UDP_H_
#define _PNEUBOTICS_SOCKET_UDP_H_

#include "kl_pneubotics_primitives/exception.h"

#include <boost/asio.hpp>

#include <string>

namespace pneubotics
{
namespace socket
{

class SocketUdp
{
public:
    SocketUdp(void);
    ~SocketUdp(void);

    /**
     * @brief   Open a UDP socket for communications
     *
     * @param   host Host address
     * @param   host Port
     */
    void open(std::string host, int port);

    /**
     * @brief   Bind a UDP socket to a specific endpoint
     */
    void bind(void);
    /**
     * @brief   Open a socket
     */
    void close(void);
    /**
     * @brief   Receive data from socket. This will block until at least the number of bytes
     *          specified by size is read.
     *
     * @param   pData   Pointer to location for storing data
     * @param   size    Maximum number of bytes to read
     * @retval          Number of bytes that was actually read.
     */
    int recv(char *pData, int size);
    /**
     * @brief   Receive data from socket. This will block until at least the number of bytes
     *          specified by size is read or the more than timeout number of microseconds has
     *          elapsed. A SocketTimeoutException is thrown if a timeout occurs.
     *
     * @param   pData       Pointer to location for storing data
     * @param   size        Maximum number of bytes to read
     * @param   timeoutUS   Timeout in microseconds. Passing in 0 as the timeout provides BLOCKING behavior.
     * @retval              Number of bytes that was actually read.
     */
    int recv(char *pData, int size, int timeoutUs);
    /**
     * @brief   Send data on socket.
     *
     * @param   pData       Pointer to location of data to send
     * @param   size        Number of bytes to send from pData
     * @retval              Number of bytes that was actually sent.
     */
    int send(const char *pData, int size);
    /**
     * @brief   Check if a socket has already been opened.
     *
     * @retval true if socket is open, false otherwise
     */
    bool isOpen(void);

protected:
    typedef enum
    {
        RESULT_IN_PROGRESS,
        RESULT_SUCCESS,
        RESULT_ERROR,
        RESULT_TIMED_OUT
    } read_result_t;

    boost::asio::io_service ioService_;
    boost::asio::deadline_timer timer_;
    boost::asio::streambuf rxBuffer_;
    boost::asio::ip::udp::socket socket_;
    boost::asio::ip::address_v4 ip_;
    int port_;
    boost::system::error_code lastError_;
    boost::asio::ip::udp::endpoint endpoint_;
    bool open_;
    read_result_t result_;
    size_t bytesTransferred_;

private:
    void readComplete(const boost::system::error_code& error, size_t bytes_transferred);
    void readTimedOut(const boost::system::error_code& error);
};

}  // namespace socket
}  // namespace pneubotics

#endif // _PNEUBOTICS_SOCKET_UDP_H_
