/*
 * @file    duration.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Class for representing time durations
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _PNEUBOTICS_DURATION_H_
#define _PNEUBOTICS_DURATION_H_

#include "kl_pneubotics_primitives/exception.h"

#include <boost/chrono/chrono.hpp>
#include <boost/chrono/chrono_io.hpp>
#include <boost/thread/thread.hpp>

#include <ostream>
#include <string>
#include <string>

namespace pneubotics
{
namespace exception
{

class TimeException: public pneubotics::exception::Exception
{
public:
    TimeException(const std::string &msg) :
            ::pneubotics::exception::Exception(msg)
    {
    }

    virtual ~TimeException() throw ()
    {
    }
};

}  // namespace exception

namespace time
{

typedef boost::chrono::duration<double, boost::nano> dnsec;
typedef boost::chrono::duration<double, boost::micro> dusec;
typedef boost::chrono::duration<double, boost::milli> dmsec;
typedef boost::chrono::duration<double, boost::ratio<1> > dsec;
typedef boost::chrono::duration<double, boost::ratio<60> > dmin;
typedef boost::chrono::duration<double, boost::ratio<3600> > dhour;

template<class T>
class DurationBase
{
public:
    DurationBase()
    {
    }
    DurationBase(boost::chrono::high_resolution_clock::duration dur) :
            rep_(dur)
    {
    }
    ~DurationBase()
    {
    }

    T operator+(const T &rhs) const;
    T operator-(const T &rhs) const;
    T operator-() const;
    T operator*(double scale) const;
    T& operator+=(const T &rhs);
    T& operator-=(const T &rhs);
    T& operator*=(double scale);
    bool operator==(const T &rhs) const;
    inline bool operator!=(const T &rhs) const
            {
        return !(*static_cast<const T*>(this) == rhs);
    }
    bool operator>(const T &rhs) const;
    bool operator<(const T &rhs) const;
    bool operator>=(const T &rhs) const;
    bool operator<=(const T &rhs) const;

    boost::chrono::nanoseconds rep_;
};

class Duration: public DurationBase<Duration>
{
public:
    Duration()
    :
            DurationBase<Duration>()
    {
        rep_ = boost::chrono::high_resolution_clock::duration(0);
    }

    Duration(const double durSec)
    {
        rep_ = boost::chrono::high_resolution_clock::duration((long int) (1e9 * durSec));
    }

    void sleep(void) const
            {
        boost::this_thread::sleep_until(boost::chrono::high_resolution_clock::now() + rep_);
    }

    Duration(boost::chrono::high_resolution_clock::duration dur) :
            DurationBase<Duration>(dur)
    {
    }
    bool isZero(void)
    {
        return (rep_.count() == 0);
    }
    double toNSec(void) const
    {
        return boost::chrono::duration_cast<dnsec>(rep_).count();
    }
    double toUSec(void) const
    {
        return boost::chrono::duration_cast<dusec>(rep_).count();
    }
    double toMSec(void) const
    {
        return boost::chrono::duration_cast<dmsec>(rep_).count();
    }
    double toSec(void) const
    {
        return boost::chrono::duration_cast<dsec>(rep_).count();
    }
    double toMin(void) const
    {
        return boost::chrono::duration_cast<dmin>(rep_).count();
    }
    double toHour(void) const
    {
        return boost::chrono::duration_cast<dhour>(rep_).count();
    }
};

std::ostream& operator<<(std::ostream& os, const Duration& rhs);

template<class T>
T DurationBase<T>::operator+(const T &rhs) const
        {
    return T(rep_ + rhs.rep_);
}

template<class T>
T DurationBase<T>::operator*(double scale) const
        {
    return T(rep_ * scale);
}

template<class T>
T DurationBase<T>::operator-(const T &rhs) const
        {
    return T(rep_ - rhs.rep_);
}

template<class T>
T DurationBase<T>::operator-() const
{
    return T(-rep_);
}

template<class T>
T& DurationBase<T>::operator+=(const T &rhs)
{
    *this = *this + rhs;
    return *static_cast<T*>(this);
}

template<class T>
T& DurationBase<T>::operator-=(const T &rhs)
{
    *this += (-rhs);
    return *static_cast<T*>(this);
}

template<class T>
T& DurationBase<T>::operator*=(double scale)
{
    rep_ * scale;
    return *static_cast<T*>(this);
}

template<class T>
bool DurationBase<T>::operator<(const T &rhs) const
        {
    if (rep_ < rhs.rep_)
        return true;
    return false;
}

template<class T>
bool DurationBase<T>::operator>(const T &rhs) const
        {
    if (rep_ > rhs.rep_)
        return true;
    return false;
}

template<class T>
bool DurationBase<T>::operator<=(const T &rhs) const
        {
    if (rep_ <= rhs.rep_)
        return true;
    return false;
}

template<class T>
bool DurationBase<T>::operator>=(const T &rhs) const
        {
    if (rep_ >= rhs.rep_)
        return true;
    return false;
}

template<class T>
bool DurationBase<T>::operator==(const T &rhs) const
        {
    return rep_ == rhs.rep_;
}

}  // namespace duration
}  // namespace pneubotics
#endif // _PNEUBOTICS_DURATION_H_
