/*
 * @file    mutex.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Mutex for shared resource protection
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _PNEUBOTICS_MUTEX_H_
#define _PNEUBOTICS_MUTEX_H_

#include "kl_pneubotics_primitives/exception.h"
#include "kl_pneubotics_primitives/pneubotics_object.h"

#include <pthread.h>

namespace pneubotics
{
namespace exception
{
class MutexException: public pneubotics::exception::Exception
{
public:
    MutexException(const std::string &msg) :
            ::pneubotics::exception::Exception(msg)
    {
    }

    virtual ~MutexException() throw ()
    {
    }
};

}  // namespace exception

namespace mutex
{

class Mutex
{
public:
    Mutex();
    virtual ~Mutex();
    void lock(void);
    void unlock(void);

private:
    pthread_mutex_t mutexHandle_;
};

}  // namespace mutex
}  // namespace pneubotics

#endif  // _PNEUBOTICS_MUTEX_H_
