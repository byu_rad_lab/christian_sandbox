/*
 * @file    loop.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Provides ability to create a new thread for executing some work on a periodic basis
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _PNEUBOTICS_LOOP_H_
#define _PNEUBOTICS_LOOP_H_

#include "kl_pneubotics_primitives/thread.h"
#include "kl_pneubotics_primitives/mutex.h"
#include "kl_pneubotics_primitives/time.h"
#include "kl_pneubotics_primitives/queue.h"
#include "kl_pneubotics_primitives/data_logger.h"

namespace pneubotics
{
namespace loop
{

class Loop: public thread::Thread
{
public:
    Loop();
    Loop(const std::string &name);
    Loop(const std::string &name, const time::Duration &period);
    Loop(const time::Duration &period);
    Loop(thread::thread_prio_t prio);
    Loop(const std::string &name, thread::thread_prio_t prio);
    Loop(const std::string &name, const time::Duration &period, thread::thread_prio_t prio);
    Loop(const time::Duration &period, thread::thread_prio_t prio);
    virtual ~Loop();
    virtual void setLogging(bool ena);
    virtual bool getLogging(void);

protected:
    virtual void *mainLoop(void);
    virtual void ownWork(void);
    virtual void doLogging(void) = 0;

    /*
     * Virtual methods for implementing logging functionality
     */
    virtual void debug(const std::string &msg) = 0;
    virtual void info(const std::string &msg) = 0;
    virtual void warn(const std::string &msg) = 0;
    virtual void error(const std::string &msg) = 0;
    virtual void fatal(const std::string &msg) = 0;

    /* Variables for dealing with loop timing
     *
     */
    time::Time time_;
    time::Duration timeDiff_;
    time::Duration period_;
    time::Time timeNext_;

    /*
     * Data logging related variables
     */
    file_io::DataLogger dataLogger_;
    bool logging_;
};

}  // namespace loop
}  // namespace pneubotics
#endif // _PNEUBOTICS_LOOP_H_
