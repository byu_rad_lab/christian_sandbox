/*
 * @file    exception.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Establishes namespace and base class for all Pneubotics exceptions.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _PNEUBOTICS_EXCEPTION_H_
#define _PNEUBOTICS_EXCEPTION_H_

#include <stdexcept>
#include <string>

namespace pneubotics
{
namespace exception
{

class Exception: public std::runtime_error
{
public:
    Exception(const std::string &msg) :
            std::runtime_error(msg)
    {
    }

    ~Exception() throw ()
    {
    }
};

}  // namespace exception
}  // namespace pneubotics
#endif // _PNEUBOTICS_EXCEPTION_H_
