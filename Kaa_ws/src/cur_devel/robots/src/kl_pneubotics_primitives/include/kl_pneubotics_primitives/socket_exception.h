/*
 * @file    socket_exception.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Defines a family of exceptions related to socket communications.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _PNEUBOTICS_SOCKET_EXCEPTION_H_
#define _PNEUBOTICS_SOCKET_EXCEPTION_H_

#include "kl_pneubotics_primitives/exception.h"

#include <string>

namespace pneubotics
{
namespace exception
{
class SocketException: public Exception
{
public:
    SocketException(const std::string &msg) :
            Exception(msg)
    {
    }
    ~SocketException() throw ()
    {
    }
};

class SocketTimeout: public SocketException
{
public:
    SocketTimeout(const std::string &msg) :
            SocketException(msg)
    {
    }
    ~SocketTimeout() throw ()
    {
    }
};

class SocketRecvException: public SocketException
{
public:
    SocketRecvException(const std::string &msg) :
            SocketException(msg)
    {
    }
    ~SocketRecvException() throw ()
    {
    }
};

class SocketSendException: public SocketException
{
public:
    SocketSendException(const std::string &msg) :
            SocketException(msg)
    {
    }
    ~SocketSendException() throw ()
    {
    }
};

class SocketBindException: public SocketException
{
public:
    SocketBindException(const std::string &msg) :
            SocketException(msg)
    {
    }
    ~SocketBindException() throw ()
    {
    }
};

class SocketOpenException: public SocketException
{
public:
    SocketOpenException(const std::string &msg) :
            SocketException(msg)
    {
    }
    ~SocketOpenException() throw ()
    {
    }
};

class SocketCloseException: public SocketException
{
public:
    SocketCloseException(const std::string &msg) :
            SocketException(msg)
    {
    }
    ~SocketCloseException() throw ()
    {
    }
};
//class SocketException: Exception
//{
//public:
//    SocketException(const char *msg) :
//            Exception(msg)
//    {
//    }
//    SocketException(const std::string &msg) :
//            Exception(msg)
//    {
//    }
//    ~SocketException() throw ()
//    {
//    }
//};
//
//class SocketTimeout: public SocketException
//{
//public:
//    SocketTimeout(const char *msg) :
//            SocketException(msg)
//    {
//    }
//    SocketTimeout(const std::string &msg) :
//            SocketException(msg)
//    {
//    }
//    ~SocketTimeout() throw ()
//    {
//    }
//};
//
//class SocketRecvException: public SocketException
//{
//public:
//    SocketRecvException(const char *msg) :
//            SocketException(msg)
//    {
//    }
//    SocketRecvException(const std::string &msg) :
//            SocketException(msg)
//    {
//    }
//    ~SocketRecvException() throw ()
//    {
//    }
//};
//
//class SocketSendException: public SocketException
//{
//public:
//    SocketSendException(const char *msg) :
//            SocketException(msg)
//    {
//    }
//    SocketSendException(const std::string &msg) :
//            SocketException(msg)
//    {
//    }
//    ~SocketSendException() throw ()
//    {
//    }
//};
//
//class SocketBindException: public SocketException
//{
//public:
//    SocketBindException(const char *msg) :
//            SocketException(msg)
//    {
//    }
//    SocketBindException(const std::string &msg) :
//            SocketException(msg)
//    {
//    }
//    ~SocketBindException() throw ()
//    {
//    }
//};
//
//class SocketOpenException: public SocketException
//{
//public:
//    SocketOpenException(const char *msg) :
//            SocketException(msg)
//    {
//    }
//    SocketOpenException(const std::string &msg) :
//            SocketException(msg)
//    {
//    }
//    ~SocketOpenException() throw ()
//    {
//    }
//};
//
//class SocketCloseException: public SocketException
//{
//public:
//    SocketCloseException(const char *msg) :
//            SocketException(msg)
//    {
//    }
//    SocketCloseException(const std::string &msg) :
//            SocketException(msg)
//    {
//    }
//    ~SocketCloseException() throw ()
//    {
//    }
//};
}// namespace exception
}  // namespace pneubotics
#endif // _PNEUBOTICS_SOCKET_EXCEPTION_H_
