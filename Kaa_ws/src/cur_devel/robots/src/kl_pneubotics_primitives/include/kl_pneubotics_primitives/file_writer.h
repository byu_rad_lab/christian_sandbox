/*
 * @file    file_writer.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Provides basic low level file writing functionality.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _PNEUBOTICS_FILE_WRITER_H_
#define _PNEUBOTICS_FILE_WRITER_H_

#include "kl_pneubotics_primitives/pneubotics_object.h"
#include "kl_pneubotics_primitives/exception.h"

#include <boost/filesystem.hpp>

#include <string>
#include <iostream>
#include <fstream>

namespace pneubotics
{
namespace exception
{
class FileException: public pneubotics::exception::Exception
{
public:
    FileException(const std::string &msg) :
            ::pneubotics::exception::Exception(msg)
    {
    }

    virtual ~FileException() throw ()
    {
    }
};

}  // namespace exception

namespace file_io
{

class FileWriter: public Object
{
public:
    FileWriter();
    FileWriter(const std::string &name);
    FileWriter(const std::string &name, const std::ios::openmode openmode);
    FileWriter(const std::string &name, const std::string &filepath);
    FileWriter(const std::string &name, const std::ios::openmode openmode, const std::string &filepath);
    virtual ~FileWriter();

    void openmodeSet(const std::ios::openmode openmode);
    std::ios::openmode getOpenMode(void) const;
    void setFilePath(const std::string &filepath);
    std::string getFilePath(void) const;
    virtual void open(const std::string &name);
    virtual void open(void);
    virtual void open(const std::string &name, const std::ios::openmode openmode);
    virtual void open(const std::ios::openmode openmode);
    virtual void close(void);
    virtual bool is_open(void);
    virtual void write(const std::string &msg);
    virtual void write(const std::string &msg, bool addLineEnding);

protected:
    std::ofstream *ofstream_;
    std::ios::openmode openmode_;
    std::string filepath_;
};

}  // namespace file_io
}  // namespace pneubotics
#endif // _PNEUBOTICS_FILE_WRITER_H_
