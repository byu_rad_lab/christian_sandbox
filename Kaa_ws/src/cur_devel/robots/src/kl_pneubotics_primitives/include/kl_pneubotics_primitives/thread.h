/*
 * @file    thread.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Provide ability to create new execution thread
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _PNEUBOTICS_THREAD_H_
#define _PNEUBOTICS_THREAD_H_

#include "kl_pneubotics_primitives/exception.h"
#include "kl_pneubotics_primitives/pneubotics_object.h"

#include <pthread.h>
#include <string>
#include <sstream>
#include <vector>

namespace pneubotics
{
namespace exception
{

class ThreadException: public Exception
{
public:
    ThreadException(const std::string &msg) :
            Exception(msg)
    {
    }

    virtual ~ThreadException() throw ()
    {
    }
};

}  // namespace exception

namespace thread
{
typedef enum
{
    THREAD_PRIO_NORMAL = 0,
    THREAD_PRIO_HIGH
} thread_prio_t;

class Thread: public Object
{
public:
    Thread();
    Thread(std::string name);
    Thread(thread_prio_t prio);
    Thread(std::string name, thread_prio_t prio);
    virtual ~Thread();
    virtual void start(void);
    virtual bool isAlive(void);
    virtual void join(void);
    virtual void stop(void);
    virtual void cancel(void);
    void bind(void *arg);

protected:
    static void *mainLoopCaller(void *context);
    static void boundWorkCaller(void *context);
    static void boundInitCaller(void *context);

    void doStart(void);

    virtual void ownWork(void);

    // Pure virtual methods to be overriden in concrete implementation
    virtual void doOwnWork(void) = 0;
    virtual void doOwnWorkPre(void) = 0;
    virtual void doOwnWorkPost(void) = 0;
    virtual void doOwnInit(void) = 0;

    virtual void boundWork(void);
    virtual void allWork(void);

    void init(void);

    void setPriority(void);

    /*
     * Virtual methods for implementing logging functionality
     */
    virtual void debug(const std::string &msg) = 0;
    virtual void info(const std::string &msg) = 0;
    virtual void warn(const std::string &msg) = 0;
    virtual void error(const std::string &msg) = 0;
    virtual void fatal(const std::string &msg) = 0;

    // Enter infinite loop calling ownWork
    virtual void *mainLoop(void);

    bool running_;
    bool initalized_;
    std::vector<void *> boundThreads_;

private:
    pthread_t threadHandle_;
    thread_prio_t threadPrio_;
};

}  // namespace thread
}  // namespace pneubotics

#endif // _PNEUBOTICS_THREAD_H_
