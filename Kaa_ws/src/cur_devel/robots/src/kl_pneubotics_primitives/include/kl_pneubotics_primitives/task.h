/*
 * @file    task.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Some unit of work to be done
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _PNEUBOTICS_TASK_H_
#define _PNEUBOTICS_TASK_H_

#include "kl_pneubotics_primitives/pneubotics_object.h"
#include "kl_pneubotics_primitives/exception.h"

#include <string>

namespace pneubotics
{
namespace exception
{
class TaskException: public pneubotics::exception::Exception
{
public:
    TaskException(const std::string &msg) :
            pneubotics::exception::Exception(msg)
    {
    }

    virtual ~TaskException() throw ()
    {
    }
};

class TaskInitException: public TaskException
{
public:
    TaskInitException(const std::string &msg) :
            TaskException(msg)
    {
    }

    virtual ~TaskInitException() throw ()
    {
    }
};

class TaskRunException: public TaskException
{
public:
    TaskRunException(const std::string &msg) :
            TaskException(msg)
    {
    }

    virtual ~TaskRunException() throw ()
    {
    }
};

}  // namespace exception

namespace task
{

class Task: public Object
{
public:
    Task();
    Task(const std::string &name);
    ~Task(void);

    virtual void init(void);
    virtual void run(void);

    bool isInitialized(void) const;

protected:
    virtual void doRun(void) = 0;
    virtual void doInit(void) = 0;

    bool initialized_;
};

}  // namespace socket
}  // namespace pneubotics

#endif // _PNEUBOTICS_TASK_H_
