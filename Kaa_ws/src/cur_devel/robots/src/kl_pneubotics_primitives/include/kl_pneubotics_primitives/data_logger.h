/*
 * @file    data_logger.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Provides basic functionality for logging data to a file.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _PNEUBOTICS_DATA_LOGGER_H_
#define _PNEUBOTICS_DATA_LOGGER_H_

#include "kl_pneubotics_primitives/file_writer.h"
#include "kl_pneubotics_primitives/time.h"
#include "kl_pneubotics_primitives/mutex.h"

#include <string>

namespace pneubotics
{
namespace file_io
{
/**
 * @brief Abstract base class for loggable objects. Derived classes must override the getColumnHeaders,
 *        getColumnDefinitions, and getColumnData methods.
 */
class LoggableObject: public Object
{
public:
    LoggableObject();
    LoggableObject(const std::string &logName);
    LoggableObject(const std::string &logName, const std::string &logPath);
    virtual ~LoggableObject();

    virtual std::string getPath(void) const;

    virtual std::string getColumnHeaders(void) const = 0;
    virtual std::string getColumnDefinitions(int startNum) const = 0;
    virtual std::string getColumnData(void) const = 0;

protected:
    std::string logPath_;
};

/**
 * @brief Base class for loggable objects. Derived classes must override the getHeader and getData methods.
 */
class DataLogger
{
public:
    DataLogger();
    virtual ~DataLogger();

    virtual void init(const LoggableObject &loggableObject);
    virtual void end(void);

    virtual std::string getLogFilePath(void) const;
    virtual void writeLogEntry(const LoggableObject &loggableObject);

protected:
    FileWriter *fileWriter_;
    std::string startDateTime_;
    mutex::Mutex fileWriterLock_;
};

}  // namespace file_io
}  // namespace pneubotics
#endif // _PNEUBOTICS_DATA_LOGGER_H_
