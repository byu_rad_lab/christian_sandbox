/*
 * @file    pneubotics_object.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Base Object class for deriving most Pneubotics classes.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _PNEUBOTICS_OBJECT_H_
#define _PNEUBOTICS_OBJECT_H_

#include <string>

namespace pneubotics
{

class Object
{
public:
    /**
     * @brief Default constructor
     */
    Object();
    /**
     * @brief Construct object with name provided
     * @param name
     */
    Object(const std::string &name);
    /**
     * @brief Destructor
     */
    virtual ~Object();
    /**
     * @brief Set the name of this object
     * @param name
     */
    virtual void setName(const std::string &name);
    /**
     * @brief Get the name of this object
     * @retval name
     */
    virtual std::string getName(void) const;

protected:
    std::string name_;

};

}  // namespace pneubotics
#endif // _PNEUBOTICS_OBJECT_H_
