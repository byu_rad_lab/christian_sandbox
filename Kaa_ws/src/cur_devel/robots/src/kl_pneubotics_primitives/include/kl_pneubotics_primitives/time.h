/*
 * @file    time.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Class for representing points in time
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _PNEUBOTICS_TIME_H_
#define _PNEUBOTICS_TIME_H_

#include "kl_pneubotics_primitives/duration.h"

#include <boost/chrono/chrono.hpp>
#include <boost/chrono/chrono_io.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <locale>
#include <sstream>
#include <ostream>
#include <string>

namespace pneubotics
{
namespace time
{

template<class T, class D>
class TimeBase
{
public:
    TimeBase()
    {
    }
    TimeBase(boost::chrono::high_resolution_clock::time_point timeRep) :
            rep_(timeRep)
    {
    }
    ~TimeBase()
    {
    }

    D operator-(const T &rhs) const;
    T operator+(const D &rhs) const;
    T operator-(const D &rhs) const;
    T& operator+=(const D &rhs);
    T& operator-=(const D &rhs);
    bool operator==(const T &rhs) const;
    inline bool operator!=(const T &rhs) const
            {
        return !(*static_cast<const T*>(this) == rhs);
    }
    bool operator>(const T &rhs) const;
    bool operator<(const T &rhs) const;
    bool operator>=(const T &rhs) const;
    bool operator<=(const T &rhs) const;

    boost::chrono::high_resolution_clock::time_point rep_;
};

class Time: public TimeBase<Time, Duration>
{
public:
    Time() :
            TimeBase<Time, Duration>()
    {
    }
    Time(const Time &timeRep) :
            TimeBase<Time, Duration>()
    {
        rep_ = timeRep.rep_;
    }
    Time(boost::chrono::high_resolution_clock::time_point timeRep) :
            TimeBase(timeRep)
    {
    }
    ~Time()
    {
    }

    static Time now(void)
    {
        return Time(boost::chrono::high_resolution_clock::now());
    }

    double toNSec(void) const
    {
        return boost::chrono::duration_cast<dnsec>(rep_.time_since_epoch()).count();
    }
    double toUSec(void) const
    {
        return boost::chrono::duration_cast<dusec>(rep_.time_since_epoch()).count();
    }
    double toMSec(void) const
    {
        return boost::chrono::duration_cast<dmsec>(rep_.time_since_epoch()).count();
    }
    double toSec(void) const
    {
        return boost::chrono::duration_cast<dsec>(rep_.time_since_epoch()).count();
    }
    double toMin(void) const
    {
        return boost::chrono::duration_cast<dmin>(rep_.time_since_epoch()).count();
    }
    double toHour(void) const
    {
        return boost::chrono::duration_cast<dhour>(rep_.time_since_epoch()).count();
    }

    static void sleepUntil(const Time& end)
    {
        boost::this_thread::sleep_until(end.rep_);
    }
};

class WallTime
{
public:
    WallTime();
    ~WallTime();

    static std::string isoTimeLocal(void);
    static std::string isoTimeUTC(void);

    static std::string isoDateLocal(void);
    static std::string isoDateUTC(void);

    static std::string isoDateTimeLocal(void);
    static std::string isoDateTimeUTC(void);
protected:
    static const std::string dateTimeDelim_;
};

std::ostream& operator<<(std::ostream& os, const Time& rhs);

template<class T, class D>
D TimeBase<T, D>::operator-(const T &rhs) const
        {
    return D(rep_ - rhs.rep_);
}

template<class T, class D>
T TimeBase<T, D>::operator-(const D &rhs) const
        {
    return *static_cast<const T*>(this) + (-rhs);
}

template<class T, class D>
T TimeBase<T, D>::operator+(const D &rhs) const
        {
    return T(rep_ + rhs.rep_);
}

template<class T, class D>
T& TimeBase<T, D>::operator+=(const D &rhs)
{
    *this = *this + rhs;
    return *static_cast<T*>(this);
}

template<class T, class D>
T& TimeBase<T, D>::operator-=(const D &rhs)
{
    *this += (-rhs);
    return *static_cast<T*>(this);
}

template<class T, class D>
bool TimeBase<T, D>::operator==(const T &rhs) const
        {
    return rep_ == rhs.rep_;
}

template<class T, class D>
bool TimeBase<T, D>::operator<(const T &rhs) const
        {
    if (rep_ < rhs.rep_)
        return true;
    return false;
}

template<class T, class D>
bool TimeBase<T, D>::operator>(const T &rhs) const
        {
    if (rep_ > rhs.rep_)
        return true;
    return false;
}

template<class T, class D>
bool TimeBase<T, D>::operator<=(const T &rhs) const
        {
    if (rep_ <= rhs.rep_)
        return true;
    return false;
}

template<class T, class D>
bool TimeBase<T, D>::operator>=(const T &rhs) const
        {
    if (rep_ >= rhs.rep_)
        return true;
    return false;
}

}  // namespace time
}  // namespace pneubotics

#endif // _PNEUBOTICS_TIME_H_
