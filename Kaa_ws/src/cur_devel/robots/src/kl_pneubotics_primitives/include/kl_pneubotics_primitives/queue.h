/*
 * @file    queue.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Thread safe queue for sharing data
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _PNEUBOTICS_QUEUE_H_
#define _PNEUBOTICS_QUEUE_H_

#include "kl_pneubotics_primitives/mutex.h"
#include "kl_pneubotics_primitives/exception.h"
#include "kl_pneubotics_primitives/time.h"

#include <queue>

namespace pneubotics
{
namespace exception
{
class QueueException: public pneubotics::exception::Exception
{
public:
    QueueException(const std::string &msg) :
            ::pneubotics::exception::Exception(msg)
    {
    }

    virtual ~QueueException() throw ()
    {
    }
};
class QueueFullException: public QueueException
{
public:
    QueueFullException(const std::string &msg) :
            QueueException(msg)
    {
    }

    virtual ~QueueFullException() throw ()
    {
    }
};

class QueueEmptyException: public QueueException
{
public:
    QueueEmptyException(const std::string &msg) :
            QueueException(msg)
    {
    }

    virtual ~QueueEmptyException() throw ()
    {
    }
};

}  // namespace exception

namespace queue
{

template<class T>
class Queue
{
public:
    Queue() :
            maxSize_(0), sleepTime_(time::Duration(defaultSleepTimeSec_))
    {
    }
    Queue(int maxSize) :
            maxSize_(maxSize), sleepTime_(time::Duration(defaultSleepTimeSec_))
    {
        if (maxSize_ <= 0)
        {
            throw exception::QueueException("Queue size cannot be less than or equal to zero");
        }
    }
    Queue(time::Duration sleepTime) :
            maxSize_(0), sleepTime_(sleepTime)
    {
    }
    Queue(int maxSize, time::Duration sleepTime) :
            maxSize_(maxSize), sleepTime_(sleepTime)
    {
        if (maxSize_ <= 0)
        {
            throw exception::QueueException("Queue size cannot be less than or equal to zero");
        }
    }
    ~Queue()
    {
    }

    T& pop(void)
    {
        lock_.lock();
        if (q_.empty())
        {
            lock_.unlock();
            throw exception::QueueEmptyException("Cannot pop item. Queue is empty.");
        }
        T& ret = q_.front();
        q_.pop();
        lock_.unlock();
        return ret;
    }

    T& pop_blocking(void)
    {
        while (true)
        {
            lock_.lock();
            if (q_.empty())
            {
                lock_.unlock();
                sleepTime_.sleep();
            }
            else
            {
                T& ret = q_.front();
                q_.pop();
                lock_.unlock();
                return ret;
            }
        }
        throw exception::QueueException("Failed to retrieve item from queue");
    }

    void push(const T& val)
    {
        lock_.lock();
        if (maxSize_ > 0)
        {
            if (q_.size() == maxSize_)
            {
                q_.pop();
            }
        }
        q_.push(val);
        lock_.unlock();
    }

    bool empty(void)
    {
        lock_.lock();
        bool ret = q_.empty();
        lock_.unlock();
        return ret;
    }
    int size()
    {
        lock_.lock();
        int ret = q_.size();
        lock_.unlock();
        return ret;
    }
    T& front(void)
    {
        lock_.lock();
        if (q_.empty())
        {
            lock_.unlock();
            throw exception::QueueEmptyException("Cannot get front item. Queue is empty.");
        }
        T& ret = q_.front();
        lock_.unlock();
        return ret;
    }

    T& front_blocking(void)
    {
        while (true)
        {
            lock_.lock();
            if (q_.empty())
            {
                lock_.unlock();
                sleepTime_.sleep();
            }
            else
            {
                T& ret = q_.front();
                lock_.unlock();
                return ret;
            }
        }
        throw exception::QueueException("Failed to retrieve item from queue");
    }

    T& back(void)
    {
        lock_.lock();
        if (q_.empty())
        {
            lock_.unlock();
            throw exception::QueueEmptyException("Cannot get back item. Queue is empty.");
        }
        T& ret = q_.back();
        lock_.unlock();
        return ret;
    }

    T& back_blocking(void)
    {
        while (true)
        {
            lock_.lock();
            if (q_.empty())
            {
                lock_.unlock();
                sleepTime_.sleep();
            }
            else
            {
                T& ret = q_.back();
                lock_.unlock();
                return ret;
            }
        }
        throw exception::QueueException("Failed to retrieve item from queue");
    }

    static constexpr double defaultSleepTimeSec_ = 1.0e-6;

protected:
    time::Duration sleepTime_;
    std::queue<T> q_;
    int maxSize_;
    mutex::Mutex lock_;
};

}  // namespace queue
}  // namespace pneubotics
#endif // _PNEUBOTICS_QUEUE_H_
