cmake_minimum_required(VERSION 2.8.3)
project(king_louie)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  message_generation
  std_msgs
  kl_pneubotics_primitives
  trac_ik_lib #added this for TRACIK
  eigen_conversions #added this for TRACIK (Eigen in C++)
  dynamic_reconfigure
)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)
#find_package(Boost REQUIRED COMPONENTS system chrono thread timer date_time filesystem)

## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

## To declare and build messages, services or actions from within this
## package, follow these steps:
## * Let MSG_DEP_SET be the set of packages whose message types you use in
##   your messages/services/actions (e.g. std_msgs, actionlib_msgs, ...).
## * In the file package.xml:
##   * add a build_depend and a run_depend tag for each package in MSG_DEP_SET
##   * If MSG_DEP_SET isn't empty the following dependencies might have been
##     pulled in transitively but can be declared for certainty nonetheless:
##     * add a build_depend tag for "message_generation"
##     * add a run_depend tag for "message_runtime"
## * In this file (CMakeLists.txt):
##   * add "message_generation" and every package in MSG_DEP_SET to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * add "message_runtime" and every package in MSG_DEP_SET to
##     catkin_package(CATKIN_DEPENDS ...)
##   * uncomment the add_*_files sections below as needed
##     and list every .msg/.srv/.action file to be processed
##   * uncomment the generate_messages entry below
##   * add every package in MSG_DEP_SET to generate_messages(DEPENDENCIES ...)

## Generate messages in the 'msg' folder
add_message_files(
  FILES
  current_controller_msg.msg
  current_controllers_msg.msg
  current_msg.msg
  data_msg.msg
  desired_pressure_msg.msg
  driver_board_msg.msg
  drivers_msg.msg
  gripper_status.msg
  joint_angles_msg.msg
  joint_angles_msg_array.msg #added this
  lqr_msg.msg
  mpc_msg.msg
  param_msg.msg
  pose_msg.msg
  pressure_msg.msg
  pressure_controller_msg.msg
  pressure_controllers_msg.msg
  sensor_board_msg.msg
  sensors.msg
  sensors_msg.msg
  sensor_board_pose_msg.msg
  set_current_ref_msg.msg
  set_pressure_ref_msg.msg
  set_pressure_ctrl_gains_msg.msg
)

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################

## To declare and build dynamic reconfigure parameters within this
## package, follow these steps:
## * In the file package.xml:
##   * add a build_depend and a run_depend tag for "dynamic_reconfigure"
## * In this file (CMakeLists.txt):
##   * add "dynamic_reconfigure" to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * uncomment the "generate_dynamic_reconfigure_options" section below
##     and list every .cfg file to be processed

## Generate dynamic reconfigure parameters in the 'cfg' folder
generate_dynamic_reconfigure_options(
  cfg/PressurePIDTuner.cfg
)

## Generate services in the 'srv' folder
# add_service_files(
#   FILES
#   Service1.srv
#   Service2.srv
# )

## Generate actions in the 'action' folder
#add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
generate_messages(
  DEPENDENCIES
  std_msgs
)

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES low_level_control
 CATKIN_DEPENDS roscpp rospy std_msgs message_runtime trac_ik_lib #added the last one for TRACIK
#  DEPENDS system_lib
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  ${eigen_INCLUDE_DIRS} #added this for TRACIK
)

## Declare a cpp library
# add_library(low_level_control
#   src/${PROJECT_NAME}/low_level_control.cpp
# )

## Declare a cpp executable
add_executable(low_level_control_node 
  src/low_level_control_node.cpp
  src/low_level_controller.cpp
  src/comms_handler.cpp
  src/real_time_loop.cpp
  #src/sensor_formatter.cpp
  src/pose_estimator.cpp
  src/ros_utils.cpp
  src/pressure_reference_generator.cpp
  src/pressure_controller.cpp
  src/loggable_data.cpp
  src/current_reference_generator.cpp
  src/current_controller.cpp
  )

#add_executable(joint_angle_function src/joint_angle_function.cpp src/JointAngleFinder.cpp) #added this for joint estimation
#add_executable(copy_of_jons_teleop_code src/copy_of_jons_teleop_code.cpp) #added this for Dustand and Nathan's project, Not working yet though, so currently commented out

# target_link_libraries(low_level_control_node ${catkin_LIBRARIES})

# add_dependencies(low_level_control_node ${catkin_EXPORTED_TARGETS})

## Add cmake target dependencies of the executable/library
## as an example, message headers may need to be generated before nodes
add_dependencies(low_level_control_node king_louie_generate_messages_cpp)

## Specify libraries to link a library or executable target against
target_link_libraries(low_level_control_node
  ${catkin_LIBRARIES}
)

#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
# install(PROGRAMS
#   scripts/my_python_script
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark executables and/or libraries for installation
# install(TARGETS low_level_control low_level_control_node
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark cpp header files for installation
# install(DIRECTORY include/${PROJECT_NAME}/
#   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
#   FILES_MATCHING PATTERN "*.h"
#   PATTERN ".svn" EXCLUDE
# )

## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_low_level_control.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
## location of the Python header files
