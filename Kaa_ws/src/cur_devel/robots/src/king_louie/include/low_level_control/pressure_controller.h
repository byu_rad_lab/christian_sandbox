/*
 * @file    pressure_controller.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Task that runs PID pressure controllers
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _LOW_LEVEL_PRESSURE_CONTROLLER_H_
#define _LOW_LEVEL_PRESSURE_CONTROLLER_H_

#include "king_louie/pressure_controllers_msg.h"
#include "king_louie/drivers_msg.h"
#include "king_louie/sensors_msg.h"
#include "king_louie/set_pressure_ctrl_gains_msg.h"

#include "low_level_control/gateway_data_types.h"

#include "low_level_control/ros_utils.h"

#include <kl_pneubotics_primitives/queue.h>

namespace king_louie
{
namespace exception
{

class PressureControllerException: public LowLevelControlException
{
public:
    PressureControllerException(const std::string &msg) :
            LowLevelControlException(msg)
    {
    }
    ~PressureControllerException() throw ()
    {
    }
};

}  // namespace exception

class PressureController: public RosTask
{
public:
    PressureController(const std::string &name, const std::string &paramServerBasePath,
            pneubotics::queue::Queue<pressure_controllers_msg> *prsCtrlDataQueue_,
            pneubotics::queue::Queue<drivers_msg> *driverDataQueue_,
            pneubotics::queue::Queue<sensors_msg> *sensorDataQueue);
    ~PressureController();

    void setPressureCtrlGains(const king_louie::set_pressure_ctrl_gains_msg &msg);
    void resetIntegrators(void);

protected:
    void loadParams(void);

    virtual void doRun(void);
    virtual void doInit(void);

    virtual void getInputs(void);
    virtual void setOutputs(void);

    bool resetIntegrators_;

    /*
     * Pointers to input/output queues
     */
    pneubotics::queue::Queue<drivers_msg> *driverDataQueue_;
    pneubotics::queue::Queue<pressure_controllers_msg> *prsCtrlDataQueue_;
    pneubotics::queue::Queue<sensors_msg> *sensorDataQueue_;

    /*
     * Local copies of data
     */
    drivers_msg driverData_;
    pressure_controllers_msg ctrlData_;
    sensors_msg sensorData_;

    /*
     * Local copies of control gains
     */
    double kP_[GATEWAY_DRIVER_BOARDS_PER_GATEWAY][GATEWAY_SENSORS_PRS_PER_SENSOR_BOARD];
    double kI_[GATEWAY_DRIVER_BOARDS_PER_GATEWAY][GATEWAY_SENSORS_PRS_PER_SENSOR_BOARD];
    double kD_[GATEWAY_DRIVER_BOARDS_PER_GATEWAY][GATEWAY_SENSORS_PRS_PER_SENSOR_BOARD];

    pneubotics::queue::Queue<king_louie::set_pressure_ctrl_gains_msg> setPressureCtrlGainsQueue_;

};

}  // namespace low_level_control

#endif // _LOW_LEVEL_PRESSURE_CONTROLLER_H_
