/*
 * @file    real_time_loop.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Real time loop which runs in its own thread. Responsible for running all of the individual tasks related
 *          controlling the robot and interfacing with the embedded system.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _LOW_LEVEL_CONTROL_REAL_TIME_LOOP_H_
#define _LOW_LEVEL_CONTROL_REAL_TIME_LOOP_H_

#include "low_level_control/comms_handler.h"
#include "low_level_control/pose_estimator.h"
#include "low_level_control/pressure_reference_generator.h"
#include "low_level_control/current_reference_generator.h"
#include "low_level_control/pressure_controller.h"
#include "low_level_control/current_controller.h"
#include "low_level_control/loggable_data.h"

#include "king_louie/sensors_msg.h"
#include "king_louie/drivers_msg.h"
#include "king_louie/pose_msg.h"
#include "king_louie/pressure_controllers_msg.h"
#include "king_louie/current_controllers_msg.h"
#include "king_louie/set_pressure_ref_msg.h"
#include "king_louie/set_current_ref_msg.h"
#include "king_louie/current_msg.h"
#include "king_louie/set_pressure_ctrl_gains_msg.h"
#include "std_msgs/Int16.h"

#include <kl_pneubotics_primitives/loop.h>
#include <kl_pneubotics_primitives/data_logger.h>

#include "ros/ros.h"

namespace king_louie
{

typedef enum
{
    RTL_CTRL_MODE_CURRENT = 0,
    RTL_CTRL_MODE_PRESSURE,

    RTL_CTRL_NUM_MODES       // Must be last element in enum
} controller_mode_t;

class RealTimeLoop: public pneubotics::loop::Loop
{
public:
    RealTimeLoop(const std::string &name, const std::string &paramServerBasePath,
            const pneubotics::time::Duration &period,
            pneubotics::queue::Queue<sensors_msg> *sensorDataQueue,
            pneubotics::queue::Queue<drivers_msg> *driverDataQueue,
            pneubotics::queue::Queue<pose_msg> *poseDataQueue,
            pneubotics::queue::Queue<pressure_controllers_msg> *prsCtrlDataQueue,
            pneubotics::queue::Queue<current_controllers_msg> *curCtrlDataQueue);
    ~RealTimeLoop();

    virtual void setLogging(const bool ena);

    void setPressureRef(const king_louie::set_pressure_ref_msg &msg);
    void setCurrentRef(const king_louie::current_msg &msg);
    void setPressureCtrlGains(const king_louie::set_pressure_ctrl_gains_msg &msg);
    void setControllerMode(const std_msgs::Int16 &msg);
    void resetIntegrators(void);

protected:
    virtual void doLogging(void);
    virtual void doOwnWork(void);
    virtual void doOwnWorkPre(void);
    virtual void doOwnWorkPost(void);
    virtual void doOwnInit(void);
    virtual void debug(const std::string &msg);
    virtual void info(const std::string &msg);
    virtual void warn(const std::string &msg);
    virtual void error(const std::string &msg);
    virtual void fatal(const std::string &msg);

    std::string paramServerPath_;

    CommsHandler *commsHandler_;
    PoseEstimator *poseEstimator_;
    PressureRefGen *pressureRefGen_;
    PressureController *pressureController_;
    CurrentRefGen *currentRefGen_;
    CurrentController *currentController_;

    pneubotics::queue::Queue<sensors_msg> *sensorDataQueue_;
    pneubotics::queue::Queue<drivers_msg> *driverDataQueue_;
    pneubotics::queue::Queue<pose_msg> *poseDataQueue_;
    pneubotics::queue::Queue<pressure_controllers_msg> *prsCtrlDataQueue_;
    pneubotics::queue::Queue<current_controllers_msg> *curCtrlDataQueue_;

    controller_mode_t controllerMode_;

    LoggableData loggableData_;
    pneubotics::file_io::DataLogger dataLogger_;
};
}  // namespace low_level_control

#endif // _LOW_LEVEL_CONTROL_REAL_TIME_LOOP_H_
