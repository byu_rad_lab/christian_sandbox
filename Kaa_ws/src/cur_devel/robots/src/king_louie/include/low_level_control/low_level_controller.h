/*
 * @file    low_level_controller.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Task that provides an interface between ROS and the real time loop
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _LOW_LEVEL_CONTROL_LOW_LEVEL_CONTROLLER_H_
#define _LOW_LEVEL_CONTROL_LOW_LEVEL_CONTROLLER_H_

#include "low_level_control/real_time_loop.h"
#include "low_level_control/ros_utils.h"

#include "king_louie/set_pressure_ref_msg.h"
#include "king_louie/current_msg.h"
#include "king_louie/set_pressure_ctrl_gains_msg.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Int16.h"
#include "king_louie/data_msg.h"
#include "king_louie/sensors.h"
#include "sensor_msgs/Imu.h"
#include "geometry_msgs/Vector3Stamped.h"
#include <vector>

namespace king_louie
{

class LowLevelController: public RosTask
{
public:
    LowLevelController(const std::string &name, const std::string &paramServerBasePath);
    virtual ~LowLevelController();

    void setLoggingCallback(const std_msgs::Bool &msg);
    void setPressureRefCallback(const king_louie::set_pressure_ref_msg &msg);
    void setCurrentRefCallback(const king_louie::current_msg &msg);
    void setPressureCtrlGains(const king_louie::set_pressure_ctrl_gains_msg &msg);
    void setControllerMode(const std_msgs::Int16 &msg);


protected:
    virtual void doInit(void);
    virtual void doRun(void);
    virtual void loadParams(void);

    virtual void getInputs(void);
    virtual void setOutputs(void);

    RealTimeLoop *realTimeLoop_;

    double realTimeLoopPeriodSec_;
    int rosPublisherQueueSize_;
    int rosSubscriberQueueSize_;

    void publishTopics(void);

    ros::NodeHandle nodeHandle_;
    ros::Rate rosInterfaceRate_;

    /*
     * Shared queues
     */
    pneubotics::queue::Queue<sensors_msg> sensorDataQueue_;
    pneubotics::queue::Queue<drivers_msg> driverDataQueue_;
    pneubotics::queue::Queue<pose_msg> poseDataQueue_;
    pneubotics::queue::Queue<pressure_controllers_msg> prsCtrlDataQueue_;
    pneubotics::queue::Queue<current_controllers_msg> curCtrlDataQueue_;

    /*
     * Publishers
     */
    ros::Publisher posePub_;
    ros::Publisher sensorsPub_;
    ros::Publisher prsCtrlPub_;
    ros::Publisher curCtrlPub_;
    ros::Publisher driverCmdPub_;

    /*
     * Subscribers
     */
    ros::Subscriber setLoggingSub_;
    ros::Subscriber setPressureRefSub_;
    ros::Subscriber setPressureCtrlGains_;
    ros::Subscriber setCurrentRef_;
    ros::Subscriber setControllerMode_;

    ros::NodeHandle n;

    //ros::Publisher pub_data;
    ros::Publisher pub_pressure;
    std::vector<ros::Publisher> rawImusPub_;
    std::vector<ros::Publisher> rawMagnetometersPub_;

    double t;
    double t_last;
    double h;
    double h_min;
    double cur_time;

    /*double ax;
    double ay;
    double az;
    double gz;
    double p0;
    double p1;
    double time;*/

    //data_msg msg;
    
    sensors msg;
    std::vector <sensor_msgs::Imu> raw_imu_msgs;
    std::vector <geometry_msgs::Vector3Stamped> raw_magnetometer_msgs;
};
}  // namespace low_level_control
#endif // _LOW_LEVEL_CONTROL_LOW_LEVEL_CONTROLLER_H_
