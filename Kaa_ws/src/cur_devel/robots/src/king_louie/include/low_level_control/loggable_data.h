/*
 * @file    loggable_data.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Define what and how data will be logged by the data logger.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _LOW_LEVEL_CONTROL_LOGGABLE_DATA_H_
#define _LOW_LEVEL_CONTROL_LOGGABLE_DATA_H_

#include <kl_pneubotics_primitives/data_logger.h>
#include <kl_pneubotics_primitives/queue.h>

#include "king_louie/sensors_msg.h"
#include "king_louie/drivers_msg.h"
#include "king_louie/pose_msg.h"
#include "king_louie/pressure_controllers_msg.h"

namespace king_louie
{

class LoggableData: public pneubotics::file_io::LoggableObject
{
public:
    LoggableData(const std::string &logName, const std::string &logPath,
            pneubotics::queue::Queue<sensors_msg> *sensorDataQueue,
            pneubotics::queue::Queue<drivers_msg> *driverDataQueue,
            pneubotics::queue::Queue<pose_msg> *poseDataQueue,
            pneubotics::queue::Queue<pressure_controllers_msg> *prsCtrlDataQueue);
    virtual ~LoggableData();

    virtual std::string getColumnHeaders(void) const;
    virtual std::string getColumnDefinitions(int startNum) const;
    virtual std::string getColumnData(void) const;

protected:
    pneubotics::queue::Queue<sensors_msg> *sensorDataQueue_;
    pneubotics::queue::Queue<drivers_msg> *driverDataQueue_;
    pneubotics::queue::Queue<pose_msg> *poseDataQueue_;
    pneubotics::queue::Queue<pressure_controllers_msg> *prsCtrlDataQueue_;
};

}  // namespace low_level_control
#endif // _LOW_LEVEL_CONTROL_LOGGABLE_DATA_H_
