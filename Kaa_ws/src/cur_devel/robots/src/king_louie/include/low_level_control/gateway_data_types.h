/*
 * @file    gateway_data_types.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Define constants based on embedded system configuration.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _LOW_LEVEL_CONTROL_GATEWAY_DATA_TYPES_H_
#define _LOW_LEVEL_CONTROL_GATEWAY_DATA_TYPES_H_

#include <stdint.h>

#define MIN(x,y) (((x) <= (y)) ? (x) : (y))
#define MAX(x,y) (((x) >= (y)) ? (x) : (y))
#define SATURATE(x, min, max) (MIN(MAX(x, min), max))

#define GATEWAY_SENSOR_BOARDS_PER_GATEWAY   7
#define GATEWAY_DRIVER_BOARDS_PER_GATEWAY   7

#define GATEWAY_DRIVERS_PER_DRIVERS_BOARD   4
#define GATEWAY_BYTES_PER_DRIVER_VALUE      2
#define GATEWAY_DRIVERS_BYTES_PER_BOARD     (GATEWAY_DRIVERS_PER_DRIVERS_BOARD*GATEWAY_BYTES_PER_DRIVER_VALUE)
#define GATEWAY_DRIVER_COMMANDS_PACKET_SIZE 72
#define GATEWAY_DRIVER_COMMANDS_BYTE_OFFSET 8
#define GATEWAY_DRIVER_PACKET_HEADER        0xFE
#define GATEWAY_DRIVER_MAX_COMMAND          1000.0
#define GATEWAY_DRIVER_MIN_COMMAND         -1000.0

#define GATEWAY_SENSORS_ACC_PER_SENSOR_BOARD 3
#define GATEWAY_SENSORS_GYR_PER_SENSOR_BOARD 3
#define GATEWAY_SENSORS_MAG_PER_SENSOR_BOARD 3
#define GATEWAY_SENSORS_PRS_PER_SENSOR_BOARD 4

#define GATEWAY_SENSOR_READING_SIZE_BYTES   36

#define GATEWAY_DRIVER_DATA_IP              "192.168.0.200"
#define GATEWAY_SENSOR_DATA_IP              "192.168.0.10"
#define GATEWAY_DRIVER_DATA_PORT            1235
#define GATEWAY_SENSOR_DATA_PORT            1234

#define GATEWAY_SENSOR_READINGS_BYTE_OFFSET 76
#define GATEWAY_SENSOR_READINGS_PACKET_SIZE 456

#define GATEWAY_SENSORS_ACC_MIN                -2.0  // G
#define GATEWAY_SENSORS_ACC_MAX                2.0   // G
#define GATEWAY_SENSORS_ACC_ADC_MIN            0
#define GATEWAY_SENSORS_ACC_ADC_MAX            (1 << 16)
#define GATEWAY_SENSORS_ADC_TO_ACC_SCALE       ((GATEWAY_SENSORS_ACC_MAX-GATEWAY_SENSORS_ACC_MIN)/(GATEWAY_SENSORS_ACC_ADC_MAX-GATEWAY_SENSORS_ACC_ADC_MIN))
#define GATEWAY_SENSORS_ADC_TO_ACC_OFFSET      0.0

#define GATEWAY_SENSORS_GYR_MIN                 -34.906585 // rad/s
#define GATEWAY_SENSORS_GYR_MAX                 34.906585  // rad/s
#define GATEWAY_SENSORS_GYR_ADC_MIN             0
#define GATEWAY_SENSORS_GYR_ADC_MAX             (1 << 16)
#define GATEWAY_SENSORS_ADC_TO_GYR_SCALE        ((GATEWAY_SENSORS_GYR_MAX-GATEWAY_SENSORS_GYR_MIN)/(GATEWAY_SENSORS_GYR_ADC_MAX-GATEWAY_SENSORS_GYR_ADC_MIN))
#define GATEWAY_SENSORS_ADC_TO_GYR_OFFSET       0.0

#define GATEWAY_SENSORS_ADC_TO_MAG_SCALE         0.3
#define GATEWAY_SENSORS_ADC_TO_MAG_OFFSET        0.0

#define GATEWAY_SENSORS_PRS_MIN_60_PSI            101325.0    // Pa
#define GATEWAY_SENSORS_PRS_MAX_60PSI             (413685.438 + GATEWAY_SENSORS_PRS_MIN_60_PSI) // Pa
#define GATEWAY_SENSORS_PRS_ADC_MAX_60_PSI         ((1 << 14) * 0.9)
#define GATEWAY_SENSORS_PRS_ADC_MIN_60_PSI         ((1 << 14) * 0.1)
#define GATEWAY_SENSORS_ADC_TO_PRS_SCALE_60_PSI      ((GATEWAY_SENSORS_PRS_MAX_60PSI - GATEWAY_SENSORS_PRS_MIN_60_PSI) / (GATEWAY_SENSORS_PRS_ADC_MAX_60_PSI - GATEWAY_SENSORS_PRS_ADC_MIN_60_PSI))
#define GATEWAY_SENSORS_ADC_TO_PRS_OFFSET_60_PSI     (-GATEWAY_SENSORS_ADC_TO_PRS_SCALE_60_PSI * GATEWAY_SENSORS_PRS_ADC_MIN_60_PSI + GATEWAY_SENSORS_PRS_MIN_60_PSI)

#define GATEWAY_SENSORS_PRS_MIN_100_PSI            101325.0    // Pa
#define GATEWAY_SENSORS_PRS_MAX_100PSI             (413685.438 + GATEWAY_SENSORS_PRS_MIN_100_PSI) // Pa
#define GATEWAY_SENSORS_PRS_ADC_MAX_100_PSI         ((1 << 14) * 0.9)
#define GATEWAY_SENSORS_PRS_ADC_MIN_100_PSI         ((1 << 14) * 0.1)
#define GATEWAY_SENSORS_ADC_TO_PRS_SCALE_100_PSI    ((GATEWAY_SENSORS_PRS_MAX_100PSI - GATEWAY_SENSORS_PRS_MIN_100_PSI) / (GATEWAY_SENSORS_PRS_ADC_MAX_100_PSI - GATEWAY_SENSORS_PRS_ADC_MIN_100_PSI))
#define GATEWAY_SENSORS_ADC_TO_PRS_OFFSET_100_PSI   (-GATEWAY_SENSORS_ADC_TO_PRS_SCALE_100_PSI * GATEWAY_SENSORS_PRS_ADC_MIN_100_PSI + GATEWAY_SENSORS_PRS_MIN_100_PSI)

#define SATURATE_CURRENT(x) SATURATE(x, (GATEWAY_DRIVER_MIN_COMMAND + 1.0), (GATEWAY_DRIVER_MAX_COMMAND - 1.0))

#endif // _LOW_LEVEL_CONTROL_GATEWAY_DATA_TYPES_H_
