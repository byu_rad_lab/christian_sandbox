/*
 * @file    current_reference_generator.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Task that generates valve current references to feed into the current controller
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _LOW_LEVEL_CONTROL_CURRENT_REFERENCE_GENERATOR_H_
#define _LOW_LEVEL_CONTROL_CURRENT_REFERENCE_GENERATOR_H_

#include "low_level_control/ros_utils.h"

#include "king_louie/sensors_msg.h"
#include "king_louie/current_controllers_msg.h"
#include "king_louie/set_current_ref_msg.h"
#include "king_louie/current_msg.h"

#include <kl_pneubotics_primitives/pneubotics_object.h>
#include <kl_pneubotics_primitives/queue.h>

namespace king_louie
{

class CurrentRefGen: public RosTask
{
public:
    CurrentRefGen(const std::string &name, const std::string &paramServerBasePath,
            pneubotics::queue::Queue<sensors_msg> *sensorDataQueue,
            pneubotics::queue::Queue<current_controllers_msg> *crtlDataQueue);
    ~CurrentRefGen();

    void setCurrentRef(const king_louie::current_msg &msg);

protected:
    virtual void loadParams(void);

    virtual void doRun(void);
    virtual void doInit(void);

    virtual void getInputs(void);
    virtual void setOutputs(void);

    /*
     * Pointers to input/output queues
     */
    pneubotics::queue::Queue<sensors_msg> *sensorDataQueue_;
    pneubotics::queue::Queue<current_controllers_msg> *curCtrlDataQueue_;

    pneubotics::queue::Queue<current_msg> currentRefUpdateRequests_;

    /*
     * Local copies of data
     */
    current_controllers_msg ctrlData_;
    sensors_msg sensorData_;
};

}  // namespace low_level_control

#endif // _LOW_LEVEL_CONTROL_CURRENT_REFERENCE_GENERATOR_H_
