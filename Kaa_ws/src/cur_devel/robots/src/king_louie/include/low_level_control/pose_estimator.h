/*
 * @file    pose_estimator.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Task that estimates the system pose
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _LOW_LEVEL_CONTROL_POSE_ESTIMATOR_H_
#define _LOW_LEVEL_CONTROL_POSE_ESTIMATOR_H_

#include "low_level_control/ros_utils.h"

#include "low_level_control/gateway_data_types.h"

#include "king_louie/sensors_msg.h"
#include "king_louie/pose_msg.h"

#include <kl_pneubotics_primitives/time.h>
#include <kl_pneubotics_primitives/loop.h>
#include <kl_pneubotics_primitives/queue.h>

#include <eigen3/Eigen/Dense>

namespace king_louie
{
namespace exception
{
class PoseException: public LowLevelControlException
{
public:
    PoseException(const std::string &msg) :
            LowLevelControlException(msg)
    {
    }
    ~PoseException() throw ()
    {
    }
};

}  // namespace exception

class IMUParams
{
public:
    IMUParams() :
            aSat_(0.0), wSat_(0.0), ka_(0.0), km_(0.0)
    {
    }
    ~IMUParams()
    {
    }

    double aSat_;
    double wSat_;
    double ka_;
    double km_;
};

class IMUFilter
{
public:
    IMUFilter();
    ~IMUFilter();

    void UpdateFilter(const double * acc, const double * gyr, const double * mag, double dt);
    void ResetFilter(const double *acc, const double*mag);
    void loadParams(void);
    void setParamServerPath(const std::string &path);

    Eigen::Matrix3d getRwmEstimate(void);
    Eigen::Vector3d getAccEst(void);
    Eigen::Vector3d getMagEst(void);
    Eigen::Vector3d getAccErr(void);
    Eigen::Vector3d getMagErr(void);

protected:
    std::string paramServerPath_;

    Eigen::Vector3d a_;
    Eigen::Vector3d w_;
    Eigen::Vector3d m_;

    Eigen::Vector3d a0_;
    Eigen::Vector3d m0_;

    Eigen::Vector3d aEst_;
    Eigen::Vector3d mEst_;

    Eigen::Vector3d aErr_;
    Eigen::Vector3d mErr_;

    Eigen::Matrix3d rWM_;

private:
    IMUParams p_;
};

class PoseEstimator: public RosTask
{
public:
    PoseEstimator(const std::string &name, const std::string &paramServerBasePath,
            pneubotics::queue::Queue<sensors_msg> *sensorDataQueue,
            pneubotics::queue::Queue<pose_msg> *poseDataQueue);
    ~PoseEstimator();

protected:
    virtual void loadParams(void);

    virtual void doRun(void);
    virtual void doInit(void);

    virtual void getInputs(void);
    virtual void setOutputs(void);

    /*
     * Pointers to input/output queues
     */
    pneubotics::queue::Queue<sensors_msg> *sensorDataQueue_;
    pneubotics::queue::Queue<pose_msg> *poseDataQueue_;

    /*
     * Local copies of data
     */
    pose_msg poseData_;
    sensors_msg sensorData_;

    IMUFilter imuFilter_[GATEWAY_SENSOR_BOARDS_PER_GATEWAY];
    bool initFilter_;
};

}  // namespace low_level_control

#endif // _LOW_LEVEL_CONTROL_POSE_ESTIMATOR_H_
