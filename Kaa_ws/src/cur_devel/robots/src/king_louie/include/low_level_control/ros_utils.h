/*
 * @file    ros_utils.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Provides some useful wrappers around ROS related functionality
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _LOW_LEVEL_CONTROL_ROS_PARAMS_H_
#define _LOW_LEVEL_CONTROL_ROS_PARAMS_H_

#include "ros/ros.h"

#include <kl_pneubotics_primitives/exception.h>
#include <kl_pneubotics_primitives/task.h>

namespace king_louie
{
namespace exception
{

class LowLevelControlException: public pneubotics::exception::Exception
{
public:
    LowLevelControlException(const std::string &msg) :
            pneubotics::exception::Exception(msg)
    {
    }
    ~LowLevelControlException() throw ()
    {
    }
};

class ParamLoadException: public LowLevelControlException
{
public:
    ParamLoadException(const std::string &msg) :
            LowLevelControlException(msg)
    {
    }
    ~ParamLoadException() throw ()
    {
    }
};

}  // namespace exception

class RosUtils
{
public:
    RosUtils()
    {
    }
    ~RosUtils()
    {
    }
    /*
     * Load a parameter from the parameter server and throw ParamLoadException if we fail.
     */
    static void getParam(const std::string &key, bool &b);
    static void getParam(const std::string &key, int &i);
    static void getParam(const std::string &key, double &d);
    static void getParam(const std::string &key, std::string &s);

    static void setParam(const std::string &key, const bool &b);
    static void setParam(const std::string &key, const int &i);
    static void setParam(const std::string &key, const double &d);
    static void setParam(const std::string &key, const std::string &s);
};

class RosTask: public pneubotics::task::Task
{
public:
    RosTask(const std::string &name, const std::string &paramServerPath);
    virtual ~RosTask(void);

    virtual void init(void);
    virtual void run(void);

protected:
    virtual void loadParams(void) = 0;

    virtual void doRun(void) = 0;
    virtual void doInit(void) = 0;

    virtual void getInputs(void) = 0;
    virtual void setOutputs(void) = 0;

    std::string paramServerPath_;

    ros::Time time_;
    ros::Time timePrev_;
    ros::Duration timeDiff_;
};

}  // namespace low_level_control

#endif // _LOW_LEVEL_CONTROL_ROS_PARAMS_H_
