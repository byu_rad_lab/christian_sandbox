/*
 * @file    robot_mapping.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Provide a mapping from node and axis indices to parts of the physical robot
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _LOW_LEVEL_CONTROL_ROBOT_MAPPING_H_
#define _LOW_LEVEL_CONTROL_ROBOT_MAPPING_H_

namespace king_louie
{

struct NodeAddress
{
    int board;
    int axis;
};

struct Actuator
{
    NodeAddress driverAddrss;
    NodeAddress sensorAddrss;
};

typedef enum
{
    ACT_RSPL = 0,   // Right shoulder proximal lateral
    ACT_RSPM,       // Right shoulder proximal medial
    ACT_RSDP,       // Right shoulder distal posterior
    ACT_RSDA,       // Right shoulder distal anterior

    ACT_REP,        // Right elbow posterior
    ACT_REA,        // Right elbow anterior
    ACT_RWPP,       // Right wrist proximal posterior
    ACT_RWPA,       // Right wrist  proximal anterior

    ACT_RWDL,       // Right wrist distal lateral
    ACT_RWDM,       // Right wrist distal medial
    ACT_RGRIP,      // Right gripper

    ACT_LSPL,       // Left shoulder proximal lateral
    ACT_LSPM,       // Left shoulder proximal medial
    ACT_LSDP,       // Left shoulder distal posterior
    ACT_LSDA,       // Left shoulder distal anterior

    ACT_LEP,        // Left elbow posterior
    ACT_LEA,        // Left elbow anterior
    ACT_LWPP,       // Left wrist proximal posterior

    ACT_LWPA,       // Left wrist  proximal anterior
    ACT_LWDL,       // Left wrist distal lateral
    ACT_LWDM,       // Left wrist distal medial
    ACT_LGRIP,      // Left gripper

    ACT_RHIP,       // Right hip
    ACT_LHIP,       // Left hip
    ACT_ABS,        // Abdominals

    ACT_NUM_ACTUATORS  // Must be last element in enum

} body_location_t;

const Actuator RobotMap[ACT_NUM_ACTUATORS] =
        {
                // Driver   Sensor/Feedback for driver
                { { 0, 0 }, { 0, 0 } },  // ACT_RSPL,       // Right shoulder proximal lateral
                { { 0, 1 }, { 0, 1 } },  // ACT_RSPM,       // Right shoulder proximal medial
                { { 0, 2 }, { 0, 2 } },  // ACT_RSDP,       // Right shoulder distal posterior
                { { 0, 3 }, { 0, 3 } },  // ACT_RSDA,       // Right shoulder distal anterior

                { { 1, 0 }, { 1, 0 } },  // ACT_REP,        // Right elbow posterior
                { { 1, 1 }, { 1, 1 } },  // ACT_REA,        // Right elbow anterior
                { { 1, 2 }, { 1, 2 } },  // ACT_RWPP,       // Right wrist proximal posterior
                { { 1, 3 }, { 1, 3 } },  // ACT_RWPA,       // Right wrist  proximal anterior

                { { 2, 0 }, { 2, 0 } },  // ACT_RWDL,       // Right wrist distal lateral
                { { 2, 1 }, { 2, 1 } },  // ACT_RWDM,       // Right wrist distal medial
                { { 2, 3 }, { 2, 2 } },  // ACT_RGRIP,      // Right gripper

                { { 3, 0 }, { 3, 0 } },  // ACT_LSPL,       // Left shoulder proximal lateral
                { { 3, 1 }, { 3, 1 } },  // ACT_LSPM,       // Left shoulder proximal medial
                { { 3, 2 }, { 3, 2 } },  // ACT_LSDP,       // Left shoulder distal posterior
                { { 3, 3 }, { 3, 3 } },  // ACT_LSDA,       // Left shoulder distal anterior

                { { 4, 0 }, { 4, 0 } },  // ACT_LEP,        // Left elbow posterior
                { { 4, 1 }, { 4, 1 } },  // ACT_LEA,        // Left elbow anterior
                { { 4, 2 }, { 4, 2 } },  // ACT_LWPP,       // Left wrist proximal posterior
                { { 4, 3 }, { 4, 3 } },  // ACT_LWPA,       // Left wrist  proximal anterior

                { { 5, 0 }, { 5, 0 } },  // ACT_LWDL,       // Left wrist distal lateral
                { { 5, 1 }, { 5, 1 } },  // ACT_LWDM,       // Left wrist distal medial
                { { 5, 3 }, { 5, 2 } },  // ACT_LGRIP,      // Left gripper

                { { 6, 1 }, { 6, 1 } },  // ACT_RHIP,       // Right hip
                { { 6, 2 }, { 6, 2 } },  // ACT_LHIP,       // Left hip
                { { 6, 3 }, { 6, 3 } },  // ACT_ABS,        // Abdominals
        };

}  // namespace low_level_control

#endif // _LOW_LEVEL_CONTROL_ROBOT_MAPPING_H_
