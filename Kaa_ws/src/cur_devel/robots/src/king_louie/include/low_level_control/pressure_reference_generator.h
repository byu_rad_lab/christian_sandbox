/*
 * @file    pressure_reference_generator.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Task that generates pressure references to feed into the pressure controller
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _LOW_LEVEL_CONTROL_PRESSURE_REFERENCE_GENERATOR_H_
#define _LOW_LEVEL_CONTROL_PRESSURE_REFERENCE_GENERATOR_H_

#include "low_level_control/ros_utils.h"

#include "king_louie/sensors_msg.h"
#include "king_louie/pressure_controllers_msg.h"
#include "king_louie/set_pressure_ref_msg.h"

#include <kl_pneubotics_primitives/pneubotics_object.h>
#include <kl_pneubotics_primitives/queue.h>

namespace king_louie
{

class PressureRefGen: public RosTask
{
public:
    PressureRefGen(const std::string &name, const std::string &paramServerBasePath,
            pneubotics::queue::Queue<sensors_msg> *sensorDataQueue,
            pneubotics::queue::Queue<pressure_controllers_msg> *crtlDataQueue);
    ~PressureRefGen();

    void setPressureRef(const king_louie::set_pressure_ref_msg &msg);

protected:
    virtual void loadParams(void);

    virtual void doRun(void);
    virtual void doInit(void);

    virtual void getInputs(void);
    virtual void setOutputs(void);

    /*
     * Pointers to input/output queues
     */
    pneubotics::queue::Queue<sensors_msg> *sensorDataQueue_;
    pneubotics::queue::Queue<pressure_controllers_msg> *prsCtrlDataQueue_;

    pneubotics::queue::Queue<king_louie::set_pressure_ref_msg> pressureRefUpdateRequests_;

    /*
     * Local copies of data
     */
    pressure_controllers_msg ctrlData_;
    sensors_msg sensorData_;
};

}  // namespace low_level_control

#endif // _LOW_LEVEL_CONTROL_PRESSURE_REFERENCE_GENERATOR_H_
