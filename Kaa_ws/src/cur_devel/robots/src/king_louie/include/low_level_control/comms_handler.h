/*
 * @file    comms_handler.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Task to handle reading sensor data and writing driver data to/from from embedded system.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _LOW_LEVEL_CONTROL_COMMS_HANDLER_H_
#define _LOW_LEVEL_CONTROL_COMMS_HANDLER_H_

#include "low_level_control/ros_utils.h"

#include "king_louie/sensors_msg.h"
#include "king_louie/drivers_msg.h"

#include "low_level_control/gateway_data_types.h"

#include <kl_pneubotics_primitives/socket_udp.h>
#include <kl_pneubotics_primitives/socket_exception.h>
#include <kl_pneubotics_primitives/pneubotics_object.h>
#include <kl_pneubotics_primitives/queue.h>

namespace king_louie
{
namespace exception
{

class CommsException: public LowLevelControlException
{
public:
    CommsException(const std::string &msg) :
            LowLevelControlException(msg)
    {
    }
    ~CommsException() throw ()
    {
    }
};

class CommsSendException: public CommsException
{
public:
    CommsSendException(const std::string &msg) :
            CommsException(msg)
    {
    }
    ~CommsSendException() throw ()
    {
    }
};

class CommsRecvException: public CommsException
{
public:
    CommsRecvException(const std::string &msg) :
            CommsException(msg)
    {
    }
    ~CommsRecvException() throw ()
    {
    }
};

class CommsRecvTimeoutException: public CommsRecvException
{
public:
    CommsRecvTimeoutException(const std::string &msg) :
            CommsRecvException(msg)
    {
    }
    ~CommsRecvTimeoutException() throw ()
    {
    }
};
}  // namespace exception

class CommsHandler: public RosTask
{
public:
    CommsHandler(const std::string &name, const std::string &paramServerBasePath,
            pneubotics::queue::Queue<sensors_msg> *sensorDataQueue,
            pneubotics::queue::Queue<drivers_msg> *driverDataQueue);
    virtual ~CommsHandler();

    /*
     * Send whatever is in driverDataQueue_ to the embedded system
     */
    void send(void);

    /*
     * Read sensor data from embedded system and push it to sensorDataQueue_
     */
    void recv(void);

protected:
    /*
     * Virtual method overrides from the RosTask class
     */
    virtual void doInit(void);
    virtual void doRun(void);
    virtual void loadParams(void);
    virtual void getInputs(void);
    virtual void setOutputs(void);

    /*
     * Pointers to input/output queues
     */
    pneubotics::queue::Queue<sensors_msg> *sensorDataQueue_;
    pneubotics::queue::Queue<drivers_msg> *driverDataQueue_;
    sensors_msg sensorData_;
    drivers_msg driverData_;

    /*
     * Socket read/write variables
     */
    int packetSize_;
    int readTimeoutUs_;
    pneubotics::socket::SocketUdp rxSocket_;
    pneubotics::socket::SocketUdp txSocket_;
    char rxBuffer_[GATEWAY_SENSOR_READINGS_PACKET_SIZE];
    char txBuffer_[GATEWAY_DRIVER_COMMANDS_PACKET_SIZE];

    /*
     * Sensor calibration variables
     */
    double accCalOffset_[GATEWAY_SENSOR_BOARDS_PER_GATEWAY][GATEWAY_SENSORS_ACC_PER_SENSOR_BOARD];
    double gyrCalOffset_[GATEWAY_SENSOR_BOARDS_PER_GATEWAY][GATEWAY_SENSORS_GYR_PER_SENSOR_BOARD];
    double magCalOffset_[GATEWAY_SENSOR_BOARDS_PER_GATEWAY][GATEWAY_SENSORS_MAG_PER_SENSOR_BOARD];
    double prsCalOffset_[GATEWAY_SENSOR_BOARDS_PER_GATEWAY][GATEWAY_SENSORS_PRS_PER_SENSOR_BOARD];
    int prsCalRating_[GATEWAY_SENSOR_BOARDS_PER_GATEWAY];
};

}  // namespace low_level_control

#endif // _LOW_LEVEL_CONTROL_COMMS_HANDLER_H_
