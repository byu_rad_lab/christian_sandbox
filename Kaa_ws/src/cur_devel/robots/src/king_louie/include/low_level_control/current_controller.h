/*
 * @file    current_controller.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Task that runs open loop current controllers
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#ifndef _LOW_LEVEL_CURRENT_CONTROLLER_H_
#define _LOW_LEVEL_CURRENT_CONTROLLER_H_

#include "king_louie/current_controllers_msg.h"
#include "king_louie/drivers_msg.h"
#include "king_louie/sensors_msg.h"

#include "low_level_control/gateway_data_types.h"

#include "low_level_control/ros_utils.h"

#include <kl_pneubotics_primitives/queue.h>

namespace king_louie
{
namespace exception
{

class CurrentControllerException: public LowLevelControlException
{
public:
    CurrentControllerException(const std::string &msg) :
            LowLevelControlException(msg)
    {
    }
    ~CurrentControllerException() throw ()
    {
    }
};

}  // namespace exception

class CurrentController: public RosTask
{
public:
    CurrentController(const std::string &name, const std::string &paramServerBasePath,
            pneubotics::queue::Queue<current_controllers_msg> *prsCtrlDataQueue_,
            pneubotics::queue::Queue<drivers_msg> *driverDataQueue_);
    ~CurrentController();

protected:
    void loadParams(void);

    virtual void doRun(void);
    virtual void doInit(void);

    virtual void getInputs(void);
    virtual void setOutputs(void);

    /*
     * Pointers to input/output queues
     */
    pneubotics::queue::Queue<drivers_msg> *driverDataQueue_;
    pneubotics::queue::Queue<current_controllers_msg> *prsCtrlDataQueue_;

    /*
     * Local copies of data
     */
    drivers_msg driverData_;
    current_controllers_msg ctrlData_;
    sensors_msg sensorData_;
};

}  // namespace low_level_control

#endif // _LOW_LEVEL_CURRENT_CONTROLLER_H_
