/*
 * @file    low_level_control_node.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Main entry point for the low level control node application.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "low_level_control/ros_utils.h"

#include "low_level_control/low_level_controller.h"

#include "ros/ros.h"

/*
 * Main entry point for the low level control node application
 */
int main(int argc, char **argv)
{
    /*
     * Initialize ROS
     */
    std::string name("LowLevelControlNode");
    ros::init(argc, argv, name);

    /*
     * Create the low level controller
     */
    king_louie::LowLevelController lowLevelControl("LowLevelController", "/" + name);

    /*
     * Initialize the low level controller
     */
    lowLevelControl.init();

    /*
     * Run the low level controller
     */
    lowLevelControl.run();

    return 0;
}
