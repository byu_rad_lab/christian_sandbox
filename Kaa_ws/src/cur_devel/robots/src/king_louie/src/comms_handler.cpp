/*
 * @file    comms_handler.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Task to handle reading sensor data and writing driver data to/from from embedded system.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "low_level_control/comms_handler.h"

#include "low_level_control/ros_utils.h"

#include "ros/ros.h"

namespace king_louie
{

static double getSensorValue(const char *pData, int boardIndex, int valueIndex)
{
    int idx = GATEWAY_SENSOR_READINGS_BYTE_OFFSET
            + GATEWAY_SENSOR_READING_SIZE_BYTES * boardIndex + GATEWAY_BYTES_PER_DRIVER_VALUE * valueIndex;
    int16_t upper = pData[idx + 1] & 0x00FF;
    upper = upper << 8;
    int16_t lower = pData[idx] & 0x00FF;
    int16_t val = (upper & 0xFF00) | (lower & 0x00FF);
    return ((double) val);
}

static void addValveCommand(char *buffer, int boardIndex, int valveIndex, double value)
{
    int idx = boardIndex * GATEWAY_DRIVERS_BYTES_PER_BOARD + valveIndex * GATEWAY_BYTES_PER_DRIVER_VALUE
            + GATEWAY_DRIVER_COMMANDS_BYTE_OFFSET;

    /*
     * Limit value to be within acceptable range
     */
    value = SATURATE_CURRENT(value);

    int16_t val = (int16_t) value;

    buffer[idx] = val & 0xFF;
    buffer[idx + 1] = (val >> 8) & 0xFF;
}

CommsHandler::CommsHandler(const std::string &name, const std::string &paramServerBasePath,
        pneubotics::queue::Queue<sensors_msg> *sensorDataQueue,
        pneubotics::queue::Queue<drivers_msg> *driverDataQueue) :
        RosTask(name, paramServerBasePath + "/" + name), packetSize_(0), readTimeoutUs_(0)

{
    sensorDataQueue_ = sensorDataQueue;
    driverDataQueue_ = driverDataQueue;
    ROS_INFO("%s", paramServerPath_.c_str());
}

CommsHandler::~CommsHandler()
{
}

void CommsHandler::doRun(void)
{
    throw exception::CommsException("run() and doRun() methods not implemented for " + name_ +
            " use send() and recv() methods instead");
}

void CommsHandler::doInit(void)
{
    /*
     * Initialize TX buffer
     */
    txBuffer_[0] = GATEWAY_DRIVER_PACKET_HEADER;
    for (int i = 1; i < GATEWAY_DRIVER_COMMANDS_PACKET_SIZE; i++)
    {
        txBuffer_[i] = 0;
    }

    /*
     * Initialize RX socket
     */
    try
    {
        rxSocket_.open(GATEWAY_SENSOR_DATA_IP, GATEWAY_SENSOR_DATA_PORT);
        rxSocket_.bind();
    }
    catch (pneubotics::exception::SocketBindException &err)
    {
        ROS_FATAL("Failed to connect to embedded system: %s", err.what());
        throw;
    }
    catch (pneubotics::exception::SocketOpenException &err)
    {
        ROS_FATAL("Failed to connect to embedded system: %s", err.what());
        throw;
    }

    ROS_INFO("%s successfully connected RX socket at %s:%d",
            name_.c_str(), GATEWAY_SENSOR_DATA_IP, GATEWAY_SENSOR_DATA_PORT);

    /*
     * Initialize TX socket
     */
    try
    {
        txSocket_.open(GATEWAY_DRIVER_DATA_IP, GATEWAY_DRIVER_DATA_PORT);
    }
    catch (pneubotics::exception::SocketBindException &err)
    {
        ROS_FATAL("Failed to connect to embedded system: %s", err.what());
        throw;
    }
    catch (pneubotics::exception::SocketOpenException &err)
    {
        ROS_FATAL("Failed to connect to embedded system: %s", err.what());
        throw;
    }
    ROS_INFO("%s successfully connected TX socket at %s:%d",
            name_.c_str(), GATEWAY_DRIVER_DATA_IP, GATEWAY_DRIVER_DATA_PORT);

}

void CommsHandler::getInputs(void)
{
    driverData_ = driverDataQueue_->front_blocking();
}

void CommsHandler::setOutputs(void)
{
    sensorDataQueue_->push(sensorData_);
}

void CommsHandler::recv(void)
{
    if (!isInitialized())
    {
        throw pneubotics::exception::TaskInitException(
                "Cannot run " + name_ + " instance of Task object before initialization");
    }

    /*
     * Read a packet
     */
    try
    {
        packetSize_ = rxSocket_.recv(rxBuffer_, GATEWAY_SENSOR_READINGS_PACKET_SIZE, readTimeoutUs_);
    }
    catch (pneubotics::exception::SocketTimeout &err)
    {
        throw exception::CommsRecvTimeoutException(name_ + " timed out while reading socket");
    }

    /*
     * Parse packet if it is the correct size
     */
    if (packetSize_ == GATEWAY_SENSOR_READINGS_PACKET_SIZE)
    {
        sensorData_.time = ros::Time::now();

        for (int i = 0; i < GATEWAY_SENSOR_BOARDS_PER_GATEWAY; i++)
        {
            for (int j = 0; j < GATEWAY_SENSORS_ACC_PER_SENSOR_BOARD; j++)
            {
                sensorData_.boards[i].acc[j] = getSensorValue(rxBuffer_, i, j) * GATEWAY_SENSORS_ADC_TO_ACC_SCALE
                        + GATEWAY_SENSORS_ADC_TO_ACC_OFFSET - accCalOffset_[i][j];
            }
            for (int j = 0; j < GATEWAY_SENSORS_GYR_PER_SENSOR_BOARD; j++)
            {
                sensorData_.boards[i].gyr[j] = getSensorValue(rxBuffer_, i, j + 3) * GATEWAY_SENSORS_ADC_TO_GYR_SCALE
                        + GATEWAY_SENSORS_ADC_TO_GYR_OFFSET - gyrCalOffset_[i][j];
            }
            for (int j = 0; j < GATEWAY_SENSORS_MAG_PER_SENSOR_BOARD; j++)
            {
                sensorData_.boards[i].mag[j] = getSensorValue(rxBuffer_, i, j + 6) * GATEWAY_SENSORS_ADC_TO_MAG_SCALE
                        + GATEWAY_SENSORS_ADC_TO_MAG_OFFSET - magCalOffset_[i][j];
            }

            for (int j = 0; j < GATEWAY_SENSORS_PRS_PER_SENSOR_BOARD; j++)
            {
                if (prsCalRating_[i] == 60)
                {
                    sensorData_.boards[i].prs[j] = getSensorValue(rxBuffer_, i, j + 9)
                            * GATEWAY_SENSORS_ADC_TO_PRS_SCALE_60_PSI
                            + GATEWAY_SENSORS_ADC_TO_PRS_OFFSET_60_PSI - prsCalOffset_[i][j];
                }
                else if (prsCalRating_[i] == 100)
                {
                    sensorData_.boards[i].prs[j] = getSensorValue(rxBuffer_, i, j + 9)
                            * GATEWAY_SENSORS_ADC_TO_PRS_SCALE_100_PSI
                            + GATEWAY_SENSORS_ADC_TO_PRS_OFFSET_100_PSI - prsCalOffset_[i][j];
                }
                else
                {
                }
            }
        }
    }
    else
    {
        std::stringstream ss;
        ss << name_ << " wrong number of packets read: got " << packetSize_ << " expected "
                << GATEWAY_SENSOR_READINGS_PACKET_SIZE;
        throw exception::CommsRecvException(ss.str());
    }

    setOutputs();
}

void CommsHandler::send(void)
{
    if (!isInitialized())
    {
        throw pneubotics::exception::TaskInitException(
                "Cannot run " + name_ + " instance of Task object before initialization");
    }

    getInputs();

    /*
     * Stuff valve command buffer and send it
     */
    for (int i = 0; i < GATEWAY_DRIVER_BOARDS_PER_GATEWAY; i++)
    {
        for (int j = 0; j < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; j++)
        {
            addValveCommand(txBuffer_, i, j, driverData_.boards[i].current[j]);
        }
    }
    txSocket_.send(txBuffer_, GATEWAY_DRIVER_COMMANDS_PACKET_SIZE);
}

void CommsHandler::loadParams(void)
{
    std::stringstream paramName;
    paramName << paramServerPath_ << "/read_timeout_us";
    RosUtils::getParam(paramName.str(), readTimeoutUs_);

    for (int i = 0; i < GATEWAY_SENSOR_BOARDS_PER_GATEWAY; i++)
    {
        for (int j = 0; j < GATEWAY_SENSORS_ACC_PER_SENSOR_BOARD; j++)
        {
            paramName.str("");
            paramName << paramServerPath_ << "/acc_off_" << i << "_" << j;
            RosUtils::getParam(paramName.str(), accCalOffset_[i][j]);
        }

        for (int j = 0; j < GATEWAY_SENSORS_GYR_PER_SENSOR_BOARD; j++)
        {
            paramName.str("");
            paramName << paramServerPath_ << "/gyr_off_" << i << "_" << j;
            RosUtils::getParam(paramName.str(), gyrCalOffset_[i][j]);
        }

        for (int j = 0; j < GATEWAY_SENSORS_MAG_PER_SENSOR_BOARD; j++)
        {
            paramName.str("");
            paramName << paramServerPath_ << "/mag_off_" << i << "_" << j;
            RosUtils::getParam(paramName.str(), magCalOffset_[i][j]);
        }

        for (int j = 0; j < GATEWAY_SENSORS_PRS_PER_SENSOR_BOARD; j++)
        {
            paramName.str("");
            paramName << paramServerPath_ << "/prs_off_" << i << "_" << j;
            RosUtils::getParam(paramName.str(), prsCalOffset_[i][j]);
        }

        paramName.str("");
        paramName << paramServerPath_ << "/pressure_sensor_rating_" << i;
        RosUtils::getParam(paramName.str(), prsCalRating_[i]);
        if ((prsCalRating_[i] != 60) && (prsCalRating_[i] != 100))
        {
            std::stringstream ss;
            ss << "Invalid value loader for " << paramName.str() << "must be 60 or 100, got " << prsCalRating_[i];
            throw exception::ParamLoadException(ss.str());
        }
    }
}

}  // namespace low_level_control
