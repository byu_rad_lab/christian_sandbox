/*
 * @file    low_level_controller.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Task that provides an interface between ROS and the real time loop
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "low_level_control/low_level_controller.h"

#include "low_level_control/ros_utils.h"

#include <kl_pneubotics_primitives/time.h>

namespace king_louie
{
LowLevelController::LowLevelController(const std::string &name, const std::string &paramServerBasePath) :
        RosTask(name, paramServerBasePath + "/" + name), rosInterfaceRate_(0.0),
                sensorDataQueue_(1), driverDataQueue_(1), poseDataQueue_(1), prsCtrlDataQueue_(1), curCtrlDataQueue_(1),
                realTimeLoopPeriodSec_(0.0), rosPublisherQueueSize_(0), rosSubscriberQueueSize_(0)
{
    realTimeLoop_ = NULL;
    ROS_INFO("%s", paramServerPath_.c_str());
    t = 0;
    t_last = 0;
    h_min = 10000;
    raw_magnetometer_msgs.resize(7);
    raw_imu_msgs.resize(7);
    rawImusPub_.resize(7);
    rawMagnetometersPub_.resize(7);

}

LowLevelController::~LowLevelController()
{
    if (realTimeLoop_)
    {
        if (realTimeLoop_->isAlive())
        {
            realTimeLoop_->stop();
        }
        delete realTimeLoop_;
    }
}

void LowLevelController::setLoggingCallback(const std_msgs::Bool &msg)
{
    realTimeLoop_->setLogging(msg.data);
    if (msg.data)
    {
        ROS_INFO("Enabling logging");
    }
    else
    {
        ROS_INFO("Disabling logging");
    }
}

void LowLevelController::setPressureRefCallback(const king_louie::set_pressure_ref_msg &msg)
{
    ROS_INFO("Setting pressure reference to %fPa, %fPa/s for board %d, circuit %d", msg.refP, msg.refD, msg.boardIndex,
            msg.axisIndex);
    realTimeLoop_->setPressureRef(msg);
}

void LowLevelController::setCurrentRefCallback(const king_louie::current_msg &msg)
{
    //ROS_INFO("Setting current reference to %fmA for board %d, circuit %d", msg.value, msg.boardIndex,
            //msg.axisIndex);
    realTimeLoop_->setCurrentRef(msg);
}

void LowLevelController::setPressureCtrlGains(const king_louie::set_pressure_ctrl_gains_msg &msg)
{
    ROS_INFO("Setting pressure controller gains to %fkp %fki %fkd for board %d, circuit %d", msg.kp, msg.ki, msg.kd,
            msg.boardIndex, msg.axisIndex);
    LowLevelController::realTimeLoop_->resetIntegrators();
    LowLevelController::realTimeLoop_->setPressureCtrlGains(msg);
}

void LowLevelController::setControllerMode(const std_msgs::Int16 &msg)
{
    ROS_INFO("Setting controller mode to %d", msg.data);
    LowLevelController::realTimeLoop_->resetIntegrators();
    LowLevelController::realTimeLoop_->setControllerMode(msg);
}

void LowLevelController::doInit(void)
{
    /*
     * Create the real time loop
     */
    realTimeLoop_ = new RealTimeLoop("RealTimeLoop", paramServerPath_,
            pneubotics::time::Duration(realTimeLoopPeriodSec_),
            &sensorDataQueue_, &driverDataQueue_, &poseDataQueue_, &prsCtrlDataQueue_, &curCtrlDataQueue_);

    /*
     * Configure ROS publishers
     */
    sensorsPub_ = nodeHandle_.advertise<sensors_msg>("Sensors", rosPublisherQueueSize_);
    posePub_ = nodeHandle_.advertise<pose_msg>("Pose", rosPublisherQueueSize_);
    prsCtrlPub_ = nodeHandle_.advertise<king_louie::pressure_controllers_msg>("PressureController",
            rosPublisherQueueSize_);
    curCtrlPub_ = nodeHandle_.advertise<king_louie::current_controllers_msg>("CurrentController",
            rosPublisherQueueSize_);
    driverCmdPub_ = nodeHandle_.advertise<drivers_msg>("Drivers", rosPublisherQueueSize_);

    /*
     * Configure ROS subscribers
     */
    setLoggingSub_ = nodeHandle_.subscribe("SetLogging", rosSubscriberQueueSize_,
					   &LowLevelController::setLoggingCallback, this, ros::TransportHints().tcpNoDelay());

    setPressureRefSub_ = nodeHandle_.subscribe("SetPressureRef", rosSubscriberQueueSize_,
					       &LowLevelController::setPressureRefCallback, this, ros::TransportHints().tcpNoDelay());

    setPressureCtrlGains_ = nodeHandle_.subscribe("SetPressureCtrlGains", rosSubscriberQueueSize_,
						  &LowLevelController::setPressureCtrlGains, this, ros::TransportHints().tcpNoDelay());

    setCurrentRef_ = nodeHandle_.subscribe("SetCurrentRef", rosSubscriberQueueSize_,
					   &LowLevelController::setCurrentRefCallback, this, ros::TransportHints().tcpNoDelay());

    setControllerMode_ = nodeHandle_.subscribe("SetControllerMode", rosSubscriberQueueSize_,
					       &LowLevelController::setControllerMode, this, ros::TransportHints().tcpNoDelay());
}

void LowLevelController::doRun(void)
{

    /*
     * Start the real time loop
     */
    realTimeLoop_->start();

    /*
     * Sleep for a short time before we start trying to publish data to allow all queues to populate
     */
    ros::Duration(0.1).sleep();

    ros::Rate r(rosInterfaceRate_);

    //pub_data = n.advertise<data_msg>("/Data", 1);
    pub_pressure = n.advertise<sensors>("/PressureData",1);

    for(int i = 0; i < 7; i++)
      {
	std::string str = boost::lexical_cast<std::string>(i);
	rawImusPub_[i] = n.advertise<sensor_msgs::Imu>("imu_"+str+"/data_raw", 1);
	rawMagnetometersPub_[i] = n.advertise<geometry_msgs::Vector3Stamped>("imu_"+str+"/mag_wrong", 1);
      }

    //rawImuPub_ = nodeHandle_.advertise<sensor_msgs::Imu>("imu/data_raw", 1);

    /*
     * Main ROS interface loop where publishing and subscribing happens
     */
    while (ros::ok())
    {
        /*
         * Publish all our topics
         */
        publishTopics();

        /*
         * Spin once to allow callbacks to happen
         */
        ros::spinOnce();

        /*
         * Sleep appropriate amount of time
         */
        r.sleep();
    }

    /*
     * Stop the real time loop
     */
    realTimeLoop_->stop();
}

void LowLevelController::loadParams(void)
{
    std::stringstream paramName;
    paramName << paramServerPath_ << "/real_time_loop_period_sec";
    RosUtils::getParam(paramName.str(), realTimeLoopPeriodSec_);

    double tmpPeriod;
    paramName.str("");
    paramName << paramServerPath_ << "/ros_interface_loop_period_sec";
    RosUtils::getParam(paramName.str(), tmpPeriod);
    rosInterfaceRate_ = ros::Rate(1.0 / tmpPeriod);

    paramName.str("");
    paramName << paramServerPath_ << "/ros_publisher_queue_size";
    RosUtils::getParam(paramName.str(), rosPublisherQueueSize_);

    paramName.str("");
    paramName << paramServerPath_ << "/ros_subscriber_queue_size";
    RosUtils::getParam(paramName.str(), rosSubscriberQueueSize_);
}

void LowLevelController::getInputs(void)
{
}

void LowLevelController::setOutputs(void)
{
}

void LowLevelController::publishTopics(void)
{
    /*
     * Publish all topics
     */


    //sensorsPub_.publish(sensorDataQueue_.front());

    /*ax = sensorDataQueue_.front_blocking().boards[0].acc[0];
    ay = sensorDataQueue_.front_blocking().boards[0].acc[1];
    az = sensorDataQueue_.front_blocking().boards[0].acc[2];	
    gz = sensorDataQueue_.front_blocking().boards[0].gyr[2];
    p0 = sensorDataQueue_.front_blocking().boards[0].prs[0];
    p1 = sensorDataQueue_.front_blocking().boards[0].prs[1];

    msg.t = time;
    msg.ax = ax;
    msg.ay = ay;
    msg.az = az;
    msg.gz = gz;
    msg.p0 = p0;
    msg.p1 = p1;

    pub_data.publish(msg);
    t_last = t;
    t = ros::Time::now().toSec();
    h = 1.0/(t-t_last);

    if(h < h_min && h >.5)
    {
        h_min = h;
    }*/

    for(int i = 0; i < 7; i++) //0, 3
    {
        for(int j = 0; j < 4; j++)
        {
            // std::cout << "board: " << i << "      pressure: " << j << std::endl;
            double pub_press_val;
            if(i==0 || i==3 || i ==6) 
            {
            	//we measured the pressures, and found that these boards are inproperly calibrated. See pressuredata.ods in kl_ws/src/low_level_control/src/pressure_data folder.
            	pub_press_val = sensorDataQueue_.front_blocking().boards[i].prs[j]*1.6699733715-68453.9009312556;
            }
            else
            {
            	pub_press_val = sensorDataQueue_.front_blocking().boards[i].prs[j];
            }

            msg.p.push_back(pub_press_val);
        }
        /*for(int j = 0; j < 3; j++)
        {
            //std::cout << "board: " << i << "      sensor: " << j << std::endl;
            msg.a.push_back(sensorDataQueue_.front_blocking().boards[i].acc[j]);
            msg.g.push_back(sensorDataQueue_.front_blocking().boards[i].gyr[j]);
            msg.m.push_back(sensorDataQueue_.front_blocking().boards[i].mag[j]);
	    } */
    }

    msg.flag = 1;
    pub_pressure.publish(msg);
    //msg.a.clear();
    //msg.g.clear();
    //msg.m.clear();
    msg.p.clear();

    cur_time = sensorDataQueue_.front_blocking().time.toSec();

    for(int i = 0; i < 7; i ++)
      {
	std::string str = boost::lexical_cast<std::string>(i);

	raw_imu_msgs[i].header.stamp = ros::Time(cur_time);
	raw_imu_msgs[i].header.frame_id = "imu_board_"+str;
	raw_imu_msgs[i].angular_velocity.x = sensorDataQueue_.front_blocking().boards[i].gyr[0];
	raw_imu_msgs[i].angular_velocity.y = sensorDataQueue_.front_blocking().boards[i].gyr[1];
	raw_imu_msgs[i].angular_velocity.z = sensorDataQueue_.front_blocking().boards[i].gyr[2];
	raw_imu_msgs[i].linear_acceleration.x = sensorDataQueue_.front_blocking().boards[i].acc[0];
	raw_imu_msgs[i].linear_acceleration.y = sensorDataQueue_.front_blocking().boards[i].acc[1];
	raw_imu_msgs[i].linear_acceleration.z = sensorDataQueue_.front_blocking().boards[i].acc[2];


	raw_magnetometer_msgs[i].header.stamp = ros::Time(cur_time);
	raw_magnetometer_msgs[i].header.frame_id = "imu_board_"+str;
	raw_magnetometer_msgs[i].vector.x = sensorDataQueue_.front_blocking().boards[i].mag[0];
	raw_magnetometer_msgs[i].vector.y = sensorDataQueue_.front_blocking().boards[i].mag[1];
	raw_magnetometer_msgs[i].vector.z = sensorDataQueue_.front_blocking().boards[i].mag[2];

	rawImusPub_[i].publish(raw_imu_msgs[i]);
	rawMagnetometersPub_[i].publish(raw_magnetometer_msgs[i]);

      }



    

    //std::cout<<"Hz = "<<h<<std::endl;
    //std::cout<<"Hz_min = "<<h_min<<std::endl;
    //std::cout<<"data: "<<sensorDataQueue_.front_blocking()<<std::endl;
    //posePub_.publish(poseDataQueue_.front_blocking());
    //driverCmdPub_.publish(driverDataQueue_.front_blocking());
    //prsCtrlPub_.publish(prsCtrlDataQueue_.front_blocking());
    //curCtrlPub_.publish(curCtrlDataQueue_.front_blocking());
}

}  // namespace low_level_control
