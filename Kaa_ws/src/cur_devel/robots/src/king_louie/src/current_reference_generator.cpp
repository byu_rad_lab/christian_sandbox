/*
 * @file    current_reference_generator.h
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Task that generates valve current references to feed into the current controller
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "ros/ros.h"
#include "low_level_control/current_reference_generator.h"

#include "low_level_control/gateway_data_types.h"

namespace king_louie
{
CurrentRefGen::CurrentRefGen(const std::string &name, const std::string &paramServerBasePath,
        pneubotics::queue::Queue<sensors_msg> *sensorDataQueue,
        pneubotics::queue::Queue<current_controllers_msg> *curCtrlDataQueue) :
        RosTask(name, paramServerBasePath + "/" + name)

{
    sensorDataQueue_ = sensorDataQueue;
    curCtrlDataQueue_ = curCtrlDataQueue;
    ROS_INFO("%s", paramServerPath_.c_str());
}

CurrentRefGen::~CurrentRefGen()
{
}

void CurrentRefGen::doInit(void)
{
}

void CurrentRefGen::doRun(void)
{
    ctrlData_.time = sensorData_.time;

    if (currentRefUpdateRequests_.size() > 0)
    {
	current_msg msg = currentRefUpdateRequests_.front();
        currentRefUpdateRequests_.pop();
        for(int i = 0; i < msg.l; i++)
        {
          ctrlData_.boards[msg.boardIndex[i]].refP[msg.axisIndex[i]] = msg.cmd[i];
        }
    }
}

void CurrentRefGen::getInputs(void)
{
    sensorData_ = sensorDataQueue_->front_blocking();
}

void CurrentRefGen::setOutputs(void)
{
    curCtrlDataQueue_->push(ctrlData_);
}

void CurrentRefGen::CurrentRefGen::loadParams(void)
{
}

void CurrentRefGen::setCurrentRef(const king_louie::current_msg &msg)
{
    for(int i = 0; i < msg.l; i++)
    {
        if ((msg.boardIndex[i] < 0) || (msg.boardIndex[i] >= GATEWAY_DRIVER_BOARDS_PER_GATEWAY))
        {
            ROS_WARN("Board index out of range. Min %d, Max %d", 0,
                    GATEWAY_DRIVER_BOARDS_PER_GATEWAY-1);
            return;
        }

        if ((msg.axisIndex[i] < 0) || (msg.axisIndex[i] >= GATEWAY_DRIVERS_PER_DRIVERS_BOARD))
        {
            ROS_WARN("Axis index out of range. Min %d, Max %d", 0,
                    GATEWAY_DRIVERS_PER_DRIVERS_BOARD-1);
            return;
        }

        if ((msg.cmd[i] < GATEWAY_DRIVER_MIN_COMMAND) || (msg.cmd[i] < GATEWAY_DRIVER_MIN_COMMAND))
        {
            ROS_WARN("Reference current out of range. Min %f, Max %f", GATEWAY_DRIVER_MIN_COMMAND,
                    GATEWAY_DRIVER_MAX_COMMAND);
            return;
        }
    }
	//std::cout << "CRG " << msg << std::endl;
    currentRefUpdateRequests_.push(msg);
}

}  // namespace low_level_control
