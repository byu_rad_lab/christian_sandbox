#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Pose
from king_louie.msg import joint_angles_msg_array as j_a_msg
from king_louie.msg import joint_angles_msg as j_a
import numpy as np

rospy.init_node('des_q', anonymous=True)
pub = rospy.Publisher('/SetJointGoal', j_a_msg, queue_size = 1)
rate = rospy.Rate(1000)

q_des = j_a()
q_des.appendage = 'Left'
conv_rad_to_deg = 180.0/np.pi
#shoulder 1, shoulder 2, elbow, wrist 1, wrist 2
q_des.q = np.array([-1.0576, 0.9326, 0.0, 0.3647, -0.1961])*conv_rad_to_deg #from matlab script
q_des.flag = 1
q_des.q_dot = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
q_des_arr = j_a_msg()
q_des_arr.joint_angles = [q_des]
print(q_des_arr)
print('\n')
print(type(q_des_arr))

def qdes_publisher():
    while not rospy.is_shutdown():
        rospy.sleep(0.1)
        pub.publish(q_des_arr)
        q_new = input('Enter a new q_des in degrees: ') #format: 30,20,0,10,5
        q_des.q = np.fromstring(q_new, dtype = float, count = 5, sep = ',')
        q_des_arr.joint_angles = [q_des]

if __name__=='__main__':
	qdes_publisher()



