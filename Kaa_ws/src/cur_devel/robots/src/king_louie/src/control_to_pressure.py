#!/usr/bin/env python

import sys
import os
import time
import rospy
import math
import numpy as np
import scipy.io as io
import yaml
import optparse
from king_louie.msg import joint_angles_msg_array as j_a_msg
from king_louie.msg import sensors as sensor
from king_louie.msg import current_msg as set_current
from king_louie.msg import mpc_msg
from king_louie.msg import desired_pressure_msg
from king_louie.msg import pressure_msg as set_pressure
from king_louie.msg import gripper_status
from copy import deepcopy
from threading import RLock, Timer
from collections import deque
from tools.iir_filter import iir_filter
from dynamic_reconfigure.server import Server
from king_louie.cfg import PressurePIDTunerConfig
# from pdb import set_trace as stop

class PressureControl():

    def __init__(self, tuning, parampath, nodename):

        self.lock = RLock()
        self.start_time = time.time()
        self.time = 0
        self.time_0 = 0
        self.initial = 1
        self.num = 0
        self.rate = 1000.0
        self.flag = 1

        self.Kp = []
        self.Kd = []
        self.Ki = []

        self.tstep = 1.0/self.rate
        self.paps = 0.000145037738

        self.order = 5

        self.p_array = deque([],maxlen=10000)
        self.time_array = deque([],maxlen=10000)
        self.p_desired_array = deque([],maxlen=10000)

        self.p_d = np.array([])
        self.gripper_goal = np.array([0.0, 0.0])
        self.sensor_flag = 0
        self.desired_pressure_flag = 0

        self.boardIndex = []
        self.axisIndex = []
        self.j = []
        self.joint_list_ = []
        self.joint_list =  [0,1,2,3,4,5,6,7,8,9,10]

        self.p_d_ = 14.7*np.ones(22)/self.paps #initialize all joints to 0 gauge pressure (14.7 psi = atmospheric pressure... ish)
        self.pid_gains = {}
        self.parampath = parampath
        self.nodename = nodename
        self.tuning = tuning

        self.initialize_joints()

        self.fill = np.zeros(self.num)
        self.p_ = np.zeros(self.num)
        self.p = np.zeros(self.num)
        self.plast = np.zeros(self.num)
        self.pfilt_last = np.zeros(self.num)
        self.p_d_last = np.zeros(self.num)
        self.errorlast = np.zeros(self.num)
        self.pfilt = np.zeros(self.num)
        self.integrator = np.zeros(self.num)
        self.cmd = np.zeros(self.num)
        self.offset = np.array([35 for i in range(self.num)]) # def 25

        self.filters = [None]*self.num
        #filt_a = np.array([1, -3.671729089161945, 5.06799838673419, -3.115966925201750, 0.719910327291871])
        #filt_b = np.array([1.32937288987529e-05, 5.31749155950116e-05, 7.97623733925174e-05, 5.31749155950116e-05, 1.32937288987529e-05])

        # #5th order Fs = 30 Fc = 1 IIR Filter
        # filt_a = np.array([1,    -4.32259612675314,   7.51418241128532,    -6.56261229709249,   2.87829142186758,    -0.506973166690259])
        # filt_b = np.array([9.13258178192835e-06, 4.56629089096418e-05, 9.13258178192835e-05, 9.13258178192835e-05, 4.56629089096418e-05, 9.13258178192835e-06])

    	# #2nd order Fstop = 50 Fpass = 1 Lag ~ 0.05
    	# filt_a = np.array([1,	-1.97481020856613,	0.975123549412707])
    	# filt_b = np.array([7.83352116439921e-05,	0.000156670423287984,	7.83352116439921e-05])

    	# #2nd order Fstop = 25 Fpass = 0.75 Lag ~ 0.08
    	# filt_a = np.array([1,	-1.98748237961592,	0.987560239232928])
    	# filt_b = np.array([1.94649042513659e-05,	3.89298085027318e-05,	1.94649042513659e-05])

    	#2nd order Fstop = 20 Fpass = .15 Lag ~ 0.06
    	filt_a = np.array([1,	-1.98999326095977,	0.990043079731635])
    	filt_b = np.array([1.24546929656125e-05,	2.49093859312251e-05,	1.24546929656125e-05])

        for i in range(self.num):
            self.filters[i] = iir_filter(filt_a, filt_b)

        if self.tuning:
            self.dyn_reconfig_srv = Server(PressurePIDTunerConfig, self.dyn_reconfig_callback)
            self.pub_pressures_psi = rospy.Publisher('/PressureDataPSI', sensor, queue_size = 1, tcp_nodelay=True)
            self.pub_pressures_filt = rospy.Publisher('/PressureDatafilt', sensor, queue_size = 1, tcp_nodelay=True)
            self.pub_int = rospy.Publisher('/IntegratorVal', sensor, queue_size = 1, tcp_nodelay=True)

        else:
            rospy.Subscriber('/Joint0DesiredPressures', mpc_msg, self.pdes_callback0, tcp_nodelay=True)
            rospy.Subscriber('/Joint1DesiredPressures', mpc_msg, self.pdes_callback1, tcp_nodelay=True)
            rospy.Subscriber('/Joint3DesiredPressures', mpc_msg, self.pdes_callback3, tcp_nodelay=True)
            rospy.Subscriber('/Joint4DesiredPressures', mpc_msg, self.pdes_callback4, tcp_nodelay=True)

            rospy.Subscriber('/Joint5DesiredPressures', mpc_msg, self.pdes_callback5, tcp_nodelay=True)
            rospy.Subscriber('/Joint6DesiredPressures', mpc_msg, self.pdes_callback6, tcp_nodelay=True)
            rospy.Subscriber('/Joint8DesiredPressures', mpc_msg, self.pdes_callback8, tcp_nodelay=True)
            rospy.Subscriber('/Joint9DesiredPressures', mpc_msg, self.pdes_callback9, tcp_nodelay=True)

        rospy.Subscriber("/PressureData", sensor, self.sensor_callback, tcp_nodelay=True)
        rospy.Subscriber('/SetGrippers', gripper_status, self.gripper_goal_callback, tcp_nodelay=True)
        self.pub_current = rospy.Publisher('/SetCurrentRef', set_current, queue_size = 1,  tcp_nodelay=True)

    def dyn_reconfig_callback(self, config, level):
        # reassign gains as given in the Dynamic Reconfigure GUI
        i = -1
        for ch in self.pid_gains['chamber_order']:
            i = i + 1

            if ch == "right_elbow1" or ch == "right_elbow2" or \
               ch == "left_elbow1"or ch == "left_elbow2" or \
               ch == "abs":
                continue

            gain_list = self.string2floatlist( config[ch] )

            self.Kp[i] = gain_list[0]
            self.Ki[i] = gain_list[1]
            self.Kd[i] = gain_list[2]
            self.p_d_[i] = gain_list[3]/self.paps

        return config

    def string2floatlist(self, str_in):
        string_list = str_in.split(',')
        float_list = [float(entry) for entry in string_list]

        return float_list

    def initialize_joints(self):

        joint_data_dict = {0:{'boardIndex':[0, 0], 'axisIndex':[0, 1], 'j':[1, 0]},
                           1:{'boardIndex':[0, 0], 'axisIndex':[2, 3], 'j':[3, 2]},
                           2:{'boardIndex':[1, 1], 'axisIndex':[0, 1], 'j':[4, 5]},
                           3:{'boardIndex':[1, 1], 'axisIndex':[2, 3], 'j':[7, 6]},
                           4:{'boardIndex':[2, 2], 'axisIndex':[0, 1], 'j':[9, 8]},
                           5:{'boardIndex':[3, 3], 'axisIndex':[1, 0], 'j':[12, 13]},
                           6:{'boardIndex':[3, 3], 'axisIndex':[3, 2], 'j':[14, 15]},
                           7:{'boardIndex':[4, 4], 'axisIndex':[0, 1], 'j':[16, 17]},
                           8:{'boardIndex':[4, 4], 'axisIndex':[2, 3], 'j':[18, 19]},
                           9:{'boardIndex':[5, 5], 'axisIndex':[0, 1], 'j':[20, 21]},
                           10:{'boardIndex':[6, 6],'axisIndex':[2, 3], 'j':[26, 27]},
                           11:{'boardIndex':[6],   'axisIndex':[1],    'j':[25]}}

        with open(self.parampath) as stream:
            self.pid_gains = yaml.load(stream)

        self.pid_gains['chamber_order'] = ['RSPL', 'RSPM', 'RSDP', 'RSDA',  # order coincides with self.j
                                           'right_elbow1', 'right_elbow2',
                                           'RWPP', 'RWPA', 'RWDL', 'RWDM',
                                           'LSPM', 'LSPL', 'LSDA', 'LSDP',
                                           'left_elbow1', 'left_elbow2',
                                           'LWPP', 'LWPA', 'LWDL', 'LWDM',
                                           'RHIP', 'LHIP', 'abs' ]

        for ch in self.pid_gains['chamber_order']:
            self.pid_gains[ch] = self.string2floatlist( self.pid_gains[ch] )

            if ch == 'abs':
                continue
            self.Kp.append(self.pid_gains[ch][0])
            self.Ki.append(self.pid_gains[ch][1])
            self.Kd.append(self.pid_gains[ch][2])

        for i in range(len(self.joint_list)):
            self.boardIndex = self.boardIndex + joint_data_dict[i]['boardIndex']
            self.axisIndex = self.axisIndex + joint_data_dict[i]['axisIndex']
            self.j = self.j + joint_data_dict[i]['j']
            self.num = self.num+len(joint_data_dict[i]['boardIndex'])

    def get_pdes_data(self, data, jt):
        self.lock.acquire()
        try:
            i1 = jt*2
            i2 = i1+1
            i3 = i1+2

            if data.flag == 1:
                if jt == 5 or jt == 6 or jt == 9:  # for some reason these joints are indexed backwards. not sure about joint 7 though
                    self.p_d_[i1] = data.p_d[1]/self.paps
                    self.p_d_[i2] = data.p_d[0]/self.paps
                else:
                    self.p_d_[i1] = data.p_d[0]/self.paps
                    self.p_d_[i2] = data.p_d[1]/self.paps
            else:
                self.p_d_[i1:i3] = [23.5, 23.5]/self.paps 
            # print self.p_d_
        finally:
            self.lock.release()

    def pdes_callback0(self, data):
        self.get_pdes_data(data, 0)

    def pdes_callback1(self, data):
        self.get_pdes_data(data, 1)

    def pdes_callback3(self, data):
        self.get_pdes_data(data, 3)

    def pdes_callback4(self, data):
        self.get_pdes_data(data, 4)

    def pdes_callback5(self, data):
        self.get_pdes_data(data, 5)

    def pdes_callback6(self, data):
        self.get_pdes_data(data, 6)

    def pdes_callback8(self, data):
        self.get_pdes_data(data, 8)

    def pdes_callback9(self, data):
        self.get_pdes_data(data, 9)

    def sensor_callback(self,data):
        self.lock.acquire()
        try:
            if self.flag != 1:
                return
            self.sensor_flag = data.flag

            for i in range(self.num):
                self.p_[i] = data.p[self.j[i]]

        finally:
            self.lock.release()

    def gripper_goal_callback(self, data):
        self.lock.acquire()
        try:
            self.gripper_goal = np.array(data.gripper_cmd)
        finally:
            self.lock.release()

    def set_data(self):
        t = time.time()

        self.lock.acquire()
        try:
            if self.initial == 1:
                self.time_0 = t
                self.plast = self.p_
                self.pfilt_last = self.p_
                return
            self.p = np.array(self.p_)

            if self.tuning:  # publish current and desired pressures in psi for ease of viewing in rqt plots`
                p_out = self.p * self.paps
                p_des = self.p_d * self.paps
                p_filt = self.pfilt * self.paps

                msg = sensor(p_out, [], p_des, [], 0)
                msgfilt = sensor(p_filt, [], p_des, [], 0)
                msgInt = sensor(self.Ki * self.integrator / 50 , [], self.cmd/50, [], 0)
                self.pub_pressures_psi.publish(msg)
                self.pub_pressures_filt.publish(msgfilt)

            for i in range(self.num):
                filt = self.filters[i]
                self.pfilt[i] = filt.get_filt_val(self.p[i])

            self.time = t - self.time_0

        finally:
            self.lock.release()


    def get_des_pressures(self):

        self.lock.acquire()
        try:
            if self.flag != 1:
                return
            self.p_d = self.p_d_

            for i in range(len(self.p_d)):
                if self.p_d[i] > 35.0/self.paps:
                    self.p_d[i] = 35.0/self.paps

            if self.tuning != True:
            	self.p_d[20] = 34.7/self.paps # right hip
            	self.p_d[21] = 34.7/self.paps # left hip
            self.joint_list = self.joint_list_
        finally:
            self.lock.release()

    def set_valves(self):

        if self.sensor_flag == 0:
            print("No Sensor Data")
            return

        self.get_des_pressures()

        self.set_data()

        if self.initial == 1:
            self.initial = 0
            return

        error = self.p_d - self.p
        # print self.p_d

        for i in range(self.num):

            if error[i]*self.integrator[i] < 0 or self.p_d[i] != self.p_d_last[i]:
            	self.integrator[i] = 0.
            else:
            	self.integrator[i] = self.integrator[i] + (self.tstep)*(error[i])


        p_dot = (self.pfilt - self.pfilt_last)/self.tstep

        if len(self.p_array) > (self.order):
            for i in range(1, self.order):
                self.fill = self.fill + (self.p_array[(-i)]-self.p_array[(-i-1)])/(1.0/self.rate)
            p_dot = self.fill/self.order

        self.fill = np.zeros(self.num)

        self.cmd = self.Kp*error + self.offset - (self.Kd*p_dot) + self.Ki*self.integrator #offset so valve actually closes

        for i in range(self.num):
            self.cmd[i] = min(1000,max(-1000,self.cmd[i]))

        self.cmd[0:10] = [-10.0]*10  # shut off right arm 
        #self.cmd[10:20] = [-10.0]*10  # shut off left arm

        self.cmd[14:16] = [-10.0, -10.0]  # shutting off left elbow valves 
        self.cmd[4:6] = [-10.0, -10.0]  # shutting off right elbow valves 

        #cmd_pub = np.append(self.cmd, self.gripper_goal*1600.0-800.0)   # use both grippers
        cmd_pub = np.append(self.cmd, -10.0)                            # kills both grippers
        #cmd_pub = np.append(self.cmd, self.gripper_goal[0]*1600.0-800.0)  # use right gripper only (activates right)
        #cmd_pub = np.append(cmd_pub, 50.0)                                # use right gripper only (kills left)

        boardIndex_pub = np.append(self.boardIndex, np.array([2, 5])).tolist()
        axisIndex_pub = np.append(self.axisIndex, np.array([3, 3])).tolist()
        num_pub = self.num + 2

        msg = set_current(cmd_pub, boardIndex_pub, axisIndex_pub, num_pub)
        # msg.cmd[10] = 500.0
        self.print_stuff()
        self.pub_current.publish(msg)
        self.plast = self.p
        self.pfilt_last = deepcopy(self.pfilt)
        self.p_d_last = deepcopy(self.p_d)
        self.errorlast = error

    def print_stuff(self):
        # print msg.cmd
        # print "p = ", self.p*self.paps
        print('--------------right--------------')
        print('sh1: ', 'SPM - ', np.around(self.p[1]*self.paps - 14.7, 1),'\t', "SPL - ", np.around(self.p[0]*self.paps - 14.7, 1))
        print('sh2: ', 'SDA - ', np.around(self.p[3]*self.paps - 14.7, 1),'\t',  "SDP - ", np.around(self.p[2]*self.paps - 14.7, 1))
        print('w1:  ', 'WPP - ', np.around(self.p[6]*self.paps - 14.7, 1),'\t',  "WPA - ", np.around(self.p[7]*self.paps - 14.7, 1))
        print('w2:  ', 'WDL - ', np.around(self.p[8]*self.paps - 14.7, 1),'\t',  "WDM - ", np.around(self.p[9]*self.paps - 14.7, 1))
        print('---------------left---------------')
        print('sh1: ', 'SPM - ', np.around(self.p[10]*self.paps - 14.7, 1),'\t', "SPL - ", np.around(self.p[11]*self.paps - 14.7, 1))
        print('sh2: ', 'SDA - ', np.around(self.p[12]*self.paps - 14.7, 1),'\t',  "SDP - ", np.around(self.p[13]*self.paps - 14.7, 1))
        print('w1:  ', 'WPP - ', np.around(self.p[16]*self.paps - 14.7, 1),'\t',  "WPA - ", np.around(self.p[17]*self.paps - 14.7, 1))
        print('w2:  ', 'WDL - ', np.around(self.p[18]*self.paps - 14.7, 1),'\t',  "WDM - ", np.around(self.p[19]*self.paps - 14.7, 1), '\n')

    def drain(self):

        print("Draining")
        self.joint_list = [0,1,2,3,4,5,6,7,8,9,10,11]
        self.boardIndex = []
        self.axisIndex = []
        self.j = []
        self.num = 0
        self.flag = 0
        self.initialize_joints()

        cmd = []
        for i in range(len(self.j)):
            cmd.append(-1000)

        msg = set_current(cmd, self.boardIndex, self.axisIndex, self.num)
        self.pub_current.publish(msg)

    def gain_dump(self):
        os.system("rosparam dump " + self.parampath + " /" + self.nodename)
        os.system("rosparam delete /" + self.nodename)
        rospy.sleep(4)
        print("-----------------------Finish-----------------------")


if __name__ == '__main__':

    parser = optparse.OptionParser()
    parser.set_defaults(tuning=False)
    parser.add_option('-t', action="store_true", dest="tuning")
    (options, args) = parser.parse_args()

    parampath = os.path.expanduser('~') + '/git/byu/cur_devel/robots/src/king_louie/src/pid_gains.yaml'
    nodename = "kl_pressure_control"

    os.system("rosparam load " + parampath + " /" + nodename)
    # os.system("rosrun rqt_gui rqt_gui -s reconfigure")

    rospy.init_node(nodename, anonymous=False)

    Pcontrol = PressureControl(options.tuning, parampath, nodename)
    rate = rospy.Rate(Pcontrol.rate)

    rospy.on_shutdown(Pcontrol.drain)
    rospy.on_shutdown(Pcontrol.gain_dump)

    while not rospy.is_shutdown():
        Pcontrol.set_valves()
        rate.sleep()
