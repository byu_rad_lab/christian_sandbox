/*
 * @file    loggable_data.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Define what and how data will be logged by the data logger.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "low_level_control/loggable_data.h"
#include "low_level_control/gateway_data_types.h"

namespace king_louie
{

LoggableData::LoggableData(const std::string &logName, const std::string &logPath,
        pneubotics::queue::Queue<sensors_msg> *sensorDataQueue,
        pneubotics::queue::Queue<drivers_msg> *driverDataQueue,
        pneubotics::queue::Queue<pose_msg> *poseDataQueue,
        pneubotics::queue::Queue<pressure_controllers_msg> *prsCtrlDataQueue) :
        pneubotics::file_io::LoggableObject(logName, logPath),
                sensorDataQueue_(sensorDataQueue),
                driverDataQueue_(driverDataQueue),
                poseDataQueue_(poseDataQueue),
                prsCtrlDataQueue_(prsCtrlDataQueue)
{
}

LoggableData::~LoggableData()
{
}

std::string LoggableData::getColumnHeaders(void) const
        {
    std::stringstream ss;

    /*
     * Python dictionary style column headers
     */
    int cnt = 0;
    ss << "# JSON: {\"TIME\":" << cnt++ << ",";
    for (int sns = 0; sns < GATEWAY_SENSOR_BOARDS_PER_GATEWAY; sns++)
    {
        for (int axis = 0; axis < GATEWAY_SENSORS_ACC_PER_SENSOR_BOARD; axis++)
        {
            ss << "\"ACC_" << sns << "_" << axis << "\":" << cnt++ << ",";
        }

        for (int axis = 0; axis < GATEWAY_SENSORS_GYR_PER_SENSOR_BOARD; axis++)
        {
            ss << "\"GYR_" << sns << "_" << axis << "\":" << cnt++ << ",";
        }

        for (int axis = 0; axis < GATEWAY_SENSORS_MAG_PER_SENSOR_BOARD; axis++)
        {
            ss << "\"MAG_" << sns << "_" << axis << "\":" << cnt++ << ",";
        }

        for (int axis = 0; axis < GATEWAY_SENSORS_PRS_PER_SENSOR_BOARD; axis++)
        {
            ss << "\"PRS_" << sns << "_" << axis << "\":" << cnt++ << ",";
        }
    }

    ss << "\"TIME_POSE_S\":" << cnt++ << ",";
    for (int sns = 0; sns < GATEWAY_SENSOR_BOARDS_PER_GATEWAY; sns++)
    {
        for (int axis = 0; axis < 3; axis++)
        {
            ss << "\"POSE_ACC_" << sns << "_" << axis << "\":" << cnt++ << ",";
        }

        for (int axis = 0; axis < 3; axis++)
        {
            ss << "\"POSE_MAG_" << sns << "_" << axis << "\":" << cnt++ << ",";
        }

        for (int axis = 0; axis < 9; axis++)
        {
            ss << "\"POSE_ROT_" << sns << "_" << axis << "\":" << cnt++ << ",";
        }
    }

    ss << "\"TIME_CTRL\":" << cnt++ << ",";
    ss << "\"TIME_CTRL_LAG\":" << cnt++ << ",";
    ss << "\"TIME_CTRL_DIF\":" << cnt++ << ",";
    for (int drv = 0; drv < GATEWAY_DRIVER_BOARDS_PER_GATEWAY; drv++)
    {
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "\"CTRL_REFP_" << drv << "_" << axis << "\":" << cnt++ << ",";
        }
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "\"CTRL_REFI_" << drv << "_" << axis << "\":" << cnt++ << ",";
        }
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "\"CTRL_REFD_" << drv << "_" << axis << "\":" << cnt++ << ",";
        }

        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "\"CTRL_ERRP_" << drv << "_" << axis << "\":" << cnt++ << ",";
        }
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "\"CTRL_ERRI_" << drv << "_" << axis << "\":" << cnt++ << ",";
        }
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "\"CTRL_ERRD_" << drv << "_" << axis << "\":" << cnt++ << ",";
        }
    }

    ss << "\"TIME_DRV\":" << cnt++ << ",";
    for (int drv = 0; drv < GATEWAY_DRIVER_BOARDS_PER_GATEWAY; drv++)
    {
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "\"DRV_" << drv << "_" << axis << "\":" << cnt++;
            if (!(drv == (GATEWAY_DRIVER_BOARDS_PER_GATEWAY - 1) && (axis == (GATEWAY_DRIVERS_PER_DRIVERS_BOARD - 1))))
            {
                ss << ",";
            }
        }
    }
    ss << "}" << std::endl;

    /*
     * Standard comma separated column headers
     */
    ss << "# TIME,";
    for (int sns = 0; sns < GATEWAY_SENSOR_BOARDS_PER_GATEWAY; sns++)
    {
        for (int axis = 0; axis < GATEWAY_SENSORS_ACC_PER_SENSOR_BOARD; axis++)
        {
            ss << "ACC_" << sns << "_" << axis << ",";
        }

        for (int axis = 0; axis < GATEWAY_SENSORS_GYR_PER_SENSOR_BOARD; axis++)
        {
            ss << "GYR_" << sns << "_" << axis << ",";
        }

        for (int axis = 0; axis < GATEWAY_SENSORS_MAG_PER_SENSOR_BOARD; axis++)
        {
            ss << "MAG_" << sns << "_" << axis << ",";
        }

        for (int axis = 0; axis < GATEWAY_SENSORS_PRS_PER_SENSOR_BOARD; axis++)
        {
            ss << "PRS_" << sns << "_" << axis << ",";
        }
    }

    ss << "TIME_POS,";
    for (int sns = 0; sns < GATEWAY_SENSOR_BOARDS_PER_GATEWAY; sns++)
    {
        for (int axis = 0; axis < 3; axis++)
        {
            ss << "POSE_ACC_" << sns << "_" << axis << ",";
        }

        for (int axis = 0; axis < 3; axis++)
        {
            ss << "POSE_MAG_" << sns << "_" << axis << ",";
        }

        for (int axis = 0; axis < 9; axis++)
        {
            ss << "POSE_ROT_" << sns << "_" << axis << ",";
        }
    }

    ss << "TIME_CTRL,";
    ss << "TIME_CTRL_LAG,";
    ss << "TIME_CTRL_DIF,";
    for (int drv = 0; drv < GATEWAY_DRIVER_BOARDS_PER_GATEWAY; drv++)
    {
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "CTRL_REFP_" << drv << "_" << axis << ",";
        }
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "CTRL_REFI_" << drv << "_" << axis << ",";
        }
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "CTRL_REFD_" << drv << "_" << axis << ",";
        }

        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "CTRL_ERRP_" << drv << "_" << axis << ",";
        }
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "CTRL_ERRI_" << drv << "_" << axis << ",";
        }
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "CTRL_ERRD_" << drv << "_" << axis << ",";
        }
    }

    ss << "TIME_DRV,";
    for (int drv = 0; drv < GATEWAY_DRIVER_BOARDS_PER_GATEWAY; drv++)
    {
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "DRV_" << drv << "_" << axis;
            if (!(drv == (GATEWAY_DRIVER_BOARDS_PER_GATEWAY - 1) && (axis == (GATEWAY_DRIVERS_PER_DRIVERS_BOARD - 1))))
            {
                ss << ",";
            }
            else
            {
                ss << std::endl;
            }
        }
    }
    return ss.str();
}

std::string LoggableData::getColumnDefinitions(int startNum) const
        {
    std::stringstream ss;

    ss << "# TIME = " << startNum++ << " # Sensor measurement time-stamp in s" << std::endl;
    for (int sns = 0; sns < GATEWAY_SENSOR_BOARDS_PER_GATEWAY; sns++)
    {
        for (int axis = 0; axis < GATEWAY_SENSORS_ACC_PER_SENSOR_BOARD; axis++)
        {
            ss << "# ACC_G_" << sns << "_" << axis << " = " << startNum++ << " # Acceleration measurement for board "
                    << sns << " axis " << axis << "in G" << std::endl;
        }

        for (int axis = 0; axis < GATEWAY_SENSORS_GYR_PER_SENSOR_BOARD; axis++)
        {
            ss << "# GYR_RPS_" << sns << "_" << axis << " = " << startNum++
                    << " # Rotational rate measurement for board " << sns << " axis " << axis << "in rad/s"
                    << std::endl;
        }

        for (int axis = 0; axis < GATEWAY_SENSORS_MAG_PER_SENSOR_BOARD; axis++)
        {
            ss << "# MAG_G_" << sns << "_" << axis << " = " << startNum++
                    << " # Magnetic field measurement for board " << sns << " axis " << axis << "in uT" << std::endl;

        }

        for (int axis = 0; axis < GATEWAY_SENSORS_PRS_PER_SENSOR_BOARD; axis++)
        {
            ss << "# PRS_PA_" << sns << "_" << axis << " = " << startNum++
                    << " # Absolute pressure measurement for board " << sns << " axis " << axis << "in Pa" << std::endl;
        }
    }

    ss << "# TIME_POSE = " << startNum++ << " #  Pose calculations time-stamp in s" << std::endl;
    for (int sns = 0; sns < GATEWAY_SENSOR_BOARDS_PER_GATEWAY; sns++)
    {
        for (int axis = 0; axis < 3; axis++)
        {
            ss << "# POSE_ACC_G_" << sns << "_" << axis << " = " << startNum++
                    << " # Acceleration estimate for board " << sns << " axis " << axis << "in G" << std::endl;
        }

        for (int axis = 0; axis < 3; axis++)
        {
            ss << "# POSE_MAG_UT_" << sns << "_" << axis << " = " << startNum++
                    << " # Magnetic field estimate for board " << sns << " axis " << axis << "in uT" << std::endl;
        }

        for (int axis = 0; axis < 9; axis++)
        {
            ss << "# POSE_ROT_" << sns << "_" << axis << " = " << startNum++
                    << " # Rotation matrix between the inertial frame and the node frame for board " << sns << " element " << axis << std::endl;
        }
    }

    ss << "# TIME_CTRL_S = " << startNum++ << " #  Control calculations time-stamp in s" << std::endl;
    ss << "# TIME_CTRL_LAG_S = " << startNum++ << " #  Control calculations latency in s" << std::endl;
    ss << "# TIME_CTRL_DIF_S = " << startNum++ << " #  Time since control calculations was last run in s" << std::endl;
    for (int drv = 0; drv < GATEWAY_DRIVER_BOARDS_PER_GATEWAY; drv++)
    {
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "# CTRL_REFP_" << drv << "_" << axis << " = " << startNum++
                    << " # Proportional pressure reference for board " << drv << " axis " << axis << std::endl;
        }

        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "# CTRL_REFI_" << drv << "_" << axis << " = " << startNum++
                    << " # Integral pressure reference for board " << drv << " axis " << axis << std::endl;
        }

        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "# CTRL_REFD_" << drv << "_" << axis << " = " << startNum++
                    << " # Derivative pressure reference for board " << drv << " axis " << axis << std::endl;
        }

        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "# CTRL_ERRP_" << drv << "_" << axis << " = " << startNum++
                    << " # Proportional pressure error for board " << drv << " axis " << axis << std::endl;
        }

        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "# CTRL_ERRI_" << drv << "_" << axis << " = " << startNum++
                    << " # Integral pressure error for board " << drv << " axis " << axis << std::endl;
        }

        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "# CTRL_ERRD_" << drv << "_" << axis << " = " << startNum++
                    << " # Derivative pressure error for board " << drv << " axis " << axis << std::endl;
        }
    }

    ss << "# TIME_DRV_S = " << startNum++ << " #  Driver output time-stamp in s" << std::endl;
    for (int drv = 0; drv < GATEWAY_DRIVER_BOARDS_PER_GATEWAY; drv++)
    {
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << "# DRV_MA_" << drv << "_" << axis << " = " << startNum++
                    << " # Commanded valve current for board " << drv << " axis " << axis << "in mA" << std::endl;
        }
    }
    return ss.str();
}

std::string LoggableData::getColumnData(void) const
        {
    std::stringstream ss;

    sensors_msg sensors(sensorDataQueue_->front_blocking());
    ss << sensors.time << ",";
    for (int sns = 0; sns < GATEWAY_SENSOR_BOARDS_PER_GATEWAY; sns++)
    {
        for (int axis = 0; axis < GATEWAY_SENSORS_ACC_PER_SENSOR_BOARD; axis++)
        {
            ss << sensors.boards[sns].acc[axis] << ",";
        }

        for (int axis = 0; axis < GATEWAY_SENSORS_GYR_PER_SENSOR_BOARD; axis++)
        {
            ss << sensors.boards[sns].gyr[axis] << ",";
        }

        for (int axis = 0; axis < GATEWAY_SENSORS_MAG_PER_SENSOR_BOARD; axis++)
        {
            ss << sensors.boards[sns].mag[axis] << ",";
        }

        for (int axis = 0; axis < GATEWAY_SENSORS_PRS_PER_SENSOR_BOARD; axis++)
        {
            ss << sensors.boards[sns].prs[axis] << ",";
        }
    }

    pose_msg pose(poseDataQueue_->front_blocking());
    ss << pose.time << ",";
    for (int sns = 0; sns < GATEWAY_SENSOR_BOARDS_PER_GATEWAY; sns++)
    {
        for (int axis = 0; axis < 3; axis++)
        {
            ss << pose.boards[sns].acc[axis] << ",";
        }

        for (int axis = 0; axis < 3; axis++)
        {
            ss << pose.boards[sns].mag[axis] << ",";
        }

        for (int axis = 0; axis < 9; axis++)
        {
            ss << pose.boards[sns].rotationMatrix[axis] << ",";
        }
    }

    pressure_controllers_msg ctrl(prsCtrlDataQueue_->front_blocking());
    ss << ctrl.time << ",";
    ss << ctrl.latency << ",";
    ss << ctrl.delta << ",";
    for (int drv = 0; drv < GATEWAY_DRIVER_BOARDS_PER_GATEWAY; drv++)
    {
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << ctrl.boards[drv].refP[axis] << ",";
        }

        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << ctrl.boards[drv].refI[axis] << ",";
        }

        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << ctrl.boards[drv].refD[axis] << ",";
        }

        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << ctrl.boards[drv].errP[axis] << ",";
        }

        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << ctrl.boards[drv].errI[axis] << ",";
        }

        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << ctrl.boards[drv].errD[axis] << ",";
        }
    }

    drivers_msg drive(driverDataQueue_->front_blocking());
    ss << drive.time << ",";
    for (int drv = 0; drv < GATEWAY_DRIVER_BOARDS_PER_GATEWAY; drv++)
    {
        for (int axis = 0; axis < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; axis++)
        {
            ss << drive.boards[drv].current[axis];
            if (!(drv == (GATEWAY_DRIVER_BOARDS_PER_GATEWAY - 1) && (axis == (GATEWAY_DRIVERS_PER_DRIVERS_BOARD - 1))))
            {
                ss << ",";
            }
        }
    }
    return ss.str();
}

}  // namespace low_level_control
