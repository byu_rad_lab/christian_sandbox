#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Pose
from low_level_control.msg import joint_angles_msg_array as j_a_msg
from low_level_control.msg import joint_angles_msg as j_a
import numpy as np

class pub_continuous_qdes():
    def __init__(self):
        rospy.init_node('des_q_cont', anonymous=True)
        rospy.Subscriber("/SetJointGoal", j_a_msg, self.joint_goal_callback)
        self.pub = rospy.Publisher('/SetJointGoal2', j_a, queue_size = 1) #for visualization
        self.rate = rospy.Rate(1000)
        self.q_des = j_a()

    def pub_qdes_cont(self):    
        while not rospy.is_shutdown():
            self.pub.publish(self.q_des)
            self.rate.sleep()

    def joint_goal_callback(self,data):
        self.q_des = data.joint_angles[0]
        self.q_des.q = np.array(self.q_des.q)

        for i in range(len(self.q_des.q)):
            self.q_des.q[i] = self.q_des.q[i]*np.pi/180.0

if __name__== '__main__':
    pubcont = pub_continuous_qdes()
    pubcont.pub_qdes_cont()