/*
 * @file    ros_utils.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Provides some useful wrappers around ROS related functionality
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "low_level_control/ros_utils.h"

namespace king_louie
{
static void throwLoadException(const std::string &key)
{
    std::stringstream ss;
    ss << "Failed to load parameter from parameter server " << key;
    throw exception::ParamLoadException(ss.str());
}

void RosUtils::getParam(const std::string &key, bool &b)
{
    if (ros::param::get(key, b))
    {
        return;
    }
    throwLoadException(key);
}

void RosUtils::getParam(const std::string &key, int &i)
{
    if (ros::param::get(key, i))
    {
        return;
    }
    throwLoadException(key);
}

void RosUtils::getParam(const std::string &key, double &d)
{
    if (ros::param::get(key, d))
    {
        return;
    }
    throwLoadException(key);
}

void RosUtils::getParam(const std::string &key, std::string &s)
{
    if (ros::param::get(key, s))
    {
        return;
    }
    throwLoadException(key);
}

void RosUtils::setParam(const std::string &key, const bool &b)
{
    ros::param::set(key, b);
}

void RosUtils::setParam(const std::string &key, const int &i)
{
    ros::param::set(key, i);
}

void RosUtils::setParam(const std::string &key,const double &d)
{
    ros::param::set(key, d);
}

void RosUtils::setParam(const std::string &key,const std::string &s)
{
    ros::param::set(key, s);
}
RosTask::RosTask(const std::string &name, const std::string &paramServerPath) :
        pneubotics::task::Task(name), paramServerPath_(paramServerPath)
{
}

RosTask::~RosTask(void)
{
}

void RosTask::init(void)
{
    loadParams();
    doInit();
    initialized_ = true;
}

void RosTask::run(void)
{
    {
        if (!initialized_)
        {
            throw pneubotics::exception::TaskInitException(
                    "Cannot run " + name_ + " instance of Task object before initialization");
        }

        time_ = ros::Time::now();
        timeDiff_ = time_ - timePrev_;

        getInputs();
        doRun();
        setOutputs();

        timePrev_ = time_;
    }
}

}  // namespace low_level_control
