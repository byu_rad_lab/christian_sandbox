/*
 * @file    pressure_controller.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Task that runs PID pressure controllers
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "ros/ros.h"

#include "low_level_control/pressure_controller.h"

#include "low_level_control/gateway_data_types.h"
#include "low_level_control/robot_mapping.h"
#include "low_level_control/ros_utils.h"
#include <cmath>
#include <vector>

#define PAPS 0.000145037738
#define OFFSET 25

namespace king_louie
{
PressureController::PressureController(const std::string &name, const std::string &paramServerBasePath,
        pneubotics::queue::Queue<pressure_controllers_msg> *prsCtrlDataQueue,
        pneubotics::queue::Queue<drivers_msg> *driverDataQueue,
        pneubotics::queue::Queue<sensors_msg> *sensorDataQueue) :
        RosTask(name, paramServerBasePath + "/" + name), setPressureCtrlGainsQueue_(1), resetIntegrators_(true)
{
    prsCtrlDataQueue_ = prsCtrlDataQueue;
    driverDataQueue_ = driverDataQueue;
    sensorDataQueue_ = sensorDataQueue;
    ROS_INFO("%s", paramServerPath_.c_str());
}

PressureController::~PressureController()
{
}

void PressureController::doInit(void)
{
}

void PressureController::doRun()
{
    bool resetIntegrators = resetIntegrators_;  // Get local copy in case it changes during method
    double dt = timeDiff_.toSec();

    if (setPressureCtrlGainsQueue_.size() > 0)
    {
        king_louie::set_pressure_ctrl_gains_msg msg(setPressureCtrlGainsQueue_.pop());
        kP_[msg.boardIndex][msg.axisIndex] = msg.kp;
        kI_[msg.boardIndex][msg.axisIndex] = msg.ki;
        kD_[msg.boardIndex][msg.axisIndex] = msg.kd;
    }

    for (int actuator = 0; actuator < ACT_NUM_ACTUATORS; actuator++)
    {
        Actuator act = RobotMap[actuator];

        if (resetIntegrators)
        {
            ctrlData_.boards[act.driverAddrss.board].errI[act.driverAddrss.axis] = 0.0;
        }

        /*
         * Make sure our gains are up to date
         */
        ctrlData_.boards[act.driverAddrss.board].kP = kP_[act.driverAddrss.board][act.driverAddrss.axis];
        ctrlData_.boards[act.driverAddrss.board].kI = kI_[act.driverAddrss.board][act.driverAddrss.axis];
        ctrlData_.boards[act.driverAddrss.board].kD = kD_[act.driverAddrss.board][act.driverAddrss.axis];

        /*
         * Calculate actual signals
         */
        ctrlData_.boards[act.driverAddrss.board].actD[act.driverAddrss.axis] = 0.995
                * ctrlData_.boards[act.driverAddrss.board].actD[act.driverAddrss.axis] + 0.005
                * ((sensorData_.boards[act.sensorAddrss.board].prs[act.sensorAddrss.axis]
                        - ctrlData_.boards[act.driverAddrss.board].actP[act.driverAddrss.axis]) / dt);

        ctrlData_.boards[act.driverAddrss.board].actP[act.driverAddrss.axis] =
                sensorData_.boards[act.sensorAddrss.board].prs[act.sensorAddrss.axis];

        /*
         * Calculate error signales
         */
        double errP = ctrlData_.boards[act.driverAddrss.board].refP[act.driverAddrss.axis]
                - ctrlData_.boards[act.driverAddrss.board].actP[act.driverAddrss.axis];

        ctrlData_.boards[act.driverAddrss.board].errD[act.driverAddrss.axis] =
                ctrlData_.boards[act.driverAddrss.board].refD[act.driverAddrss.axis]
                        - ctrlData_.boards[act.driverAddrss.board].actD[act.driverAddrss.axis];

        /*ctrlData_.boards[act.driverAddrss.board].errI[act.driverAddrss.axis] += 0.5 * dt
                * (errP + ctrlData_.boards[act.driverAddrss.board].errP[act.driverAddrss.axis]);
        // TODO - Make integrator saturation configurable
        ctrlData_.boards[act.driverAddrss.board].errI[act.driverAddrss.axis] = SATURATE(
                ctrlData_.boards[act.driverAddrss.board].errI[act.driverAddrss.axis], -100000.0, 100000.0);*/

        ctrlData_.boards[act.driverAddrss.board].errP[act.driverAddrss.axis] = errP;

        /*
         * Testing new stuff
         */
	double errI = 0.0;
        if (std::abs(errP)*PAPS < 3) {
            double errI = ctrlData_.boards[act.driverAddrss.board].errI[act.driverAddrss.axis] + errP*dt;
        }
        
        ctrlData_.boards[act.driverAddrss.board].errI[act.driverAddrss.axis] = errI;
        
        /*
         * Run PID control law
         */
        /*driverData_.boards[act.driverAddrss.board].current[act.driverAddrss.axis] =
                ctrlData_.boards[act.driverAddrss.board].errP[act.driverAddrss.axis]
                        * ctrlData_.boards[act.driverAddrss.board].kP
                        + ctrlData_.boards[act.driverAddrss.board].errI[act.driverAddrss.axis]
                                * ctrlData_.boards[act.driverAddrss.board].kI
                        + ctrlData_.boards[act.driverAddrss.board].errD[act.driverAddrss.axis]
                                * ctrlData_.boards[act.driverAddrss.board].kD;*/
        driverData_.boards[act.driverAddrss.board].current[act.driverAddrss.axis] =
                ctrlData_.boards[act.driverAddrss.board].errP[act.driverAddrss.axis]
                        * ctrlData_.boards[act.driverAddrss.board].kP
                        + ctrlData_.boards[act.driverAddrss.board].errI[act.driverAddrss.axis]
                                * ctrlData_.boards[act.driverAddrss.board].kI
                        + 25;

        /*
         * Saturate control output to be within actuator limits
         */
        driverData_.boards[act.driverAddrss.board].current[act.driverAddrss.axis] = SATURATE_CURRENT(
                driverData_.boards[act.driverAddrss.board].current[act.driverAddrss.axis]);

    }

    if (resetIntegrators && resetIntegrators_)
    {
        resetIntegrators_ = false;
    }

    ctrlData_.latency = ros::Time::now() - ctrlData_.time;
    ctrlData_.delta = timeDiff_;
}

void PressureController::loadParams(void)
{
    std::stringstream paramName;
    for (int i = 0; i < GATEWAY_SENSOR_BOARDS_PER_GATEWAY; i++)
    {
        for (int j = 0; j < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; j++)
        {
            paramName.str("");
            paramName << paramServerPath_ << "/kp_" << i << "_" << j;
            RosUtils::getParam(paramName.str(), kP_[i][j]);

            paramName.str("");
            paramName << paramServerPath_ << "/ki_" << i << "_" << j;
            RosUtils::getParam(paramName.str(), kI_[i][j]);

            paramName.str("");
            paramName << paramServerPath_ << "/kd_" << i << "_" << j;
            RosUtils::getParam(paramName.str(), kD_[i][j]);
        }
    }
}

void PressureController::getInputs(void)
{
    ctrlData_ = prsCtrlDataQueue_->front_blocking();
    sensorData_ = sensorDataQueue_->front_blocking();
}

void PressureController::setOutputs(void)
{
    prsCtrlDataQueue_->push(ctrlData_);
    driverDataQueue_->push(driverData_);
}

void PressureController::setPressureCtrlGains(const king_louie::set_pressure_ctrl_gains_msg &msg)
{
    setPressureCtrlGainsQueue_.push(msg);

    std::stringstream paramName;
    paramName.str("");
    paramName << paramServerPath_ << "/kp_" << msg.boardIndex << "_" << msg.axisIndex;
    RosUtils::setParam(paramName.str(), msg.kp);

    paramName.str("");
    paramName << paramServerPath_ << "/ki_" << msg.boardIndex << "_" << msg.axisIndex;
    RosUtils::setParam(paramName.str(), msg.ki);

    paramName.str("");
    paramName << paramServerPath_ << "/kd_" << msg.boardIndex << "_" << msg.axisIndex;
    RosUtils::setParam(paramName.str(), msg.kd);
}

void PressureController::resetIntegrators(void)
{
    resetIntegrators_ = true;
}

}  // namespace low_level_control
