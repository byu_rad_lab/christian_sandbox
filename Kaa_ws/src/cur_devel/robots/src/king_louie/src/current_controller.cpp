/*
 * @file    current_controller.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Task that runs open loop current controllers
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "ros/ros.h"

#include "low_level_control/current_controller.h"

#include "low_level_control/gateway_data_types.h"
#include "low_level_control/robot_mapping.h"
#include "low_level_control/ros_utils.h"

namespace king_louie
{
CurrentController::CurrentController(const std::string &name, const std::string &paramServerBasePath,
        pneubotics::queue::Queue<current_controllers_msg> *prsCtrlDataQueue,
        pneubotics::queue::Queue<drivers_msg> *driverDataQueue) :
        RosTask(name, paramServerBasePath + "/" + name)
{
    prsCtrlDataQueue_ = prsCtrlDataQueue;
    driverDataQueue_ = driverDataQueue;
    ROS_INFO("%s", paramServerPath_.c_str());
}

CurrentController::~CurrentController()
{
}

void CurrentController::doInit(void)
{
}

void CurrentController::doRun()
{
    for (int actuator = 0; actuator < ACT_NUM_ACTUATORS; actuator++)
    {
        Actuator act = RobotMap[actuator];

        // Just directly set drive current to reference
        driverData_.boards[act.driverAddrss.board].current[act.driverAddrss.axis] =
                ctrlData_.boards[act.driverAddrss.board].refP[act.driverAddrss.axis];
    }

    ctrlData_.latency = ros::Time::now() - ctrlData_.time;
    ctrlData_.delta = timeDiff_;
}

void CurrentController::loadParams(void)
{
}

void CurrentController::getInputs(void)
{
    ctrlData_ = prsCtrlDataQueue_->front_blocking();
}

void CurrentController::setOutputs(void)
{
    prsCtrlDataQueue_->push(ctrlData_);
    driverDataQueue_->push(driverData_);
}

}  // namespace low_level_control
