/*
 * @file    real_time_loop.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Real time loop which runs in its own thread. Responsible for running all of the individual tasks related
 *          controlling the robot and interfacing with the embedded system.
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "low_level_control/real_time_loop.h"

namespace king_louie
{
RealTimeLoop::RealTimeLoop(const std::string &name, const std::string &paramServerBasePath,
        const pneubotics::time::Duration &period,
        pneubotics::queue::Queue<sensors_msg> *sensorDataQueue,
        pneubotics::queue::Queue<drivers_msg> *driverDataQueue,
        pneubotics::queue::Queue<pose_msg> *poseDataQueue,
        pneubotics::queue::Queue<pressure_controllers_msg> *prsCtrlDataQueue,
        pneubotics::queue::Queue<current_controllers_msg> *curCtrlDataQueue) :
        pneubotics::loop::Loop(name, period, pneubotics::thread::THREAD_PRIO_HIGH),
                paramServerPath_(paramServerBasePath + "/" + name_), loggableData_(name, "./data", sensorDataQueue,
                        driverDataQueue, poseDataQueue, prsCtrlDataQueue), controllerMode_(RTL_CTRL_MODE_CURRENT)
{
    sensorDataQueue_ = sensorDataQueue;
    driverDataQueue_ = driverDataQueue;
    poseDataQueue_ = poseDataQueue;
    prsCtrlDataQueue_ = prsCtrlDataQueue;
    curCtrlDataQueue_ = curCtrlDataQueue;

    commsHandler_ = new CommsHandler("CommsHandler", paramServerPath_, sensorDataQueue_, driverDataQueue_);
    poseEstimator_ = new PoseEstimator("PoseEstimator", paramServerPath_, sensorDataQueue_, poseDataQueue_);
    pressureRefGen_ = new PressureRefGen("PressureReferenceGenerator", paramServerPath_, sensorDataQueue_,
            prsCtrlDataQueue_);
    pressureController_ = new PressureController("PressureController", paramServerPath_, prsCtrlDataQueue_,
            driverDataQueue_, sensorDataQueue_);

    currentRefGen_ = new CurrentRefGen("CurrentReferenceGenerator", paramServerPath_, sensorDataQueue_,
            curCtrlDataQueue_);

    currentController_ = new CurrentController("CurrentController", paramServerPath_, curCtrlDataQueue_,
            driverDataQueue_);

    ROS_INFO("%s", paramServerPath_.c_str());
}

RealTimeLoop::~RealTimeLoop()
{
    if (commsHandler_)
    {
        delete commsHandler_;
    }
    if (poseEstimator_)
    {
        delete poseEstimator_;
    }
    if (pressureRefGen_)
    {
        delete pressureRefGen_;
    }
    if (pressureController_)
    {
        delete pressureController_;
    }
}

void RealTimeLoop::setPressureRef(const king_louie::set_pressure_ref_msg &msg)
{
    pressureRefGen_->setPressureRef(msg);
}

void RealTimeLoop::setCurrentRef(const king_louie::current_msg &msg)
{
  //std::cout << "RTL " << msg << std::endl;
    currentRefGen_->setCurrentRef(msg);
}

void RealTimeLoop::setPressureCtrlGains(const king_louie::set_pressure_ctrl_gains_msg &msg)
{
    pressureController_->setPressureCtrlGains(msg);
}

void RealTimeLoop::setControllerMode(const std_msgs::Int16 &msg)
{
    switch (msg.data)
    {
    case RTL_CTRL_MODE_CURRENT:
        controllerMode_ = RTL_CTRL_MODE_CURRENT;
        break;

    case RTL_CTRL_MODE_PRESSURE:
        controllerMode_ = RTL_CTRL_MODE_PRESSURE;
        break;

    default:
        ROS_WARN("Attempting to set invalid control mode: 0 = current, 1 = pressure, got %d", msg.data);
        break;
    }
}

void RealTimeLoop::resetIntegrators(void)
{
    pressureController_->resetIntegrators();
}

void RealTimeLoop::setLogging(const bool ena)
{
    if (ena)
    {
        dataLogger_.init(loggableData_);
    }
    else
    {
        dataLogger_.end();
    }
    logging_ = ena;
}

void RealTimeLoop::doLogging(void)
{
    dataLogger_.writeLogEntry(loggableData_);
}

void RealTimeLoop::doOwnWork(void)
{
    /*
     * Read sensors
     */
    try
    {
        commsHandler_->recv();
    }
    catch (exception::CommsRecvTimeoutException &err)
    {
        ROS_WARN("Timed out while reading socket. Skipping rest of %s work.", name_.c_str());
        return;
    }
    catch (exception::CommsRecvException &err)
    {
        ROS_WARN("Problem with received packet. Skipping rest of %s work.", name_.c_str());
        return;
    }

    /*
     * Estimate Pose
     */
    poseEstimator_->run();

    switch (controllerMode_)
    {
    case RTL_CTRL_MODE_CURRENT:
        /*
         * Generate References
         */
        currentRefGen_->run();

        /*
         * Run Controllers
         */
        currentController_->run();
        break;

    case RTL_CTRL_MODE_PRESSURE:
        /*
         * Generate References
         */
        pressureRefGen_->run();

        /*
         * Run Controllers
         */
        pressureController_->run();
        break;

    default:
        ROS_FATAL("Invalid controller mode is selected %d", controllerMode_);
        break;
    }

    /*
     * Send Driver Commands
     */
    commsHandler_->send();

}

void RealTimeLoop::doOwnWorkPre(void)
{
}

void RealTimeLoop::doOwnWorkPost(void)
{
}

void RealTimeLoop::doOwnInit(void)
{
    pressure_controllers_msg tmpPrsMsg;
    current_controllers_msg tmpCurMsg;

    // Initialize controller references to zero
    for (int i = 0; i < GATEWAY_DRIVER_BOARDS_PER_GATEWAY; i++)
    {
        for (int j = 0; j < GATEWAY_DRIVERS_PER_DRIVERS_BOARD; j++)
        {
            tmpPrsMsg.boards[i].refP[j] = 0.0;
            tmpPrsMsg.boards[i].refI[j] = 0.0;
            tmpPrsMsg.boards[i].refD[j] = 0.0;

            tmpCurMsg.boards[i].refP[j] = 0.0;
        }
    }
    prsCtrlDataQueue_->push(tmpPrsMsg);
    curCtrlDataQueue_->push(tmpCurMsg);

    commsHandler_->init();
    poseEstimator_->init();
    pressureRefGen_->init();
    currentRefGen_->init();
    pressureController_->init();
    currentController_->init();
}

void RealTimeLoop::debug(const std::string &msg)
{
    ROS_DEBUG("%s", msg.c_str());
}

void RealTimeLoop::info(const std::string &msg)
{
    ROS_INFO("%s", msg.c_str());
}

void RealTimeLoop::warn(const std::string &msg)
{
    ROS_WARN("%s", msg.c_str());
    //std::cout << "hello" <<std::endl;
}

void RealTimeLoop::error(const std::string &msg)
{
    ROS_ERROR("%s", msg.c_str());
}

void RealTimeLoop::fatal(const std::string &msg)
{
    ROS_FATAL("%s", msg.c_str());
}

}  // namespace low_level_control
