/*
 * @file    pressure_reference_generator.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Task that generates pressure references to feed into the pressure controller
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "ros/ros.h"
#include "low_level_control/pressure_reference_generator.h"

#include "low_level_control/gateway_data_types.h"

namespace king_louie
{
PressureRefGen::PressureRefGen(const std::string &name, const std::string &paramServerBasePath,
        pneubotics::queue::Queue<sensors_msg> *sensorDataQueue,
        pneubotics::queue::Queue<pressure_controllers_msg> *prsCtrlDataQueue) :
        RosTask(name, paramServerBasePath + "/" + name)

{
    sensorDataQueue_ = sensorDataQueue;
    prsCtrlDataQueue_ = prsCtrlDataQueue;
    ROS_INFO("%s", paramServerPath_.c_str());
}

PressureRefGen::~PressureRefGen()
{
}

void PressureRefGen::doInit(void)
{
}

void PressureRefGen::doRun(void)
{
    ctrlData_.time = sensorData_.time;

    if (pressureRefUpdateRequests_.size() > 0)
    {
        king_louie::set_pressure_ref_msg msg = pressureRefUpdateRequests_.pop();
        ctrlData_.boards[msg.boardIndex].refP[msg.axisIndex] = msg.refP;
        ctrlData_.boards[msg.boardIndex].refD[msg.axisIndex] = msg.refD;
    }
}

void PressureRefGen::getInputs(void)
{
    sensorData_ = sensorDataQueue_->front_blocking();
    ctrlData_ = prsCtrlDataQueue_->front_blocking();
}

void PressureRefGen::setOutputs(void)
{
    prsCtrlDataQueue_->push(ctrlData_);
}

void PressureRefGen::PressureRefGen::loadParams(void)
{
}

void PressureRefGen::setPressureRef(const king_louie::set_pressure_ref_msg &msg)
{
    if ((msg.boardIndex < 0) || (msg.boardIndex >= GATEWAY_DRIVER_BOARDS_PER_GATEWAY))
    {
        ROS_WARN("Board index out of range. Min %d, Max %d", 0,
                GATEWAY_DRIVER_BOARDS_PER_GATEWAY-1);
        return;
    }

    if ((msg.axisIndex < 0) || (msg.axisIndex >= GATEWAY_DRIVERS_PER_DRIVERS_BOARD))
    {
        ROS_WARN("Axis index out of range. Min %d, Max %d", 0,
                GATEWAY_DRIVERS_PER_DRIVERS_BOARD-1);
        return;
    }

    pressureRefUpdateRequests_.push(msg);
}

}  // namespace low_level_control
