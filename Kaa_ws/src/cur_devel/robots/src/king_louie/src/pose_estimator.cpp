/*
 * @file    pose_estimator.cpp
 *
 * @date    August 2014
 *
 * @author  Gabriel Hein
 *
 * @brief   Task that estimates the system pose
 *
 * @copyright   All information contained herein is, and remains the property of Pneubotics and Otherlab.
 *              The intellectual and technical concepts contained herein are proprietary and may be covered
 *              by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 *              copyright law. Dissemination of this information or reproduction of this material is
 *              strictly forbidden unless prior written permission is obtained from Pneubotics and Otherlab.
 *
 *              This code and information are provided "as is" without warranty of any kind, either expressed
 *              or implied, including but not limited to the implied warranties of merchantability and/or
 *              fitness for a particular purpose.
 */
#include "ros/ros.h"

#include "low_level_control/pose_estimator.h"

#include "low_level_control/gateway_data_types.h"

#include <math.h>

namespace king_louie
{

static Eigen::Matrix3d skew(const Eigen::Vector3d &vec)
{
    Eigen::Matrix3d out;

    out << 0, -vec[2], vec[1], vec[2], 0, -vec[0], -vec[1], vec[0], 0;

    return out;
}

IMUFilter::IMUFilter()
{
    a_ << 0.0, 0.0, 0.0;
    w_ << 0.0, 0.0, 0.0;
    m_ << 0.0, 0.0, 0.0;

    a0_ << 0.0, 0.0, 0.0;
    m0_ << 0.0, 0.0, 0.0;

    aEst_ << 0.0, 0.0, 0.0;
    mEst_ << 0.0, 0.0, 0.0;

    rWM_ << 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0,
            0.0, 0.0, 0.0;
}

IMUFilter::~IMUFilter()
{

}

void IMUFilter::setParamServerPath(const std::string &path)
{
    paramServerPath_ = path;
}

void IMUFilter::loadParams(void)
{
    std::stringstream paramName;
    paramName << paramServerPath_ << "/a_sat";
    RosUtils::getParam(paramName.str(), p_.aSat_);

    paramName.str("");
    paramName << paramServerPath_ << "/w_sat";
    RosUtils::getParam(paramName.str(), p_.wSat_);

    paramName.str("");
    paramName << paramServerPath_ << "/k_acc";
    RosUtils::getParam(paramName.str(), p_.ka_);

    paramName.str("");
    paramName << paramServerPath_ << "/k_mag";
    RosUtils::getParam(paramName.str(), p_.km_);
}

// Updating the filter performs various data manipulations on the newest
// set of data from the sensors.
void IMUFilter::UpdateFilter(const double * acc, const double * gyr, const double * mag, double dt)
{
    // Remove offsets from a, w, and m (the most recent data sets from the sensors).
    a_ << acc[0], acc[1], acc[2];
    w_ << gyr[0], gyr[1], gyr[2];
    m_ << mag[0], mag[1], mag[2];

    // Scale the acceleration gain if we are moving a lot.
    double scale_a = 1.0 - SATURATE(fabs(1.0 - a_.norm() / p_.aSat_), 0.0, 1.0);
    double scale_w = SATURATE(fabs(1.0 - w_.norm() / p_.wSat_), 0.0, 1.0);
    double scale = scale_a * scale_w;

    // Compute the error.
    aErr_ = a_.cross(aEst_);
    mErr_ = m_.cross(mEst_);

    // Add correction angular velocity to the measured angular velocity.
    Eigen::Vector3d w_hat = w_ + (scale * p_.ka_ * aErr_) + (p_.km_ * mErr_);

    // Calculate the rotation matrix derivative.
    Eigen::Matrix3d R_dot = rWM_ * skew(w_hat);

    // Integrate to find the new rotation matrix.
    Eigen::Matrix3d R_new = rWM_ + R_dot * dt;

    // Orthogonalize the rotation matrix.
    Eigen::Vector3d x_vec;
    Eigen::Vector3d y_vec;
    Eigen::Vector3d z_vec;
    x_vec = R_new.col(0);
    y_vec = R_new.col(2).cross(x_vec);
    z_vec = x_vec.cross(y_vec);

    // Normalize and set the new rotation matrix.
    rWM_.col(0) = x_vec.normalized();
    rWM_.col(1) = y_vec.normalized();
    rWM_.col(2) = z_vec.normalized();

    // Compute the accel and mag vectors estimated using the filtered rotation
    // matrix and the initial a and m values.
    aEst_ = rWM_.transpose() * a0_;
    mEst_ = rWM_.transpose() * m0_;
}

// Resetting the filter defines the initial set of axes as the reference axes.
void IMUFilter::ResetFilter(const double *acc, const double*mag)    //Eigen::Vector3d a, Eigen::Vector3d m)
{
    // Remove offsets.
    a_ << acc[0], acc[1], acc[2];
    m_ << mag[0], mag[1], mag[2];

    // Find orthagonal set of axes.
    Eigen::Vector3d z_hat = a_;
    Eigen::Vector3d y_hat = z_hat.cross(m_);
    Eigen::Vector3d x_hat = y_hat.cross(z_hat);

    // Normalize axes.
    z_hat.normalize();
    y_hat.normalize();
    x_hat.normalize();

    // Create ortho-normal rotation matrix from node frame to world frame.
    rWM_.row(0) = x_hat.transpose();
    rWM_.row(1) = y_hat.transpose();
    rWM_.row(2) = z_hat.transpose();

    // Maps initial acc, mag vectors into the inertial frame.
    a0_ = rWM_ * a_;
    m0_ = rWM_ * m_;

    // initialize estimates to current sensor readings
    aEst_ = Eigen::Vector3d(a_);
    mEst_ = Eigen::Vector3d(m_);
}

Eigen::Matrix3d IMUFilter::getRwmEstimate(void)
{
    return Eigen::Matrix3d(rWM_);
}

Eigen::Vector3d IMUFilter::getAccEst(void)
{
    return aEst_;
}
Eigen::Vector3d IMUFilter::getMagEst(void)
{
    return mEst_;
}

PoseEstimator::PoseEstimator(const std::string &name, const std::string &paramServerBasePath,
        pneubotics::queue::Queue<sensors_msg> *sensorDataQueue,
        pneubotics::queue::Queue<pose_msg> *poseDataQueue) :
        RosTask(name, paramServerBasePath + "/" + name), initFilter_(true)

{
    sensorDataQueue_ = sensorDataQueue;
    poseDataQueue_ = poseDataQueue;
    ROS_INFO("%s", paramServerPath_.c_str());
}

PoseEstimator::~PoseEstimator()
{
}

void PoseEstimator::doInit(void)
{
}

void PoseEstimator::doRun(void)
{
    poseData_.time = sensorData_.time;
    double dt = timeDiff_.toSec();
    for (int i = 0; i < GATEWAY_SENSOR_BOARDS_PER_GATEWAY; i++)
    {
        if (initFilter_)
        {
            imuFilter_[i].ResetFilter(&sensorData_.boards[i].acc[0], &sensorData_.boards[i].mag[0]);
        }
        else
        {
            imuFilter_[i].UpdateFilter(&sensorData_.boards[i].acc[0], &sensorData_.boards[i].gyr[0],
                    &sensorData_.boards[i].mag[0], dt);
        }

        Eigen::Matrix3d tmpRot = imuFilter_[i].getRwmEstimate();

        int idx = 0;
        for (int row = 0; row < 3; row++)
        {
            for (int col = 0; col < 3; col++)
            {
                poseData_.boards[i].rotationMatrix[idx++] = tmpRot(row, col);
            }
        }

        Eigen::Vector3d aEst = imuFilter_[i].getAccEst();
        Eigen::Vector3d mEst = imuFilter_[i].getMagEst();
        for (int j = 0; j < 3; j++)
        {
            poseData_.boards[i].acc[j] = aEst[j];
            poseData_.boards[i].mag[j] = mEst[j];
        }
    }
    initFilter_ = false;
}

void PoseEstimator::getInputs(void)
{
    sensorData_ = sensorDataQueue_->front_blocking();
}

void PoseEstimator::setOutputs(void)
{
    poseDataQueue_->push(poseData_);
}

void PoseEstimator::loadParams(void)
{
    for (int i = 0; i < GATEWAY_SENSOR_BOARDS_PER_GATEWAY; i++)
    {
        imuFilter_[i].setParamServerPath(paramServerPath_);
        imuFilter_[i].loadParams();
    }
}

}  // namespace low_level_control
