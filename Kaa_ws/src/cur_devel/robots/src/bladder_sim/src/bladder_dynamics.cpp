

#include "bladder_sim/bladder_dynamics.h"


BladderDynamics::BladderDynamics()
{
}

//BladderDynamics::BladderDynamics(double const1, double const2, double const3, double const4, double const5, double send_p0, double send_pdes, int calcRate, ros::NodeHandle & nh, std::string myID)

BladderDynamics::BladderDynamics ( double constC, double constAlpha1, double constAlpha2, double send_p0, double send_pdes, double signSet, double constA, double constB, double constD)
  {

    a = constA;
    b = constB;
    c = constC;
    d = constD;
    alpha1 = constAlpha1;
    alpha2 = constAlpha2;
    sign = signSet;
    p0 = send_p0;
    pdes = send_pdes;
    pMax = 200; // kPa
    pMin = 0; // kPa
    qdot = 0;
    if(p0 < pMin)
      {
	p0 = pMin;
      }
    if(pdes < pMax)
      {
	pdes = pMax;
      }

    //dT = 1/double(calcRate);
    p = p0;
    pdot = 0.0;
    start = ros::Time::now().toSec();
    std::cout << "Start is : " << start << std::endl;
    now = ros::Time::now().toSec() - start;
    last = 0;
    //ros::Subscriber sub = nh.subscribe("/pdes", 1000, &BladderDynamics::pdesCallback, this);
  }

// void BladderDynamics::pdesCallback(const std_msgs::Float64& msg)
//    {
//      pdes = msg.data;
//      std::cout << "Got in callback" << std::endl;
//   }

  double BladderDynamics::calcP()
  {
    double A = -b/a;
    double invA = 1/A;
    double B1 = c/a;
    double B2 = d/a;
    double Ad = exp(A*dT);
    double Bd1 = invA*(Ad-1)*B1;
    double Bd2 = invA*(Ad-1)*B2;
    p = Ad*p+Bd1*pdes+sign*Bd2*qdot;
    return p;
    //return (c/b)*pdes + (p0 - c/b*pdes)*exp(-(b/a)*now);
  }

  double BladderDynamics::calcPdot()
  {
    return -b/a*p + c/a*pdes + sign*d/a*qdot;
  }

  void BladderDynamics::update(double qdotSet)
  {
    
    now = ros::Time::now().toSec() - start;
    stepTime = now-last;
    dT = stepTime;
    setqdot(qdotSet);
    p = calcP();
    if(p < pMin)
      {
	p = pMin;
      }
    else if(p>pMax)
      {
	p=pMax;
      }
    pdot = calcPdot();
    last = now;
    // std::cout << " P = " << p << std::endl;

  }

void BladderDynamics::getPressure(double& pressure, double qdotSet)
  {
    update(qdotSet);
    pressure = p;
  }

void BladderDynamics::getPressureDot(double& pressure_dot, double qdotSet)
  {
    update(qdotSet);
    pressure_dot = pdot;
  }

  void BladderDynamics::setPdes(double new_pdes)
  {
    pdes = new_pdes;

        if(pdes > pMax)
     {
       std::cout << "The pressure cannot be commanded higher than pMax" << std::endl;
    //pdes = 35;
     }
  }
  
double BladderDynamics::getTorque()
{
  double tau;
  tau =  alpha1*p + alpha2;
  return tau;
}

std::vector<double> BladderDynamics::getData(double qdotSet)
{
  std::vector<double> data(3);
  update(qdotSet);
  data[0] = p;
  data[1] = pdot;
  data[2] = getTorque();

  return data;   
}

void BladderDynamics::setqdot(double qdotset)
{
  qdot = qdotset;
}
