
#include "bladder_sim/multiple_bladders.h"

MultipleBladders::MultipleBladders()
{
}

MultipleBladders::MultipleBladders(int numJoints, ros::NodeHandle& nh, std::string armName, ros::CallbackQueue rosQueue)
{
  _nh = nh;
  numJoints_ = numJoints;
  const double constA = 0.25;
  const double constB = 1.767;
  const double constC= 1.772;
  const double constAlpha1 = 0.629775;
  const double constAlpha2 = 0; //-0.3548;
  double sendP = 0;
  double sendPdes = 20;
  pPlusDes.resize(numJoints_);
  pMinusDes.resize(numJoints_);

  // std::cout << "Number of Joints : " << numJoints_ << std::endl;

  for(int i=0;i < numJoints_;i++)
    {
      // std::cout << "Loop Count : " << i << std::endl;
      bladdersPlus.push_back(BladderDynamics(constC, constAlpha1, constAlpha2, sendP, sendPdes, -1, constA, constB));
      bladdersMinus.push_back(BladderDynamics(constC, constAlpha1, constAlpha2, sendP, sendPdes, 1, constA, constB));
    }
  // bladdersPlus.push_back(BladderDynamics(const3, const4, const5, sendP, sendPdes, const1, const2));
  // bladdersMinus.push_back(BladderDynamics(const3, const4, const5, sendP, sendPdes, const1, const2));
  std::cout << "Done making bladders" << std::endl;
  std::string topicName = "/pdes" + armName;
  // ros::SubscribeOptions so = ros::SubscribeOptions::create<bladder_dynamics::pdes_msgs>(topicName, 10, boost::bind(&MultipleBladders::pdesCallbacks, this, _1), ros::VoidPtr(), &rosQueue);

  //   this->sub = this->_nh.subscribe(so);
  // sub = nh.subscribe(topicName, 10, &MultipleBladders::pdesCallbacks,this);

}

MultipleBladders::MultipleBladders(int numJoints, ros::NodeHandle& nh, std::string armName)
{
  _nh = nh;
  numJoints_ = numJoints;
  const double constA = 0.25;
  const double constB = 1.767;
  const double constC= 1.772;
  const double constAlpha1 = 0.629775;
  const double constAlpha2 = 0; //-0.3548;
  double sendP = 0;
  double sendPdes = 20;
  pPlusDes.resize(numJoints_);
  pMinusDes.resize(numJoints_);

  // std::cout << "Number of Joints : " << numJoints_ << std::endl;

  for(int i=0;i < numJoints_;i++)
    {
      bladdersPlus.push_back(BladderDynamics(constC, constAlpha1, constAlpha2, sendP, sendPdes, -1, constA, constB));
      bladdersMinus.push_back(BladderDynamics(constC, constAlpha1, constAlpha2, sendP, sendPdes, 1, constA, constB));
      std::cout << "Loop Count : " << i << std::endl;
      // this->bladdersPlus.push_back(BladderDynamics(const3, const4, const5, sendP, sendPdes, const1, const2));
      // bladdersMinus.push_back(BladderDynamics(const3, const4, const5, sendP, sendPdes, const1, const2));
    }
  // bladdersPlus.push_back(BladderDynamics(const3, const4, const5, sendP, sendPdes, const1, const2));
  // bladdersMinus.push_back(BladderDynamics(const3, const4, const5, sendP, sendPdes, const1, const2));
  std::cout << "Done making bladders" << std::endl;
  
  // sub = nh.subscribe("/pdes", 10, &MultipleBladders::pdesCallbacks,this, ros::VoidPtr(), rosQueue);
  std::string topicName = "/pdes" + armName;
  // sub = _nh.subscribe(topicName, 10, &MultipleBladders::pdesCallbacks,this);
}


// void MultipleBladders::pdesCallbacks(const bladder_dynamics::pdes_msgs msg)
// {
//   std::cout << "Here" << std::endl;
//   std::cout << "NumJoints_ : " << numJoints_ << std::endl;
//   int numJoints = this->bladdersPlus.size();
//   int msgSize = msg.pPlusDes.size();
//   std::cout << "NumJoints : " << numJoints << std::endl;
//   std::cout << "msgSize : " << msgSize << std::endl;
//   std::cout << "pPlusDes size : " << this->pPlusDes.size() << std::endl;
//   if(numJoints > msgSize)
//     {
//       numJoints = msgSize;
//     }
//   std::cout << "NumJoints : " << numJoints << std::endl;
//   for(int i=0;i < msgSize;i++)
//     {
//       bladdersPlus[i].setPdes(msg.pPlusDes[i]);
//       bladdersMinus[i].setPdes(msg.pMinusDes[i]);
//       std::cout << msg.pPlusDes[i] << " " << msg.pMinusDes[i] << std::endl;
//       std::cout << "i " << i << std::endl;
//       // pPlusDes[i] = msg.pPlusDes[i];
//       // pMinusDes[i] = msg.pMinusDes[i];
//     }
// }

void MultipleBladders::getPressures(std::vector<double>& pPlus, std::vector<double>& pMinus, std::vector<double> qdot)
{
 

  for(int i=0;i < numJoints_;i++)
    {
      std::vector<double> data;
      data = bladdersPlus[i].getData(qdot[i]);
      pPlus[i] = data[0];
      data = bladdersMinus[i].getData(qdot[i]);
      pMinus[i] = data[0];
    }
  
}


void MultipleBladders::getPressureDots(std::vector<double>& pdotPlus, std::vector<double>& pdotMinus, std::vector<double> qdot)
{

  for(int i=0;i < numJoints_;i++)
    {
      std::vector<double> data;
      data = bladdersPlus[i].getData(qdot[i]);
      pdotPlus[i] = data[1];
      data = bladdersMinus[i].getData(qdot[i]);
      pdotMinus[i] = data[1];

    }
  
}

void MultipleBladders::getTorques(std::vector<double>& tauPlus, std::vector<double>& tauMinus, std::vector<double> qdot)
{

   for(int i=0;i < numJoints_;i++)
    {

      std::vector<double> data;
      data = bladdersPlus[i].getData(qdot[i]);
      tauPlus[i] = data[2];
      data = bladdersMinus[i].getData(qdot[i]);
      tauMinus[i] = data[2];
	    
    }
  
}

void MultipleBladders::setPressures(std::vector<double> pPlusDesSet, std::vector<double> pMinusDesSet)
{
 
  for(int i=0;i < numJoints_;i++)
    {
      bladdersPlus[i].setPdes(pPlusDesSet[i]);
      bladdersMinus[i].setPdes(pMinusDesSet[i]);
      // std::cout << "Set Bladder Plus " << i << " to " << pPlusDesSet[i] << " kPa" << std::endl;
    }
}


