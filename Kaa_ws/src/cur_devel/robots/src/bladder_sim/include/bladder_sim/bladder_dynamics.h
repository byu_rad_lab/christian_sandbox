#ifndef BLADDER_DYNAMICS_H_
#define BLADDER_DYNAMICS_H_

#include "ros/ros.h"
#include <time.h>
#include <iostream>
#include "std_msgs/Float64.h"
#include <math.h>

/*
Pressure dynamics are as follows
Pdot+ = -b/a*P+ + c/a*Pdes+ - d/a*qdot 
Pdot- = -b/a*P- + c/a*Pdes- + d/a*qdot 
 */


class BladderDynamics
{

 public:
  BladderDynamics();
  BladderDynamics(double constC, double constAlpha1, double constAlpha2, double send_p0, double send_pdes, double signSet,  double constA=0.25, double constB = 1.767, double costD = .50 );
  void getPressure(double& pressure, double qdotSet);
  void getPressureDot(double& pressure_dot, double qdotSet);
  void setPdes(double new_pdes);
  void setqdot(double qdotset);
  std::vector<double> getData(double qdotSet);
  double getTorque();

  
  
 protected:
  
  std::string id;
  double stepTime;
  double a;
  double alpha1;
  double alpha2;
  double b;
  double c;
  double d;
  double qdot;
  double p0;
  double pdes;
  double p;
  double pdot;
  double start;
  double now;
  double last;
  double dT;
  double pMax;
  double pMin;
  double sign;

  //void pdesCallback(const std_msgs::Float64& msg);
  double calcP(void);
  double calcPdot(void);
  void update(double qdotSet);  
  


};

#endif


