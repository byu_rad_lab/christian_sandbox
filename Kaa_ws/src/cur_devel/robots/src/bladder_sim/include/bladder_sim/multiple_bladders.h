#ifndef MULTIPLE_BLADDERS_H_
#define MULTIPLE_BLADDERS_H_

#include "ros/ros.h"
#include <time.h>
#include <iostream>
#include "std_msgs/Float64.h"
#include <math.h>
#include "bladder_sim/bladder_dynamics.h"
/* #include "bladder_dynamics/pdes_msgs.h" */
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"


class MultipleBladders
{
 public:
  MultipleBladders();
  MultipleBladders(int numJoints, ros::NodeHandle& nh, std::string armName, ros::CallbackQueue rosQueue);
  MultipleBladders(int numJoints, ros::NodeHandle& nh, std::string armName);
  void getPressures(std::vector<double>& pPlus, std::vector<double>& pMinus, std::vector<double> qdot);
  void getPressureDots(std::vector<double>& pdotPlus, std::vector<double>& pdotMinus, std::vector<double> qdot);
  void getTorques(std::vector<double>& tauPlus, std::vector<double>& tauMinus, std::vector<double> qdot);
  void setPressures(std::vector<double> pPlusDesSet, std::vector<double> pMinusDesSet);
  int numJoints_;
  std::vector<BladderDynamics> bladdersPlus;
  std::vector<BladderDynamics> bladdersMinus;
  std::vector<double> pPlusDes;
  std::vector<double> pMinusDes;
  ros::Subscriber sub;
  /* void pdesCallbacks(const bladder_dynamics::pdes_msgs msg); */

 private:





  ros::NodeHandle _nh;
  



};
    
#endif
