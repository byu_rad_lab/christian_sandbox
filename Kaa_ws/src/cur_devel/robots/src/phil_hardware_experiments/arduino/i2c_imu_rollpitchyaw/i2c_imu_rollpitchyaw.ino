#include <Wire.h>
#include <Adafruit_Sensor.h> //No use of Wire.h
#include <Adafruit_LSM303_U.h>
#include <Adafruit_L3GD20_U.h>
#include <Adafruit_10DOF.h>


/* Assign a unique ID to the sensors */
Adafruit_10DOF                dof   = Adafruit_10DOF();


//TODO - Pass the i2c bus as an argument. It should use that bus instead
// of Wire
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(30301);
Adafruit_LSM303_Mag_Unified   mag   = Adafruit_LSM303_Mag_Unified(30302);

int i2c_address = 13;

sensors_vec_t   orientation;



/**************************************************************************/
/*!
    @brief  Initialises all the sensors used by this example
*/
/**************************************************************************/
void initSensors()
{
  if(!accel.begin())
  {
    /* There was a problem detecting the LSM303 ... check your connections */
    Serial.println(F("Ooops, no LSM303 detected ... Check your wiring!"));
    while(1);
  }
  if(!mag.begin())
  {
    /* There was a problem detecting the LSM303 ... check your connections */
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while(1);
  }
}

/**************************************************************************/
/*!

*/
/**************************************************************************/
void setup(void)
{
//  HardWire.begin(i2c_address);
//  HardWire.onReceive(receiveEvent);
//  HardWire.onRequest(requestEvent);
  
  Serial.begin(115200);
  Serial.println(F("Adafruit 10 DOF Pitch/Roll/Heading Example")); Serial.println("");
  
  /* Initialise the sensors */
  initSensors();
}

/**************************************************************************/
/*!
    @brief  Constantly check the roll/pitch/heading/altitude/temperature
*/
/**************************************************************************/
void loop(void)
{
  sensors_event_t accel_event;
  sensors_event_t mag_event;
  

  /* Calculate pitch and roll from the raw accelerometer data */
  accel.getEvent(&accel_event);
  dof.accelGetOrientation(&accel_event, &orientation);

  /* Calculate the yaw using the magnetometer */
  mag.getEvent(&mag_event);
  dof.magGetOrientation(SENSOR_AXIS_Z, &mag_event, &orientation);
  
  if (1)
  {
    Serial.print(F("Roll: "));
    Serial.print(orientation.roll);
    Serial.print(F("; "));
    Serial.print(F("Pitch: "));
    Serial.print(orientation.pitch);
    Serial.print(F("; "));
    Serial.print(F("Heading: "));
    Serial.print(orientation.heading);
    Serial.println(F("; "));
  }

  /* Calculate the altitude using the barometric pressure sensor */
//  bmp.getEvent(&bmp_event);
//  if (bmp_event.pressure)
//  {
//    /* Get ambient temperature in C */
//    float temperature;
//    bmp.getTemperature(&temperature);
//    /* Convert atmospheric pressure, SLP and temp to altitude    */
//    Serial.print(F("Alt: "));
//    Serial.print(bmp.pressureToAltitude(seaLevelPressure,
//                                        bmp_event.pressure,
//                                        temperature)); 
//    Serial.print(F(" m; "));
//    /* Display the temperature */
//    Serial.print(F("Temp: "));
//    Serial.print(temperature);
//    Serial.print(F(" C"));
//  }
  
//  Serial.println(F(""));
//  delay(1000);
}

void receiveEvent(int howMany)
{
}

void requestEvent()
{
  char rpychar[4*3]; //4 byte float, 3 floats
  memcpy(&rpychar[0], &orientation.roll, 4);
  memcpy(&rpychar[4], &orientation.pitch, 4);
  memcpy(&rpychar[8], &orientation.heading, 4);
//  HardWire.write(rpychar,sizeof(rpychar));
}
