#include "pins_arduino.h"

const int num_bytes = 8;

volatile byte pos;
volatile bool process_it = false;

char pcmd_char [num_bytes];
char p_char [num_bytes];

volatile char rx_buf [num_bytes];
volatile char tx_buf[num_bytes];

float p[4] = {0.12,0.34,0.56,0.78};
float pcmd[4];
float t = 0;



void setup (void)
{
// Serial.begin (2000000);
 memcpy(&tx_buf[0], &p, num_bytes);

 // have to send on master in, *slave out*
 // WITH MULTIPLE SLAVES, THIS PIN MUST BE OUTPUT ONLY WHEN IT IS THE SELECTED SLAVE
 // OTHERWISE IT MUST BE HIGH IMPEDANCE
 pinMode(MISO, OUTPUT);
 
 // turn on SPI in slave mode
 SPCR |= _BV(SPE);
 
 // turn on interrupts
 SPCR |= _BV(SPIE);
 
 pos = 0;
 SPDR = tx_buf[0];
}


// SPI interrupt routine
ISR (SPI_STC_vect)
{
  rx_buf [pos] = SPDR;
  SPDR = tx_buf[++pos];
  //delay(10);

  if (pos >= num_bytes)
  {  
//    memcpy(&pcmd_char[0], &rx_buf, num_bytes);
//    memcpy(&tx_buf[0], &p_char, num_bytes);
//    pos = 0;   
//    SPDR = tx_buf[0];
    process_it = true;
  }
}

// main loop - wait for flag set in interrupt routine
void loop (void)
{
  for(int i=0; i<4; i++)
  {
    float_to_two_bytes(p[i],&p_char[i*2]);
    pcmd[i] = two_bytes_to_float(&pcmd_char[i*2]);
  }
  
  if(process_it==true)
  {
    SPDR = tx_buf[0];
    pos = 0;
    
    memcpy(&pcmd_char[0], &rx_buf, num_bytes);
    memcpy(&tx_buf[0], &p_char, num_bytes);
    process_it=false;
  }

  p[0] = 25*sin(t) + 70;
  p[1] = 5*cos(t) + 20;
  p[2] = 10*sin(t) + 30;
  p[3] = 10*cos(t) + 40;

  t = t+.001;
  
//  Serial.print(pcmd[0]);
//  Serial.print("    ");
//  Serial.print(pcmd[1]);
//  Serial.print("    ");
//  Serial.print(pcmd[2]);
//  Serial.print("    ");
//  Serial.println(pcmd[3]);

//  Serial.print(p[0]);
//  Serial.print("    ");
//  Serial.print(p[1]);
//  Serial.print("    ");
//  Serial.print(p[2]);
//  Serial.print("    ");
//  Serial.println(p[3]);  

  //memcpy(&pcmd[0], &pcmd_char, num_bytes);
  //memcpy(&p_char[0], &p, num_bytes);  
}

float two_bytes_to_float(unsigned char * twobytes)
{
  uint16_t myint;
  memcpy(&myint, twobytes, 2);
  return float(100.0*myint/65536.0);
}

void float_to_two_bytes(float myfloat, unsigned char * twobytes)
{
  uint16_t myint = myfloat*65536.0/100.0;
  memcpy(twobytes, &myint, 2);
}
