
#include<Wire.h>

int analogPins[4] = {A0, A1, A2, A3};
int driverPins[12] = {2, 4, 3, 2, 4, 5, 7, 8, 6, 9, 10, 11};

float p[4] = {0, 0, 0, 0};
float pcmd[4] = {0, 0, 0, 0};
char pchar[2 * 4];
char pcmdchar[2 * 4];

int togglePin = 12;
float t = 0;

float kp = 10.0;
int i2c_address = 13;

int filtered = 0; //For testing noise through analog filter
int unfiltered = 0; // For noise comparison with filtered data

void setup()
{
  // This makes the analogRead function faster
  bitClear(ADCSRA, ADPS0);
  bitSet(ADCSRA, ADPS1);
  bitClear(ADCSRA, ADPS2);

  for (int i = 0; i < 4; i++)
  {
    pinMode(analogPins[i], INPUT);
  }
  for (int i = 0; i < 4; i++)
  {
    pinMode(driverPins[i], OUTPUT);
  }

  pinMode(togglePin, OUTPUT);

  Wire.begin(i2c_address);
  Wire.onReceive(receiveEvent);
  Wire.onRequest(requestEvent);
  Wire.setClock(400000);

  //  Serial.begin(9600);
}

void loop()
{
//  filtered = analogRead(analogPins[0]); //read voltage on pin A0
//  unfiltered = analogRead(analogPins[1]); //read voltage on pin A1
//
//  Serial.println(filtered); //output filtered noise data
//  Serial.println(unfiltered); // output unfiltered noise data

  for (int i = 0; i < 4; i++)
  {
    p[i] = analogRead(analogPins[i]) * 60.0 / 1024.0;

    if (pcmd[i] - p[i] > 0)
    {
      digitalWrite(driverPins[3 * i + 0], 1);
      digitalWrite(driverPins[3 * i + 1], 0);
    }
    else
    {
      digitalWrite(driverPins[3 * i + 0], 0);
      digitalWrite(driverPins[3 * i + 1], 1);
    }
    int cmd = min(255, int(abs(pcmd[i] - p[i]) * kp));
    analogWrite(driverPins[3 * i + 2], cmd);
  }

  //Flip pin 12 to get an external measure of loop frequency
  PORTB ^= _BV(PB4);

  for (int i = 0; i < 4; i++)
  {
    float_to_two_bytes(p[i], &pchar[i * 2]);
    pcmd[i] = two_bytes_to_float(&pcmdchar[i * 2]);
  }
}

void receiveEvent(int howMany)
{
  if (howMany > 1)
  {
    Wire.read();
    for (int j = 0; j < 4 * 2; j++)
    {
      pcmdchar[j] = Wire.read();
    }
  }
  else
  {
    Wire.read();
  }
}

void requestEvent()
{
  Wire.write(pchar, sizeof(pchar));
}




float two_bytes_to_float(unsigned char * twobytes)
{
  uint16_t myint;
  memcpy(&myint, twobytes, 2);
  return float(100.0 * myint / 65536.0);
}

void float_to_two_bytes(float myfloat, unsigned char * twobytes)
{
  uint16_t myint = myfloat * 65536.0 / 100.0;
  memcpy(twobytes, &myint, 2);
}
