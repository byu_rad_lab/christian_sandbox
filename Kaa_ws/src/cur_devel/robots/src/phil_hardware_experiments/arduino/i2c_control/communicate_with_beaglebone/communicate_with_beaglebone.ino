#include <Wire.h>
#include <stdio.h>

float myFloat = 0;

void setup()
{
  Wire.begin(5);                // join i2c bus with address #5
  Wire.onReceive(receiveEvent); // register event
  Wire.onRequest(requestEvent);
  Serial.begin(9600);           // start serial for output
}

void loop()
{
  delay(100);
}

void requestEvent()
{
  //Serial.print("Someone requested something. I'll send ");
   
  char t[8];
  myFloat = myFloat + .01;
  
  dtostrf(myFloat,5,2,t);
  Wire.write(t);
  Serial.println(t);

  dtostrf(myFloat*2,5,2,t);
  Wire.write(t);
  Serial.println(t);
  
  dtostrf(myFloat*3,5,2,t);
  Wire.write(t);
  Serial.println(t);
  
  dtostrf(myFloat*4,5,2,t);
  Wire.write(t);
  Serial.println(t);
  
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany)
{
  
  float p = 0;
  int c = 0;
  /*
  c = Wire.read();
  
  c = Wire.read();
  Serial.print(c);
  Serial.print("  ");  
  p = p + float(10.0*c);
  
  c = Wire.read();
  Serial.print(c);
  Serial.print("  "); 
  p = p + float(c);
  
  c = Wire.read();
  Serial.print(c);
  Serial.print("  ");
  p = p + float(.1*c);
  
  c = Wire.read();
  Serial.print(c);
  Serial.print("  ");
  p = p + float(.01*c);
  
  Serial.println(p);
  */
  while(Wire.available()) // loop through all
  {
    int c = Wire.read(); // receive byte as an int
    //Serial.print(c);         // print the character
  }
  
  //float pressure = dataString.toFloat();
  //int x = Wire.read();    // receive byte as an integer
  
  //Serial.println(pressure);         // print the integer
}

float two_bytes_to_float(unsigned char * twobytes)
{
  uint16_t myint;
  memcpy(&myint, twobytes, 2);
  return float(100.0*myint/65536.0);
}

void float_to_two_bytes(float myfloat, unsigned char * twobytes)
{
  uint16_t myint = myfloat*65536.0/100.0;
  memcpy(twobytes, &myint, 2);
}
