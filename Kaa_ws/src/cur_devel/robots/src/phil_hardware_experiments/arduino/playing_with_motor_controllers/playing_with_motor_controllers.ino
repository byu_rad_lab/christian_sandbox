void run_motor(int speed, bool forward)
{
  if(forward)
  {
    digitalWrite(2,LOW);
    analogWrite(3,255-speed);
  }
  else
  {
    digitalWrite(2,HIGH);
    analogWrite(3,speed);
  }
}

void setup() {
  // put your setup code here, to run once:
  pinMode(9,OUTPUT);
  pinMode(10,OUTPUT);
  pinMode(11,OUTPUT);

  pinMode(2,OUTPUT);
  pinMode(3,OUTPUT);
}

int speed = 0;
int ms = 1000;
void loop() 
{
  run_motor(speed,0);
  delay(ms);

  run_motor(speed,1);
  delay(ms);
}
