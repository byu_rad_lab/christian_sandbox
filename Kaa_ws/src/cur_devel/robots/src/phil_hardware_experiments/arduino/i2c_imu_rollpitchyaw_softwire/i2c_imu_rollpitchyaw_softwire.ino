#include <SoftwareWire.h>
SoftwareWire SoftWire(A3,A2);

#include <Wire.h>
#include <Adafruit_Sensor.h> //No use of Wire.h
#include <Adafruit_LSM303_U_softwire.h>
#include <Adafruit_L3GD20_U_softwire.h>
#include <Adafruit_10DOF_softwire.h>

/* Assign a unique ID to the sensors */
Adafruit_10DOF                dof   = Adafruit_10DOF();
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(30301);
Adafruit_LSM303_Mag_Unified   mag   = Adafruit_LSM303_Mag_Unified(30302);

// Phil found these by experiments using the 10 DOF Adafruit sensor
// They should be specific to each sensor really
int xmax = 1243;
int xmin = 126;
int ymax = 473;
int ymin = -696;
int zmax = 57;
int zmin = -965;

// To find the actual mins and maxes start with something like this
// and rotate the sensor around a bunch
//int xmax = -1000;
//int xmin = 1000;
//int ymax = -1000;
//int ymin = 1000;
//int zmax = -1000;
//int zmin = 1000;

int i2c_address = 12;

sensors_vec_t   orientation;

float rpy[3];

void initSensors()
{
  accel.begin();
  mag.begin();
  mag.enableAutoRange(1);
}

void setup(void)
{
  //Serial.begin(115200);
  Wire.begin(i2c_address);
  Wire.onReceive(receiveEvent);
  Wire.onRequest(requestEvent);

  /* Initialise the sensors */
  initSensors();
}


void loop(void)
{
  sensors_event_t accel_event;
  sensors_event_t mag_event;
  
  /* Calculate pitch and roll from the raw accelerometer data */
  accel.getEvent(&accel_event);
  dof.accelGetOrientation(&accel_event, &orientation);

  /* Calculate the yaw using the magnetometer */
  mag.getEvent(&mag_event);

  // Start Phil's custom magnetometer calibration---------------------
  if(mag.raw.x>xmax){xmax=mag.raw.x;}
  if(mag.raw.x<xmin){xmin=mag.raw.x;}
  if(mag.raw.y>ymax){ymax=mag.raw.y;}
  if(mag.raw.y<ymin){ymin=mag.raw.y;}
  if(mag.raw.z>zmax){zmax=mag.raw.z;}
  if(mag.raw.z<zmin){zmin=mag.raw.z;}

  float xrange = float(xmax)-float(xmin);
  float yrange = float(ymax)-float(ymin);
  float zrange = float(zmax)-float(zmin);

  mag_event.magnetic.x = float(mag.raw.x-(xmax+xmin)/2)/xrange*100.0;
  mag_event.magnetic.y = float(mag.raw.y-(ymax+ymin)/2)/yrange*100.0;
  mag_event.magnetic.z = float(mag.raw.z-(zmax+zmin)/2)/zrange*100.0;
  // End Phil's custom magnetometer calibration-----------------------
  
  dof.magGetOrientation(SENSOR_AXIS_Z, &mag_event, &orientation);

  rpy[0] = orientation.roll;
  rpy[1] = orientation.pitch;
  rpy[2] = orientation.heading;
//  for(int i=0; i<3; i++)
//  {
//    Serial.print(rpy[i]);
//    Serial.print("    ");
//  }
//  Serial.println("");
}

void receiveEvent(int howMany)
{
}

void requestEvent()
{
  char rpychar[4*3]; //4 byte float, 3 floats
  memcpy(&rpychar[0], &rpy[0], sizeof(rpy));
  Wire.write(rpychar,sizeof(rpychar));
}
