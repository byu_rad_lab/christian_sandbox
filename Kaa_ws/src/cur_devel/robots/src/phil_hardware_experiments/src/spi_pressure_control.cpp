#include<sstream>
#include<ros/ros.h>
#include<std_msgs/Float32MultiArray.h>
#include "SPIDevice.h"

float two_bytes_to_float(unsigned char * twobytes)
{
  uint16_t myint;
  memcpy(&myint, twobytes, 2);
  float myfloat = (float)(100.0*myint/65536.0);
  return myfloat;
}

void float_to_two_bytes(float myfloat, unsigned char * twobytes)
{
  uint16_t myint = myfloat*65536.0/100.0;
  memcpy(twobytes, &myint, 2);
}




int main(int argc, char **argv)
{

  ros::init(argc, argv, "PressureController");
  ros::NodeHandle n;
  ros::Publisher p_pub = n.advertise<std_msgs::Float32MultiArray>("pressures", 1000);
  
  // This assumes that wires are connected to pins 17,18,21,22 of the BB and 10,11,12,13 of the arduino
  int spi_bus = 1;
  int spi_addr = 0;
  SPIDevice device(spi_bus,spi_addr);

  float p[4] = {0,0,0,0};
  float pfilt[4];
  float pfilt2[4];
  float pcmd[4] = {12.34, 56.78, 20.78, 46.82};

  unsigned char pchar[4*2];    
  unsigned char pcmdchar[4*2];

  while(ros::ok())
    {

      for(int i=0; i<4; i++)
	{
	  float_to_two_bytes(pcmd[i],&pcmdchar[i*2]);
	}

      device.transfer(pcmdchar,pchar,4*2);



      for(int i=0; i<4; i++)
	{
	  p[i] = two_bytes_to_float(&pchar[i*2]);
	}

      for(int i=0; i<4; i++)
	{
	  pfilt[i] = p[i];
	  // pfilt2[i] = pfilt[i];
	  // pfilt[i] = .8*pfilt2[i] + 0.1*pfilt[i] + 0.1*p[i];
	}

      std_msgs::Float32MultiArray msg;
      msg.data = {pfilt[0], pfilt[1], pfilt[2], pfilt[3]};
      p_pub.publish(msg);
      ros::spinOnce();

      // std::cout<<"p0: "<<p0<<"    p1: "<<p1<<"    p2: "<<p2<<"    p3: "<<p3<<std::endl;
      // std::cout<<"p0_filt: "<<p0_filt<<"    p1_filt: "<<p1_filt<<"    p2_filt: "<<p2_filt<<"    p3_filt: "<<p3_filt<<std::endl;      
      
    }
  return 0;  
}
