from smbus import SMBus
import rospy
from sensor_msgs.msg import FluidPressure
import time

bus = SMBus(2)
arduino_addr = 5
pcmd0 = 0
pcmd1 = 0
pcmd2 = 0
pcmd3 = 0
p0 = 0
p1 = 0
p2 = 0
p3 = 0

def pressure_to_bytes(pressure):
    pstr = str(round(pressure,2))
    if(pressure<=9.999):
        pstr = '0'+pstr
    while(len(pstr)<5):
        pstr = pstr+'0'

    p = [int(pstr[0]),int(pstr[1]),int(pstr[3]),int(pstr[4])]

    return p[0],p[1],p[2],p[3]

def bytes_to_pressure(bytes):
    a = ''
    for i in range(0,len(bytes)):
        a = a + chr(bytes[i])

    return float(a)

rospy.init_node('PressureControllerNode')

pcmd_pub = rospy.Publisher('pressure_command',FluidPressure,queue_size=10)
p_pub = rospy.Publisher('pressure',FluidPressure,queue_size=10)


start = time.time()
up = 1

while not rospy.is_shutdown():

    if(time.time()-start >= 1):
        start = time.time()
        pcmd0 = pcmd0 + 10*up
        pcmd1=  pcmd1 + 5*up
        pcmd2=  pcmd2 + 2.5*up
        pcmd3=  pcmd3 + 1.0*up        

    if(pcmd0>50):
        pcmd0 = 25
        up = -1
        
    if(pcmd0<0):
        pcmd0 = 25
        up = 1



    pcmd_bytes = []
    pcmd_bytes.append(pressure_to_bytes(pcmd0))    
    pcmd_bytes.append(pressure_to_bytes(pcmd1))
    pcmd_bytes.append(pressure_to_bytes(pcmd2))
    pcmd_bytes.append(pressure_to_bytes(pcmd3))

    # Flatten the list of lists to a list
    pcmd_bytes = [byte for sublist in pcmd_bytes for byte in sublist]

    tic = time.time()    

    bus.write_i2c_block_data(arduino_addr,0,pcmd_bytes)

    toc = time.time()
    print("Writing time: ",toc-tic)
    tic = time.time()

    p_bytes = bus.read_i2c_block_data(arduino_addr,0,16)

    toc = time.time()
    print("Reading time: ",toc-tic)

    p0 = bytes_to_pressure(p_bytes[0:4])
    p1 = bytes_to_pressure(p_bytes[4:8])
    p2 = bytes_to_pressure(p_bytes[8:12])
    p3 = bytes_to_pressure(p_bytes[12:16])

    
    tic = time.time()    

    msg1 = FluidPressure()
    msg1.fluid_pressure = p0
    msg1.header.stamp = rospy.Time.now()
    p_pub.publish(msg1)

    msg2 = FluidPressure()
    msg2.fluid_pressure = pcmd0
    msg2.header.stamp = rospy.Time.now()    
    pcmd_pub.publish(msg2)

    toc = time.time()
    print("ROS time: ",toc-tic)


    print("\npcmd0: ",pcmd0,"    pcmd1: ",pcmd1,"    pcmd2: ",pcmd2,"    pcmd3: ",pcmd3)
    print("p0: ",p0,"    p1: ",p1,"    p2: ",p2,"    p3: ",p3)    
    
