#!/usr/bin/env python

import sys
import os
import rospy
import numpy as np
import math
from threading import RLock, Timer
import time
from copy import deepcopy

from byu_robot.msg import states as states_msg
from tools.iir_filter import iir_filter
from pdb import set_trace as pause

class thisFilter():

    def __init__(self, num_jangles=4):

        self.lock = RLock()

        self.rate = 300

        self.time0 = None
        self.time = None
        self.timePrev = None

        self.q = []
        self.qFilt = [None]*num_jangles
        # self.qFiltPrev = [0, 0, 0, 0]
        self.qFiltPrev = [0]*num_jangles
        self.qdotFilt = [None]*num_jangles

        # 10th Order, Fc = 10 - more delay
        # a = np.array([1, -8.66133120135125,   33.8379111594402,    -78.5155814397811,   119.815727607322,    -125.635905892562,   91.6674737604165,    -45.9506609307245,   15.1439586368785,    -2.96290103992492,   0.261309426400920])
        # b = np.array([8.40968453539440e-11,  8.40968453539440e-10,    3.78435804092748e-09,    1.00916214424733e-08,    1.76603375243282e-08,    2.11924050291939e-08,    1.76603375243282e-08,    1.00916214424733e-08,    3.78435804092748e-09,    8.40968453539440e-10,    8.40968453539440e-11])

        # # 10th Order, Fc = 20
        # a = np.array([1, -7.32360579351311,   24.4140405748189,    -48.7287075346528,   64.4268039455009,    -58.9142293060521,   37.7097153381243,   -16.6734203083454,   4.87130972354328,   -0.848817218704528,  0.0669604624846912])
        # b = np.array([4.87140665512032e-08,    4.87140665512032e-07,    2.19213299480414e-06,    5.84568798614438e-06,    1.02299539757527e-05,    1.22759447709032e-05,    1.02299539757527e-05,    5.84568798614438e-06,    2.19213299480414e-06,    4.87140665512032e-07,    4.87140665512032e-08])

        # # 10th Order, Fc = 30 - less delay
        a = np.array([1, -5.98758962981667,   16.6721933230027,    -28.2587879002005,   32.1597564876946,   -25.6017495970534,   14.4056874262078,    -5.64707434413248,   1.47372793697391,    -0.230919345862029,  0.0164796305471309])
        b = np.array([1.68358140723295e-06,  1.68358140723295e-05,    7.57611633254827e-05,    0.000202029768867954,    0.000353552095518919,    0.000424262514622703,    0.000353552095518919,    0.000202029768867954,    7.57611633254827e-05,    1.68358140723295e-05,    1.68358140723295e-06])

        self.filter = [None]*num_jangles
        
        for i in range(num_jangles):
            self.filter[i] = iir_filter(a, b)

        rospy.Subscriber("/states", states_msg, self.states_callback, queue_size = 1, tcp_nodelay = True)

        self.sfPub = rospy.Publisher('/statesFiltered', states_msg, queue_size = 1, tcp_nodelay = True)

        print("\n\n Filtering States! \n\n")

    def states_callback(self, data):
        self.lock.acquire()
        try:
            self.q_ = list(data.q)

        finally:
            self.lock.release()

    def get_data(self):
        self.lock.acquire()
        try:
            self.q = self.q_

            t = time.time()
            
            if self.time0 == None:
                self.time0 = t
            self.timePrev = self.time
            self.time = t - self.time0

        finally:
            self.lock.release()

    def get_qdot(self):
        try:
            dt = self.time - self.timePrev
            self.qdotFilt = [(A - B) / dt for A, B, in zip(self.qFilt, self.qFiltPrev)]
        except:
            dt = 1.0/self.rate
            self.qdotFilt = [(A - B) / dt for A, B, in zip(self.qFilt, self.qFiltPrev)]

        self.qFiltPrev = deepcopy(self.qFilt)

    def filter_stuff(self):

        self.get_data()

        for i in range(len(self.q)):
            filt = self.filter[i]
            self.qFilt[i] = filt.get_filt_val(self.q[i])

        self.get_qdot()

        msg = states_msg()
        msg.q = self.qFilt
        msg.qdot = self.qdotFilt

        self.sfPub.publish(msg)

        print(np.array(self.qFilt) * 180/math.pi)


if __name__ == '__main__':
    myFilter = thisFilter()

    rospy.init_node('filter_jangles', anonymous=True)
    rate = rospy.Rate(myFilter.rate)

    while not rospy.is_shutdown():
        myFilter.filter_stuff()
        rate.sleep()
