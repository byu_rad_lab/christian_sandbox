#include "JointAngleFinder/JointAngleFinder.h"

using namespace boost::python;

JointAngleFinder::JointAngleFinder()
{
};

//std::deque<double> JointAngleFinder::joint_angle_calc( std::deque<Eigen::Quaterniond> quats, std::vector<std::vector<double> > dh, std::vector<int> frame, std::vector<double> q_prev)
boost::python::list JointAngleFinder::joint_angle_calc( boost::python::list quat, boost::python::list dh_py, boost::python::list frames, boost::python::list q_prev_py, boost::python::list rot_baseDH_2_sensor_py)
{

	//cast the python list arguments into C++ types that will be used in the function
	std::deque<Eigen::Quaterniond> quats(boost::python::len(quat));
	std::vector<std::vector<double> > dh(boost::python::len(dh_py), std::vector<double> (4));
	std::vector<int> frame;
	std::vector<double> q_prev;
    Eigen::Matrix3d rot_baseDH_2_sensor;
	
	for(int i=0; i< boost::python::len(quat); i++)
	{		
		Eigen::Quaterniond q(boost::python::extract<double>(quat[i][3]), boost::python::extract<double>(quat[i][0]), boost::python::extract<double>(quat[i][1]), boost::python::extract<double>(quat[i][2]));
		quats[i] = q;
	}

	for(int i=0; i< boost::python::len(dh_py); i++)
	{
		for(int j=0; j < boost::python::len(dh_py[i]); j++)
		{
			dh[i][j] = boost::python::extract<double>(dh_py[i][j]);
		}
	}
	

	for(int i=0; i< boost::python::len(frames); i++)
	{
		frame.push_back(boost::python::extract<int>(frames[i]));
	}	

	for(int i=0; i< boost::python::len(q_prev_py); i++)
	{
		q_prev.push_back(boost::python::extract<double>(q_prev_py[i]));
	}

    rot_baseDH_2_sensor << boost::python::extract<double>(rot_baseDH_2_sensor_py[0][0]), boost::python::extract<double>(rot_baseDH_2_sensor_py[0][1]), boost::python::extract<double>(rot_baseDH_2_sensor_py[0][2]),
                            boost::python::extract<double>(rot_baseDH_2_sensor_py[1][0]), boost::python::extract<double>(rot_baseDH_2_sensor_py[1][1]), boost::python::extract<double>(rot_baseDH_2_sensor_py[1][2]),
                            boost::python::extract<double>(rot_baseDH_2_sensor_py[2][0]), boost::python::extract<double>(rot_baseDH_2_sensor_py[2][1]), boost::python::extract<double>(rot_baseDH_2_sensor_py[2][2]);


	std::deque<double> q; //create angle deque that will return from function


    //create nominal rotations

	int quat_size; 

	quat_size = quats.size(); //find how many links have orientation data for

	std::vector<Eigen::Matrix3d> nominal_rots(quat_size); //make a vector of matrices to store nominal rotations in
	std::vector<Eigen::Matrix3d> data_rots(quat_size); //make a vector of matrices to store rotation matrices from quaternion data
	std::vector<Eigen::Matrix3d> dh_rots(quat_size); //make a vector of matrices to store rotation matrices to a given dh frame
	std::vector<Eigen::Matrix3d> dh_2_dh_rots(quat_size); //make a vector of matrices to store rotation matrices to a between dh frames
	
	for(int i=0; i<quat_size; i++)
	{
		if (i==0) //this means that it is the reference (world) frame and so it will not be rotated with regard to itself
		{
			nominal_rots[i] = rot_baseDH_2_sensor;
		}
		else
		{
			int frames_between;
			frames_between = frame[i] - frame[i-1]; //find out how many DH frames are between the previous orientation data frame and the current one

			for(int j=0; j<frames_between; j++) //add rotations
			{
			
				if(j==0) //if it is first time then need to use previous nominal rotation to create new
				{
					nominal_rots[i] = nominal_rots[i-1]*dh_2_rot(dh[frame[i-1]+j][1],dh[frame[i-1]+j][3]); //post multiply by the correct dh parameters
				}
				else  //if there were multiple dh parameters in between need to use the last value for the nominal_rot
				{
					nominal_rots[i] = nominal_rots[i]*dh_2_rot(dh[frame[i-1]+j][1],dh[frame[i-1]+j][3]); //post multiply by the correct dh parameters
				}
			}
		}	

		//find what rotation from quaternion is and multiply by nominal rotation
		quats[i].normalize();
		data_rots[i] = quats[i].toRotationMatrix();
		dh_rots[i] = data_rots[i]*nominal_rots[i];

		//find rotation between frames by taking transpose of previous frame's rotation by the current frame's rotation
		if (i!=0)
		{
			dh_2_dh_rots[i] = dh_rots[i-1].transpose()*dh_rots[i]; //find out the current rotation matrix between the 2 dh frames

			int num_joints = frame[i] - frame[i-1]; //find out how many joints there are in between the 2 dh frames

			switch(num_joints) //switch statement to calculate joint angles depending on how many joints are between frames
			{
				case 1:
					q.push_back(range_check(atan2(dh_2_dh_rots[i](1,0),dh_2_dh_rots[i](0,0)) - dh[frame[i]-1][3])); //finding angle if only one joint between
					break;

				case 2:
                    {
                    // this is the analytical solution that did not always give the same answers
					double q_1st_num;
					double q_1st_den;
					double q_1st_ang; 
					double q_2nd_ang; 
					//q_2nd_ang = atan2((dh_2_dh_rots[i](2,0)*cos(dh[frame[i]-1][1])*sin(dh[frame[i]-2][1])),(sin(dh[frame[i]-2][1])*(dh_2_dh_rots[i](2,1)-cos(dh[frame[i]-2][1])*sin(dh[frame[i]-1][1]))))-dh[frame[i]-1][3];
                    q_2nd_ang = atan2((dh_2_dh_rots[i](2,0)*sin(dh[frame[i]-1][1])),(cos(dh[frame[i]-2][1])*cos(dh[frame[i]-1][1])-dh_2_dh_rots[i](2,2)))-dh[frame[i]-1][3];

					q_1st_num = dh_2_dh_rots[i](0,2)*sin(dh[frame[i]-2][1])*cos(dh[frame[i]-1][1])+dh_2_dh_rots[i](1,2)*sin(dh[frame[i]-1][1])*sin(q_2nd_ang+dh[frame[i]-1][1])+dh_2_dh_rots[i](0,2)*cos(dh[frame[i]-2][1])*sin(dh[frame[i]-1][1])*cos(q_2nd_ang+dh[frame[i]-1][3]); 

					q_1st_den = -dh_2_dh_rots[i](1,2)*sin(dh[frame[i]-2][1])*cos(dh[frame[i]-1][1])+dh_2_dh_rots[i](0,2)*sin(dh[frame[i]-1][1])*sin(q_2nd_ang+dh[frame[i]-1][3])-dh_2_dh_rots[i](1,2)*cos(dh[frame[i]-2][1])*sin(dh[frame[i]-1][1])*cos(q_2nd_ang+dh[frame[i]-1][3]);

					q_1st_ang = atan2(q_1st_num,q_1st_den)-dh[frame[i]-2][3];
	
					//check to make sure the joint angles are between -PI and PI
					q_1st_ang = range_check(q_1st_ang);
					q_2nd_ang = range_check(q_2nd_ang);
					//append the joint angles to the q deque
					q.push_back(q_1st_ang);
					q.push_back(q_2nd_ang);
                     //This is the trac-ik solution
                        /*
                        KDL::JntArray q_trac = KDL::JntArray(2); //declaring array for current joint angles to be used in TRAC-IK
					    KDL::JntArray q_init = KDL::JntArray(2); //declaring array for guess for these joint angles for the TRAC-IK
					    KDL::JntArray ll = KDL::JntArray(2); //declaring array for lower limit for these joint angles in ik solution
					    KDL::JntArray ul = KDL::JntArray(2); //declaring array for upper limit for these joint angles in ik solution
					    KDL::Vector Vel_vel = KDL::Vector(1.0,1.0,1.0); //determines bounds for the position (set big so it doesn't worry about position but just orientation since we are just solving for joint angles)
					    KDL::Vector Vel_rot = KDL::Vector(1.0e-3,1.0e-3,1.0e-3); //determines bounds for the rotation
					    KDL::Twist bounds = KDL::Twist(Vel_vel, Vel_rot); //combines the two bounds together

					    //declare the robot chain for TRAC-IK
					    KDL::Chain chain;

                        //std::cout << dh[frame[i]-2][0] << " " << dh[frame[i]-2][1]<< " " << dh[frame[i]-2][2]<< " " << dh[frame[i]-2][3] << std::endl;
                        //std::cout << dh[frame[i]-1][0] << " " << dh[frame[i]-1][1]<< " " << dh[frame[i]-1][2]<< " " << dh[frame[i]-1][3] << std::endl;

					    chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(dh[frame[i]-2][0],dh[frame[i]-2][1],dh[frame[i]-2][2],dh[frame[i]-2][3])));
					    chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(dh[frame[i]-1][0],dh[frame[i]-1][1],dh[frame[i]-1][2],dh[frame[i]-1][3])));

					    KDL::Rotation rot_trac_ik;
					    rot_trac_ik = Matrix_to_Rot(dh_2_dh_rots[i]);

                        //print_matrix(dh_2_dh_rots[i]);

					    KDL::Frame desiredFrame(rot_trac_ik); //create a desired frame for the TRAC-IK

					    //define window of upper and lower limits for the TRAC-IK optimization.  This means it will only allow the angle to be withn 30 degrees of the last known angle.  
					    double eps = 360.0*M_PI/180.0;

					    for(int j=0; j<2; j++)
					    {
						    q_init(j) = q_prev[frame[i]-(2-j)];
						    ll(j) = q_prev[frame[i]-(2-j)] - eps;
						    ul(j) = q_prev[frame[i]-(2-j)] + eps;
					    }

					    //set up trac_ik solver. Inputs are (chain, lower limits, upper limits, time for solve, eps)  I think epsilon is accepted error.  I have it high for the first 3 (position because I don't care about position, but it didn't work until I added KDL::Vector Vel_vel(1.0,1.0,1.0) above as well).  The last three are low since they are the angles we are solving for.
			        	TRAC_IK::TRAC_IK tracik_solver(chain, ll, ul, 1.0e-2, (1.0e6, 1.0e6, 1.0e6, 1.0e-3, 1.0e-3, 1.0e-3));
					    //this is what you do to run the TRAC-IK optimization    	
					    int rc;
					    rc = tracik_solver.CartToJnt(q_init, desiredFrame, q_trac, bounds); //solve using trac_ik
					
					    for(int j=0; j<2; j++)
					    {
						    q.push_back(range_check(q_trac(j)));
					    }	*/

					}
					break;

				case 3:
                    {
					    KDL::JntArray q_trac = KDL::JntArray(3); //declaring array for current joint angles to be used in TRAC-IK
					    KDL::JntArray q_init = KDL::JntArray(3); //declaring array for guess for these joint angles for the TRAC-IK
					    KDL::JntArray ll = KDL::JntArray(3); //declaring array for lower limit for these joint angles in ik solution
					    KDL::JntArray ul = KDL::JntArray(3); //declaring array for upper limit for these joint angles in ik solution*/
					    KDL::Vector Vel_vel = KDL::Vector(1.0,1.0,1.0); //determines bounds for the position (set big so it doesn't worry about position but just orientation since we are just solving for joint angles)
					    KDL::Vector Vel_rot = KDL::Vector(1.0e-6,1.0e-6,1.0e-6); //determines bounds for the rotation
					    KDL::Twist bounds = KDL::Twist(Vel_vel, Vel_rot); //combines the two bounds together

					    //declare the robot chain for TRAC-IK
					    KDL::Chain chain;

                        //Add segments to the chain declared above to create robot for TRAC-IK
	                    //define robot according to DH parameters
	                    //static Frame 	DH (double a, double alpha, double d, double theta).  RotZ means it is rotating about the z axis (theta) for the joint angle (RotX would be for the x-axis if the angle was in alpha)
					    chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(dh[frame[i]-3][0],dh[frame[i]-3][1],dh[frame[i]-3][2],dh[frame[i]-3][3])));
					    chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(dh[frame[i]-2][0],dh[frame[i]-2][1],dh[frame[i]-2][2],dh[frame[i]-2][3])));
					    chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(dh[frame[i]-1][0],dh[frame[i]-1][1],dh[frame[i]-1][2],dh[frame[i]-1][3])));

					    KDL::Rotation rot_trac_ik;
					    rot_trac_ik = Matrix_to_Rot(dh_2_dh_rots[i]);

					    KDL::Frame desiredFrame(rot_trac_ik); //create a desired frame for the TRAC-IK

					    //define window of upper and lower limits for the TRAC-IK optimization.  This means it will only allow the angle to be within 30 degrees of the last known angle.  
					    double eps_h = 30.0*M_PI/180.0;
                        double eps_l = 30.0*M_PI/180.0;

					    for(int j=0; j<3; j++)
					    {
						    q_init(j) = q_prev[frame[i]-(3-j)];
                            //if statement to make the bounds smaller when closer to zero so it won't flip sides
                            if (abs(q_init(j)) >= .3)
                            {
                                ll(j) = q_prev[frame[i]-(3-j)] - eps_h;
						        ul(j) = q_prev[frame[i]-(3-j)] + eps_h;    
                            }
                            else if (abs(q_init(j)) <= eps_l)
                            {
                                ll(j) = q_prev[frame[i]-(3-j)] - eps_l;
						        ul(j) = q_prev[frame[i]-(3-j)] + eps_l;  
                            }
                            else
                            {
                                ll(j) = q_prev[frame[i]-(3-j)] - ((eps_h-eps_l)/(.3-eps_l)*q_init(j) + (1-(eps_h-eps_l)/(.3-eps_l))*eps_l);
						        ul(j) = q_prev[frame[i]-(3-j)] + ((eps_h-eps_l)/(.3-eps_l)*q_init(j) + (1-(eps_h-eps_l)/(.3-eps_l))*eps_l);
                            }
						    
					    }

					    //set up trac_ik solver. Inputs are (chain, lower limits, upper limits, time for solve, eps)  I think epsilon is accepted error.  I have it high for the first 3 (position because I don't care about position, but it didn't work until I added KDL::Vector Vel_vel(1.0,1.0,1.0) above as well).  The last three are low since they are the angles we are solving for.
			        	TRAC_IK::TRAC_IK tracik_solver_left(chain, ll, ul, 5.0e-3, (1.0e6, 1.0e6, 1.0e6, 1.0e-6, 1.0e-6, 1.0e-6));
					    //this is what you do to run the TRAC-IK optimization    	
					    int rc;
					    rc = tracik_solver_left.CartToJnt(q_init, desiredFrame, q_trac, bounds); //solve using trac_ik
					
					    for(int j=0; j<3; j++)
					    {
						    q.push_back(range_check(q_trac(j)));
					    }	
					}
					break;
			}
		}
		

	}

	//q.push_back(5);
	//q.push_back(7);

	boost::python::list q_py;

	for (int i=0; i<q.size(); i++)
	{
		q_py.append(q[i]);
	}
	
	return q_py;
}

//used to make a rotation matrix from one DH frame to the next from the DH parameters
Eigen::Matrix3d JointAngleFinder::dh_2_rot(double alpha, double theta)
{
	Eigen::Matrix3d rot;
	rot << cos(theta), -sin(theta)*cos(alpha), sin(theta)*sin(alpha),
	       sin(theta), cos(theta)*cos(alpha), -cos(theta)*sin(alpha),
               0, sin(alpha), cos(alpha);

	return(rot);
}

//used to change a matrix in the Eigen::Matrix3d construct to a KDL::Rotation construct so can use TRAC-IK
KDL::Rotation JointAngleFinder::Matrix_to_Rot(Eigen::Matrix3d Rot)
{
	//declare variables for indices
	double a, b, c, d, e, f, g, h, i;
	
	//assign them from the matrix that was inputed
	a = Rot(0,0);
	b = Rot(0,1);
	c = Rot(0,2);
	d = Rot(1,0);
	e = Rot(1,1);
	f = Rot(1,2);
	g = Rot(2,0);
	h = Rot(2,1);
	i = Rot(2,2);
	
	//create a matrix in the Rotation construct from these indices
	KDL::Rotation R(a, b, c, d, e, f, g, h, i);

	return(R);
}

void JointAngleFinder::print_matrix(Eigen::Matrix3d r)
{
	std::cout << r(0,0) << "\t" << r(0,1) << "\t" << r(0,2) << std::endl;
	std::cout << r(1,0) << "\t" << r(1,1) << "\t" << r(1,2) << std::endl;
	std::cout << r(2,0) << "\t" << r(2,1) << "\t" << r(2,2) << std::endl;

	std::cout << std::endl;
}

double JointAngleFinder::range_check(double q)
{
	if(q<-M_PI)
	{
		while(q<-M_PI)
		{
			q = q + 2*M_PI;
		}
	}
	else if(q>M_PI)
	{
		while(q>M_PI)
		{
			q = q - 2*M_PI;
		}
	}

	return q;
}

BOOST_PYTHON_MODULE(JointAngleFinder)
{
    class_<JointAngleFinder>("JointAngleFinder")
      .def("joint_angle_calc", &JointAngleFinder::joint_angle_calc)
      .def("range_check", &JointAngleFinder::range_check)
    ;
    //def("sum",&example::sum);
}
