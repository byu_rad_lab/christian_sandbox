#!/usr/bin/env python
import roslib
import rospy
import numpy as np
import math
from collections import deque
import scipy.io as sio
import tf2_ros
import tf
import geometry_msgs.msg
from geometry_msgs.msg import Quaternion
import JointAngleFinder
from sensor_msgs.msg import JointState
from state_estimator.msg import states

class BaxterTestVive():
    def __init__(self, tfBuffer):

        self.tfBuffer = tfBuffer
        pi = np.pi
        self.joint_angle_finder = JointAngleFinder.JointAngleFinder()


    ##==============================  BAXTER  ==============================##

        #         # BAXTER RIGHT ARM DH PARAMETERS
        #         # a         alpha       d           theta
        self.dh = [
                (0.069,     -pi/2,      0.27035,    -pi/4),
                (0.0,       pi/2,       0.0,        pi/2),
                (0.069,     -pi/2,      0.36435,    0)]
        #         (0.0,      pi/2,       0.0,        0),
        #         (0.01,     -pi/2,      0.37429,    0),
        #         (0.0,      pi/2,       0.0,        0)]
        #         # (0.0,      0,          0.229525,   0)]
        #
        arm = 'Baxter_Right_Arm'
    ##======================================================================##

        # Establish the frame for the base of the robot
        self.base_frame = [0,0,0,1]

        self.rot_baseDH_2_sensor = [ [-1,0,0],
                                [0,-1,0],
                                [0,0,1]]

        # what DH frames are the inputs coming from?
        self.frames = [0,3]

        # Establish what joint angles the robot starts in
        q = np.array([0,0,0])
        self.q_prev = deque()
        self.q_prev.append(q)
        self.q_prev.append(q)
        self.q_prev.append(q)
        self.q_prev.append(q)
        self.q_prev.append(q)

        self.q_publisher = rospy.Publisher('qs', states, queue_size=1)
        print("COWMAN")

        self.q_enc_save = []
        self.q_calc_save = []
        self.error_save = []
        self.time_save = []

        # print("\n******** INSTRUCTIONS FOR ZEROING THE ORIENTATION ********")
        # rospy.logwarn("Once the cameras are on the robot, press ENTER to establish the zero configuration\n")
        # raw_input()
        print("Kaladin")
        transform1 = self.tfBuffer.lookup_transform('neo_tracker3', 'neo_tracker3', rospy.Time(0)) #rospy.Time.now(),rospy.Duration(1.0))
        transform2 = self.tfBuffer.lookup_transform('neo_tracker3', 'neo_tracker2', rospy.Time(0)) #rospy.Time.now(),rospy.Duration(1.0))
        print("MONKEYS UNCLE")
        rot1 = transform1.transform.rotation
        rot2 = transform2.transform.rotation
        rospy.loginfo("Joint angles initialized to zero.")

        #How are the cameras oriented at 0 with respect to the base frame?
        self.offset1 = [-rot1.x, -rot1.y, -rot1.z, rot1.w]
        self.offset2 = [-rot2.x, -rot2.y, -rot2.z, rot2.w]

        print("POTATOEEEEEEEEEEEEEEEEEEEEEEEEEEEE!!!!!!")

        rospy.Subscriber("/robot/joint_states",JointState, self.joint_states_callback)

    def joint_states_callback(self,msg):
        # print msg.header.stamp
        string_to_print = [msg.position[11], msg.position[12], msg.position[9]]
        print(string_to_print)
        self.q_enc_save.append([msg.position[11], msg.position[12], msg.position[9]])
        self.time_save.append([msg.header.stamp.to_sec()])

        try:
            # Pull the quaternions from vive_trackers into rots
            # rospy.loginfo("Finding transforms.")
            transform1 = self.tfBuffer.lookup_transform('neo_tracker3', 'neo_tracker3', rospy.Time(0)) #ospy.Time.now(),rospy.Duration(1.0))
            transform2 = self.tfBuffer.lookup_transform('neo_tracker3', 'neo_tracker2', rospy.Time(0)) #ospy.Time.now(),rospy.Duration(1.0))
            rot1 = transform1.transform.rotation
            rot2 = transform2.transform.rotation
            # rospy.loginfo("Transforms found.")
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
            print(e)

        # print "MONKEYYYYYYYYYYYYYYYYYYYYYYYYY"
        quat1 = tf.transformations.quaternion_multiply([rot1.x, rot1.y, rot1.z, rot1.w],self.offset1)
        quat2 = tf.transformations.quaternion_multiply([rot2.x, rot2.y, rot2.z, rot2.w],self.offset2)

        # identify the input quaternions to do the joint calculations
        quats = [quat1,quat2]

        # This is where the joint angle calculation is performed
        q = self.joint_angle_finder.joint_angle_calc(quats, self.dh, self.frames, self.q_prev[4].tolist(), self.rot_baseDH_2_sensor)

        error = [q[0] - msg.position[11], q[1] - msg.position[12], q[2] - msg.position[9]]
        print(error)

        self.q_calc_save.append(q)
        self.error_save.append(error)

        # Update the q_prev deque
        q = np.array(q) #This does no filtering
        self.q_prev.pop()
        self.q_prev.append(q)

        num=100.0

        qd = [(q[0]-self.q_prev[4][0])*num] #, (q[1]-self.q_prev[4][1])*num, (q[2]-self.q_prev[4][2])*num]
        msg = states()
        msg.q = q
        msg.qdot = qd
        # print "Publishing"
        self.q_publisher.publish(msg)

if __name__ == '__main__':

    rospy.init_node('baxter_test')

    print("STARTED NODE")

    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)

    rospy.sleep(0.1)

    baxter_test = BaxterTestVive(tfBuffer)

    rospy.spin()

    print("IT'S HOCKEY TIME!!")

    data = {'time':baxter_test.time_save, 'q_enc':baxter_test.q_enc_save, 'q_calc':baxter_test.q_calc_save, 'error':baxter_test.error_save}
    sio.savemat('./data',data)

    # num = 100

    # rate = rospy.Rate(num)


    #     rate.sleep()
