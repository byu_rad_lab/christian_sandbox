import numpy as np
import rospy
import tf2_ros
import tf
from geometry_msgs.msg import TransformStamped
from copy import deepcopy

#This class creates the ability to estimate the end effector in the base frame
class EndEffectorEstimation():
    
    def __init__(self, base_sensor, end_effector_sensor, base_sensor_in_base_frame=np.eye(4), end_effector_sensor_in_end_effector_frame=np.eye(4), publish=True, publish_topic='end_effector_estimate'):

        self.base_sensor = base_sensor
        self.end_effector_sensor = end_effector_sensor

        # Make subscribers to link orientation data on tf
        self.tfbuffer = tf2_ros.Buffer()
        self.tflistener = tf2_ros.TransformListener(self.tfbuffer)

        self.g_base_sensor = base_sensor_in_base_frame
        self.g_ee_sensor = end_effector_sensor_in_end_effector_frame

        # Make a publisher if desired
        if publish == True:
            self.est_pub = rospy.Publisher(publish_topic,TransformStamped)

    def estimate(self):

            try:
                transform = self.tfbuffer.lookup_transform(self.base_sensor,self.end_effector_sensor,rospy.Time()).transform

                # Transform into a homogeneous matrix
                my_quat = np.zeros([4,1])
                my_quat[0] = transform.rotation.x
                my_quat[1] = transform.rotation.y
                my_quat[2] = transform.rotation.z
                my_quat[3] = transform.rotation.w
                rot = tf.transformations.quaternion_matrix(my_quat.squeeze())

                my_trans = np.zeros([3,1])
                my_trans[0] = transform.translation.x
                my_trans[1] = transform.translation.y
                my_trans[2] = transform.translation.z

                trans = tf.transformations.translation_matrix(my_trans.squeeze())

                trans_mat = np.dot(trans,rot)

                g_base_ee = np.dot(np.dot(self.g_base_sensor, trans_mat),np.linalg.inv(self.g_ee_sensor))

                trans_out = tf.transformations.translation_from_matrix(g_base_ee)
                rot_out = tf.transformations.quaternion_from_matrix(g_base_ee)

                msg = TransformStamped()

                msg.header.stamp = rospy.Time()

                msg.transform.translation.x = trans_out[0]
                msg.transform.translation.y = trans_out[1]
                msg.transform.translation.z = trans_out[2]

                msg.transform.rotation.x = rot_out[0]
                msg.transform.rotation.y = rot_out[1]
                msg.transform.rotation.z = rot_out[2]
                msg.transform.rotation.w = rot_out[3]

                self.est_pub.publish(msg)

            except(tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
                print(e)


if __name__=='__main__':

    ee_est = EndEffectorEstimation('neo_tracker1', 'neo_tracker2')

    # Initialize a ROS node
    rospy.init_node('EndEffectorEstimator')

    rate = rospy.Rate(500)

    while not rospy.is_shutdown():

        ee_est.estimate()

        rate.sleep()

