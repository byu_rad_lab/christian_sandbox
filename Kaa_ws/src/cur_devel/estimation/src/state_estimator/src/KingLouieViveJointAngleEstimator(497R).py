#!/usr/bin/env python
import rospy
import tf
from tf2_msgs.msg import TFMessage
import math
from math import atan2
import numpy as np
import time
from tf.transformations import quaternion_matrix


class ViveSubscriber():
	def __init__(self, trackers):
		rospy.init_node('vive_tf_listener')
		# publisher = rospy.Publisher('')
		# rospy.Subscriber('/tf', TFMessage, self.ViveCallback)
		self.listener = tf.TransformListener()

		self.rate = rospy.Rate(10)

		self.R_S1_0_T = np.matrix([[0, -1, 0], [-1, 0, 0], [0, 0, -1]])
		self.R_S1_0_T.transpose()

		R_0_2_T = np.matrix([[1,  0, 0], [0,  0, 1],[0, -1, 0]]).transpose()

		R_2_4_T = R_0_2_T

		self.trackers = [trackers[0],trackers[1]]

		trackerNumber = 0

		# for i in range(0,len(self.trackers)):
		# 	for j in range(0,len(self.trackers[i])):
		# 		if j == 0 and i == 0:
		# 			trackerNumber = raw_input('Enter the number in rviz of the tracker that is on King Louie\'s chest: ')
		# 		elif (i == 0 and j == 1) or (i == 1 and j == 0):
		# 			trackerNumber = raw_input('Enter the number in rviz of the tracker that is on King Louie\'s upper arm (between the shoulder and wrist joints): ')
		# 		elif i == 1 and j == 1:
		# 			trackerNumber = raw_input('Enter the number in rviz of the tracker that is on King Louie\'s lower arm (between the wrist joints and the end effector): ')

		# 		self.trackers[i][j] += trackerNumber
		# 		print(self.trackers[i][j])
		                        
		                                        
		# raw_input('Hit Enter when system is ready to calibrate')                        

		self.start_flag = False

		while not self.start_flag:
			try:
				(trans, rot) = self.listener.lookupTransform(self.trackers[0][0], self.trackers[0][1], rospy.Time(0))
				(trans, rot) = self.listener.lookupTransform(self.trackers[1][0], self.trackers[1][1], rospy.Time(0))
			except tf.LookupException:
				continue
			except KeyboardInterrupt:
				raise
			self.start_flag = True

		               
		input('Hit Enter when system is ready to calibrate')
		exceptionOccurred = True
		while exceptionOccurred:
			try:
				(trans1, sensor0Q1_time0) = self.listener.lookupTransform(self.trackers[0][0], self.trackers[0][1], rospy.Time(0))
				(trans2, sensor1Q2_time0) = self.listener.lookupTransform(self.trackers[1][0], self.trackers[1][1], rospy.Time(0))
				self.R_2_S2_T = (R_0_2_T * self.R_S1_0_T * self.quaternion(sensor0Q1_time0)).transpose()
				self.R_4_S3_T = (R_2_4_T * self.R_2_S2_T.transpose() * self.quaternion(sensor1Q2_time0)).transpose()

				# print(self.R_2_S2_T)
				# print(self.sensor0R1_time0)
				# print(self.sensor1R2_time0)
				exceptionOccurred = False
			except tf.LookupException:
				print('lookup exception')
				continue
			except KeyboardInterrupt:
				raise


	            
	# def ViveCallback(self, msg):
		# print(msg.transforms[0].child_frame_id)
		# print(msg.transforms[0].transform.translation)
		# print(msg.transforms[0].transform.rotation)
		# y = 1

	def Run(self):
		input('Hit enter when ready to start taking data')
		while not rospy.is_shutdown():
		#for trans in self.trackers:
		#  	(trans, rot) = self.listener.lookupTransform(trans[0], trans[1], rospy.Time(0))
			x = 0
			for pair in self.trackers:
				try:
					(trans,q_rot) = self.listener.lookupTransform(pair[0], pair[1], rospy.Time(0))
					# (trans2, rot2) = self.listener.lookupTransform(self.trackers[1][0], self.trackers[1][1], rospy.Time(0))
				except (tf.LookupException):
					print("lookup exception occurred")
					continue
				except(tf.ConnectivityException):
					print("connectivity exception")
					continue
				except(tf.ExtrapolationException):
					print("extrapolation exception")
					continue
				except KeyboardInterrupt:
					raise

				if x == 0:
					rotS1_S2 = q_rot
				elif x == 1:
					rotS2_S3 = q_rot

				x += 1

			R_S1_S2 = self.quaternion(rotS1_S2)
			R_0_2 = self.R_S1_0_T * R_S1_S2 * self.R_2_S2_T
			print("Q1")
			print((atan2(-1 * R_0_2.item((0,2)), R_0_2.item((1,2)))))
			print("Q2")
			print((-1 * atan2(R_0_2.item((2,0)), -1 * R_0_2.item((2,1)))))

			R_S2_S3 = self.quaternion(rotS2_S3)
			R_2_4 = self.R_2_S2_T.transpose() * R_S2_S3 * self.R_4_S3_T
			print("Q3")
			print((atan2(-1 * R_2_4.item((0,2)), R_2_4.item((1,2)))))
			print("Q4")
			print((atan2(R_2_4.item((2,0)), -1 * R_2_4.item((2,1)))))

			self.rate.sleep()

	def quaternion(self, quat):
		fourByFour = quaternion_matrix(quat)
		threeByThree = fourByFour[0:3,0:3]
		# print(threeByThree)
		return threeByThree

if __name__ == '__main__':
	print("Hello")
	#	vs = ViveSubscriber(['/neo_tracker1'])
	trackers = [['/neo_tracker1', '/neo_tracker2'], ['/neo_tracker2', '/neo_tracker3']]
	vs = ViveSubscriber(trackers)
	vs.Run()
