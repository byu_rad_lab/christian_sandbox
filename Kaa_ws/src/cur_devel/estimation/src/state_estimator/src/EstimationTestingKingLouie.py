#!/usr/bin/env python
import rospy
import tf
import time
from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import Imu
from tf2_msgs.msg import TFMessage
from low_level_control.msg import joint_angles_msg as j_a_msg
from low_level_control.msg import joint_angles_msg_array as j_a_msg_array
from collections import deque
import math
import numpy as np
import scipy.io as sio
import JointAngleFinder
from threading import RLock
import os
import sys

np.set_printoptions(precision=2)

class KingLouieJointFinder():
    def __init__(self):

        rospy.init_node('KingLouieJointFinder')

        self.sensor = rospy.get_param(rospy.get_name() + '/sensor_type','IMU')
        self.appendage = rospy.get_param(rospy.get_name() + '/appendage','Right')
        print(rospy.get_name())
        self.appendage = self.appendage.title()
        print("Appendage:   " + self.appendage)
        print("Sensor:  " + self.sensor)
        #self.dh_file = rospy.get_param(rospy.get_name() + '/dh_parameters')

        self.qfilt_list = deque()
        self.q_list = deque()

        self.joint_angle_finder = JointAngleFinder.JointAngleFinder()
        self.rate = 250.0
        self.lock = RLock()

        self.q_prev = deque()
        self.q_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
        self.q_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
        self.q_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
        self.q_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
        self.q_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))

        self.qd_prev = deque()
        self.qd_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
        self.qd_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
        self.qd_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
        self.qd_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
        self.qd_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))

        self.qfilt_prev = deque()
        self.qfilt_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
        self.qfilt_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
        self.qfilt_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
        self.qfilt_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))

        self.qdfilt_prev = deque()
        self.qdfilt_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
        self.qdfilt_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
        self.qdfilt_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
        self.qdfilt_prev.append(np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))

        self.q = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.q_right = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.qd_right = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])


        #moving average filter variables for joints and
        self.num_pts = 10
        self.qhip = []
        self.q0 = []
        self.qp_shoulder = []
        self.q1 = []
        self.q3 = []
        self.qp_wrist = []
        self.q4 = []

        self.qdhip = []
        self.qd0 = []
        self.qdp_shoulder = []
        self.qd1 = []
        self.qd3 = []
        self.qdp_wrist = []
        self.qd4 = []

        # Parameters for Dirty Derivative
        self.tau = .05
        self.rate = 240

        # Parameters for IIR filter (4th order Fc = 10 Hz)
        self.a2 = -3.316807910624418
        self.a3 = 4.17424555007657
        self.a4 = -2.357402780562259
        self.a5 = 0.503375360741704
        self.b1 = 0.0002131387269750786
        self.b2 = 0.0008525549079003143
        self.b3 = 0.00127883236185047
        self.b4 = 0.0008525549079003143
        self.b5 = 0.0002131387269750786


        # Parameters for IIR filter (2nd order Fc = 10 Hz)
        '''
        self.a2 = 1.632993161855452
        self.a3 = 0.690598923241497
        self.a4 = 0.0
        self.a5 = 0.0
        self.b1 = 0.014401440346511
        self.b2 = 0.0288028806930224
        self.b3 = 0.014401440346511
        self.b4 = 0.0
        self.b5 = 0.0
        '''

        #initialize quaternions
        self.quat_abdomen_ = [0.0,0.0,0.0,1.0]
        self.quat_shoulder_ = [0.0,0.0,0.0,1.0]
        self.quat_link1_ = [0.0,0.0,0.0,1.0]
        self.quat_endeffecter_ = [0.0,0.0,0.0,1.0]
        self.quat_link_left_ = [0.0,0.0,0.0,1.0]
        self.quat_left_hand_ = [0.0,0.0,0.0,1.0]

        # Load Optimized DH parameters
        try:
            #bla
            data = sio.loadmat(os.path.expanduser('~') + '/git/byu/development/kl_ws/src/low_level_control/src/dh_params_' + self.sensor +'.mat')
            dh = np.matrix(data['DH'])
            dh_reshaped = dh.reshape([7,4])
            self.dh = dh_reshaped.tolist()
            print("Loaded Optimized DH parameters...")
            rospy.sleep(2)
        except:
            print("Found no saved DH parameters...")
            print("Loaded default DH parameters...")
            rospy.sleep(2)

            #a, alpha, d, theta
            self.dh = [[-.52,0,-0.1,-1.27],
                       [0,np.pi/2,0,1.27],
                       [0,-np.pi/2,.18,np.pi/2],
                       [-.305,0,0,np.pi/2],
                       [-.195,np.pi/2,0,np.pi/2],
                       [.095,-np.pi/2,0,np.pi/2],
                       [.095,0,0,-np.pi/2]]

        self.frames = [0,1,4,7]

        self.rot_baseDH_2_sensor = [[-1,0,0],
                                   [0,0,1],
                                   [0,1,0]]


        rospy.sleep(3)

        if self.sensor == "MOCAP":
       	    #subscribe to the markerset data being published by cortex
            rospy.Subscriber("/abdomen/abdomen",PoseStamped, self.MC_callback,4, tcp_nodelay=True)

            rospy.Subscriber("/shoulder/shoulder",PoseStamped, self.MC_callback,0, tcp_nodelay=True)

            if self.appendage == "Right" or self.appendage == "Both":
                rospy.Subscriber("/link1/link1",PoseStamped, self.MC_callback,2, tcp_nodelay=True)

                rospy.Subscriber("/endeffecter/endeffecter",PoseStamped, self.MC_callback,'e', tcp_nodelay=True)

            if self.appendage == "Left" or self.appendage == "Both":
                rospy.Subscriber("/link_left/link_left",PoseStamped,self.MC_callback,2, tcp_nodelay=True)

                rospy.Subscriber("/left_hand/left_hand",PoseStamped,self.MC_callback,'e', tcp_nodelay=True)

        elif self.sensor == "IMU":
            #shoulder
            rospy.Subscriber("/imu_6/data",Imu, self.IMU_callback,0, tcp_nodelay=True)
            #link1
            rospy.Subscriber("/imu_0/data",Imu, self.IMU_callback,2, tcp_nodelay=True)
            #Abdomen
            rospy.Subscriber("/imu_6/data",Imu, self.IMU_callback,4, tcp_nodelay=True)
            #end effector
            rospy.Subscriber("/imu_2/data",Imu, self.IMU_callback,'e', tcp_nodelay=True)

            offsets = sio.loadmat(os.path.expanduser('~') + '/git/byu/development/kl_ws/src/low_level_control/src/imu_offsets')

            self.offset_shoulder = offsets['offset_shoulder'][0]
            self.offset_link1 = offsets['offset_link1'][0]
            self.offset_abdomen = offsets['offset_abdomen'][0]
            self.offset_endeffecter = offsets['offset_endeffecter'][0]
            self.quat_cortex_madgwick = offsets['quat_cortex_madgwick'][0]

        self.jangles_pub = rospy.Publisher('KLJointAngles', j_a_msg_array, queue_size=1)

    def MC_callback(self,data,which):
        data = data.pose
        quat = [data.orientation.x,data.orientation.y,data.orientation.z,data.orientation.w]
        if which==0:
            self.quat_shoulder_ = quat

        elif which==2:
            self.quat_link1_ = quat

    	elif which==4:
            self.quat_abdomen_ = quat

        elif which=='e':
            self.quat_endeffecter_ = quat

        elif which==6:
            self.quat_link_ = quat

        elif which=='le':
            self.quat_left_hand_ = quat

    def IMU_callback(self,data,which):

        quat = [data.orientation.x,data.orientation.y,data.orientation.z,data.orientation.w]

        # Make the rotation from the IMU as if from Cortex
        quat = tf.transformations.quaternion_multiply(self.quat_cortex_madgwick,quat)

        # Make the rotation to the IMU as if to the Cortex frame on it
        if which==0:
            self.quat_shoulder_ = tf.transformations.quaternion_multiply(quat,self.offset_shoulder)

        elif which==2:
            self.quat_link1_ = tf.transformations.quaternion_multiply(quat,self.offset_link1)

    	elif which==4:
            self.quat_abdomen_ = tf.transformations.quaternion_multiply(quat,self.offset_abdomen)

        elif which=='e':
            self.quat_endeffecter_ = tf.transformations.quaternion_multiply(quat,self.offset_endeffecter)


    def find_joint_angles(self):

        self.lock.acquire()
        try:
            self.quat_shoulder = self.quat_shoulder_
            self.quat_link1 = self.quat_link1_
            self.quat_abdomen = self.quat_abdomen_
            self.quat_endeffecter = self.quat_endeffecter_
            self.quat_link_left = self.quat_link_left_
            self.quat_left_hand = self.quat_left_hand_
        finally:
            self.lock.release()

        
        quats = [self.quat_abdomen, self.quat_shoulder, self.quat_link1, self.quat_endeffecter]

        self.q = self.joint_angle_finder.joint_angle_calc(quats, self.dh, self.frames, self.q_prev[4].tolist(), self.rot_baseDH_2_sensor)

        #append values to arrays that will be used in moving average filter
        '''
        self.append_to_list(self.qhip, 0, self.q)
        self.append_to_list(self.q0, 1, self.q)
        self.append_to_list(self.qp_shoulder, 2, self.q)
        self.append_to_list(self.q1, 3, self.q)
        self.append_to_list(self.q3, 4, self.q)
        self.append_to_list(self.qp_wrist, 5, self.q)
        self.append_to_list(self.q4, 6, self.q)

        self.q[0] = self.moving_average_filter(self.qhip)
        self.q[1] = self.moving_average_filter(self.q0)
        self.q[2] = self.moving_average_filter(self.qp_shoulder)
        self.q[3] = self.moving_average_filter(self.q1)
        self.q[4] = self.moving_average_filter(self.q3)
        self.q[5] = self.moving_average_filter(self.qp_wrist)
        self.q[6] = self.moving_average_filter(self.q4)

        self.q = np.array(self.q)
        self.q_prev = np.array(self.q_prev)
        '''

        # IIR filter the joint angles

        q_filt = -self.a2*self.qfilt_prev[3]-self.a3*self.qfilt_prev[2]-self.a4*self.qfilt_prev[1]-self.a5*self.qfilt_prev[0]+self.b1*self.q_prev[4]+self.b2*self.q_prev[3]+self.b3*self.q_prev[2]+self.b4*self.q_prev[1]+self.b5*self.q_prev[0]

        # Update the q_prev and qfilt_prev deques
        self.q_prev.popleft()
        self.q_prev.append(np.array(self.q))
        #self.q = np.array(q_filt) #This does filtering
        self.q = np.array(self.q) #This does no filtering
        self.qfilt_prev.popleft()
        self.qfilt_prev.append(self.q)


    def find_joint_velocities(self):

        self.qd = ((2.0*self.tau-(1.0/self.rate))/(2.0*self.tau+(1.0/self.rate))*self.qd_prev[4] + 2.0/(2.0*self.tau+(1.0/self.rate))*(self.q - self.qfilt_prev[2]))

        #self.qd = (self.qfilt_prev[1]-self.qfilt_prev[2])*self.rate

        qd_filt = -self.a2*self.qdfilt_prev[3]-self.a3*self.qdfilt_prev[2]-self.a4*self.qdfilt_prev[1]-self.a5*self.qdfilt_prev[0]+self.b1*self.qd_prev[4]+self.b2*self.qd_prev[3]+self.b3*self.qd_prev[2]+self.b4*self.qd_prev[1]+self.b5*self.qd_prev[0]

        # Update the qd_prev and qdfilt_prev deques
        self.qd_prev.popleft()
        self.qd_prev.append(np.array(self.qd))
        self.qd = np.array(qd_filt)
        self.qdfilt_prev.popleft()
        self.qdfilt_prev.append(self.qd)

        #append values to arrays that will be used in moving average filter
        '''
        self.append_to_list(self.qdhip, 0, self.qd)
        self.append_to_list(self.qd0, 1, self.qd)
        self.append_to_list(self.qdp_shoulder, 2, self.qd)
        self.append_to_list(self.qd1, 3, self.qd)
        self.append_to_list(self.qd3, 4, self.qd)
        self.append_to_list(self.qdp_wrist, 5, self.qd)
        self.append_to_list(self.qd4, 6, self.qd)

        self.qd[0] = self.moving_average_filter(self.qdhip)
        self.qd[1] = self.moving_average_filter(self.qd0)
        self.qd[2] = self.moving_average_filter(self.qdp_shoulder)
        self.qd[3] = self.moving_average_filter(self.qd1)
        self.qd[4] = self.moving_average_filter(self.qd3)
        self.qd[5] = self.moving_average_filter(self.qdp_wrist)
        self.qd[6] = self.moving_average_filter(self.qd4)
        '''

        #self.qd_prev = self.qd
        #self.q_prev = self.q

    def pub_joint_angles(self):

        self.q_hip = self.q[0]
        self.q_right[0] = self.q[1]#Shoulder 1
        self.q_right[1] = self.q[3]#Shoulder 2
        self.q_right[2] = 0 #elbow
        self.q_right[3] = self.q[4]#Wrist 1
        self.q_right[4] = self.q[6]#Wrist 2
        self.q_right[5] = self.q[2] #ShoulderPassive joint
        self.q_right[6] = self.q[5] #WristPassive joint

        self.qd_hip = self.qd[0]
        self.qd_right[0] = self.qd[1]
        self.qd_right[1] = self.qd[3]
        self.qd_right[2] = 0
        self.qd_right[3] = self.qd[4]
        self.qd_right[4] = self.qd[6]#Because of wrist passive joint
        self.qd_right[5] = self.qd[2] #Passive joint
        self.qd_right[6] = self.qd[5] #Passive joint

        print(np.array([self.q[1],self.q[3],0,self.q[4],self.q[6]])*180.0/np.pi)
        #print np.array(self.q0)*180.0/np.pi
        #print np.array(self.qd_right)*180.0/np.pi

        q_right_msg = j_a_msg(self.appendage,self.q_right,self.qd_right,1)

        msg = j_a_msg_array()
        msg.joint_angles.append(q_right_msg)

        #if self.appendage == "Right":
        q_hip_msg = j_a_msg("Hip", [self.q_hip],[self.qd_hip],1)
        msg.joint_angles.append(q_hip_msg)

        self.jangles_pub.publish(msg)

    def moving_average_filter(self, array):
        k = 0

        for i in range(0, len(array)):
            k = k + array[i]

        array_filtered = k/len(array)

        return array_filtered

    def append_to_list(self, array, jt_num, jt_array):
        array.append(jt_array[jt_num])

        while len(array) > self.num_pts:
            array.pop(0)

    def publish_to_tf(self):
        br01 = tf.TransformBroadcaster()
        br02 = tf.TransformBroadcaster()
        br03 = tf.TransformBroadcaster()
        br04 = tf.TransformBroadcaster()
        br05 = tf.TransformBroadcaster()
        br06 = tf.TransformBroadcaster()
        br07 = tf.TransformBroadcaster()

        br01.sendTransform([0,0,1],
                           self.quat_abdomen,
                           rospy.Time.now(),
                           'abdomen',
                           'my_cortex')

        br02.sendTransform([0,1,2],
                           self.quat_shoulder,
                          rospy.Time.now(),
                          'shoulder',
                          'my_cortex')

        br03.sendTransform([0,1,1.5],
                           self.quat_link1,
                          rospy.Time.now(),
                          'link1',
                          'my_cortex')

        br04.sendTransform([0,1,1],
                           self.quat_endeffecter,
                          rospy.Time.now(),
                          'ee',
                          'my_cortex')

        #br05.sendTransform([0,0,0],
        #                  self.quat_cortex_madgwick,
        #                  rospy.Time.now(),
        #                  'madgwick',
        #                  'my_cortex')


if __name__=='__main__':
    Louie = KingLouieJointFinder()
    rospy.sleep(1)
    rate = rospy.Rate(Louie.rate)

    while not rospy.is_shutdown():
        Louie.find_joint_angles()
        Louie.find_joint_velocities()
        Louie.pub_joint_angles()
        Louie.publish_to_tf()

        rate.sleep()

#    data = {'q':Louie.q_list,'qfilt':Louie.qfilt_list}
#    sio.savemat('q_and_qfilt',data)
