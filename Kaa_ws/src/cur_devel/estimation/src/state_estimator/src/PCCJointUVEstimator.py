import numpy as np
import yaml
import rospy
import tf2_ros
import tf
from std_msgs.msg import Float32MultiArray
from ros_msg.msg import SystemMinimalMsg
from ros_msg.msg import JointFlexusMinimalMsg
from copy import deepcopy

class PCCJointUVEstimator():

    def __init__(self, arm, sensor_strings, sensors2links_yaml, publish=True, publish_topic='uv_estimates',filter_velocity=True):

        self.arm = arm
        self.num_joints = self.arm.num_dof()/2
        self.filter_velocity = filter_velocity

        # Current Estimates
        self.u = np.zeros(self.num_joints)
        self.v = np.zeros(self.num_joints)
        self.u_last = np.zeros(self.num_joints)
        self.v_last = np.zeros(self.num_joints)
        self.u_last2 = np.zeros(self.num_joints)
        self.v_last2 = np.zeros(self.num_joints)
        self.udot = np.zeros(self.num_joints)
        self.vdot = np.zeros(self.num_joints)
        self.udot_last = np.zeros(self.num_joints)
        self.vdot_last = np.zeros(self.num_joints)
        self.udot_last2 = np.zeros(self.num_joints)
        self.vdot_last2 = np.zeros(self.num_joints)

        self.phi = np.zeros(self.num_joints)

        # Current Measurements (rotation matrices from bot of jt to top of jt)
        self.rotation_matrices = np.zeros([3,3,self.num_joints])

        # Make subscribers to link orientation data on tf
        self.tfbuffer = tf2_ros.Buffer()
        self.tflistener = tf2_ros.TransformListener(self.tfbuffer)

        # Make a publisher if desired
        if publish == True:
            self.est_pub = rospy.Publisher(publish_topic+"/estimation_minimal",SystemMinimalMsg)
            self.upub = rospy.Publisher(publish_topic+"/u",Float32MultiArray)
            self.vpub = rospy.Publisher(publish_topic+"/v",Float32MultiArray)
            self.udotpub = rospy.Publisher(publish_topic+"/udot",Float32MultiArray)
            self.vdotpub = rospy.Publisher(publish_topic+"/vdot",Float32MultiArray)

            self.system_minimalpub = rospy.Publisher(publish_topic+"/system_minimal",SystemMinimalMsg)

        # These rotations are from the sensor frame to the bottom of the joint (top of the link)
        s2l = yaml.load(sensors2links_yaml)
        self.sensors2links = np.zeros([3,3,self.num_joints+1])
        for i in range(0,self.num_joints + 1):
            self.sensors2links[:,:,i] = np.array(s2l[i]).reshape([3,3])

        # Define which sensors are on which links
        self.sensor_strings = sensor_strings

        # Rate at which to do estimation
        self.hz = 500
        self.rate = rospy.Rate(self.hz)

        while not rospy.is_shutdown():

            # Store the last u, v estimates
            self.u_last2 = deepcopy(self.u_last)
            self.v_last2 = deepcopy(self.v_last)
            self.u_last = deepcopy(self.u)
            self.v_last = deepcopy(self.v)

            # Udpate the u and v estimates
            self.update_uv_estimates()

            # Estimate udot and vdot by backwards differencing
            if self.filter_velocity == True:
                self.udot_last2 = deepcopy(self.udot_last)
                self.vdot_last2 = deepcopy(self.vdot_last)
                self.udot_last = deepcopy(self.udot)
                self.vdot_last = deepcopy(self.vdot)

                self.udot = .0625*(self.u-self.u_last)*self.hz + .0625*self.udot_last + .875*self.udot_last2
                self.vdot = .0625*(self.v-self.v_last)*self.hz + .0625*self.vdot_last + .875*self.vdot_last2
            else:
                self.udot = self.u - self.u_last
                self.vdot = self.v - self.v_last

            if publish == True:
                # Create messages
                u_msg = Float32MultiArray()
                v_msg = Float32MultiArray()
                udot_msg = Float32MultiArray()
                vdot_msg = Float32MultiArray()
                est_msg = SystemMinimalMsg()

                # Fill the messages
                u_msg.data = self.u
                v_msg.data = self.v
                udot_msg.data = self.udot
                vdot_msg.data = self.vdot

                # est_msg.joints_est.resize(self.num_joints)
                for i in range(0,self.num_joints):
                    joint_msg = JointFlexusMinimalMsg()

                    joint_msg.q[0] = self.u[i]
                    joint_msg.q[1] = self.v[i]
                    joint_msg.qd[0] = self.udot[i]
                    joint_msg.qd[1] = self.vdot[i]
                    est_msg.joints_est.append(joint_msg)

                # publish the message
                self.upub.publish(u_msg)
                self.vpub.publish(v_msg)
                self.udotpub.publish(udot_msg)
                self.vdotpub.publish(vdot_msg)
                self.system_minimalpub.publish(est_msg)

            print("\n\nu: ",self.u)
            print("v: ",self.v)

            self.rate.sleep()


    def update_uv_estimates(self):
        for jt in range(0,self.num_joints):
            try:
                # Get the rotation from this link to the next
                quat = self.tfbuffer.lookup_transform(self.sensor_strings[jt],self.sensor_strings[jt+1],rospy.Time()).transform.rotation

                # Transform into a rotation matrix
                my_quat = np.zeros([4,1])
                my_quat[0] = quat.x
                my_quat[1] = quat.y
                my_quat[2] = quat.z
                my_quat[3] = quat.w
                rot = tf.transformations.quaternion_matrix(my_quat.squeeze())[0:3,0:3]

                # Pre-multiply by rotation from link to sensor
                rot = np.dot(self.sensors2links[:,:,jt].T,rot)

                # Post-multiply by rotation from sensor to end of next link
                rot = np.dot(rot,self.sensors2links[:,:,jt+1])

                #if jt==0:
                    # print "\nrot:\n",rot
                    #print "\n\ngtopend:\n",self.arm.links()[jt].g_top_end()[0:3,0:3]

                # Post-multiply by rotation from end of next link to top of joint
                rot = np.matmul(rot,self.arm.links()[jt].g_top_end()[0:3,0:3].T)

                # Update the measurement
                self.rotation_matrices[:,:,jt] = rot

            except(tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
                print(e)

        # Estimate the u and v based on the new measurements
        self.estimate_uv()

    def estimate_uv(self):

        # for each joint
        for i in range(0,self.num_joints):

            # Calculate phi
            self.phi[i] = np.arccos(self.rotation_matrices[2,2,i])

            if abs(self.phi[i]) >= 1e-3:

                # Calculate u as the average of two estimates
                self.u[i] = 0.125*(.5*self.phi[i]/np.sin(self.phi[i])*self.rotation_matrices[2,1,i] - .5*self.phi[i]/np.sin(self.phi[i])*self.rotation_matrices[1,2,i]) + 0.125*self.u_last[i] + 0.75*self.u_last2[i]

                # Calculate v as the average of two estimates
                self.v[i] = 0.125*(-.5*self.phi[i]/np.sin(self.phi[i])*self.rotation_matrices[2,0,i] + .5*self.phi[i]/np.sin(self.phi[i])*self.rotation_matrices[0,2,i]) + 0.125*self.v_last[i] + 0.75*self.v_last2[i]

            else:

                # Calculate u as the average of two estimates
                self.u[i] = 0.125*(.5*self.rotation_matrices[2,1,i] - .5*self.rotation_matrices[1,2,i]) + 0.125*self.u_last[i] + 0.75*self.u_last2[i]

                # Calculate v as the average of two estimates
                self.v[i] = 0.125*(-.5*self.rotation_matrices[2,0,i] + .5*self.rotation_matrices[0,2,i]) + 0.125*self.v_last2[i] + 0.75*self.v_last2[i]


if __name__=='__main__':

    # Create a Zephyrus arm for example
    from zephyrus_arm import make_zephyrus_arm
    arm = make_zephyrus_arm()

    # These are the tf names of the sensors in order from proximal to distal
    sensor_tf_strings = ['neo_tracker1','neo_tracker2','neo_tracker3','neo_tracker4']

    # This is the yaml containing the sensor2link transformations
    sensor2link_yaml = open('./zephyrus_sensors2links.yaml')

    # Initialize a ROS node
    rospy.init_node('ZephyrusUVEstimator')

    # Start the estimator
    uv_estimator = PCCJointUVEstimator(arm,sensor_tf_strings,sensor2link_yaml,publish=True,publish_topic='zephyrus_uv')





