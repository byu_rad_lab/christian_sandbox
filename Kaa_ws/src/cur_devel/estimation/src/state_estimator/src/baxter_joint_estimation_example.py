#!/usr/bin/env python
import roslib
import rospy
import numpy as np
import math
from collections import deque
import tf
import geometry_msgs.msg
from geometry_msgs.msg import Quaternion
import JointAngleFinder
from vision_msgs.msg import joint_angles_msg as j_a_msg
from vision_msgs.msg import joint_angles_msg_array as j_a_msg_array

if __name__ == '__main__':
    rospy.init_node('baxter_test')

    listener = tf.TransformListener()
    pi = np.pi
    joint_angle_finder = JointAngleFinder.JointAngleFinder()


##==============================  BAXTER  ==============================##

    #         # BAXTER RIGHT ARM DH PARAMETERS
    #         # a         alpha       d           theta
    dh = [
            (0.069,     -pi/2,      0.27035,    -pi/4),
            (0.0,       pi/2,       0.0,        pi/2),
            (0.069,     -pi/2,      0.36435,    0)]
    #         (0.0,      pi/2,       0.0,        0),
    #         (0.01,     -pi/2,      0.37429,    0),
    #         (0.0,      pi/2,       0.0,        0)]
    #         # (0.0,      0,          0.229525,   0)]
    #
    arm = 'Baxter_Right_Arm'
##======================================================================##

    # Establish the frame for the base of the robot
    base_frame = [0,0,0,1]

    rot_baseDH_2_sensor = [ [1,0,0],
                            [0,1,0],
                            [0,0,1]]

    # what DH frames are the inputs coming from?
    frames = [0,3]

    # Establish what joint angles the robot starts in
    q = np.array([0,0,0])
    q_prev = deque()
    q_prev.append(q)
    q_prev.append(q)
    q_prev.append(q)
    q_prev.append(q)
    q_prev.append(q)

    pose = rospy.Publisher('transform_quat', geometry_msgs.msg.Quaternion,queue_size=1)
    q_publisher = rospy.Publisher('qs', j_a_msg_array, queue_size=1)

    print("\n******** INSTRUCTIONS FOR ZEROING THE ORIENTATION ********")
    rospy.logwarn("Once the cameras are on the robot, press ENTER to establish the zero configuration\n")
    input()
    rospy.loginfo("Joint angles initialized to zero.")
    (trans,rot) = listener.lookupTransform('world', 'camera_pose', rospy.Time(0))

    #How are the cameras oriented at 0 with respect to the base frame?
    offset1 = [-rot[0], -rot[1], -rot[2], rot[3]]

    num = 15
    rate = rospy.Rate(num)

    while not rospy.is_shutdown():

        try:
            # Pull the quaternions from ORB_SLAM cameras into rot
            (trans,rot) = listener.lookupTransform('world', 'camera_pose', rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

        pose.publish(Quaternion(*rot))
        rot = tf.transformations.quaternion_multiply(rot,offset1)

        # identify the input quaternions to do the joint calculations
        quats = [base_frame,rot]

        # This is where the joint angle calculation is performed
        q = joint_angle_finder.joint_angle_calc(quats, dh, frames, q_prev[4].tolist(), rot_baseDH_2_sensor)

        # Update the q_prev deque
        q = np.array(q) #This does no filtering
        q_prev.pop()
        q_prev.append(q)

        qd = [(q[0]-q_prev[4][0])*num, (q[1]-q_prev[4][1])*num, (q[2]-q_prev[4][2])*num]
        q_msg = j_a_msg(arm,q,qd,1)
        msg = j_a_msg_array()
        msg.joint_angles.append(q_msg)
        q_publisher.publish(msg)

        rate.sleep()
