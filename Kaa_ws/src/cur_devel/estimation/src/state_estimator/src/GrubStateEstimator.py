#!/usr/bin/env python


# Written by Curtis Johnson 2/25/2020
# Purpose: Estimate the state of the grub robot platform real-time for control. The state is [u,v,udot,vdot].
# The raw data is captured by two vive trackers, one of which is attached to the base frame and the other attached to the top of the grub. This code solves for the rotation matrix between them, and then filters and differentiates to estimate udot and vdot. This is all published on ROS topics called grubState/u, grubState/v, grubState/udot, grubState/vdot from a ROS node called GrubStateEstimator.

# 2: Note that methods starting with an underscore(_FunctionName) are private and will not be imported with "from GrubStateEstimator import *". They are only weak private however bc Python does not support private methods, so they can still technically be called (GrubStateEstimatorObject._calcRotation). (see https://www.geeksforgeeks.org/underscore-_-python/ for more details.)


import numpy as np
import rospy
import tf
import tf2_ros
from std_msgs.msg import Float32
#NOTE: The ego-atheris ROS workspace needs to be sourced in order for python to find ros_msg. This may only work on the RTPC until we can replace the pressure sensors. 
from std_msgs.msg import Float32MultiArray
from ros_msg.msg import SystemMinimalMsg
from copy import deepcopy
import time


class GrubStateEstimator():
    def __init__ (self):
        self.vive_tracker = ['neo_tracker1','neo_tracker2']
        self.num_joints = 1
        self.topic_name = "grubState"
        self.udot_prev = 0
        self.vdot_prev = 0

        #Initialize states
        self.u = np.zeros(self.num_joints)
        self.v = np.zeros(self.num_joints)
        self.u_prev = np.zeros(self.num_joints)
        self.v_prev = np.zeros(self.num_joints)
        self.udot = np.zeros(self.num_joints)
        self.vdot = np.zeros(self.num_joints)

        self.phi = np.zeros(self.num_joints)
        self.rotation = np.zeros([3,3]) #rotation from neo_tracker1 to neo_tracker2

        #initialize subscriber to tf (see http://wiki.ros.org/tf2/Tutorials/Writing%20a%20tf2%20listener%20%28Python%29)
        self.tfbuffer = tf2_ros.Buffer()
        self.tflistener = tf2_ros.TransformListener(self.tfbuffer)

        #make publishers to publish state data
        self.estimation_pub = rospy.Publisher(self.topic_name+"/estimation_minimal",SystemMinimalMsg, queue_size = 10)
        
        #individual publishers - we want this all in one message now to sync everything
        #self.upub = rospy.Publisher(self.topic_name+"/u",Float32, queue_size = 10)
        #self.vpub = rospy.Publisher(self.topic_name+"/v",Float32, queue_size = 10)
        #self.udotpub = rospy.Publisher(self.topic_name+"/udot",Float32, queue_size = 10)
        #self.vdotpub = rospy.Publisher(self.topic_name+"/vdot",Float32, queue_size = 10)
        self.system_minimalpub = rospy.Publisher(self.topic_name+"/system_minimal",SystemMinimalMsg, queue_size = 10)
        
        self.state_pub = rospy.Publisher(self.topic_name, Float32MultiArray, queue_size = 10)

        # Rate at which to do estimation
        self.hz = 500.0
        self.Ts = 1/self.hz #Ts 
        self.rate = rospy.Rate(self.hz)
        
        #param for dirty derivative. Dirty derivative differentiates signals with frequency content less than 1/sigma (sigma = .05 means only signals <20Hz will be differentiated.)
        self.sigma = .1
        
        time.sleep(1)
        
        while not rospy.is_shutdown():
            #update delayed variables
            self.u_prev = deepcopy(self.u)
            self.v_prev = deepcopy(self.v)
            self.udot_prev = deepcopy(self.udot)
            self.vdot_prev = deepcopy(self.vdot)
            
            #Note: this try statement ensures that the tfbuffer is actually filled with something before trying to publish and manipulate it
            #try:
            self._calcRotation()

            #update u and v estimates
            self._estimateUV()

            #estimate u and v dot
            self._estimateUdotVdot()

            #create ROS messages
            #u_msg = Float32()
            #v_msg = Float32()
            #udot_msg = Float32()
            #vdot_msg = Float32()
            #est_msg = SystemMinimalMsg()
            state_msg = Float32MultiArray()

                #fill ROS messages and resize data as needed
            #u_msg.data = -self.u #negative to make x and y axes on vive line up with grub axes
            #v_msg.data = self.v                
            #udot_msg.data = self.udot
            #dot_msg.data = self.vdot
            state_msg.data = [self.udot, self.vdot, -self.u, self.v]

                #publish message
            #self.upub.publish(u_msg)
            #self.vpub.publish(v_msg)                
            #self.udotpub.publish(udot_msg)
            #self.vdotpub.publish(vdot_msg)
            #self.system_minimalpub.publish(est_msg)
            self.state_pub.publish(state_msg)  
                #wait to keep estimation rate @ self.hz
            self.rate.sleep() 
            #except:
             #   print "Unable to get transform data.\n --- Make sure trackers are all on and not in standby mode.\n --- Make sure all trackers are visible by tower."
              #  raise
               


    def _calcRotation(self): #see note 2 above
    
        #lookup_transform returns translation xyz and rotation as quaternion
        quat = self.tfbuffer.lookup_transform(self.vive_tracker[0], self.vive_tracker[1], rospy.Time()).transform.rotation
                
        #convert published quaternion to a homogeneous transform
        transform = tf.transformations.quaternion_matrix([quat.x,quat.y,quat.z,quat.w])
              
        #extract rotation matrix from transform
        self.rotation = transform[0:3,0:3]
        #print self.rotation, "\n"
        
    def _estimateUV(self):
        #Note: these terms are taken from this paper (INSERT PAPER LINK HERE) which defines the kinematics of the grub joint. 
        
        #calculate phi from rotation matrix. 
        self.phi = np.arccos(self.rotation[2,2])
        #print "Phi"
        #print self.phi*180/np.pi
        
        #estimate u from by averaging two different terms of transformation matrix
        u_tilde1 = self.rotation[2,1]/np.sin(self.phi)
        u_tilde2 = self.rotation[1,2]/-np.sin(self.phi)
        self.u = .5*self.phi*(u_tilde1 + u_tilde2)
                
        #estimate v from by averaging two different terms of transformation matrix
        v_tilde1 = self.rotation[0,2]/np.sin(self.phi)
        v_tilde2 = self.rotation[2,0]/-np.sin(self.phi)
        self.v = .5*self.phi*(v_tilde1 + v_tilde2)
        
        #print "Actual"
        #print (self.u*180/np.pi, self.v*180/np.pi)
        

        
    def _estimateUdotVdot(self):
        
        #dirty derivative
        self.udot = ((2*self.sigma - self.Ts)/(2*self.sigma + self.Ts))*self.udot_prev + (2/(2*self.sigma + self.Ts))*(self.u - self.u_prev)
        self.vdot = ((2*self.sigma - self.Ts)/(2*self.sigma + self.Ts))*self.vdot_prev + (2/(2*self.sigma + self.Ts))*(self.v - self.v_prev)
        
        #backward differencing
        #self.udot = (self.udot + (self.u-self.u_prev)*self.hz)/2.0
        #self.vdot = (self.vdot + (self.v-self.v_prev)*self.hz)/2.0
        
        


if __name__=="__main__":

    #initialize ROS node
    rospy.init_node('GrubStateEstimator')

    #initialize estimator object. ROS will hang out in __init__ func until ROS is shutdown.
    grubStateEstimator = GrubStateEstimator()



