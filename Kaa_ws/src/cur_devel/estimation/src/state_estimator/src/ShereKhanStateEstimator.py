#!/usr/bin/env python
from kinematics import arm_tools as at
import numpy as np
import os
import PCCJointUVEstimator as PCC
import rospy
import sys
from tools import kin_tools as kt


if __name__=='__main__':

    #define leg
    density = 4.980 #4.980 was measured empirically of the aluminium used in the adjustable links made for the leg
    min_lengths = [0.185, 0.263, 0.143, 0.15]
    R = 0.18 #radius of shoulder mount
    d = 0.145 #offset to beginning of joint from shoulder mount rotation center

    #robot 66
    # lengths = [0.185, 0.263, 0.143, 0.15]
    # angles = [1.199,0.0,90.0*np.pi/180.0,0.0]
    # x_rotation = -1.2146
    # base_hom = kt.ax_vec2homog(np.array([x_rotation, 0, 0]), np.array([0.0, R + d*np.sin(x_rotation), d*np.cos(x_rotation)]))
    # base_width = 1.1169
    # base_length = 0.4537
    # mount_angle = 0.826

    #robot 4
    lengths = [0.207, 0.371, 0.143, 0.234]
    angles = [57.0*np.pi/180.0,0.0,63.0*np.pi/180.0,0.0]
    x_rotation = -95.0*np.pi/180.0
    base_hom = kt.ax_vec2homog(np.array([x_rotation, 0, 0]), np.array([0.0, R + d*np.sin(x_rotation), d*np.cos(x_rotation)]))
    base_width = 1.2917
    base_length = 1.1814
    mount_angle = 0.895

    quadruped = at.quadruped(lengths,angles,base_hom,base_width,base_length,mount_angle,0.205, density, min_lengths)
    quadruped.calcMass()

    jangles = [-0.114, -0.411, 0.930, -0.917]
    quadruped.setJointAngs(np.array(jangles))
    quadruped.calcJac()
    J = quadruped.getJacHybrid()
    wrench = np.array([[0.0],[0.0],[(0.5*quadruped.getBaseMass() + 2.0*quadruped.getMass())*9.81], [0.0], [0.0], [0.0]])
    torques = np.dot(J.T,wrench)
    print("Torques: ", torques)
    print("Wrench: ", wrench)
    print("body mass: ", quadruped.getBaseMass())
    print("leg mass: ", quadruped.getMass())
#   vanilla_quadruped = at.quadruped(min_lengths,angles,base_hom,base_width,base_length,mount_angle,0.205, density)
#   vanilla_quadruped.setJointAngs(np.array([0.0,0.0,0.0,0.0]))
#   vanilla_quadruped.calcFK()
#   print vanilla_quadruped.getGWorldTool()



    # These are the tf names of the sensors in order from proximal to distal
    sensor_tf_strings = ['neo_tracker1','neo_tracker2','neo_tracker3']

    print("# args: ", len(sys.argv))
    print("args: ", sys.argv)

    # This is the yaml containing the sensor2link transformations
    #sensor2link_yaml = open('./sherekhan_sensors2links.yaml')
    sensor2link_yaml = open(os.path.expanduser(sys.argv[1]))

    # Initialize a ROS node
    rospy.init_node('ShereKhanUVEstimator')

    # Start the estimator
    uv_estimator = PCC.PCCJointUVEstimator(quadruped,sensor_tf_strings,sensor2link_yaml,publish_topic='shere_khan_uv')


