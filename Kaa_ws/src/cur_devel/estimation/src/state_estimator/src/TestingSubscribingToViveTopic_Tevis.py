#!/usr/bin/env python
import rospy
import tf
from tf2_msgs.msg import TFMessage
import math
import numpy as np
import time


class ViveSubscriber():
	def __init__(self, trackers):
		self.trackers = trackers
		rospy.init_node('vive_tf_listener')
		# rospy.Subscriber('/tf', TFMessage, self.ViveCallback)
		self.listener = tf.TransformListener()

		self.rate = rospy.Rate(10)

		self.start_flag = False
		while self.start_flag == False:
			input('Hit Enter when system is ready to calibrate')
			try:
				(trans, rot) = self.listener.lookupTransform('world_vive', '/neo_tracker1', rospy.Time(0))
				print(rot)
				self.start_flag = True
			except (tf.LookupException):
				print("lookup exception occurred")
				continue
			except(tf.ConnectivityException):
				print("connectivity exception")
				continue
			except(tf.ExtrapolationException):
				print("extrapolation exception")
				continue
			
			
		

	# def ViveCallback(self, msg):
		# print(msg.transforms[0].child_frame_id)
		# print(msg.transforms[0].transform.translation)
		# print(msg.transforms[0].transform.rotation)
		# y = 1

	def Run(self, x):
		while not rospy.is_shutdown():
			for pair in self.trackers:
				try:
			  		(trans, rot) = self.listener.lookupTransform(pair[0], pair[1], rospy.Time(0))
				except (tf.LookupException):
					print("lookup exception occurred")
					continue
				except(tf.ConnectivityException):
					print("connectivity exception")
					continue
				except(tf.ExtrapolationException):
					print("extrapolation exception")
					continue

			x += 1

			print(x)
			#rot is a quaternion, not a rotations matrix...
			print((math.atan2(2*(rot[1]*rot[3]+rot[2]*rot[0]), 2*(rot[2]*rot[3]-rot[1]*rot[0]))))


			self.rate.sleep()

if __name__ == '__main__':
	print("Hello")
#	vs = ViveSubscriber(['/neo_tracker1'])
	trackers = [['world_vive', '/neo_tracker1']]
	vs = ViveSubscriber(trackers)
	vs.Run(0)