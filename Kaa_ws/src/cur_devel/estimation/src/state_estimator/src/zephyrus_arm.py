import kinematics.kinematics as kin
import numpy as np

def make_zephyrus_arm():
    # Create an arm
    arm = kin.Arm()
    num_joints = 3
    
    jt_limit=np.pi/2
    
    # Create the first joint
    joint = kin.JointCCC(.212)
    
    # Add the first joint to the first link
    l = kin.Link(joint)
    
    # Specify the transformation from the top of the joint to the end of the link
    rot = np.array([[.8536, .1464, -.5],
                    [.1464, .8536, .5],
                    [.5, -.5, .7071]])
    
    vec = np.array([[-.0148],
                    [.0148],
                    [.293]])
    hom0 = kin.HomFromRotVec(rot,vec)
    l.g_top_end_ = np.array(hom0)
    
    # Specify the inertia of this joint/link
    m = 6.534
    I = np.array([.108, .108, .023])
    pos = np.array([0.0, 0, .115]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
    l.setInertiaSelf(m, pos, I)
    
    # Add the link to the robot
    arm.addLink(l)
    
    
    
    
    
    # Create the second joint
    joint = kin.JointCCC(.212)
    
    # Add the second joint to the third link
    l = kin.Link(joint)
    
    # Specify the transformation from the top of the joint to the end of the link
    rot = np.array([[.8536, .1464, -.5],
                    [.1464, .8536, .5],
                    [.5, -.5, .7071]])
    
    vec = np.array([[-.0148],
                    [.0148],
                    [.293]])
    
    hom0 = np.array(kin.HomFromRotVec(rot,vec))
    l.g_top_end_ = np.array(hom0)
    
    # Specify the inertia of this joint/link
    m = 6.534
    I = np.array([.108, .108, .023])
    pos = np.array([0.0, 0, .115]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
    l.setInertiaSelf(m, pos, I)
    
    # Add the link to the robot
    arm.addLink(l)
    
    
    
    
    
    # Create the third joint
    joint = kin.JointCCC(.212)
    
    # Add the third joint to the third link
    l = kin.Link(joint)
    
    # Specify the transformation from the top of the joint to the end of the link
    rot = np.eye(3)
    
    vec = np.array([[0.0],
                    [0.0],
                    [.025]])
    
    hom0 = np.array(kin.HomFromRotVec(rot,vec))
    l.g_top_end_ = np.array(hom0)
    
    # Specify the inertia of this joint/link
    m = 1.5
    I = np.array([.05, .05, .023]) 
    pos = np.array([0.0, 0, .0125]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
    l.setInertiaSelf(m, pos, I)
    
    # Add the link to the robot
    arm.addLink(l)

    return arm
