#!/usr/bin/env python
from EndEffectorEstimation import EndEffectorEstimation
from tools import kin_tools as kt
import numpy as np
import rospy

if __name__=='__main__':

    #robot 66
    # base_sensor_in_base_frame = np.array([[ 1.,          0.,          0.,          0.        ],
    #                                      [ 0.,         0.34871185, 0.93722999, 0.13476673],
    #                                      [ 0.,        -0.93722999, 0.34871185, -0.19311658],
    #                                      [ 0.,         0.        , 0.        , 1.        ]])
    #robot 4
    R = 0.18 #radius of shoulder mount
    d = 0.145 #offset to beginning of joint from shoulder mount rotation center
    x_rotation = -95.0*np.pi/180.0
    base_hom = kt.ax_vec2homog(np.array([x_rotation, 0, 0]), np.array([0.0, R + d*np.sin(x_rotation), d*np.cos(x_rotation)]))
    offset_hom = kt.ax_vec2homog(np.array([0.0,0.0,0.0]), np.array([0.0, 0.26, 0.0]))
    base_sensor_in_base_frame = np.dot(base_hom,offset_hom)

   #base_sensor_in_base_frame = np.eye(4)
   #base_sensor_in_base_frame[0][3] = 0.15
   #base_sensor_in_base_frame[1][3] = -0.052
   #base_sensor_in_base_frame[2][3] = 0.44
    carpet_sensor_in_carpet_frame = np.eye(4)
    carpet_sensor_in_carpet_frame[0][3] = -0.164
    carpet_sensor_in_carpet_frame[1][3] = 0.096
    carpet_sensor_in_carpet_frame[2][3] = 0.0

    carpet_est = EndEffectorEstimation('neo_tracker1', 'neo_tracker4', base_sensor_in_base_frame, carpet_sensor_in_carpet_frame, True, 'carpet_estimate')

    # Initialize a ROS node
    rospy.init_node('CarpetEstimator')

    rate = rospy.Rate(500)

    while not rospy.is_shutdown():

        carpet_est.estimate()

        rate.sleep()

