#ifndef __JOINTANGLEFINDER_H_INCLUDED__
#define __JOINTANGLEFINDER_H_INCLUDED__

#include <trac_ik/trac_ik.hpp>
#include <boost/date_time.hpp>
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/frames.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <stdio.h>
#include <math.h> //so can use M_PI (pi)
#include <iostream>
#include <fstream>
#include <eigen3/Eigen/Geometry>


//#include "ros/ros.h"
//#include "geometry_msgs/PoseStamped.h"
//#include "low_level_control/joint_angles_msg.h"
//#include "low_level_control/joint_angles_msg_array.h"

#include <Python.h>
#include <boost/python.hpp>


class JointAngleFinder
{
	public:
		
		//declare constructor function
		JointAngleFinder();
		

		
		

		//declare functions that will be used
		boost::python::list joint_angle_calc( boost::python::list quat, boost::python::list dh_py, boost::python::list frames, boost::python::list q_prev_py, boost::python::list rot_baseDH_2_sensor_py);
		//std::deque<double> joint_angle_calc( std::deque<Eigen::Quaterniond> quats, std::vector<std::vector<double> > dh, std::vector<int> frame, std::vector<double> q_prev); //calculates joint angles from quaternion data, dh parameters, the frame numbers of the quaternion data, and the previous joint angles
		Eigen::Matrix3d dh_2_rot(double alpha, double theta); //used to make a rotation matrix from one DH frame to the next from the DH parameters
		KDL::Rotation Matrix_to_Rot(Eigen::Matrix3d Rot); //used to change a matrix in the Eigen::Matrix3d construct to a KDL::Rotation construct so can use TRAC-IK
		void print_matrix(Eigen::Matrix3d r); //prints a 3d matrix to the screen
		double range_check(double q); //checks to make sure that an angle is between -PI and PI
	
		

		
};





#endif
