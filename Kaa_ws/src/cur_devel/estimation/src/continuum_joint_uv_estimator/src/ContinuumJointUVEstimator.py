import numpy as np
import yaml
import rospy
import tf2_ros
import tf
from std_msgs.msg import Float32MultiArray
from copy import deepcopy

class ContinuumJointUVEstimator():
    
    def __init__(self, num_joints, sensor_strings, sensors2links_yaml, publish=True, publish_topic='uv_estimates',filter_velocity=True):

        self.num_joints = num_joints
        self.filter_velocity = filter_velocity

        # Current Estimates
        self.u = np.zeros(self.num_joints)
        self.v = np.zeros(self.num_joints)
        self.u_last = np.zeros(self.num_joints)
        self.v_last = np.zeros(self.num_joints)
        self.udot = np.zeros(self.num_joints)
        self.vdot = np.zeros(self.num_joints)
        
        self.phi = np.zeros(self.num_joints)        

        # Current Measurements (rotation matrices from bot of jt to top of jt)
        self.rotation_matrices = np.zeros([3,3,self.num_joints])

        # Make subscribers to link orientation data on tf
        self.tfbuffer = tf2_ros.Buffer()
        self.tflistener = tf2_ros.TransformListener(self.tfbuffer)

        # Make a publisher if desired
        self.publish = publish
        if self.publish == True:
            self.upub = rospy.Publisher(publish_topic+"/u",Float32MultiArray)
            self.vpub = rospy.Publisher(publish_topic+"/v",Float32MultiArray)
            self.udotpub = rospy.Publisher(publish_topic+"/udot",Float32MultiArray)
            self.vdotpub = rospy.Publisher(publish_topic+"/vdot",Float32MultiArray)            

        # These rotations are from the sensor frame to the bottom of the joint (top of the link)
        s2l = yaml.load(sensors2links_yaml)
        self.sensors2links = np.zeros([3,3,self.num_joints+1])
        for i in range(0,self.num_joints + 1):
            self.sensors2links[:,:,i] = np.array(s2l[i]).reshape([3,3])

        # Define which sensors are on which links
        self.sensor_strings = sensor_strings

        # Rate at which to do estimation
        self.hz = 500
        self.rate = rospy.Rate(self.hz)
        
        while not rospy.is_shutdown():

            # Store the last u, v estimates
            self.u_last = deepcopy(self.u)
            self.v_last = deepcopy(self.v)
            
            # Udpate the u and v estimates
            self.update_uv_estimates()

            # Estimate udot and vdot by backwards differencing
            if self.filter_velocity == True:
                self.udot = (self.udot + (self.u-self.u_last)*self.hz)/2.0
                self.vdot = (self.vdot + (self.v-self.v_last)*self.hz)/2.0
            else:
                self.udot = self.u - self.u_last
                self.vdot = self.v - self.v_last            

            if self.publish == True:
                # Create messages
                u_msg = Float32MultiArray()
                v_msg = Float32MultiArray()
                udot_msg = Float32MultiArray()
                vdot_msg = Float32MultiArray()

                # Fill the messages                
                u_msg.data = self.u
                v_msg.data = self.v                
                udot_msg.data = self.udot
                vdot_msg.data = self.vdot

                # publish the message
                self.upub.publish(u_msg)
                self.vpub.publish(v_msg)                
                self.udotpub.publish(udot_msg)
                self.vdotpub.publish(vdot_msg)

            print("\n\nu: ",self.u)
            print("v: ",self.v)
                
            self.rate.sleep()
        

    def update_uv_estimates(self):
        for jt in range(0,self.num_joints):
            try:
                # Get the rotation from this link to the next
                quat = self.tfbuffer.lookup_transform(self.sensor_strings[jt],self.sensor_strings[jt+1],rospy.Time()).transform.rotation
                
                # Transform into a rotation matrix
                my_quat = np.zeros([4,1])
                my_quat[0] = quat.x
                my_quat[1] = quat.y
                my_quat[2] = quat.z
                my_quat[3] = quat.w
                rot = tf.transformations.quaternion_matrix(my_quat.squeeze())[0:3,0:3]

                # Pre-multiply by inverse of rotation from link to sensor
                rot = np.matmul(self.sensors2links[:,:,jt].T,rot)
                
                # Post-multiply by rotation from sensor to end of next link
                rot = np.matmul(rot,self.sensors2links[:,:,jt+1])

                # # Post-multiply by rotation from end of next link to top of joint
                # rot = np.matmul(rot,self.arm.links()[jt].g_top_end()[0:3,0:3].T)
                
                # Update the measurement
                self.rotation_matrices[:,:,jt] = rot
                
            except(tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
                print(e)
                
        # Estimate the u and v based on the new measurements
        self.estimate_uv()

    def estimate_uv(self):

        # for each joint
        for i in range(0,self.num_joints):

            # Calculate phi
            self.phi[i] = np.arccos(self.rotation_matrices[2,2,i])

            if abs(self.phi[i]) >= 1e-3:

                # Calculate u as the average of two estimates
                self.u[i] = .5*self.phi[i]/np.sin(self.phi[i])*self.rotation_matrices[2,1,i] - .5*self.phi[i]/np.sin(self.phi[i])*self.rotation_matrices[1,2,i]
                
                # Calculate v as the average of two estimates
                self.v[i] = -.5*self.phi[i]/np.sin(self.phi[i])*self.rotation_matrices[2,0,i] + .5*self.phi[i]/np.sin(self.phi[i])*self.rotation_matrices[0,2,i]

            else:

                # Calculate u as the average of two estimates
                self.u[i] = .5*self.rotation_matrices[2,1,i] - .5*self.rotation_matrices[1,2,i]
                
                # Calculate v as the average of two estimates
                self.v[i] = -.5*self.rotation_matrices[2,0,i] + .5*self.rotation_matrices[0,2,i]


if __name__=='__main__':

    # These are the tf names of the sensors in order from proximal to distal
    sensor_tf_strings = ['world','IMU 0','IMU 1']

    # This is the yaml containing the sensor2link transformations
    sensor2link_yaml = open('./zephyrus_sensors2links.yaml')

    # Initialize a ROS node
    rospy.init_node('ZephyrusUVEstimator')

    # Start the estimator
    num_joints = 2
    uv_estimator = ContinuumJointUVEstimator(num_joints,sensor_tf_strings,sensor2link_yaml,publish=True,publish_topic='zephyrus_uv')
    


            
                    
