from rad_models.BellowsArm2 import BellowsArm
from std_msgs.msg import Float32MultiArray
import numpy as np
import rospy
import matplotlib.pyplot as plt

u = np.zeros([3,1])
v = np.zeros([3,1])
x = np.zeros([24,1])

def u_cb(msg):
    u[0] = msg.data[0]
    u[1] = msg.data[1]
    # u[2] = msg.data[2]        

def v_cb(msg):
    v[0] = msg.data[0]
    v[1] = msg.data[1]
    # v[2] = msg.data[2]        
    
arm = BellowsArm()

rospy.init_node('arm_visualizer')

usub = rospy.Subscriber('/zephyrus_uv/u',Float32MultiArray,u_cb)
vsub = rospy.Subscriber('/zephyrus_uv/v',Float32MultiArray,v_cb)

ax = plt.gca(projection='3d')
plt.xlabel('X')
plt.ylabel('Y')        
plt.ylabel('Z')        

plt.ion()

while not rospy.is_shutdown():
    x[18] = u[0]
    x[19] = v[0]
    x[20] = u[1]
    x[21] = v[1]
    # x[22] = u[2]
    # x[23] = v[2]

    arm.visualize(x,ax)
    plt.show()
    
