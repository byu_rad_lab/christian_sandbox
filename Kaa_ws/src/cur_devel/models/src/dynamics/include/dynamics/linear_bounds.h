// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_DYNAMICS_INCLUDE_DYNAMICS_LINEAR_BOUNDS_H_
#define SRC_DYNAMICS_INCLUDE_DYNAMICS_LINEAR_BOUNDS_H_

#include <eigen3/Eigen/Dense>
#include "dynamics/types.h"

namespace dynamics {

// A class for defining an checking linear inequality constraints of the form:
//   A * q <= b, where q is the state
// Also keeps track of a cuboid region defined by box_{lower, upper}
//   This is used for rejection sampling, and should be a superset of the
//   region defined by A * x <= b.
class LinearBounds {
 public:
  explicit LinearBounds(double num_dof_);

  // getters
  const MatXd& a(void) const { return a_; }
  const VecXd& b(void) const { return b_; }

  const Vec& box_lower(void) const { return box_lower_; }
  const Vec& box_upper(void) const { return box_upper_; }

  // setters
  void set_num_dof(int num_dof) { num_dof_ = num_dof; }

  void set_a(const MatXd& a) { a_ = a; }
  void set_b(const VecXd& b) { b_ = b; }

  void set_box_lower(const Vec& box_lower) { box_lower_ = box_lower; }
  void set_box_upper(const Vec& box_upper) { box_upper_ = box_upper; }

  // Sets bounds to: -limit <= q <= limit
  // Sets A, b to match
  void SetLimitsScalar(double limit);

  // Sets bounds to box_lower <= q <= box_upper
  // Sets A, b to match
  void SetLimitsVector(const Vec& box_lower, const Vec& box_upper);

  // Creates a polygon of limits.  Radius is of inscribed circle.
  // Uses num_constraints lines, around center, rotated by phase.
  // Only worlds on 2-dof joints.
  void SetLimitsCircular(double radius,
                         int num_constraints = 8,
                         Vec center = Vec::Zero(0),
                         double phase = 0.0);

  // Returns A * q - b.  Should all be negative if within joint limits.
  VecXd CheckLimits(const Vec& q);

  // Returns max coefficient of A * q - b.  Negative is within joint limits.
  double CheckLimitsWorst(const Vec& q);

  // Find random state in A x <= b
  // Samples uniformly over box_{lower, upper}_
  // Rejects based on A * q <= b
  // If valid space is a small fraction of cuboid defined by bounds,
  //   this could be very slow.
  Vec GetQRandom(void);

 private:
  int num_dof_;
  MatXd a_;
  VecXd b_;

  // Scalar bounds on each joint variable
  // Used when cuboid bounds are needed, and for rejection sampling.
  // Cuboid should be superset of allowable states, or some states won't be sampled.
  // It would be nice to find this programmatically,
  //   but it's a non-trivial linear programming problem.
  Vec box_upper_;
  Vec box_lower_;
};

}  // namespace dynamics

#endif  // SRC_DYNAMICS_INCLUDE_DYNAMICS_LINEAR_BOUNDS_H_
