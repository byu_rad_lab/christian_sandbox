// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_DYNAMICS_INCLUDE_DYNAMICS_JOINT_ROT_H_
#define SRC_DYNAMICS_INCLUDE_DYNAMICS_JOINT_ROT_H_

#include "dynamics/types.h"
#include "dynamics/joint_base.h"

namespace dynamics {

class JointRot : public JointBase {
 public:
  JointRot(void);

  JointRot* Clone() const override { return new JointRot(*this); }
  const Twist& JointAccApparent(void) override { return joint_acc_apparent_; }

  void CalcFK(void) override;
  void CalcJac(void) override {}  // constant
  const Hom& g_joint_link(void) const override { return g_joint_link_; };
  const Jac& jac(void) const override { return jac_; };

 private:
  static const int num_dof_derived_ = 1;
  Hom g_joint_link_ = Hom::Identity();

  Jac jac_ = Jac::Zero(6, num_dof_derived_);

  double th_;   // theta
  double sth_;  // sin(theta)
  double cth_;  // cos(theta)

  Twist joint_acc_apparent_ = Twist::Zero();  // never changes
};

}  // namespace dynamics

#endif  // SRC_DYNAMICS_INCLUDE_DYNAMICS_JOINT_ROT_H_
