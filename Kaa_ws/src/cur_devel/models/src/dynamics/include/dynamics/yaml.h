// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_DYNAMICS_INCLUDE_DYNAMICS_YAML_H_
#define SRC_DYNAMICS_INCLUDE_DYNAMICS_YAML_H_

#include "yaml-cpp/yaml.h"
#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <string>

#include "primitives/exception.h"
#include "primitives/eigen_yaml.h"

#include "dynamics/arm.h"
#include "dynamics/joint_base.h"
#include "dynamics/joint_coriolis.h"
#include "dynamics/joint_fixed.h"
#include "dynamics/joint_flexus.h"
#include "dynamics/joint_arquus.h"
#include "dynamics/joint_prism.h"
#include "dynamics/joint_rot.h"
#include "dynamics/link.h"
#include "dynamics/types.h"

namespace dynamics {
// class ExceptionDynamics : public primitives::exception::Exception {
class ExceptionDynamics : public std::runtime_error {
 public:
  explicit ExceptionDynamics(const std::string& msg) : std::runtime_error(msg) {}
  // : primitives::exception::Exception(msg) {}
  ~ExceptionDynamics() throw() {}
};

class ExceptionYAML : public ExceptionDynamics {
 public:
  explicit ExceptionYAML(const std::string& msg) : ExceptionDynamics(msg) {}
  ~ExceptionYAML() throw() {}
};

}  // namespace dynamics

namespace YAML {

template <>
struct convert<dynamics::Rot> {
  static bool decode(const YAML::Node& node,
                     dynamics::Rot& rot) {  // NOLINT[runtime/references]
    unsigned int num_rows = 3;
    unsigned int num_cols = 3;
    if (!node.IsSequence() || node.size() != num_rows) {
      throw dynamics::ExceptionYAML(
          "dynamics::Rot expects rot matrix to be a sequence of size " +
          std::to_string(num_rows));
    }
    for (unsigned int i = 0; i < num_rows; ++i) {
      if (!node[i].IsSequence() || node[i].size() != num_cols) {
        throw dynamics::ExceptionYAML("dynamics::Rot expects rot matrix row " +
                                      std::to_string(i) + " to be a sequence of size " +
                                      std::to_string(num_cols));
      }
      for (unsigned int j = 0; j < num_cols; ++j) {
        if (!node[i][j].IsScalar()) {
          throw dynamics::ExceptionYAML("dynamics::Rot expects rot matrix element at " +
                                        std::to_string(i) + "," + std::to_string(j) +
                                        " to be scalar");
        }
        rot(i, j) = node[i][j].as<double>();
      }
    }
    rot = dynamics::RotNormalize(rot);
    return true;
  }
};

template <>
struct convert<dynamics::Hom> {
  static bool decode(const YAML::Node& node,
                     dynamics::Hom& hom) {  // NOLINT[runtime/references]
    if (!node.IsMap()) {
      throw dynamics::ExceptionYAML("dynamics::Hom expects a yaml map definition");
    }
    if (node["r"] == NULL || !node["r"].IsSequence()) {
      throw dynamics::ExceptionYAML("dynamics::Hom requires r component");
    }
    if (node["t"] == NULL || !node["t"].IsSequence()) {
      throw dynamics::ExceptionYAML("dynamics::Hom requires t component");
    }

    if (!node["t"].IsSequence() || node["t"].size() != 3) {
      throw dynamics::ExceptionYAML(
          "dynamics::Hom expected t component to be a sequence of size 3");
    }
    if (!node["t"][0].IsScalar()) {
      throw dynamics::ExceptionYAML("dynamics::Hom expected t elements to be scalars");
    }
    dynamics::Vec3 t_vec = node["t"].as<dynamics::Vec3>();

    if (!node["r"].IsSequence() || node["r"].size() != 3) {
      throw dynamics::ExceptionYAML(
          "dynamics::Hom expected r component to be a sequence of size 3");
    }
    if (node["r"][0].IsScalar()) {
      dynamics::Vec3 r_vec = node["r"].as<dynamics::Vec3>();
      hom = dynamics::HomFromRvecVec(r_vec, t_vec);
    } else {
      dynamics::Rot rot = node["r"].as<dynamics::Rot>();
      hom = dynamics::HomFromRotVec(rot, t_vec);
    }

    return true;
  }
};

template <>
struct convert<dynamics::Inertia> {
  static bool decode(const YAML::Node& node,
                     dynamics::Inertia& inert) {  // NOLINT[runtime/references]
    if (!node.IsMap()) {
      throw dynamics::ExceptionYAML("dynamics::Inertia comes from a yaml map definition");
    }
    if (node["inertia"] == NULL || !node["inertia"].IsSequence() ||
        node["inertia"].size() != 3) {
      throw dynamics::ExceptionYAML(
          "dynamics::Inertia expects a valid inertia yaml field");
    }
    if (node["vec"] == NULL || !node["vec"].IsSequence() || node["vec"].size() != 3) {
      throw dynamics::ExceptionYAML("dynamics::Inertia expects a valid vec yaml field");
    }

    dynamics::Vec3 inertia_rot_diag = node["inertia"].as<dynamics::Vec3>();
    dynamics::Vec3 vec = node["vec"].as<dynamics::Vec3>();
    inert = dynamics::CreateInertiaMassVecIcmDiag(
        node["mass"].as<double>(0.0), vec, inertia_rot_diag);
    return true;
  }
};

// this can't be a struct convert as it can't return abstract jointbase
void DecodeJointBase(const YAML::Node& node, dynamics::JointBase* joint);

template <>
struct convert<dynamics::JointCoriolis> {
  static bool decode(const YAML::Node& node, dynamics::JointCoriolis& joint) {
    joint = dynamics::JointCoriolis();
    DecodeJointBase(node, &joint);
    return true;
  }
};

template <>
struct convert<dynamics::JointFixed> {
  static bool decode(const YAML::Node& node, dynamics::JointFixed& joint) {
    joint = dynamics::JointFixed();
    DecodeJointBase(node, &joint);

    return true;
  }
};

template <>
struct convert<dynamics::JointFlexus> {
  static bool decode(const YAML::Node& node, dynamics::JointFlexus& joint) {
    if (node["h"]) {
      joint = dynamics::JointFlexus(node["h"].as<double>(0.0));
      DecodeJointBase(node, &joint);
    } else {
      throw dynamics::ExceptionYAML("dynamics::JointFlexus expects a 'h' yaml field");
    }

    return true;
  }
};

template <>
struct convert<dynamics::JointArquus> {
  static bool decode(const YAML::Node& node, dynamics::JointArquus& joint) {
    if (node["h"]) {
      joint = dynamics::JointArquus(node["h"].as<double>(0.0));
      DecodeJointBase(node, &joint);
    } else {
      throw dynamics::ExceptionYAML("dynamics::JointArquus expects a 'h' yaml field");
    }

    return true;
  }
};

template <>
struct convert<dynamics::JointPrism> {
  static bool decode(const YAML::Node& node, dynamics::JointPrism& joint) {
    joint = dynamics::JointPrism();
    DecodeJointBase(node, &joint);

    return true;
  }
};

template <>
struct convert<dynamics::JointRot> {
  static bool decode(const YAML::Node& node, dynamics::JointRot& joint) {
    joint = dynamics::JointRot();
    DecodeJointBase(node, &joint);

    return true;
  }
};

template <>
struct convert<dynamics::Link> {
  static bool decode(const YAML::Node& node, dynamics::Link& link) {
    dynamics::Hom g_prox_joint;
    if (node["g_prox_joint"]) {
      g_prox_joint = node["g_prox_joint"].as<dynamics::Hom>();
    } else {
      throw dynamics::ExceptionYAML("dynamics::Link expects g_prox_joint yaml field");
    }

    if (node["joint_coriolis"]) {
      dynamics::JointCoriolis joint =
          node["joint_coriolis"].as<dynamics::JointCoriolis>();
      link = dynamics::Link(joint, g_prox_joint);
    } else if (node["joint_fixed"]) {
      dynamics::JointFixed joint = node["joint_fixed"].as<dynamics::JointFixed>();
      link = dynamics::Link(joint, g_prox_joint);
    } else if (node["joint_flexus"]) {
      dynamics::JointFlexus joint =
          dynamics::JointFlexus(node["joint_flexus"]["h"].as<double>());
      DecodeJointBase(node["joint_flexus"], &joint);
      link = dynamics::Link(joint, g_prox_joint);
    } else if (node["joint_arquus"]) {
      dynamics::JointArquus joint =
          dynamics::JointArquus(node["joint_arquus"]["h"].as<double>());
      DecodeJointBase(node["joint_arquus"], &joint);
      link = dynamics::Link(joint, g_prox_joint);
    } else if (node["joint_prism"]) {
      dynamics::JointPrism joint = node["joint_prism"].as<dynamics::JointPrism>();
      link = dynamics::Link(joint, g_prox_joint);
    } else if (node["joint_rot"]) {
      dynamics::JointRot joint = node["joint_rot"].as<dynamics::JointRot>();
      link = dynamics::Link(joint, g_prox_joint);
    } else {
      throw dynamics::ExceptionYAML(
          "dynamics::Link expects a valid joint type yaml field");
    }

    if (node["inertia"]) {
      link.set_inertia(node["inertia"].as<dynamics::Inertia>());
    } else {
      throw dynamics::ExceptionYAML(
          "dynamics::Link expects a valid inertia type yaml field");
    }

    return true;
  }
};

template <>
struct convert<dynamics::Arm> {
  static bool decode(const YAML::Node& node, dynamics::Arm& arm) {
    dynamics::Hom g_world_link0;
    if (node["g_world_link0"]) {
      g_world_link0 = node["g_world_link0"].as<dynamics::Hom>();
    } else {
      throw dynamics::ExceptionYAML("dynamics::Arm expects g_world_link0 yaml field");
    }

    if (node["links"] == NULL) {
      throw dynamics::ExceptionYAML("dynamics::Arm expects links yaml field");
    }

    int num_links = (int)node["links"].size();
    if (num_links == 0) {
      throw dynamics::ExceptionYAML("dynamics::Arm expects at least one link");
    }

    arm = dynamics::Arm(g_world_link0);  // this will add a base link
    for (int i = 0; i < num_links; i++) {
      arm.AddLink(node["links"][i].as<dynamics::Link>() /* idx_prox = -1 */);
    }

    return true;
  }
};

}  // namespace YAML

namespace dynamics {

YAML::Node LoadYAMLFilesCommandLine(int argc, char* argv[]);

template <typename T>
T YAMLConvertFromFilestring(std::string filestring) {
  YAML::Node node;
  node = YAML::Load(filestring);
  T t = node.as<T>();
  return t;
}

template <typename T>
T YAMLConvertFromFilename(std::string filename) {
  YAML::Node node;
  try {
    node = YAML::LoadFile(filename);
  } catch (YAML::BadFile& e) {
    std::cout << "Warning: there was a problem with your file.\n";
    throw(e);
  }
  T t = node.as<T>();
  return t;
}

}  // namespace dynamics

#endif  // SRC_DYNAMICS_INCLUDE_DYNAMICS_YAML_H_
