// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_DYNAMICS_INCLUDE_DYNAMICS_JOINT_BASE_H_
#define SRC_DYNAMICS_INCLUDE_DYNAMICS_JOINT_BASE_H_

// This is a base class for joints.
// Each joint must:
// - Inherit from JointBase
// - Implement g_joint_link() method
// - Implement jac() method
// - Implement Clone() method
// - Implement CalcFK() method
// - Implement CalcJac() method
// - Constructor must initialize num_dof_ appropriately

#include <memory>  // needed for JointPtr below

#include "dynamics/types.h"
#include "dynamics/value_ref.h"
#include "dynamics/linear_bounds.h"

namespace dynamics {

class JointBase;
typedef ValueRef<JointBase> JointPtr;  // perhaps should be renamed JointRef

class JointBase {
 public:
  virtual JointBase* Clone() const = 0;
  virtual ~JointBase() {}  // very important

  explicit JointBase(int num_dof);  // used by derived classes

  virtual void CalcFK(void) = 0;  // caculates g_joint_link() return value

  // calculates jac() return value, must call CalcFK first
  virtual void CalcJac(void) = 0;

  // Homegeneous transform from base to top of link
  virtual const Hom& g_joint_link(void) const = 0;

  // Spatial Jacobian for the joint, in link (end of joint) frame
  virtual const Jac& jac(void) const = 0;

  // JointAccApparent: this is S_circ qd =
  //     S_circ = sum over i of [dS/dqi qi_dot]
  //     S_circ = 0 for many joints
  //     see Featherstone, Rigid Body Dynamics Algorithms
  virtual const Twist& JointAccApparent(void) = 0;

  // void calcDerivJacNumeric(void);   // fallback method

  // virtual const Twist& JointVel(void) { return jac() * qd_; }
  // virtual const Twist& JointAcc(void) const { return jac() * qdd_; }
  virtual const Twist& JointVel(void) const;
  virtual const Twist& JointAcc(void) const;

  LinearBounds& limits(void) { return limits_; }

  int num_dof(void) const { return num_dof_; }
  const Vec& tau(void) const { return tau_; }

  void set_q(const Vec& q);
  void set_qd(const Vec& qd);
  void set_qdd(const Vec& qdd);
  void set_tau(const Vec& tau);

  void SetState(const Vec& q, const Vec& qd, const Vec& qdd);
  void SetStateZero(void);

  void SetQZero(void) { q_.setZero(); }
  void SetQdZero(void) { qd_.setZero(); }
  void SetQddZero(void) { qdd_.setZero(); }

  void SetQRandom(void);

  void SetTauFromWrench(const Twist& wrench) { tau_ = jac().transpose() * wrench; }

  void CheckMatSizes(void);

  const Vec& q(void) const { return q_; }
  const Vec& qd(void) const { return qd_; }

  virtual double GravityTrust(const Vec3& /*acc_joint*/,
                              const Vec3& /*acc_link*/,
                              const Vec& /*q*/) {
    return 0.0;
  }

 private:
  int num_dof_ = -1;  // -1 not valid, derived class must call JointBase(num_dof_derived)
  Vec q_ = Vec::Zero(num_dof_);
  Vec qd_ = Vec::Zero(num_dof_);
  Vec qdd_ = Vec::Zero(num_dof_);
  Vec tau_ = Vec::Zero(num_dof_);
  LinearBounds limits_ = LinearBounds(num_dof_);

  // Temporaries to allow return by reference
  mutable Twist joint_vel_ = Twist::Zero(num_dof_);
  mutable Twist joint_acc_ = Twist::Zero(num_dof_);
};

}  // namespace dynamics

#endif  // SRC_DYNAMICS_INCLUDE_DYNAMICS_JOINT_BASE_H_
