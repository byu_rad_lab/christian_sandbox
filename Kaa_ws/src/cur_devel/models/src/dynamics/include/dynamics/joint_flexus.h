// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_DYNAMICS_INCLUDE_DYNAMICS_JOINT_FLEXUS_H_
#define SRC_DYNAMICS_INCLUDE_DYNAMICS_JOINT_FLEXUS_H_

#include "dynamics/types.h"
#include "dynamics/joint_base.h"

namespace dynamics {

class JointFlexus : public JointBase {
 public:
  explicit JointFlexus(double h);

  JointFlexus* Clone() const override { return new JointFlexus(*this); }
  const Twist& JointAccApparent(void) override;

  void CalcFK(void) override;
  const Hom& CalcFKScaled(double scale);

  void CalcJac(void) override;
  const Hom& g_joint_link(void) const override { return g_joint_link_; };
  const Jac& jac(void) const override { return jac_; };
  double GravityTrust(const Vec3& acc_joint,
                            const Vec3& acc_link,
                            const Vec& /*q*/) override;

 private:
  void SetDefaultQLimits(void);
  void CalcJacDeriv(void);
  static const int num_dof_derived_ = 2;
  Hom g_joint_link_ = Hom::Identity();
  Jac jac_ = Jac::Zero(6, num_dof_derived_);
  Twist joint_acc_apparent_ = Twist::Zero();  // never changes

  static constexpr double zero_tol_ = (1.0e-6) * (1.0e-6);

  Jac jac_part_u_ = Jac::Zero(6, num_dof_derived_);
  Jac jac_part_v_ = Jac::Zero(6, num_dof_derived_);
  Jac jac_circ_ = Jac::Zero(6, num_dof_derived_);

  Vec qd_ = Vec::Zero(num_dof_derived_);  // local copy of joint_base.qd_

  Vec3 acc_trust_check_vec;

  double h_;

  double u_;
  double v_;
  double phi_;

  // w = (u, v, 0)
  // wt = w / ||w|| = (ut_, vt_, 0)
  // phi_ = ||w||
  double ut_;       // u_ / phi_
  double vt_;       // v_ / phi_
  double u2_;       // u_^2
  double v2_;       // v_^2
  double ut2_;      // ut_^2
  double vt2_;      // vt_^2
  double sp_;       // sin(phi_)
  double cp_;       // cos(phi_)
  double phi2_;     // phi_ * phi_
  double phi_inv_;  // 1 / phi
  double p2i_;      // 1 / phi_^2
  double p3i_;      // 1 / phi_^3
  double cpm1_;     // cos(phi_) - 1

  Hom g_joint_link_scaled_ = Hom::Identity();
};

}  // namespace dynamics

#endif  // SRC_DYNAMICS_INCLUDE_DYNAMICS_JOINT_FLEXUS_H_
