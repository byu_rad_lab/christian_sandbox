// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_DYNAMICS_INCLUDE_DYNAMICS_TYPES_H_
#define SRC_DYNAMICS_INCLUDE_DYNAMICS_TYPES_H_

#include <eigen3/Eigen/Dense>
#include <stdexcept>
#include <string>

#include "yaml-cpp/yaml.h"

namespace dynamics {

static const int kNumDofMax = 32;

// speed depends on kNumDofMax, 32 faster than 8
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, 0, kNumDofMax, kNumDofMax>
    Mat;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1, 0, kNumDofMax, 1> Vec;

typedef Eigen::Vector2d Vec2;
typedef Eigen::Vector3d Vec3;
typedef Eigen::Vector4d Vec4;
typedef Eigen::Matrix<double, 6, 1> Twist;

typedef Eigen::Matrix3d Rot;
typedef Eigen::Quaterniond Quat;
typedef Eigen::AngleAxisd AngAx;

typedef Eigen::Matrix4d Hom;

typedef Eigen::Matrix<double, 6, Eigen::Dynamic, 0, 6, kNumDofMax> Jac;
typedef Eigen::Matrix3d InertRot;

// Truely variable size matrices and vectors
typedef Eigen::MatrixXd MatXd;
typedef Eigen::VectorXd VecXd;


// Create a common type for both Adj and Inertia
// Keeps SWIG happy
typedef Eigen::Matrix<double, 6, 6> Mat66;
typedef Mat66 Adj;
typedef Mat66 Inertia;

static const double kGravity = 9.81;
Twist kGravityTwist(void);

struct Pose {
 public:
  Vec3 vec = Vec3::Zero();
  Quat quat = Quat::Identity();
};

// typedef Eigen::Matrix<double, 3, 3> InertiaTensor;
Rot RotNormalize(Rot& rot);
Hom HomNormalize(Hom& hom);

// These are cleaner and faster, but are contrary to the style guide.
// void RotNormalize(Rot& rot);
// void HomNormalize(Hom& hom);

// TODO(Tom):  Test these
// Pick a couple easy cases
// Do random (and maybe not random) conversions back and forth
Rot RotFromRvec(const Vec3& ax);    // untested
Vec3 RvecFromQuat(const Quat& q);   // untested
Quat QuatFromRvec(const Vec3& ax);  // untested
Quat QuatFromVec4(const Vec4& vec);

inline Vec3 RvecFromRot(const Rot& rot) { return RvecFromQuat(Quat(rot)); }  // untested
inline Quat QuatFromRot(const Rot& rot) { return Quat(rot); }                // untested

Hom HomFromRotVec(const Rot& rot, const Vec3& vec);

inline Rot RotFromHom(const Hom& g) { return g.topLeftCorner<3, 3>(); }
inline Vec3 VecFromHom(const Hom& g) { return g.topRightCorner<3, 1>(); }
inline Hom HomFromRvecVec(const Vec3& rvec, const Vec3& vec) {
  return HomFromRotVec(RotFromRvec(rvec), vec);
}
inline Vec3 RvecFromHom(const Hom& g) { return RvecFromRot(RotFromHom(g)); }

inline Vec VecFromTwist(const Twist& xi) { return xi.head<3>(); }
inline Vec RvecFromTwist(const Twist& xi) { return xi.tail<3>(); }

Pose PoseFromHom(const Hom& hom);
Hom HomFromPose(const Pose& pose);
Hom HomFromQuatVec(const Quat& q, const Vec3& vec);
Hom HomFromVec4Vec3(const Vec4& vec4, const Vec3& vec3);

Hom HomInv(const Hom& hom);  // untested

// TODO(Tom, Charlie): move tests from kinematics to here
Rot RotScale(const Rot& rot, double scale);
// Returns a rotational equivalent of r1*(1 - scale) + r2*scale
Rot RotInterp(const Rot& r1, const Rot& r2, double scale);
Hom HomScale(const Hom& hom, double scale);  // untested
Hom HomInterp(const Hom& h1, const Hom& h2, double scale);

// Returns the total rotation angle for a given rotation
double RotMag(const Rot& rot);

// Returns the magnitude of a homogeneous transform in meters
// The total rotation angle is converted to meters
double HomMag(const Hom& hom, double meters_per_rad);

double HomError(const Hom& hom1, const Hom& hom2, double meters_per_rad);

Adj AdjFromHom(const Hom& hom);      // untested
Adj AdjInvFromHom(const Hom& hom);   // untested
Adj AdjDualFromHom(const Hom& hom);  // adjoint inverse, transpose, untested

Adj AdjHybridSpatialFromVec(const Vec3& vec);
inline Adj AdjHybridSpatialFromHom(const Hom& hom) {
  return AdjHybridSpatialFromVec(VecFromHom(hom));
}

Hom HomTransFromXYZ(double x, double y, double z);

Vec ClampVecMaxElem(Vec vec, double max_elem);

// hat and vee operators
Rot VecHat(const Vec3& p);
Hom HatTwist(const Vec& xi);           // untested
Twist TwistFromHom(const Hom& hom);    // untested
Hom HomFromTwist(const Twist& twist);  // untested
Adj TwistCross(Twist twist);           // untested

enum class Frame {
  Spatial,
  Body,
  Hybrid,
};

Inertia CreateInertiaMassVecIcm(double mass, Vec3 p, InertRot inert_cm);
Inertia CreateInertiaMassVecIcmDiag(double mass, Vec3 p, Vec3 inert_cm_diag);

class Exception : public std::runtime_error {
 public:
  explicit Exception(const std::string& msg) : std::runtime_error(msg) {}
  ~Exception() throw() {}
};

class ExceptionMatrixSize : public Exception {
 public:
  explicit ExceptionMatrixSize(const std::string& msg) : Exception(msg) {}
  ~ExceptionMatrixSize() throw() {}
};

class ExceptionIndexingError : public Exception {
 public:
  explicit ExceptionIndexingError(const std::string& msg) : Exception(msg) {}
  ~ExceptionIndexingError() throw() {}
};

class ExceptionUsageError : public Exception {
 public:
  explicit ExceptionUsageError(const std::string& msg) : Exception(msg) {}
  ~ExceptionUsageError() throw() {}
};

class ExceptionDofMaxExceeded : public Exception {
 public:
  explicit ExceptionDofMaxExceeded(const std::string& msg) : Exception(msg) {}
  ~ExceptionDofMaxExceeded() throw() {}
};

// function for checking if matrix sizes are correct
template <typename Derived>
void CheckMatSize(const Eigen::MatrixBase<Derived>& mat,
                  const int rows,
                  const int cols = 1) {
  if (mat.rows() != rows || mat.cols() != cols) {
    throw ExceptionMatrixSize("CheckMatSize detected incorrect matrix size.");
  }
}

std::ostream& operator<<(std::ostream& os, const Quat& rhs);
std::ostream& operator<<(std::ostream& os, const Pose& rhs);

// template<typename T>
// T YamlConvert(YAML::Node node) {
//   return node.as<T>();
// }

// // Inputs to these must be block diagonal
// // Not sure they're actually useful.
// // In both cases rot must be skew symetric, so not really a Rot or Hom
Vec3 VeeRot(const Rot& rot);
Twist VeeHom(const Hom& hom);  // Used by QPIK, even though it's not right

// // conversion

// // converts OpenCV Hom to OpenGL Hom (rotation by 180 around x axis)
// Hom HomGLFromCV(const Hom& cv);

// Twist TwistFromHom(const Hom& hom);

// Vec ClampVec(Vec w, double d, bool norm_type = true);

// // Measure of error between rotations, given by trace(I - r1.transpose() * r2)
// double RotError(const Rot& r1, const Rot& r2);

// // Euclidean distance between vectors
// double Vec3Error(const Vec3& v1, const Vec3& v2);

// // Euclidean distance or VecError and error_ratio * RotError
// // error_ratio of 1.0 means unit length is equivalent to 90 degrees
// double HomError(const Hom& g1, const Hom& g2, double error_ratio);

}  // namespace dynamics

#endif  // SRC_DYNAMICS_INCLUDE_DYNAMICS_TYPES_H_
