// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_DYNAMICS_INCLUDE_DYNAMICS_VALUE_REF_H_
#define SRC_DYNAMICS_INCLUDE_DYNAMICS_VALUE_REF_H_

#include <cassert>
#include <memory>

namespace dynamics {

// A class to hold pointers that act like values.
// Allows holding various types derived from a single base class.
// Base clase needs a virtual Clone method
// Holds the object in a unique_ptr, but copies the data when pointer is copied.
// Cannot be nullptr
template <typename T>
class ValueRef {
 public:
  ValueRef() = delete;  // Values can't be null

  // Constructor from ValueRef
  ValueRef(const ValueRef& obj) {
    assert(obj.ptr_.get());
    ptr_.reset(obj.ptr_->Clone());
  }

  // Constructor from T
  explicit ValueRef(const T& obj) { ptr_.reset(obj.Clone()); }

  // Equals operator with ValueRef
  ValueRef& operator=(const ValueRef& obj) {
    assert(obj.ptr_.get());
    ptr_.reset(obj.ptr_->Clone());
    return *this;
  }

  // Equals operator with T
  ValueRef& operator=(const T& obj) {
    ptr_.reset(obj.Clone());
    return *this;
  }

  // Overload pointer operator
  T* operator->() {
    assert(ptr_.get());  // never null
    return ptr_.get();
  }

  const T* operator->() const {
    assert(ptr_.get());  // never null
    return ptr_.get();
  }

  const T* get(void) const {
    assert(ptr_.get());  // never null
    return ptr_.get();
  }

  T* get(void) {
    assert(ptr_.get());  // never null
    return ptr_.get();
  }

 private:
  std::unique_ptr<T> ptr_;
};

}  // namespace dynamics

#endif  // SRC_DYNAMICS_INCLUDE_DYNAMICS_VALUE_REF_H_
