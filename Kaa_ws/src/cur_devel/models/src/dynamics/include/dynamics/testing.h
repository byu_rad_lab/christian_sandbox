// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_DYNAMICS_INCLUDE_DYNAMICS_TESTING_H_
#define SRC_DYNAMICS_INCLUDE_DYNAMICS_TESTING_H_

#include "dynamics/types.h"
#include "eigen3/Eigen/Dense"
#include "gtest/gtest.h"
#include <iostream>

// This has code to test general eigen matrices as well as dynamics types.
// The Eigen code should probably be in primitives::testing.
// The dynamics code is header only (hacked with inline) so that
// dynamics library isn't depended on gtest.

namespace dynamics {

template <typename Derived>
::testing::AssertionResult EigenIsNotNaN(Eigen::MatrixBase<Derived>& mat) {
  if (!(mat == mat)) {
    return ::testing::AssertionFailure() << "EigenIsNotNaN error, mat constains Nan:\n"
                                         << mat << std::endl;
  }
  return ::testing::AssertionSuccess();
}

template <typename Derived>
::testing::AssertionResult EigenIsFinite(Eigen::MatrixBase<Derived>& mat) {
  if (!mat.allFinite()) {
    return ::testing::AssertionFailure() << "EigenIsFinite error, mat is not finite:\n"
                                         << mat << std::endl;
  }
  return ::testing::AssertionSuccess();
}

template <typename Derived>
::testing::AssertionResult EigenValidNumber(Eigen::MatrixBase<Derived>& mat) {
  // check not nan
  ::testing::AssertionResult res1 = EigenIsNotNaN(mat);
  if (res1 == ::testing::AssertionFailure()) {
    return res1;
  }
  // check finite
  ::testing::AssertionResult res2 = EigenIsFinite(mat);
  if (res2 == ::testing::AssertionFailure()) {
    return res2;
  }
  return ::testing::AssertionSuccess();
}

template <typename D1, typename D2>
::testing::AssertionResult EigenEqual(const Eigen::MatrixBase<D1>& a,
                                      const Eigen::MatrixBase<D2>& b,
                                      double tol = 1e-12) {
  if (a.rows() != b.rows()) {
    return ::testing::AssertionFailure() << "a.rows() = " << a.rows()
                                         << " != " << b.rows() << " = b.rows()";
  }
  if (a.cols() != b.cols()) {
    return ::testing::AssertionFailure() << "a.cols() = " << a.cols()
                                         << " != " << b.cols() << " = b.cols()";
  }
  Eigen::MatrixXd diff = a - b;
  double error = diff.lpNorm<Eigen::Infinity>();
  if (error > tol) {
    return ::testing::AssertionFailure() << "EigenEqual:  error " << error << " > tol "
                                         << tol << std::endl
                                         << "\n--- a: ---\n"
                                         << a << "\n\n--- b: ---\n"
                                         << b << "\n\n--- diff: ---\n"
                                         << diff << std::endl;
  }
  return ::testing::AssertionSuccess();
}

inline ::testing::AssertionResult TestRot(Rot rot, double tol = 1e-12) {
  // check finite and is not nan
  ::testing::AssertionResult res = EigenValidNumber(rot);
  if (res == ::testing::AssertionFailure()) {
    return res;
  }
  // check orthogonality
  if (!EigenEqual(rot * rot.transpose(), Rot::Identity(), tol)) {
    return ::testing::AssertionFailure() << "TestRot error, rotation is not orthogonal:\n"
                                         << rot << std::endl;
  }
  return ::testing::AssertionSuccess();
}

::testing::AssertionResult TestHom(Hom hom, double tol = 1e-12) {
  // check finite and is not nan
  ::testing::AssertionResult res1 = EigenValidNumber(hom);
  if (res1 == ::testing::AssertionFailure()) {
    return res1;
  }
  // check rotation
  ::testing::AssertionResult res2 = TestRot(RotFromHom(hom));
  if (res2 == ::testing::AssertionFailure()) {
    return res2;
  }
  // check bottom row
  Vec expected(4);
  expected << 0.0, 0.0, 0.0, 1.0;
  for (int i = 0; i < 4; i++) {
    if (fabs(hom(3, i) - expected(i)) > tol) {
      return ::testing::AssertionFailure() << "TestHom error, hom bottom row incorrect:\n"
                                           << hom << std::endl;
    }
  }
  return ::testing::AssertionSuccess();
}

}  // namespace dynamics

#endif  // SRC_DYNAMICS_INCLUDE_DYNAMICS_TESTING_H_
