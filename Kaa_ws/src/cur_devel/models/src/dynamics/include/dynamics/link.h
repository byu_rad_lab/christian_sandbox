// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_DYNAMICS_INCLUDE_DYNAMICS_LINK_H_
#define SRC_DYNAMICS_INCLUDE_DYNAMICS_LINK_H_

#include <vector>

#include "dynamics/joint_base.h"
#include "dynamics/joint_fixed.h"
#include "dynamics/types.h"

namespace dynamics {

class Link;
typedef std::vector<Link> LinkVec;

class Link {
 public:
  // SWIG also needs this for LinkVec (?)
  // Creates a Link with JointFixed joint
  explicit Link(const Hom& g_prox_joint = Hom::Identity());

  // Standard link constructor
  // In Python, use default, then set_g_prox_joint
  explicit Link(const JointBase& joint, const Hom& g_prox_joint = Hom::Identity());

  // getters
  const JointBase& joint(void) const { return *joint_.get(); }
  JointBase& joint(void) { return *joint_.get(); }

  int prox_idx(void) const { return prox_idx_; }
  int dof_idx_start(void) const { return dof_idx_start_; }
  int dof_idx_end(void) const { return dof_idx_end_; }
  const Hom& g_world_link(void) const { return g_world_link_; }
  const Adj& adj_world_link(void) const { return adj_world_link_; }
  const Adj& adj_link_world(void) const { return adj_link_world_; }
  const Hom& g_world_joint(void) const { return g_world_joint_; }
  const Hom& g_prox_joint(void) const { return g_prox_joint_; }
  const Twist& vel(void) const { return vel_; }
  const Twist& acc(void) const { return acc_; }
  const Twist& wrench(void) const { return wrench_; }
  const Inertia& inertia(void) const { return inertia_; }

  // for testings
  const Adj& adj_link_prox(void) const { return adj_link_prox_; }

  // Setters
  void set_prox_idx(int idx) { prox_idx_ = idx; }
  void set_dof_idx_start(int idx);
  void set_g_prox_joint(const Hom& g) { g_prox_joint_ = g; }
  void set_inertia(Inertia inertia) { inertia_ = inertia; }
  void set_wrench_ext(const Twist& w) { wrench_ext_ = w; }

  void SetLinkProxRefs(LinkVec* links);

  void SetQRandom(void) { joint_->SetQRandom(); }
  void SetExtWrenchZero(void) { wrench_ext_.setZero(); }

  // Given full vector of arm state variable, picks the bits it needs
  void SetQFullVec(const Vec& q);
  void SetQdFullVec(const Vec& qd);
  void SetQddFullVec(const Vec& qdd);

  // Given pointer to vector, populates correct bit
  void GetTauFullVec(Vec* tau);
  void GetQFullVec(Vec* tau);

  const Twist& GetJointWrench(void) const;

  void CalcFKLink0(void);
  void CalcFK(void);

  // Populates the appropriate columns of jac_spatial
  void CalcJac(Jac* jac_spatial);

  void CalcDynSetup(void);

  void CalcID(void);
  void CalcIDStatic(void);
  void CalcIDDelta(int idx);  // make private?

  // Propogates forces proximally, calculates torques
  void CalcIDPassTwo(void);

  // Convenience method for setting link inertia
  void SetInertiaMassVecIcmDiag(double mass, Vec3 p, Vec3 inert_cm_diag);
  void SetInertiaZero(void) { inertia_.setZero(); }

 private:
  JointPtr joint_ = JointPtr(JointFixed());

  Link* link_prox_ = nullptr;

  int prox_idx_ = -1;  // index of proximal link
  int dof_idx_start_ = 0;
  int dof_idx_end_ = 0;

  Hom g_prox_joint_ = Hom::Identity();  // proximal link frame to joint frame

  Inertia inertia_ = Inertia::Identity();  // spatial inertia

  // calculated by Link
  Hom g_world_joint_ = Hom::Identity();
  Hom g_world_link_ = Hom::Identity();
  Hom g_prox_link_ = Hom::Identity();

  Adj adj_world_joint_ = Adj::Identity();
  Adj adj_world_link_ = Adj::Identity();
  Adj adj_link_world_ = Adj::Identity();
  Adj adj_dual_prox_link_ = Adj::Identity();
  Adj adj_link_prox_ = Adj::Identity();

  // all are of link, in link frame
  Twist vel_ = Twist::Zero();         // velocity of link
  Twist acc_ = Twist::Zero();         // acclerlation of link
  Twist wrench_ = Twist::Zero();      // force/torque from previous link, acting on link
  Twist wrench_ext_ = Twist::Zero();  // force/torque from external source

  Twist gravity_ = kGravityTwist();
  Twist gravity_link_ = Twist::Zero();  // gravity in the link frame

  // data to avoid allocating temporaries
  Twist joint_vel_ = Twist::Zero();
  Adj vel_cross_ = Adj::Zero();  // TwistCross(vel_)
  mutable Twist joint_wrench_ = Twist::Zero();

  // Vec tau_;  // kept in joint, to avoid resizing
};

}  // namespace dynamics

#endif  // SRC_DYNAMICS_INCLUDE_DYNAMICS_LINK_H_
