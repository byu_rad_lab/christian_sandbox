// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_DYNAMICS_INCLUDE_DYNAMICS_GRAVITY_SOLVER_H_
#define SRC_DYNAMICS_INCLUDE_DYNAMICS_GRAVITY_SOLVER_H_

#include <vector>
#include <memory>

#include "nlopt.hpp"

#include "dynamics/types.h"
#include "dynamics/joint_base.h"

namespace dynamics {

typedef std::vector<double> DoubleVec;

class GravitySolver {
 public:
  explicit GravitySolver(const JointPtr joint_ptr);
  explicit GravitySolver(const JointBase& joint);

  GravitySolver(const GravitySolver& other) : joint_(other.joint_) { Initialize(); }

  GravitySolver& operator=(const GravitySolver& other) {
    joint_ = other.joint_;
    Initialize();
    return *this;
  }

  JointBase* joint(void) { return joint_.get(); }

  const Vec& Solve(const Vec3& acc_joint, const Vec3& acc_link, const Vec& q);
  double TrustCoeff(const Vec3& acc_joint, const Vec3& acc_link, const Vec& q);
  double trust_coef(void) const { return trust_coef_; }

 private:
  void Initialize(void);

  void UpdateBounds(void);

  static double CostCaller(const DoubleVec& x, DoubleVec& /*grad*/, void* f_data);
  double CostFunc(const DoubleVec& x);

 private:
  JointPtr joint_;

  nlopt::opt opt_;

  Vec3 acc_joint_;
  Vec3 acc_link_;
  Vec soln_;
  double trust_coef_;

  unsigned int feval_;
  DoubleVec x_;

  // temporary variables
  Rot rot_joint_link_;
  Vec3 acc_link_calc_;
  Vec3 err_;
};

}  // namespace dynamics

#endif  // SRC_DYNAMICS_INCLUDE_DYNAMICS_GRAVITY_SOLVER_H_
