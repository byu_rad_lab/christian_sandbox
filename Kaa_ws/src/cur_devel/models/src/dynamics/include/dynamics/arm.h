// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_DYNAMICS_INCLUDE_DYNAMICS_ARM_H_
#define SRC_DYNAMICS_INCLUDE_DYNAMICS_ARM_H_

#include <vector>

#include "dynamics/joint_base.h"
#include "dynamics/linear_bounds.h"
#include "dynamics/link.h"
#include "dynamics/types.h"

namespace dynamics {

typedef std::vector<double> IntegState;

class Arm {
 public:
  // constructors
  explicit Arm(Hom g_world_link0 = Hom::Identity());
  Arm(const Arm& other);

  Arm& operator=(const Arm& other);

  // getters
  int num_dof(void) const { return num_dof_; }
  int num_links(void) const { return links_.size(); }
  const Vec& q(void) const { return q_; }
  const Vec& qd(void) const { return qd_; }
  const Vec& qdd(void) const { return qdd_; }
  const Vec& tau(void) const { return tau_; }
  const LinkVec& links(void) const { return links_; }
  const Jac& jac_spatial(void) const { return jac_spatial_; }
  const Mat& inertia_mat(void) const { return inertia_mat_; }
  const LinearBounds& limits(void) const { return limits_; }

  LinkVec& links(void) { return links_; }

  const Link& GetLink(int link_num) const;  // allows Python-style negative indexing
  Link& GetLink(int link_num);              // allows Python-style negative indexing

  const Hom& GWorldLink(int link_num);

  // setters
  void set_q(const Vec& q);
  void set_qd(const Vec& qd);
  void set_qdd(const Vec& qdd);

  void set_g_world_link0(Hom g);  // actually sets links_[0].g_prox_link

  // Convenience methods, set q then call
  void SetState(Vec q, Vec qd, Vec qdd);

  void SetQAndCalcFK(const Vec& q);
  void SetQAndCalcJac(const Vec& q);

  void SetQZero(void);
  void SetQdZero(void);
  void SetQddZero(void);
  void SetStateZero(void);

  void SetQRandom(void);    // uses joint.limits method
  void SetQdRandom(void);   // uniform distribution (-1, 1)
  void SetQddRandom(void);  // uniform distribution (-1, 1)
  void SetStateRandom(void);

  void SetExtWrenchZero(void);

  // functions
  void AddLink(const Link link_new, int idx_prox = -1);
  void AddLinkTool(Hom g_link_tool);  // link with fixed joint
  void Resize(void);
  void UpdateReferences(void);  // call if Arm is copied
  void UpdateLimits(void);  // call if Arm is copied
  void CalcFK(void);
  void CalcJac(void);

  // given q, qd, qdd, calc tau
  Vec CalcID(void);

  // given q, assumes qd = qdd = 0, returns joint torques
  Vec CalcIDStatic(void);

  // given tau (joint torques), q, and qd, calculates and returns qdd
  Vec CalcFD(const Vec& tau);
  // Vec CalcFD(void);

  void CalcInertiaMat(void);

  size_t Integrate(const Vec& tau, double dt);
  size_t IntegrateFast(const Vec& tau, double dt);
  size_t IntegrateStable(const Vec& tau, double dt);

  //  Get Jacobians in Body, Hybrid, Spatial frame, with off set from link/tool frame
  Jac JacLink(int link_num, Frame frame, const Hom& g_link_jac = Hom::Identity()) const;
  Jac JacLink(int link_num) const;  // body Jacobian of link frame

  void CalcDynSetup(void);

 private:
  // Function for Integrate to call
  void ODE(const IntegState& x,
           IntegState& dxdt,     // NOLINT(runtime/references)
           const double /*t*/);  // NOLINT(runtime/references)

  // Used as part of CalcFD
  Vec CalcIDQddZero(void);

  // given q, calc tau assuming qd = qdd = 0
  Vec CalcIDDelta(int idx);

  // Propogates wrenches distally, calculates torques
  void CalcIDPassTwo(void);

 private:  // data
  LinkVec links_;
  int num_dof_ = 0;

  Vec q_ = Vec::Zero(0);
  Vec qd_ = Vec::Zero(0);
  Vec qdd_ = Vec::Zero(0);
  Vec tau_ = Vec::Zero(0);

  Vec tau_c_ = Vec::Zero(0);  // C in tau = H * qdd + C

  Vec tau_int_ = Vec::Zero(0);  // for Integrate method

  Jac jac_spatial_ = Jac::Zero(6, 0);

  Mat inertia_mat_;  // H in tau = H * qdd + C

  LinearBounds limits_ = LinearBounds(0);

  double damping_coef_numeric = 0.1;  // heuristic for forward Euler integration
};

}  // namespace dynamics

#endif  // SRC_DYNAMICS_INCLUDE_DYNAMICS_ARM_H_
