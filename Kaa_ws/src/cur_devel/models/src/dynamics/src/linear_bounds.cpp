// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include "dynamics/linear_bounds.h"

#include <cassert>

namespace dynamics {

LinearBounds::LinearBounds(double num_dof) : num_dof_(num_dof) {
  SetLimitsScalar(1.57);  // reasonable default
}

void LinearBounds::SetLimitsScalar(double limit) {
  SetLimitsVector(-limit * Vec::Ones(num_dof_), limit * Vec::Ones(num_dof_));
}

void LinearBounds::SetLimitsVector(const Vec& box_lower, const Vec& box_upper) {
  if (box_lower.size() != num_dof_) {
    throw ExceptionMatrixSize("box_lower is the wrong size.");
  }
  if (box_upper.size() != num_dof_) {
    throw ExceptionMatrixSize("box_upper is the wrong size.");
  }

  box_lower_ = box_lower;
  box_upper_ = box_upper;

  a_.resize(2 * num_dof_, num_dof_);
  a_.topRows(num_dof_) = -MatXd::Identity(num_dof_, num_dof_);
  a_.bottomRows(num_dof_) = MatXd::Identity(num_dof_, num_dof_);

  b_.resize(2 * num_dof_);
  b_.head(num_dof_) = -box_lower;
  b_.tail(num_dof_) = box_upper;
}

VecXd LinearBounds::CheckLimits(const Vec& q) { return a_ * q - b_; }

double LinearBounds::CheckLimitsWorst(const Vec& q) {
  assert(a_.rows() == b_.size());
  VecXd diff = CheckLimits(q);
  return diff.maxCoeff();
}

void LinearBounds::SetLimitsCircular(double radius,
                                     int num_constraints,
                                     Vec center,
                                     double phase) {
  if (num_dof_ != 2) {
    throw ExceptionMatrixSize("SetLimitsCircular only works in R2");
  }
  if (center.size() == 0) {
    center = Vec::Zero(num_dof_);
  }
  if (center.size() != num_dof_) {
    throw ExceptionMatrixSize("center has the wrong number of elements");
  }

  box_lower_ = center - radius * Vec::Ones(num_dof_);
  box_upper_ = center + radius * Vec::Ones(num_dof_);

  double nc = num_constraints;

  a_.resize(nc, num_dof_);
  b_.resize(nc);

  double theta;
  for (int i = 0; i < nc; i++) {
    theta = 2.0 * M_PI * static_cast<double>(i) / static_cast<double>(nc) + phase;
    a_.row(i) << cos(theta), sin(theta);
  }
  b_ = radius * VecXd::Ones(nc) + a_ * center;
}

Vec LinearBounds::GetQRandom(void) {
  assert(a_.cols() == num_dof_);
  assert(a_.rows() == b_.size());
  Vec q = Vec(num_dof_);
  if (num_dof_ == 0) {
    return q.setZero();
  }
  assert((box_lower_ - box_upper_).maxCoeff() <= 0.0);
  double worst_limit = 1.0;  // out of bounds
  while (worst_limit >= 0.0) {
    // set the angles randomly between 0 and 1
    // then add the average plus q times the spread
    q.setRandom();
    q = 0.5 * (box_upper_ + box_lower_ + (q.cwiseProduct(box_upper_ - box_lower_)));

    worst_limit = CheckLimitsWorst(q);
  }
  return q;
}

}  // namespace dynamics
