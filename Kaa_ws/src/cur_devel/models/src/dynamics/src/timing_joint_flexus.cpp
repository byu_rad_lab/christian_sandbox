// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <stdio.h>
#include <iostream>
#include <string>

#include "dynamics/types.h"
#include "dynamics/joint_flexus.h"
#include "dynamics/eigen_bench_timer.h"

using std::cout;
using std::endl;
using dynamics::Vec2;
using dynamics::Vec3;
using dynamics::Vec;
using dynamics::Hom;
using dynamics::JointFlexus;

namespace dyn = dynamics;

void PrintTimer(Eigen::BenchTimer timer, int reps, std::string name) {
  printf(
      "%15s: %8.3f ns\n", name.c_str(), timer.best() / static_cast<double>(reps) * 1e9);
}

// three functions
// just set joints randomly
// set random and calc sin, cos
// set random and joint fk

class Jacobains {
 public:
  EIGEN_DONT_INLINE void CalcJac(void) {
    q_.setRandom();
    joint.set_q(q_);
    joint.CalcFK();
    joint.CalcJac();
  }

 private:
  const double height = 0.20;
  JointFlexus joint = JointFlexus(height);
  Vec q_ = Vec(2);
};

class ForwardKinematics {
 public:
  EIGEN_DONT_INLINE void CalcFK(void) {
    q_.setRandom();
    // q_ *= 3.14159 * 1000.0;
    joint.set_q(q_);
    joint.CalcFK();
    // hom_total_ += joint.g_joint_link();
  }

  Hom hom_total(void) { return hom_total_; }

 private:
  const double height = 0.20;
  JointFlexus joint = JointFlexus(height);
  Vec q_ = Vec(2);
  Hom hom_total_ = Hom::Zero();
};

class RandomSinCos {
 public:
  EIGEN_DONT_INLINE void CalcSinCos(void) {
    q_.setRandom();
    phi_ = sqrt(q_(0) * q_(0) + q_(1) * q_(1));
    sp_ = sin(phi_);
    cp_ = cos(phi_);
  }

 private:
  Vec q_ = Vec(2);
  double phi_;
  double sp_;
  double cp_;
};

class RandomGenerate {
 public:
  EIGEN_DONT_INLINE void CalcRandom(void) { q_.setRandom(); }

 private:
  Vec q_ = Vec(2);
};

int main() {
  Jacobains jac;
  ForwardKinematics fk;
  RandomGenerate rg;
  RandomSinCos sc;

  int reps = static_cast<int>(3e3);
  int tries = static_cast<int>(3e3);

  cout << "Starting FK timing." << endl;

  Eigen::BenchTimer timer_random;
  BENCH(timer_random, tries, reps, rg.CalcRandom());
  PrintTimer(timer_random, reps, "CalcRandom");

  Eigen::BenchTimer timer_sincos;
  BENCH(timer_sincos, tries, reps, sc.CalcSinCos());
  PrintTimer(timer_sincos, reps, "CalcSinCos");

  Eigen::BenchTimer timer_fk;
  BENCH(timer_fk, tries, reps, fk.CalcFK());
  PrintTimer(timer_fk, reps, "CalcFK");

  Eigen::BenchTimer timer_jac;
  BENCH(timer_jac, tries, reps, jac.CalcJac());
  PrintTimer(timer_jac, reps, "CalcJac");

  std::cout << "FK hom_total:\n" << fk.hom_total() << std::endl;

  return 0;
}
