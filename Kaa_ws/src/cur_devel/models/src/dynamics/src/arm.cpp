// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include "dynamics/arm.h"

#include <cassert>
#include <functional>

#include <boost/numeric/odeint.hpp>

#include "dynamics/joint_fixed.h"

#include "primitives/stringprintf.h"

using boost::numeric::odeint::integrate;

namespace dynamics {

// constructor
Arm::Arm(Hom g_world_link0) {
  Link link = Link(JointFixed());  // create a base link
  links_.push_back(link);
  set_g_world_link0(g_world_link0);  // don't put in initializer list
}

Arm::Arm(const Arm& other) {
  links_ = LinkVec(other.links_);
  num_dof_ = other.num_dof_;
  Resize();
  UpdateReferences();
  SetState(other.q_, other.qd_, other.qdd_);
  CalcJac();
}

Arm& Arm::operator=(const Arm& other) {
  links_ = LinkVec(other.links_);
  num_dof_ = other.num_dof_;
  Resize();
  UpdateReferences();
  SetState(other.q_, other.qd_, other.qdd_);
  CalcJac();
  return *this;
}

// setters
void Arm::set_q(const Vec& q) {
  if (q.rows() != num_dof_) {
    throw ExceptionMatrixSize(primitives::stringsprintf(
        "Error in set_q: wrong vector length. Got %d, expected %d.", q.rows(), num_dof_));
  }
  q_ = q;
  for (auto link = (links_.begin() + 1); link != links_.end(); ++link) {
    link->SetQFullVec(q_);
  }
}

void Arm::set_qd(const Vec& qd) {
  if (qd.rows() != num_dof_) {
    throw ExceptionMatrixSize(primitives::stringsprintf(
        "Error in set_qd: wrong vector length. Got %d, expected %d.",
        qd.rows(),
        num_dof_));
  }
  qd_ = qd;
  for (auto link = (links_.begin() + 1); link != links_.end(); ++link) {
    link->SetQdFullVec(qd_);
  }
}

void Arm::set_qdd(const Vec& qdd) {
  if (qdd.rows() != num_dof_) {
    throw ExceptionMatrixSize(primitives::stringsprintf(
        "Error in set_qdd: wrong vector length. Got %d, expected %d.",
        qdd.rows(),
        num_dof_));
  }
  qdd_ = qdd;
  for (auto link = (links_.begin() + 1); link != links_.end(); ++link) {
    link->SetQddFullVec(qdd_);
  }
}

void Arm::SetState(Vec q, Vec qd, Vec qdd) {
  set_q(q);
  set_qd(qd);
  set_qdd(qdd);
}

const Link& Arm::GetLink(int link_num) const {
  // Accomodate Python style indexing from back
  if (link_num < 0) {
    link_num += links_.size();
  }

  if (link_num < 0 || link_num >= static_cast<int>(links_.size())) {
    throw ExceptionIndexingError(
        "Error in Arm::link: requested link number isn't valid.");
  }

  return links_[link_num];
}

// Ugly, but perhaps the cleanest way to have const and non-const versions
// of this function without duplicating the code.
// Based on Scott Meyers Effective C++, via:
//   http://stackoverflow.com/questions/123758/
//   how-do-i-remove-code-duplication-between-similar-const-and-non-const-member-func
Link& Arm::GetLink(int link_num) {
  return const_cast<Link&>(static_cast<const Arm&>(*this).GetLink(link_num));
}

const Hom& Arm::GWorldLink(int link_num) { return GetLink(link_num).g_world_link(); }

void Arm::set_g_world_link0(Hom g) {
  links_[0].set_g_prox_joint(g);
  links_[0].CalcFKLink0();
}

void Arm::AddLink(const Link link_new, int prox_idx) {
  // The first link should have been added by Arm c'tor
  assert(links_.size() > 0);

  // Accomodate Python style indexing from back
  if (prox_idx < 0) {
    prox_idx += links_.size();
  }
  assert(prox_idx >= 0);
  assert(prox_idx < static_cast<int>(links_.size()));

  // Add the link
  links_.push_back(link_new);
  Link& link = links_.back();

  if (link.joint().num_dof() + num_dof_ > kNumDofMax) {
    throw ExceptionDofMaxExceeded("kNumDofMax exceeded.  Can't add link.");
  }

  // TODO: this is probably incorrect for branched kinematic chains
  // Should probably be last link before this one was added.
  int idx_start = links_.at(prox_idx).dof_idx_end();
  link.set_prox_idx(prox_idx);
  link.set_dof_idx_start(idx_start);

  num_dof_ = link.dof_idx_end();
  Resize();
  UpdateReferences();
}

void Arm::AddLinkTool(Hom g_link_tool) {
  Link link = Link(JointFixed(), g_link_tool);  // create a base link
  AddLink(link);
}

void Arm::Resize(void) {
  q_.resize(num_dof_);
  qd_.resize(num_dof_);
  qdd_.resize(num_dof_);

  tau_.setZero(num_dof_);
  tau_c_.setZero(num_dof_);
  jac_spatial_.setZero(6, num_dof_);
  inertia_mat_.setZero(num_dof_, num_dof_);

  SetStateZero();
  UpdateLimits();
}

void Arm::UpdateReferences(void) {
  for (auto& link : links_) {
    link.SetLinkProxRefs(&links_);
  }
}

void Arm::UpdateLimits(void) {
  int nc = 0;  // number of columns
  int nr = 0;  // number of rows
  for (auto& link : links_) {
    nr += link.joint().limits().a().rows();
    nc += link.joint().limits().a().cols();
  }
  assert(nc == num_dof_);

  MatXd a_full = MatXd::Zero(nr, nc);
  VecXd b_full = VecXd::Zero(nr);
  VecXd box_lower = VecXd::Zero(nc);
  VecXd box_upper = VecXd::Zero(nc);

  nc = 0;
  nr = 0;
  for (auto& link : links_) {
    const LinearBounds& lims = link.joint().limits();
    MatXd a = link.joint().limits().a();
    VecXd b = link.joint().limits().b();
    a_full.block(nr, nc, a.rows(), a.cols()) = a;
    b_full.segment(nr, b.rows()) = b;
    box_lower.segment(nc, a.cols()) = lims.box_lower();
    box_upper.segment(nc, a.cols()) = lims.box_upper();

    nr += link.joint().limits().a().rows();
    nc += link.joint().limits().a().cols();
  }
  limits_.set_a(a_full);
  limits_.set_b(b_full);
  limits_.set_box_lower(box_lower);
  limits_.set_box_upper(box_upper);
}

void Arm::CalcFK(void) {
  // Skip link 0, set and calc for the rest
  for (auto link = (links_.begin() + 1); link != links_.end(); ++link) {
    link->CalcFK();
  }
}

void Arm::SetQAndCalcFK(const Vec& q) {
  set_q(q);
  CalcFK();
}

void Arm::CalcJac(void) {
  CalcFK();
  for (auto link = (links_.begin() + 1); link != links_.end(); ++link) {
    link->CalcJac(&jac_spatial_);
  }
}

void Arm::SetQAndCalcJac(const Vec& q) {
  set_q(q);
  CalcJac();
}

void Arm::SetQZero() {
  q_.setZero();
  set_q(q_);
}

void Arm::SetQdZero() {
  qd_.setZero();
  set_qd(qd_);
}

void Arm::SetQddZero() {
  qdd_.setZero();
  set_qdd(qdd_);
}

void Arm::SetStateZero() {
  SetQZero();
  SetQdZero();
  SetQddZero();
}

// calls joint SetQRandom, then repopulates q_ with that data
void Arm::SetQRandom() {
  for (auto link = (links_.begin() + 1); link != links_.end(); ++link) {
    link->SetQRandom();
    link->GetQFullVec(&q_);
  }
}

void Arm::SetQdRandom() {
  qd_.setRandom();
  set_qd(qd_);
}

void Arm::SetQddRandom() {
  qdd_.setRandom();
  set_qdd(qdd_);
}

void Arm::SetStateRandom() {
  SetQRandom();
  SetQdRandom();
  SetQddRandom();
}

void Arm::SetExtWrenchZero(void) {
  for (auto& link : links_) {
    link.SetExtWrenchZero();
  }
}

void Arm::CalcDynSetup(void) {
  CalcJac();
  for (auto& link : links_) {
    link.CalcDynSetup();
  }
}

void Arm::CalcIDPassTwo(void) {
  for (auto link = (links_.end() - 1); link != links_.begin(); --link) {
    link->CalcIDPassTwo();
    link->GetTauFullVec(&tau_);
  }
}

Vec Arm::CalcID(void) {
  CalcDynSetup();
  for (auto& link : links_) {
    link.CalcID();
  }

  CalcIDPassTwo();
  return tau_;
}

Vec Arm::CalcIDQddZero(void) {
  Vec qdd_temp = qdd_;
  // not much win in replacing this with Link::CalcIDQddZero version
  SetQddZero();
  CalcID();           // populates tau_
  set_qdd(qdd_temp);  // restore qdd
  return tau_;
}

Vec Arm::CalcIDStatic(void) {
  CalcDynSetup();
  for (auto& link : links_) {
    link.CalcIDStatic();
  }

  CalcIDPassTwo();
  return tau_;
}

Vec Arm::CalcFD(const Vec& tau) {
  if (tau.rows() != num_dof_) {
    throw ExceptionMatrixSize(primitives::stringsprintf(
        "Error in CalcFD: wrong vector length. Got %d, expected %d.",
        tau.rows(),
        num_dof_));
  }
  tau_c_ = CalcIDQddZero();
  CalcInertiaMat();
  // tau = H * qdd + C
  // qdd = H_inv (tau - C)
  set_qdd(inertia_mat_.ldlt().solve(tau - tau_c_));
  return qdd_;
}

void Arm::ODE(const IntegState& x, IntegState& dxdt, const double /*t*/) {
  set_q(Eigen::Map<const Vec>(x.data(), num_dof_));
  set_qd(Eigen::Map<const Vec>(x.data() + num_dof_, num_dof_));
  qdd_ = CalcFD(tau_int_);
  for (int i = 0; i < num_dof_; i++) {
    dxdt[i] = x[i + num_dof_];
    dxdt[i + num_dof_] = qdd_[i];
  }
}
size_t Arm::Integrate(const Vec& tau, double dt) { return IntegrateStable(tau, dt); }

size_t Arm::IntegrateFast(const Vec& tau, double dt) {
  if (tau.rows() != num_dof_) {
    throw ExceptionMatrixSize(primitives::stringsprintf(
        "Error in Integrate: wrong vector length. Got %d, expected %d.",
        tau.rows(),
        num_dof_));
  }
  tau_int_ = tau;

  qdd_ = CalcFD(tau) - damping_coef_numeric * sqrt(dt) * qd_;
  q_ += 0.5 * qd_ * dt;
  qd_ += qdd_ * dt;
  q_ += 0.5 * qd_ * dt;
  set_q(q_);
  set_qd(qd_);
  CalcID();  // populates link acc
  return 1;
}

size_t Arm::IntegrateStable(const Vec& tau, double dt) {
  if (tau.rows() != num_dof_) {
    throw ExceptionMatrixSize(primitives::stringsprintf(
        "Error in Integrate: wrong vector length. Got %d, expected %d.",
        tau.rows(),
        num_dof_));
  }
  tau_int_ = tau;

  IntegState dxdt(2 * num_dof_);
  IntegState x(q_.data(), q_.data() + q_.size());
  for (int i = 0; i < num_dof_; i++) {
    x.push_back(qd_[i]);
  }
  auto f = [this](const IntegState& x, IntegState& dxdt, const double t) {
    this->ODE(x, dxdt, t);
  };
  CalcID();  // populates link acc
  return integrate(f, x, 0.0, dt, dt);
}

// Calculate delta torque caused by unit acceleration of single dof.
// No gravity, qd = 0
Vec Arm::CalcIDDelta(int idx) {
  for (auto link = (links_.begin() + 1); link != links_.end(); ++link) {
    link->CalcIDDelta(idx);
  }

  CalcIDPassTwo();
  return tau_;
}

// Calculates H in tau = H * qdd + C
// CalcDynSetup must be called first
void Arm::CalcInertiaMat(void) {
  for (int i = 0; i < num_dof_; i++) {
    inertia_mat_.row(i) = CalcIDDelta(i);
  }
}

Jac Arm::JacLink(int link_num) const {
  const Link& link = GetLink(link_num);
  return link.adj_link_world() * jac_spatial_.leftCols(link.dof_idx_end());
}

Jac Arm::JacLink(int link_num, Frame frame, const Hom& g_link_jac) const {
  const Link& link = GetLink(link_num);
  Hom g_world_jac = link.g_world_link() * g_link_jac;
  int num_dof = link.dof_idx_end();
  switch (frame) {
    case Frame::Spatial:
      return jac_spatial_.leftCols(num_dof);
    case Frame::Body:
      return AdjInvFromHom(g_world_jac) * jac_spatial_.leftCols(num_dof);
    case Frame::Hybrid:
      return AdjHybridSpatialFromHom(g_world_jac) * jac_spatial_.leftCols(num_dof);
  }
  // Should never get here, but keeps the compiler happy.
  throw ExceptionUsageError("Frame type not handled.");
}

}  // namespace dynamics
