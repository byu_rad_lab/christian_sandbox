// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <stdio.h>
#include <iostream>
#include "dynamics/types.h"
#include "dynamics/joint_rot.h"
#include "dynamics/joint_fixed.h"
#include "dynamics/link.h"
#include "dynamics/arm.h"

using std::cout;
using std::endl;
using dynamics::Vec3;
using dynamics::Vec;
using dynamics::Hom;
using dynamics::JointRot;
using dynamics::JointFixed;
using dynamics::Link;
using dynamics::Arm;

namespace dyn = dynamics;

int main() {
  // make an arm
  // rotational joints, arrayed along x-axis
  // link 0 at x = 0.25
  // link 1 at x = 1.00
  // link 2 at x = 1.50

  JointRot joint = JointRot();

  Vec3 vec;
  Hom g_prox_joint;
  double dx0 = 0.25;
  vec = Vec3(dx0, 0.0, 0.0);
  Hom g_world_link0 = dyn::HomFromRvecVec(Vec3::Zero(), vec);
  Arm arm = Arm(g_world_link0);

  vec = Vec3(0.0, 0.0, 0.0);
  g_prox_joint = dyn::HomFromRvecVec(Vec3::Zero(), vec);
  Link link1 = Link(joint, g_prox_joint);

  vec = Vec3(0.75, 0.0, 0.0);
  g_prox_joint = dyn::HomFromRvecVec(Vec3::Zero(), vec);
  Link link2 = Link(joint, g_prox_joint);

  arm.AddLink(link1);
  arm.AddLink(link2);

  vec = Vec3(0.5, 0.0, 0.0);
  Hom g_link_tool = dyn::HomFromRvecVec(Vec3::Zero(), vec);
  arm.AddLink(dyn::Link(dyn::JointFixed(), g_link_tool));
  arm.AddLinkTool(g_link_tool);

  Vec q;
  q = Vec::Zero(2);

  cout << "q:\n" << q << endl;
  cout << "arm.num_dof(): " << arm.num_dof() << endl;

  arm.set_q(q);
  arm.CalcJac();

  dyn::LinkVec& links = arm.links();
  for (unsigned int i = 0; i < links.size(); i++) {
    Link& link = links.at(i);
    printf("link %d:\n", i);
    cout << "link.g_world_link:\n" << link.g_world_link() << endl;
  }
  cout << "\narm.jac_spatial():\n" << arm.jac_spatial() << endl;

  q << M_PI / 2.0, 0.0;
  arm.set_q(q);
  arm.CalcJac();

  for (unsigned int i = 0; i < links.size(); i++) {
    Link& link = links.at(i);
    printf("link %d:\n", i);
    cout << "link.g_world_link:\n" << link.g_world_link() << endl;
  }

  Vec tau = arm.CalcIDStatic();
  cout << "tau:\n" << tau << endl;

  return 0;
}
