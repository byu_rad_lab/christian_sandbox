// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <stdio.h>
#include <iostream>
#include <string>

#include "dynamics/types.h"
#include "dynamics/joint_flexus.h"
#include "dynamics/link.h"
#include "dynamics/arm.h"
#include "dynamics/eigen_bench_timer.h"

using std::cout;
using std::endl;
using dynamics::Vec3;
using dynamics::Vec;
using dynamics::Hom;
using dynamics::JointFlexus;
using dynamics::Link;
using dynamics::Arm;

namespace dyn = dynamics;

void PrintTimer(Eigen::BenchTimer timer, int reps, std::string name) {
  printf(
      "%15s: %6.2f us\n", name.c_str(), timer.best() / static_cast<double>(reps) * 1e6);
}

int main() {
  double height = 0.20;
  double link_length = 0.30;
  JointFlexus joint = JointFlexus(height);

  Vec3 vec;
  Hom g_prox_joint = dyn::HomTransFromXYZ(0.0, 0.0, link_length);
  Arm arm = Arm(g_prox_joint);

  Link link = Link(joint, g_prox_joint);

  for (int i = 0; i < 4; i++) {
    arm.AddLink(link);
  }
  cout << "Adding ToolLink" << endl;

  arm.AddLinkTool(g_prox_joint);
  arm.SetStateRandom();

  Vec tau = Vec(arm.num_dof());
  tau.setRandom();

  int reps = static_cast<int>(1e3);
  int tries = static_cast<int>(1e2);

  cout << "Starting FK timing." << endl;

  Eigen::BenchTimer timer_fk;
  BENCH(timer_fk, tries, reps, arm.CalcFK());
  PrintTimer(timer_fk, reps, "CalcFK");

  Eigen::BenchTimer timer_jac;
  BENCH(timer_jac, tries, reps, arm.CalcJac());
  PrintTimer(timer_jac, reps, "CalcJac");

  Eigen::BenchTimer timer_id_static;
  BENCH(timer_id_static, tries, reps, arm.CalcIDStatic());
  PrintTimer(timer_id_static, reps, "CalcIDStatic");

  Eigen::BenchTimer timer_id;
  BENCH(timer_id, tries, reps, arm.CalcID());
  PrintTimer(timer_id, reps, "CalcID");

  Eigen::BenchTimer timer_fd;
  BENCH(timer_fd, tries, reps, arm.CalcFD(tau));
  PrintTimer(timer_fd, reps, "CalcFD");

  return 0;
}
