// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <stdio.h>

#include <iostream>

#include "yaml-cpp/yaml.h"

#include "dynamics/joint_base.h"
#include "dynamics/joint_flexus.h"
#include "dynamics/joint_rot.h"

#include "dynamics/arm.h"
#include "dynamics/link.h"
#include "dynamics/types.h"
#include "dynamics/yaml.h"

using std::cout;
using std::endl;

int main(int argc, char* argv[]) {
  YAML::Node node = dynamics::LoadYAMLFilesCommandLine(argc, argv);

  // Vec3
  dynamics::Vec3 vec3 = node["r_vec_default"].as<dynamics::Vec3>();
  cout << vec3(0) << endl;

  // Rot
  dynamics::Rot rot = node["r_mat_identity"].as<dynamics::Rot>();
  cout << rot(0, 0) << endl;

  // Hom
  dynamics::Hom hom = node["hom_identity"].as<dynamics::Hom>();
  cout << hom(0, 0) << endl;

  // Inertia
  dynamics::Inertia inertia = node["inertia_default"].as<dynamics::Inertia>();
  cout << inertia(0, 0) << endl;

  // Arm
  dynamics::Arm arm = node["arm"].as<dynamics::Arm>();

  // Link
  dynamics::LinkVec& links = arm.links();
  cout << "number of links in arm: " << links.size() << endl;

  dynamics::Link joint_rot_link = links.at(1);
  cout << joint_rot_link.g_prox_joint()(2, 3) << endl;  // 0.2

  dynamics::JointRot& joint_rot = (dynamics::JointRot&)joint_rot_link.joint();
  dynamics::LinearBounds& lb1 = joint_rot.limits();
  cout << lb1.box_lower() << endl;

  dynamics::Link joint_flexus_link = links.at(2);
  cout << joint_flexus_link.g_prox_joint()(2, 3) << endl;  // 0.3

  dynamics::JointFlexus& joint_flexus = (dynamics::JointFlexus&)joint_flexus_link.joint();
  dynamics::LinearBounds& lb2 = joint_flexus.limits();

  cout << lb2.box_lower() << endl;

  return 0;
}
