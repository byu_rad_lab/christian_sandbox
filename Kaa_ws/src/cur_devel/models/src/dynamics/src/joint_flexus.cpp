// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include "dynamics/joint_flexus.h"

namespace dynamics {

const double kOneSixth = 1.0 / 6.0;

JointFlexus::JointFlexus(double h) : JointBase(num_dof_derived_), h_(h) {
  SetDefaultQLimits();
}

void JointFlexus::SetDefaultQLimits(void) {
  double r = 1.57;
  int num_constraints = 16;
  limits().SetLimitsCircular(r, num_constraints);
}

void JointFlexus::CalcFK(void) {
  u_ = q()(0);
  v_ = q()(1);

  u2_ = u_ * u_;
  v2_ = v_ * v_;

  phi2_ = u2_ + v2_;
  phi_ = sqrt(phi2_);
  phi_inv_ = 1.0 / phi_;
  p2i_ = 1.0 / phi2_;
  p3i_ = phi_inv_ * p2i_;

  sp_ = sin(phi_);
  cp_ = cos(phi_);
  cpm1_ = cp_ - 1.0;

  ut_ = u_ * phi_inv_;
  vt_ = v_ * phi_inv_;
  ut2_ = u2_ * p2i_;
  vt2_ = v2_ * p2i_;

  if (phi2_ > zero_tol_) {
    g_joint_link_(0, 0) = cpm1_ * vt2_ + 1.0;
    g_joint_link_(0, 1) = -cpm1_ * ut_ * vt_;
    g_joint_link_(0, 2) = vt_ * sp_;
    g_joint_link_(0, 3) = -cpm1_ * h_ * vt_ * phi_inv_;

    g_joint_link_(1, 0) = -cpm1_ * ut_ * vt_;
    g_joint_link_(1, 1) = cpm1_ * ut2_ + 1.0;
    g_joint_link_(1, 2) = -ut_ * sp_;
    g_joint_link_(1, 3) = cpm1_ * h_ * ut_ * phi_inv_;

    g_joint_link_(2, 0) = -vt_ * sp_;
    g_joint_link_(2, 1) = ut_ * sp_;
    g_joint_link_(2, 2) = cp_;
    g_joint_link_(2, 3) = h_ * sp_ * phi_inv_;

  } else {
    g_joint_link_(0, 0) = 1.0 - 0.5 * v2_;
    g_joint_link_(0, 1) = 0.5 * u_ * v_;
    g_joint_link_(0, 2) = v_;
    g_joint_link_(0, 3) = 0.5 * h_ * v_;

    g_joint_link_(1, 0) = 0.5 * u_ * v_;
    g_joint_link_(1, 1) = 1.0 - 0.5 * u2_;
    g_joint_link_(1, 2) = -u_;
    g_joint_link_(1, 3) = -0.5 * h_ * u_;

    g_joint_link_(2, 0) = -v_;
    g_joint_link_(2, 1) = u_;
    g_joint_link_(2, 2) = 1.0 - 0.5 * (u2_ + v2_);
    g_joint_link_(2, 3) = h_ * (1.0 - kOneSixth * (u2_ + v2_));
  }

  g_joint_link_(3, 0) = 0.0;
  g_joint_link_(3, 1) = 0.0;
  g_joint_link_(3, 2) = 0.0;
  g_joint_link_(3, 3) = 1.0;
}

const Hom& JointFlexus::CalcFKScaled(double scale) {
  double u = q()(0) * scale;
  double v = q()(1) * scale;
  double h = h_ * scale;

  double u2 = u * u;
  double v2 = v * v;

  double phi2 = u2 + v2;
  double phi = sqrt(phi2);
  double phi_inv = 1 / phi;
  double phi2_inv = 1 / phi2;

  double sp = sin(phi);
  double cp = cos(phi);
  double cpm1 = cp - 1;

  double ut = u * phi_inv;
  double vt = v * phi_inv;
  double ut2 = u2 * phi2_inv;
  double vt2 = v2 * phi2_inv;

  if (phi2 > zero_tol_) {
    g_joint_link_scaled_(0, 0) = cpm1 * vt2 + 1.0;
    g_joint_link_scaled_(0, 1) = -cpm1 * ut * vt;
    g_joint_link_scaled_(0, 2) = vt * sp;
    g_joint_link_scaled_(0, 3) = -cpm1 * h * vt * phi_inv;

    g_joint_link_scaled_(1, 0) = -cpm1 * ut * vt;
    g_joint_link_scaled_(1, 1) = cpm1 * ut2 + 1.0;
    g_joint_link_scaled_(1, 2) = -ut * sp;
    g_joint_link_scaled_(1, 3) = cpm1 * h * ut * phi_inv;

    g_joint_link_scaled_(2, 0) = -vt * sp;
    g_joint_link_scaled_(2, 1) = ut * sp;
    g_joint_link_scaled_(2, 2) = cp;
    g_joint_link_scaled_(2, 3) = h * sp * phi_inv;

    g_joint_link_scaled_(3, 0) = 0.0;
    g_joint_link_scaled_(3, 1) = 0.0;
    g_joint_link_scaled_(3, 2) = 0.0;
    g_joint_link_scaled_(3, 3) = 1.0;
  } else {
    g_joint_link_scaled_.setIdentity();
    g_joint_link_scaled_(2, 3) = h;
  }
  return g_joint_link_scaled_;
}

void JointFlexus::CalcJac(void) {
  double cpm1ophi2 = cp_ * p2i_ - p2i_;
  if (phi2_ > zero_tol_) {
    jac_(0, 0) = 0.0;
    // jac_(1, 0) = -cpm1ophi2 * h_;
    jac_(1, 0) = cpm1ophi2 * h_;
    jac_(2, 0) = h_ * ut_ * (phi_ - sp_) * p2i_;
    jac_(3, 0) = ut2_ * ut2_ + ut2_ * vt2_ + vt2_ * sp_ * phi_inv_;
    jac_(4, 0) = ut_ * vt_ * (phi_ - sp_) * phi_inv_;
    // jac_(5, 0) = cpm1_ * vt_ * phi_inv_;
    jac_(5, 0) = -cpm1_ * vt_ * phi_inv_;

    // jac_(0, 1) = cpm1ophi2 * h_;
    jac_(0, 1) = -cpm1ophi2 * h_;
    jac_(1, 1) = 0.0;
    jac_(2, 1) = h_ * vt_ * (phi_ - sp_) * p2i_;
    jac_(3, 1) = ut_ * vt_ * (phi_ - sp_) * phi_inv_;
    jac_(4, 1) = ut2_ * vt2_ + vt2_ * vt2_ + ut2_ * sp_ * phi_inv_;
    // jac_(5, 1) = -cpm1_ * ut_ * phi_inv_;
    jac_(5, 1) = cpm1_ * ut_ * phi_inv_;
  } else {
    jac_(0, 0) = 0.0;
    // jac_(1, 0) = 0.5 * h_;
    jac_(1, 0) = -0.5 * h_;
    jac_(2, 0) = kOneSixth * u_ * h_;
    jac_(3, 0) = 1.0 - kOneSixth * v2_;
    jac_(4, 0) = kOneSixth * u_ * v_;
    // jac_(5, 0) = -0.5 * v_;
    jac_(5, 0) = 0.5 * v_;

    // jac_(0, 1) = -0.5 * h_;
    jac_(0, 1) = 0.5 * h_;
    jac_(1, 1) = 0.0;
    jac_(2, 1) = kOneSixth * v_ * h_;
    jac_(3, 1) = kOneSixth * u_ * v_;
    jac_(4, 1) = 1.0 - kOneSixth * u2_;
    // jac_(5, 1) = 0.5 * u_;
    jac_(5, 1) = -0.5 * u_;
  }
}

const Twist& JointFlexus::JointAccApparent(void) {
  CalcJacDeriv();
  qd_ = qd();  // get qd from joint_base
  jac_circ_ = jac_part_u_ * qd_[0] + jac_part_v_ * qd_[1];
  joint_acc_apparent_ = jac_circ_ * qd_;
  return joint_acc_apparent_;
}

void JointFlexus::CalcJacDeriv(void) {
  if (phi2_ > zero_tol_) {
    // jac_part_u_ = partials wrt u
    // column of jac_ for u
    jac_part_u_(0, 0) = 0.0;
    // jac_part_u_(1, 0) = h_ * ut_ * (2.0 * cpm1_ + phi_ * sp_) * p3i_;
    jac_part_u_(1, 0) = -h_ * ut_ * (2.0 * cpm1_ + phi_ * sp_) * p3i_;
    jac_part_u_(2, 0) =
        -h_ * (phi_ * (ut2_ * cp_ + 2.0 * ut2_ - 1.0) - (3.0 * ut2_ - 1.0) * sp_) * p3i_;
    jac_part_u_(3, 0) = ut_ * (phi_ * (-4.0 * ut2_ * ut2_ - 4.0 * ut2_ * vt2_ +
                                       4.0 * ut2_ + vt2_ * cp_ + 2.0 * vt2_) -
                               3.0 * vt2_ * sp_) *
                        p2i_;
    jac_part_u_(4, 0) =
        -vt_ * (phi_ * (ut2_ * cp_ + 2.0 * ut2_ - 1.0) - (3.0 * ut2_ - 1.0) * sp_) * p2i_;
    // jac_part_u_(5, 0) = -ut_ * vt_ * (2.0 * cpm1_ + phi_ * sp_) * p2i_;
    jac_part_u_(5, 0) = ut_ * vt_ * (2.0 * cpm1_ + phi_ * sp_) * p2i_;

    // column of jac_ for v
    // jac_part_u_(0, 1) = -h_ * ut_ * (2.0 * cpm1_ + phi_ * sp_) * p3i_;
    jac_part_u_(0, 1) = h_ * ut_ * (2.0 * cpm1_ + phi_ * sp_) * p3i_;
    jac_part_u_(1, 1) = 0.0;
    jac_part_u_(2, 1) = -h_ * ut_ * vt_ * (phi_ * (cpm1_ + 3.0) - 3.0 * sp_) * p3i_;
    jac_part_u_(3, 1) =
        -vt_ * (phi_ * (ut2_ * cp_ + 2.0 * ut2_ - 1.0) - (3.0 * ut2_ - 1.0) * sp_) * p2i_;
    jac_part_u_(4, 1) =
        -ut_ * (phi_ * (4.0 * ut2_ * vt2_ - ut2_ * cp_ + 4.0 * vt2_ * vt2_ - 2.0 * vt2_) +
                (3.0 * ut2_ - 2.0) * sp_) *
        p2i_;
    jac_part_u_(5, 1) =
        -(-cpm1_ + phi_ * ut2_ * sp_ + 2.0 * ut2_ * cp_ - 2.0 * ut2_) * p2i_;
    // (-cpm1_ + phi_ * ut2_ * sp_ + 2.0 * ut2_ * cp_ - 2.0 * ut2_) * p2i_;

    // jac_part_v_ = partials wrt v
    // column of jac_ for u
    jac_part_v_(0, 0) = 0.0;
    // jac_part_v_(1, 0) = h_ * vt_ * (2.0 * cpm1_ + phi_ * sp_) * p3i_;
    jac_part_v_(1, 0) = -h_ * vt_ * (2.0 * cpm1_ + phi_ * sp_) * p3i_;
    jac_part_v_(2, 0) = -h_ * ut_ * vt_ * (phi_ * (cpm1_ + 3.0) - 3.0 * sp_) * p3i_;
    jac_part_v_(3, 0) =
        -vt_ * (phi_ * (4.0 * ut2_ * ut2_ + 4.0 * ut2_ * vt2_ - 2.0 * ut2_ - vt2_ * cp_) +
                (3.0 * vt2_ - 2.0) * sp_) *
        p2i_;
    jac_part_v_(4, 0) =
        -ut_ * (phi_ * (vt2_ * cp_ + 2.0 * vt2_ - 1.0) - (3.0 * vt2_ - 1.0) * sp_) * p2i_;
    jac_part_v_(5, 0) =
        -(cpm1_ - phi_ * vt2_ * sp_ - 2.0 * vt2_ * cp_ + 2.0 * vt2_) * p2i_;
    // (cpm1_ - phi_ * vt2_ * sp_ - 2.0 * vt2_ * cp_ + 2.0 * vt2_) * p2i_;

    // column of jac_ for v
    // jac_part_v_(0, 1) = -h_ * vt_ * (2.0 * cpm1_ + phi_ * sp_) * p3i_;
    jac_part_v_(0, 1) = h_ * vt_ * (2.0 * cpm1_ + phi_ * sp_) * p3i_;
    jac_part_v_(1, 1) = 0.0;
    jac_part_v_(2, 1) =
        -h_ * (phi_ * (vt2_ * cp_ + 2.0 * vt2_ - 1.0) - (3.0 * vt2_ - 1.0) * sp_) * p3i_;
    jac_part_v_(3, 1) =
        -ut_ * (phi_ * (vt2_ * cp_ + 2.0 * vt2_ - 1.0) - (3.0 * vt2_ - 1.0) * sp_) * p2i_;
    jac_part_v_(4, 1) = vt_ * (phi_ * (-4.0 * ut2_ * vt2_ + ut2_ * cp_ + 2.0 * ut2_ -
                                       4.0 * vt2_ * vt2_ + 4.0 * vt2_) -
                               3.0 * ut2_ * sp_) *
                        p2i_;
    // jac_part_v_(5, 1) = ut_ * vt_ * (2.0 * cpm1_ + phi_ * sp_) * p2i_;
    jac_part_v_(5, 1) = -ut_ * vt_ * (2.0 * cpm1_ + phi_ * sp_) * p2i_;
  } else {
    jac_part_u_.setZero();
    jac_part_v_.setZero();

    jac_part_u_(2, 0) = h_ * kOneSixth;
    // jac_part_u_(5, 1) = 0.5;
    // jac_part_v_(5, 0) = -0.5;
    jac_part_u_(5, 1) = -0.5;
    jac_part_v_(5, 0) = 0.5;
    jac_part_v_(2, 1) = h_ * kOneSixth;
  }
}

double JointFlexus::GravityTrust(const Vec3& acc_joint,
                               const Vec3& acc_link,
                               const Vec& /*q*/) {
      acc_trust_check_vec(0) = acc_joint(0) - acc_link(0);
      acc_trust_check_vec(1) = acc_joint(1) - acc_link(1);
      acc_trust_check_vec(2) = acc_joint(2) + acc_link(2);
      return acc_trust_check_vec.norm() / (acc_joint.norm() + acc_link.norm() + 1e-16);
}

}  // namespace dynamics
