// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
#include <fstream>
#include <sstream>

#include "dynamics/yaml.h"

namespace YAML {

// this can't be a struct convert as it can't return abstract jointbase
void DecodeJointBase(const YAML::Node& node, dynamics::JointBase* joint) {
  if (node["limits"] == NULL) {
    return;
  }

  dynamics::LinearBounds& joint_limits = joint->limits();
  if (node["limits"].IsScalar()) {
    joint_limits.SetLimitsScalar(node["limits"].as<double>(1.57));
  } else if (node["limits"].IsMap() && node["limits"]["box_upper"] != NULL &&
             node["limits"]["box_lower"] != NULL) {
    dynamics::Vec box_upper = node["limits"]["box_upper"].as<dynamics::Vec>();
    dynamics::Vec box_lower = node["limits"]["box_lower"].as<dynamics::Vec>();
    joint_limits.SetLimitsVector(box_lower, box_upper);
  } else if (node["limits"].IsMap() && node["limits"]["circular"] != NULL) {
    YAML::Node circular_limits = node["limits"]["circular"];
    if (circular_limits["radius"] != NULL && circular_limits["phase"] &&
        circular_limits["num_constraints"] && circular_limits["center"]) {
      double radius = circular_limits["radius"].as<double>(M_PI / 2.0);
      double phase = circular_limits["phase"].as<double>(0.0);
      int num_constraints = circular_limits["num_constraints"].as<int>(8);
      dynamics::Vec center = circular_limits["center"].as<dynamics::Vec>();
      joint_limits.SetLimitsCircular(radius, num_constraints, center, phase);
    } else {
      throw dynamics::ExceptionYAML("dynamics::JointBase found invalid circular_limits");
    }
  }

  joint->CheckMatSizes();  // ensure matrix sizes are right
}

}  // namespace YAML

namespace dynamics {

YAML::Node LoadYAMLFilesCommandLine(int argc, char* argv[]) {
  int number_of_file_names = argc - 1;
  if (argc <= 1) {
    std::cout << "Error: you need at least one YAML filename\n";
    throw;
  }
  printf("Loading: %d yaml files\n", number_of_file_names);

  YAML::Node result;
  std::stringstream file_str_buffer;

  for (int i = 1; i < argc; ++i) {
    std::ifstream temp(argv[i]);
    file_str_buffer << temp.rdbuf() << "\n\n";
  }

  try {
    result = YAML::Load(file_str_buffer.str());
  } catch (YAML::BadFile& e) {
    std::cout << "Error: there was a problem with your files.\n";
    throw e;
  }

  return result;
}

}  // namespace dynamics
