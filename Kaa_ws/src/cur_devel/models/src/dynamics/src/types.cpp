// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include "dynamics/types.h"

#include <cassert>

namespace dynamics {

Twist kGravityTwist(void) {
  Twist gravity = Twist::Zero();
  gravity[2] = -kGravity;
  return gravity;
}

Rot RotNormalize(Rot& rot) {
  Quat q = QuatFromRot(rot);
  q.normalize();
  return q.matrix();
}

Hom HomNormalize(Hom& hom) {
  Quat q = QuatFromRot(RotFromHom(hom));
  Vec3 vec = VecFromHom(hom);
  q.normalize();
  return HomFromQuatVec(q, vec);
}

// void RotNormalize(Rot& rot) {
//   Quat q = QuatFromRot(rot);
//   q.normalize();
//   rot = q.matrix();
// }

// void HomNormalize(Hom& hom) {
//   Quat q = QuatFromRot(RotFromHom(hom));
//   q.normalize();
//   hom.topLeftCorner<3, 3>() = q.matrix();
// }

Rot RotFromRvec(const Vec3& w) {
  double th = sqrt(w[0] * w[0] + w[1] * w[1] + w[2] * w[2]);
  if (th < 1e-9) {
    return Rot::Identity() + VecHat(w);
  } else {
    Vec3 w2 = w / th;
    Rot wh = VecHat(w2);
    return Rot::Identity() + wh * sin(th) + wh * wh * (1.0 - cos(th));
  }
}

Vec3 RvecFromQuat(const Quat& q) {
  AngAx aa(q);
  if (aa.angle() > M_PI) {
    return (aa.angle() - 2.0 * M_PI) * aa.axis();
  } else {
    return aa.angle() * aa.axis();
  }
}

Quat QuatFromRvec(const Vec3& ax) {
  double mag_half = 0.5 * ax.norm();
  if (mag_half < 1e-6) {
    return Quat(cos(mag_half), ax[0], ax[1], ax[2]);
  } else {
    double smh = sin(mag_half);
    Vec3 axn = ax.normalized();
    return Quat(cos(mag_half), smh * axn[0], smh * axn[1], smh * axn[2]);
  }
}

Quat QuatFromVec4(const Vec4& vec) {
  Quat q;
  q.w() = vec[0];
  q.x() = vec[1];
  q.y() = vec[2];
  q.z() = vec[3];
  return q;
}

Hom HomFromRotVec(const Rot& rot, const Vec3& vec) {
  Hom hom = Hom::Identity();
  hom.topLeftCorner<3, 3>() = rot;
  hom.topRightCorner<3, 1>() = vec;
  return hom;
}

Pose PoseFromHom(const Hom& hom) {
  Pose pose;
  pose.quat = QuatFromRot(RotFromHom(hom));
  pose.vec = VecFromHom(hom);
  return pose;
}

Hom HomFromPose(const Pose& pose) { return HomFromRotVec(pose.quat.matrix(), pose.vec); }

Hom HomFromQuatVec(const Quat& q, const Vec3& vec) {
  return HomFromRotVec(q.matrix(), vec);
}

Hom HomFromVec4Vec3(const Vec4& vec4, const Vec3& vec3) {
  return HomFromQuatVec(QuatFromVec4(vec4), vec3);
}

Hom HomInv(const Hom& hom) {
  Rot rot = RotFromHom(hom);
  Vec3 p = VecFromHom(hom);
  return HomFromRotVec(rot.transpose(), -rot.transpose() * p);
}

Rot RotScale(const Rot& rot, double scale) {
  Vec3 rvec = RvecFromRot(rot);
  return RotFromRvec(scale * rvec);
}

// Returns a rotational equivalent of r1*(1 - scale) + r2*scale
Rot RotInterp(const Rot& r1, const Rot& r2, double scale) {
  Rot r_delta = r1.transpose() * r2;
  return r1 * RotScale(r_delta, scale);
}

Hom HomScale(const Hom& hom, double scale) {
  Vec3 rvec = RvecFromRot(RotFromHom(hom));
  Vec3 vec = VecFromHom(hom);
  return HomFromRvecVec(scale * rvec, scale * vec);
}

Hom HomInterp(const Hom& h1, const Hom& h2, double scale) {
  Hom h_delta = HomInv(h1) * h2;
  return h1 * HomScale(h_delta, scale);
}

double RotMag(const Rot& rot) {
  return RvecFromRot(rot).norm();
}

double HomMag(const Hom& hom, double meters_per_rad) {
  double rot_mag = meters_per_rad * RotMag(RotFromHom(hom));
  double vec_mag = VecFromHom(hom).norm();
  return sqrt(rot_mag * rot_mag + vec_mag * vec_mag);
}

double HomError(const Hom& hom1, const Hom& hom2, double meters_per_rad) {
  return HomMag(HomInv(hom1) * hom2, meters_per_rad);
}

Adj AdjFromHom(const Hom& hom) {
  Rot rot = RotFromHom(hom);
  Vec3 p = VecFromHom(hom);
  Rot phat = VecHat(p);

  Adj adj;
  adj << rot, phat * rot, Rot::Zero(), rot;
  return adj;
}

Adj AdjInvFromHom(const Hom& hom) {
  Rot RT = RotFromHom(hom).transpose();
  Vec3 p = VecFromHom(hom);
  Rot phat = VecHat(p);

  Adj adj_inv;
  adj_inv << RT, -RT * phat, Rot::Zero(), RT;
  return adj_inv;
}

Adj AdjHybridSpatialFromVec(const Vec3& vec) {
  Adj adj_hs = Adj::Identity();
  adj_hs.topRightCorner<3, 3>() = VecHat(-vec);
  return adj_hs;
}

Adj AdjDualFromHom(const Hom& hom) {
  Rot rot = RotFromHom(hom);
  Vec3 p = VecFromHom(hom);
  Rot p_hat = VecHat(p);

  Adj adj;
  adj << rot, Rot::Zero(), p_hat * rot, rot;
  return adj;
}

Hom HomTransFromXYZ(double x, double y, double z) {
  return HomFromRvecVec(Vec3::Zero(), Vec3(x, y, z));
}

Vec ClampVecMaxElem(Vec vec, double max_elem) {
  assert(max_elem > 1e-12);
  double max_found = vec.cwiseAbs().maxCoeff();
  if (max_found <= max_elem) {
    return vec;
  } else {
    return vec * max_elem / max_found;
  }
}

Rot VecHat(const Vec3& p) {
  Rot phat;
  phat << 0, -p(2), p(1), p(2), 0, -p(0), -p(1), p(0), 0;
  return phat;
}

Hom HatTwist(const Vec& xi) {
  Hom xi_hat;
  xi_hat << 0.0, -xi(5), xi(4), xi(0),  //
      xi(5), 0.0, -xi(3), xi(1),        //
      -xi(4), xi(3), 0, xi(2),          //
      0.0, 0.0, 0.0, 0.0;               //
  return xi_hat;
}

Twist TwistFromHom(const Hom& hom) {
  Rot rot = RotFromHom(hom);
  Vec3 p = VecFromHom(hom);
  double cos_th = (rot.trace() - 1.0) / 2.0;
  double th;
  Vec3 v, w;

  if (cos_th > 1.0) {
    th = 0.0;
  } else if (cos_th < -1.0) {
    th = M_PI;
  } else {
    th = acos(cos_th);
  }

  Twist out;
  if (th < 1e-6) {
    // TODO(Tom): Use small angle approximation here.
    w << 0.0, 0.0, 0.0;
    v = p;
    out << v(0), v(1), v(2), w(0), w(1), w(2);
  } else {
    w << (rot(2, 1) - rot(1, 2)), (rot(0, 2) - rot(2, 0)), (rot(1, 0) - rot(0, 1));
    w = w / (2.0 * sin(th));
    Rot wh = VecHat(w);
    Rot A = ((Rot::Identity() - rot) * wh) + w * w.transpose() * th;

    // You really do need this inversion, see MLS p. 43
    v = A.colPivHouseholderQr().solve(p);

    w = w.transpose() * th;
    v = v.transpose() * th;
    out << v(0), v(1), v(2), w(0), w(1), w(2);
  }
  return out;
}

Hom HomFromTwist(const Twist& twist) {
  Rot rot;
  Vec3 p;
  Vec3 w = RvecFromTwist(twist);
  double rot_mag = w.norm();
  if (rot_mag < 1e-9) {
    rot.setIdentity();
    p = VecFromTwist(twist);
  } else {
    Vec3 w1 = w / rot_mag;
    Vec3 v1 = VecFromTwist(twist) / rot_mag;
    rot = RotFromRvec(w);
    Rot w_hat = VecHat(w1);
    // Rot temp = (Rot::Identity() - rot) * w_hat + w1 * w1.transpose() * rot_mag;
    // p = temp * v1;
    p = ((Rot::Identity() - rot) * w_hat + w1 * w1.transpose() * rot_mag) * v1;
  }
  return HomFromRotVec(rot, p);
}

Adj TwistCross(Twist twist) {
  Rot p_hat = VecHat(twist.head<3>());
  Rot w_hat = VecHat(twist.tail<3>());
  Adj twist_cross = Adj::Zero();
  twist_cross.topLeftCorner<3, 3>() = w_hat;
  twist_cross.bottomRightCorner<3, 3>() = w_hat;
  twist_cross.topRightCorner<3, 3>() = p_hat;
  return twist_cross;
}

Vec3 VeeRot(const Rot& rot) {
  Vec3 v;
  v << -rot(1, 2), rot(0, 2), -rot(0, 1);
  return v;
}

Twist VeeHom(const Hom& hom) {
  Twist t;
  t << hom(0, 3), hom(1, 3), hom(2, 3), -hom(1, 2), hom(0, 2), -hom(0, 1);
  return t;
}

Inertia CreateInertiaMassVecIcm(double mass, Vec3 p, InertRot inert_cm) {
  Rot p_hat = VecHat(p);

  Inertia inertia;
  inertia.topLeftCorner<3, 3>() = mass * InertRot::Identity();
  inertia.topRightCorner<3, 3>() = mass * p_hat.transpose();
  inertia.bottomLeftCorner<3, 3>() = mass * p_hat;
  inertia.bottomRightCorner<3, 3>() = inert_cm + mass * p_hat * p_hat.transpose();
  return inertia;
}

Inertia CreateInertiaMassVecIcmDiag(double mass, Vec3 p, Vec3 inert_cm_diag) {
  InertRot inert_cm = InertRot(inert_cm_diag.asDiagonal());
  return CreateInertiaMassVecIcm(mass, p, inert_cm);
}

// // Rotation bit not checked, be careful
// Inertia CreateInertiaMassHomIcm(double mass, Hom g_body_icm, InertRot inert_cm) {
//   Vec3 p = g_body_icm.topRightCorner<3, 1>();
//   Rot rot = g_body_icm.topLeftCorner<3, 3>();
//   InertRot inert_rot = rot * inert_cm * rot.transpose();  // might not be right
//   return CreateInertiaMassVecIcm(mass, p, inert_rot);
// }

std::ostream& operator<<(std::ostream& os, const Quat& rhs) {
  os << " " << rhs.w() << " " << rhs.x() << " " << rhs.y() << " " << rhs.z();
  return os;
}

std::ostream& operator<<(std::ostream& os, const Pose& rhs) {
  os << "quat:" << rhs.quat << std::endl;
  os << "vec: " << rhs.vec.transpose();
  return os;
}

}  // namespace dynamics
