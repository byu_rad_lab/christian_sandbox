// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include "dynamics/joint_arquus.h"

namespace dynamics {

const double kOneSixth = 1.0 / 6.0;

JointArquus::JointArquus(double h) : JointBase(num_dof_derived_), h_(h) {
  SetDefaultQLimits();
}

void JointArquus::SetDefaultQLimits(void) {
  double r = 1.57;
  limits().SetLimitsScalar(r);
}

void JointArquus::CalcFK(void) {
  th_ = q()(0);

  sth_ = sin(th_);
  cth_ = cos(th_);
  thi_ = 1.0 / th_;
  th2_ = th_ * th_;
  cthm1_ = cth_ - 1.0;

  if (th2_ > zero_tol_) {
    g_joint_link_(0, 0) = 1.0;
    g_joint_link_(0, 1) = 0.0;
    g_joint_link_(0, 2) = 0.0;
    g_joint_link_(0, 3) = 0.0;

    g_joint_link_(1, 0) = 0.0;
    g_joint_link_(1, 1) = cth_;
    g_joint_link_(1, 2) = -sth_;
    g_joint_link_(1, 3) = cthm1_ * h_ * thi_;

    g_joint_link_(2, 0) = 0.0;
    g_joint_link_(2, 1) = sth_;
    g_joint_link_(2, 2) = cth_;
    g_joint_link_(2, 3) = h_ * sth_ * thi_;

    g_joint_link_(3, 0) = 0.0;
    g_joint_link_(3, 1) = 0.0;
    g_joint_link_(3, 2) = 0.0;
    g_joint_link_(3, 3) = 1.0;

  } else {
    g_joint_link_(0, 0) = 1.0;
    g_joint_link_(0, 1) = 0.0;
    g_joint_link_(0, 2) = 0.0;
    g_joint_link_(0, 3) = 0.0;

    g_joint_link_(1, 0) = 0.0;
    g_joint_link_(1, 1) = 1.0;
    g_joint_link_(1, 2) = -th_;
    g_joint_link_(1, 3) = -0.5 * h_ * th_;

    g_joint_link_(2, 0) = 0.0;
    g_joint_link_(2, 1) = th_;
    g_joint_link_(2, 2) = 1.0;
    g_joint_link_(2, 3) = h_;

    g_joint_link_(3, 0) = 0.0;
    g_joint_link_(3, 1) = 0.0;
    g_joint_link_(3, 2) = 0.0;
    g_joint_link_(3, 3) = 1.0;
  }

  g_joint_link_(3, 0) = 0.0;
  g_joint_link_(3, 1) = 0.0;
  g_joint_link_(3, 2) = 0.0;
  g_joint_link_(3, 3) = 1.0;
}

const Hom& JointArquus::CalcFKScaled(double scale) {
  double u = q()(0) * scale;
  double ui = 1.0 / u;
  double h = h_ * scale;
  double su = sin(u);
  double cu = cos(u);

  double cthm1 = cu - 1;

  if (u * u > zero_tol_) {
    g_joint_link_scaled_(0, 0) = 1.0;
    g_joint_link_scaled_(0, 1) = 0.0;
    g_joint_link_scaled_(0, 2) = 0.0;
    g_joint_link_scaled_(0, 3) = 0.0;

    g_joint_link_scaled_(1, 0) = 0.0;
    g_joint_link_scaled_(1, 1) = cu;
    g_joint_link_scaled_(1, 2) = -su;
    g_joint_link_scaled_(1, 3) = cthm1 * h * ui;

    g_joint_link_scaled_(2, 0) = 0.0;
    g_joint_link_scaled_(2, 1) = su;
    g_joint_link_scaled_(2, 2) = cu;
    g_joint_link_scaled_(2, 3) = h * su * ui;

    g_joint_link_scaled_(3, 0) = 0.0;
    g_joint_link_scaled_(3, 1) = 0.0;
    g_joint_link_scaled_(3, 2) = 0.0;
    g_joint_link_scaled_(3, 3) = 1.0;
  } else {
    g_joint_link_scaled_.setIdentity();
    g_joint_link_scaled_(2, 3) = h;
  }
  return g_joint_link_scaled_;
}

void JointArquus::CalcJac(void) {
  // th2i_ = thi_ * thi_;
  th2i_ = 1.0 / th2_;
  if (th2_ > zero_tol_) {
    jac_(0, 0) = 0.0;
    // jac_(1, 0) = cthm1_ * h_ * th2i_;  // subtract before divide is bad numerically
    jac_(1, 0) = (cth_ * th2i_ - 1.0 * th2i_) * h_;
    jac_(2, 0) = h_ * (th_ - sth_) * th2i_;
    jac_(3, 0) = 1.0;
    jac_(4, 0) = 0.0;
    jac_(5, 0) = 0.0;
  } else {
    jac_(0, 0) = 0.0;
    jac_(1, 0) = -0.5 * h_;
    jac_(2, 0) = kOneSixth * th_ * h_;
    jac_(3, 0) = 1.0;
    jac_(4, 0) = 0.0;
    jac_(5, 0) = 0.0;
  }
}

const Twist& JointArquus::JointAccApparent(void) {
  CalcJacDeriv();
  qd_ = qd()[0];  // get qd from joint_base
  jac_circ_ = jac_part_ * qd_;
  joint_acc_apparent_ = jac_circ_ * qd_;
  return joint_acc_apparent_;
}

void JointArquus::CalcJacDeriv(void) {
  th3i_ = th2i_ * thi_;
  if (th2_ > zero_tol_) {
    jac_part_(0, 0) = 0.0;
    jac_part_(1, 0) = -2.0 * cthm1_ * h_ * th3i_ - h_ * sth_ * th2i_;
    jac_part_(2, 0) = -cthm1_ * h_ * th2i_ - 2.0 * h_ * (th_ - sth_) * th3i_;
    jac_part_(3, 0) = 0.0;
    jac_part_(4, 0) = 0.0;
    jac_part_(5, 0) = 0.0;
  } else {
    jac_part_.setZero();

    jac_part_(2, 0) = h_ * kOneSixth;
    jac_part_(5, 1) = -0.5;
  }
}

// Copied from Flexus, not really right, but probably depricated
double JointArquus::GravityTrust(const Vec3& acc_joint,
                                 const Vec3& acc_link,
                                 const Vec& /*q*/) {
  acc_trust_check_vec_(0) = acc_joint(0) - acc_link(0);
  acc_trust_check_vec_(1) = acc_joint(1) - acc_link(1);
  acc_trust_check_vec_(2) = acc_joint(2) + acc_link(2);
  return acc_trust_check_vec_.norm() / (acc_joint.norm() + acc_link.norm() + 1e-16);
}

}  // namespace dynamics
