// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <dynamics/link.h>

#include <cassert>
#include <iostream>

namespace dynamics {

Link::Link(const Hom& g_prox_joint) : g_prox_joint_(g_prox_joint) {}

Link::Link(const JointBase& joint, const Hom& g_prox_joint)
    : joint_(joint), g_prox_joint_(g_prox_joint) {}

void Link::set_dof_idx_start(int idx) {
  dof_idx_start_ = idx;
  dof_idx_end_ = dof_idx_start_ + joint_->num_dof();
}

void Link::SetLinkProxRefs(LinkVec* links) {
  assert(prox_idx_ < static_cast<int>(links->size()));
  if (prox_idx_ < 0) {
    link_prox_ = nullptr;
  } else {
    link_prox_ = &((*links)[prox_idx_]);
  }
}

void Link::SetQFullVec(const Vec& q) {
  assert(q.rows() >= dof_idx_end_);
  assert(joint_.get());
  joint_->set_q(q.segment(dof_idx_start_, joint_->num_dof()));
}

void Link::SetQdFullVec(const Vec& qd) {
  assert(qd.rows() >= dof_idx_end_);
  assert(joint_.get());
  joint_->set_qd(qd.segment(dof_idx_start_, joint_->num_dof()));
}

void Link::SetQddFullVec(const Vec& qdd) {
  assert(qdd.rows() >= dof_idx_end_);
  assert(joint_.get());
  joint_->set_qdd(qdd.segment(dof_idx_start_, joint_->num_dof()));
}

void Link::GetTauFullVec(Vec* tau) {
  assert(tau);
  assert(tau->rows() >= dof_idx_end_);
  assert(joint_.get());
  tau->segment(dof_idx_start_, joint_->num_dof()) = joint_->tau();
}

void Link::GetQFullVec(Vec* q) {
  assert(q);
  assert(q->rows() >= dof_idx_end_);
  assert(joint_.get());
  q->segment(dof_idx_start_, joint_->num_dof()) = joint_->q();
}

const Twist& Link::GetJointWrench(void) const {
  joint_wrench_ = joint_->jac() * joint_->tau();
  return joint_wrench_;
}

void Link::CalcFKLink0(void) {
  // only call on the base link
  assert(prox_idx_ == -1);
  g_world_link_ = g_prox_joint_;
  adj_world_link_ = AdjFromHom(g_world_link_);
  adj_link_world_ = AdjInvFromHom(g_world_link_);  // needed for body frame stuff
}

void Link::CalcFK(void) {
  joint_->CalcFK();
  g_world_joint_ = link_prox_->g_world_link_ * g_prox_joint_;
  g_world_link_ = g_world_joint_ * joint_->g_joint_link();
}

void Link::CalcJac(Jac* jac_spatial) {
  joint_->CalcJac();
  adj_world_link_ = AdjFromHom(g_world_link_);
  adj_link_world_ = AdjInvFromHom(g_world_link_);  // needed for body frame stuff
  jac_spatial->middleCols(dof_idx_start_, joint_->num_dof()) =
      adj_world_link_ * joint_->jac();
}

void Link::CalcDynSetup(void) {
  gravity_link_ = adj_link_world_ * gravity_;

  if (prox_idx_ >= 0) {
    g_prox_link_ = g_prox_joint_ * joint_->g_joint_link();
    adj_link_prox_ = AdjInvFromHom(g_prox_link_);
    adj_dual_prox_link_ = AdjDualFromHom(g_prox_link_);
  }
}

void Link::CalcID(void) {
  wrench_ = -inertia_ * gravity_link_;
  wrench_ += wrench_ext_;

  if (prox_idx_ < 0) {
    return;
  }

  joint_vel_ = joint_->JointVel();

  vel_ = adj_link_prox_ * link_prox_->vel_;
  vel_ += joint_vel_;

  vel_cross_ = TwistCross(vel_);

  acc_ = adj_link_prox_ * link_prox_->acc_;  // previous joint acceleration
  acc_ += joint_->JointAcc();                // Si * qdd (per Featherstone)
  acc_ += joint_->JointAccApparent();        //
  acc_ += vel_cross_ * joint_vel_;           // Coriolis(?)

  wrench_ += inertia_ * acc_;
  wrench_ += -vel_cross_.transpose() * (inertia_ * vel_);  // centripetal(?)
}

void Link::CalcIDStatic(void) {
  wrench_ = -inertia_ * gravity_link_;
  wrench_ += wrench_ext_;
}

void Link::CalcIDDelta(int idx) {
  assert(prox_idx_ >= 0);  // don't call on base link

  acc_ = adj_link_prox_ * link_prox_->acc_;  // previous joint acceleration

  // Add unit acceleration for one dof
  if (dof_idx_start_ <= idx && idx < dof_idx_end_) {
    acc_ += joint_->jac().col(idx - dof_idx_start_);
  }

  wrench_ = inertia_ * acc_;
}

void Link::CalcIDPassTwo(void) {
  // Project joint wrenchs through Jacobian transpose
  joint_->SetTauFromWrench(wrench_);
  // Propogate wrenchs to proximal link
  // F_prox = Ad_link_prox.T F_link
  link_prox_->wrench_ += adj_dual_prox_link_ * wrench_;
}

void Link::SetInertiaMassVecIcmDiag(double mass, Vec3 p, Vec3 inert_cm_diag) {
  inertia_ = CreateInertiaMassVecIcmDiag(mass, p, inert_cm_diag);
}

}  // namespace dynamics
