// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include "dynamics/joint_coriolis.h"

namespace dynamics {

JointCoriolis::JointCoriolis(void) : JointBase(num_dof_derived_) {
  jac_.setZero();
  jac_(5, 0) = 1.0;
  jac_(0, 1) = 1.0;
  jac_part_r_(1, 0) = 1.0;
}

void JointCoriolis::CalcFK(void) {
  th_ = q()[0];
  r_ = q()[1];
  sth_ = sin(th_);
  cth_ = cos(th_);
  g_joint_link_(0, 0) = cth_;
  g_joint_link_(0, 1) = -sth_;
  g_joint_link_(1, 0) = sth_;
  g_joint_link_(1, 1) = cth_;
  g_joint_link_(0, 3) = r_ * cth_;
  g_joint_link_(1, 3) = r_ * sth_;
}

void JointCoriolis::CalcJac(void) { jac_(1, 0) = r_; }

const Twist& JointCoriolis::JointAccApparent(void) {
  // jac_part_th_  doesn't change
  // jac_part_r_  doesn't change
  qd_ = qd();  // get qd from joint_base
  // jac_circ_ = jac_part_u_ * qd_[0] + jac_part_v_ * qd_[1];
  jac_circ_ = jac_part_r_ * qd_[1];  // th component is zero
  joint_acc_apparent_ = jac_circ_ * qd_;
  return joint_acc_apparent_;
}

}  // namespace dynamics
