// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <stdio.h>
#include <iostream>
#include "dynamics/joint_rot.h"
#include "dynamics/gravity_solver.h"

namespace dyn = dynamics;
using dyn::Vec;
using dyn::Vec3;

int main() {
  dyn::JointRot joint = dyn::JointRot();
  joint.CalcFK();

  // dyn::GravitySolver gs = dyn::GravitySolver(joint);
  // dyn::GravitySolver gs(joint);
  dyn::GravitySolver gs = dyn::GravitySolver(joint);
  gs = dyn::GravitySolver(joint);
  // gs.Initialize();

  std::cout << "box_lower: " << gs.joint()->limits().box_lower().transpose() << std::endl;
  std::cout << "box_upper: " << gs.joint()->limits().box_upper().transpose() << std::endl;

  Vec3 acc_prox(0.0, 1.0, 0.0);
  Vec3 acc_dist(1.0, 0.0, 0.0);
  Vec q_guess, q_soln;

  q_guess = Vec::Zero(joint.num_dof());
  q_soln = gs.Solve(acc_prox, acc_dist, q_guess);

  std::cout << "q_soln: " << q_soln.transpose() << std::endl;

  return 0;
}
