// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include "dynamics/gravity_solver.h"

#include <vector>
#include <iostream>

#include "dynamics/joint_flexus.h"

namespace dynamics {

GravitySolver::GravitySolver(const JointPtr joint_ptr) : joint_(joint_ptr) {
  Initialize();
}

GravitySolver::GravitySolver(const JointBase& joint) : GravitySolver(JointPtr(joint)) {}

void GravitySolver::Initialize(void) {
  opt_ = nlopt::opt(nlopt::LN_BOBYQA, joint_->num_dof());
  UpdateBounds();
  opt_.set_min_objective(GravitySolver::CostCaller, this);
  opt_.set_xtol_rel(1e-4);
  feval_ = 0.0;
  x_ = DoubleVec(0.0, joint_->num_dof());
}

void GravitySolver::UpdateBounds(void) {
  Vec limits;

  Vec lower = joint_->limits().box_lower();
  DoubleVec lb(lower.data(), lower.data() + lower.size());
  opt_.set_lower_bounds(lb);

  Vec upper = joint_->limits().box_upper();
  DoubleVec ub(upper.data(), upper.data() + upper.size());
  opt_.set_upper_bounds(ub);
}

const Vec& GravitySolver::Solve(const Vec3& acc_joint,
                                const Vec3& acc_link,
                                const Vec& q) {
  // Vec* q) {
  assert(q->size() == joint_->num_dof());
  feval_ = 0;
  acc_joint_ = acc_joint;
  acc_link_ = acc_link;
  double minf;

  x_ = DoubleVec(q.data(), q.data() + q.size());

  try {
    opt_.optimize(x_, minf);
  } catch (nlopt::roundoff_limited& e) {
    // This is ok
  } catch (std::invalid_argument& e) {
    std::cout << "### Error: ###" << std::endl;
    std::cout << "nlopt invalid argument. Fix this!" << std::endl;
    std::cout << "minf = " << minf << std::endl;
    for (int i = 0; i < joint_->num_dof(); i++) {
      printf("x[%d] = %7.3f\n", i, x_[i]);
    }
    std::cout << "*q = \n" << q << std::endl;
    std::cout << "Gravity - \n";
    std::cout << "acc_joint = \n" << acc_joint << std::endl;
    std::cout << "acc_link = \n" << acc_link << std::endl;
    std::cout << "Joint limits are - \n";
    std::cout << "lower: \n" << joint_->limits().box_lower() << std::endl;
    std::cout << "upper: \n" << joint_->limits().box_upper() << std::endl;
  }

  trust_coef_ = joint_->GravityTrust(acc_joint_, acc_link_, q);
  assert(trust_coeff >= 0.0);
  assert(trust_coeff <= 1.0);

  soln_ = Vec(Eigen::Map<const Vec>(x_.data(), x_.size()));
  return soln_;
}

double GravitySolver::TrustCoeff(const Vec3& acc_joint,
                                 const Vec3& acc_link,
                                 const Vec& q) {
  return joint_->GravityTrust(acc_joint, acc_link, q);
}

double GravitySolver::CostFunc(const DoubleVec& x) {
  // return 0.0;
  joint_->set_q(Eigen::Map<const Vec>(x.data(), x.size()));

  joint_->CalcFK();
  rot_joint_link_ = RotFromHom(joint_->g_joint_link());
  acc_link_calc_ = rot_joint_link_.transpose() * acc_joint_;
  err_ = acc_link_calc_ - acc_link_;

  return err_.dot(err_);
}

double GravitySolver::CostCaller(const DoubleVec& x, DoubleVec& /*grad*/, void* f_data) {
  return (reinterpret_cast<GravitySolver*>(f_data))->CostFunc(x);
}

}  // namespace dynamics
