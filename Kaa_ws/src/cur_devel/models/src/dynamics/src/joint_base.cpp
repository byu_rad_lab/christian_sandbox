// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <dynamics/joint_base.h>

#include <cassert>

namespace dynamics {

JointBase::JointBase(int num_dof) : num_dof_(num_dof) {}

const Twist& JointBase::JointVel(void) const {
  joint_vel_ = jac() * qd_;
  return joint_vel_;
}

const Twist& JointBase::JointAcc(void) const {
  joint_acc_ = jac() * qdd_;
  return joint_acc_;
}

void JointBase::set_q(const Vec& q) {
  assert(q.rows() == num_dof_);
  q_ = q;
}

void JointBase::set_qd(const Vec& qd) {
  assert(qd.rows() == num_dof_);
  qd_ = qd;
}

void JointBase::set_qdd(const Vec& qdd) {
  assert(qdd.rows() == num_dof_);
  qdd_ = qdd;
}

void JointBase::set_tau(const Vec& tau) {
  assert(tau.rows() == num_dof_);
  tau_ = tau;
}

void JointBase::SetState(const Vec& q, const Vec& qd, const Vec& qdd) {
  set_q(q);
  set_qd(qd);
  set_qdd(qdd);
}

void JointBase::SetStateZero(void) {
  SetQZero();
  SetQdZero();
  SetQddZero();
}

void JointBase::SetQRandom(void) { set_q(limits().GetQRandom()); }

void JointBase::CheckMatSizes(void) {
  CheckMatSize(q_, num_dof_);
  CheckMatSize(qd_, num_dof_);
  CheckMatSize(qdd_, num_dof_);

  CheckMatSize(jac(), 6, num_dof_);
}

}  // namespace dynamics
