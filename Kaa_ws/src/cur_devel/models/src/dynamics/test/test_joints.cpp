// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <eigen3/Eigen/Dense>
#include "gtest/gtest.h"
// #include "test/common.h"
#include "dynamics/types.h"
#include "dynamics/testing.h"
#include "dynamics/joint_rot.h"
#include "dynamics/joint_prism.h"
#include "dynamics/joint_flexus.h"
#include "dynamics/joint_coriolis.h"

using dynamics::Vec;
using dynamics::Vec3;
using dynamics::Rot;
using dynamics::Hom;
using dynamics::Jac;
using dynamics::Mat;
using dynamics::Twist;
using dynamics::JointRot;
using dynamics::JointPrism;
using dynamics::JointFlexus;
using dynamics::JointCoriolis;
using std::cout;
using std::endl;

namespace dyn = dynamics;

class JointRotTest : public ::testing::Test {
 public:
  JointRot joint_;
  Vec q1_ = Vec::Zero(1);
  Jac jac_;

  void SetState0(void) {
    joint_.SetStateZero();
    joint_.CalcFK();
    joint_.CalcJac();
  }

  void SetState1(void) {
    joint_.SetStateZero();
    joint_.set_q(q1_);
    joint_.CalcFK();
    joint_.CalcJac();
  }

  virtual void SetUp() {
    joint_ = JointRot();
    q1_ << 0.5;
    jac_ = Jac::Zero(6, 1);
    jac_(5, 0) = 1.0;
  }

  virtual void TearDown() {}
};

TEST_F(JointRotTest, State0FK) {
  SetState0();
  EXPECT_TRUE(dyn::EigenEqual(joint_.g_joint_link(), Hom::Identity()));
}

TEST_F(JointRotTest, State0Jac) {
  SetState0();
  EXPECT_TRUE(dyn::EigenEqual(jac_, joint_.jac()));
}

TEST_F(JointRotTest, State0AccApparent) {
  SetState0();
  EXPECT_TRUE(dyn::EigenEqual(joint_.JointAccApparent(), Twist::Zero()));
}

TEST_F(JointRotTest, State1FK) {
  SetState1();
  Hom g = dyn::HomFromRvecVec(Vec3(0.0, 0.0, q1_[0]), Vec3::Zero());
  EXPECT_TRUE(dyn::EigenEqual(joint_.g_joint_link(), g));
}

TEST_F(JointRotTest, State1AccApparent) {
  SetState1();
  EXPECT_TRUE(dyn::EigenEqual(joint_.JointAccApparent(), Twist::Zero()));
}

class JointPrismTest : public ::testing::Test {
 public:
  JointPrism joint_;
  Vec q1_ = Vec::Zero(1);
  Jac jac_;

  void SetState0(void) {
    joint_.SetStateZero();
    joint_.CalcFK();
    joint_.CalcJac();
  }

  void SetState1(void) {
    joint_.SetStateZero();
    joint_.set_q(q1_);
    joint_.CalcFK();
    joint_.CalcJac();
  }

  virtual void SetUp() {
    joint_ = JointPrism();
    q1_ << 0.5;
    jac_ = Jac::Zero(6, 1);
    jac_(2, 0) = 1.0;
  }

  virtual void TearDown() {}
};

TEST_F(JointPrismTest, State0FK) {
  SetState0();
  EXPECT_TRUE(dyn::EigenEqual(joint_.g_joint_link(), Hom::Identity()));
}

TEST_F(JointPrismTest, State0Jac) {
  SetState0();
  EXPECT_TRUE(dyn::EigenEqual(jac_, joint_.jac()));
}

TEST_F(JointPrismTest, State0AccApparent) {
  SetState0();
  EXPECT_TRUE(dyn::EigenEqual(joint_.JointAccApparent(), Twist::Zero()));
}

TEST_F(JointPrismTest, State1FK) {
  SetState1();
  Hom g = dyn::HomFromRvecVec(Vec3::Zero(), Vec3(0.0, 0.0, q1_[0]));
  EXPECT_TRUE(dyn::EigenEqual(joint_.g_joint_link(), g));
}

TEST_F(JointPrismTest, State1AccApparent) {
  SetState1();
  EXPECT_TRUE(dyn::EigenEqual(joint_.JointAccApparent(), Twist::Zero()));
}

class JointFlexusTest : public ::testing::Test {
 public:
  // Needs a default constructor
  JointFlexusTest(void) : joint_(h_) {}

  double h_ = 0.20;
  JointFlexus joint_;

  Vec q1_ = Vec::Zero(2);
  Vec q2_ = Vec::Zero(2);
  Jac jac_ = Jac::Zero(6, 2);
  double r_;
  double th_ = M_PI / 2.0;
  double tol_ = 1e-12;

  void SetState0(void) {
    joint_.SetStateZero();
    joint_.CalcFK();
    joint_.CalcJac();
  }

  void SetState1(void) {
    joint_.SetStateZero();
    joint_.set_q(q1_);
    joint_.CalcJac();
  }

  void SetState2(void) {
    joint_.SetStateZero();
    joint_.set_q(q2_);
    joint_.CalcJac();
  }

  virtual void SetUp() {
    joint_ = JointFlexus(h_);

    q1_ << th_, 0.0;
    q2_ << 0.0, th_;
    r_ = h_ / th_;

    jac_ = Jac::Zero(6, 1);
    jac_(2, 0) = 1.0;
  }

  virtual void TearDown() {}
};

// test straight
TEST_F(JointFlexusTest, State0FK) {
  SetState0();
  Hom hom = Hom::Identity();
  hom(2, 3) = h_;
  EXPECT_TRUE(dyn::EigenEqual(joint_.g_joint_link(), hom));
  dyn::TestHom(joint_.g_joint_link());
}

TEST_F(JointFlexusTest, State1FK) {
  SetState0();
  joint_.set_q(q1_);
  joint_.CalcFK();
  Hom hom = joint_.g_joint_link();
  ASSERT_NEAR(0.0, hom(0, 3), tol_);
  ASSERT_NEAR(-r_, hom(1, 3), tol_);
  ASSERT_NEAR(r_, hom(2, 3), tol_);

  dyn::TestHom(hom);
}

TEST_F(JointFlexusTest, State2FK) {
  SetState0();
  joint_.set_q(q2_);
  joint_.CalcFK();
  Hom hom = joint_.g_joint_link();
  ASSERT_NEAR(r_, hom(0, 3), tol_);
  ASSERT_NEAR(0.0, hom(1, 3), tol_);
  ASSERT_NEAR(r_, hom(2, 3), tol_);

  dyn::TestHom(hom);
}

TEST_F(JointFlexusTest, ExerciseDerivs) {
  SetState0();
  Hom g_joint_link = joint_.g_joint_link();
  dyn::TestHom(g_joint_link);

  Jac jac = joint_.jac();
  ASSERT_EQ(jac.rows(), 6);
  ASSERT_EQ(jac.cols(), 2);

  Twist joint_acc_apparent = joint_.JointAccApparent();
  // Should be zero for state zero
  EXPECT_TRUE(dyn::EigenEqual(joint_acc_apparent, Twist::Zero()));
}

class JointCoriolisTest : public ::testing::Test {
 public:
  // Needs a default constructor

  JointCoriolis joint_;

  Vec q1_ = Vec::Zero(2);
  Jac jac_ = Jac::Zero(6, 2);
  double th1_ = 0.5;
  double r1_ = 2.5;

  void SetState0(void) {
    joint_.SetStateZero();
    joint_.CalcFK();
    joint_.CalcJac();
  }

  void SetState1(void) {
    joint_.SetStateZero();
    joint_.set_q(q1_);
    joint_.CalcJac();
  }

  virtual void SetUp() {
    joint_ = JointCoriolis();

    q1_ << th1_, r1_;
  }

  virtual void TearDown() {}
};

TEST_F(JointCoriolisTest, ExerciseDerivs) {
  SetState0();
  Hom g_joint_link = joint_.g_joint_link();
  dyn::TestHom(g_joint_link);

  Jac jac = joint_.jac();
  ASSERT_EQ(jac.rows(), 6);
  ASSERT_EQ(jac.cols(), 2);

  Twist joint_acc_apparent = joint_.JointAccApparent();
  // Should be zero for state zero
  EXPECT_TRUE(dyn::EigenEqual(joint_acc_apparent, Twist::Zero()));
}

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
