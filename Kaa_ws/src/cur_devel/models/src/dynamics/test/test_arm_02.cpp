// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <eigen3/Eigen/Dense>
#include <tuple>
#include <vector>

#include "dynamics/arm.h"
#include "dynamics/joint_coriolis.h"
#include "dynamics/joint_fixed.h"
#include "dynamics/joint_prism.h"
#include "dynamics/joint_rot.h"
#include "dynamics/link.h"
#include "dynamics/testing.h"
#include "dynamics/types.h"
#include "gtest/gtest.h"

using std::cout;
using std::endl;

namespace dyn = dynamics;

// th, r, thd, rd, thdd, rdd
typedef std::tuple<double, double, double, double, double, double> Input;

class ArmConfig02 {
 public:
  dyn::Arm arm1_;
  dyn::Arm arm2_;

  dyn::Twist vel_ = dyn::Twist::Zero();
  dyn::Twist acc_ = dyn::Twist::Zero();
  dyn::Twist wrench_ = dyn::Twist::Zero();

  double th_ = 0;
  double r_ = 0;
  double thd_ = 0;
  double rd_ = 0;
  double thdd_ = 0;
  double rdd_ = 0;

  ArmConfig02(void) { SetUp(); }

  void SetState(dyn::Vec q, dyn::Vec qd, dyn::Vec qdd) {
    th_ = q(0);
    r_ = q(1);
    thd_ = qd(0);
    rd_ = qd(1);
    thdd_ = qdd(0);
    rdd_ = qdd(1);

    arm1_.SetState(q, qd, qdd);
    arm2_.SetState(q, qd, qdd);
  }

  void CalcVel(void) {
    vel_(0) = rd_;
    vel_(1) = r_ * thd_;
    vel_(5) = thd_;
  }

  void CalcAcc(void) {
    acc_(0) = rdd_;
    // acc_(0) -= thd_ * thd_ * r_;  // no centipetal in body frame

    acc_(1) = r_ * thdd_;
    acc_(1) += rd_ * thd_;  // half the coriolis force
    // acc_(1) += 2 * rd_ * thd_; // only half shows up in acceleration

    // acc_(2) = dyn::kGravity;  // shows up in wrench, not acceleration
    acc_(5) = thdd_;
  }

  void CalcWrench(void) {
    // only inertia is identity at last link
    wrench_(0) = rdd_;
    wrench_(0) -= thd_ * thd_ * r_;  // centripetal comes from vx* I v
    wrench_(1) = r_ * thdd_;
    wrench_(1) += 2 * rd_ * thd_;  // now we get the factor of two
    wrench_(2) = dyn::kGravity;    // shows up in wrench, not acceleration
    wrench_(5) = thdd_;
  }

  void SetUp(void) {
    // rotational joint, then prismatic, then fixed link to undo rotation
    arm1_ = dyn::Arm(dyn::Hom::Identity());
    arm1_.GetLink(0).set_inertia(dyn::Inertia::Zero());
    dyn::Hom link_prism_hom =
        dyn::HomFromRvecVec(dyn::Vec3(0.0, M_PI / 2.0, 0.0), dyn::Vec3::Zero());

    dyn::Link link_rot = dyn::Link(dyn::JointRot());
    dyn::Link link_prism = dyn::Link(dyn::JointPrism(), link_prism_hom);
    dyn::Link link_fixed_1 = dyn::Link(dyn::JointFixed(), dyn::HomInv(link_prism_hom));

    link_rot.set_inertia(dyn::Inertia::Zero());
    link_prism.set_inertia(dyn::Inertia::Zero());
    link_fixed_1.set_inertia(dyn::Inertia::Identity());

    arm1_.AddLink(link_rot);
    arm1_.AddLink(link_prism);
    arm1_.AddLink(link_fixed_1);

    // coriolis joint, which should be the same as above
    arm2_ = dyn::Arm(dyn::Hom::Identity());
    dyn::Link link_coriolis = dyn::Link(dyn::JointCoriolis());
    dyn::Link link_fixed_2 = dyn::Link(dyn::JointFixed());

    arm2_.GetLink(0).set_inertia(dyn::Inertia::Zero());
    link_coriolis.set_inertia(dyn::Inertia::Zero());
    link_fixed_2.set_inertia(dyn::Inertia::Identity());

    arm2_.AddLink(link_coriolis);
    arm2_.AddLink(link_fixed_2);  // not really needed
  }
};

class ArmConfig02Test : public ::testing::Test {
 public:
  ArmConfig02 ac_ = ArmConfig02();
  dyn::Arm& arm1_ = ac_.arm1_;
  dyn::Arm& arm2_ = ac_.arm2_;
};

class ArmConfig02TestP : public ::testing::TestWithParam<Input> {
 public:
  ArmConfig02 ac_ = ArmConfig02();
  dyn::Arm& arm1_ = ac_.arm1_;
  dyn::Arm& arm2_ = ac_.arm2_;

  dyn::Vec q_ = dyn::Vec::Zero(2);
  dyn::Vec qd_ = dyn::Vec::Zero(2);
  dyn::Vec qdd_ = dyn::Vec::Zero(2);

  Input input_;

  virtual void SetUp() {
    input_ = GetParam();
    q_(0) = std::get<0>(input_);
    q_(1) = std::get<1>(input_);

    qd_(0) = std::get<2>(input_);
    qd_(1) = std::get<3>(input_);

    qdd_(0) = std::get<4>(input_);
    qdd_(1) = std::get<5>(input_);

    ac_.SetState(q_, qd_, qdd_);
  }
};

TEST_F(ArmConfig02Test, NumDof) {
  EXPECT_EQ(arm1_.num_dof(), 2);
  EXPECT_EQ(arm2_.num_dof(), 2);
  EXPECT_EQ(arm1_.links().size(), 4ul);
  EXPECT_EQ(arm2_.links().size(), 3ul);
}

TEST_F(ArmConfig02Test, FK) {
  double dx = 2.3;
  dyn::Vec q = dyn::Vec::Zero(2);
  q(1) = dx;

  arm1_.set_q(q);
  arm2_.set_q(q);

  arm1_.CalcFK();
  arm2_.CalcFK();

  dyn::Hom g_world_tool = dyn::HomTransFromXYZ(dx, 0.0, 0.0);
  EXPECT_TRUE(dyn::EigenEqual(arm1_.GetLink(-1).g_world_link(), g_world_tool));
  EXPECT_TRUE(dyn::EigenEqual(arm2_.GetLink(-1).g_world_link(), g_world_tool));
}

// parametrized testing

TEST_P(ArmConfig02TestP, CompareGeom) {
  arm1_.CalcFK();
  arm2_.CalcFK();
  EXPECT_TRUE(dyn::EigenEqual(arm1_.GetLink(-1).g_world_link(),
                              arm2_.GetLink(-1).g_world_link()));
  EXPECT_TRUE(dyn::EigenEqual(arm1_.jac_spatial(), arm2_.jac_spatial()));
  EXPECT_TRUE(dyn::EigenEqual(arm1_.JacLink(-1), arm2_.JacLink(-1)));
  EXPECT_TRUE(dyn::EigenEqual(arm1_.GetLink(-1).g_world_link(),
                              arm2_.GetLink(-1).g_world_link()));
  EXPECT_TRUE(dyn::EigenEqual(arm1_.JacLink(-1), arm2_.JacLink(-1)));
}

TEST_P(ArmConfig02TestP, CompareVelAndAcc) {
  arm1_.CalcID();
  arm2_.CalcID();
  EXPECT_TRUE(dyn::EigenEqual(arm1_.GetLink(0).vel(), arm2_.GetLink(0).vel()));
  EXPECT_TRUE(dyn::EigenEqual(arm1_.GetLink(0).acc(), arm2_.GetLink(0).acc()));
  EXPECT_TRUE(dyn::EigenEqual(arm1_.GetLink(-1).vel(), arm2_.GetLink(-1).vel()));
  EXPECT_TRUE(dyn::EigenEqual(arm1_.GetLink(-1).acc(), arm2_.GetLink(-1).acc()));
}

TEST_P(ArmConfig02TestP, CompareWrenches) {
  arm1_.CalcID();
  arm2_.CalcID();
  EXPECT_TRUE(dyn::EigenEqual(arm1_.GetLink(0).wrench(), arm2_.GetLink(0).wrench()));
  EXPECT_TRUE(dyn::EigenEqual(arm1_.GetLink(-1).wrench(), arm2_.GetLink(-1).wrench()));
}

TEST_P(ArmConfig02TestP, VelsHandCheck) {
  ac_.CalcVel();
  arm1_.CalcID();
  arm2_.CalcID();

  ASSERT_TRUE(dyn::EigenEqual(arm1_.q(), arm2_.q()));
  ASSERT_TRUE(dyn::EigenEqual(arm1_.qd(), arm2_.qd()));
  ASSERT_TRUE(dyn::EigenEqual(arm1_.qdd(), arm2_.qdd()));

  EXPECT_TRUE(dyn::EigenEqual(arm1_.GetLink(-1).vel(), ac_.vel_));
  EXPECT_TRUE(dyn::EigenEqual(arm2_.GetLink(-1).vel(), ac_.vel_));

  EXPECT_TRUE(dyn::EigenEqual(arm1_.JacLink(-1) * arm1_.qd(), ac_.vel_));
  EXPECT_TRUE(dyn::EigenEqual(arm2_.JacLink(-1) * arm1_.qd(), ac_.vel_));
}

TEST_P(ArmConfig02TestP, AccHandCheck) {
  ac_.CalcAcc();
  arm1_.CalcID();
  arm2_.CalcID();

  EXPECT_TRUE(dyn::EigenEqual(arm1_.GetLink(-1).acc(), ac_.acc_));
  EXPECT_TRUE(dyn::EigenEqual(arm2_.GetLink(-1).acc(), ac_.acc_));
}

TEST_P(ArmConfig02TestP, FDVsID) {
  dyn::Vec qdd = arm1_.qdd();
  dyn::Vec tau = arm1_.CalcID();
  dyn::Vec qdd_calc = arm1_.CalcFD(tau);

  ASSERT_TRUE(dyn::EigenEqual(qdd, qdd_calc));  // make sure qdd is recovered
}

TEST_P(ArmConfig02TestP, WrenchHandCheck) {
  ac_.CalcWrench();
  arm1_.CalcID();
  arm2_.CalcID();

  EXPECT_TRUE(dyn::EigenEqual(arm1_.GetLink(-1).wrench(), ac_.wrench_));
  EXPECT_TRUE(dyn::EigenEqual(arm2_.GetLink(-1).wrench(), ac_.wrench_));
}

INSTANTIATE_TEST_CASE_P(ParameterizedTesting,
                        ArmConfig02TestP,
                        ::testing::Values(                                  //
                            std::make_tuple(0.3, 0.0, 0.0, 0.0, 0.0, 0.0),  //
                            std::make_tuple(0.0, 0.5, 0.0, 0.0, 0.0, 0.0),  //
                            std::make_tuple(0.3, 0.5, 0.0, 0.0, 0.0, 0.0),  //

                            std::make_tuple(0.0, 0.0, 0.3, 0.0, 0.0, 0.0),  //
                            std::make_tuple(0.3, 0.0, 0.3, 0.0, 0.0, 0.0),  //
                            std::make_tuple(0.0, 0.5, 0.3, 0.0, 0.0, 0.0),  //
                            std::make_tuple(0.3, 0.5, 0.3, 0.0, 0.0, 0.0),  //

                            std::make_tuple(0.0, 0.0, 0.0, 0.7, 0.0, 0.0),  //
                            std::make_tuple(0.3, 0.0, 0.0, 0.7, 0.0, 0.0),  //
                            std::make_tuple(0.0, 0.5, 0.0, 0.7, 0.0, 0.0),  //
                            std::make_tuple(0.3, 0.5, 0.0, 0.7, 0.0, 0.0),  //

                            std::make_tuple(0.0, 0.0, 0.4, 0.7, 0.0, 0.0),  //
                            std::make_tuple(0.3, 0.0, 0.4, 0.7, 0.0, 0.0),  //
                            std::make_tuple(0.0, 0.5, 0.4, 0.7, 0.0, 0.0),  //
                            std::make_tuple(0.3, 0.5, 0.4, 0.7, 0.0, 0.0),  //

                            std::make_tuple(0.0, 0.0, 0.0, 0.0, 0.2, 0.0),  //
                            std::make_tuple(0.0, 0.0, 0.0, 0.0, 0.0, 0.6),  //

                            std::make_tuple(0.0, 0.0, 0.7, 0.0, 0.2, 0.0),  //
                            std::make_tuple(0.0, 0.0, 0.7, 0.0, 0.0, 0.6),  //

                            std::make_tuple(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)  //
                            ));  // NOLINT(whitespace/parens)

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
