// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include "gtest/gtest.h"
#include <eigen3/Eigen/Dense>
#include <iostream>
// #include "test/common.h"
#include "dynamics/testing.h"
#include "dynamics/types.h"

using dynamics::Vec3;
using dynamics::Rot;
using dynamics::Hom;
using dynamics::Adj;
using dynamics::Twist;
using dynamics::Mat;
using dynamics::Inertia;
using dynamics::InertRot;
using dynamics::EigenEqual;
using dynamics::TestHom;
using std::cout;
using std::endl;

namespace dyn = dynamics;

TEST(RotFromRVecTest, TwoSimpleCases) {
  Vec3 rvec;
  Rot rot_a, rot_b;
  // Identity rotation case
  rvec << 0.0, 0.0, 0.0;
  rot_a = dyn::RotFromRvec(rvec);
  rot_b = Rot::Identity();
  EXPECT_TRUE(EigenEqual(rot_a, rot_b));

  // 90 degree rotation about x
  rvec << M_PI / 2.0, 0.0, 0.0;
  rot_b << 1.0, 0.0, 0.0,  //
      0.0, 0.0, -1.0,      //
      0.0, 1.0, 0.0;       //
  rot_a = dyn::RotFromRvec(rvec);
  EXPECT_TRUE(EigenEqual(rot_a, rot_b));

  // Identity rotation from large rotation vector
  rvec << 6.0 * M_PI, 0.0, 0.0;
  rot_a = dyn::RotFromRvec(rvec);
  rot_b = Rot::Identity();
  EXPECT_TRUE(EigenEqual(rot_a, rot_b));
}

TEST(HomComponentTest, Test01) {
  Rot rot_a, rot_b, rot_c;
  Vec3 vec_a, vec_b, vec_c;
  Vec3 r_vec_a, r_vec_b;

  // Create some data
  r_vec_a = Vec3(1.0, 2.0, 3.0);
  rot_a = dyn::RotFromRvec(r_vec_a);
  vec_a = Vec3(4.0, 5.0, 6.0);
  Hom g = dyn::HomFromRotVec(rot_a, vec_a);

  // Check we got the bits we wanted
  rot_b = g.topLeftCorner(3, 3);
  vec_b = g.topRightCorner(3, 1);
  EXPECT_TRUE(EigenEqual(rot_a, rot_b));
  EXPECT_TRUE(EigenEqual(vec_a, vec_b));

  // Make sure the bottom row is correct
  for (int i = 0; i < 3; i++) {
    EXPECT_NEAR(g(3, i), 0.0, 1e-12);
  }
  EXPECT_NEAR(g(3, 3), 1.0, 1e-12);

  // Reverse the process and check twice
  vec_c = dyn::VecFromHom(g);
  rot_c = dyn::RotFromHom(g);
  EXPECT_TRUE(EigenEqual(rot_c, rot_b));
  EXPECT_TRUE(EigenEqual(vec_c, vec_b));
  EXPECT_TRUE(EigenEqual(rot_c, rot_a));
  EXPECT_TRUE(EigenEqual(vec_c, vec_a));

  // Check rvec convienence is correct
  r_vec_b = dyn::RvecFromHom(g);
  Rot rot_d = dyn::RotFromRvec(r_vec_b);
  EXPECT_TRUE(EigenEqual(rot_a, rot_d));
}

TEST(HatTest, TestVecHat) {
  Vec3 v(2.0, 3.0, 5.0);
  Rot vh = dyn::VecHat(v);
  Rot vhtn = -vh.transpose();
  EXPECT_TRUE(EigenEqual(vh, vhtn));  // checks for skew symmetry
  ASSERT_DOUBLE_EQ(v(0), -vh(1, 2));
  ASSERT_DOUBLE_EQ(v(1), vh(0, 2));
  ASSERT_DOUBLE_EQ(v(2), -vh(0, 1));
}

TEST(TwistHomTest, TestInverses) {
  Vec3 v(2.0, 3.0, 5.0);
  Vec3 w(2.0, 1.0, 0.0);
  Hom hom1 = dynamics::HomFromRvecVec(w, v);
  Twist twist1 = dynamics::TwistFromHom(hom1);
  Hom hom2 = dynamics::HomFromTwist(twist1);

  EXPECT_TRUE(EigenEqual(hom1, hom2));

  Vec3 v2(2.0, 3.0, -1.0);
  Vec3 w2(0.0, 0.0, 0.0);
  hom1 = dynamics::HomFromRvecVec(w2, v2);
  twist1 = dynamics::TwistFromHom(hom1);
  hom2 = dynamics::HomFromTwist(twist1);

  EXPECT_TRUE(EigenEqual(hom1, hom2));
}

TEST(TwistHomTest, TestZero) {
  Twist t0 = Twist::Zero();
  Hom h0 = dynamics::HomFromTwist(t0);
  EXPECT_TRUE(EigenEqual(h0, Hom::Identity()));

  h0 = Hom::Identity();
  t0 = dynamics::TwistFromHom(h0);
  EXPECT_TRUE(EigenEqual(t0, Twist::Zero()));
}

// limited, doesn't check p
TEST(Inertia, CreateInertiaPZero) {
  Vec3 p = Vec3::Zero();
  Vec3 inert_diag = Vec3(0.7, 1.2, 1.5);
  double mass = 4.0;
  Inertia inert_00 = mass * Inertia::Identity();
  inert_00(3, 3) = inert_diag(0);
  inert_00(4, 4) = inert_diag(1);
  inert_00(5, 5) = inert_diag(2);

  Inertia inert_01 = dyn::CreateInertiaMassVecIcmDiag(mass, p, inert_diag);
  EXPECT_TRUE(EigenEqual(inert_00, inert_01));

  InertRot inert_rot = InertRot(inert_diag.asDiagonal());
  Inertia inert_02 = dyn::CreateInertiaMassVecIcm(mass, p, inert_rot);
  EXPECT_TRUE(EigenEqual(inert_00, inert_02));
}

TEST(Inertia, CreateInertiaPNonZero) {
  Vec3 p = Vec3(2.0, 3.0, 5.0);
  Vec3 inert_diag = Vec3(0.7, 1.2, 1.5);
  double mass = 4.0;

  Rot p_hat = dyn::VecHat(p);
  Inertia inert_00 = mass * Inertia::Identity();
  // inert_00.topLeftCorner(3, 3) // taken care of by mass * I
  inert_00.topRightCorner(3, 3) = mass * p_hat.transpose();
  inert_00.bottomLeftCorner(3, 3) = mass * p_hat;
  inert_00.bottomRightCorner(3, 3) =
      InertRot(inert_diag.asDiagonal()) + mass * p_hat * p_hat.transpose();

  Inertia inert_01 = dyn::CreateInertiaMassVecIcmDiag(mass, p, inert_diag);
  EXPECT_TRUE(EigenEqual(inert_00, inert_01));

  InertRot inert_rot = InertRot(inert_diag.asDiagonal());
  Inertia inert_02 = dyn::CreateInertiaMassVecIcm(mass, p, inert_rot);
  EXPECT_TRUE(EigenEqual(inert_00, inert_02));
}

TEST(Adjoints, AdjVAdjInv) {
  Vec3 vec = Vec3(2.0, 3.0, 5.0);
  Vec3 rvec = Vec3(7.0, 8.0, 9.0);

  Hom hom = dyn::HomFromRvecVec(rvec, vec);
  Adj adj = dyn::AdjFromHom(hom);
  Adj adj_inv = dyn::AdjInvFromHom(hom);
  EXPECT_TRUE(EigenEqual(adj * adj_inv, Adj::Identity()));

  vec.setZero();
  rvec.setZero();

  hom = dyn::HomFromRvecVec(rvec, vec);
  adj = dyn::AdjFromHom(hom);
  adj_inv = dyn::AdjInvFromHom(hom);
  EXPECT_TRUE(EigenEqual(adj * adj_inv, Adj::Identity()));
}

TEST(Adjoints, AdjDual) {
  Vec3 vec = Vec3(2.0, 3.0, 5.0);
  Vec3 rvec = Vec3(7.0, 8.0, 9.0);

  Hom hom = dyn::HomFromRvecVec(rvec, vec);
  Adj adj_inv = dyn::AdjInvFromHom(hom);
  Adj adj_dual = dyn::AdjDualFromHom(hom);
  EXPECT_TRUE(EigenEqual(adj_inv.transpose(), adj_dual));

  vec.setZero();
  rvec.setZero();

  hom = dyn::HomFromRvecVec(rvec, vec);
  adj_inv = dyn::AdjInvFromHom(hom);
  adj_dual = dyn::AdjDualFromHom(hom);
  EXPECT_TRUE(EigenEqual(adj_inv.transpose(), adj_dual));
}

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
