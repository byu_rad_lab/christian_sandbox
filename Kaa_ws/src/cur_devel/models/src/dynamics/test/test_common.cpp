// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <cmath>
#include <iostream>
#include <limits>

#include "eigen3/Eigen/Dense"
#include "gtest/gtest.h"
#include "dynamics/testing.h"

using Mat = Eigen::MatrixXd;

namespace dyn = dynamics;

TEST(EigenEqual, SucceedZero) {
  Mat a = Mat::Zero(3, 4);
  Mat b = Mat::Zero(3, 4);
  EXPECT_TRUE(dyn::EigenEqual(a, b));
}

TEST(EigenEqual, SucceedNonZero) {
  Mat a = Mat(3, 4);
  a.setRandom();
  Mat b = a;
  EXPECT_TRUE(dyn::EigenEqual(a, b));
}

TEST(EigenEqual, SucceedDiffTypes) {
  Mat a = Mat(3, 1);
  a.setRandom();
  Eigen::Vector3d b = a;
  EXPECT_TRUE(dyn::EigenEqual(a, b));
}

TEST(EigenEqual, SucceedClose) {
  Mat a = Mat(3, 4);
  a.setRandom();
  Mat b = a;
  double tol = 1e-3;
  a(0, 0) += tol / 2.0;
  EXPECT_TRUE(dyn::EigenEqual(a, b, tol));
}

TEST(EigenEqual, FailRows) {
  Mat a = Mat::Zero(3, 5);
  Mat b = Mat::Zero(4, 5);
  EXPECT_FALSE(dyn::EigenEqual(a, b));
}

TEST(EigenEqual, FailCols) {
  Mat a = Mat::Zero(4, 5);
  Mat b = Mat::Zero(4, 4);
  EXPECT_FALSE(dyn::EigenEqual(a, b));
}

TEST(EigenEqual, FailValue) {
  Mat a = Mat(3, 4);
  a.setRandom();
  Mat b = a;
  a(0, 0) += 1e-3;
  EXPECT_FALSE(dyn::EigenEqual(a, b));
}

TEST(Hom, CheckNaN) {
  dynamics::Hom a = dynamics::Hom::Identity();
  a(0, 0) = nan("");
  EXPECT_FALSE(dyn::TestHom(a));
}

TEST(Hom, CheckFinite) {
  dynamics::Hom a = dynamics::Hom::Identity();
  a(1, 3) = std::numeric_limits<double>::infinity();
  EXPECT_FALSE(dyn::TestHom(a));
}

TEST(Hom, CheckOrthogonal) {
  dynamics::Hom a = dynamics::Hom::Identity();
  a(0, 0) = 2.0;
  EXPECT_FALSE(dyn::TestHom(a));
}

TEST(Hom, CheckBottomRow) {
  dynamics::Hom a = dynamics::Hom::Identity();
  a(3, 0) = 2.0;
  EXPECT_FALSE(dyn::TestHom(a));
}

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
