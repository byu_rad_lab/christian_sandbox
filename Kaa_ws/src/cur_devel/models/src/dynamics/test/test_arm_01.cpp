// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <eigen3/Eigen/Dense>
#include <tuple>
#include <vector>

#include "dynamics/arm.h"
#include "dynamics/joint_rot.h"
#include "dynamics/link.h"
#include "dynamics/testing.h"
#include "dynamics/types.h"
#include "gtest/gtest.h"

using std::cout;
using std::endl;

namespace dyn = dynamics;

typedef std::tuple<double, double, double, bool> Input;

class ArmConfig01 {
 public:
  dyn::Arm arm_;

  double th_ = 0;
  double thd_ = 0;
  double thdd_ = 0;
  bool use_grasped_inertia_ = false;

  dyn::Mat inertia_mat_ = dyn::Mat::Zero(1, 1);

  std::vector<double> masses_ = {2.0, 3.0, 5.0};
  std::vector<double> x_prev_next_{4.0, 7.0, 2.0};
  std::vector<double> x_to_mass_{3.0, 4.0, 6.0};

  std::vector<dyn::Vec3> inertias_;

  // Stuff to solve for
  std::vector<dyn::Hom> g_world_links_;

  dyn::Jac jac_link_body_;
  dyn::Jac jac_spatial_;
  dyn::Jac jac_tool_body_;
  dyn::Jac jac_tool_spatial_;
  dyn::Jac jac_tool_hybrid_;
  dyn::Jac jac_tool_body_offset_;

  // tool offset in x and rotated about x axis by 90 deg.
  double x_tool_offset_ = 0.5;
  double rot_x_tool_offset_ = M_PI / 2.0;
  dyn::Hom hom_tool_offset_;

  std::vector<dyn::Twist> wrenches_;
  std::vector<dyn::Twist> vels_;
  std::vector<dyn::Twist> accs_;

  dyn::Vec tau_;

  // double dx0_ = x_prev_next_[0];
  double dx1_ = x_prev_next_[1];
  double dxt_ = x_prev_next_[2];

  double rx0_ = x_to_mass_[0];
  double rx1_ = x_to_mass_[1];
  double rxt_ = x_to_mass_[2];

  double m0g = dyn::kGravity * masses_[0];
  double m1g = dyn::kGravity * masses_[1];
  double m2g = dyn::kGravity * masses_[2];

  ArmConfig01(void) { SetUp(); }

  void Initalize(void) {
    inertias_.push_back(dyn::Vec3(2.0, 3.0, 5.0));  // link 0
    inertias_.push_back(dyn::Vec3(7.0, 5.0, 2.0));  // link 1
    inertias_.push_back(dyn::Vec3(7.0, 3.0, 3.0));  // tool

    for (int i = 0; i < 2; i++) {
      wrenches_.push_back(dyn::Twist::Zero());
      vels_.push_back(dyn::Twist::Zero());
      accs_.push_back(dyn::Twist::Zero());
      g_world_links_.push_back(dyn::Hom::Identity());
    }
    g_world_links_.push_back(dyn::Hom::Identity());  // for tool
  }

  void SetInertiaTool(void) {
    arm_.GetLink(-1).SetInertiaMassVecIcmDiag(
        masses_[2], dyn::Vec3(x_to_mass_[2], 0.0, 0.0), inertias_[2]);
  }

  dyn::Hom HomDeltaX(double dx) {
    return dyn::HomFromRvecVec(dyn::Vec3::Zero(), dyn::Vec3(dx, 0.0, 0.0));
  }

  dyn::Hom MakeG(double th, double x, double y) {
    dyn::Hom g = dyn::Hom::Identity();
    g(0, 0) = cos(th);
    g(0, 1) = -sin(th);
    g(1, 0) = sin(th);
    g(1, 1) = cos(th);
    g(0, 3) = x;
    g(1, 3) = y;
    return g;
  }

  void CalcFK(void) {
    double dx0_ = x_prev_next_[0];
    double dx1_ = x_prev_next_[1];
    double dxt_ = x_prev_next_[2];
    g_world_links_[0] = MakeG(0.0, dx0_, 0.0);
    g_world_links_[1] = MakeG(th_, dx0_ + dx1_, 0.0);
    g_world_links_[2] = MakeG(th_, dx0_ + dx1_ + dxt_ * cos(th_), dxt_ * sin(th_));
  }

  void CalcJac(void) {
    double dx0_ = x_prev_next_[0];
    double dx1_ = x_prev_next_[1];
    double dxt_ = x_prev_next_[2];

    jac_link_body_ = dyn::Jac::Zero(6, 1);
    jac_link_body_(5) = 1.0;

    jac_tool_body_ = dyn::Jac::Zero(6, 1);
    jac_tool_body_(5) = 1.0;
    jac_tool_body_(1) = dxt_;

    jac_spatial_ = dyn::Jac::Zero(6, 1);
    jac_spatial_(5) = 1.0;
    jac_spatial_(1) = -(dx0_ + dx1_);

    jac_tool_hybrid_ = dyn::Jac::Zero(6, 1);
    jac_tool_hybrid_(5) = 1.0;
    jac_tool_hybrid_(0) = -dxt_ * sin(th_);
    jac_tool_hybrid_(1) = dxt_ * cos(th_);

    hom_tool_offset_ = dyn::HomFromRvecVec(dyn::Vec3(rot_x_tool_offset_, 0.0, 0.0),
                                           dyn::Vec3(x_tool_offset_, 0.0, 0.0));

    jac_tool_body_offset_ = dyn::Jac::Zero(6, 1);
    jac_tool_body_offset_(4) = 1.0;  // positive y rotation
    jac_tool_body_offset_(2) = -(dxt_ + x_tool_offset_);
  }

  void CalcVels(void) {
    // vels_[0] remains zero
    vels_[1](5) = thd_;
  }

  void CalcAccs(void) {
    // accs_[0] remains zero
    accs_[1](5) = thdd_;
  }

  void CalcWrenches(void) {
    // wrenches are *in the link frame*
    CalcWrenchesGrav();
    CalcWrenchesAcc();
    CalcWrenchesVel();
  }

  void CalcWrenchesGrav(void) {
    // Gravity terms
    wrenches_[1](2) += m1g;
    wrenches_[1](4) += -m1g * rx1_;

    // link 0 self mass
    wrenches_[0](2) += m0g;
    wrenches_[0](4) += -m0g * rx0_;

    // link 0, wrenches from link 1 mass
    wrenches_[0](2) += m1g;

    wrenches_[0](3) += m1g * rx1_ * sin(th_);
    wrenches_[0](4) += -m1g * (dx1_ + rx1_ * cos(th_));

    // Gravity terms, tool inertia
    if (use_grasped_inertia_) {
      wrenches_[0](2) += m2g;
      wrenches_[0](3) += m2g * (dxt_ + rxt_) * sin(th_);
      wrenches_[0](4) += -m2g * (dx1_ + (dxt_ + rxt_) * cos(th_));

      wrenches_[1](2) += m2g;
      wrenches_[1](4) += -m2g * (dxt_ + rxt_);
    }
  }

  void CalcWrenchesAcc(void) {
    // Acceleration terms
    wrenches_[1](5) += thdd_ * (inertias_[1](2) + rx1_ * rx1_ * masses_[1]);
    wrenches_[1](1) += thdd_ * masses_[1] * rx1_;  // +y reaction

    // torque from link 1
    wrenches_[0](5) += thdd_ * (inertias_[1](2) + rx1_ * rx1_ * masses_[1]);

    // torque due to reaction force at link 1, dx1_ is lever arm
    wrenches_[0](5) += thdd_ * masses_[1] * rx1_ * cos(th_) * dx1_;

    wrenches_[0](1) += thdd_ * masses_[1] * rx1_ * cos(th_);   // link 1 reaction, rotated
    wrenches_[0](0) += -thdd_ * masses_[1] * rx1_ * sin(th_);  // link 1 reaction, rotated

    // Acceleration terms, tool inertia
    if (use_grasped_inertia_) {
      double rtot = (dxt_ + rxt_);

      wrenches_[1](5) += thdd_ * (inertias_[2](2) + rtot * rtot * masses_[2]);
      wrenches_[1](1) += thdd_ * masses_[2] * rtot;  // +y reaction

      // torque from link 1
      wrenches_[0](5) += thdd_ * (inertias_[2](2) + rtot * rtot * masses_[2]);

      // torque due to reaction force at link 1, dx1_ is lever arm
      wrenches_[0](5) += thdd_ * masses_[2] * rtot * cos(th_) * dx1_;

      // link 1 reaction forces, rotated
      wrenches_[0](1) += thdd_ * masses_[2] * rtot * cos(th_);
      wrenches_[0](0) += -thdd_ * masses_[2] * rtot * sin(th_);
    }
  }

  // Velocity terms
  void CalcWrenchesVel(void) {
    double moment_arm = dx1_ * sin(th_);
    double force_centrip = masses_[1] * thd_ * thd_ * rx1_;
    wrenches_[1](0) += -force_centrip;

    wrenches_[0](0) += -cos(th_) * force_centrip;
    wrenches_[0](1) += -sin(th_) * force_centrip;
    wrenches_[0](5) += -moment_arm * force_centrip;

    if (use_grasped_inertia_) {
      force_centrip = masses_[2] * thd_ * thd_ * (dxt_ + rxt_);
      wrenches_[1](0) += -force_centrip;

      wrenches_[0](0) += -cos(th_) * force_centrip;
      wrenches_[0](1) += -sin(th_) * force_centrip;
      wrenches_[0](5) += -moment_arm * force_centrip;
    }
  }

  void CalcTau(void) {
    inertia_mat_(0, 0) = masses_[1] * rx1_ * rx1_;
    inertia_mat_(0, 0) += inertias_[1](2);

    if (use_grasped_inertia_) {
      inertia_mat_(0, 0) += masses_[2] * (dxt_ + rxt_) * (dxt_ + rxt_);
      inertia_mat_(0, 0) += inertias_[2](2);
    }
    tau_ = inertia_mat_ * arm_.qdd();
  }

  void SetState(double q, double qd, double qdd, bool use_grasped_inertia) {
    th_ = q;
    thd_ = qd;
    thdd_ = qdd;
    use_grasped_inertia_ = use_grasped_inertia;
    dyn::Vec vec(1);

    vec(0) = th_;
    arm_.set_q(vec);

    vec(0) = thd_;
    arm_.set_qd(vec);

    vec(0) = thdd_;
    arm_.set_qdd(vec);

    if (use_grasped_inertia_) {
      arm_.GetLink(-1).SetInertiaMassVecIcmDiag(
          masses_[2], dyn::Vec3(x_to_mass_[2], 0.0, 0.0), inertias_[2]);
    } else {
      arm_.GetLink(-1).SetInertiaZero();
    }

    Calculate();
  }

  void Calculate(void) {
    CalcFK();
    CalcJac();
    CalcVels();
    CalcAccs();
    CalcWrenches();
    CalcTau();
  }

  void SetUp(void) {
    Initalize();

    ASSERT_EQ(masses_.size(), 3ul);
    ASSERT_EQ(x_prev_next_.size(), 3ul);
    ASSERT_EQ(inertias_.size(), 3ul);
    ASSERT_EQ(wrenches_.size(), 2ul);
    ASSERT_EQ(vels_.size(), 2ul);
    ASSERT_EQ(accs_.size(), 2ul);

    arm_ = dyn::Arm(HomDeltaX(x_prev_next_[0]));
    dyn::Link link1 = dyn::Link(dyn::JointRot(), HomDeltaX(x_prev_next_[1]));
    arm_.AddLink(link1);
    arm_.AddLinkTool(HomDeltaX(x_prev_next_[2]));

    for (int i = 0; i < 2; i++) {
      arm_.GetLink(i).SetInertiaMassVecIcmDiag(
          masses_[i], dyn::Vec3(x_to_mass_[i], 0.0, 0.0), inertias_[i]);
    }
  }
};

class ArmConfig01Test : public ::testing::Test {
 public:
  ArmConfig01 ac_ = ArmConfig01();
  dyn::Arm& arm_ = ac_.arm_;

  virtual void SetUp() {}

  Input input_;
};

class ArmConfig01TestP : public ::testing::TestWithParam<Input> {
 public:
  ArmConfig01 ac_ = ArmConfig01();
  dyn::Arm& arm_ = ac_.arm_;

  virtual void SetUp() {
    input_ = GetParam();
    ac_.SetState(std::get<0>(input_),
                 std::get<1>(input_),
                 std::get<2>(input_),
                 std::get<3>(input_));
  }

  Input input_;
};

TEST_F(ArmConfig01Test, NumDof) { EXPECT_EQ(arm_.num_dof(), 1); }

TEST_F(ArmConfig01Test, Sizes) {
  arm_.SetStateZero();
  EXPECT_EQ(arm_.links().size(), 3ul);
  EXPECT_EQ(arm_.q().size(), 1);
  EXPECT_EQ(arm_.jac_spatial().rows(), 6);
  EXPECT_EQ(arm_.jac_spatial().cols(), 1);
}

TEST_F(ArmConfig01Test, SetStatesOK) {
  arm_.set_q(dyn::Vec::Zero(1));
  arm_.set_qd(dyn::Vec::Zero(1));
  arm_.set_qdd(dyn::Vec::Zero(1));
}

TEST_F(ArmConfig01Test, SetStatesFail) {
  dyn::Vec q1(2);
  EXPECT_THROW(arm_.set_q(q1), dyn::ExceptionMatrixSize);
  EXPECT_THROW(arm_.set_qd(dyn::Vec::Zero(4)), dyn::ExceptionMatrixSize);
  EXPECT_THROW(arm_.set_qdd(dyn::Vec3::Zero()), dyn::ExceptionMatrixSize);
}

TEST_F(ArmConfig01Test, SetZeroAndRandom) {
  dyn::Vec vec_zero = dyn::Vec::Zero(arm_.num_dof());
  dyn::Vec vec_rand = dyn::Vec(arm_.num_dof());

  vec_rand.setRandom();
  arm_.set_q(vec_rand);
  ASSERT_TRUE(dyn::EigenEqual(arm_.q(), vec_rand));
  arm_.SetQZero();
  ASSERT_TRUE(dyn::EigenEqual(arm_.q(), vec_zero));
  arm_.SetQRandom();
  ASSERT_FALSE(dyn::EigenEqual(arm_.q(), vec_zero));

  vec_rand.setRandom();
  arm_.set_qd(vec_rand);
  ASSERT_TRUE(dyn::EigenEqual(arm_.qd(), vec_rand));
  arm_.SetQdZero();
  ASSERT_TRUE(dyn::EigenEqual(arm_.qd(), vec_zero));
  arm_.SetQdRandom();
  ASSERT_FALSE(dyn::EigenEqual(arm_.qd(), vec_zero));

  vec_rand.setRandom();
  arm_.set_qdd(vec_rand);
  ASSERT_TRUE(dyn::EigenEqual(arm_.qdd(), vec_rand));
  arm_.SetQddZero();
  ASSERT_TRUE(dyn::EigenEqual(arm_.qdd(), vec_zero));
  arm_.SetQddRandom();
  ASSERT_FALSE(dyn::EigenEqual(arm_.qdd(), vec_zero));
}

TEST_F(ArmConfig01Test, AddingLinks) {
  dyn::Arm arm = dyn::Arm();
  EXPECT_NO_THROW({
    for (int i = 0; i < dyn::kNumDofMax; i++) {
      arm.AddLink(dyn::Link(dyn::JointRot()));
    }
  });
  EXPECT_THROW(arm.AddLink(dyn::Link(dyn::JointRot())), dyn::ExceptionDofMaxExceeded);
}

TEST_F(ArmConfig01Test, GetLink) {
  ASSERT_EQ(&arm_.GetLink(0), &arm_.GetLink(-3));
  ASSERT_EQ(&arm_.GetLink(1), &arm_.GetLink(-2));
  ASSERT_EQ(&arm_.GetLink(2), &arm_.GetLink(-1));
  EXPECT_THROW(arm_.GetLink(3), dyn::ExceptionIndexingError);
  EXPECT_THROW(arm_.GetLink(-4), dyn::ExceptionIndexingError);
}

TEST_F(ArmConfig01Test, Exercise) {
  arm_.CalcFK();
  arm_.CalcJac();
  arm_.CalcID();
  arm_.CalcIDStatic();
  dyn::Vec tau(1);
  tau.setZero();
  arm_.CalcFD(tau);
}

// Parameterized testing
TEST_P(ArmConfig01TestP, FK) {
  arm_.CalcFK();
  ASSERT_TRUE(dyn::EigenEqual(arm_.GetLink(0).g_world_link(), ac_.g_world_links_[0]));
  ASSERT_TRUE(dyn::EigenEqual(arm_.GetLink(1).g_world_link(), ac_.g_world_links_[1]));
  ASSERT_TRUE(dyn::EigenEqual(arm_.GetLink(2).g_world_link(), ac_.g_world_links_[2]));
}

TEST_P(ArmConfig01TestP, JacHandCalc) {
  arm_.CalcJac();
  ASSERT_TRUE(dyn::EigenEqual(arm_.JacLink(1), ac_.jac_link_body_));
  ASSERT_TRUE(dyn::EigenEqual(arm_.JacLink(-1), ac_.jac_tool_body_));
  ASSERT_TRUE(dyn::EigenEqual(arm_.JacLink(-1, dyn::Frame::Spatial), ac_.jac_spatial_));
  ASSERT_TRUE(dyn::EigenEqual(arm_.jac_spatial(), ac_.jac_spatial_));
  ASSERT_TRUE(
      dyn::EigenEqual(arm_.JacLink(-1, dyn::Frame::Hybrid), ac_.jac_tool_hybrid_));
  ASSERT_TRUE(dyn::EigenEqual(arm_.JacLink(-1, dyn::Frame::Body, ac_.hom_tool_offset_),
                              ac_.jac_tool_body_offset_));
}

TEST_P(ArmConfig01TestP, JacSelfConsistent) {
  arm_.CalcJac();
  // Eigen doesn't like this
  // ASSERT_TRUE(dyn::EigenEqual(arm_.JacLink(0), dyn::Jac::Zero(6, 0)));
  ASSERT_TRUE(dyn::EigenEqual(arm_.JacLink(1), arm_.JacLink(1, dyn::Frame::Body)));
  ASSERT_TRUE(dyn::EigenEqual(arm_.JacLink(-1), arm_.JacLink(-1, dyn::Frame::Body)));

  // Spatial Jacobian should be the same for last link, tool, and either with offset
  ASSERT_TRUE(dyn::EigenEqual(arm_.jac_spatial(), arm_.JacLink(-1, dyn::Frame::Spatial)));
  ASSERT_TRUE(dyn::EigenEqual(arm_.JacLink(-1, dyn::Frame::Spatial),
                              arm_.JacLink(1, dyn::Frame::Spatial)));
  ASSERT_TRUE(dyn::EigenEqual(arm_.JacLink(-1, dyn::Frame::Spatial, ac_.hom_tool_offset_),
                              arm_.JacLink(1, dyn::Frame::Spatial)));
  ASSERT_TRUE(dyn::EigenEqual(arm_.JacLink(-1, dyn::Frame::Spatial, ac_.hom_tool_offset_),
                              arm_.jac_spatial()));
}

TEST_P(ArmConfig01TestP, Vels) {
  arm_.CalcID();
  ASSERT_TRUE(dyn::EigenEqual(arm_.GetLink(0).vel(), ac_.vels_[0]));
  ASSERT_TRUE(dyn::EigenEqual(arm_.GetLink(1).vel(), ac_.vels_[1]));
}

TEST_P(ArmConfig01TestP, Accs) {
  arm_.CalcID();
  ASSERT_TRUE(dyn::EigenEqual(arm_.GetLink(0).acc(), ac_.accs_[0]));
  ASSERT_TRUE(dyn::EigenEqual(arm_.GetLink(1).acc(), ac_.accs_[1]));
}

TEST_P(ArmConfig01TestP, Wrenches) {
  arm_.CalcID();
  ASSERT_TRUE(dyn::EigenEqual(arm_.GetLink(0).wrench(), ac_.wrenches_[0]));
  ASSERT_TRUE(dyn::EigenEqual(arm_.GetLink(1).wrench(), ac_.wrenches_[1]));
}

TEST_P(ArmConfig01TestP, FDVsID) {
  dyn::Vec qdd = arm_.qdd();
  dyn::Vec tau = arm_.CalcID();
  dyn::Vec qdd_calc = arm_.CalcFD(tau);

  ASSERT_TRUE(dyn::EigenEqual(arm_.inertia_mat(), ac_.inertia_mat_));
  ASSERT_TRUE(dyn::EigenEqual(tau, ac_.tau_));
  ASSERT_TRUE(dyn::EigenEqual(qdd, qdd_calc));  // make sure qdd is recovered
}

TEST_P(ArmConfig01TestP, CalcIDStatic) {
  if (fabs(ac_.thdd_) < 1e-12 && fabs(ac_.thd_) < 1e-12) {
    ASSERT_TRUE(dyn::EigenEqual(arm_.CalcID(), arm_.CalcIDStatic()));
  }
}

// Could use ::testing::Combine here, but appears fragile
INSTANTIATE_TEST_CASE_P(ParameterizedTesting,
                        ArmConfig01TestP,
                        ::testing::Values(                          //
                            std::make_tuple(0.0, 0.0, 0.0, false),  //
                            std::make_tuple(0.5, 0.0, 0.0, false),  //
                            std::make_tuple(0.0, 0.2, 0.0, false),  //
                            std::make_tuple(0.5, 0.2, 0.0, false),  //
                            std::make_tuple(0.0, 0.0, 0.3, false),  //
                            std::make_tuple(0.5, 0.0, 0.3, false),  //
                            std::make_tuple(0.0, 0.4, 0.3, false),  //
                            std::make_tuple(0.5, 0.4, 0.3, false),  //
                            std::make_tuple(0.0, 0.0, 0.0, true),   //
                            std::make_tuple(0.5, 0.0, 0.0, true),   //
                            std::make_tuple(0.0, 0.2, 0.0, true),   //
                            std::make_tuple(0.5, 0.2, 0.0, true),   //
                            std::make_tuple(0.0, 0.0, 0.3, true),   //
                            std::make_tuple(0.5, 0.0, 0.3, true),   //
                            std::make_tuple(0.0, 0.4, 0.3, true),   //
                            std::make_tuple(0.5, 0.4, 0.3, true)    //
                            ));  //  NOLINT(whitespace/parens)

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
