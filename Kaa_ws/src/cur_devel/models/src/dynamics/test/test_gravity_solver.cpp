// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <eigen3/Eigen/Dense>
#include <vector>

#include "dynamics/gravity_solver.h"
#include "dynamics/joint_flexus.h"
#include "dynamics/joint_rot.h"
#include "dynamics/testing.h"
#include "dynamics/types.h"
#include "gtest/gtest.h"

namespace dyn = dynamics;

TEST(JointFlexus, TestManyConfigs) {
  double tol = 2.0 * 3.14 / 180.0;
  double joint_height = 0.20;
  dyn::JointFlexus joint = dyn::JointFlexus(joint_height);
  joint.limits().SetLimitsCircular(1.5, 16);
  dyn::GravitySolver solver(joint);

  // first a simple setup
  // joint straight, base vector is [0, 0, 1]
  dyn::Vec3 vec_joint, vec_link;

  dyn::Vec q_true = dyn::Vec::Zero(joint.num_dof());
  dyn::Vec q_guess = dyn::Vec::Zero(joint.num_dof());
  dyn::Vec q_soln = dyn::Vec::Zero(joint.num_dof());

  vec_joint = dyn::Vec3(0.0, 0.0, 1.0);
  vec_link = dyn::RotFromHom(joint.g_joint_link()).transpose() * vec_joint;
  dyn::EigenEqual(vec_link, dyn::Vec3::Zero());

  joint.set_q(q_true);
  q_guess.setRandom();
  q_soln = solver.Solve(vec_joint, vec_link, q_guess);
  ASSERT_TRUE(dyn::EigenEqual(q_soln, dyn::Vec::Zero(joint.num_dof()), tol));
  ASSERT_TRUE(dyn::EigenEqual(q_soln, q_true, tol));

  // Create a few cases
  std::vector<dyn::Vec3> inputs;
  inputs.push_back(dyn::Vec3(-0.1, 0.2, 0.3));
  inputs.push_back(dyn::Vec3(-0.6, 0.7, -0.3));
  inputs.push_back(dyn::Vec3(1.05, 0.0, -1.0));
  inputs.push_back(dyn::Vec3(2.0, 0.0, -1.0));
  inputs.push_back(dyn::Vec3(2.0, 2.0, -1.0));
  inputs.push_back(dyn::Vec3(0.0, 5.0, 1.0));
  inputs.push_back(dyn::Vec3(0.0, 1.0, 0.0));

  srand(0);
  // int num_angles = static_cast<int>(1e5);  // more thorough but slow
  int num_angles = 500;
  int num_total;
  int num_skipped;
  double check_val;

  // Some configurations give poor results.
  // Reducing opt_.set_xtol_rel(tol) to 1e-12 solves almost everything
  // but probably isn't very useful in practice.
  // Instead, skip some cases
  for (unsigned int i = 0; i < inputs.size(); i++) {
    num_skipped = 0;
    num_total = 0;
    vec_joint = inputs[i].normalized();
    // Test at a few angles
    for (int j = 0; j < num_angles; j++) {
      num_total++;
      joint.SetQRandom();
      joint.CalcFK();
      vec_link = dyn::RotFromHom(joint.g_joint_link()).transpose() * vec_joint;

      check_val = solver.TrustCoeff(vec_joint, vec_link, q_true);
      if (check_val < 0.025) {
        num_skipped++;
        continue;
      }

      q_true = joint.q();
      q_guess.setZero();
      q_soln = solver.Solve(vec_joint, vec_link, q_guess);

      if (dyn::EigenEqual(q_soln, q_true, tol) == ::testing::AssertionFailure()) {
        joint.set_q(q_soln);
        joint.CalcFK();
        dyn::Vec3 vec_calc =
            dyn::RotFromHom(joint.g_joint_link()).transpose() * vec_joint;
        std::cout << "input: " << inputs[i].transpose() << std::endl;
        std::cout << "Iteration: " << j << std::endl;
        std::cout << "q_true: " << q_true.transpose()
                  << "\nq_guess: " << q_guess.transpose() << std::endl;
        std::cout << "vec_joint: " << vec_joint.transpose()
                  << "\n vec_link: " << vec_link.transpose() << std::endl;
        std::cout << " vec_calc: " << vec_calc.transpose() << std::endl;
        std::cout << " check_val: " << check_val << std::endl;
        // std::cout << "stoping code is: " << solver.opt().last_optimize_result() <<
        // std::endl;
        std::cout << std::endl;
        EXPECT_TRUE(dyn::EigenEqual(q_guess, q_true, tol));
      }
    }

    double fraction_solved =
        (1.0 - static_cast<double>(num_skipped) / static_cast<double>(num_total));
    ASSERT_GT(fraction_solved, 0.93);
    // std::cout << "num_skipped: " << num_skipped << " num_total: " << num_total
    //           << "\nSolved " << fraction_solved * 100.0 << "%." << std::endl;
  }
}

// TODO(Tom): add check for JointRot, check angles mod 2*pi
// also check construction with joint pointer

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
