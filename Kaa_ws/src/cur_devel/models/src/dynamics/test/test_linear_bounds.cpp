// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <stdio.h>
#include <eigen3/Eigen/Dense>
#include <iostream>
#include "gtest/gtest.h"
#include "dynamics/types.h"
#include "dynamics/testing.h"
#include "dynamics/linear_bounds.h"

using dynamics::Vec;
using dynamics::Mat;
using dynamics::LinearBounds;
using std::cout;
using std::endl;

namespace dyn = dynamics;

class IneqConstCircular : public ::testing::Test {
 public:
  int num_dof_ = 2;
  LinearBounds lb_ = LinearBounds(num_dof_);
  double radius_ = 1.3;
  int num_constraints_ = 8;
  Vec center_ = Vec::Zero(2);
  double phase_ = 0.0;
  Mat a_;
  Vec b_;
  Vec boxl_, boxu_;

  virtual void SetUp() {
    lb_.SetLimitsCircular(radius_, num_constraints_, center_, phase_);
    a_ = lb_.a();
    b_ = lb_.b();
    boxl_ = lb_.box_lower();
    boxu_ = lb_.box_upper();
  }

  virtual void TearDown() {}
};

TEST_F(IneqConstCircular, Sizes) {
  EXPECT_EQ(a_.cols(), num_dof_);
  EXPECT_EQ(a_.rows(), b_.size());
}

TEST_F(IneqConstCircular, PointIn) {
  Vec q1 = Vec(2);
  q1 << 0.2, 0.5;
  EXPECT_LT(lb_.CheckLimitsWorst(q1), 0.0);  // inside
  Vec q2 = Vec(2);
  q2 << 10.2, 0.5;
  EXPECT_GT(lb_.CheckLimitsWorst(q2), 0.0);  // outside
}

TEST_F(IneqConstCircular, Box) {
  EXPECT_EQ(boxl_(0), -radius_);
  EXPECT_EQ(boxl_(1), -radius_);
  EXPECT_EQ(boxu_(0), radius_);
  EXPECT_EQ(boxu_(1), radius_);
}

TEST_F(IneqConstCircular, Random01) {
  Vec q1 = lb_.GetQRandom();
  Vec q2 = lb_.GetQRandom();
  EXPECT_NE(q1(0), q2(0));
}

TEST_F(IneqConstCircular, Random02) {
  double rmax = radius_ / cos(M_PI / num_constraints_);
  double r;
  Vec q;
  for (int i = 0; i < 1000; i++) {
    q = lb_.GetQRandom();
    r = q.norm();
    EXPECT_LE(r, rmax);
  }
}

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
