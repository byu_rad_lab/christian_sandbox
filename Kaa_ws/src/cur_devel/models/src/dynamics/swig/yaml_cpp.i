// Potential license issue:
// Copied mostly from yaml-cpp.
// Not sure how much of this works.

#ifndef PYTHON_SWIG_YAML_CPP_I_
#define PYTHON_SWIG_YAML_CPP_I_

%{
  template<typename T>
  T YamlConvert(YAML::Node node) {
    return node.as<T>();
  }
%}

%warnfilter(315) Convert;
%warnfilter(315) YamlCheckAndConvert;

template<typename T>
T YamlConvert(YAML::Node node) {
  return node.as<T>();
}

namespace YAML {

template<typename T>
struct convert;  // prevents errors, unused(?)


%rename(YamlLoadFile) LoadFile(const std::string&);  // because we lose the YAML namespace
%rename(YamlLoad) Load(const std::string&);  // because we lose the YAML namespace

%rename(YamlNode) Node;
%rename(is_) Node::is;

class Node {
  public:

  typedef YAML::iterator iterator;
  typedef YAML::const_iterator const_iterator;

  Node();
  // explicit Node(NodeType::value type);
  // template <typename T>
  // explicit Node(const T& rhs);
  // explicit Node(const detail::iterator_value& rhs);
  Node(const Node& rhs);
  ~Node();

  // NodeType::value Type() const;
  // bool IsDefined() const;
  // bool IsNull() const { return Type() == NodeType::Null; }
  // bool IsScalar() const { return Type() == NodeType::Scalar; }
  // bool IsSequence() const { return Type() == NodeType::Sequence; }
  // bool IsMap() const { return Type() == NodeType::Map; }

  // bool conversions
  // YAML_CPP_OPERATOR_BOOL();
  // bool operator!() const { return !IsDefined(); }

  // access
  template <typename T>
  const T as() const;
  template <typename T, typename S>
  const T as(const S& fallback) const;
  const std::string& Scalar() const;
  const std::string& Tag() const;
  void SetTag(const std::string& tag);

  // assignment
  bool is(const Node& rhs) const;
  template <typename T>
  // Node& operator=(const T& rhs);
  // Node& operator=(const Node& rhs);
  void reset(const Node& rhs = Node());

  // size/iterator
  std::size_t size() const;

  iterator begin();
  iterator end();

  // sequence
  template <typename T>
  void push_back(const T& rhs);
  void push_back(const Node& rhs);

  // indexing
  // template <typename Key>
  // const Node operator[](const Key& key) const;
  // template <typename Key>
  // Node operator[](const Key& key);
  // template <typename Key>
  // bool remove(const Key& key);

  // const Node operator[](const Node& key) const;
  // Node operator[](const Node& key);
  bool remove(const Node& key);

  // map
  template <typename Key, typename Value>
  void force_insert(const Key& key, const Value& value);

  private:
  enum Zombie { ZombieNode };
  explicit Node(Zombie);
  explicit Node(detail::node& node, detail::shared_memory_holder pMemory);

  void EnsureNodeExists() const;

  template <typename T>
  void Assign(const T& rhs);
  void Assign(const char* rhs);
  void Assign(char* rhs);

  void AssignData(const Node& rhs);
  void AssignNode(const Node& rhs);

  private:
  bool m_isValid;
  mutable detail::shared_memory_holder m_pMemory;
  mutable detail::node* m_pNode;
};

Node LoadFile(const std::string&);
Node Load(const std::string&);

} // namespace YAML

#endif // PYTHON_SWIG_YAML_CPP_I_
