from ego_atheris import dynamics as dyn
import numpy as np
import copy


def np_check(a, b, tol=1e-12):
    assert(a.shape == b.shape)
    assert np.max(np.abs(a - b)) < tol, \
        "np_check matrices differ by %3e > %3e." % (np.max(np.abs(a - b)), tol)


def test_demo():
    joint = dyn.JointRot()
    joint.set_q(np.array([0.5]))
    joint.CalcFK()
    g = joint.g_joint_link()
    print("joint:", joint)
    print("g:")
    print(g)

    joint.set_q(np.array([0.5]))
    joint.CalcJac()
    g = joint.g_joint_link()
    jac = joint.jac()
    print("joint:", joint)
    print("g:")
    print(g)
    print("jac:")
    print(jac)

    link = dyn.Link(joint)

    arm = dyn.Arm(np.eye(4))
    arm.AddLink(link)
    arm.AddLinkTool(dyn.HomTransFromXYZ(1.0, 0.0, 0.0))
    arm.SetStateZero()
    arm.CalcFK()
    arm.CalcJac()

    print('"g_world_tool":')
    print(arm.GetLink(-1).g_world_link())

    arm.set_qdd(np.ones(arm.num_dof()))
    tau = arm.CalcID()
    print("tau:", tau.T)

    qdd = arm.CalcFD(np.zeros(arm.num_dof()))
    print("qdd:", qdd)

    jac_tool = arm.JacLink(-1)
    print("jac_tool:")
    print(jac_tool)

    jac_tool_2 = arm.JacLink(-1, dyn.Hybrid, np.eye(4))
    print("jac_tool_2:")
    print(jac_tool_2)

    jac_link = arm.JacLink(1, dyn.Hybrid, np.eye(4))
    print("jac_link:")
    print(jac_link)

    jac_link_2 = arm.JacLink(1)
    print("jac_link_2:")
    print(jac_link_2)


def test_types():
    rvec = np.array([0.5, 0.0, 0.0])
    vec = np.array([0.2, 0.3, 0.5])
    rot = dyn.RotFromRvec(rvec)
    hom = dyn.HomFromRotVec(rot, vec)
    np_check(hom[:3, :3], rot)
    np_check(hom[:3, 3], vec)


def test_hom_inv():
    rvec = np.array([0.2, -0.3, 0.5])
    vec = np.array([0.5, 0.9, -0.8])
    hom = dyn.HomFromRvecVec(rvec, vec)
    inv = dyn.HomInv(hom)
    np_check(np.dot(hom, inv), np.eye(4))


def test_joint_rot():
    joint = dyn.JointRot()
    joint.set_q(np.array([0.0]))
    joint.CalcFK()
    np_check(joint.g_joint_link(), np.eye(4))

    joint.CalcJac()
    jac = np.zeros((6, 1))
    jac[5, 0] = 1.0
    np_check(joint.jac(), jac)

    th = 0.5
    joint.set_q(np.array([th]))
    joint.CalcFK()
    hom1 = joint.g_joint_link()
    joint.set_q(np.array([th]))
    joint.CalcFK()
    hom2 = joint.g_joint_link()
    np_check(hom1, hom2)


def test_link_01():
    joint = dyn.JointRot()
    link1 = dyn.Link()
    link2 = dyn.Link(joint)
    link1.CalcFKLink0()
    link2.CalcFKLink0()
    print("link1: ", link1)
    print("link2: ", link2)
    print(link1.joint())
    print(link1.g_world_joint())
    print(link1.g_world_link())


# make get arm func, test arm dyn
def test_arm_01():
    joint = dyn.JointRot()
    g_prox_joint = np.eye(4)
    g_prox_joint[0, 3] = 0.25
    link1 = dyn.Link(joint, g_prox_joint)

    g_prox_joint[0, 3] = 0.75
    link2 = dyn.Link(joint, g_prox_joint)

    arm = dyn.Arm()
    arm.AddLink(link1)
    arm.AddLink(link2)

    arm.AddLinkTool(dyn.HomTransFromXYZ(0.5, 0.0, 0.0))

    q = np.array([0.0, 0.0])
    arm.SetQAndCalcFK(q)  # convenience method
    arm.CalcIDStatic()
    return arm


# Barely a test, but exercises code
def test_joints():
    joint = dyn.JointRot()
    joint.set_q(np.array([0.5]))
    joint.CalcFK()
    g = joint.g_joint_link()
    print("joint:", joint)
    print("g:")
    print(g)

    joint.set_q(np.array([0.5]))
    joint.CalcJac()
    g = joint.g_joint_link()
    jac = joint.jac()
    print("joint:", joint)
    print("g:")
    print(g)
    print("jac:")
    print(jac)


def test_arm_copy():
    joint_ptr = dyn.JointPtr(dyn.JointRot())
    print("joint_ptr:")
    print(joint_ptr)
    joint_ptr_copy = copy.copy(joint_ptr)
    print("joint_ptr_copy:")
    print(joint_ptr_copy)


def test_joint_ptr():
    jp = dyn.JointPtr(dyn.JointFixed())
    print("jp:", jp)
    # jp = dyn.JointPtr()  # doesn't work
