#!/usr/bin/env python

from ego_atheris import dynamics as dyn
import numpy as np

if __name__ == "__main__":
    joint = dyn.JointRot()
    joint.set_q(np.array([0.5]))
    joint.CalcFK()
    g = joint.g_joint_link()
    print("joint:", joint)
    print("g:")
    print(g)

    joint.set_q(np.array([0.5]))
    joint.CalcJac()
    g = joint.g_joint_link()
    jac = joint.jac()
    print("joint:", joint)
    print("g:")
    print(g)
    print("jac:")
    print(jac)

    link = dyn.Link(joint)

    arm = dyn.Arm(np.eye(4))
    arm.AddLink(link)
    arm.AddLinkTool(dyn.HomTransFromXYZ(1.0, 0.0, 0.0))
    arm.SetStateZero()
    arm.CalcFK()
    arm.CalcJac()

    print("'g_world_tool':")
    print(arm.GetLink(-1).g_world_link())

    arm.set_qdd(np.ones(arm.num_dof()))
    tau = arm.CalcID()
    print("tau:", tau.T)

    qdd = arm.CalcFD(np.zeros(arm.num_dof()))
    print("qdd:", qdd)

    jac_tool = arm.JacLink(-1)
    print("jac_tool:")
    print(jac_tool)

    jac_tool_2 = arm.JacLink(-1, dyn.Frame_Hybrid, np.eye(4))
    print("jac_tool_2:")
    print(jac_tool_2)

    jac_link = arm.JacLink(1, dyn.Frame_Hybrid, np.eye(4))
    print("jac_link:")
    print(jac_link)

    jac_link_2 = arm.JacLink(1)
    print("jac_link_2:")
    print(jac_link_2)
