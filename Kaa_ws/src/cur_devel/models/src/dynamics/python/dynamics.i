%module dynamics

%{
  #define SWIG_FILE_WITH_INIT

  #include <eigen3/Eigen/Core>

  #include "primitives/exception.h"
  #include "primitives/yaml.h"
  #include "yaml-cpp/yaml.h"

  #include "dynamics/types.h"
  #include "dynamics/value_ref.h"
  #include "dynamics/linear_bounds.h"
  #include "dynamics/joint_base.h"
  #include "dynamics/joint_rot.h"
  #include "dynamics/joint_prism.h"
  #include "dynamics/joint_fixed.h"
  #include "dynamics/joint_coriolis.h"
  #include "dynamics/joint_flexus.h"
  #include "dynamics/joint_arquus.h"
  #include "dynamics/link.h"
  #include "dynamics/arm.h"
  #include "dynamics/yaml.h"
%}

%include <std_string.i>
%include <typemaps.i>
%include <std_vector.i>
%include <exception.i>

%include <../swig/numpy.i>
%init %{
  import_array();  // required for Numpy
%}

%include <../swig/eigen.i>
%include <../swig/yaml_cpp.i>

// Typemaps have to come before %include "types.h"
// Don't typemap Eigen::Matrix3d *and* dynamics::Rot
// Just typedef Eigen::Matrix3d, which will work for Rot
%eigen_typemaps(Eigen::Dynamic)
%eigen_typemaps(dynamics::Vec)
%eigen_typemaps(Eigen::Vector3d)  // works for Vec3, too
%eigen_typemaps(Eigen::Vector4d)  // works for Vec3, too
%eigen_typemaps(Eigen::VectorXd)  // works for linear bounds VecXd for b
%eigen_typemaps(dynamics::Twist)

%eigen_typemaps(dynamics::Mat)
%eigen_typemaps(Eigen::Matrix3d)  // works for Mat, InertiaTensor
%eigen_typemaps(Eigen::Matrix4d)  // works for Hom
%eigen_typemaps(Eigen::MatrixXd)  // works for linear bounds MatXd for A
%eigen_typemaps(dynamics::Jac)

// Need a single type for Adj and Inertia
// SWIG doesn't seem to like the template argument in eigen_typemaps
// %eigen_typemaps(dynamics::Adj)
%eigen_typemaps(dynamics::Mat66)

%warnfilter(362) dynamics::Arm::operator=;
%warnfilter(362) dynamics::ValueRef::operator=;
%warnfilter(512) dynamics::Arm;       // overloaded const non-const methods
%warnfilter(512) dynamics::Link;      // overloaded const non-const methods
%warnfilter(512) dynamics::ValueRef;  // overloaded const non-const methods

// these have to come after typemaps and/or typechecks
// the order matters, too
// %include "primitives/yaml.h"
// %include "primitives/exception.h"

%include "dynamics/yaml.h"
%include "dynamics/types.h"
%include "dynamics/value_ref.h"
%include "dynamics/linear_bounds.h"
%include "dynamics/joint_base.h"
%include "dynamics/joint_rot.h"
%include "dynamics/joint_prism.h"
%include "dynamics/joint_fixed.h"
%include "dynamics/joint_coriolis.h"
%include "dynamics/joint_flexus.h"
%include "dynamics/joint_arquus.h"
%include "dynamics/link.h"
%include "dynamics/arm.h"

#ifndef SRC_DYNAMICS_PYTHON_DYNAMICS_I_
#define SRC_DYNAMICS_PYTHON_DYNAMICS_I_

// needs a __deepcopy__ method for python
// memodict unused, but seems to work
%extend dynamics::Arm {
  dynamics::Arm* dynamics::Arm::__deepcopy__(PyObject* /*memodict*/) {
    dynamics::Arm* copy = new dynamics::Arm(*$self);
    // oddly, above line works without $ as well
    return copy;
  }
  MatXd CalcIDStaticBatch(const MatXd& q) {
    dynamics::MatXd tau(q.rows(), q.cols());
    for(int i = 0; i < q.rows(); i++) {
      $self->set_q(q.row(i));
      $self->CalcDynSetup();
      tau.row(i) = $self->CalcIDStatic();
    }
  return tau;
  }
}

// the functions in C++ don't work in Python, these do
%inline %{
  dynamics::JointCoriolis* CastToJointCoriolis(dynamics::JointBase* base) {
    return dynamic_cast<dynamics::JointCoriolis*>(base);
  }
  dynamics::JointFixed* CastToJointFixed(dynamics::JointBase* base) {
    return dynamic_cast<dynamics::JointFixed*>(base);
  }
  dynamics::JointFlexus* CastToJointFlexus(dynamics::JointBase* base) {
    return dynamic_cast<dynamics::JointFlexus*>(base);
  }
  dynamics::JointArquus* CastToJointArquus(dynamics::JointBase* base) {
    return dynamic_cast<dynamics::JointArquus*>(base);
  }
  dynamics::JointPrism* CastToJointPrism(dynamics::JointPrism* base) {
    return dynamic_cast<dynamics::JointPrism*>(base);
  }
  dynamics::JointRot* CastToJointRot(dynamics::JointRot* base) {
    return dynamic_cast<dynamics::JointRot*>(base);
  }
%}

class dynamics::Quat {
public:
  double w();
  double x();
  double y();
  double z();
  dynamics::Quat operator*(const dynamics::Quat);
  dynamics::Rot toRotationMatrix(void);
};

/// Template instantiations ///
%template(LinkVec) std::vector<dynamics::Link>;
%template(JointPtr) dynamics::ValueRef<dynamics::JointBase>;

%template(ArmFromYamlNode) YamlConvert<dynamics::Arm>;
%template(YAMLArmFromString) dynamics::YAMLConvertFromFilestring<dynamics::Arm>;

#endif  // SRC_DYNAMICS_PYTHON_DYNAMICS_I_
