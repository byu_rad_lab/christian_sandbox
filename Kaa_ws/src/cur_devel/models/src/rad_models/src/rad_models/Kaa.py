from rad_models.Model import Model

from kaa_dyn_params import DynParams
from kaa_6_dof_kinematics import *
from kaa_6_dof_dynamics import *

import numpy as np
from copy import deepcopy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

np.set_printoptions(precision=2)

class Kaa(Model):

    def __init__(self):
        self.numStates = 24
        self.numInputs = 12

        self.K_passive_damper = np.diag([6.3,6.3,6.3,6.3,6.3,6.3])*50.
        self.K_passive_spring = np.diag([.3,.3,.3,.3,.3,.3])*.1*0
        
        self.prs_to_torque = np.array([[.25,-.25,0,0,0,0,0,0,0,0,0,0],
                                       [0,0,.25,-.25,0,0,0,0,0,0,0,0],
                                       [0,0,0,0,.25,-.25,0,0,0,0,0,0],
                                       [0,0,0,0,0,0,.25,-.25,0,0,0,0],
                                       [0,0,0,0,0,0,0,0,.25,-.25,0,0],
                                       [0,0,0,0,0,0,0,0,0,0,.25,-.25]])*1.0
        
        self.alphas = np.diag([1,1,1,1,.5,.5,.5,.5,.5,.5,.5,.5])*1.0
        self.uMax = 130.0
        self.uMin = 0.0
        kaa_params = DynParams()
        self.params = kaa_params.params

    def calc_continuous_A_B_w(self,x,u,dt):

        p = x[0:12]
        qd = x[12:18]
        q = x[18:24].flatten()
        q = q.tolist()

        M = np.array(calc_M(self.params,q)).reshape(6,6)*2.0
        w,v = np.linalg.eig(M)
        # while np.linalg.cond(M)>1000:
        #     M += np.eye(np.size(M,1))*1e-6
        M_inv = np.linalg.inv(M)
        
        A11 = -self.alphas
        A12 = np.zeros([12,6])
        A13 = np.zeros([12,6])
        A21 = np.matmul(M_inv,self.prs_to_torque)
        A22 = np.matmul(M_inv,(-self.K_passive_damper))
        A23 = np.matmul(M_inv,-self.K_passive_spring)        
        A31 = np.zeros([6,12])        
        A32 = np.eye(6)        
        A33 = np.zeros([6,6])

        A = np.vstack([np.hstack([A11,A12,A13]),np.hstack([A21,A22,A23]),np.hstack([A31,A32,A33])])
        
        B11 = self.alphas
        B21 = np.zeros([6,12])
        B31 = np.zeros([6,12])
        B = np.vstack([B11,B21,B31])

        gravity_torque = np.array(calc_g(self.params,q)).reshape(6,1)

        w = np.matmul(M_inv,gravity_torque)
        bigw = np.vstack([np.zeros((12,1)),np.reshape(w,(6,1)),np.zeros((6,1))])
        # bigw = np.vstack([np.zeros((12,1)),np.zeros((6,1)),np.reshape(w,(6,1))])*dt     
        
        return A,B,bigw
        
        
    def calc_discrete_A_B_w(self,x,u,dt=.01):
        
        [A,B,w] = self.calc_continuous_A_B_w(x,u,dt)

        # Matrix exponentiation
        [Ad,Bd] = self.discretize_A_and_B(A,B,dt)

        # First order implicit integration
        # Ad = np.linalg.inv(np.eye(self.numStates) - A*dt)
        # Bd = np.matmul(Ad,B)*dt

        # First order explicit integration        
        # Ad = np.eye(self.numStates) + A*dt
        # Bd = B*dt

        wd = w*dt
        
        return Ad, Bd, wd

        
    def forward_simulate_dt(self,x,u,dt = 0.01):
        batch_size = x.shape[1]
        x_k_plus_one = np.zeros(x.shape)

        for i in range(0,batch_size):
            [Ad,Bd,wd] = self.calc_discrete_A_B_w(x[:,i],u[:,i],dt)
            x_k_plus_one[:,i] = np.matmul(Ad,x[:,i]) + np.matmul(Bd,u[:,i]) + wd.flatten()

        # for i in xrange(0,12):
        #     if (x_k_plus_one[i] < x[i]):
        #         x_k_plus_one[i] = (x_k_plus_one[i] - x[i])*20.0

        return x_k_plus_one

    def visualize(self,x,ax):
        
        ax.clear()
        q = x[18:24]
        self.plot_cylinder(ax, np.eye(4), FK[0](q).reshape(4,4), r=.07, color='r')
        self.plot_cylinder(ax, FK[0](q).reshape(4,4), FK[1](q).reshape(4,4), r=.07, color='b')
        self.plot_cylinder(ax, FK[1](q).reshape(4,4), FK[2](q).reshape(4,4), r=.07, color='r')
        self.plot_cylinder(ax, FK[2](q).reshape(4,4), FK[3](q).reshape(4,4), r=.07, color='b')
        self.plot_cylinder(ax, FK[3](q).reshape(4,4), FK[4](q).reshape(4,4), r=.07, color='r')
        self.plot_cylinder(ax, FK[4](q).reshape(4,4), FK[5](q).reshape(4,4), r=.07, color='b')

        self.set_axes_equal(ax)            
        plt.ion()
        plt.xlabel('X')
        plt.ylabel('Y')
        # plt.zlabel('Z')        
        plt.show()
        plt.pause(.000001)

    def plot_cylinder(self,ax, g1, g2, r=0.05, color='k', n=3):
        th_vals = [2 * np.pi * i / n for i in range(n+1)]
        # circle = [[r * np.sin(th), r * np.cos(th), 0.0, 1.0] for th in th_vals]
        circle = [[0.0,r * np.sin(th), r * np.cos(th), 1.0] for th in th_vals]        
        circle = np.array(circle).T
        
        circle1 = g1.dot(circle)
        g3 = deepcopy(g1)  # Gets rid of twist in X
        g3[0:3, 3] = g3[0:3, 3]+(g2[0:3, 3]-g1[0:3, 3])
        circle2 = g3.dot(circle)
        
        for i in range(circle1.shape[1] - 1):
            self.plot_line(ax, circle1[:, i], circle1[:, i+1], color, 1.0)
            self.plot_line(ax, circle2[:, i], circle2[:, i+1], color, 1.0)
            self.plot_line(ax, circle1[:, i], circle2[:, i], color, 1.0)
            self.plot_line(ax, circle1[:, i], circle2[:, i+1], color, 1.0)
            
            
    def plot_line(self,ax, a, b, color, linewidth_custom, alpha=1.0):  # Ax must be projection 3d
        ax.plot([a[0], b[0]], [a[1], b[1]], [a[2], b[2]], color=color, linewidth=linewidth_custom, alpha=alpha)

    def set_axes_equal(self,ax):
        
        ax.set_xlim3d([0,2])
        ax.set_ylim3d([-1.5,1.5])
        ax.set_zlim3d([-1.5,1.5])
    

if __name__=='__main__':
    import time
    sys = Kaa()

    q = np.array([1.0,0,0,0,0,0]).reshape(6,1)
    qd = np.zeros((6,1))
    p = np.array([[120],
                  [0],
                  [120],
                  [0],
                  [120],
                  [0],
                  [120],
                  [0],
                  [120],
                  [0],
                  [120],
                  [0]])
    
    x = np.vstack([p,qd,q])
    u = deepcopy(p)
    x0 = deepcopy(x)
    
    dt = .01
    
    ax = plt.gca(projection='3d')
    plt.ion()

    horizon = 5000
    for i in range(0,horizon):
        start = time.time()
        if(i>1000):
            u = u*0
            print("draining!!!!!!!!!")
        x = sys.forward_simulate_dt(x,u,dt)
        print("simulation time: ",time.time()-start)
        if i%5==0:
            print(i)
            print(x[18:24].T)
            sys.visualize(x,ax)
    
