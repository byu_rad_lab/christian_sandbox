from rad_models.Model import Model
from rad_models.KneedWalker import draw_robot
from rad_models.MyNet import *
import numpy as np
import torch
from torch.autograd import Variable
import matplotlib.pyplot as plt

class LearnedKneedWalker(Model):

    def __init__(self,use_gpu=True):
        self.numStates = 14
        self.numInputs = 7
        self.uMax = 100.0
        self.use_gpu = use_gpu

        if self.use_gpu==True:
            self.model = torch.load('/home/phil/git/byu/cur_devel/models/src/rad_models/src/rad_models/learned_models/LearnedKneedWalkerModel.pt').cuda()
        else:
            self.model = torch.load('/home/phil/git/byu/cur_devel/models/src/rad_models/src/rad_models/learned_models/LearnedKneedWalkerModel.pt')
            
    def forward_simulate_dt(self,x,u,dt=.01):
        inputs = np.vstack([x,u/self.uMax])
        inputs[0:7,:] = inputs[0:7,:]/10.0
        inputs = np.delete(inputs,[7,14,15,16],axis=0).T
        inputs
        if self.use_gpu==True:
            self.input_var = Variable(torch.from_numpy(inputs).float(),requires_grad=False).cuda()
        else:
            self.input_var = Variable(torch.from_numpy(inputs).float(),requires_grad=False)

        x = x.reshape([self.numStates,-1])
        x[0:7,:] = self.model(self.input_var).cpu().detach().numpy().T
        x[7:14,:] = x[7:14,:] + x[0:7,:]*dt
        x[9:14,:] = (x[9:14,:] + np.pi) % (2*np.pi) - np.pi
        return x


if __name__=='__main__':
    q = np.array([0,2.6,0.0,0.15,0,-0.15,0]).reshape(7,-1)
    qd = np.array([0.0,0,0,0,0,0,0.0]).reshape(7,-1)
    x = np.vstack([qd,q]).reshape(14,1)

    # x[9:14,:] = x[9:14,:].clip(-np.pi/4.0,np.pi/4.0)
    # x[9,:] = 0        
    # x[8,:] = 2.6
    # x[0:7,:] = x[0:7,:]*1.0

    walker = LearnedKneedWalker()
    
    dt = .01
    
    fig = plt.gca()
    plt.ion()
    plt.hlines(0,-15,15)
    u = np.array([0.0,0.0,0.0,0.0,0.0,0.0,0]).reshape(7,1)
    
    for i in range(0,100000):
        
        # u[3] = 200.0*(1.0*np.sin(-i*.008+np.pi/4)-x[3+7]) - 0.5*x[3]
        # u[4] = 200.0*(1.0*np.sin(-i*.008+np.pi/4)-x[4+7]) - 0.5*x[4]
        # u[5] = 200.0*(-1.0*np.sin(-i*.008)-x[5+7]) - 0.5*x[5]
        # u[6] = 200.0*(-1.0*np.sin(-i*.008)-x[6+7]) - 0.5*x[6]
        # u[3] = 500.0*(0.15-x[3+7]) - 10.0*x[3]
        # u[4] = 500.0*(0-x[4+7]) - 10.0*x[4]
        # u[5] = 500.0*(-0.15-x[5+7]) - 10.0*x[5]
        # u[6] = 500.0*(-0.0-x[6+7]) - 10.0*x[6]
        u = u.clip(-100,100)
        
        x = walker.forward_simulate_dt(x,u,dt)

        if i%200==0:
            plt.clf()
            plt.hlines(0,-15,15,linewidth=1.0)
            draw_robot(x[7:14,:],1.0)
            plt.axis(xmin=-12,xmax=12,ymin=-10,ymax=10)
            # plt.axis(aspect='equal')
            plt.pause(.000001)

    
