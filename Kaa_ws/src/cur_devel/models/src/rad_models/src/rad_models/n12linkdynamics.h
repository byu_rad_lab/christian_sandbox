#include <math.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>
#include <Eigen/LU>

namespace py = pybind11;

void calc_discrete_A_B_w(std::vector<double> q,std::vector<double> qd, double dt, Eigen::MatrixXd * A_d, Eigen::MatrixXd * B_d, Eigen::MatrixXd * w_d,double l,double m,double I,double gravity,double damping);
void calc_A_B_w(std::vector<double> q,std::vector<double> qd,Eigen::MatrixXd * A, Eigen::MatrixXd * B, Eigen::MatrixXd * w,double l,double m,double I,double gravity,double damping);

py::array calc_M(std::vector<double> q,double l,double m,double I);
py::array calc_grav(std::vector<double> q,double l,double m,double g);
py::array calc_C(std::vector<double> q,std::vector<double> qd,double l,double m,double I);
py::array fkCoM0(std::vector<double> q,double l);
py::array fkCoM1(std::vector<double> q,double l);
py::array fkCoM2(std::vector<double> q,double l);
py::array fkCoM3(std::vector<double> q,double l);
py::array fkCoM4(std::vector<double> q,double l);
py::array fkCoM5(std::vector<double> q,double l);
py::array fkCoM6(std::vector<double> q,double l);
py::array fkCoM7(std::vector<double> q,double l);
py::array fkCoM8(std::vector<double> q,double l);
py::array fkCoM9(std::vector<double> q,double l);
py::array fkCoM10(std::vector<double> q,double l);
py::array fkCoM11(std::vector<double> q,double l);
