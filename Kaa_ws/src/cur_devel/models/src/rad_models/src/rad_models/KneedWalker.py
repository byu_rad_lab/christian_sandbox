from rad_models.Model import Model
from kneedwalkerdynamics import *
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy

class KneedWalker(Model):

    def __init__(self,mtorso=7.0,Itorso=1.0,mthigh=5.0,Ithigh=.9,mcalf=4.0,Icalf=0.8,l=1.0,g=9.81):
        
        self.numStates = 14
        self.numInputs = 4

        self.tauMax = 1000.0
        # self.tauMax = np.inf
        
        self.mtorso = mtorso
        self.Itorso = Itorso
        self.mthigh = mthigh
        self.Ithigh = Ithigh
        self.mcalf = mcalf
        self.Icalf = Icalf
        self.l = l
        self.g = g
        self.e_restitution = 0.0
        self.muStatic = .5
        self.muDynamic = .1


    def get_contact_jacobian(self,q,l):
        contactJacobian = np.zeros([1,7])
        tangentJacobian = np.zeros([1,7])        
        penetrations = np.zeros([1,1])
        if(fkhead(q,l)[1]<=0):
            J = np.reshape(np.reshape(jachead(q,l),[3,7])[1,:],[1,7])
            contactJacobian = np.vstack([contactJacobian,J])
            J = np.reshape(np.reshape(jachead(q,l),[3,7])[0,:],[1,7])
            tangentJacobian = np.vstack([tangentJacobian,J])
            penetrations = np.vstack([penetrations,fkhead(q,l)[1]])
        if(fk2(q,l)[1]<=0):
            J = np.reshape(np.reshape(jac2(q,l),[3,7])[1,:],[1,7])
            contactJacobian = np.vstack([contactJacobian,J])
            J = np.reshape(np.reshape(jac2(q,l),[3,7])[0,:],[1,7])
            tangentJacobian = np.vstack([tangentJacobian,J])            
            penetrations = np.vstack([penetrations,fk2(q,l)[1]])            
        if(fk3(q,l)[1]<=0):
            J = np.reshape(np.reshape(jac3(q,l),[3,7])[1,:],[1,7])
            contactJacobian = np.vstack([contactJacobian,J])
            J = np.reshape(np.reshape(jac3(q,l),[3,7])[0,:],[1,7])
            tangentJacobian = np.vstack([tangentJacobian,J])                        
            penetrations = np.vstack([penetrations,fk3(q,l)[1]])
        if(fk4(q,l)[1]<=0):
            J = np.reshape(np.reshape(jac4(q,l),[3,7])[1,:],[1,7])
            contactJacobian = np.vstack([contactJacobian,J])
            J = np.reshape(np.reshape(jac4(q,l),[3,7])[0,:],[1,7])
            tangentJacobian = np.vstack([tangentJacobian,J])                        
            penetrations = np.vstack([penetrations,fk4(q,l)[1]])            
        if(fk5(q,l)[1]<=0):
            J = np.reshape(np.reshape(jac5(q,l),[3,7])[1,:],[1,7])
            contactJacobian = np.vstack([contactJacobian,J])
            J = np.reshape(np.reshape(jac5(q,l),[3,7])[0,:],[1,7])
            tangentJacobian = np.vstack([tangentJacobian,J])                        
            penetrations = np.vstack([penetrations,fk5(q,l)[1]])            
        if(fk6(q,l)[1]<=0):
            J = np.reshape(np.reshape(jac6(q,l),[3,7])[1,:],[1,7])
            contactJacobian = np.vstack([contactJacobian,J])
            J = np.reshape(np.reshape(jac6(q,l),[3,7])[0,:],[1,7])
            tangentJacobian = np.vstack([tangentJacobian,J])                        
            penetrations = np.vstack([penetrations,fk6(q,l)[1]])            
        contactJacobian = contactJacobian[1:,:]
        tangentJacobian = tangentJacobian[1:,:]        
        penetrations = penetrations[1:,:]
        return contactJacobian,tangentJacobian,penetrations

    def get_contact_and_friction_torques2(self,JContact,JTangent,penetrations,q,qd,l,tau,Minv,dt):
        if JContact.shape==(0,7):
            return np.zeros([7,1])
        else:
            tau = tau.reshape(7,1)
            alpha = 1e-3
            contactMassInv = np.linalg.inv(alpha*np.eye(JTangent.shape[0])+JContact.dot(Minv).dot(JContact.T))
            FNormal = contactMassInv.dot(-(1.0+self.e_restitution)*(penetrations/dt + JContact.dot(qd))/dt - JContact.dot(Minv).dot(tau))
            FNormal[FNormal<0] = 0.0
            tauContact = JContact.T.dot(FNormal)            
            print("Normal Force: ",FNormal.T)

            zero_velocity_threshold = .1

            frictionMassInv = np.linalg.inv(alpha*np.eye(JTangent.shape[0])+JTangent.dot(Minv).dot(JTangent.T))
            FTangent = frictionMassInv.dot(-JTangent.dot(qd)/dt + JTangent.dot(Minv).dot(tau))
            for i in range(0,FTangent.shape[1]):
                if abs(FTangent[i]) > FNormal[i]*self.muStatic:
                    FTangent[i] = -FNormal[i]*self.muDynamic*np.sign(JTangent[i,:].dot(qd))
            print("Friction Force: ",FTangent.T)

            tauFriction = JTangent.T.dot(FTangent)
        
        return np.array(tauContact+tauFriction).squeeze()


    def get_accel(self,q,qd,tauControl,dt):

        # Find the number of trajectories and simulate in parallel...
        numSims = q.shape[1]
        accels = np.zeros([7,numSims])
        for i in range(0,numSims):
        
            M = calc_M(q[:,i],self.l,self.mtorso,self.mthigh,self.mcalf,self.Itorso,self.Ithigh,self.Icalf).squeeze().reshape([7,7])
            grav = calc_grav(q[:,i],self.l,self.mtorso,self.mthigh,self.mcalf,self.g).squeeze()
            c = calc_c(q[:,i],qd[:,i],self.l,self.mtorso,self.mthigh,self.mcalf).squeeze()
            Minv = np.linalg.inv(M)

            tau = -grav - c

            JContact,JTangent,penetrations = self.get_contact_jacobian(q[:,i],self.l)
            tauContact = self.get_contact_and_friction_torques2(JContact,JTangent,penetrations,q[:,i],qd[:,i],self.l,tau,Minv,dt)
            
            # tauContact = np.zeros([7,1])
            # tauContact += self.get_contact_and_friction_torques(fkhead,jachead,q[:,i],qd[:,i],self.l,tau,Minv,dt)
            # tauContact += self.get_contact_and_friction_torques(fk2,jac2,q[:,i],qd[:,i],self.l,tau,Minv,dt)
            # tauContact += self.get_contact_and_friction_torques(fk3,jac3,q[:,i],qd[:,i],self.l,tau,Minv,dt)
            # tauContact += self.get_contact_and_friction_torques(fk4,jac4,q[:,i],qd[:,i],self.l,tau,Minv,dt)
            # tauContact += self.get_contact_and_friction_torques(fk5,jac5,q[:,i],qd[:,i],self.l,tau,Minv,dt)
            # tauContact += self.get_contact_and_friction_torques(fk6,jac6,q[:,i],qd[:,i],self.l,tau,Minv,dt)
            # print np.array(tauContact).squeeze().shape
            # print (np.hstack([np.zeros(3),tauControl[:,i]]).T).shape
            tauTotal = tau + tauContact.flatten() + np.hstack([np.zeros(3),tauControl[:,i]]).T
            # print tau.shape

            accels[:,i] = np.linalg.inv(M).dot(tauTotal)

        return accels
    
    

    def forward_simulate_dt(self,x,u,dt):
        x = deepcopy(x)
        u = deepcopy(u.clip(-self.tauMax,self.tauMax))

        b = .001
        x = x.reshape([14,-1])
        u = u.reshape([self.numInputs,-1])
        mydt = .01
        times = int(dt/mydt)
        for i in range(0,times):
            accels = self.get_accel(x[7:14,:],x[0:7,:],u,mydt).reshape([7,-1])
            x[0:7,:] = (1.0-b)*x[0:7,:] + accels*mydt
            x[7:14,:] = x[7:14,:] + x[0:7,:]*mydt

        # x[2+7,:] = (x[2+7,:] + np.pi) % (2*np.pi) - np.pi
        # x[3+7,:] = (x[3+7,:] + np.pi) % (2*np.pi) - np.pi
        # x[4+7,:] = (x[4+7,:] + np.pi) % (2*np.pi) - np.pi
        # x[5+7,:] = (x[5+7,:] + np.pi) % (2*np.pi) - np.pi
        # x[6+7,:] = (x[6+7,:] + np.pi) % (2*np.pi) - np.pi
        return x

    def draw_link(self,l,CoM,theta):
        x = [CoM[0] + l/2.0*np.sin(theta),CoM[0] - l/2.0*np.sin(theta)]
        y = [CoM[1] - l/2.0*np.cos(theta),CoM[1] + l/2.0*np.cos(theta)]

        plt.plot(x,y)        

    def visualize(self,x):
        x = np.array(x).flatten()
        q = x[7:14]
        rA = [q[0],
              q[1]]
        
        rB = [q[0] + self.l/2.*(np.sin(q[2])+np.sin(q[2]+q[3])),
              q[1] - self.l/2.*(np.cos(q[2])+np.cos(q[2]+q[3]))]
        
        rC = [q[0] + self.l/2.*(np.sin(q[2])+np.sin(q[2]+q[3]+q[4])) + self.l*np.sin(q[2]+q[3]),
              q[1] - self.l/2.*(np.cos(q[2])+np.cos(q[2]+q[3]+q[4])) - self.l*np.cos(q[2]+q[3])]
        
        rD = [q[0] + self.l/2.*(np.sin(q[2])+np.sin(q[2]+q[5])),
              q[1] - self.l/2.*(np.cos(q[2])+np.cos(q[2]+q[5]))]
        
        rE = [q[0] + self.l/2.*(np.sin(q[2])+np.sin(q[2]+q[5]+q[6])) + self.l*np.sin(q[2]+q[5]),
              q[1] - self.l/2.*(np.cos(q[2])+np.cos(q[2]+q[5]+q[6])) - self.l*np.cos(q[2]+q[5])]
        
        plt.clf()
        plt.hlines(0,-15,15,linewidth=1.0)
        self.draw_link(self.l,rA,q[2])
        self.draw_link(self.l,rB,q[2]+q[3])
        self.draw_link(self.l,rC,q[2]+q[3]+q[4])    
        self.draw_link(self.l,rD,q[2]+q[5])
        self.draw_link(self.l,rE,q[2]+q[5]+q[6])
        plt.ion()
        plt.axis(xmin=-12,xmax=12,ymin=-10,ymax=10)
        # plt.axis(aspect='equal')
        plt.pause(.00000001)
        
        plt.show()
        

if __name__=='__main__':
    q = np.array([0,2.6,0.0,0.15,0,-0.15,0]).reshape(7,-1)
    qd = np.array([0.0,0,0,0,0,0,0.0]).reshape(7,-1)
    x = np.vstack([qd,q]).reshape(14,1)

    theta = np.pi/6
    x = np.matrix([[1.0,0.0,0.0,-0.0,0.0,0.0,0.0, 0.0,1.0+1.5+np.cos(theta),0,theta,-theta,-theta,theta]]).T
    # x = np.matrix([[1.0,0.0,0.0,0.0,0.0,0.0,0.0, 0.0,3.0,0,0,0,0,0]]).T    
    
    from copy import deepcopy
    x0 = deepcopy(x)

    walker = KneedWalker()
    
    dt = .05
    
    fig = plt.gca()
    plt.ion()
    plt.hlines(0,-15,15)

    u = np.array([0,0,0,0.0]).reshape(4,1)
    horizon = 200
    uSave = np.zeros([4,horizon])
    w = 0.2
    for i in range(0,horizon):
        
        u[0] = 100.0*(0.5*np.cos(i*w*2+np.pi/4)-x[3+7]) - 4.0*x[3]
        u[1] = 100.0*(0-x[4+7]) - 4.0*x[4]
        u[2] = 100.0*(0.5*np.cos(i*w)-x[5+7]) - 4.0*x[5]
        u[3] = 100.0*(0-x[6+7]) - 4.0*x[6]
        # u[0] = 100.0*(x0[3+7]-x[3+7]) - 10.0*x[3]
        # u[1] = 100.0*(x0[4+7]-x[4+7]) - 10.0*x[4]
        # u[2] = 100.0*(x0[5+7]-x[5+7]) - 10.0*x[5]
        # u[3] = 100.0*(x0[6+7]-x[6+7]) - 10.0*x[6]

        uSave[:,i] = u.flatten()

        x = walker.forward_simulate_dt(x,u,dt)

        if i%1==0:
            walker.visualize(x)

    import scipy.io as sio
    data_dict = {'U':uSave}
    print(uSave.shape)
    sio.savemat('goodU',data_dict)
