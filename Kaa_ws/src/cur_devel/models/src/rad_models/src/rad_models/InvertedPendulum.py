from rad_models.Model import Model
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy
from matplotlib import patches

class InvertedPendulum(Model):

    def __init__(self,length=1.0,mass=0.2,damping=0.1,gravity=9.81, uMax=1.0, xMax=[np.inf]*2, xMin=[-np.inf]*2):
        self.numStates = 2
        self.numInputs = 1

        # self.uMax = np.inf
        self.uMax = uMax
        self.xMax = xMax
        self.xMin = xMin
        
        self.m = mass
        self.l = length
        self.b = damping
        self.g = gravity
        self.I = self.m*self.l**2.0


    def calc_state_derivs(self,x,u):
        x = deepcopy(x)
        # u = deepcopy(u.clip(-self.uMax,self.uMax))
        u = deepcopy(u)
        x = x.reshape([self.numStates,-1])
        xdot = np.zeros(x.shape)
        xdot[0,:] = (-self.b*x[0,:] + self.m*self.g*np.sin(x[1,:]) + u)/self.I
        xdot[1,:] = x[0,:]
        return xdot
        
    def forward_simulate_dt(self,x,u,dt=.01,wrapAngle=True):
        xdot = self.calc_state_derivs(x,u)
        x = x + xdot*dt
        if wrapAngle==True:
            x[1,:] = (x[1,:] + np.pi) % (2*np.pi) - np.pi
        return x

    def calc_discrete_A_B_w(self,x,u,dt=.01):
        x = deepcopy(x)
        u = deepcopy(u)
        x = x.reshape(self.numStates,-1)
        A = np.matrix([[-self.b/self.I, 0],
                       [1.0, 0]])
        B = np.matrix([[1.0/self.I],
                       [0.0]])
        w = np.matrix([[self.m*self.g*np.sin(x[1,0])/self.I],
                       [0.0]])

        [Ad,Bd] = self.discretize_A_and_B(A,B,dt)
        wd = w*dt
        
        return Ad,Bd,wd
    
        
    def visualize(self,x,u=np.zeros(1)):
        x = x.reshape(-1,1)
        u = u.reshape(-1,1)
        CoM = [-0.5*np.sin(x[1,0]),0.5*np.cos(x[1,0])]
        theta = x[1,0]
        
        x = [CoM[0] + self.l/2.0*np.sin(theta),CoM[0] - self.l/2.0*np.sin(theta)]
        y = [CoM[1] - self.l/2.0*np.cos(theta),CoM[1] + self.l/2.0*np.cos(theta)]
        
        massX = CoM[0] - self.l/2.0*np.sin(theta)
        massY = CoM[1] + self.l/2.0*np.cos(theta)

        width = .1
        height = .1
        myangle = -90
        # if(u<0):
        #     mytheta1 = 360 + np.rad2deg(u[0]*np.pi)
        #     mytheta2 = 0
        # else:
        #     mytheta1 = 0
        #     mytheta2 = np.rad2deg(u[0]*np.pi)
        # tau0 = patches.Arc([0,0],width,height,myangle,theta1=mytheta1,theta2=mytheta2,lw=2.5,color='r')

        plt.clf()
        ax = plt.gca()
        plt.plot(x,y)
        plt.scatter(massX,massY,50,'r')
        # ax.add_patch(tau0)        
        plt.axis([-1.5,1.5,-1.5,1.5])
        plt.ion()
        plt.show()
        plt.pause(.0000001)

if __name__=='__main__':
    
    sys = InvertedPendulum()    
    x = np.zeros([sys.numStates,1])
    x[1] = .001
    u = np.zeros([sys.numInputs,1])    
    
    dt = .01
    
    fig = plt.gca()
    plt.ion()

    horizon = 5000
    for i in range(0,horizon):

        x = sys.forward_simulate_dt(x,u,dt)

        if i%5==0:
            sys.visualize(x, u)
        
