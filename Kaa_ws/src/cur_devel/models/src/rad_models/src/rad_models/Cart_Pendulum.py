"""
This script defines a cart pendulum class. 

Frames are as follows:

World frame: x points into the paper, y point upwards vertically, and z points to the right. 
The cart moves along z and the pendulum rotates theta about the x axis, with positive theta a clockwise rotation about the positive x axis. 

This model assumes the same friction coeff between the cart and ground and the cart and pendulum

State Vector x: [zdot, z, thetadot, theta].T

"""

from rad_models.Model import Model
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy
from matplotlib import patches

class CartPendulum(Model):

    def __init__(self,length=1.0,cart_mass=5.0,pend_mass=.01,damping=.2,gravity=9.81,uMax=1.0,xMax=[np.inf]*2,xMin=[-np.inf]*2):
        self.numStates = 4
        self.numInputs = 1      # horizontal force on the cart

        # self.uMax = np.inf
        self.uMax = uMax
        self.xMax = xMax
        self.xMin = xMin
        
        self.m1 = cart_mass
        self.m2 = pend_mass
        self.l = length
        self.b = damping        # damping on the cart
        self.g = gravity

        


    def calc_state_derivs(self,x,u):
        x = deepcopy(x)
        # u = deepcopy(u.clip(-self.uMax,self.uMax))
        u = deepcopy(u)
        x = x.reshape([self.numStates,-1])

        # unpack state vector
        zd = x[0,-1]
        z = x[1,-1]
        thd = x[2,-1]
        th = x[3,-1]
        F = u[0,-1]

        # print(zd)
        # print(z)
        # print(thd)
        # print(th)
        # print("\n\n")
        
        xdot = np.zeros(x.shape)
        
        cth = np.cos(th)
        sth = np.sin(th)
        
        M = np.array([
            [self.m1+self.m2, self.m1*(self.l/2.)*cth],
            [(self.m1*self.l/2.)*cth, self.m1*self.l**2./3.]
            ])

        C = np.array([
            [self.m1*(self.l/2.)*thd**2*sth + F - self.b*zd],
            [self.m1*self.g*(self.l/2.)*sth - self.b*thd]
            ])

        temp = np.dot(np.linalg.inv(M), C)
        zddot = temp[0,0]
        thddot = temp[1,0]

        xdot = np.array([zddot, zd, thddot, thd]).reshape(self.numStates, -1)
        return xdot
        
    def forward_simulate_dt(self,x,u,dt=.01,wrapAngle=False):
        xdot = self.calc_state_derivs(x,u)
        x = x + xdot*dt
        if wrapAngle==True:
            x[3,:] = (x[3,:] + np.pi) % (2*np.pi) - np.pi
        return x

    def calc_discrete_A_B_w(self,x,u,dt=.01):
        x = deepcopy(x)
        u = deepcopy(u)
        x = x.reshape(self.numStates,-1)

        raise NotImplementedError
    
    
        
    def visualize(self,x,u=np.zeros(1)):
        x = x.reshape(-1,1)
        u = u.reshape(-1,1)

        xlim = 5.0
        ylim = 5.0
        cart_width = 0.3
        cart_height = 0.25

        cartx = x[1,0]
        carty = cart_height/2
        theta = x[3,0]
        
        # ground line
        groundx = np.array([-xlim, xlim])
        groundy = np.array([0,0])

        # begin and end points of pendulum.
        x = [cartx, cartx + self.l*np.sin(theta)]
        y = [cart_height, cart_height+self.l*np.cos(theta)]



        cart = patches.Rectangle((cartx - cart_width/2, 0), cart_width, cart_height)

        plt.clf()
        ax = plt.gca()
        plt.plot(groundx, groundy, 'k')
        ax.add_patch(cart)
        plt.plot(x,y,'r')        
        plt.axis([-xlim,xlim,-ylim, ylim])
        plt.ion()
        plt.show()
        plt.pause(.0000001)

if __name__=='__main__':
    
    sys = CartPendulum()    
    x = np.zeros([sys.numStates,1])
    x[1] = 0.0
    x[3] = np.pi/2
    u = np.zeros([sys.numInputs,1])
    
    dt = .01
    
    fig = plt.gca()
    plt.ion()
    xhist = np.zeros(x.shape)

    horizon = 5000
    xhist[:,0] = x.flatten()
    thist = np.zeros(1)
    
    for i in range(0,horizon):

        x = sys.forward_simulate_dt(x,u,dt)

        xhist = np.append(xhist, x, axis=1)
        thist = np.append(thist, i*dt)

        if i%5==0:
            sys.visualize(x, u)
        
