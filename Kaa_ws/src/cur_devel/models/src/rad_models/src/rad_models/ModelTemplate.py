from rad_models.Model import Model
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy
from matplotlib import patches

class My_Model_Name(Model):

    def __init__(self,model_parameters): # replace with any parameters your model will need
        """Initializes a ____ model instance"""
        # Referencing these member variables in your controller will allow
        # you to swap models out interchangeable, regardless of states and inputs

        self.numStates = 2      # the number of states for your system
        self.numInputs = 2      # the number of inputs for your system
        self.uMax = np.inf      # It is assumed that uMin == -uMax
        self.xMax = [np.inf]*self.numStates # Max and min values for each state variable
        self.xMin = [-np.inf]*self.numStates

    def calc_state_derivs(self,x,u,dt=.01):
        """Returns the x_dot for a given x, u, and dt"""
        pass
        
    def visualize(self,x,u=0):
        """Visualize the system at a given state, optionally visualizing an input, as well."""
        pass


if __name__=='__main__':
    # Put some test code here to create and test a simple instance of your model
    pass
