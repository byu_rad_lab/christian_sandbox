from rad_models.Model import Model
from n12linkdynamics import *

import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy
from matplotlib import patches
import os

class N12link(Model):

    def __init__(self,l=0.5,m=.5,I=0.1,g=9.81,damping=0.1):

        self.numLinks = 12
        self.FK = [fkCoM0,fkCoM1,fkCoM2,fkCoM3,fkCoM4,fkCoM5,fkCoM6,fkCoM7,fkCoM8,fkCoM9,fkCoM10,fkCoM11]
            
        self.numStates = self.numLinks*2
        self.numInputs = self.numLinks

        self.uMax = 10.0
        
        self.m = m
        self.I = I
        self.l = l
        self.g = g
        self.damping = damping

    def get_accel(self,q,qd,tauControl,dt):
        n = self.numLinks
        # Find the number of trajectories and simulate in parallel...
        numSims = q.shape[1]
        accels = np.zeros([n,numSims])

        for i in range(0,numSims):
        
            M = calc_M(q[:,i],self.l,self.m,self.I).squeeze().reshape([n,n])
            grav = calc_grav(q[:,i],self.l,self.m,self.g).squeeze()
            C = calc_C(q[:,i],qd[:,i],self.l,self.m,self.I).squeeze().reshape([n,n])
            Minv = np.linalg.inv(M)

            tauTotal = np.reshape(-grav - C.dot(qd[:,i]) - self.damping*qd[:,i] + tauControl[:,i],[n,1])
            
            accels[:,i] = np.linalg.inv(M).dot(tauTotal).flatten()

        return accels
    
    def forward_simulate_dt(self,x,u,dt,wrapAngle=True):
        x = x.reshape([self.numStates,-1])
        qd = x[0:self.numLinks,:]
        q = x[self.numLinks:self.numStates,:]
        numSims = x.shape[1]
        n = self.numLinks
        x = deepcopy(x).reshape([self.numStates,-1])
        u = deepcopy(np.clip(u,-self.uMax,self.uMax)).reshape([self.numInputs,-1])

        for i in range(0,numSims):
            Ad,Bd,wd = self.calc_discrete_A_B_w(q[:,i],qd[:,i],dt)

            xnext = Ad.dot(x[:,i]) + Bd.dot(u[:,i]) + wd.flatten()
            x[:,i] = xnext.T
        
        if wrapAngle==True:
            x[self.numLinks:self.numStates,:] = (x[self.numLinks:self.numStates,:] + np.pi) % (2*np.pi) - np.pi        
        return x

    def calc_A_B_w(self,q,qd):
        A = np.zeros([self.numStates,self.numStates])
        B = np.zeros([self.numStates,self.numInputs])
        w = np.zeros([self.numStates,1])

        M = calc_M(q,self.l,self.m,self.I).squeeze().reshape([self.numLinks,self.numLinks])
        grav = calc_grav(q,self.l,self.m,self.g).squeeze()
        C = calc_C(q,qd,self.l,self.m,self.I).squeeze().reshape([self.numLinks,self.numLinks])
        F = np.eye(self.numLinks)*self.damping
        Minv = np.linalg.inv(M)
        
        A[0:self.numLinks,0:self.numLinks] = -Minv.dot(C+F)
        A[self.numLinks:self.numStates,0:self.numLinks] = np.eye(self.numLinks)

        B[0:self.numLinks,:] = Minv

        w[0:self.numLinks] = -Minv.dot(grav).reshape(self.numLinks,1)

        return A,B,w

    # def calc_discrete_A_B_w(self,q,qd,dt):

    #     A,B,w = self.calc_A_B_w(q,qd)

    #     Ad = np.linalg.inv(np.eye(self.numStates)-A*dt)
    #     Bd = Ad.dot(B)*dt
    #     wd = w*dt

    #     return Ad, Bd, wd

    def calc_discrete_A_B_w(self,q,qd,dt):

        out = calc_discrete_A_B_w_py(q,qd,dt,self.l,self.m,self.I,self.g,self.damping)

        Ad = np.zeros([self.numStates,self.numStates])
        Bd = np.zeros([self.numStates,self.numInputs])
        wd = np.zeros([self.numStates,1])
        for i in range(0,self.numStates):
            for j in range(0,self.numStates):
                Ad[i,j] = out[i*self.numStates+j]

            for j in range(0,self.numInputs):
                Bd[i,j] = out[self.numStates**2 + i*self.numInputs + j]
                
            wd[i] = out[self.numStates**2 + self.numStates*self.numInputs + i]
            
        return Ad, Bd, wd
    
    def draw_link(self,l,CoM,theta):
        x = [CoM[0] + l/2.0*np.sin(theta),CoM[0] - l/2.0*np.sin(theta)]
        y = [CoM[1] - l/2.0*np.cos(theta),CoM[1] + l/2.0*np.cos(theta)]

        plt.plot(x,y)

    def draw_torque(self,l,CoM,theta,u):
        x = CoM[0] - l/2.0*np.sin(theta)
        y = CoM[1] + l/2.0*np.cos(theta)
        width = .1
        height = .1
        myangle = -90
        if(u<0):
            mytheta1 = 360 + np.rad2deg(u*np.pi/self.uMax)
            mytheta2 = 0
        else:
            mytheta1 = 0
            mytheta2 = np.rad2deg(u*np.pi/self.uMax)
        arc = patches.Arc([x,y],width,height,myangle,theta1=mytheta1,theta2=mytheta2,lw=1.5,color='r')
        ax = plt.gca()
        ax.add_patch(arc)
        

    def visualize(self,x,u=np.zeros([3,1])):
        x = np.array(x).flatten()
        n = self.numLinks
        q = x[n:self.numStates]
        
        plt.clf()

        for i in range(0,self.numLinks):
            fk = self.FK[i](q,self.l).reshape(4,4)
            theta = np.arctan2(fk[1,0],fk[0,0])
            self.draw_link(self.l,fk[0:2,3],theta)

            # if(i==1):
            #     self.draw_torque(self.l,[0,-self.l/2.0],0.0,u[0])
            # else:
            #     fk = self.FK[i-1](q,self.l).reshape(4,4)
            #     self.draw_torque(self.l,fk[0:2,3],theta,u[i])
        
        plt.ion()
        axis_length = self.numLinks*self.l
        plt.axis(xmin=-axis_length,xmax=axis_length,ymin=-axis_length,ymax=axis_length)
        plt.pause(.00000001)
        
        plt.show()
        

if __name__=='__main__':

    l = .25
    m = .25
    I = m*l*l/12.0
    g = 9.81
    damping = 0.02
    
    sys = N12link(l,m,I,g,damping)
    
    x = np.zeros([sys.numStates,1])
    x[sys.numStates/2] = 1.0
    u = np.ones([sys.numInputs,1])    
    x0 = deepcopy(x)
    
    dt = .01
    
    fig = plt.gca()
    plt.ion()

    Kp = 500.0*np.eye(sys.numLinks)
    Kd = 2.0*np.eye(sys.numLinks)
    
    horizon = 5000
    for i in range(0,horizon):

        u = Kp.dot(1.0-x[sys.numLinks:sys.numStates]) - Kd.dot(x[0:sys.numLinks])

        x = sys.forward_simulate_dt(x,u*0,dt)

        if i%5==0:
            sys.visualize(x)
