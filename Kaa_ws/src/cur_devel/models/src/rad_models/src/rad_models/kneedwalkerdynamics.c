#include "kneedwalkerdynamics.h"
#include <math.h>

namespace py = pybind11;

py::array calc_M(std::vector<double> q,double l,double mtorso,double mthigh,double mcalf,double Itorso,double Ithigh,double Icalf)
{
	 double out[49];

	 out[0] = 2.0*mcalf + 2.0*mthigh + 1.0*mtorso;
	 out[1] = 0;
	 out[2] = 0.5*l*(2*mcalf*cos(q[2]) + 2*mcalf*cos(q[2] + q[3]) + 2*mcalf*cos(q[2] + q[5]) + mcalf*cos(q[2] + q[3] + q[4]) + mcalf*cos(q[2] + q[5] + q[6]) + 2*mthigh*cos(q[2]) + mthigh*cos(q[2] + q[3]) + mthigh*cos(q[2] + q[5]));
	 out[3] = 0.5*l*(mcalf*(2*cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4])) + mthigh*cos(q[2] + q[3]));
	 out[4] = 0.5*l*mcalf*cos(q[2] + q[3] + q[4]);
	 out[5] = 0.5*l*(mcalf*(2*cos(q[2] + q[5]) + cos(q[2] + q[5] + q[6])) + mthigh*cos(q[2] + q[5]));
	 out[6] = 0.5*l*mcalf*cos(q[2] + q[5] + q[6]);
	 out[7] = 0;
	 out[8] = 2.0*mcalf + 2.0*mthigh + 1.0*mtorso;
	 out[9] = 0.5*l*(2*mcalf*sin(q[2]) + 2*mcalf*sin(q[2] + q[3]) + 2*mcalf*sin(q[2] + q[5]) + mcalf*sin(q[2] + q[3] + q[4]) + mcalf*sin(q[2] + q[5] + q[6]) + 2*mthigh*sin(q[2]) + mthigh*sin(q[2] + q[3]) + mthigh*sin(q[2] + q[5]));
	 out[10] = 0.5*l*(mcalf*(2*sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + mthigh*sin(q[2] + q[3]));
	 out[11] = 0.5*l*mcalf*sin(q[2] + q[3] + q[4]);
	 out[12] = 0.5*l*(mcalf*(2*sin(q[2] + q[5]) + sin(q[2] + q[5] + q[6])) + mthigh*sin(q[2] + q[5]));
	 out[13] = 0.5*l*mcalf*sin(q[2] + q[5] + q[6]);
	 out[14] = 0.5*l*(2*mcalf*cos(q[2]) + 2*mcalf*cos(q[2] + q[3]) + 2*mcalf*cos(q[2] + q[5]) + mcalf*cos(q[2] + q[3] + q[4]) + mcalf*cos(q[2] + q[5] + q[6]) + 2*mthigh*cos(q[2]) + mthigh*cos(q[2] + q[3]) + mthigh*cos(q[2] + q[5]));
	 out[15] = 0.5*l*(2*mcalf*sin(q[2]) + 2*mcalf*sin(q[2] + q[3]) + 2*mcalf*sin(q[2] + q[5]) + mcalf*sin(q[2] + q[3] + q[4]) + mcalf*sin(q[2] + q[5] + q[6]) + 2*mthigh*sin(q[2]) + mthigh*sin(q[2] + q[3]) + mthigh*sin(q[2] + q[5]));
	 out[16] = 2.0*Icalf + 2.0*Ithigh + 1.0*Itorso + 1.0*pow(l, 2)*mcalf*cos(q[3]) + 1.0*pow(l, 2)*mcalf*cos(q[4]) + 1.0*pow(l, 2)*mcalf*cos(q[5]) + 1.0*pow(l, 2)*mcalf*cos(q[6]) + 0.5*pow(l, 2)*mcalf*cos(q[3] + q[4]) + 0.5*pow(l, 2)*mcalf*cos(q[5] + q[6]) + 3.0*pow(l, 2)*mcalf + 0.5*pow(l, 2)*mthigh*cos(q[3]) + 0.5*pow(l, 2)*mthigh*cos(q[5]) + 1.0*pow(l, 2)*mthigh;
	 out[17] = 1.0*Icalf + 1.0*Ithigh + 0.5*pow(l, 2)*mcalf*cos(q[3]) + 1.0*pow(l, 2)*mcalf*cos(q[4]) + 0.25*pow(l, 2)*mcalf*cos(q[3] + q[4]) + 1.25*pow(l, 2)*mcalf + 0.25*pow(l, 2)*mthigh*cos(q[3]) + 0.25*pow(l, 2)*mthigh;
	 out[18] = 1.0*Icalf + 0.5*pow(l, 2)*mcalf*cos(q[4]) + 0.25*pow(l, 2)*mcalf*cos(q[3] + q[4]) + 0.25*pow(l, 2)*mcalf;
	 out[19] = 1.0*Icalf + 1.0*Ithigh + 0.5*pow(l, 2)*mcalf*cos(q[5]) + 1.0*pow(l, 2)*mcalf*cos(q[6]) + 0.25*pow(l, 2)*mcalf*cos(q[5] + q[6]) + 1.25*pow(l, 2)*mcalf + 0.25*pow(l, 2)*mthigh*cos(q[5]) + 0.25*pow(l, 2)*mthigh;
	 out[20] = 1.0*Icalf + 0.5*pow(l, 2)*mcalf*cos(q[6]) + 0.25*pow(l, 2)*mcalf*cos(q[5] + q[6]) + 0.25*pow(l, 2)*mcalf;
	 out[21] = 0.5*l*(mcalf*(2*cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4])) + mthigh*cos(q[2] + q[3]));
	 out[22] = 0.5*l*(mcalf*(2*sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + mthigh*sin(q[2] + q[3]));
	 out[23] = 1.0*Icalf + 1.0*Ithigh + 0.5*pow(l, 2)*mcalf*cos(q[3]) + 1.0*pow(l, 2)*mcalf*cos(q[4]) + 0.25*pow(l, 2)*mcalf*cos(q[3] + q[4]) + 1.25*pow(l, 2)*mcalf + 0.25*pow(l, 2)*mthigh*cos(q[3]) + 0.25*pow(l, 2)*mthigh;
	 out[24] = 1.0*Icalf + 1.0*Ithigh + 1.0*pow(l, 2)*mcalf*cos(q[4]) + 1.25*pow(l, 2)*mcalf + 0.25*pow(l, 2)*mthigh;
	 out[25] = 1.0*Icalf + 0.5*pow(l, 2)*mcalf*cos(q[4]) + 0.25*pow(l, 2)*mcalf;
	 out[26] = 0;
	 out[27] = 0;
	 out[28] = 0.5*l*mcalf*cos(q[2] + q[3] + q[4]);
	 out[29] = 0.5*l*mcalf*sin(q[2] + q[3] + q[4]);
	 out[30] = 1.0*Icalf + 0.5*pow(l, 2)*mcalf*cos(q[4]) + 0.25*pow(l, 2)*mcalf*cos(q[3] + q[4]) + 0.25*pow(l, 2)*mcalf;
	 out[31] = 1.0*Icalf + 0.5*pow(l, 2)*mcalf*cos(q[4]) + 0.25*pow(l, 2)*mcalf;
	 out[32] = 1.0*Icalf + 0.25*pow(l, 2)*mcalf;
	 out[33] = 0;
	 out[34] = 0;
	 out[35] = 0.5*l*(mcalf*(2*cos(q[2] + q[5]) + cos(q[2] + q[5] + q[6])) + mthigh*cos(q[2] + q[5]));
	 out[36] = 0.5*l*(mcalf*(2*sin(q[2] + q[5]) + sin(q[2] + q[5] + q[6])) + mthigh*sin(q[2] + q[5]));
	 out[37] = 1.0*Icalf + 1.0*Ithigh + 0.5*pow(l, 2)*mcalf*cos(q[5]) + 1.0*pow(l, 2)*mcalf*cos(q[6]) + 0.25*pow(l, 2)*mcalf*cos(q[5] + q[6]) + 1.25*pow(l, 2)*mcalf + 0.25*pow(l, 2)*mthigh*cos(q[5]) + 0.25*pow(l, 2)*mthigh;
	 out[38] = 0;
	 out[39] = 0;
	 out[40] = 1.0*Icalf + 1.0*Ithigh + 1.0*pow(l, 2)*mcalf*cos(q[6]) + 1.25*pow(l, 2)*mcalf + 0.25*pow(l, 2)*mthigh;
	 out[41] = 1.0*Icalf + 0.5*pow(l, 2)*mcalf*cos(q[6]) + 0.25*pow(l, 2)*mcalf;
	 out[42] = 0.5*l*mcalf*cos(q[2] + q[5] + q[6]);
	 out[43] = 0.5*l*mcalf*sin(q[2] + q[5] + q[6]);
	 out[44] = 1.0*Icalf + 0.5*pow(l, 2)*mcalf*cos(q[6]) + 0.25*pow(l, 2)*mcalf*cos(q[5] + q[6]) + 0.25*pow(l, 2)*mcalf;
	 out[45] = 0;
	 out[46] = 0;
	 out[47] = 1.0*Icalf + 0.5*pow(l, 2)*mcalf*cos(q[6]) + 0.25*pow(l, 2)*mcalf;
	 out[48] = 1.0*Icalf + 0.25*pow(l, 2)*mcalf;
	return py::array(49,out);

}

py::array calc_grav(std::vector<double> q,double l,double mtorso,double mthigh,double mcalf,double g)
{
	 double out[7];

	 out[0] = 0;
	 out[1] = g*(2*mcalf + 2*mthigh + mtorso);
	 out[2] = (1.0/2.0)*g*l*(2*mcalf*sin(q[2]) + 2*mcalf*sin(q[2] + q[3]) + 2*mcalf*sin(q[2] + q[5]) + mcalf*sin(q[2] + q[3] + q[4]) + mcalf*sin(q[2] + q[5] + q[6]) + 2*mthigh*sin(q[2]) + mthigh*sin(q[2] + q[3]) + mthigh*sin(q[2] + q[5]));
	 out[3] = (1.0/2.0)*g*l*(mcalf*(2*sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + mthigh*sin(q[2] + q[3]));
	 out[4] = (1.0/2.0)*g*l*mcalf*sin(q[2] + q[3] + q[4]);
	 out[5] = (1.0/2.0)*g*l*(mcalf*(2*sin(q[2] + q[5]) + sin(q[2] + q[5] + q[6])) + mthigh*sin(q[2] + q[5]));
	 out[6] = (1.0/2.0)*g*l*mcalf*sin(q[2] + q[5] + q[6]);
	return py::array(7,out);

}

py::array calc_c(std::vector<double> q,std::vector<double> qd,double l,double mtorso,double mthigh,double mcalf)
{
	 double out[7];

	 out[0] = -l*(1.0*mcalf*pow(qd[2], 2)*sin(q[2]) + 1.0*mcalf*pow(qd[2], 2)*sin(q[2] + q[3]) + 1.0*mcalf*pow(qd[2], 2)*sin(q[2] + q[5]) + 0.5*mcalf*pow(qd[2], 2)*sin(q[2] + q[3] + q[4]) + 0.5*mcalf*pow(qd[2], 2)*sin(q[2] + q[5] + q[6]) + 2.0*mcalf*qd[2]*qd[3]*sin(q[2] + q[3]) + 1.0*mcalf*qd[2]*qd[3]*sin(q[2] + q[3] + q[4]) + 1.0*mcalf*qd[2]*qd[4]*sin(q[2] + q[3] + q[4]) + 2.0*mcalf*qd[2]*qd[5]*sin(q[2] + q[5]) + 1.0*mcalf*qd[2]*qd[5]*sin(q[2] + q[5] + q[6]) + 1.0*mcalf*qd[2]*qd[6]*sin(q[2] + q[5] + q[6]) + 1.0*mcalf*pow(qd[3], 2)*sin(q[2] + q[3]) + 0.5*mcalf*pow(qd[3], 2)*sin(q[2] + q[3] + q[4]) + 1.0*mcalf*qd[3]*qd[4]*sin(q[2] + q[3] + q[4]) + 0.5*mcalf*pow(qd[4], 2)*sin(q[2] + q[3] + q[4]) + 1.0*mcalf*pow(qd[5], 2)*sin(q[2] + q[5]) + 0.5*mcalf*pow(qd[5], 2)*sin(q[2] + q[5] + q[6]) + 1.0*mcalf*qd[5]*qd[6]*sin(q[2] + q[5] + q[6]) + 0.5*mcalf*pow(qd[6], 2)*sin(q[2] + q[5] + q[6]) + 1.0*mthigh*pow(qd[2], 2)*sin(q[2]) + 0.5*mthigh*pow(qd[2], 2)*sin(q[2] + q[3]) + 0.5*mthigh*pow(qd[2], 2)*sin(q[2] + q[5]) + 1.0*mthigh*qd[2]*qd[3]*sin(q[2] + q[3]) + 1.0*mthigh*qd[2]*qd[5]*sin(q[2] + q[5]) + 0.5*mthigh*pow(qd[3], 2)*sin(q[2] + q[3]) + 0.5*mthigh*pow(qd[5], 2)*sin(q[2] + q[5]));
	 out[1] = l*(1.0*mcalf*pow(qd[2], 2)*cos(q[2]) + 1.0*mcalf*pow(qd[2], 2)*cos(q[2] + q[3]) + 1.0*mcalf*pow(qd[2], 2)*cos(q[2] + q[5]) + 0.5*mcalf*pow(qd[2], 2)*cos(q[2] + q[3] + q[4]) + 0.5*mcalf*pow(qd[2], 2)*cos(q[2] + q[5] + q[6]) + 2.0*mcalf*qd[2]*qd[3]*cos(q[2] + q[3]) + 1.0*mcalf*qd[2]*qd[3]*cos(q[2] + q[3] + q[4]) + 1.0*mcalf*qd[2]*qd[4]*cos(q[2] + q[3] + q[4]) + 2.0*mcalf*qd[2]*qd[5]*cos(q[2] + q[5]) + 1.0*mcalf*qd[2]*qd[5]*cos(q[2] + q[5] + q[6]) + 1.0*mcalf*qd[2]*qd[6]*cos(q[2] + q[5] + q[6]) + 1.0*mcalf*pow(qd[3], 2)*cos(q[2] + q[3]) + 0.5*mcalf*pow(qd[3], 2)*cos(q[2] + q[3] + q[4]) + 1.0*mcalf*qd[3]*qd[4]*cos(q[2] + q[3] + q[4]) + 0.5*mcalf*pow(qd[4], 2)*cos(q[2] + q[3] + q[4]) + 1.0*mcalf*pow(qd[5], 2)*cos(q[2] + q[5]) + 0.5*mcalf*pow(qd[5], 2)*cos(q[2] + q[5] + q[6]) + 1.0*mcalf*qd[5]*qd[6]*cos(q[2] + q[5] + q[6]) + 0.5*mcalf*pow(qd[6], 2)*cos(q[2] + q[5] + q[6]) + 1.0*mthigh*pow(qd[2], 2)*cos(q[2]) + 0.5*mthigh*pow(qd[2], 2)*cos(q[2] + q[3]) + 0.5*mthigh*pow(qd[2], 2)*cos(q[2] + q[5]) + 1.0*mthigh*qd[2]*qd[3]*cos(q[2] + q[3]) + 1.0*mthigh*qd[2]*qd[5]*cos(q[2] + q[5]) + 0.5*mthigh*pow(qd[3], 2)*cos(q[2] + q[3]) + 0.5*mthigh*pow(qd[5], 2)*cos(q[2] + q[5]));
	 out[2] = -pow(l, 2)*(1.0*mcalf*qd[2]*qd[3]*sin(q[3]) + 0.5*mcalf*qd[2]*qd[3]*sin(q[3] + q[4]) + 1.0*mcalf*qd[2]*qd[4]*sin(q[4]) + 0.5*mcalf*qd[2]*qd[4]*sin(q[3] + q[4]) + 1.0*mcalf*qd[2]*qd[5]*sin(q[5]) + 0.5*mcalf*qd[2]*qd[5]*sin(q[5] + q[6]) + 1.0*mcalf*qd[2]*qd[6]*sin(q[6]) + 0.5*mcalf*qd[2]*qd[6]*sin(q[5] + q[6]) + 0.5*mcalf*pow(qd[3], 2)*sin(q[3]) + 0.25*mcalf*pow(qd[3], 2)*sin(q[3] + q[4]) + 1.0*mcalf*qd[3]*qd[4]*sin(q[4]) + 0.5*mcalf*qd[3]*qd[4]*sin(q[3] + q[4]) + 0.5*mcalf*pow(qd[4], 2)*sin(q[4]) + 0.25*mcalf*pow(qd[4], 2)*sin(q[3] + q[4]) + 0.5*mcalf*pow(qd[5], 2)*sin(q[5]) + 0.25*mcalf*pow(qd[5], 2)*sin(q[5] + q[6]) + 1.0*mcalf*qd[5]*qd[6]*sin(q[6]) + 0.5*mcalf*qd[5]*qd[6]*sin(q[5] + q[6]) + 0.5*mcalf*pow(qd[6], 2)*sin(q[6]) + 0.25*mcalf*pow(qd[6], 2)*sin(q[5] + q[6]) + 0.5*mthigh*qd[2]*qd[3]*sin(q[3]) + 0.5*mthigh*qd[2]*qd[5]*sin(q[5]) + 0.25*mthigh*pow(qd[3], 2)*sin(q[3]) + 0.25*mthigh*pow(qd[5], 2)*sin(q[5]));
	 out[3] = pow(l, 2)*(0.5*mcalf*pow(qd[2], 2)*sin(q[3]) + 0.25*mcalf*pow(qd[2], 2)*sin(q[3] + q[4]) - 1.0*mcalf*qd[2]*qd[4]*sin(q[4]) - 1.0*mcalf*qd[3]*qd[4]*sin(q[4]) - 0.5*mcalf*pow(qd[4], 2)*sin(q[4]) + 0.25*mthigh*pow(qd[2], 2)*sin(q[3]));
	 out[4] = pow(l, 2)*mcalf*(0.5*pow(qd[2], 2)*sin(q[4]) + 0.25*pow(qd[2], 2)*sin(q[3] + q[4]) + 1.0*qd[2]*qd[3]*sin(q[4]) + 0.5*pow(qd[3], 2)*sin(q[4]));
	 out[5] = pow(l, 2)*(0.5*mcalf*pow(qd[2], 2)*sin(q[5]) + 0.25*mcalf*pow(qd[2], 2)*sin(q[5] + q[6]) - 1.0*mcalf*qd[2]*qd[6]*sin(q[6]) - 1.0*mcalf*qd[5]*qd[6]*sin(q[6]) - 0.5*mcalf*pow(qd[6], 2)*sin(q[6]) + 0.25*mthigh*pow(qd[2], 2)*sin(q[5]));
	 out[6] = pow(l, 2)*mcalf*(0.5*pow(qd[2], 2)*sin(q[6]) + 0.25*pow(qd[2], 2)*sin(q[5] + q[6]) + 1.0*qd[2]*qd[5]*sin(q[6]) + 0.5*pow(qd[5], 2)*sin(q[6]));
	return py::array(7,out);

}


py::array fk0(std::vector<double> q,double l)
{
	 double out[3];

	 out[0] = q[0];
	 out[1] = 0;
	 out[2] = 0;
	return py::array(3,out);

}

py::array fk1(std::vector<double> q,double l)
{
	 double out[3];

	 out[0] = q[0];
	 out[1] = q[1];
	 out[2] = 0;
	return py::array(3,out);

}

py::array fkhead(std::vector<double> q,double l)
{
	 double out[3];

	 out[0] = (1.0/2.0)*l*sin(q[2]) + q[0];
	 out[1] = (1.0/2.0)*l*cos(q[2]) + q[1];
	 out[2] = q[2];
	return py::array(3,out);

}

py::array fk2(std::vector<double> q,double l)
{
	 double out[3];

	 out[0] = (1.0/2.0)*l*sin(q[2]) + q[0];
	 out[1] = -1.0/2.0*l*cos(q[2]) + q[1];
	 out[2] = q[2];
	return py::array(3,out);

}

py::array fk3(std::vector<double> q,double l)
{
	 double out[3];

	 out[0] = (1.0/2.0)*l*sin(q[2]) + l*sin(q[2] + q[3]) + q[0];
	 out[1] = -1.0/2.0*l*cos(q[2]) - l*cos(q[2] + q[3]) + q[1];
	 out[2] = q[2] + q[3];
	return py::array(3,out);

}

py::array fk4(std::vector<double> q,double l)
{
	 double out[3];

	 out[0] = l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + (1.0/2.0)*l*sin(q[2]) + q[0];
	 out[1] = l*(-cos(q[2] + q[3]) - cos(q[2] + q[3] + q[4])) - 1.0/2.0*l*cos(q[2]) + q[1];
	 out[2] = q[2] + q[3] + q[4];
	return py::array(3,out);

}

py::array fk5(std::vector<double> q,double l)
{
	 double out[3];

	 out[0] = (1.0/2.0)*l*sin(q[2]) + l*sin(q[2] + q[5]) + q[0];
	 out[1] = -1.0/2.0*l*cos(q[2]) - l*cos(q[2] + q[5]) + q[1];
	 out[2] = q[2] + q[5];
	return py::array(3,out);

}

py::array fk6(std::vector<double> q,double l)
{
	 double out[3];

	 out[0] = l*(sin(q[2] + q[5]) + sin(q[2] + q[5] + q[6])) + (1.0/2.0)*l*sin(q[2]) + q[0];
	 out[1] = l*(-cos(q[2] + q[5]) - cos(q[2] + q[5] + q[6])) - 1.0/2.0*l*cos(q[2]) + q[1];
	 out[2] = q[2] + q[5] + q[6];
	return py::array(3,out);

}

py::array jac0(std::vector<double> q,double l)
{
	 double out[21];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = 0;
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 0;
	 out[11] = 0;
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 0;
	return py::array(21,out);

}

py::array jac1(std::vector<double> q,double l)
{
	 double out[21];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = 0;
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 1;
	 out[9] = 0;
	 out[10] = 0;
	 out[11] = 0;
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 0;
	return py::array(21,out);

}

py::array jachead(std::vector<double> q,double l)
{
	 double out[21];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = (1.0/2.0)*l*cos(q[2]);
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 1;
	 out[9] = -1.0/2.0*l*sin(q[2]);
	 out[10] = 0;
	 out[11] = 0;
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 1;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 0;
	return py::array(21,out);

}


py::array jac2(std::vector<double> q,double l)
{
	 double out[21];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = (1.0/2.0)*l*cos(q[2]);
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 1;
	 out[9] = (1.0/2.0)*l*sin(q[2]);
	 out[10] = 0;
	 out[11] = 0;
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 1;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 0;
	return py::array(21,out);

}

py::array jac3(std::vector<double> q,double l)
{
	 double out[21];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = (1.0/2.0)*l*cos(q[2]) + l*cos(q[2] + q[3]);
	 out[3] = l*cos(q[2] + q[3]);
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 1;
	 out[9] = (1.0/2.0)*l*sin(q[2]) + l*sin(q[2] + q[3]);
	 out[10] = l*sin(q[2] + q[3]);
	 out[11] = 0;
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 1;
	 out[17] = 1;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 0;
	return py::array(21,out);

}

py::array jac4(std::vector<double> q,double l)
{
	 double out[21];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = l*(cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4])) + (1.0/2.0)*l*cos(q[2]);
	 out[3] = l*(cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4]));
	 out[4] = l*cos(q[2] + q[3] + q[4]);
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 1;
	 out[9] = l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + (1.0/2.0)*l*sin(q[2]);
	 out[10] = l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4]));
	 out[11] = l*sin(q[2] + q[3] + q[4]);
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 1;
	 out[17] = 1;
	 out[18] = 1;
	 out[19] = 0;
	 out[20] = 0;
	return py::array(21,out);

}

py::array jac5(std::vector<double> q,double l)
{
	 double out[21];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = (1.0/2.0)*l*cos(q[2]) + l*cos(q[2] + q[5]);
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = l*cos(q[2] + q[5]);
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 1;
	 out[9] = (1.0/2.0)*l*sin(q[2]) + l*sin(q[2] + q[5]);
	 out[10] = 0;
	 out[11] = 0;
	 out[12] = l*sin(q[2] + q[5]);
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 1;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 1;
	 out[20] = 0;
	return py::array(21,out);

}

py::array jac6(std::vector<double> q,double l)
{
	 double out[21];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = l*(cos(q[2] + q[5]) + cos(q[2] + q[5] + q[6])) + (1.0/2.0)*l*cos(q[2]);
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = l*(cos(q[2] + q[5]) + cos(q[2] + q[5] + q[6]));
	 out[6] = l*cos(q[2] + q[5] + q[6]);
	 out[7] = 0;
	 out[8] = 1;
	 out[9] = l*(sin(q[2] + q[5]) + sin(q[2] + q[5] + q[6])) + (1.0/2.0)*l*sin(q[2]);
	 out[10] = 0;
	 out[11] = 0;
	 out[12] = l*(sin(q[2] + q[5]) + sin(q[2] + q[5] + q[6]));
	 out[13] = l*sin(q[2] + q[5] + q[6]);
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 1;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 1;
	 out[20] = 1;
	return py::array(21,out);

}

PYBIND11_MODULE(kneedwalkerdynamics,m) {
  m.def("calc_M",&calc_M,"a function description");
  m.def("calc_grav",&calc_grav,"another function description");
  m.def("calc_c",&calc_c,"yet another function description");
  m.def("fk0",&fk0,"a function description");
  m.def("fk1",&fk1,"a function description");
  m.def("fkhead",&fkhead,"a function description");  
  m.def("fk2",&fk2,"a function description");
  m.def("fk3",&fk3,"a function description");
  m.def("fk4",&fk4,"a function description");
  m.def("fk5",&fk5,"a function description");
  m.def("fk6",&fk6,"a function description");
  m.def("jac0",&jac0,"a function description");
  m.def("jac1",&jac1,"a function description");
  m.def("jachead",&jachead,"a function description");  
  m.def("jac2",&jac2,"a function description");
  m.def("jac3",&jac3,"a function description");
  m.def("jac4",&jac4,"a function description");
  m.def("jac5",&jac5,"a function description");
  m.def("jac6",&jac6,"a function description");
}
