from rad_models.Model import Model
import lemkelcp.lemkelcp as lemkelcp
from kneedwalkerdynamics import *
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy

np.set_printoptions(precision=2)

class KneedWalker(Model):

    def __init__(self,mtorso=7.0,Itorso=1.0,mthigh=5.0,Ithigh=.9,mcalf=4.0,Icalf=0.8,l=1.0,g=9.81):
        
        self.numStates = 14
        self.numInputs = 4

        self.tauMax = 1000.0
        # self.tauMax = np.inf
        
        self.mtorso = mtorso
        self.Itorso = Itorso
        self.mthigh = mthigh
        self.Ithigh = Ithigh
        self.mcalf = mcalf
        self.Icalf = Icalf
        self.l = l
        self.g = g
        self.e_restitution = 0.0
        self.muStatic = 0.99
        self.muDynamic = .3


    def get_contact_jacobian(self,q,l):
        normalJacobian = np.zeros([1,7])
        tangentJacobian = np.zeros([1,7])        
        penetrations = np.zeros([1,1])
        # if(fkhead(q,l)[1]<=0):
        #     J = np.reshape(np.reshape(jachead(q,l),[3,7])[1,:],[1,7])
        #     normalJacobian = np.vstack([normalJacobian,J])
        #     J = np.reshape(np.reshape(jachead(q,l),[3,7])[0,:],[1,7])
        #     tangentJacobian = np.vstack([tangentJacobian,J])
        #     penetrations = np.vstack([penetrations,fkhead(q,l)[1]])
        # if(fk2(q,l)[1]<=0):
        #     J = np.reshape(np.reshape(jac2(q,l),[3,7])[1,:],[1,7])
        #     normalJacobian = np.vstack([normalJacobian,J])
        #     J = np.reshape(np.reshape(jac2(q,l),[3,7])[0,:],[1,7])
        #     tangentJacobian = np.vstack([tangentJacobian,J])            
        #     penetrations = np.vstack([penetrations,fk2(q,l)[1]])            
        # if(fk3(q,l)[1]<=0):
        #     J = np.reshape(np.reshape(jac3(q,l),[3,7])[1,:],[1,7])
        #     normalJacobian = np.vstack([normalJacobian,J])
        #     J = np.reshape(np.reshape(jac3(q,l),[3,7])[0,:],[1,7])
        #     tangentJacobian = np.vstack([tangentJacobian,J])                        
        #     penetrations = np.vstack([penetrations,fk3(q,l)[1]])
        if(fk4(q,l)[1]<=0):
            J = np.reshape(np.reshape(jac4(q,l),[3,7])[1,:],[1,7])
            normalJacobian = np.vstack([normalJacobian,J])
            J = np.reshape(np.reshape(jac4(q,l),[3,7])[0,:],[1,7])
            tangentJacobian = np.vstack([tangentJacobian,J])                        
            penetrations = np.vstack([penetrations,fk4(q,l)[1]])            
        # if(fk5(q,l)[1]<=0):
        #     J = np.reshape(np.reshape(jac5(q,l),[3,7])[1,:],[1,7])
        #     normalJacobian = np.vstack([normalJacobian,J])
        #     J = np.reshape(np.reshape(jac5(q,l),[3,7])[0,:],[1,7])
        #     tangentJacobian = np.vstack([tangentJacobian,J])                        
        #     penetrations = np.vstack([penetrations,fk5(q,l)[1]])            
        if(fk6(q,l)[1]<=0):
            J = np.reshape(np.reshape(jac6(q,l),[3,7])[1,:],[1,7])
            normalJacobian = np.vstack([normalJacobian,J])
            J = np.reshape(np.reshape(jac6(q,l),[3,7])[0,:],[1,7])
            tangentJacobian = np.vstack([tangentJacobian,J])                        
            penetrations = np.vstack([penetrations,fk6(q,l)[1]])            
        normalJacobian = normalJacobian[1:,:]
        tangentJacobian = tangentJacobian[1:,:]        
        penetrations = penetrations[1:,:]
        return normalJacobian,tangentJacobian,penetrations

    def get_contact_torques(self,q,qd,Minv,tau,JNormal,JTangent,penetrations,dt):
        numContacts = JNormal.shape[0]
        if numContacts == 0:
            tauContact = np.zeros([7,1])
            return tauContact
        else:
            qd = qd.reshape([7,1])
            q = q.reshape([7,1])            
            D = np.hstack([-JTangent.T,JTangent.T])
            e = np.kron(np.eye(numContacts),np.ones([2,1]))
            n = JNormal.T
            alpha0 = n.T.dot(q) - penetrations
            
            MLemke1 = np.hstack([D.T.dot(Minv).dot(D), D.T.dot(Minv).dot(n), e])
            MLemke2 = np.hstack([n.T.dot(Minv).dot(D)*dt, n.T.dot(Minv).dot(n)*dt, np.zeros([numContacts,numContacts])])
            MLemke3 = np.hstack([-e.T, np.diag([self.muStatic]*numContacts), np.zeros([numContacts,numContacts])])
            MLemke = np.vstack([MLemke1,MLemke2,MLemke3])
            
            qLemke = np.vstack([D.T.dot(qd + Minv.dot(tau*dt)),
                                n.T.dot(qd + Minv.dot(tau*dt))*dt + n.T.dot(q) - alpha0,
                                np.zeros([numContacts,1])])
            
            z,exit_code,exit_string = lemkelcp(MLemke,qLemke,maxIter=100000)
            
            if exit_code != 0:
                # print "Lemke algorithm failed to solve: ",exit_string
                tauContact = np.zeros([7,1])
            else:
                beta = np.reshape(z[0:2*numContacts],[2*numContacts,1])
                cn = np.reshape(z[2*numContacts:2*numContacts+numContacts],[numContacts,1])
                tauContact = (np.reshape(D.dot(beta),[7,1]) + np.reshape(n.dot(cn),[7,1]))/dt
            return tauContact
        
    def get_knee_cap_torques(self,q,qd,M,Minv,tau,dt):
        tauKneeCap = np.zeros([7,1])
        qd = qd.reshape(7,1)
        q = q.reshape(7,1)
        if q[4]>=0:
            J = np.array([[0,0,0,0,1.0,0,0]])
            MassInv = 1.0/J.dot(Minv).dot(J.T)
            FNormal = -MassInv*((q[4]/dt + J.dot(qd))/dt + J.dot(Minv).dot(tau))
            FNormal[FNormal>0] = 0.0            
            tauKneeCap += J.T.dot(FNormal)
        if q[6]>=0:
            J = np.array([[0,0,0,0,0,0,1.0]])
            MassInv = 1.0/J.dot(Minv).dot(J.T)
            FNormal = -MassInv*((q[6]/dt + J.dot(qd))/dt + J.dot(Minv).dot(tau))
            FNormal[FNormal>0] = 0.0            
            tauKneeCap += J.T.dot(FNormal)
            
        return np.reshape(tauKneeCap,[7,1])
            
    
    def get_vels(self,q,qd,tauControl,dt):

        # Find the number of trajectories and simulate in parallel...
        numSims = q.shape[1]
        vels = np.zeros([7,numSims])
        tauControl = np.vstack([np.zeros([3,1]),tauControl.reshape([4,1])])
        for i in range(0,numSims):
        
            M = calc_M(q[:,i],self.l,self.mtorso,self.mthigh,self.mcalf,self.Itorso,self.Ithigh,self.Icalf).squeeze().reshape([7,7])
            grav = calc_grav(q[:,i],self.l,self.mtorso,self.mthigh,self.mcalf,self.g).squeeze()
            c = calc_c(q[:,i],qd[:,i],self.l,self.mtorso,self.mthigh,self.mcalf).squeeze()
            damping = -.01*qd[:,i].T
            
            bias_tau = np.array(-grav - c - damping).reshape(7,1) + tauControl
            Minv = np.linalg.inv(M)

            JNormal,JTangent,penetrations = self.get_contact_jacobian(q[:,i],self.l)
            numContacts = JNormal.shape[0]

            tauContact = self.get_contact_torques(q[:,i],qd[:,i],Minv,bias_tau,JNormal,JTangent,penetrations,dt)
            # tauKneeCaps = self.get_knee_cap_torques(q[:,i],qd[:,i],M,Minv,bias_tau+tauContact,dt)
            
            tauTotal = bias_tau + tauContact# + tauKneeCaps
            
            vel = deepcopy(np.reshape(qd[:,i],[7,1])) + Minv.dot(tauTotal)*dt
            vels[:,i] = vel.flatten()

        return vels
    
    

    def forward_simulate_dt(self,x,u,dt):
        x = deepcopy(x)
        u = deepcopy(u.clip(-self.tauMax,self.tauMax))

        b = .001
        x = x.reshape([14,-1])
        u = u.reshape([self.numInputs,-1])
        
        vels = self.get_vels(x[7:14,:],x[0:7,:],u,dt).reshape([7,-1])
        x[0:7,:] = vels
        x[7:14,:] = x[7:14,:] + vels*dt

        x[2+7,:] = (x[2+7,:] + np.pi) % (2*np.pi) - np.pi
        x[3+7,:] = (x[3+7,:] + np.pi) % (2*np.pi) - np.pi
        x[4+7,:] = (x[4+7,:] + np.pi) % (2*np.pi) - np.pi
        x[5+7,:] = (x[5+7,:] + np.pi) % (2*np.pi) - np.pi
        x[6+7,:] = (x[6+7,:] + np.pi) % (2*np.pi) - np.pi
        return x

    def draw_link(self,l,CoM,theta):
        x = [CoM[0] + l/2.0*np.sin(theta),CoM[0] - l/2.0*np.sin(theta)]
        y = [CoM[1] - l/2.0*np.cos(theta),CoM[1] + l/2.0*np.cos(theta)]

        plt.plot(x,y)        

    def visualize(self,x):
        x = np.array(x).flatten()
        q = x[7:14]
        rA = [q[0],
              q[1]]
        
        rB = [q[0] + self.l/2.*(np.sin(q[2])+np.sin(q[2]+q[3])),
              q[1] - self.l/2.*(np.cos(q[2])+np.cos(q[2]+q[3]))]
        
        rC = [q[0] + self.l/2.*(np.sin(q[2])+np.sin(q[2]+q[3]+q[4])) + self.l*np.sin(q[2]+q[3]),
              q[1] - self.l/2.*(np.cos(q[2])+np.cos(q[2]+q[3]+q[4])) - self.l*np.cos(q[2]+q[3])]
        
        rD = [q[0] + self.l/2.*(np.sin(q[2])+np.sin(q[2]+q[5])),
              q[1] - self.l/2.*(np.cos(q[2])+np.cos(q[2]+q[5]))]
        
        rE = [q[0] + self.l/2.*(np.sin(q[2])+np.sin(q[2]+q[5]+q[6])) + self.l*np.sin(q[2]+q[5]),
              q[1] - self.l/2.*(np.cos(q[2])+np.cos(q[2]+q[5]+q[6])) - self.l*np.cos(q[2]+q[5])]
        
        plt.clf()
        plt.hlines(0,-15,15,linewidth=1.0)
        self.draw_link(self.l,rA,q[2])
        self.draw_link(self.l,rB,q[2]+q[3])
        self.draw_link(self.l,rC,q[2]+q[3]+q[4])    
        self.draw_link(self.l,rD,q[2]+q[5])
        self.draw_link(self.l,rE,q[2]+q[5]+q[6])
        plt.ion()
        plt.axis(xmin=-12,xmax=12,ymin=-10,ymax=10)
        # plt.axis(aspect='equal')
        plt.pause(.00000001)
        
        plt.show()
        

if __name__=='__main__':
    
    i = 0
    w = 0
    theta = np.pi/15
    hip1_des = -2*theta*np.cos(w*i + np.pi)
    hip2_des = -2*theta*np.cos(w*i)
    knee1_des = 3.0*theta*np.min([np.cos(i*w+np.pi),0])
    knee2_des = 3.0*theta*np.min([np.cos(i*w),0])
    torso_height = .5+np.max([np.cos(knee2_des)+np.cos(hip2_des),np.cos(knee1_des)+np.cos(hip1_des)])
    print(torso_height)
    x = np.matrix([[1.0,0.0,0.0,0.0,0.0,0.0,0.0, 0.0,torso_height,.0,hip1_des,knee1_des,hip2_des,knee2_des]]).T
    
    from copy import deepcopy
    x0 = deepcopy(x)

    walker = KneedWalker()
    
    dt = .02
    
    fig = plt.gca()
    plt.ion()
    plt.hlines(0,-15,15)

    u = np.array([0,0,0,0.0]).reshape(4,1)
    horizon = 50
    uSave = np.zeros([4,horizon])
    w = 0.05
    for i in range(0,horizon):

        hip1_des = -2*theta*np.cos(w*i + np.pi)
        hip2_des = -2*theta*np.cos(w*i)
        knee1_des = 3.0*theta*np.min([np.cos(i*w+np.pi),0])
        knee2_des = 3.0*theta*np.min([np.cos(i*w),0])
        print("knee1 des: ",knee1_des)
        print("knee2 des: ",knee2_des)        
        u[0] = 200.0*(hip1_des-x[3+7]) - 10.0*x[3]
        u[1] = 200.0*(knee1_des-x[4+7]) - 10.0*x[4]
        u[2] = 200.0*(hip2_des-x[5+7]) - 10.0*x[5]
        u[3] = 200.0*(knee2_des-x[6+7]) - 10.0*x[6]
        # u[0] = 100.0*(x0[3+7]-x[3+7]) - 10.0*x[3]
        # u[1] = 100.0*(x0[4+7]-x[4+7]) - 10.0*x[4]
        # u[2] = 100.0*(x0[5+7]-x[5+7]) - 10.0*x[5]
        # u[3] = 100.0*(x0[6+7]-x[6+7]) - 10.0*x[6]

        uSave[:,i] = u.flatten()

        # print "x: ",x.T
        x = walker.forward_simulate_dt(x,u,dt)

        if i%1==0:
            walker.visualize(x)

    import scipy.io as sio
    data_dict = {'x0':x0,
                 'dt':dt,
                 'U':uSave}
    sio.savemat('goodU',data_dict)
