from rad_models.Model import Model
from rad_models.BellowsArm2 import BellowsArm
import numpy as np
import torch
from torch.autograd import Variable
from copy import deepcopy
import matplotlib.pyplot as plt

class LearnedBellowsArm(BellowsArm):

    def __init__(self,use_gpu=True):
        self.numStates = 24
        self.numInputs = 12
        self.dt = .01
        
        self.use_gpu = use_gpu

        if self.use_gpu==True:
            # self.model = torch.load('/home/phil/git/byu/cur_devel/models/src/rad_models/src/rad_models/learned_models/BellowsArmModel.pt').eval().cuda()
            # self.model = torch.load('/home/radlab/git/byu/cur_devel/models/src/rad_models/src/rad_models/learned_models/BellowsArmResNetModel.pt').eval().cuda()
            self.model = torch.load('/home/radlab/git/byu/cur_devel/models/src/rad_models/src/rad_models/learned_models/BellowsArmSmallModel.pt').eval().cuda()
        else:
            self.model = torch.load('/home/phil/git/byu/cur_devel/models/src/rad_models/src/rad_models/learned_models/BellowsArmModel.pt').eval()

        torch.backends.cudnn.benchmark = True

    def forward_simulate_dt(self,x,u,dt=.01):
        x[0:12,:] = x[0:12,:]/400.0
        u = u/400.0
        
        batch_size = u.shape[1]
        u = np.reshape(u,[12,batch_size])
        inputs = np.vstack([x,u]).reshape(36,batch_size)

        # Get delta velocity from DNN        
        self.input_var = Variable(torch.from_numpy(inputs.T).float(),requires_grad=False).cuda()
        delta_velocity = self.model(self.input_var).cpu().detach().numpy().T

        x[0:12,:] = (x[0:12,:] + .019608*(u - x[0:12,:]))*400.0
        x[12:18,:] = x[12:18,:] + delta_velocity
        x[18:24,:] = x[18:24,:] + (x[12:18,:] -0.5*delta_velocity)*dt
        return x


if __name__=='__main__':
    sys = LearnedBellowsArm()

    q = np.ones((6,1))
    qd = np.zeros((6,1))
    p = np.array([[300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300]])
    
    x = np.vstack([p,qd,q])
    u = deepcopy(p)

    x0 = deepcopy(x)
    
    dt = .01
    
    ax = plt.gca(projection='3d')
    plt.ion()

    horizon = 5000
    for i in range(0,horizon):

        x = sys.forward_simulate_dt(x,u,dt)
        if i%5==0:
            print(x[18:24].T)
            sys.visualize(x,ax)

    
