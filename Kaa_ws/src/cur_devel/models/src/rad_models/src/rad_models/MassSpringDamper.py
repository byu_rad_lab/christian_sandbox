from rad_models.Model import Model
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy
from matplotlib import patches

class MassSpringDamper(Model):

    def __init__(self, mass=3.0, spring=3.0, damping=1.5, friction=0.0, uMax=np.inf, xMin=[-np.inf]*2, xMax=[np.inf]*2): # replace with any parameters your model will need
        """Initializes a mass spring damper model instance on Earth. No other planets allowed (g=9.81)."""
        # Referencing these member variables in your controller will allow
        # you to swap models out interchangeable, regardless of states and inputs

        self.numStates = 2      # the number of states for your system
        self.numInputs = 1      # the number of inputs for your system
        self.m = mass
        self.k = spring
        self.b = damping
        self.mu = friction
        self.g = 9.81

        self.uMax = uMax
        self.xMax = xMax
        self.xMin = xMin

    def calc_state_derivs(self,x,u):
        """Returns the x_dot for a given x, u, and dt"""
        x = deepcopy(x)
        u = deepcopy(u)
        x = x.reshape([self.numStates, -1])
        xdot = np.zeros(x.shape)
        xdot[0,:] = x[1,:]
        xdot[1,:] = (-self.k*x[0,:]/self.m - self.b*x[1,:]/self.m -
                     self.mu*self.g*np.sign(x[1,:]) + u/self.m)
        # print(np.sign(x[1,:]))
        
        return xdot
        
    def visualize(self,x,u=0):
        """Visualize the system at a given state, optionally visualizing an input, as well."""
        x = x.reshape(-1, 1)
        u = u.reshape(-1, 1)

        l_box = 0.5
        h_box = 0.5
        h_wall = 1.0
        l_gap = 1.0
        l_plate = (l_gap + l_box)*1.5
        n_coils = 6
        h_coils = 0.2

        xf = x[0].astype(float)


        box_x = [xf, xf, xf+l_box, xf+l_box, xf]
        box_y = [0, h_box, h_box, 0, 0]

        base_x = [l_plate-l_gap, -l_gap, -l_gap, -l_gap]
        base_y = [0, 0, h_wall, h_box/2]

        spacing = (l_gap + xf)/float(n_coils*2)

        add = np.arange(-l_gap, xf, spacing)

        # print(np.arange(-l_gap, xf, spacing))

        for i in range(n_coils*2):
            if i%2 == 0:
                base_y.append(h_box/2.0 - h_coils/2.0)
            else:
                base_y.append(h_box/2.0 + h_coils/2.0)
            base_x.append(add[i])

        base_x.append(xf)
        base_y.append(h_box/2.0 - h_coils/2.0)

        # print(len(base_x))
        # print(len(base_y))
                    

        plt.clf()
        ax = plt.gca()
        plt.plot(base_x, base_y, 'k')
        plt.plot(box_x, box_y, 'b')
        plt.axis([-2, 2, -2+h_wall/2.0, 2+h_wall/2.0])
        plt.ion()
        plt.show()
        plt.pause(.0000001)

        


if __name__=='__main__':
    # Put some test code here to crate and test a simple instance of your model

    sys = MassSpringDamper()
    x = np.zeros([sys.numStates, 1])
    u = np.zeros([sys.numInputs, 1])

    x[0] = 0.0
    u[0] = sys.k
    dt = 0.01

    t_max = 10
    steps = int(t_max / dt)

    for i in range(steps):

        x = sys.forward_simulate_dt(x,u,dt)

        if i%10==0:
            sys.visualize(x,u)
            # print(x)



    
