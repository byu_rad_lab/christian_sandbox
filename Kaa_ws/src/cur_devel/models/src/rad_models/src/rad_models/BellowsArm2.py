from rad_models.Model import Model
from bellowsarmdynamics import *
import numpy as np
from copy import deepcopy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

np.set_printoptions(precision=2)

class BellowsArm(Model):

    def __init__(self):
        self.numStates = 24
        self.numInputs = 12

        self.K_passive_damper = np.diag([6.3,6.3,6.3,6.3,6.3,6.3])*0.1 #This for linear MPC
        self.K_passive_damper = np.diag([6.3,6.3,6.3,6.3,6.3,6.3])*1.0 #This for nonlinear MPC
        self.K_passive_spring = np.diag([150,150,30,30,30,30.0])*0.75
        
        self.prs_to_torque = np.array([[.35,-.35,0,0,0,0,0,0,0,0,0,0],
                                       [0,0,.35,-.35,0,0,0,0,0,0,0,0],
                                       [0,0,0,0,.15,-.15,0,0,0,0,0,0],
                                       [0,0,0,0,0,0,.15,-.15,0,0,0,0],
                                       [0,0,0,0,0,0,0,0,.15,-.15,0,0],
                                       [0,0,0,0,0,0,0,0,0,0,.15,-.15]])*1.25
        self.alphas = np.eye(12)*2.0
        self.uMax = 400.0
        self.uMin = 8.0

        self.jt0Mass = 6.534
        self.jt1Mass = 6.534
        self.jt2Mass = 1.5
        self.jt0Ixx = .108
        self.jt0Iyy = .108
        self.jt0Izz = .023
        self.jt0Ixy = 0.0
        self.jt0Ixz = 0.0
        self.jt0Iyz = 0.0
        self.jt1Ixx = .108
        self.jt1Iyy = .108
        self.jt1Izz = .023
        self.jt1Ixy = 0.0
        self.jt1Ixz = 0.0
        self.jt1Iyz = 0.0
        self.jt2Ixx = .05
        self.jt2Iyy = .05
        self.jt2Izz = .023
        self.jt2Ixy = 0.0
        self.jt2Ixz = 0.0
        self.jt2Iyz = 0.0
        
        # These are from crude measurements
        # They are assumed equal for all bellows in a joint
        # self.alpha_fill = np.array([2.86,2.86,2.86,2.86,5.0,5.0,5.0,5.0,3.33,3.33,3.33,3.33])
        # self.alpha_vent = np.array([18.2,18.2,18.2,18.2,23.0,23.0,23.0,23.0,6.67,6.67,6.67,6.67])

    def calc_continuous_A_B_w(self,x,u,dt,fill=np.ones([12,1])):

        p = x[0:12]
        qd = x[12:18]
        q = x[18:24].flatten()
        q[np.abs(q)<.00001] = np.sign(q[abs(q)<.00001])*.00001
        q[q==0] = .00001
        q = q.tolist()

        M = calc_M(q).reshape(6,6)
        # while np.linalg.cond(M)>1000:
        #     M += np.eye(np.size(M,1))*1e-6
        M_inv = np.linalg.inv(M)
        
        A11 = -self.alphas
        A12 = np.zeros([12,6])
        A13 = np.zeros([12,6])
        A21 = np.matmul(M_inv,self.prs_to_torque)
        A22 = np.matmul(M_inv,-self.K_passive_damper)        
        A23 = np.matmul(M_inv,-self.K_passive_spring)        
        A31 = np.zeros([6,12])        
        A32 = np.eye(6)        
        A33 = np.zeros([6,6])

        A = np.vstack([np.hstack([A11,A12,A13]),np.hstack([A21,A22,A23]),np.hstack([A31,A32,A33])])
        
        B11 = self.alphas
        B21 = np.zeros([6,12])
        B31 = np.zeros([6,12])
        B = np.vstack([B11,B21,B31])

        gravity_torque = calc_grav(q,-9.81)

        w = np.matmul(M_inv,gravity_torque)
        w = np.vstack([np.zeros((12,1)),np.reshape(w,(6,1)),np.zeros((6,1))])
        
        return A,B,w
        
        
    def calc_discrete_A_B_w(self,x,u,dt=.01,fill=np.ones([12,1])):
        
        [A,B,w] = self.calc_continuous_A_B_w(x,u,fill)

        # Matrix exponentiation
        # [Ad,Bd] = self.discretize_A_and_B(A,B,dt)

        # First order implicit integration
        Ad = np.linalg.inv(np.eye(self.numStates) - A*dt)
        Bd = np.matmul(Ad,B)*dt

        # First order explicit integration        
        # Ad = np.eye(self.numStates) + A*dt
        # Bd = B*dt

        wd = w*dt
        
        return Ad, Bd, wd

        
    def forward_simulate_dt(self,x,u,dt = 0.01,fill=np.ones([12,1])):
        batch_size = x.shape[1]
        x_k_plus_one = np.zeros(x.shape)

        for i in range(0,batch_size):
            [Ad,Bd,wd] = self.calc_discrete_A_B_w(x[:,i],u[:,i],dt)
            x_k_plus_one[:,i] = np.matmul(Ad,x[:,i]) + np.matmul(Bd,u[:,i]) + wd.flatten()

        # for i in xrange(0,12):
        #     if (x_k_plus_one[i] < x[i]):
        #         x_k_plus_one[i] = (x_k_plus_one[i] - x[i])*20.0

        return x_k_plus_one

    def visualize(self,x,ax):
        
        ax.clear()
        q = x[18:24]
        self.plot_cylinder(ax, np.eye(4), fkJt0End(q).reshape(4,4), r=.1, color='r')
        self.plot_cylinder(ax, fkJt0End(q).reshape(4,4), fkL0End(q).reshape(4,4), r=.05, color='k')
        self.plot_cylinder(ax, fkL0End(q).reshape(4,4), fkJt1End(q).reshape(4,4), r=.05, color='r')
        self.plot_cylinder(ax, fkJt1End(q).reshape(4,4), fkL1End(q).reshape(4,4), r=.05, color='k')
        self.plot_cylinder(ax, fkL1End(q).reshape(4,4), fkJt2End(q).reshape(4,4), r=.05, color='r')

        self.set_axes_equal(ax)
        plt.ion()
        plt.show()
        plt.pause(.000001)

    def plot_cylinder(self,ax, g1, g2, r=0.05, color='k', n=10):
        th_vals = [2 * np.pi * i / n for i in range(n+1)]
        circle = [[r * np.sin(th), r * np.cos(th), 0.0, 1.0] for th in th_vals]
        circle = np.array(circle).T
        
        circle1 = g1.dot(circle)
        g3 = deepcopy(g1)  # Gets rid of twist in Z
        g3[0:3, 3] = g3[0:3, 3]+(g2[0:3, 3]-g1[0:3, 3])
        circle2 = g3.dot(circle)
        
        for i in range(circle1.shape[1] - 1):
            self.plot_line(ax, circle1[:, i], circle1[:, i+1], color, 1.0)
            self.plot_line(ax, circle2[:, i], circle2[:, i+1], color, 1.0)
            self.plot_line(ax, circle1[:, i], circle2[:, i], color, 1.0)
            self.plot_line(ax, circle1[:, i], circle2[:, i+1], color, 1.0)
            
    def plot_line(self,ax, a, b, color, linewidth_custom, alpha=1.0):  # Ax must be projection 3d
        ax.plot([a[0], b[0]], [a[1], b[1]], [a[2], b[2]], color=color, linewidth=linewidth_custom, alpha=alpha)

    def set_axes_equal(self,ax):
        
        ax.set_xlim3d([-1,1])
        ax.set_ylim3d([-1,1])
        ax.set_zlim3d([0,1])
    

if __name__=='__main__':
    import time
    sys = BellowsArm()

    q = np.ones((6,1))
    qd = np.zeros((6,1))
    p = np.array([[300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300]])
    
    x = np.vstack([p,qd,q])
    u = deepcopy(p*0)
    x0 = deepcopy(x)
    
    dt = .01
    
    ax = plt.gca(projection='3d')
    plt.ion()

    horizon = 5000
    for i in range(0,horizon):
        start = time.time()
        x = sys.forward_simulate_dt(x,u,dt)
        print("simulation time: ",time.time()-start)
        if i%5==0:
            print(x[18:24].T)
            sys.visualize(x,ax)
    
