from rad_models.Model import Model
import lemkelcp.lemkelcp as lemkelcp
from anklekneewalkerdynamics import *
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy

np.set_printoptions(precision=2)

class KneedWalker(Model):

    def __init__(self,mtorso=5.0,mthigh=5.0,mcalf=3.0,mfoot=2.0,l=0.5,lfoot=0.3,g=9.81,mustatic=0.6):
        
        self.numStates = 18
        self.numInputs = 6

        self.tauMax = 1000.0
        # self.tauMax = np.inf
        
        self.mtorso = mtorso
        self.Itorso = mtorso*l*l/12.0
        self.mthigh = mthigh
        self.Ithigh = mthigh*l*l/12.0
        self.mcalf = mcalf
        self.Icalf = mcalf*l*l/12.0
        self.mfoot = mfoot
        self.Ifoot = mfoot*lfoot*lfoot/12.0
        self.l = l
        self.lfoot = lfoot        
        self.g = g
        self.muStatic = mustatic


    def get_contact_jacobian(self,q,l):
        normalJacobian = np.zeros([1,9])
        tangentJacobian = np.zeros([1,9])        
        penetrations = np.zeros([1,1])
        if(fkToe1(q,self.l,self.lfoot)[1]<=0):
            J = np.reshape(np.reshape(jacToe1(q,self.l,self.lfoot),[3,9])[1,:],[1,9])
            normalJacobian = np.vstack([normalJacobian,J])
            J = np.reshape(np.reshape(jacToe1(q,self.l,self.lfoot),[3,9])[0,:],[1,9])
            tangentJacobian = np.vstack([tangentJacobian,J])                        
            penetrations = np.vstack([penetrations,fkToe1(q,self.l,self.lfoot)[1]])            
        if(fkHeel1(q,self.l,self.lfoot)[1]<=0):
            J = np.reshape(np.reshape(jacHeel1(q,self.l,self.lfoot),[3,9])[1,:],[1,9])
            normalJacobian = np.vstack([normalJacobian,J])
            J = np.reshape(np.reshape(jacHeel1(q,self.l,self.lfoot),[3,9])[0,:],[1,9])
            tangentJacobian = np.vstack([tangentJacobian,J])                        
            penetrations = np.vstack([penetrations,fkHeel1(q,self.l,self.lfoot)[1]])            
        if(fkToe2(q,self.l,self.lfoot)[1]<=0):
            J = np.reshape(np.reshape(jacToe2(q,self.l,self.lfoot),[3,9])[1,:],[1,9])
            normalJacobian = np.vstack([normalJacobian,J])
            J = np.reshape(np.reshape(jacToe2(q,self.l,self.lfoot),[3,9])[0,:],[1,9])
            tangentJacobian = np.vstack([tangentJacobian,J])                        
            penetrations = np.vstack([penetrations,fkToe2(q,self.l,self.lfoot)[1]])            
        if(fkHeel2(q,self.l,self.lfoot)[1]<=0):
            J = np.reshape(np.reshape(jacHeel2(q,self.l,self.lfoot),[3,9])[1,:],[1,9])
            normalJacobian = np.vstack([normalJacobian,J])
            J = np.reshape(np.reshape(jacHeel2(q,self.l,self.lfoot),[3,9])[0,:],[1,9])
            tangentJacobian = np.vstack([tangentJacobian,J])                        
            penetrations = np.vstack([penetrations,fkHeel2(q,self.l,self.lfoot)[1]])
        if(fkFoot1(q,self.l,self.lfoot)[1]<=0):
            J = np.reshape(np.reshape(jacFoot1(q,self.l,self.lfoot),[3,9])[1,:],[1,9])
            normalJacobian = np.vstack([normalJacobian,J])
            J = np.reshape(np.reshape(jacFoot1(q,self.l,self.lfoot),[3,9])[0,:],[1,9])
            tangentJacobian = np.vstack([tangentJacobian,J])                        
            penetrations = np.vstack([penetrations,fkFoot1(q,self.l,self.lfoot)[1]])
        if(fkFoot2(q,self.l,self.lfoot)[1]<=0):
            J = np.reshape(np.reshape(jacFoot2(q,self.l,self.lfoot),[3,9])[1,:],[1,9])
            normalJacobian = np.vstack([normalJacobian,J])
            J = np.reshape(np.reshape(jacFoot2(q,self.l,self.lfoot),[3,9])[0,:],[1,9])
            tangentJacobian = np.vstack([tangentJacobian,J])                        
            penetrations = np.vstack([penetrations,fkFoot2(q,self.l,self.lfoot)[1]])
        normalJacobian = normalJacobian[1:,:]
        tangentJacobian = tangentJacobian[1:,:]        
        penetrations = penetrations[1:,:]
        return normalJacobian,tangentJacobian,penetrations

    def get_contact_torques(self,q,qd,Minv,tau,JNormal,JTangent,penetrations,dt):
        numContacts = JNormal.shape[0]
        if numContacts == 0:
            tauContact = np.zeros([9,1])
            return tauContact
        else:
            qd = qd.reshape([9,1])
            q = q.reshape([9,1])            
            D = np.hstack([-JTangent.T,JTangent.T])
            e = np.kron(np.eye(numContacts),np.ones([2,1]))
            n = JNormal.T
            alpha0 = n.T.dot(q) - penetrations

            MLemke1 = np.hstack([D.T.dot(Minv).dot(D), D.T.dot(Minv).dot(n), e])
            MLemke2 = np.hstack([n.T.dot(Minv).dot(D)*dt, n.T.dot(Minv).dot(n)*dt, np.zeros([numContacts,numContacts])])
            MLemke3 = np.hstack([-e.T, np.diag([self.muStatic]*numContacts), np.zeros([numContacts,numContacts])])
            MLemke = np.vstack([MLemke1,MLemke2,MLemke3])

            alpha = 1e-3
            MLemke = MLemke + np.eye(MLemke.shape[0])*alpha

            # print "MLemke condition number: ",np.linalg.cond(MLemke)
            
            qLemke = np.vstack([D.T.dot(qd + Minv.dot(tau*dt)),
                                n.T.dot(qd + Minv.dot(tau*dt))*dt + n.T.dot(q) - alpha0,
                                np.zeros([numContacts,1])])

            z,exit_code,exit_string = lemkelcp(MLemke,qLemke,maxIter=100)
            
            if exit_code != 0:
                print("Lemke algorithm failed to solve: ",exit_string)
                tauContact=np.zeros([9,1])
            else:
                beta = np.reshape(z[0:2*numContacts],[2*numContacts,1])
                cn = np.reshape(z[2*numContacts:2*numContacts+numContacts],[numContacts,1])
                tauContact = (np.reshape(D.dot(beta),[9,1]) + np.reshape(n.dot(cn),[9,1]))/dt
            return tauContact
        
    def get_knee_cap_torques(self,q,qd,M,Minv,tau,dt):
        tauKneeCap = np.zeros([9,1])
        qd = qd.reshape(9,1)
        q = q.reshape(9,1)
        if q[4]>=0:
            J = np.array([[0,0,0,0,1.0,0,0,0,0]])
            MassInv = 1.0/J.dot(Minv).dot(J.T)
            FNormal = -MassInv*((q[4]/dt + J.dot(qd))/dt + J.dot(Minv).dot(tau))
            FNormal[FNormal>0] = 0.0            
            tauKneeCap += J.T.dot(FNormal)
        if q[7]>=0:
            J = np.array([[0,0,0,0,0,0,0,1.0,0]])
            MassInv = 1.0/J.dot(Minv).dot(J.T)
            FNormal = -MassInv*((q[7]/dt + J.dot(qd))/dt + J.dot(Minv).dot(tau))
            FNormal[FNormal>0] = 0.0            
            tauKneeCap += J.T.dot(FNormal)
            
        return np.reshape(tauKneeCap,[9,1])

    def get_q_in_foot_coordinates(self,q,which_foot):
        qnew = np.zeros([9,1])
        if which_foot==1:
            qnew[0] = (q[2]+q[3]+q[4])
            qnew[1] = -q[4]
            qnew[2] = -q[3]
            qnew[3] = np.pi-q[3]+q[6]
            qnew[4] = q[7]
        elif which_foot==2:
            qnew[0] = (q[2]+q[6]+q[7])
            qnew[1] = -q[7]
            qnew[2] = -q[6]
            qnew[3] = np.pi-q[6]+q[3]
            qnew[4] = q[4]
        return qnew
    
    def get_foot_coordinate_torques_in_general_coordinates(self,tau,which_foot):
        taunew = np.zeros([6,1])
        if which_foot==1:
            taunew[2] = -tau[0]
            taunew[1] = -tau[1]
            taunew[0] = -tau[2]
            taunew[3] = tau[3]
            taunew[4] = tau[4]
            taunew[5] = tau[5]
        if which_foot==2:
            taunew[5] = -tau[0]
            taunew[4] = -tau[1]
            taunew[3] = -tau[2]
            taunew[0] = tau[3]
            taunew[1] = tau[4]
            taunew[2] = tau[5]
        return taunew
    
    def get_vels(self,q,qd,tauControl,dt):

        # Find the number of trajectories and simulate in parallel...
        numSims = q.shape[1]
        vels = np.zeros([9,numSims])
        tauControl = np.vstack([np.zeros([3,1]),tauControl.reshape([6,1])])
        for i in range(0,numSims):
        
            M = calc_M(q[:,i],self.l,self.lfoot,self.mtorso,self.mthigh,self.mcalf,self.mfoot,self.Itorso,self.Ithigh,self.Icalf,self.Ifoot).squeeze().reshape([9,9])
            grav = calc_grav(q[:,i],self.l,self.lfoot,self.mtorso,self.mthigh,self.mcalf,self.mfoot,self.g).squeeze()
            c = calc_c(q[:,i],qd[:,i],self.l,self.lfoot,self.mtorso,self.mthigh,self.mcalf,self.mfoot).squeeze()
            # damping = -.01*np.power(qd[:,i].T,3.0)
            damping = -.05*qd[:,i].T
            

            bias_tau = np.array(-grav -c - damping).reshape(9,1)
            alpha = 1e-3
            Minv = np.linalg.inv(M+alpha*np.eye(M.shape[0]))

            JNormal,JTangent,penetrations = self.get_contact_jacobian(q[:,i],self.l)
            numContacts = JNormal.shape[0]

            tauContact = self.get_contact_torques(q[:,i],qd[:,i],Minv,bias_tau+tauControl,JNormal,JTangent,penetrations,dt)
            
            # tauKneeCaps = self.get_knee_cap_torques(q[:,i],qd[:,i],M,Minv,bias_tau,dt)
            
            tauTotal = bias_tau + tauContact + tauControl# + tauKneeCaps
            
            vel = deepcopy(np.reshape(qd[:,i],[9,1])) + Minv.dot(tauTotal)*dt
            vels[:,i] = vel.flatten()

        return vels
    
    

    def forward_simulate_dt(self,x,u,dt):
        x = deepcopy(x)
        u = deepcopy(u.clip(-self.tauMax,self.tauMax))

        b = .001
        x = x.reshape([18,-1])
        u = u.reshape([self.numInputs,-1])

        vels = self.get_vels(x[9:18,:],x[0:9,:],u,dt).reshape([9,-1])
        x[0:9,:] = vels
        x[9:18,:] = x[9:18,:] + vels*dt

        # x[2+9,:] = (x[2+9,:] + np.pi) % (2*np.pi) - np.pi
        # x[3+9,:] = (x[3+9,:] + np.pi) % (2*np.pi) - np.pi
        # x[4+9,:] = (x[4+9,:] + np.pi) % (2*np.pi) - np.pi
        # x[5+9,:] = (x[5+9,:] + np.pi) % (2*np.pi) - np.pi
        # x[6+9,:] = (x[6+9,:] + np.pi) % (2*np.pi) - np.pi
        return x

    def draw_link(self,l,CoM,theta):
        x = [CoM[0] - l*np.sin(theta),CoM[0]]
        y = [CoM[1] + l*np.cos(theta),CoM[1]]
        plt.plot(x,y)        

    def draw_dot(self,CoM,c='r'):
        plt.scatter(CoM[0],CoM[1],10,c=c)
        
    def visualize(self,x):
        x = np.array(x).flatten()
        q = x[9:18]
        rTorso = fkTorsoBot(q,self.l,self.lfoot)
        rThigh1 = fkThigh1Bot(q,self.l,self.lfoot)
        rCalf1 = fkCalf1Bot(q,self.l,self.lfoot)
        rFoot1 = fkToe1(q,self.l,self.lfoot)        
        rThigh2 = fkThigh2Bot(q,self.l,self.lfoot)
        rCalf2 = fkCalf2Bot(q,self.l,self.lfoot)
        rFoot2 = fkToe2(q,self.l,self.lfoot)
        rCOM = fkCOM(q,self.l,self.lfoot,self.mtorso,self.mthigh,self.mcalf,self.mfoot)
        # qfoot1 = self.get_q_in_foot_coordinates(q,1)
        # qfoot2 = self.get_q_in_foot_coordinates(q,2)
        # rCOMWRTFoot1 = fkFoot1(q,self.l,self.lfoot)+fkCOMWRTFoot(qfoot1,self.l,self.lfoot,self.mtorso,self.mthigh,self.mcalf,self.mfoot)
        # rCOMWRTFoot2 = fkFoot2(q,self.l,self.lfoot)+fkCOMWRTFoot(qfoot2,self.l,self.lfoot,self.mtorso,self.mthigh,self.mcalf,self.mfoot)
        
        plt.clf()
        plt.hlines(0,-15,15,linewidth=1.0,linestyles='dashed')
        self.draw_link(self.l,rTorso[0:2],rTorso[2])
        self.draw_link(self.l,rThigh1[0:2],rThigh1[2])
        self.draw_link(self.l,rCalf1[0:2],rCalf1[2])
        self.draw_link(self.lfoot,rFoot1[0:2],rFoot1[2]+np.pi/2)
        self.draw_link(self.l,rThigh2[0:2],rThigh2[2])
        self.draw_link(self.l,rCalf2[0:2],rCalf2[2])
        self.draw_link(self.lfoot,rFoot2[0:2],rFoot2[2]+np.pi/2)
        self.draw_dot(rCOM,'r')
        # self.draw_dot(rCOMWRTFoot1,c='b')
        # self.draw_dot(rCOMWRTFoot2,c='c')
        plt.ion()
        plt.axis(xmin=rCOM[0]-6,xmax=rCOM[0]+6,ymin=-5,ymax=5)
        plt.pause(.00000001)
        plt.show()
        # plt.pause(10)        

if __name__=='__main__':

    x = np.matrix([[0.0,0.0,0.0, 0.0,0.0,0.0, 0.0,0.0,0.0,
                    0.0,1.55,0.0, -0.2,0.2,0.0, .0,0.0,0.0]]).T
    # x = np.matrix([[0.0,0.0,0.0, 0.0,0.0,0.0, 0.0,0.0,0.0,
    #                 0.0,-1.0,np.pi, -0.0,0.0,0.0, .0,0.0,0.0]]).T
    
    
    from copy import deepcopy
    x0 = deepcopy(x)

    walker = KneedWalker()    
    dt = .02
    
    fig = plt.gca()
    plt.ion()
    plt.hlines(0,-15,15)

    u = np.array([0,0,0,0.0,0.0,0.0]).reshape(6,1)
    horizon = 5000
    w = .05
    A = .1
    for i in range(0,horizon):
        # print i

        hip1_des = 1.0*A*np.sin(w*i)
        knee1_des = -2*A*np.max([0,np.sin(w*i+np.pi/4)])*0
        hip2_des = -1.0*A*np.sin(w*i)
        knee2_des = -2*A*np.max([0,np.sin(w*i+np.pi)])*0
        ankle_des = 0.0

        # print "hip1_des: ",hip1_des
        # print "hip1: ",(x[3+9])
        # print "hip2_des: ",hip2_des
        # print "hip2: ",(x[6+9])
        # print "knee1_des: ",knee1_des
        # print "knee2_des: ",knee2_des        
        # u[0] = 400.0*(hip1_des-(x[3+9])) - 1.0*(x[3])
        # u[1] = 400.0*(knee1_des-(x[2+9]+x[3+9]+x[4+9])) - 1.0*(x[2]+x[3]+x[4])
        # u[2] = 400.0*(ankle_des-(x[2+9]+x[3+9]+x[4+9]+x[5+9])) - 1.0*(x[2]+x[3]+x[4]+x[5])
        # u[3] = 400.0*(hip2_des-(x[6+9])) - 1.0*(x[6])
        # u[4] = 400.0*(knee2_des-(x[2+9]+x[6+9]+x[7+9])) - 1.0*(x[2]+x[6]+x[7])
        # u[5] = 400.0*(ankle_des-(x[2+9]+x[6+9]+x[7+9]+x[8+9])) - 1.0*(x[2]+x[6]+x[7]+x[8])
        
        # u[0] = 200.0*(0-(x[3+9])) - 10.0*x[3]
        # u[1] = 200.0*(0-(x[4+9])) - 10.0*x[4]
        # u[2] = 200.0*(0-(x[5+9])) - 10.0*x[5]
        # u[3] = 200.0*(0-(x[6+9])) - 10.0*x[6]
        # u[4] = 200.0*(0-(x[7+9])) - 10.0*x[7]
        # u[5] = 200.0*(0-(x[8+9])) - 10.0*x[8]

        # print "x: ",x.T
        x = walker.forward_simulate_dt(x,u,dt)

        if i%3==0:
            walker.visualize(x)
