from rad_models.Model import Model
from baxter_right_dynamics import *
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy


class BaxterRight(Model):

    def __init__(self):
        
        self.numStates = 14
        self.numInputs = 7

        self.uMax = np.inf

    def calc_discrete_A_B_w(self,q,qd,dt):
        mat = calc_discrete_A_B_w(q,qd,dt)

        Ad = np.reshape(mat[0:self.numStates*self.numStates],[self.numStates,self.numStates],'F')
        Bd = np.reshape(mat[self.numStates*self.numStates:self.numStates*self.numStates+self.numStates*self.numInputs],[self.numStates,self.numInputs],'F')
        wd = np.reshape(mat[self.numStates*self.numStates+self.numStates*self.numInputs:],[self.numStates,1],'F')
        return Ad, Bd, wd
        
    def forward_simulate_dt(self,x,u,dt):
        n = self.numStates/2
        x = x.reshape([self.numStates,-1])
        qd = x[0:n,:]
        q = x[n:self.numStates,:]
        numSims = x.shape[1]
        x = deepcopy(x).reshape([self.numStates,-1])
        u = deepcopy(np.clip(u,-self.uMax,self.uMax)).reshape([self.numInputs,-1])

        for i in range(0,numSims):
            Ad,Bd,wd = self.calc_discrete_A_B_w(q[:,i],qd[:,i],dt)

            xnext = Ad.dot(x[:,i]) + Bd.dot(u[:,i]) + wd.reshape(14,)
            x[:,i] = xnext
        
        return x


if __name__=='__main__':
    
    sys = BaxterRight()    
    x = np.zeros([sys.numStates,1])
    x[3] = .001
    u = np.zeros([sys.numInputs,1])    
    x0 = deepcopy(x)
    
    dt = .01
    
    fig = plt.gca()
    plt.ion()

    horizon = 500
    Kp = 1.0
    Kd = 2.0

    x_hist = []
    for i in range(0,horizon):

        u = np.ones((7,1))*2
        # u[0] = Kp*(np.pi/3 - x[0+3]) - Kd*x[0]
        # u[1] = Kp*(np.pi/3 - x[1+3]) - Kd*x[1]
        # u[2] = Kp*(np.pi/3 - x[2+3]) - Kd*x[2]        
        x = sys.forward_simulate_dt(x,u,dt)
        x_hist.append(x)
        print(x[7:14].T)

        if i%5==0:
            pass
            # sys.visualize(x)

    x_hist = np.array(x_hist).squeeze()

    plt.ion()
    plt.plot(x_hist[:,7:14])
    plt.show()
    plt.pause(10000)
