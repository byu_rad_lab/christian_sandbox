#include "n2linkdynamics.h"

namespace py = pybind11;

py::array calc_M_py(std::vector<double> q,double l,double m,double I)
{
	 double out[4];

	double x0 = sin(q[0]);
	double x1 = pow(x0, 2);
	double x2 = cos(q[0]);
	double x3 = pow(x2, 2);
	double x4 = (1.0/2.0)*l;
	double x5 = cos(q[1]);
	double x6 = x2*x5;
	double x7 = sin(q[1]);
	double x8 = x0*x7;
	double x9 = x6 - x8;
	double x10 = x0*x5;
	double x11 = x2*x7;
	double x12 = -x10 - x11;
	double x13 = I*pow(pow(x12, 2) + pow(x9, 2), 2);
	double x14 = x10 + x11;
	double x15 = -x10*x4 - x11*x4;
	double x16 = -l*x0 + x15;
	double x17 = -x4*x6 + x4*x8;
	double x18 = -l*x2 + x17;
	double x19 = x14*x16 + x18*x9;
	double x20 = x12*x18 + x16*x9;
	double x21 = x14*x15 + x17*x9;
	double x22 = x12*x17 + x15*x9;
	double x23 = m*x19*x21 + m*x20*x22 + x13;
	 out[0] = I*pow(x1 + x3, 2) + m*pow(x19, 2) + m*pow(x20, 2) + m*pow(-x1*x4 - x3*x4, 2) + x13;
	 out[1] = x23;
	 out[2] = x23;
	 out[3] = m*pow(x21, 2) + m*pow(x22, 2) + x13;
	return py::array(4,out);

}

void calc_M(std::vector<double> q,Eigen::MatrixXd * M,double l,double m,double I)
{

	double x0 = sin(q[0]);
	double x1 = pow(x0, 2);
	double x2 = cos(q[0]);
	double x3 = pow(x2, 2);
	double x4 = (1.0/2.0)*l;
	double x5 = cos(q[1]);
	double x6 = x2*x5;
	double x7 = sin(q[1]);
	double x8 = x0*x7;
	double x9 = x6 - x8;
	double x10 = x0*x5;
	double x11 = x2*x7;
	double x12 = -x10 - x11;
	double x13 = I*pow(pow(x12, 2) + pow(x9, 2), 2);
	double x14 = x10 + x11;
	double x15 = -x10*x4 - x11*x4;
	double x16 = -l*x0 + x15;
	double x17 = -x4*x6 + x4*x8;
	double x18 = -l*x2 + x17;
	double x19 = x14*x16 + x18*x9;
	double x20 = x12*x18 + x16*x9;
	double x21 = x14*x15 + x17*x9;
	double x22 = x12*x17 + x15*x9;
	double x23 = m*x19*x21 + m*x20*x22 + x13;
	(*M)(0,0) = I*pow(x1 + x3, 2) + m*pow(x19, 2) + m*pow(x20, 2) + m*pow(-x1*x4 - x3*x4, 2) + x13;
	(*M)(0,1) = x23;
	(*M)(1,0) = x23;
	(*M)(1,1) = m*pow(x21, 2) + m*pow(x22, 2) + x13;
}

py::array calc_grav_py(std::vector<double> q,double l,double m,double g)
{
	 double out[2];

	double x0 = g*m;
	double x1 = l*sin(q[0]);
	double x2 = (1.0/2.0)*x1;
	double x3 = -1.0/2.0*l*sin(q[1])*cos(q[0]) - x2*cos(q[1]);
	 out[0] = -x0*x2 + x0*(-x1 + x3);
	 out[1] = x0*x3;
	return py::array(2,out);
}

void calc_grav(std::vector<double> q,Eigen::MatrixXd * grav,double l,double m,double g)
{

	double x0 = g*m;
	double x1 = l*sin(q[0]);
	double x2 = (1.0/2.0)*x1;
	double x3 = -1.0/2.0*l*sin(q[1])*cos(q[0]) - x2*cos(q[1]);
	(*grav)(0,0) = -x0*x2 + x0*(-x1 + x3);
	(*grav)(1,0) = x0*x3;
}

py::array calc_C_py(std::vector<double> q,std::vector<double> qd,double l,double m,double I)
{
	 double out[4];

	double x0 = cos(q[0]);
	double x1 = cos(q[1]);
	double x2 = x0*x1;
	double x3 = sin(q[0]);
	double x4 = sin(q[1]);
	double x5 = x3*x4;
	double x6 = x2 - x5;
	double x7 = x1*x3;
	double x8 = x0*x4;
	double x9 = -x7 - x8;
	double x10 = 0.5*I*(pow(x6, 2) + pow(x9, 2))*(2*x6*(-2*x7 - 2*x8) + 2*x9*(-2*x2 + 2*x5));
	double x11 = l*x3;
	double x12 = (1.0/2.0)*l;
	double x13 = x12*x7;
	double x14 = x12*x8;
	double x15 = -x13 - x14;
	double x16 = -x11 + x15;
	double x17 = x16*x6;
	double x18 = -x12*x2 + x12*x5;
	double x19 = -l*x0 + x18;
	double x20 = x19*x9;
	double x21 = 2*x17 + 2*x20;
	double x22 = x13 + x14;
	double x23 = x22*x6;
	double x24 = x7 + x8;
	double x25 = x18*x24;
	double x26 = 2*x23 + 2*x25;
	double x27 = x19*x6;
	double x28 = x16*x24 + x27;
	double x29 = 0.5*m;
	double x30 = x28*x29;
	double x31 = x30*(x21 + x26);
	double x32 = -x2 + x5;
	double x33 = x19*x32;
	double x34 = x16*x9;
	double x35 = 2*x33 + 2*x34;
	double x36 = x18*x6;
	double x37 = x22*x9;
	double x38 = 2*x36 + 2*x37;
	double x39 = x17 + x20;
	double x40 = x29*x39;
	double x41 = x40*(x35 + x38);
	double x42 = x10 + x31 + x41;
	double x43 = x11 + x22;
	double x44 = x43*x6;
	double x45 = x19*x24;
	double x46 = x43*x9;
	double x47 = x23 + x25;
	double x48 = x15*x24 + x36;
	double x49 = 1.0*m;
	double x50 = x48*x49;
	double x51 = x36 + x37;
	double x52 = x33 + x34;
	double x53 = x15*x6;
	double x54 = x18*x9;
	double x55 = x53 + x54;
	double x56 = x49*x55;
	double x57 = x29*x48*(x26 + 2*x53 + 2*x54);
	double x58 = x18*x32;
	double x59 = x15*x9;
	double x60 = x29*x55*(x38 + 2*x58 + 2*x59);
	double x61 = x10 + x28*x49*(x47 + x55) + x39*x49*(x51 + x58 + x59);
	double x62 = x10 + x57 + x60;
	double x63 = qd[1]*x62;
	 out[0] = qd[0]*(x10 + x30*(x21 + 2*x44 + 2*x45) + x40*(2*x27 + x35 + 2*x46)) + qd[1]*x42;
	 out[1] = qd[0]*x42 + qd[1]*(x50*(x39 + x47) + x56*(x51 + x52) - x57 - x60 + x61);
	 out[2] = qd[0]*(-x31 - x41 + x50*(x39 + x44 + x45) + x56*(x27 + x46 + x52) + x61) + x63;
	 out[3] = qd[0]*x62 + x63;
	return py::array(4,out);
}

void calc_C(std::vector<double> q,std::vector<double> qd,Eigen::MatrixXd * C,double l,double m,double I)
{

	double x0 = cos(q[0]);
	double x1 = cos(q[1]);
	double x2 = x0*x1;
	double x3 = sin(q[0]);
	double x4 = sin(q[1]);
	double x5 = x3*x4;
	double x6 = x2 - x5;
	double x7 = x1*x3;
	double x8 = x0*x4;
	double x9 = -x7 - x8;
	double x10 = 0.5*I*(pow(x6, 2) + pow(x9, 2))*(2*x6*(-2*x7 - 2*x8) + 2*x9*(-2*x2 + 2*x5));
	double x11 = l*x3;
	double x12 = (1.0/2.0)*l;
	double x13 = x12*x7;
	double x14 = x12*x8;
	double x15 = -x13 - x14;
	double x16 = -x11 + x15;
	double x17 = x16*x6;
	double x18 = -x12*x2 + x12*x5;
	double x19 = -l*x0 + x18;
	double x20 = x19*x9;
	double x21 = 2*x17 + 2*x20;
	double x22 = x13 + x14;
	double x23 = x22*x6;
	double x24 = x7 + x8;
	double x25 = x18*x24;
	double x26 = 2*x23 + 2*x25;
	double x27 = x19*x6;
	double x28 = x16*x24 + x27;
	double x29 = 0.5*m;
	double x30 = x28*x29;
	double x31 = x30*(x21 + x26);
	double x32 = -x2 + x5;
	double x33 = x19*x32;
	double x34 = x16*x9;
	double x35 = 2*x33 + 2*x34;
	double x36 = x18*x6;
	double x37 = x22*x9;
	double x38 = 2*x36 + 2*x37;
	double x39 = x17 + x20;
	double x40 = x29*x39;
	double x41 = x40*(x35 + x38);
	double x42 = x10 + x31 + x41;
	double x43 = x11 + x22;
	double x44 = x43*x6;
	double x45 = x19*x24;
	double x46 = x43*x9;
	double x47 = x23 + x25;
	double x48 = x15*x24 + x36;
	double x49 = 1.0*m;
	double x50 = x48*x49;
	double x51 = x36 + x37;
	double x52 = x33 + x34;
	double x53 = x15*x6;
	double x54 = x18*x9;
	double x55 = x53 + x54;
	double x56 = x49*x55;
	double x57 = x29*x48*(x26 + 2*x53 + 2*x54);
	double x58 = x18*x32;
	double x59 = x15*x9;
	double x60 = x29*x55*(x38 + 2*x58 + 2*x59);
	double x61 = x10 + x28*x49*(x47 + x55) + x39*x49*(x51 + x58 + x59);
	double x62 = x10 + x57 + x60;
	double x63 = qd[1]*x62;
	(*C)(0,0) = qd[0]*(x10 + x30*(x21 + 2*x44 + 2*x45) + x40*(2*x27 + x35 + 2*x46)) + qd[1]*x42;
	(*C)(0,1) = qd[0]*x42 + qd[1]*(x50*(x39 + x47) + x56*(x51 + x52) - x57 - x60 + x61);
	(*C)(1,0) = qd[0]*(-x31 - x41 + x50*(x39 + x44 + x45) + x56*(x27 + x46 + x52) + x61) + x63;
	(*C)(1,1) = qd[0]*x62 + x63;
}

py::array fkCoM0_py(std::vector<double> q,double l)
{
	 double out[16];

	double x0 = cos(q[0]);
	double x1 = sin(q[0]);
	double x2 = (1.0/2.0)*l;
	 out[0] = x0;
	 out[1] = -x1;
	 out[2] = 0;
	 out[3] = -x1*x2;
	 out[4] = x1;
	 out[5] = x0;
	 out[6] = 0;
	 out[7] = x0*x2;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = 0;
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 1;
	return py::array(16,out);

}

py::array fkCoM1_py(std::vector<double> q,double l)
{
	 double out[16];

	double x0 = cos(q[0]);
	double x1 = cos(q[1]);
	double x2 = x0*x1;
	double x3 = sin(q[0]);
	double x4 = sin(q[1]);
	double x5 = x3*x4;
	double x6 = x2 - x5;
	double x7 = x1*x3;
	double x8 = x0*x4;
	double x9 = (1.0/2.0)*l;
	 out[0] = x6;
	 out[1] = -x7 - x8;
	 out[2] = 0;
	 out[3] = -l*x3 - x7*x9 - x8*x9;
	 out[4] = x7 + x8;
	 out[5] = x6;
	 out[6] = 0;
	 out[7] = l*x0 + x2*x9 - x5*x9;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = 0;
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 1;
	return py::array(16,out);

}

void calc_A_B_w(std::vector<double> q,std::vector<double> qd,Eigen::MatrixXd * A, Eigen::MatrixXd * B, Eigen::MatrixXd * w,double l=1.0,double m=1.0,double I=.1,double gravity=9.81,double damping=.01)
{
  int n = 2;
  Eigen::MatrixXd M(n,n);
  Eigen::MatrixXd C(n,n);
  Eigen::MatrixXd F(n,n);
  F.setIdentity();
  F = F*damping;
  Eigen::MatrixXd g(n,1);

  calc_M(q,&M,l,m,I);
  calc_C(q,qd,&C,l,m,I);
  calc_grav(q,&g,l,m,gravity);

  Eigen::MatrixXd M_inv;
  M_inv = M.inverse();

  (*A).topLeftCorner(n,n) = -M_inv*(C+F);
  (*A).topRightCorner(n,n).setZero();
  (*A).bottomLeftCorner(n,n).setIdentity();
  (*A).bottomRightCorner(n,n).setZero();

  for(int i=0; i<M_inv.rows(); i++)
    {
      for(int j=0; j<M_inv.cols(); j++)
	{
	  (*B)(i,j) = M_inv(i,j);
	}
    }
  for(int i=n; i<2*n; i++)
    {
      for(int j=0; j<n; j++)
	{
	  (*B)(i,j) = 0.0;
	}
    }

  Eigen::MatrixXd b(n,1);
  b = M_inv*(-g);  
  for(int i=0; i<g.rows(); i++)
    {
      (*w)(i) = b(i);
    }
  for(int i=g.rows(); i<2*n; i++)
    {
      (*w)(i) = 0;
    }
  
}

void calc_discrete_A_B_w(std::vector<double> q,std::vector<double> qd, double dt, Eigen::MatrixXd * A_d, Eigen::MatrixXd * B_d, Eigen::MatrixXd * w_d,double l=1.0,double m=1.0,double I=.1,double gravity=9.81,double damping=.01)
{
  int n = 2;
  Eigen::MatrixXd A(2*n,2*n);
  Eigen::MatrixXd B(2*n,n);
  Eigen::MatrixXd w(2*n,1);
  calc_A_B_w(q,qd,&A,&B,&w,l,m,I,gravity,damping);
  
  Eigen::MatrixXd eye(A.rows(),A.cols());
  eye.setIdentity();
  *A_d = (eye - A*dt).inverse();
  *B_d = (eye - A*dt).inverse()*B*dt;
  *w_d = (eye - A*dt).inverse()*w*dt;  
}

py::array calc_discrete_A_B_w_py(std::vector<double> q,std::vector<double> qd, double dt,double l=1.0,double m=1.0,double I=.1,double gravity=9.81,double damping=.01)
{
  int n = 2;
  int num_states = n*2;
  int num_inputs = n;
  double out[num_states*num_states + num_states*num_inputs + num_states];
  
  Eigen::MatrixXd A(num_states,num_states);
  Eigen::MatrixXd B(num_states,num_inputs);
  Eigen::MatrixXd w(num_states,1);
  Eigen::MatrixXd A_d(num_states,num_states);
  Eigen::MatrixXd B_d(num_states,num_inputs);
  Eigen::MatrixXd w_d(num_states,1);

  calc_A_B_w(q,qd,&A,&B,&w,l,m,I,gravity,damping);
  
  Eigen::MatrixXd eye(A.rows(),A.cols());
  eye.setIdentity();
  A_d = (eye - A*dt).inverse();
  B_d = (eye - A*dt).inverse()*B*dt;

  w_d = (eye - A*dt).inverse()*w*dt;

  for(int i=0; i<num_states; i++)
    {
      for(int j=0; j<num_states; j++)
	{
	  out[i*num_states+j] = A_d(i,j);
	}
      for(int j=0; j<num_inputs; j++)
	{
	  out[num_states*num_states + i*num_inputs+j] = B_d(i,j);
	}
      out[num_states*num_states + num_states*num_inputs + i] = w_d(i);
    }
  return py::array(num_states*num_states + num_states*num_inputs + num_states,out);
}


PYBIND11_MODULE(n2linkdynamics,m) {
  m.def("calc_discrete_A_B_w_py",&calc_discrete_A_B_w_py,"a function description");
  m.def("calc_M",&calc_M_py,"a function description");
  m.def("calc_grav",&calc_grav_py,"another function description");
  m.def("calc_C",&calc_C_py,"yet another function description");
  m.def("fkCoM0",&fkCoM0_py,"a function description");
  m.def("fkCoM1",&fkCoM1_py,"a function description");
}
