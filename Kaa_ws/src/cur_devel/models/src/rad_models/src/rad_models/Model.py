import numpy as np
from copy import deepcopy
from scipy.linalg import expm

class Model():

    def __init__(self):
        self.numStates = None
        self.numInputs = None
        self.uMax = None
        self.xMax = None
        self.xMin = None

    def forward_simulate_dt(self,x,u,dt,method='euler'):
        """Returns x at the next time step. Default method is 1st order Euler."""

        x_dot = self.calc_state_derivs(x, u)
        x = deepcopy(x)
        
        if method=='euler':    
            x = x + x_dot*dt
        else:
            s = (method + ' is unimplemented. Using 1st Order Euler instead. "' + method +
                 '" sounds like a fun method, though! You should implement it! Consider looking at https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.ode.html')
            print(s)
            
        return x

    def calc_state_derivs(self,x,u):
        print("calc_state_derivs function not yet implemented. Please add one!")

    def visualize(x):
        print("visualize function not yet implemented. Please add one!")


    
    def calc_A_B_w(self,x,u,delta=1.0e-6, method='central'):
        """Calculates a numerical A, B, and w for the system defined by 'calc_state_derivs'. Default method is a central difference approximation. Can be overwritten if an analytical solution is desired."""
        x = deepcopy(x)
        u = deepcopy(u)        
        A = np.zeros([self.numStates,self.numStates])
        B = np.zeros([self.numStates,self.numInputs])

        xdot0 = self.calc_state_derivs(x,u) 
        xup = deepcopy(x)
        uup = deepcopy(u)        
        xdown = deepcopy(x)
        udown = deepcopy(u)        

        for i in range(0,self.numStates):
            xup[i] = x[i] + delta
            xdown[i] = x[i] - delta            
            xdotup = self.calc_state_derivs(xup,u).flatten()
            xdotdown = self.calc_state_derivs(xdown,u).flatten()
            A[:,i] = (xdotup-xdotdown)/(2.0*delta)
            xup[i] = x[i]
            xdown[i] = x[i]

        for i in range(0,self.numInputs):
            uup[i] = u[i] + delta
            udown[i] = u[i] - delta            
            xdotup = self.calc_state_derivs(x,uup).flatten()
            xdotdown = self.calc_state_derivs(x,udown).flatten()      
            B[:,i] = (xdotup-xdotdown)/(2.0*delta)
            uup[i] = u[i]
            udown[i] = u[i]

            # print(xdot0)
            # print(A.dot(x))
            # print(B.dot(u))

        w = xdot0 - A.dot(x) - B.dot(u)
            
        return A,B,w

    def calc_A_B(self,x,u,dt, delta=1.0e-6):
        """Old method - use "calc_A_B_w" instead"""
        A, B, _ = self.calc_A_B_w(x, u, delta=delta)
        return A,B

    
    def discretize_A_and_B(self,A,B,dt):
        """Discretizes a given A and B matrix. Attempts matrix exponentiation, but will perform Euler integration if exponentiation fails."""
        try:
            Ad = expm(A*dt)            
            Bd = np.matmul(np.linalg.inv(A),np.matmul(Ad-np.eye(Ad.shape[0]),B))
        except:
            # print("Failed to do matrix exponentiation for discretization...")
            Ad = np.eye(A.shape[0]) + A*dt
            Bd = B*dt
        return Ad,Bd
    
    def calc_discrete_A_B_w(self,x,u,dt):
        """Calculates a dicrete A, B and w for a given state and input."""
        x = deepcopy(x)
        u = deepcopy(u)
        x = x.reshape(self.numStates,-1)
        A, B, w = self.calc_A_B_w(x, u)

        # print(A)
        # print(B)

        [Ad,Bd] = self.discretize_A_and_B(A,B,dt)
        wd = w*dt
        
        return Ad,Bd,wd
