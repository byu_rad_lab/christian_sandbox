from rad_models.Model import Model
from kneedwalkerdynamics import *
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy

class PDKneedWalker(Model):

    def __init__(self,mtorso=7.0,Itorso=1.0,mthigh=5.0,Ithigh=.9,mcalf=4.0,Icalf=0.8,l=1.0,g=9.81):
        
        self.numStates = 14
        self.numInputs = 4

        self.Kp = 500.0
        self.Kd = 20.0

        self.uMax = np.pi
        self.tauMax = 5000.0
        
        self.mtorso = mtorso
        self.Itorso = Itorso
        self.mthigh = mthigh
        self.Ithigh = Ithigh
        self.mcalf = mcalf
        self.Icalf = Icalf
        self.l = l
        self.g = g
        self.e_restitution = 0.0
        self.muStatic = .8
        self.muDynamic = .01

    def get_contact_and_friction_torques(self,fk,jac,q,qd,l,tau,Minv,dt):
        if(fk(q,l)[1]<=0):
            v_desired = fk(q,l)[1]/dt
            J = np.reshape(np.reshape(jac(q,l),[3,7])[1,:],[1,7])
            contactMassInv = 1.0/(J.dot(Minv).dot(J.T))
            F = contactMassInv*(-(1.0+self.e_restitution)*(v_desired+J.dot(qd))/dt - J.dot(Minv).dot(tau))
            F = np.matrix(np.max([0.0,F]))
            tauContact = J.T.dot(F)
            
            J = np.reshape(np.reshape(jac(q,l),[3,7])[0,:],[1,7])
            frictionMassInv = 1.0/(J.dot(Minv).dot(J.T))        
            Ftangential = frictionMassInv*(-J.dot(qd)/dt - J.dot(Minv).dot(tau))

            if(np.abs(Ftangential)>np.abs(self.muStatic*F)):
                Ffriction = -np.sign(J.dot(qd))*self.muDynamic*F
            else:        
                Ffriction = Ftangential
            tauFriction = J.T.dot(Ffriction)

            # return tauContact+tauFriction
            # This should not be multiplied by 0.5
            # We should likely use a contact jacobian and calculate
            # tauContact using that and all desired velocities...
            return 0.5*tauContact+tauFriction        
        else:
            return np.zeros([7,1])

    def get_accel(self,q,qd,tauControl,dt):

        # Find the number of trajectories and simulate in parallel...
        numSims = q.shape[1]
        accels = np.zeros([7,numSims])
        for i in range(0,numSims):
        
            M = calc_M(q[:,i],self.l,self.mtorso,self.mthigh,self.mcalf,self.Itorso,self.Ithigh,self.Icalf).squeeze().reshape([7,7])
            grav = calc_grav(q[:,i],self.l,self.mtorso,self.mthigh,self.mcalf,self.g).squeeze()
            c = calc_c(q[:,i],qd[:,i],self.l,self.mtorso,self.mthigh,self.mcalf).squeeze()
            Minv = np.linalg.inv(M)

            tau = -grav - c
            
            tauContact = np.zeros([7,1])
            # tauContact += self.get_contact_and_friction_torques(fkhead,jachead,q[:,i],qd[:,i],self.l,tau,Minv,dt)
            # tauContact += self.get_contact_and_friction_torques(fk2,jac2,q[:,i],qd[:,i],self.l,tau,Minv,dt)
            # tauContact += self.get_contact_and_friction_torques(fk3,jac3,q[:,i],qd[:,i],self.l,tau,Minv,dt)
            tauContact += self.get_contact_and_friction_torques(fk4,jac4,q[:,i],qd[:,i],self.l,tau,Minv,dt)
            # tauContact += self.get_contact_and_friction_torques(fk5,jac5,q[:,i],qd[:,i],self.l,tau,Minv,dt)
            tauContact += self.get_contact_and_friction_torques(fk6,jac6,q[:,i],qd[:,i],self.l,tau,Minv,dt)
            tauTotal = tau + tauContact.flatten() + np.hstack([np.zeros(3),tauControl[:,i]]).T

            accels[:,i] = np.linalg.inv(M).dot(tauTotal)

        return accels
    
    

    def forward_simulate_dt(self,x,u,dt):
        x = deepcopy(x)
        u = deepcopy(u.clip(-self.uMax,self.uMax))
        u = deepcopy(u)
        
        b = .001
        x = x.reshape([self.numStates,-1])
        u = u.reshape([self.numInputs,-1])
        mydt = .01
        times = int(dt/mydt)
        tau = np.zeros(u.shape)
        for i in range(0,times):
            tau[0,:] = self.Kp*(u[0,:]-x[3+7,:]) - self.Kd*x[3,:]
            tau[1,:] = self.Kp*(u[1,:]-x[4+7,:]) - self.Kd*x[4,:]
            tau[2,:] = self.Kp*(u[2,:]-x[5+7,:]) - self.Kd*x[5,:]
            tau[3,:] = self.Kp*(u[3,:]-x[6+7,:]) - self.Kd*x[6,:]

            tau = tau.clip(-self.tauMax,self.tauMax)
            
            accels = self.get_accel(x[7:14,:],x[0:7,:],tau,mydt).reshape([7,-1])
            x[0:7,:] = (1.0-b)*x[0:7,:] + accels*mydt
            x[7:14,:] = x[7:14,:] + x[0:7,:]*mydt

        # x[2+7,:] = (x[2+7,:] + np.pi) % (2*np.pi) - np.pi
        # x[3+7,:] = (x[3+7,:] + np.pi) % (2*np.pi) - np.pi
        # x[4+7,:] = (x[4+7,:] + np.pi) % (2*np.pi) - np.pi
        # x[5+7,:] = (x[5+7,:] + np.pi) % (2*np.pi) - np.pi
        # x[6+7,:] = (x[6+7,:] + np.pi) % (2*np.pi) - np.pi
        return x

    def draw_link(self,l,CoM,theta):
        x = [CoM[0] + l/2.0*np.sin(theta),CoM[0] - l/2.0*np.sin(theta)]
        y = [CoM[1] - l/2.0*np.cos(theta),CoM[1] + l/2.0*np.cos(theta)]

        plt.plot(x,y)        

    def visualize(self,x):
        x = np.array(x).flatten()
        q = x[7:14]
        rA = [q[0],
              q[1]]
        
        rB = [q[0] + self.l/2.*(np.sin(q[2])+np.sin(q[2]+q[3])),
              q[1] - self.l/2.*(np.cos(q[2])+np.cos(q[2]+q[3]))]
        
        rC = [q[0] + self.l/2.*(np.sin(q[2])+np.sin(q[2]+q[3]+q[4])) + self.l*np.sin(q[2]+q[3]),
              q[1] - self.l/2.*(np.cos(q[2])+np.cos(q[2]+q[3]+q[4])) - self.l*np.cos(q[2]+q[3])]
        
        rD = [q[0] + self.l/2.*(np.sin(q[2])+np.sin(q[2]+q[5])),
              q[1] - self.l/2.*(np.cos(q[2])+np.cos(q[2]+q[5]))]
        
        rE = [q[0] + self.l/2.*(np.sin(q[2])+np.sin(q[2]+q[5]+q[6])) + self.l*np.sin(q[2]+q[5]),
              q[1] - self.l/2.*(np.cos(q[2])+np.cos(q[2]+q[5]+q[6])) - self.l*np.cos(q[2]+q[5])]
        
        plt.clf()
        plt.hlines(0,-15,15,linewidth=1.0)
        self.draw_link(self.l,rA,q[2])
        self.draw_link(self.l,rB,q[2]+q[3])
        self.draw_link(self.l,rC,q[2]+q[3]+q[4])    
        self.draw_link(self.l,rD,q[2]+q[5])
        self.draw_link(self.l,rE,q[2]+q[5]+q[6])
        plt.ion()
        plt.axis(xmin=-12,xmax=12,ymin=-10,ymax=10)
        # plt.axis(aspect='equal')
        plt.pause(.00000001)
        
        plt.show()
        

if __name__=='__main__':
    q = np.array([0,2.6,0.0,0.15,0,-0.15,0]).reshape(7,-1)
    qd = np.array([0.0,0,0,0,0,0,0.0]).reshape(7,-1)
    x = np.vstack([qd,q]).reshape(14,1)

    theta = np.pi/24
    # x = np.matrix([[1.0,0.0,0,0,0,0,0,0,1.5+np.cos(0),0,theta,-theta*0,0*-theta,0*theta]]).T
    x = np.matrix([[1.0,0.0,0,0,0,0,0,0,2.5,0,theta,0,0,0]]).T
    from copy import deepcopy
    x0 = deepcopy(x)

    walker = PDKneedWalker()
    
    dt = .05
    
    fig = plt.gca()
    plt.ion()
    plt.hlines(0,-15,15)

    u = np.array([10.0,10.0,-10.-10,0]).reshape(4,1)
    horizon = 40
    uSave = np.zeros([4,horizon])
    w = 0.1
    horizon = int(np.round(np.pi/w))
    print(horizon)
    for i in range(0,horizon):
        
        u[0] = x0[3+7] + 0.25*np.cos(i*w)
        u[1] = x0[4+7] - 0.5*np.sin(i*w)
        u[2] = x0[5+7] - 0.25*np.cos(i*w)
        u[3] = x0[6+7] + 0.5*np.sin(i*w)

        print(x[0])

        uSave[:,i] = u.flatten()

        x = walker.forward_simulate_dt(x,u,dt)

        if i%1==0:
            walker.visualize(x)

        

    import scipy.io as sio
    data_dict = {'U':uSave}
    print(uSave.shape)
    sio.savemat('goodU',data_dict)
