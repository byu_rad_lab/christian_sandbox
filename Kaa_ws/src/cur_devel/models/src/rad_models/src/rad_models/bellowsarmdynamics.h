#include <math.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

py::array calc_grav(std::vector<double> q, double g);

py::array calc_M(std::vector<double> q);

py::array fkJt0COM(std::vector<double> q);
py::array fkJt0End(std::vector<double> q);
py::array fkL0End(std::vector<double> q);
py::array fkJt1COM(std::vector<double> q);
py::array fkJt1End(std::vector<double> q);
py::array fkL1End(std::vector<double> q);
py::array fkJt2COM(std::vector<double> q);
py::array fkJt2End(std::vector<double> q);
