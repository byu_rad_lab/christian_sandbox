#include "kinematics/arm.h"
#include "kinematics/joint_ccc.h"

int main()
{
  //kinematics::Arm arm;
  /*

  // First Joint
  double height = .212;
  kinematics::JointCCC joint1(height);
  kinematics::Link link1(joint1);

  Eigen::Matrix3d rot1;
  rot1<<.8536, .1464, -.5,
    .1464, .8536, .5,
    .5, -.5, .7071;
  
  Eigen::Vector3d vec1;
  vec1<<-.0148,.0148,.293;
  
  Hom hom1 = kinematics::HomFromRotVec(rot1,vec1)
  link1.g_top_end_ = hom1;

  double m1=6.534;
  Eigen::Vector3d I1;
  I1<<.108, .108, .023;
  Eigen::Vector3d pos1;
  pos1<<0.0,0.0,.115;
  
  link1.setInertiaSelf(m1,pos1,I1);

  arm.addLink(link1);

  // Second Joint
  kinematics::JointCCC joint2(height);
  kinematics::Link link2(joint2);

  Eigen::Matrix3d rot2;
  rot1<<.8536, .1464, -.5,
    .1464, .8536, .5,
    .5, -.5, .7071;
  
  Eigen::Vector3d vec2;
  vec1<<-.0148,.0148,.293;
  
  Hom hom2 = kinematics::HomFromRotVec(rot2,vec2)
  link2.g_top_end_ = hom2;

  double m2=6.534;
  Eigen::Vector3d I2;
  I2<<.108, .108, .023;
  Eigen::Vector3d pos2;
  pos2<<0.0,0.0,.115;
  
  link2.setInertiaSelf(m2,pos2,I2);

  arm.addLink(link2);  

  // Third Joint
  double height = .212;
  kinematics::JointCCC joint3(height);
  kinematics::Link link3(joint3);

  Eigen::Matrix3d rot3;
  rot3<<1.0,0,0,
    0,1,0,
    0,0,1;
  
  Eigen::Vector3d vec3;
  vec3<<0,0,.025;
  
  Hom hom3 = kinematics::HomFromRotVec(rot3,vec3)
  link3.g_top_end_ = hom3;

  double m3 = 1.5;
  Eigen::Vector3d I3;
  I3<<.05,.05,.023;
  Eigen::Vector3d pos3;
  pos3<<0.0,0.0,.0125;
  
  link3.setInertiaSelf(m3,pos3,I3);

  arm.addLink(link3);
  */
  return 0;
}
