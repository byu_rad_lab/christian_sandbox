#include "threelinkdynamics.h"
#include <math.h>

namespace py = pybind11;

py::array calc_M(std::vector<double> q,double l,double m,double I)
{
	 double out[9];

	 out[0] = 3.0*I + 3.0*pow(l, 2)*m*cos(q[1]) + 1.0*pow(l, 2)*m*cos(q[2]) + 1.0*pow(l, 2)*m*cos(q[1] + q[2]) + 3.75*pow(l, 2)*m;
	 out[1] = 2.0*I + 1.5*pow(l, 2)*m*cos(q[1]) + 1.0*pow(l, 2)*m*cos(q[2]) + 0.5*pow(l, 2)*m*cos(q[1] + q[2]) + 1.5*pow(l, 2)*m;
	 out[2] = 1.0*I + 0.5*pow(l, 2)*m*cos(q[2]) + 0.5*pow(l, 2)*m*cos(q[1] + q[2]) + 0.25*pow(l, 2)*m;
	 out[3] = 2.0*I + 1.5*pow(l, 2)*m*cos(q[1]) + 1.0*pow(l, 2)*m*cos(q[2]) + 0.5*pow(l, 2)*m*cos(q[1] + q[2]) + 1.5*pow(l, 2)*m;
	 out[4] = 2.0*I + 1.0*pow(l, 2)*m*cos(q[2]) + 1.5*pow(l, 2)*m;
	 out[5] = 1.0*I + 0.5*pow(l, 2)*m*cos(q[2]) + 0.25*pow(l, 2)*m;
	 out[6] = 1.0*I + 0.5*pow(l, 2)*m*cos(q[2]) + 0.5*pow(l, 2)*m*cos(q[1] + q[2]) + 0.25*pow(l, 2)*m;
	 out[7] = 1.0*I + 0.5*pow(l, 2)*m*cos(q[2]) + 0.25*pow(l, 2)*m;
	 out[8] = 1.0*I + 0.25*pow(l, 2)*m;
	return py::array(9,out);

}

py::array calc_grav(std::vector<double> q,double l,double m,double g)
{
	 double out[3];

	 out[0] = -1.0/2.0*g*l*m*(5*sin(q[0]) + 3*sin(q[0] + q[1]) + sin(q[0] + q[1] + q[2]));
	 out[1] = -1.0/2.0*g*l*m*(3*sin(q[0] + q[1]) + sin(q[0] + q[1] + q[2]));
	 out[2] = -1.0/2.0*g*l*m*sin(q[0] + q[1] + q[2]);
	return py::array(3,out);

}

py::array calc_c(std::vector<double> q,std::vector<double> qd,double l,double m)
{
	 double out[3];

	 out[0] = -pow(l, 2)*m*(3.0*qd[0]*qd[1]*sin(q[1]) + 1.0*qd[0]*qd[1]*sin(q[1] + q[2]) + 1.0*qd[0]*qd[2]*sin(q[2]) + 1.0*qd[0]*qd[2]*sin(q[1] + q[2]) + 1.5*pow(qd[1], 2)*sin(q[1]) + 0.5*pow(qd[1], 2)*sin(q[1] + q[2]) + 1.0*qd[1]*qd[2]*sin(q[2]) + 1.0*qd[1]*qd[2]*sin(q[1] + q[2]) + 0.5*pow(qd[2], 2)*sin(q[2]) + 0.5*pow(qd[2], 2)*sin(q[1] + q[2]));
	 out[1] = pow(l, 2)*m*(1.5*pow(qd[0], 2)*sin(q[1]) + 0.5*pow(qd[0], 2)*sin(q[1] + q[2]) - 1.0*qd[0]*qd[2]*sin(q[2]) - 1.0*qd[1]*qd[2]*sin(q[2]) - 0.5*pow(qd[2], 2)*sin(q[2]));
	 out[2] = pow(l, 2)*m*(0.5*pow(qd[0], 2)*sin(q[2]) + 0.5*pow(qd[0], 2)*sin(q[1] + q[2]) + 1.0*qd[0]*qd[1]*sin(q[2]) + 0.5*pow(qd[1], 2)*sin(q[2]));
	return py::array(3,out);

}

py::array fk0(std::vector<double> q,double l)
{
	 double out[3];

	 out[0] = -1.0/2.0*l*sin(q[0]);
	 out[1] = (1.0/2.0)*l*cos(q[0]);
	 out[2] = q[0];
	return py::array(3,out);

}

py::array fk1(std::vector<double> q,double l)
{
	 double out[3];

	 out[0] = -l*sin(q[0]) - 1.0/2.0*l*sin(q[0] + q[1]);
	 out[1] = l*cos(q[0]) + (1.0/2.0)*l*cos(q[0] + q[1]);
	 out[2] = q[0] + q[1];
	return py::array(3,out);

}

py::array fk2(std::vector<double> q,double l)
{
	 double out[3];

	 out[0] = -l*sin(q[0]) - l*sin(q[0] + q[1]) - 1.0/2.0*l*sin(q[0] + q[1] + q[2]);
	 out[1] = l*cos(q[0]) + l*cos(q[0] + q[1]) + (1.0/2.0)*l*cos(q[0] + q[1] + q[2]);
	 out[2] = q[0] + q[1] + q[2];
	return py::array(3,out);

}

py::array jac0(std::vector<double> q,double l)
{
	 double out[9];

	 out[0] = -1.0/2.0*l*cos(q[0]);
	 out[1] = 0;
	 out[2] = 0;
	 out[3] = -1.0/2.0*l*sin(q[0]);
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = 1;
	 out[7] = 0;
	 out[8] = 0;
	return py::array(9,out);

}

py::array jac1(std::vector<double> q,double l)
{
	 double out[9];

	 out[0] = -l*cos(q[0]) - 1.0/2.0*l*cos(q[0] + q[1]);
	 out[1] = -1.0/2.0*l*cos(q[0] + q[1]);
	 out[2] = 0;
	 out[3] = -l*sin(q[0]) - 1.0/2.0*l*sin(q[0] + q[1]);
	 out[4] = -1.0/2.0*l*sin(q[0] + q[1]);
	 out[5] = 0;
	 out[6] = 1;
	 out[7] = 1;
	 out[8] = 0;
	return py::array(9,out);

}

py::array jac2(std::vector<double> q,double l)
{
	 double out[9];

	 out[0] = -l*cos(q[0]) - l*cos(q[0] + q[1]) - 1.0/2.0*l*cos(q[0] + q[1] + q[2]);
	 out[1] = -l*cos(q[0] + q[1]) - 1.0/2.0*l*cos(q[0] + q[1] + q[2]);
	 out[2] = -1.0/2.0*l*cos(q[0] + q[1] + q[2]);
	 out[3] = -l*sin(q[0]) - l*sin(q[0] + q[1]) - 1.0/2.0*l*sin(q[0] + q[1] + q[2]);
	 out[4] = -l*sin(q[0] + q[1]) - 1.0/2.0*l*sin(q[0] + q[1] + q[2]);
	 out[5] = -1.0/2.0*l*sin(q[0] + q[1] + q[2]);
	 out[6] = 1;
	 out[7] = 1;
	 out[8] = 1;
	return py::array(9,out);

}

PYBIND11_MODULE(threelinkdynamics,m) {
  m.def("calc_M",&calc_M,"a function description");
  m.def("calc_grav",&calc_grav,"another function description");
  m.def("calc_c",&calc_c,"yet another function description");
  m.def("fk0",&fk0,"a function description");
  m.def("fk1",&fk1,"a function description");
  m.def("fk2",&fk2,"a function description");
  m.def("jac0",&jac0,"a function description");
  m.def("jac1",&jac1,"a function description");
  m.def("jac2",&jac2,"a function description");
}
