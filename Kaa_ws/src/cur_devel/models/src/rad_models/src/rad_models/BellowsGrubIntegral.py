from rad_models.Model import Model
#from bellowsgrubintegraldynamics import *
from bellows_grub_dynamics import * # this is the fixed version
import numpy as np
from copy import deepcopy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

np.set_printoptions(precision=2)

class BellowsGrub(Model):

    def __init__(self):
        self.numStates = 8
        self.numInputs = 4

        self.K_passive_damper = np.diag([7.5,7.5])*1.0
        self.K_passive_spring = np.diag([30,30.0])*1.0
        
        self.prs_to_torque = np.array([[.15,-.15,0,0],
                                       [0,0,.15,-.15]])*1.0
        self.uMax = 400.0
        self.uMin = 8.0

        self.h = .21
        self.m = 2.0 #1.923 measured        
        self.r = .16
        self.alphas = np.eye(4)*20.0
        self.M_hist = np.zeros([4,1])
        

    def calc_grub_regressor(self,q,qd,qdr,qddr):
        q[np.abs(q)<.001] = np.sign(q[abs(q)<.001])*.001
        q[q==0] = .001
        regressor = calc_regressor(q,qd,qdr,qddr,self.h,self.m,self.r,-9.81).reshape(-1,2).T
        return regressor
        
    def calc_continuous_A_B_w(self,x,fill=np.ones([4,1])):

        p = x[0:4]
        qd = x[4:6]
        q = x[6:8].flatten()
        q[np.abs(q)<.001] = np.sign(q[abs(q)<.001])*.001
        q[q==0] = .001
        q = q.tolist()

        M = calc_M(q,self.h,self.m,self.r).reshape(2,2)
        self.M_hist = np.append(self.M_hist, M)
        M_inv = np.linalg.inv(M)
        if np.linalg.cond(M) > 10000:
            print("Near singular Mass matrix...")
            print("q: ",q)
            print("M: ",M)

        C = calc_C(q,qd,self.h,self.m,self.r).reshape(2,2)
        
        A11 = -self.alphas
        A12 = np.zeros([4,2])
        A13 = np.zeros([4,2])
        A21 = np.matmul(M_inv,self.prs_to_torque)
        A22 = np.matmul(M_inv,-C-self.K_passive_damper)        
        A23 = np.matmul(M_inv,-self.K_passive_spring)        
        A31 = np.zeros([2,4])        
        A32 = np.eye(2)        
        A33 = np.zeros([2,2])

        A = np.vstack([np.hstack([A11,A12,A13]),np.hstack([A21,A22,A23]),np.hstack([A31,A32,A33])])
        
        B11 = self.alphas
        B21 = np.zeros([2,4])
        B31 = np.zeros([2,4])
        B = np.vstack([B11,B21,B31])

        gravity_torque = calc_grav(q,self.h,self.m,-9.81)

        w = np.matmul(M_inv,gravity_torque)
        w = np.vstack([np.zeros((4,1)),np.reshape(w,(2,1)),np.zeros((2,1))])
        
        return A,B,w
        
        
    def calc_discrete_A_B_w(self,x,dt=.01,fill=np.ones([2,1])):
        
        [A,B,w] = self.calc_continuous_A_B_w(x,fill)

        # Matrix exponentiation
        [Ad,Bd] = self.discretize_A_and_B(A,B,dt)

        # First order implicit integration
        # Ad = np.linalg.inv(np.eye(self.numStates) - A*dt)
        # Bd = np.matmul(Ad,B)*dt

        # First order explicit integration        
        # Ad = np.eye(self.numStates) + A*dt
        # Bd = B*dt

        wd = np.linalg.inv(A).dot(Ad-np.eye(Ad.shape[0])).dot(w)
        
        return Ad, Bd, wd

        
    def forward_simulate_dt(self,x,u,dt = 0.01,fill=np.ones([2,1])):
        batch_size = x.shape[1]
        x_k_plus_one = np.zeros(x.shape)

        for i in range(0,batch_size):
            [Ad,Bd,wd] = self.calc_discrete_A_B_w(x[:,i],dt)
            x_k_plus_one[:,i] = np.matmul(Ad,x[:,i]).flatten() + np.matmul(Bd,u[:,i]).flatten() + wd.flatten()

        return x_k_plus_one

    def visualize(self,x,ax):
        ax.clear()
        q = x[6:8]
        num_segments = 20
        last_pos = np.zeros([3,1])
        for i in range(0,num_segments):
            scalar = float(i+1)/num_segments
            this_pos = fkEnd(q*scalar,self.h*scalar,self.h*scalar).reshape(4,4)[0:3,3]
            self.plot_line(ax, last_pos,this_pos,'orange',self.r*50)
            last_pos = this_pos


        self.set_axes_equal(ax)
        plt.ion()
        plt.show()
        plt.pause(.000001)

    def plot_line(self,ax, a, b, color, linewidth_custom, alpha=1.0):  # Ax must be projection 3d
        a = a.flatten()
        ax.plot([a[0], b[0]], [a[1], b[1]], [a[2], b[2]], color=color, linewidth=linewidth_custom, alpha=alpha)

    def set_axes_equal(self,ax):

        ax.set_xlim3d([-self.h,self.h])
        ax.set_ylim3d([-self.h,self.h])
        ax.set_zlim3d([0,self.h])
    
    

if __name__=='__main__':
    import time
    sys = BellowsGrub()

    q = np.ones((2,1))*.001
    qd = np.zeros((2,1))
    p = np.array([[10],
                  [10],
                  [10],
                  [10]])
    
    x = np.vstack([p,qd,q])
    u = deepcopy(p*1)
    x0 = deepcopy(x)
    
    dt = .01
    
    ax = plt.gca(projection='3d')
    plt.ion()

    horizon = 100
    a1 = 500
    a2 = 50

    x_hist = x
    
    for i in range(0,horizon):
        u[0] = a1 + np.sin(i*.2)*a1/2.
        u[1] = a1 - np.sin(i*.2)*a1/2.
        u[2] = a2
        u[3] = a2
        chamber_num = 3

        # 0 and 2 move bellows, u = v = .707
        # 0 and 1 don't do anything
        # 0 and 3 move bellows, u = .707, v = -.707
        # 2 and 3 don't do anything
        
         
        
        print(i)
        x = sys.forward_simulate_dt(x,u,dt)
        if i%1==0:
            print(x)
            sys.visualize(x,ax)
            x_hist = np.append(x_hist, x, axis=1)

    u_hist = x_hist[4,:]
    v_hist = x_hist[5,:]

    plt.figure('Ud and Vd history')
    plt.plot(u_hist)
    plt.plot(v_hist)
    plt.xlabel('t')
    plt.ylabel('Angle (rads)')
    plt.legend(['u','v'])
    plt.show()
    
