from rad_models.Model import Model
#from bellowsgrubintegraldynamics import *
from bellows_grub_dynamics import * #fixed dynamics
import numpy as np
from copy import deepcopy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

np.set_printoptions(precision=2)

class BellowsGrub(Model):

    def __init__(self,spring_offset=0.0):
        self.spring_offset = spring_offset
        self.numStates = 4
        self.numInputs = 4

        self.K_passive_damper = np.diag([5.0,5.0])*1.0
        self.K_passive_spring = np.diag([10,10.0])*2.0

        self.prs_to_torque = np.array([[.1,-.1,0,0],
                                       [0,0,.1,-.1]])*1.0
        self.uMax = 400.0
        self.uMin = 8.0

        self.h = .21
        self.m = 2.0 #1.923 measured        
        self.r = .15
        



    def calc_grub_regressor(self,q,qd,qdr,qddr):
        q[np.abs(q)<.001] = np.sign(q[abs(q)<.001])*.001
        q[q==0] = .001
        regressor = calc_regressor(q,qd,qdr,qddr,self.h,self.m,self.r,-9.81).reshape(-1,2).T
        return regressor
        
    def calc_continuous_A_B_w(self,x,fill=np.ones([4,1])):

        qd = x[0:2]
        q = x[2:4].flatten()
        q[np.abs(q)<.001] = np.sign(q[abs(q)<.001])*.001
        q[q==0] = .001
        q = q.tolist()

        M = calc_M(q,self.h,self.m,self.r).reshape(2,2)
        M_inv = np.linalg.inv(M)
        if np.linalg.cond(M) > 10000:
            print("Near singular Mass matrix...")
            print("q: ",q)
            print("M: ",M)

        C = calc_C(q,qd,self.h,self.m,self.r).reshape(2,2)
        
        
        A11 = np.matmul(M_inv,-C-self.K_passive_damper)
        A12 = np.matmul(M_inv,-self.K_passive_spring)
            
        A21 = np.eye(2)        
        A22 = np.zeros([2,2])

        A = np.vstack([np.hstack([A11,A12]),np.hstack([A21,A22])])

        B11 = np.matmul(M_inv,self.prs_to_torque)
        B21 = np.zeros([2,4])
        B = np.vstack([B11,B21])

        gravity_torque = np.array(calc_grav(q,self.h,self.m,-9.81))
        
        offset_spring_torque = np.matmul(self.K_passive_spring,np.ones([2,]))*self.spring_offset

        w = np.matmul(M_inv,gravity_torque+offset_spring_torque)
        w = np.vstack([np.reshape(w,(2,1)),np.zeros((2,1))])
        
        return A,B,w
        
        
    def calc_discrete_A_B_w(self,x,dt=.01,fill=np.ones([2,1])):
        
        [A,B,w] = self.calc_continuous_A_B_w(x,fill)

        # Matrix exponentiation
        [Ad,Bd] = self.discretize_A_and_B(A,B,dt)

        # First order implicit integration
        # Ad = np.linalg.inv(np.eye(self.numStates) - A*dt)
        # Bd = np.matmul(Ad,B)*dt

        # First order explicit integration        
        # Ad = np.eye(self.numStates) + A*dt
        # Bd = B*dt

        wd = np.linalg.inv(A).dot(Ad-np.eye(Ad.shape[0])).dot(w)
            
        return Ad, Bd, wd

        
    def forward_simulate_dt(self,x,u,dt = 0.01,fill=np.ones([2,1])):
        batch_size = x.shape[1]
        x_k_plus_one = np.zeros(x.shape)

        for i in range(0,batch_size):
            [Ad,Bd,wd] = self.calc_discrete_A_B_w(x[:,i],dt)
            x_k_plus_one[:,i] = np.matmul(Ad,x[:,i]).flatten() + np.matmul(Bd,u[:,i]).flatten() + wd.flatten()

        return x_k_plus_one

    def visualize(self,x,ax):
        ax.clear()
        q = x[2:4]
        num_segments = 20
        last_pos = np.zeros([3,1])
        for i in range(0,num_segments):
            scalar = float(i+1)/num_segments
            this_pos = fkEnd(q*scalar,self.h*scalar,self.h*scalar).reshape(4,4)[0:3,3]
            self.plot_line(ax, last_pos,this_pos,'orange',self.r*50)
            last_pos = this_pos


        self.set_axes_equal(ax)
        plt.ion()
        plt.show()
        plt.pause(.000001)

    def plot_line(self,ax, a, b, color, linewidth_custom, alpha=1.0):  # Ax must be projection 3d
        a = a.flatten()
        ax.plot([a[0], b[0]], [a[1], b[1]], [a[2], b[2]], color=color, linewidth=linewidth_custom, alpha=alpha)

    def set_axes_equal(self,ax):

        ax.set_xlim3d([-self.h,self.h])
        ax.set_ylim3d([-self.h,self.h])
        ax.set_zlim3d([0,self.h])
    
    

if __name__=='__main__':
    import time
    sys = BellowsGrub()

    q = np.ones((2,1))
    qd = np.zeros((2,1))
    u = np.array([[0],
                  [0],
                  [0],
                  [0]])
    
    x = np.vstack([qd,q])
    x0 = deepcopy(x)
    
    dt = .01
    
    ax = plt.gca(projection='3d')
    plt.ion()

    horizon = 5000
    a1 = 100
    a2 = 300
    for i in range(0,horizon):
        # u[0] = a1 + np.sin(i*.57)*a1/2.
        # u[1] = a1 - np.sin(i*.57)*a1/2.
        # u[2] = a2 + np.sin(i*.34)*a2/2.
        # u[3] = a2 - np.sin(i*.34)*a2/2.
        
        x = sys.forward_simulate_dt(x,u,dt)
        print(x.T)
        if i%10==0:
            sys.visualize(x,ax)
    
