#include <pybind11/stl.h>
#include <pybind11/numpy.h>
#include <math.h>

namespace py = pybind11;

py::array calc_M(std::vector<double> q,double l,double mtorso,double mthigh,double mcalf,double Itorso,double Ithigh,double Icalf);
py::array calc_grav(std::vector<double> q,double l,double mtorso,double mthigh,double mcalf,double g);
py::array calc_c(std::vector<double> q,std::vector<double> qd,double l,double mtorso,double mthigh,double mcalf);

py::array fk0(std::vector<double> q,double l);
py::array fk1(std::vector<double> q,double l);
py::array fk2(std::vector<double> q,double l);
py::array fkhead(std::vector<double> q,double l);
py::array fk3(std::vector<double> q,double l);
py::array fk4(std::vector<double> q,double l);
py::array fk5(std::vector<double> q,double l);
py::array fk6(std::vector<double> q,double l);
py::array jac0(std::vector<double> q,double l);
py::array jac1(std::vector<double> q,double l);
py::array jachead(std::vector<double> q,double l);
py::array jac2(std::vector<double> q,double l);
py::array jac3(std::vector<double> q,double l);
py::array jac4(std::vector<double> q,double l);
py::array jac5(std::vector<double> q,double l);
py::array jac6(std::vector<double> q,double l);
