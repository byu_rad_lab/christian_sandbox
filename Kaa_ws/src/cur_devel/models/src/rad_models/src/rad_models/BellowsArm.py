from rad_models.Model import Model
from kinematics import kinematics as kin
import numpy as np
from copy import deepcopy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

np.set_printoptions(precision=2)

def make_bellows_arm():
    # Create an arm
    arm = kin.Arm()
    num_joints = 3
    
    jt_limit=np.pi/2
    
    # Create the first joint
    joint = kin.JointCCC(.212)
    
    # Add the first joint to the first link
    l = kin.Link(joint)
    
    # Specify the transformation from the top of the joint to the end of the link
    rot = np.array([[.8536, .1464, -.5],
                    [.1464, .8536, .5],
                    [.5, -.5, .7071]])
    
    vec = np.array([[-.0148],
                    [.0148],
                    [.293]])
    hom0 = kin.HomFromRotVec(rot,vec)
    l.g_top_end_ = np.array(hom0)
    
    # Specify the inertia of this joint/link
    m = 6.534
    I = np.array([.108, .108, .023])
    pos = np.array([0.0, 0.0, .115]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
    l.setInertiaSelf(m, pos, I)
    
    # Add the link to the robot
    arm.addLink(l)
    
    # Create the second joint
    joint = kin.JointCCC(.212)
    
    # Add the second joint to the third link
    l = kin.Link(joint)
    
    # Specify the transformation from the top of the joint to the end of the link
    rot = np.array([[.8536, .1464, -.5],
                    [.1464, .8536, .5],
                    [.5, -.5, .7071]])
    
    vec = np.array([[-.0148],
                    [.0148],
                    [.293]])
    
    hom0 = np.array(kin.HomFromRotVec(rot,vec))
    l.g_top_end_ = np.array(hom0)
    
    # Specify the inertia of this joint/link
    m = 6.534
    I = np.array([.108, .108, .023])
    pos = np.array([0.0, 0.0, .115]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
    l.setInertiaSelf(m, pos, I)
    
    # Add the link to the robot
    arm.addLink(l)
    
    
    # Create the third joint
    joint = kin.JointCCC(.212)
    
    # Add the third joint to the third link
    l = kin.Link(joint)
    
    # Specify the transformation from the top of the joint to the end of the link
    rot = np.eye(3)
    
    vec = np.array([[0.0],
                    [0.0],
                    [.025]])
    
    hom0 = np.array(kin.HomFromRotVec(rot,vec))
    l.g_top_end_ = np.array(hom0)
    
    # Specify the inertia of this joint/link
    m = 1.5
    I = np.array([.05, .05, .023])
    pos = np.array([0.0, 0.0, .0125]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
    l.setInertiaSelf(m, pos, I)
    
    # Add the link to the robot
    arm.addLink(l)

    return arm

class BellowsArm(Model):

    def __init__(self):
        self.numStates = 24
        self.numInputs = 12
        self.arm = make_bellows_arm()

        # Linear model parameters
        # The values for these were taken from:
        # ego-atheris/src/app/yaml/zephyrus_1/robot.yaml
        self.K_passive_damper = np.diag([35.5,35.5,6.3,6.3,6.3,6.3])
        self.K_passive_spring = np.diag([136.336,136.336,22.119,22.119,22.119,22.119])
        
        self.prs_to_torque = np.array([[4.629e-4,-4.629e-4,0,0,0,0,0,0,0,0,0,0],
                                       [0,0,4.629e-4,-4.629e-4,0,0,0,0,0,0,0,0],
                                       [0,0,0,0,1.512e-4,-1.512e-4,0,0,0,0,0,0],
                                       [0,0,0,0,0,0,1.512e-4,-1.512e-4,0,0,0,0],
                                       [0,0,0,0,0,0,0,0,1.512e-4,-1.512e-4,0,0],
                                       [0,0,0,0,0,0,0,0,0,0,1.512e-4,-1.512e-4]])*1000.0
        # These are from crude measurements
        # They are assumed equal for all bellows in a joint
        self.alpha_fill = np.array([2.86,2.86,2.86,2.86,5.0,5.0,5.0,5.0,3.33,3.33,3.33,3.33])
        self.alpha_vent = np.array([18.2,18.2,18.2,18.2,23.0,23.0,23.0,23.0,6.67,6.67,6.67,6.67])

    def calc_continuous_A_B_w(self,x,u,dt,fill=np.ones([12,1])):
        p = x[0:12]
        qd = x[12:18]
        q = x[18:24]

        self.arm.setJointAngsReal(q)
        self.arm.setJointVels(qd)
        self.arm.calcDynamicsMats()
        M = self.arm.getInertiaMat()
        M_inv = np.linalg.inv(M)
        C = self.arm.coriolis_mat_
        
        while(np.isnan(M_inv).any() or np.isnan(C).any()):
            self.arm = make_bellows_arm()            
            print("recalculating Mass matrix...")
            print("q: ",q, "  qd: ",qd)
            self.arm.setJointAngsReal(q)
            self.arm.setJointVels(qd)
            self.arm.calcDynamicsMats()
            M = self.arm.getInertiaMat()
            M_inv = np.linalg.inv(M)
            C = self.arm.coriolis_mat_

        # Decide if alphas are for fill or vent
        self.alphas = np.eye(12)
        for i in range(0,12):
            # If pressure increased last time => fill=True
            if(fill[i]):
                self.alphas[i,i] = self.alpha_fill[i]
            else:
                self.alphas[i,i] = self.alpha_vent[i]

        A11 = -self.alphas
        A12 = np.zeros([12,6])
        A13 = np.zeros([12,6])
        A21 = np.matmul(M_inv,self.prs_to_torque)
        A22 = np.matmul(M_inv,-self.K_passive_damper-C)        
        A23 = np.matmul(M_inv,-self.K_passive_spring)        
        A31 = np.zeros([6,12])        
        A32 = np.eye(6)        
        A33 = np.zeros([6,6])

        A = np.vstack([np.hstack([A11,A12,A13]),np.hstack([A21,A22,A23]),np.hstack([A31,A32,A33])])
        
        B11 = self.alphas
        B21 = np.zeros([6,12])
        B31 = np.zeros([6,12])
        B = np.vstack([B11,B21,B31])

        self.arm.calcTorqueGravity()
        gravity_torque = self.arm.getTorqueGravity()

        w = np.matmul(M_inv,gravity_torque)
        w = np.vstack([np.zeros((12,1)),np.reshape(w,(6,1)),np.zeros((6,1))])
        
        return A,B,w
        
        
    def calc_discrete_A_B_w(self,x,u,dt=.01,fill=np.ones([12,1])):
        
        [A,B,w] = self.calc_continuous_A_B_w(x,u,fill)

        [Ad,Bd] = self.discretize_A_and_B(A,B,dt)
        wd = w*dt

        return Ad, Bd, wd

        
    def forward_simulate_dt(self,x,u,dt = 0.01,fill=np.ones([12,1])):

        [Ad,Bd,wd] = self.calc_discrete_A_B_w(x,u,dt)
        
        x_k_plus_one = np.matmul(Ad,x) + np.matmul(Bd,u) + wd

        return x_k_plus_one

    def visualize(self,x,ax):
        
        ax.clear()
        for i in range(0,len(self.arm.links())):
            if i==0:
                self.plot_cylinder(ax, self.arm.links_[i].g_world_bot(), self.arm.links_[i].g_world_top(), r=.1, color='r')
            else:
                self.plot_cylinder(ax, self.arm.links_[i].g_world_bot(), self.arm.links_[i].g_world_top(), r=.05, color='r')
            self.plot_cylinder(ax, self.arm.links_[i].g_world_top(), self.arm.links_[i].g_world_end(), r=.05, color='k')                            
            


        self.set_axes_equal(ax)            
        plt.ion()
        plt.show()
        plt.pause(.000001)

    def plot_cylinder(self,ax, g1, g2, r=0.05, color='k', n=10):
        th_vals = [2 * np.pi * i / n for i in range(n+1)]
        circle = [[r * np.sin(th), r * np.cos(th), 0.0, 1.0] for th in th_vals]
        circle = np.array(circle).T
        
        circle1 = g1.dot(circle)
        g3 = deepcopy(g1)  # Gets rid of twist in Z
        g3[0:3, 3] = g3[0:3, 3]+(g2[0:3, 3]-g1[0:3, 3])
        circle2 = g3.dot(circle)
        
        for i in range(circle1.shape[1] - 1):
            self.plot_line(ax, circle1[:, i], circle1[:, i+1], color, 1.0)
            self.plot_line(ax, circle2[:, i], circle2[:, i+1], color, 1.0)
            self.plot_line(ax, circle1[:, i], circle2[:, i], color, 1.0)
            self.plot_line(ax, circle1[:, i], circle2[:, i+1], color, 1.0)
            
    def plot_line(self,ax, a, b, color, linewidth_custom, alpha=1.0):  # Ax must be projection 3d
        ax.plot([a[0], b[0]], [a[1], b[1]], [a[2], b[2]], color=color, linewidth=linewidth_custom, alpha=alpha)

    def set_axes_equal(self,ax):
        
        ax.set_xlim3d([-1,1])
        ax.set_ylim3d([-1,1])
        ax.set_zlim3d([0,1])
    

if __name__=='__main__':
    import time
    sys = BellowsArm()

    q = np.ones((6,1))
    qd = np.zeros((6,1))
    p = np.array([[300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300],
                  [300]])
    
    x = np.vstack([p,qd,q])
    u = deepcopy(p*0)
    x0 = deepcopy(x)
    
    dt = .01
    
    ax = plt.gca(projection='3d')
    plt.ion()

    horizon = 5000
    for i in range(0,horizon):
        start = time.time()
        x = sys.forward_simulate_dt(x,u,dt)
        print("simulation time: ",time.time()-start)

        if i%5==0:
            print(x[18:24].T)
            sys.visualize(x,ax)
    
