#include <math.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

py::array calc_M(std::vector<double> q,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot,double Itorso,double Ithigh,double Icalf,double Ifoot);

py::array calc_grav(std::vector<double> q,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot,double g);

py::array calc_c(std::vector<double> q,std::vector<double> qd,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot);

py::array fkCOM(std::vector<double> q,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot);

py::array fkCOMWRTFoot(std::vector<double> q,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot);

py::array fkTorsoWRTFoot(std::vector<double> q,double l,double lfoot);

py::array fkFootWRTFoot(std::vector<double> q,double l,double lfoot);

py::array fkTorso(std::vector<double> q,double l,double lfoot);

py::array fkTorsoTop(std::vector<double> q,double l,double lfoot);

py::array fkTorsoBot(std::vector<double> q,double l,double lfoot);

py::array fkThigh1Bot(std::vector<double> q,double l,double lfoot);

py::array fkCalf1Bot(std::vector<double> q,double l,double lfoot);

py::array fkFoot1(std::vector<double> q,double l,double lfoot);

py::array fkToe1(std::vector<double> q,double l,double lfoot);

py::array fkHeel1(std::vector<double> q,double l,double lfoot);

py::array fkThigh2Bot(std::vector<double> q,double l,double lfoot);

py::array fkCalf2Bot(std::vector<double> q,double l,double lfoot);

py::array fkFoot2(std::vector<double> q,double l,double lfoot);

py::array fkToe2(std::vector<double> q,double l,double lfoot);

py::array fkHeel2(std::vector<double> q,double l,double lfoot);

py::array jacCOM(std::vector<double> q,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot);

py::array jacCOMWRTFoot(std::vector<double> q,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot);

py::array jacFootWRTFoot(std::vector<double> q,double l,double lfoot);

py::array jacTorsoWRTFoot(std::vector<double> q,double l,double lfoot);

py::array jacTorso(std::vector<double> q,double l,double lfoot);

py::array jacTorsoWRTFoot1(std::vector<double> q,double l,double lfoot);

py::array jacTorsoWRTFoot2(std::vector<double> q,double l,double lfoot);

py::array jacTorsoTop(std::vector<double> q,double l,double lfoot);

py::array jacTorsoBot(std::vector<double> q,double l,double lfoot);

py::array jacThigh1Bot(std::vector<double> q,double l,double lfoot);

py::array jacCalf1Bot(std::vector<double> q,double l,double lfoot);

py::array jacFoot1(std::vector<double> q,double l,double lfoot);

py::array jacToe1(std::vector<double> q,double l,double lfoot);

py::array jacHeel1(std::vector<double> q,double l,double lfoot);

py::array jacThigh2Bot(std::vector<double> q,double l,double lfoot);

py::array jacCalf2Bot(std::vector<double> q,double l,double lfoot);

py::array jacFoot2(std::vector<double> q,double l,double lfoot);

py::array jacToe2(std::vector<double> q,double l,double lfoot);

py::array jacHeel2(std::vector<double> q,double l,double lfoot);
