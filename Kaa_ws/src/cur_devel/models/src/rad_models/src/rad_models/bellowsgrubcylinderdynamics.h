#include <math.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>
#include <Eigen/LU>

namespace py = pybind11;

py::array calc_M(std::vector<double> q,double h,double m,double r);
py::array calc_grav(std::vector<double> q,double h,double m,double g);
py::array calc_C(std::vector<double> q,std::vector<double> qd,double h,double m,double r);
py::array fkEnd(std::vector<double> q,double h);
