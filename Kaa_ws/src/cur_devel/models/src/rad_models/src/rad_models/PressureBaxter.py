from rad_models.Model import Model
from pressure_baxter_right_dynamics import *
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy


class PressureBaxter(Model):

    def __init__(self):
        
        self.numStates = 28
        self.numInputs = 14

    def calc_discrete_A_B_w(self,x,u,dt):
        qd = x[14:21]        
        q = x[21:28]

        mat = calc_discrete_A_B_w_python(q,qd,dt)

        Ad = np.reshape(mat[0:self.numStates*self.numStates],[self.numStates,self.numStates],'F')
        Bd = np.reshape(mat[self.numStates*self.numStates:self.numStates*self.numStates+self.numStates*self.numInputs],[self.numStates,self.numInputs],'F')
        wd = np.reshape(mat[self.numStates*self.numStates+self.numStates*self.numInputs:],[self.numStates,1],'F')
        return Ad, Bd, wd
        

if __name__=='__main__':
    
    sys = PressureBaxter()
    x = np.array([100.0,100,100,100,100,100,100,100,100,100,100,100,100,100, 0,0,0,0,0,0,0, 1,1,1,1,1,1,1]).reshape(28,1)
    u = np.ones([sys.numInputs,1])*200.0
    u = np.array([8,300,8,300,8,300,8,300,8,300,8,300,8,300.0]).reshape(sys.numInputs,1)
    x0 = deepcopy(x)
    
    dt = .01
    
    fig = plt.gca()
    plt.ion()

    sim_length = 1000
    x_hist = np.zeros([sys.numStates,sim_length])
    u_hist = np.zeros([sys.numInputs,sim_length])
    
    for i in range(0,sim_length):
        [Ad,Bd,wd] = sys.calc_discrete_A_B_w(x,u,dt)
        # print "Ad: ",Ad
        # print "Bd: ",Bd
        # print "wd: ",wd
        # raw_input()
        x = Ad.dot(x) + Bd.dot(u) + wd
        u_hist[:,i] = u.flatten()
        x_hist[:,i] = x.flatten()

    plt.figure(1)
    plt.ion()
    plt.plot(x_hist[0:14,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Pressure (KPa)')
    plt.title('Pressure')
    plt.show()
    
    plt.figure(2)
    plt.ion()
    plt.plot(x_hist[14:21,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Velocity (rad/s)')
    plt.title('Velocity')
    plt.show()

    plt.figure(4)
    plt.ion()
    plt.plot(x_hist[21:28,:].T)
    plt.xlabel('Timestep')
    plt.ylabel('Theta (rad)')
    plt.title('Position')
    plt.show()
        
    plt.figure(5)
    plt.plot(u_hist.T)
    plt.show()
    plt.xlabel('Timestep')
    plt.ylabel('Torque (Nm)')
    plt.title('Input')

    plt.pause(1000)
        
