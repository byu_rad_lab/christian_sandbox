from rad_models.Model import Model
from threelinkdynamics import *
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy

class PDThreelink(Model):

    def __init__(self,m=.5,I=1.0,l=.5,g=9.81):
        
        self.numStates = 6
        self.numInputs = 3

        self.uMax = np.pi*2.0
        self.tauMax = 5.0

        self.Kp = 10.0
        self.Kd = 1.0
        
        self.m = m
        self.I = I
        self.l = l
        self.g = g

    def get_accel(self,q,qd,tauControl,dt):
        n = 3
        # Find the number of trajectories and simulate in parallel...
        numSims = q.shape[1]
        accels = np.zeros([n,numSims])
        for i in range(0,numSims):
        
            M = calc_M(q[:,i],self.l,self.m,self.I).squeeze().reshape([n,n])
            grav = calc_grav(q[:,i],self.l,self.m,self.g).squeeze()
            c = calc_c(q[:,i],qd[:,i],self.l,self.m).squeeze()
            Minv = np.linalg.inv(M)

            tauTotal = np.reshape(-grav - c + tauControl[:,i],[n,1])
            
            accels[:,i] = np.linalg.inv(M).dot(tauTotal).flatten()

        return accels
    
    def forward_simulate_dt(self,x,u,dt):
        u = deepcopy(u.clip(-self.uMax,self.uMax)).reshape([self.numInputs,-1])

        tau = np.zeros(u.shape)
        tau[0,:] = self.Kp*(u[0,:] - x[0+3,:]) - self.Kd*x[0,:]
        tau[1,:] = self.Kp*(u[1,:] - x[1+3,:]) - self.Kd*x[1,:]
        tau[2,:] = self.Kp*(u[2,:] - x[2+3,:]) - self.Kd*x[2,:]        

        tau = tau.clip(-self.tauMax,self.tauMax)
        
        numSims = x.shape[1]
        n = 3
        b = .001        
        x = deepcopy(x).reshape([self.numStates,-1])

        accels = self.get_accel(x[n:self.numStates,:],x[0:n,:],tau,dt).reshape([n,-1])
        x[0:n,:] = (1.0-b)*x[0:n,:] + accels*dt
        x[n:self.numStates,:] = x[n:self.numStates,:] + x[0:n,:]*dt
        return x

    def draw_link(self,l,CoM,theta):
        theta+=np.pi/2
        x = [CoM[0] + l/2.0*np.sin(theta),CoM[0] - l/2.0*np.sin(theta)]
        y = [CoM[1] - l/2.0*np.cos(theta),CoM[1] + l/2.0*np.cos(theta)]

        plt.plot(x,y)        

    def visualize(self,x):
        x = np.array(x).flatten()
        n = 3
        q = x[n:self.numStates]
        
        rA = fk0(q,self.l)        
        rB = fk1(q,self.l)        
        rC = fk2(q,self.l)        
        
        plt.clf()
        self.draw_link(self.l,rA[0:2],q[0])
        self.draw_link(self.l,rB[0:2],q[0] + q[1])
        self.draw_link(self.l,rC[0:2],rC[2])
        plt.ion()
        plt.axis(xmin=-5,xmax=5,ymin=-4,ymax=4)
        plt.pause(.00000001)
        
        plt.show()
        

if __name__=='__main__':
    
    sys = PDThreelink()    
    x = np.zeros([sys.numStates,1])
    u = np.ones([sys.numInputs,1])    
    x0 = deepcopy(x)
    
    dt = .01
    
    fig = plt.gca()
    plt.ion()

    horizon = 5000
    Kp = 500.0
    Kd = 10.0
    for i in range(0,horizon):

        x = sys.forward_simulate_dt(x,u,dt)

        if i%5==0:
            sys.visualize(x)
