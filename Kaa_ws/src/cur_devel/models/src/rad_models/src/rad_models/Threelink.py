from rad_models.Model import Model
from threelinkdynamics import *
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy
from matplotlib import patches


class Threelink(Model):

    def __init__(self,m=.5,I=.1,l=.5,g=9.81):
        
        self.numStates = 6
        self.numInputs = 3

        self.uMax = 10.0
        
        self.m = m
        self.I = I
        self.l = l
        self.g = g

    def get_accel(self,q,qd,tauControl,dt):
        n = 3
        # Find the number of trajectories and simulate in parallel...
        numSims = q.shape[1]
        accels = np.zeros([n,numSims])
        for i in range(0,numSims):
        
            M = calc_M(q[:,i],self.l,self.m,self.I).squeeze().reshape([n,n])
            grav = calc_grav(q[:,i],self.l,self.m,self.g).squeeze()
            c = calc_c(q[:,i],qd[:,i],self.l,self.m).squeeze()
            Minv = np.linalg.inv(M)

            tauTotal = np.reshape(-grav - c + tauControl[:,i],[n,1])
            
            accels[:,i] = np.linalg.inv(M).dot(tauTotal).flatten()

        return accels
    
    def forward_simulate_dt(self,x,u,dt,wrapAngle=True):
        x = x.reshape([self.numStates,-1])
        numSims = x.shape[1]
        n = 3
        b = .015
        x = deepcopy(x).reshape([self.numStates,-1])
        # u = deepcopy(np.clip(u,-self.uMax,self.uMax)).reshape([self.numInputs,-1])
        u = deepcopy(u).reshape([self.numInputs,-1])        

        accels = self.get_accel(x[n:self.numStates,:],x[0:n,:],u,dt).reshape([n,-1])
        x[n:self.numStates,:] = x[n:self.numStates,:] + x[0:n,:]*dt        
        x[0:n,:] = (1.0-b)*x[0:n,:] + accels*dt
        if wrapAngle==True:
            x[3:6,:] = (x[3:6,:] + np.pi) % (2*np.pi) - np.pi        
        return x

    def draw_link(self,l,CoM,theta):
        x = [CoM[0] + l/2.0*np.sin(theta),CoM[0] - l/2.0*np.sin(theta)]
        y = [CoM[1] - l/2.0*np.cos(theta),CoM[1] + l/2.0*np.cos(theta)]

        plt.plot(x,y)

    def draw_torque(self,l,CoM,theta,u):
        x = CoM[0] - l/2.0*np.sin(theta)
        y = CoM[1] + l/2.0*np.cos(theta)
        width = .1
        height = .1
        myangle = -90
        if(u<0):
            mytheta1 = 360 + np.rad2deg(u*np.pi/self.uMax)
            mytheta2 = 0
        else:
            mytheta1 = 0
            mytheta2 = np.rad2deg(u*np.pi/self.uMax)
        arc = patches.Arc([x,y],width,height,myangle,theta1=mytheta1,theta2=mytheta2,lw=1.5,color='r')
        ax = plt.gca()
        ax.add_patch(arc)
        

    def visualize(self,x,u=np.zeros([3,1])):
        x = np.array(x).flatten()
        n = 3
        q = x[n:self.numStates]
        
        rA = fk0(q,self.l)        
        rB = fk1(q,self.l)        
        rC = fk2(q,self.l)

        plt.clf()
        self.draw_link(self.l,rA[0:2],rA[2])
        self.draw_link(self.l,rB[0:2],rB[2])
        self.draw_link(self.l,rC[0:2],rC[2])
        
        # self.draw_torque(self.l,[0,-self.l/2.0],0.0,u[0])
        # self.draw_torque(self.l,rA[0:2],rA[2],u[1])
        # self.draw_torque(self.l,rB[0:2],rB[2],u[2])
        
        plt.ion()
        plt.axis(xmin=-1.75,xmax=1.75,ymin=-1.75,ymax=1.75)
        plt.pause(.00000001)
        
        plt.show()
        

if __name__=='__main__':
    
    sys = Threelink()    
    x = np.zeros([sys.numStates,1])
    x[3] = .001
    u = np.zeros([sys.numInputs,1])    
    x0 = deepcopy(x)
    
    dt = .01
    
    fig = plt.gca()
    plt.ion()

    horizon = 5000
    Kp = 500.0
    Kd = 10.0
    for i in range(0,horizon):

        # u[0] = Kp*(np.pi/3 - x[0+3]) - Kd*x[0]
        # u[1] = Kp*(np.pi/3 - x[1+3]) - Kd*x[1]
        # u[2] = Kp*(np.pi/3 - x[2+3]) - Kd*x[2]        
        x = sys.forward_simulate_dt(x,u,dt)

        if i%5==0:
            sys.visualize(x)
