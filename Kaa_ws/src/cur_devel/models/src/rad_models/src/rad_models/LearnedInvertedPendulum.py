from rad_models.InvertedPendulum import InvertedPendulum
import numpy as np
import torch
from torch.autograd import Variable
from copy import deepcopy
import matplotlib.pyplot as plt

class LearnedInvertedPendulum(InvertedPendulum):

    def __init__(self,use_gpu=True):
        self.numStates = 2
        self.numInputs = 1
        self.use_gpu = use_gpu
        self.uMax = 10.0
        self.l = 1.0

        if self.use_gpu==True:
            self.model = torch.load('/home/phil/git/byu/cur_devel/models/src/rad_models/src/rad_models/learned_models/InvertedPendulumModel.pt').cuda()
        else:
            self.model = torch.load('/home/phil/git/byu/cur_devel/models/src/rad_models/src/rad_models/learned_models/InvertedPendulumModel.pt')

    def forward_simulate_dt(self,x,u,dt=.01,wrapAngle=True):
        u = np.reshape(u,[u.shape[0],-1])
        x = np.reshape(x,[x.shape[0],-1])        
        inputs = np.vstack([x,u/self.uMax]).T
        if self.use_gpu==True:
            self.input_var = Variable(torch.from_numpy(inputs).float(),requires_grad=False).cuda()
        else:
            self.input_var = Variable(torch.from_numpy(inputs).float(),requires_grad=False)

        x = self.model(self.input_var).cpu().detach().numpy().T
        if wrapAngle==True:
            x[1,:] = (x[1,:] + np.pi) % (2*np.pi) - np.pi        
        return x
            

    # def forward_simulate_dt(self,x,u,dt=.01):
    #     inputs = np.vstack([x,u/self.uMax]).T
    #     if self.use_gpu==True:
    #         self.input_var = Variable(torch.from_numpy(inputs).float(),requires_grad=False).cuda()
    #     else:
    #         self.input_var = Variable(torch.from_numpy(inputs).float(),requires_grad=False)

    #     x = self.model(self.input_var).cpu().detach().numpy().T
    #     x[1,:] = (x[1,:] + np.pi) % (2*np.pi) - np.pi
    #     return x

if __name__=='__main__':
    sys = LearnedInvertedPendulum()
    x = np.reshape([0,np.pi/2],[sys.numStates,1])
    u = np.zeros([sys.numInputs,1])    
    x0 = deepcopy(x)
    
    dt = .01
    
    fig = plt.gca()
    plt.ion()

    horizon = 5000
    Kp = 500.0
    Kd = 10.0
    for i in range(0,horizon):

        x = sys.forward_simulate_dt(x,u,dt)
        
        if i%5==0:
            sys.visualize(x)
    
