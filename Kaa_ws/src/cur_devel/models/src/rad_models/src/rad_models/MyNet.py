import torch
import torch.nn as nn
from torch.autograd import Variable

class StructuredDynamicsNet(nn.Module):

    def __init__(self,n,m,dt=.01):
        super(StructuredDynamicsNet, self).__init__()
        self.dt = .01
        self.n = n
        self.m = m
        self.input_size = 2*n + m
        self.output_size = n
        self.activation = nn.ReLU()
        # self.activation = nn.Sigmoid()
        self.fc0 = nn.Linear(self.input_size,100)        
        self.fc1 = nn.Linear(100,200)
        self.fc2 = nn.Linear(200,400)
        self.fc3 = nn.Linear(400,600)
        self.fc4 = nn.Linear(600,800)
        # self.fc5 = nn.Linear(800,800)
        self.fc6 = nn.Linear(800,600)
        self.fc7 = nn.Linear(600,400)        
        self.fc8 = nn.Linear(400,200)        
        self.fc9 = nn.Linear(200,100)
        self.fc11 = nn.Linear(100,n*n+n)

    def forward(self,x):
        out100 = self.activation(self.fc0(x))
        out200 = self.activation(self.fc1(out100))
        out400 = self.activation(self.fc2(out200))
        out600 = self.activation(self.fc3(out400))
        out800 = self.activation(self.fc4(out600))
        # out800 = out800 + self.activation(self.fc5(out800))
        # TQ note: This archetecture is not the same as the U-net, the two outputs should be concatenated,
        # not added. All of Phil Hyatt's work uses addition, but results can be improved
        # through concatenation. See class DynamicsNet_cat (next class) for a correct version.
        out600 = out600 + self.activation(self.fc6(out800))        
        out400 = out400 + self.activation(self.fc7(out600))        
        out200 = out200 + self.activation(self.fc8(out400))
        out100 = out100 + self.activation(self.fc9(out200))
        out = self.fc11(out100)
        Minv = torch.reshape(out[:,0:self.n*self.n],[-1,self.n,self.n])
        b = torch.reshape(out[:,self.n*self.n:self.n*self.n+self.n],[-1,self.n,1])
        tau = torch.reshape(x[:,0:self.n],[-1,self.n,1])
        qd = torch.reshape(x[:,self.n:self.n+self.m],[-1,self.n,1])
        q = torch.reshape(x[:,self.n+self.m:self.n+self.m+self.n],[-1,self.n,1])        
        accel = Minv.matmul(-b + tau)
        qnew = q + qd*self.dt        
        qdnew = qd + accel*self.dt
        xnew = torch.reshape(torch.cat((qdnew,qnew),1),[self.n*2,-1])
        return xnew


class DynamicsNet_cat(nn.Module):

    def __init__(self,n,m,dt=.01):
        super(DynamicsNet, self).__init__()
        self.dt = .01
        self.n = n
        self.m = m
        self.input_size = n + m
        self.output_size = n/2
        self.activation = nn.ReLU()
        # self.activation = nn.Sigmoid()
        self.fc0 = nn.Linear(self.input_size,100)        
        self.fc1 = nn.Linear(100,200)
        self.fc2 = nn.Linear(200,400)
        self.fc3 = nn.Linear(400,600)
        self.fc4 = nn.Linear(600,800)
        # self.fc5 = nn.Linear(800,800)

        # Incorrect
        # self.fc6 = nn.Linear(800,600)
        # self.fc7 = nn.Linear(600,400)        
        # self.fc8 = nn.Linear(400,200)        
        # self.fc9 = nn.Linear(200,100)
        # self.fc11 = nn.Linear(100,n/2)

        # Correct
        
        self.fc6 = nn.Linear(800,600)
        self.fc7 = nn.Linear(2*600,400)        
        self.fc8 = nn.Linear(2*400,200)        
        self.fc9 = nn.Linear(2*200,100)
        self.fc11 = nn.Linear(2*100,self.output_size)
        

    def forward(self,x):
        out100 = self.activation(self.fc0(x))
        out200 = self.activation(self.fc1(out100))
        out400 = self.activation(self.fc2(out200))
        out600 = self.activation(self.fc3(out400))
        out800 = self.activation(self.fc4(out600))
        # out800 = out800 + self.activation(self.fc5(out800))
        # Incorrect
        # out600 = out600 + self.activation(self.fc6(out800))        
        # out400 = out400 + self.activation(self.fc7(out600))        
        # out200 = out200 + self.activation(self.fc8(out400))
        # out100 = out100 + self.activation(self.fc9(out200))

        # Correct U-Net archetecture
        
        out600 = torch.cat((out600, self.activation(self.fc6(out800))), dim=1)        
        out400 = torch.cat((out400, self.activation(self.fc7(out600))), dim=1)      
        out200 = torch.cat((out200, self.activation(self.fc8(out400))), dim=1)
        out100 = torch.cat((out100, self.activation(self.fc9(out200))), dim=1)
        
        xnew = torch.zeros(x.shape[0],self.n).cuda()
        xnew[:,self.n/2:self.n] = x[:,self.n/2:self.n] + x[:,0:self.n/2]*self.dt
        xnew[:,0:self.n/2] = x[:,0:self.n/2] + self.fc11(out100)        
        return xnew



class DynamicsNet(nn.Module):

    def __init__(self,n,m,dt=.01):
        super(DynamicsNet_add, self).__init__()
        self.dt = .01
        self.n = n
        self.m = m
        self.input_size = n + m
        self.output_size = n/2
        self.activation = nn.ReLU()
        # self.activation = nn.Sigmoid()
        self.fc0 = nn.Linear(self.input_size,100)        
        self.fc1 = nn.Linear(100,200)
        self.fc2 = nn.Linear(200,400)
        self.fc3 = nn.Linear(400,600)
        self.fc4 = nn.Linear(600,800)
        # self.fc5 = nn.Linear(800,800)

        # Incorrect
        self.fc6 = nn.Linear(800,600)
        self.fc7 = nn.Linear(600,400)        
        self.fc8 = nn.Linear(400,200)        
        self.fc9 = nn.Linear(200,100)
        self.fc11 = nn.Linear(100,n/2)

        # Correct
        
        # self.fc6 = nn.Linear(800,600)
        # self.fc7 = nn.Linear(2*600,400)        
        # self.fc8 = nn.Linear(2*400,200)        
        # self.fc9 = nn.Linear(2*200,100)
        # self.fc11 = nn.Linear(2*100,self.output_size)
        

    def forward(self,x):
        out100 = self.activation(self.fc0(x))
        out200 = self.activation(self.fc1(out100))
        out400 = self.activation(self.fc2(out200))
        out600 = self.activation(self.fc3(out400))
        out800 = self.activation(self.fc4(out600))
        # out800 = out800 + self.activation(self.fc5(out800))
        # Incorrect
        out600 = out600 + self.activation(self.fc6(out800))        
        out400 = out400 + self.activation(self.fc7(out600))        
        out200 = out200 + self.activation(self.fc8(out400))
        out100 = out100 + self.activation(self.fc9(out200))

        # Correct U-Net archetecture
        
        # out600 = torch.cat((out600, self.activation(self.fc6(out800))), dim=1)        
        # out400 = torch.cat((out400, self.activation(self.fc7(out600))), dim=1)      
        # out200 = torch.cat((out200, self.activation(self.fc8(out400))), dim=1)
        # out100 = torch.cat((out100, self.activation(self.fc9(out200))), dim=1)
        
        xnew = torch.zeros(x.shape[0],self.n).cuda()
        xnew[:,self.n/2:self.n] = x[:,self.n/2:self.n] + x[:,0:self.n/2]*self.dt
        xnew[:,0:self.n/2] = x[:,0:self.n/2] + self.fc11(out100)        
        return xnew



# this was used to represent inverted pendulum, 3 link, and bellows arm in the RA-L NEMPC paper (Phil Thinks) 
# was intentionally small to execute faster


class SmallDynamicsNet(nn.Module):

    def __init__(self,n,m,dt=.01):
        super(SmallDynamicsNet, self).__init__()
        self.dt = dt
        self.n = n
        self.m = m
        self.input_size = n + m
        self.output_size = n/2
        self.activation = nn.ReLU()
        # self.activation = nn.Sigmoid()
        self.fc0 = nn.Linear(self.input_size,100)        
        self.fc1 = nn.Linear(100,200)
        self.fc2 = nn.Linear(200,400)
        # self.fc3 = nn.Linear(400,600)
        # self.fc7 = nn.Linear(600,400)        
        self.fc8 = nn.Linear(400,200)        
        self.fc9 = nn.Linear(200,100)
        self.fc11 = nn.Linear(100,n/2)

    def forward(self,x):
        out100 = self.activation(self.fc0(x))
        out200 = self.activation(self.fc1(out100))
        out400 = self.activation(self.fc2(out200))
        # out600 = self.activation(self.fc3(out400))
        # out400 = out400 + self.activation(self.fc7(out600))        
        out200 = out200 + self.activation(self.fc8(out400))
        out100 = out100 + self.activation(self.fc9(out200))
        xnew = torch.zeros(x.shape[0],self.n).cuda()
        
        # this is just doing the numerical integration, could do trapezoidal rule instead since we have initial and final velocity
        xnew[:,self.n/2:self.n] = x[:,self.n/2:self.n] + x[:,0:self.n/2]*self.dt
        xnew[:,0:self.n/2] = x[:,0:self.n/2] + self.fc11(out100)        
        return xnew

class BellowsArmNet(nn.Module):

    def __init__(self,n,m,dt=.01):
        super(BellowsArmNet, self).__init__()
        self.dt = dt
        self.n = 24
        self.m = 12
        self.input_size = n + m
        self.output_size = 6
        # self.alphas = nn.Linear(12,12,bias=False)
        self.activation = nn.ReLU()        
        self.fc0 = nn.Linear(self.input_size,100)
        self.fc1 = nn.Linear(100,200)
        self.fc2 = nn.Linear(200,400)
        # self.fc2big = nn.Linear(400,400)
        # self.fc2bigbig = nn.Linear(800,400)
        self.fc3 = nn.Linear(400,200)        
        self.fc4 = nn.Linear(200,100)
        self.fc5 = nn.Linear(100,6)

    def forward(self,x):
        # x[:,0:12] = x[:,0:12]/400.0
        # x[:,12:18] = x[:,12:18]/(2.0*3.14)
        # x[:,18:24] = x[:,18:24]/3.14
        # x[:,24:36] = x[:,24:36]/400.0

        out100 = self.activation(self.fc0(x))
        out200 = self.activation(self.fc1(out100))
        out400 = self.activation(self.fc2(out200))
        # out400 = out400 + self.activation(self.fc2big(out400))
        # out400 = out400 + self.activation(self.fc2bigbig(out600))
        out200 = out200 + self.activation(self.fc3(out400))
        out100 = out100 + self.activation(self.fc4(out200))
        # out200 = self.activation(self.fc3(out400))
        # out100 = self.activation(self.fc4(out200))
        
        # xnew = torch.zeros(x.shape[0],self.n).cuda()
        # this is us handling the integration of 1st-order pressure dynamics
        # xnew[:,0:12] = (x[:,0:12] + self.alphas(x[:,24:36] - x[:,0:12]))*400.0        
        # xnew[:,12:18] = x[:,12:18] + self.fc5(out100)
        # xnew[:,18:24] = x[:,18:24] + xnew[:,12:18]*self.dt
        
        return self.fc5(out100)

class BellowsArmResNet(nn.Module):

    def __init__(self,n,m,dt=.01):
        super(BellowsArmResNet, self).__init__()
        layer_size = 100
        self.dt = dt
        self.n = 24
        self.m = 12
        self.input_size = n + m + 3
        self.output_size = 18
        self.alphas = nn.Linear(12,12,bias=False)
        self.activation = nn.ReLU()        
        self.fc0 = nn.Linear(self.input_size,layer_size)
        self.bn0 = nn.BatchNorm1d(layer_size)
        self.fc1 = nn.Linear(layer_size,layer_size)
        self.bn1 = nn.BatchNorm1d(layer_size)        
        self.fc2 = nn.Linear(layer_size,layer_size)
        self.bn2 = nn.BatchNorm1d(layer_size)
        self.fc3 = nn.Linear(layer_size,layer_size)
        self.bn3 = nn.BatchNorm1d(layer_size)
        self.fc4 = nn.Linear(layer_size,layer_size)
        self.bn4 = nn.BatchNorm1d(layer_size)
        self.fc5 = nn.Linear(layer_size,layer_size)
        self.bn5 = nn.BatchNorm1d(layer_size)
        # self.fc6 = nn.Linear(layer_size,layer_size)
        # self.bn6 = nn.BatchNorm1d(layer_size)
        # self.fc7 = nn.Linear(layer_size,layer_size)
        # self.bn7 = nn.BatchNorm1d(layer_size)
        # self.fc8 = nn.Linear(layer_size,layer_size)
        # self.bn8 = nn.BatchNorm1d(layer_size)
        # self.fc9 = nn.Linear(layer_size,layer_size)
        # self.bn9 = nn.BatchNorm1d(layer_size)
        # self.fc10 = nn.Linear(layer_size,layer_size)
        # self.bn10 = nn.BatchNorm1d(layer_size)
        # self.fc11 = nn.Linear(layer_size,layer_size)
        # self.bn11 = nn.BatchNorm1d(layer_size)
        # self.fc12 = nn.Linear(layer_size,layer_size)
        # self.bn12 = nn.BatchNorm1d(layer_size)
        # self.fc13 = nn.Linear(layer_size,layer_size)
        # self.bn13 = nn.BatchNorm1d(layer_size)
        # self.fc14 = nn.Linear(layer_size,layer_size)
        # self.bn14 = nn.BatchNorm1d(layer_size)
        # self.fc15 = nn.Linear(layer_size,layer_size)
        # self.bn15 = nn.BatchNorm1d(layer_size)
        # self.fc16 = nn.Linear(layer_size,layer_size)
        # self.bn16 = nn.BatchNorm1d(layer_size)
        # self.fc17 = nn.Linear(layer_size,layer_size)
        # self.bn17 = nn.BatchNorm1d(layer_size)
        # self.fc18 = nn.Linear(layer_size,layer_size)
        # self.bn18 = nn.BatchNorm1d(layer_size)
        # self.fc19 = nn.Linear(layer_size,layer_size)
        # self.bn19 = nn.BatchNorm1d(layer_size)
        self.fclast = nn.Linear(layer_size,6)

    def forward(self,x):
        x[:,0:12] = x[:,0:12]/400.0 - .5
        x[:,12:18] = x[:,12:18]/(2.0*3.14)
        # x[:,18:24] = x[:,18:24]/3.14
        x[:,18:24] = x[:,18:24]
        x[:,24:36] = x[:,24:36]/400.0 - .5

        # import numpy as np

        # print "mean: ",torch.mean(x,dim=0)
        # print "std: ",torch.std(x,dim=0)

        phi0 = torch.sqrt(x[:,18]**2+x[:,19]**2).reshape(x.shape[0],1)
        phi1 = torch.sqrt(x[:,20]**2+x[:,21]**2).reshape(x.shape[0],1)
        phi2 = torch.sqrt(x[:,22]**2+x[:,23]**2).reshape(x.shape[0],1)

        x_with_extras = torch.cat([x,phi0,phi1,phi2],dim=1)

        out1 = self.bn1(self.fc1(self.activation(self.bn0(self.fc0(x_with_extras)))))
        out2 = out1 + self.bn3(self.fc3(self.activation(self.bn2(self.fc2(out1)))))
        out3 = out2 + self.bn5(self.fc5(self.activation(self.bn4(self.fc4(out2)))))
        # out4 = out3 + self.bn7(self.fc7(self.activation(self.bn6(self.fc6(out3)))))
        # out5 = out4 + self.bn9(self.fc9(self.activation(self.bn8(self.fc8(out4)))))
        # out6 = out5 + self.bn11(self.fc11(self.activation(self.bn10(self.fc10(out5)))))
        # out7 = out6 + self.bn13(self.fc13(self.activation(self.bn12(self.fc12(out6)))))
        # out8 = out7 + self.bn15(self.fc15(self.activation(self.bn14(self.fc14(out7)))))
        # out9 = out8 + self.bn17(self.fc17(self.activation(self.bn16(self.fc16(out8)))))
        # out10 = out9 + self.bn19(self.fc19(self.activation(self.bn18(self.fc18(out9)))))
        
        xnew = torch.zeros(x.shape[0],self.n).cuda()
        xnew[:,0:12] = (x[:,0:12]+.5 + self.alphas(x[:,24:36] - x[:,0:12]))*400.0        
        xnew[:,12:18] = (x[:,12:18] + self.fclast(out3))*(2.0*3.14)
        # xnew[:,12:18] = self.fclast(out10)*(2.0*3.14)        
        # xnew[:,18:24] = (x[:,18:24] + 0.5*(xnew[:,12:18] + x[:,12:18])*self.dt)*3.14
        xnew[:,18:24] = (x[:,18:24] + 0.5*(xnew[:,12:18] + x[:,12:18])*self.dt)

        return xnew
    
    

class MyNet(nn.Module):

    def __init__(self,input_size=21,output_size=7):
        super(MyNet, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.activation = nn.ReLU()
        # self.activation = nn.Sigmoid()
        self.fc0 = nn.Linear(input_size,100)        
        self.fc1 = nn.Linear(100,200)
        self.fc2 = nn.Linear(200,400)
        self.fc3 = nn.Linear(400,600)
        self.fc4 = nn.Linear(600,800)
        # self.fc5 = nn.Linear(800,800)
        self.fc6 = nn.Linear(800,600)
        self.fc7 = nn.Linear(600,400)        
        self.fc8 = nn.Linear(400,200)        
        self.fc9 = nn.Linear(200,100)
        self.fc11 = nn.Linear(100,output_size)

    def init_weights(self,m):
        if type(m) == nn.Linear:
            nn.init.xavier_normal_(m.weight,gain=nn.init.calculate_gain('relu'))
            nn.init.normal_(m.bias,nn.init.calculate_gain('relu'))
        
    def forward(self,x):
        out100 = self.activation(self.fc0(x))        
        out200 = self.activation(self.fc1(out100))
        out400 = self.activation(self.fc2(out200))
        out600 = self.activation(self.fc3(out400))
        out800 = self.activation(self.fc4(out600))
        # out800 = out800 + self.activation(self.fc5(out800))       
        out600 = out600 + self.activation(self.fc6(out800))        
        out400 = out400 + self.activation(self.fc7(out600))        
        out200 = out200 + self.activation(self.fc8(out400))
        out100 = out100 + self.activation(self.fc9(out200))
        out = x[:,0:self.output_size] + self.fc11(out100)
        return out

class MyBigNet(nn.Module):

    def __init__(self,input_size=21,output_size=7):
        super(MyBigNet, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.activation = nn.ReLU()
        # self.activation = nn.Sigmoid()
        self.fc0 = nn.Linear(input_size,100)        
        self.fc1 = nn.Linear(100,200)
        self.fc2 = nn.Linear(200,400)
        self.fc3 = nn.Linear(400,600)
        self.fc4 = nn.Linear(600,800)
        self.fcbig = nn.Linear(800,1600)
        self.fcbigbig = nn.Linear(1600,1600)
        self.fcbig2 = nn.Linear(1600,800)                        
        # self.fc5 = nn.Linear(800,800)
        self.fc6 = nn.Linear(800,600)
        self.fc7 = nn.Linear(600,400)        
        self.fc8 = nn.Linear(400,200)        
        self.fc9 = nn.Linear(200,100)
        self.fc11 = nn.Linear(100,output_size)

    def init_weights(self,m):
        if type(m) == nn.Linear:
            nn.init.xavier_normal_(m.weight,gain=nn.init.calculate_gain('relu'))
            nn.init.normal_(m.bias,nn.init.calculate_gain('relu'))
        
    def forward(self,x):
        out100 = self.activation(self.fc0(x))        
        out200 = self.activation(self.fc1(out100))
        out400 = self.activation(self.fc2(out200))
        out600 = self.activation(self.fc3(out400))
        out800 = self.activation(self.fc4(out600))
        out1600 = self.activation(self.fcbig(out800))
        out1600 = out1600 + self.activation(self.fcbigbig(out1600))
        out800 = out800 + self.activation(self.fcbig2(out1600))
        # out800 = out800 + self.activation(self.fc5(out800))       
        out600 = out600 + self.activation(self.fc6(out800))        
        out400 = out400 + self.activation(self.fc7(out600))        
        out200 = out200 + self.activation(self.fc8(out400))
        out100 = out100 + self.activation(self.fc9(out200))
        out = x[:,0:self.output_size] + self.fc11(out100)
        return out
    
class MySmallNet(nn.Module):

    def __init__(self,input_size=21,output_size=7):        
        super(MySmallNet, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.activation = nn.ReLU()
        self.fc0 = nn.Linear(input_size,100)        
        self.fc1 = nn.Linear(100,200)
        self.fc2 = nn.Linear(200,400)
        self.fc3 = nn.Linear(400,200)        
        self.fc4 = nn.Linear(200,100)
        self.fc5 = nn.Linear(100,output_size)        

    def init_weights(self,m):
        if type(m) == nn.Linear:
            nn.init.xavier_normal_(m.weight,gain=nn.init.calculate_gain('sigmoid'))
            nn.init.normal_(m.bias)
        
    def forward(self,x):
        out100 = self.activation(self.fc0(x))        
        out200 = self.activation(self.fc1(out100))
        out400 = self.activation(self.fc2(out200))
        out200 = out200 + self.activation(self.fc3(out400))
        out100 = out100 + self.activation(self.fc4(out200))
        out12 = x[:,0:self.output_size] + self.fc5(out100)
        return out12
    
