#include "anklekneewalkerdynamics.h"
#include <math.h>

namespace py = pybind11;

py::array calc_M(std::vector<double> q,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot,double Itorso,double Ithigh,double Icalf,double Ifoot)
{
	 double out[81];

	 out[0] = 2.0*mcalf + 2.0*mfoot + 2.0*mthigh + 1.0*mtorso;
	 out[1] = 0;
	 out[2] = 0.5*l*(2*mcalf*cos(q[2]) + 2*mcalf*cos(q[2] + q[3]) + 2*mcalf*cos(q[2] + q[6]) + mcalf*cos(q[2] + q[3] + q[4]) + mcalf*cos(q[2] + q[6] + q[7]) + 2*mfoot*cos(q[2]) + 2*mfoot*cos(q[2] + q[3]) + 2*mfoot*cos(q[2] + q[6]) + 2*mfoot*cos(q[2] + q[3] + q[4]) + 2*mfoot*cos(q[2] + q[6] + q[7]) + 2*mthigh*cos(q[2]) + mthigh*cos(q[2] + q[3]) + mthigh*cos(q[2] + q[6]));
	 out[3] = l*(0.5*mcalf*(2*cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4])) + mfoot*(cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4])) + 0.5*mthigh*cos(q[2] + q[3]));
	 out[4] = l*(0.5*mcalf + 1.0*mfoot)*cos(q[2] + q[3] + q[4]);
	 out[5] = 0;
	 out[6] = l*(0.5*mcalf*(2*cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7])) + mfoot*(cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7])) + 0.5*mthigh*cos(q[2] + q[6]));
	 out[7] = l*(0.5*mcalf + 1.0*mfoot)*cos(q[2] + q[6] + q[7]);
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 2.0*mcalf + 2.0*mfoot + 2.0*mthigh + 1.0*mtorso;
	 out[11] = 0.5*l*(2*mcalf*sin(q[2]) + 2*mcalf*sin(q[2] + q[3]) + 2*mcalf*sin(q[2] + q[6]) + mcalf*sin(q[2] + q[3] + q[4]) + mcalf*sin(q[2] + q[6] + q[7]) + 2*mfoot*sin(q[2]) + 2*mfoot*sin(q[2] + q[3]) + 2*mfoot*sin(q[2] + q[6]) + 2*mfoot*sin(q[2] + q[3] + q[4]) + 2*mfoot*sin(q[2] + q[6] + q[7]) + 2*mthigh*sin(q[2]) + mthigh*sin(q[2] + q[3]) + mthigh*sin(q[2] + q[6]));
	 out[12] = l*(0.5*mcalf*(2*sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + mfoot*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + 0.5*mthigh*sin(q[2] + q[3]));
	 out[13] = l*(0.5*mcalf + 1.0*mfoot)*sin(q[2] + q[3] + q[4]);
	 out[14] = 0;
	 out[15] = l*(0.5*mcalf*(2*sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + mfoot*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + 0.5*mthigh*sin(q[2] + q[6]));
	 out[16] = l*(0.5*mcalf + 1.0*mfoot)*sin(q[2] + q[6] + q[7]);
	 out[17] = 0;
	 out[18] = 0.5*l*(2*mcalf*cos(q[2]) + 2*mcalf*cos(q[2] + q[3]) + 2*mcalf*cos(q[2] + q[6]) + mcalf*cos(q[2] + q[3] + q[4]) + mcalf*cos(q[2] + q[6] + q[7]) + 2*mfoot*cos(q[2]) + 2*mfoot*cos(q[2] + q[3]) + 2*mfoot*cos(q[2] + q[6]) + 2*mfoot*cos(q[2] + q[3] + q[4]) + 2*mfoot*cos(q[2] + q[6] + q[7]) + 2*mthigh*cos(q[2]) + mthigh*cos(q[2] + q[3]) + mthigh*cos(q[2] + q[6]));
	 out[19] = 0.5*l*(2*mcalf*sin(q[2]) + 2*mcalf*sin(q[2] + q[3]) + 2*mcalf*sin(q[2] + q[6]) + mcalf*sin(q[2] + q[3] + q[4]) + mcalf*sin(q[2] + q[6] + q[7]) + 2*mfoot*sin(q[2]) + 2*mfoot*sin(q[2] + q[3]) + 2*mfoot*sin(q[2] + q[6]) + 2*mfoot*sin(q[2] + q[3] + q[4]) + 2*mfoot*sin(q[2] + q[6] + q[7]) + 2*mthigh*sin(q[2]) + mthigh*sin(q[2] + q[3]) + mthigh*sin(q[2] + q[6]));
	 out[20] = 2.0*Icalf + 2.0*Ifoot + 2.0*Ithigh + 1.0*Itorso + 1.0*pow(l, 2)*mcalf*cos(q[3]) + 1.0*pow(l, 2)*mcalf*cos(q[4]) + 1.0*pow(l, 2)*mcalf*cos(q[6]) + 1.0*pow(l, 2)*mcalf*cos(q[7]) + 0.5*pow(l, 2)*mcalf*cos(q[3] + q[4]) + 0.5*pow(l, 2)*mcalf*cos(q[6] + q[7]) + 3.0*pow(l, 2)*mcalf + 1.0*pow(l, 2)*mfoot*cos(q[3]) + 2.0*pow(l, 2)*mfoot*cos(q[4]) + 1.0*pow(l, 2)*mfoot*cos(q[6]) + 2.0*pow(l, 2)*mfoot*cos(q[7]) + 1.0*pow(l, 2)*mfoot*cos(q[3] + q[4]) + 1.0*pow(l, 2)*mfoot*cos(q[6] + q[7]) + 4.5*pow(l, 2)*mfoot + 0.5*pow(l, 2)*mthigh*cos(q[3]) + 0.5*pow(l, 2)*mthigh*cos(q[6]) + 1.0*pow(l, 2)*mthigh;
	 out[21] = 1.0*Icalf + 1.0*Ifoot + 1.0*Ithigh + 0.5*pow(l, 2)*mcalf*cos(q[3]) + 1.0*pow(l, 2)*mcalf*cos(q[4]) + 0.25*pow(l, 2)*mcalf*cos(q[3] + q[4]) + 1.25*pow(l, 2)*mcalf + 0.5*pow(l, 2)*mfoot*cos(q[3]) + 2.0*pow(l, 2)*mfoot*cos(q[4]) + 0.5*pow(l, 2)*mfoot*cos(q[3] + q[4]) + 2.0*pow(l, 2)*mfoot + 0.25*pow(l, 2)*mthigh*cos(q[3]) + 0.25*pow(l, 2)*mthigh;
	 out[22] = 1.0*Icalf + 1.0*Ifoot + 0.5*pow(l, 2)*mcalf*cos(q[4]) + 0.25*pow(l, 2)*mcalf*cos(q[3] + q[4]) + 0.25*pow(l, 2)*mcalf + 1.0*pow(l, 2)*mfoot*cos(q[4]) + 0.5*pow(l, 2)*mfoot*cos(q[3] + q[4]) + 1.0*pow(l, 2)*mfoot;
	 out[23] = 1.0*Ifoot;
	 out[24] = 1.0*Icalf + 1.0*Ifoot + 1.0*Ithigh + 0.5*pow(l, 2)*mcalf*cos(q[6]) + 1.0*pow(l, 2)*mcalf*cos(q[7]) + 0.25*pow(l, 2)*mcalf*cos(q[6] + q[7]) + 1.25*pow(l, 2)*mcalf + 0.5*pow(l, 2)*mfoot*cos(q[6]) + 2.0*pow(l, 2)*mfoot*cos(q[7]) + 0.5*pow(l, 2)*mfoot*cos(q[6] + q[7]) + 2.0*pow(l, 2)*mfoot + 0.25*pow(l, 2)*mthigh*cos(q[6]) + 0.25*pow(l, 2)*mthigh;
	 out[25] = 1.0*Icalf + 1.0*Ifoot + 0.5*pow(l, 2)*mcalf*cos(q[7]) + 0.25*pow(l, 2)*mcalf*cos(q[6] + q[7]) + 0.25*pow(l, 2)*mcalf + 1.0*pow(l, 2)*mfoot*cos(q[7]) + 0.5*pow(l, 2)*mfoot*cos(q[6] + q[7]) + 1.0*pow(l, 2)*mfoot;
	 out[26] = 1.0*Ifoot;
	 out[27] = l*(0.5*mcalf*(2*cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4])) + mfoot*(cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4])) + 0.5*mthigh*cos(q[2] + q[3]));
	 out[28] = l*(0.5*mcalf*(2*sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + mfoot*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + 0.5*mthigh*sin(q[2] + q[3]));
	 out[29] = 1.0*Icalf + 1.0*Ifoot + 1.0*Ithigh + 0.5*pow(l, 2)*mcalf*cos(q[3]) + 1.0*pow(l, 2)*mcalf*cos(q[4]) + 0.25*pow(l, 2)*mcalf*cos(q[3] + q[4]) + 1.25*pow(l, 2)*mcalf + 0.5*pow(l, 2)*mfoot*cos(q[3]) + 2.0*pow(l, 2)*mfoot*cos(q[4]) + 0.5*pow(l, 2)*mfoot*cos(q[3] + q[4]) + 2.0*pow(l, 2)*mfoot + 0.25*pow(l, 2)*mthigh*cos(q[3]) + 0.25*pow(l, 2)*mthigh;
	 out[30] = 1.0*Icalf + 1.0*Ifoot + 1.0*Ithigh + 1.0*pow(l, 2)*mcalf*cos(q[4]) + 1.25*pow(l, 2)*mcalf + 2.0*pow(l, 2)*mfoot*cos(q[4]) + 2.0*pow(l, 2)*mfoot + 0.25*pow(l, 2)*mthigh;
	 out[31] = 1.0*Icalf + 1.0*Ifoot + 0.5*pow(l, 2)*mcalf*cos(q[4]) + 0.25*pow(l, 2)*mcalf + 1.0*pow(l, 2)*mfoot*cos(q[4]) + 1.0*pow(l, 2)*mfoot;
	 out[32] = 1.0*Ifoot;
	 out[33] = 0;
	 out[34] = 0;
	 out[35] = 0;
	 out[36] = l*(0.5*mcalf + 1.0*mfoot)*cos(q[2] + q[3] + q[4]);
	 out[37] = l*(0.5*mcalf + 1.0*mfoot)*sin(q[2] + q[3] + q[4]);
	 out[38] = 1.0*Icalf + 1.0*Ifoot + 0.5*pow(l, 2)*mcalf*cos(q[4]) + 0.25*pow(l, 2)*mcalf*cos(q[3] + q[4]) + 0.25*pow(l, 2)*mcalf + 1.0*pow(l, 2)*mfoot*cos(q[4]) + 0.5*pow(l, 2)*mfoot*cos(q[3] + q[4]) + 1.0*pow(l, 2)*mfoot;
	 out[39] = 1.0*Icalf + 1.0*Ifoot + 0.5*pow(l, 2)*mcalf*cos(q[4]) + 0.25*pow(l, 2)*mcalf + 1.0*pow(l, 2)*mfoot*cos(q[4]) + 1.0*pow(l, 2)*mfoot;
	 out[40] = 1.0*Icalf + 1.0*Ifoot + 0.25*pow(l, 2)*mcalf + 1.0*pow(l, 2)*mfoot;
	 out[41] = 1.0*Ifoot;
	 out[42] = 0;
	 out[43] = 0;
	 out[44] = 0;
	 out[45] = 0;
	 out[46] = 0;
	 out[47] = 1.0*Ifoot;
	 out[48] = 1.0*Ifoot;
	 out[49] = 1.0*Ifoot;
	 out[50] = 1.0*Ifoot;
	 out[51] = 0;
	 out[52] = 0;
	 out[53] = 0;
	 out[54] = l*(0.5*mcalf*(2*cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7])) + mfoot*(cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7])) + 0.5*mthigh*cos(q[2] + q[6]));
	 out[55] = l*(0.5*mcalf*(2*sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + mfoot*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + 0.5*mthigh*sin(q[2] + q[6]));
	 out[56] = 1.0*Icalf + 1.0*Ifoot + 1.0*Ithigh + 0.5*pow(l, 2)*mcalf*cos(q[6]) + 1.0*pow(l, 2)*mcalf*cos(q[7]) + 0.25*pow(l, 2)*mcalf*cos(q[6] + q[7]) + 1.25*pow(l, 2)*mcalf + 0.5*pow(l, 2)*mfoot*cos(q[6]) + 2.0*pow(l, 2)*mfoot*cos(q[7]) + 0.5*pow(l, 2)*mfoot*cos(q[6] + q[7]) + 2.0*pow(l, 2)*mfoot + 0.25*pow(l, 2)*mthigh*cos(q[6]) + 0.25*pow(l, 2)*mthigh;
	 out[57] = 0;
	 out[58] = 0;
	 out[59] = 0;
	 out[60] = 1.0*Icalf + 1.0*Ifoot + 1.0*Ithigh + 1.0*pow(l, 2)*mcalf*cos(q[7]) + 1.25*pow(l, 2)*mcalf + 2.0*pow(l, 2)*mfoot*cos(q[7]) + 2.0*pow(l, 2)*mfoot + 0.25*pow(l, 2)*mthigh;
	 out[61] = 1.0*Icalf + 1.0*Ifoot + 0.5*pow(l, 2)*mcalf*cos(q[7]) + 0.25*pow(l, 2)*mcalf + 1.0*pow(l, 2)*mfoot*cos(q[7]) + 1.0*pow(l, 2)*mfoot;
	 out[62] = 1.0*Ifoot;
	 out[63] = l*(0.5*mcalf + 1.0*mfoot)*cos(q[2] + q[6] + q[7]);
	 out[64] = l*(0.5*mcalf + 1.0*mfoot)*sin(q[2] + q[6] + q[7]);
	 out[65] = 1.0*Icalf + 1.0*Ifoot + 0.5*pow(l, 2)*mcalf*cos(q[7]) + 0.25*pow(l, 2)*mcalf*cos(q[6] + q[7]) + 0.25*pow(l, 2)*mcalf + 1.0*pow(l, 2)*mfoot*cos(q[7]) + 0.5*pow(l, 2)*mfoot*cos(q[6] + q[7]) + 1.0*pow(l, 2)*mfoot;
	 out[66] = 0;
	 out[67] = 0;
	 out[68] = 0;
	 out[69] = 1.0*Icalf + 1.0*Ifoot + 0.5*pow(l, 2)*mcalf*cos(q[7]) + 0.25*pow(l, 2)*mcalf + 1.0*pow(l, 2)*mfoot*cos(q[7]) + 1.0*pow(l, 2)*mfoot;
	 out[70] = 1.0*Icalf + 1.0*Ifoot + 0.25*pow(l, 2)*mcalf + 1.0*pow(l, 2)*mfoot;
	 out[71] = 1.0*Ifoot;
	 out[72] = 0;
	 out[73] = 0;
	 out[74] = 1.0*Ifoot;
	 out[75] = 0;
	 out[76] = 0;
	 out[77] = 0;
	 out[78] = 1.0*Ifoot;
	 out[79] = 1.0*Ifoot;
	 out[80] = 1.0*Ifoot;
	return py::array(81,out);

}

py::array calc_grav(std::vector<double> q,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot,double g)
{
	 double out[9];

	 out[0] = 0;
	 out[1] = g*(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[2] = (1.0/2.0)*g*l*(2*mcalf*sin(q[2]) + 2*mcalf*sin(q[2] + q[3]) + 2*mcalf*sin(q[2] + q[6]) + mcalf*sin(q[2] + q[3] + q[4]) + mcalf*sin(q[2] + q[6] + q[7]) + 2*mfoot*sin(q[2]) + 2*mfoot*sin(q[2] + q[3]) + 2*mfoot*sin(q[2] + q[6]) + 2*mfoot*sin(q[2] + q[3] + q[4]) + 2*mfoot*sin(q[2] + q[6] + q[7]) + 2*mthigh*sin(q[2]) + mthigh*sin(q[2] + q[3]) + mthigh*sin(q[2] + q[6]));
	 out[3] = (1.0/2.0)*g*l*(mcalf*(2*sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + 2*mfoot*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + mthigh*sin(q[2] + q[3]));
	 out[4] = (1.0/2.0)*g*l*(mcalf + 2*mfoot)*sin(q[2] + q[3] + q[4]);
	 out[5] = 0;
	 out[6] = (1.0/2.0)*g*l*(mcalf*(2*sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + 2*mfoot*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + mthigh*sin(q[2] + q[6]));
	 out[7] = (1.0/2.0)*g*l*(mcalf + 2*mfoot)*sin(q[2] + q[6] + q[7]);
	 out[8] = 0;
	return py::array(9,out);

}

py::array calc_c(std::vector<double> q,std::vector<double> qd,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot)
{
	 double out[9];

	 out[0] = -l*(1.0*mcalf*pow(qd[2], 2)*sin(q[2]) + 1.0*mcalf*pow(qd[2], 2)*sin(q[2] + q[3]) + 1.0*mcalf*pow(qd[2], 2)*sin(q[2] + q[6]) + 0.5*mcalf*pow(qd[2], 2)*sin(q[2] + q[3] + q[4]) + 0.5*mcalf*pow(qd[2], 2)*sin(q[2] + q[6] + q[7]) + 2.0*mcalf*qd[2]*qd[3]*sin(q[2] + q[3]) + 1.0*mcalf*qd[2]*qd[3]*sin(q[2] + q[3] + q[4]) + 1.0*mcalf*qd[2]*qd[4]*sin(q[2] + q[3] + q[4]) + 2.0*mcalf*qd[2]*qd[6]*sin(q[2] + q[6]) + 1.0*mcalf*qd[2]*qd[6]*sin(q[2] + q[6] + q[7]) + 1.0*mcalf*qd[2]*qd[7]*sin(q[2] + q[6] + q[7]) + 1.0*mcalf*pow(qd[3], 2)*sin(q[2] + q[3]) + 0.5*mcalf*pow(qd[3], 2)*sin(q[2] + q[3] + q[4]) + 1.0*mcalf*qd[3]*qd[4]*sin(q[2] + q[3] + q[4]) + 0.5*mcalf*pow(qd[4], 2)*sin(q[2] + q[3] + q[4]) + 1.0*mcalf*pow(qd[6], 2)*sin(q[2] + q[6]) + 0.5*mcalf*pow(qd[6], 2)*sin(q[2] + q[6] + q[7]) + 1.0*mcalf*qd[6]*qd[7]*sin(q[2] + q[6] + q[7]) + 0.5*mcalf*pow(qd[7], 2)*sin(q[2] + q[6] + q[7]) + 1.0*mfoot*pow(qd[2], 2)*sin(q[2]) + 1.0*mfoot*pow(qd[2], 2)*sin(q[2] + q[3]) + 1.0*mfoot*pow(qd[2], 2)*sin(q[2] + q[6]) + 1.0*mfoot*pow(qd[2], 2)*sin(q[2] + q[3] + q[4]) + 1.0*mfoot*pow(qd[2], 2)*sin(q[2] + q[6] + q[7]) + 2.0*mfoot*qd[2]*qd[3]*sin(q[2] + q[3]) + 2.0*mfoot*qd[2]*qd[3]*sin(q[2] + q[3] + q[4]) + 2.0*mfoot*qd[2]*qd[4]*sin(q[2] + q[3] + q[4]) + 2.0*mfoot*qd[2]*qd[6]*sin(q[2] + q[6]) + 2.0*mfoot*qd[2]*qd[6]*sin(q[2] + q[6] + q[7]) + 2.0*mfoot*qd[2]*qd[7]*sin(q[2] + q[6] + q[7]) + 1.0*mfoot*pow(qd[3], 2)*sin(q[2] + q[3]) + 1.0*mfoot*pow(qd[3], 2)*sin(q[2] + q[3] + q[4]) + 2.0*mfoot*qd[3]*qd[4]*sin(q[2] + q[3] + q[4]) + 1.0*mfoot*pow(qd[4], 2)*sin(q[2] + q[3] + q[4]) + 1.0*mfoot*pow(qd[6], 2)*sin(q[2] + q[6]) + 1.0*mfoot*pow(qd[6], 2)*sin(q[2] + q[6] + q[7]) + 2.0*mfoot*qd[6]*qd[7]*sin(q[2] + q[6] + q[7]) + 1.0*mfoot*pow(qd[7], 2)*sin(q[2] + q[6] + q[7]) + 1.0*mthigh*pow(qd[2], 2)*sin(q[2]) + 0.5*mthigh*pow(qd[2], 2)*sin(q[2] + q[3]) + 0.5*mthigh*pow(qd[2], 2)*sin(q[2] + q[6]) + 1.0*mthigh*qd[2]*qd[3]*sin(q[2] + q[3]) + 1.0*mthigh*qd[2]*qd[6]*sin(q[2] + q[6]) + 0.5*mthigh*pow(qd[3], 2)*sin(q[2] + q[3]) + 0.5*mthigh*pow(qd[6], 2)*sin(q[2] + q[6]));
	 out[1] = l*(1.0*mcalf*pow(qd[2], 2)*cos(q[2]) + 1.0*mcalf*pow(qd[2], 2)*cos(q[2] + q[3]) + 1.0*mcalf*pow(qd[2], 2)*cos(q[2] + q[6]) + 0.5*mcalf*pow(qd[2], 2)*cos(q[2] + q[3] + q[4]) + 0.5*mcalf*pow(qd[2], 2)*cos(q[2] + q[6] + q[7]) + 2.0*mcalf*qd[2]*qd[3]*cos(q[2] + q[3]) + 1.0*mcalf*qd[2]*qd[3]*cos(q[2] + q[3] + q[4]) + 1.0*mcalf*qd[2]*qd[4]*cos(q[2] + q[3] + q[4]) + 2.0*mcalf*qd[2]*qd[6]*cos(q[2] + q[6]) + 1.0*mcalf*qd[2]*qd[6]*cos(q[2] + q[6] + q[7]) + 1.0*mcalf*qd[2]*qd[7]*cos(q[2] + q[6] + q[7]) + 1.0*mcalf*pow(qd[3], 2)*cos(q[2] + q[3]) + 0.5*mcalf*pow(qd[3], 2)*cos(q[2] + q[3] + q[4]) + 1.0*mcalf*qd[3]*qd[4]*cos(q[2] + q[3] + q[4]) + 0.5*mcalf*pow(qd[4], 2)*cos(q[2] + q[3] + q[4]) + 1.0*mcalf*pow(qd[6], 2)*cos(q[2] + q[6]) + 0.5*mcalf*pow(qd[6], 2)*cos(q[2] + q[6] + q[7]) + 1.0*mcalf*qd[6]*qd[7]*cos(q[2] + q[6] + q[7]) + 0.5*mcalf*pow(qd[7], 2)*cos(q[2] + q[6] + q[7]) + 1.0*mfoot*pow(qd[2], 2)*cos(q[2]) + 1.0*mfoot*pow(qd[2], 2)*cos(q[2] + q[3]) + 1.0*mfoot*pow(qd[2], 2)*cos(q[2] + q[6]) + 1.0*mfoot*pow(qd[2], 2)*cos(q[2] + q[3] + q[4]) + 1.0*mfoot*pow(qd[2], 2)*cos(q[2] + q[6] + q[7]) + 2.0*mfoot*qd[2]*qd[3]*cos(q[2] + q[3]) + 2.0*mfoot*qd[2]*qd[3]*cos(q[2] + q[3] + q[4]) + 2.0*mfoot*qd[2]*qd[4]*cos(q[2] + q[3] + q[4]) + 2.0*mfoot*qd[2]*qd[6]*cos(q[2] + q[6]) + 2.0*mfoot*qd[2]*qd[6]*cos(q[2] + q[6] + q[7]) + 2.0*mfoot*qd[2]*qd[7]*cos(q[2] + q[6] + q[7]) + 1.0*mfoot*pow(qd[3], 2)*cos(q[2] + q[3]) + 1.0*mfoot*pow(qd[3], 2)*cos(q[2] + q[3] + q[4]) + 2.0*mfoot*qd[3]*qd[4]*cos(q[2] + q[3] + q[4]) + 1.0*mfoot*pow(qd[4], 2)*cos(q[2] + q[3] + q[4]) + 1.0*mfoot*pow(qd[6], 2)*cos(q[2] + q[6]) + 1.0*mfoot*pow(qd[6], 2)*cos(q[2] + q[6] + q[7]) + 2.0*mfoot*qd[6]*qd[7]*cos(q[2] + q[6] + q[7]) + 1.0*mfoot*pow(qd[7], 2)*cos(q[2] + q[6] + q[7]) + 1.0*mthigh*pow(qd[2], 2)*cos(q[2]) + 0.5*mthigh*pow(qd[2], 2)*cos(q[2] + q[3]) + 0.5*mthigh*pow(qd[2], 2)*cos(q[2] + q[6]) + 1.0*mthigh*qd[2]*qd[3]*cos(q[2] + q[3]) + 1.0*mthigh*qd[2]*qd[6]*cos(q[2] + q[6]) + 0.5*mthigh*pow(qd[3], 2)*cos(q[2] + q[3]) + 0.5*mthigh*pow(qd[6], 2)*cos(q[2] + q[6]));
	 out[2] = pow(l, 2)*(-1.0*mcalf*qd[2]*qd[3]*sin(q[3]) - 0.5*mcalf*qd[2]*qd[3]*sin(q[3] + q[4]) - 1.0*mcalf*qd[2]*qd[4]*sin(q[4]) - 0.5*mcalf*qd[2]*qd[4]*sin(q[3] + q[4]) - 1.0*mcalf*qd[2]*qd[6]*sin(q[6]) - 0.5*mcalf*qd[2]*qd[6]*sin(q[6] + q[7]) - 1.0*mcalf*qd[2]*qd[7]*sin(q[7]) - 0.5*mcalf*qd[2]*qd[7]*sin(q[6] + q[7]) - 0.5*mcalf*pow(qd[3], 2)*sin(q[3]) - 0.25*mcalf*pow(qd[3], 2)*sin(q[3] + q[4]) - 1.0*mcalf*qd[3]*qd[4]*sin(q[4]) - 0.5*mcalf*qd[3]*qd[4]*sin(q[3] + q[4]) - 0.5*mcalf*pow(qd[4], 2)*sin(q[4]) - 0.25*mcalf*pow(qd[4], 2)*sin(q[3] + q[4]) - 0.5*mcalf*pow(qd[6], 2)*sin(q[6]) - 0.25*mcalf*pow(qd[6], 2)*sin(q[6] + q[7]) - 1.0*mcalf*qd[6]*qd[7]*sin(q[7]) - 0.5*mcalf*qd[6]*qd[7]*sin(q[6] + q[7]) - 0.5*mcalf*pow(qd[7], 2)*sin(q[7]) - 0.25*mcalf*pow(qd[7], 2)*sin(q[6] + q[7]) - 1.0*mfoot*qd[2]*qd[3]*sin(q[3]) - 1.0*mfoot*qd[2]*qd[3]*sin(q[3] + q[4]) - 2.0*mfoot*qd[2]*qd[4]*sin(q[4]) - 1.0*mfoot*qd[2]*qd[4]*sin(q[3] + q[4]) - 1.0*mfoot*qd[2]*qd[6]*sin(q[6]) - 1.0*mfoot*qd[2]*qd[6]*sin(q[6] + q[7]) - 2.0*mfoot*qd[2]*qd[7]*sin(q[7]) - 1.0*mfoot*qd[2]*qd[7]*sin(q[6] + q[7]) - 0.5*mfoot*pow(qd[3], 2)*sin(q[3]) - 0.5*mfoot*pow(qd[3], 2)*sin(q[3] + q[4]) - 2.0*mfoot*qd[3]*qd[4]*sin(q[4]) - 1.0*mfoot*qd[3]*qd[4]*sin(q[3] + q[4]) - 1.0*mfoot*pow(qd[4], 2)*sin(q[4]) - 0.5*mfoot*pow(qd[4], 2)*sin(q[3] + q[4]) - 0.5*mfoot*pow(qd[6], 2)*sin(q[6]) - 0.5*mfoot*pow(qd[6], 2)*sin(q[6] + q[7]) - 2.0*mfoot*qd[6]*qd[7]*sin(q[7]) - 1.0*mfoot*qd[6]*qd[7]*sin(q[6] + q[7]) - 1.0*mfoot*pow(qd[7], 2)*sin(q[7]) - 0.5*mfoot*pow(qd[7], 2)*sin(q[6] + q[7]) - 0.5*mthigh*qd[2]*qd[3]*sin(q[3]) - 0.5*mthigh*qd[2]*qd[6]*sin(q[6]) - 0.25*mthigh*pow(qd[3], 2)*sin(q[3]) - 0.25*mthigh*pow(qd[6], 2)*sin(q[6]));
	 out[3] = pow(l, 2)*(0.5*mcalf*pow(qd[2], 2)*sin(q[3]) + 0.25*mcalf*pow(qd[2], 2)*sin(q[3] + q[4]) - 1.0*mcalf*qd[2]*qd[4]*sin(q[4]) - 1.0*mcalf*qd[3]*qd[4]*sin(q[4]) - 0.5*mcalf*pow(qd[4], 2)*sin(q[4]) + 0.5*mfoot*pow(qd[2], 2)*sin(q[3]) + 0.5*mfoot*pow(qd[2], 2)*sin(q[3] + q[4]) - 2.0*mfoot*qd[2]*qd[4]*sin(q[4]) - 2.0*mfoot*qd[3]*qd[4]*sin(q[4]) - 1.0*mfoot*pow(qd[4], 2)*sin(q[4]) + 0.25*mthigh*pow(qd[2], 2)*sin(q[3]));
	 out[4] = pow(l, 2)*(0.5*mcalf*pow(qd[2], 2)*sin(q[4]) + 0.25*mcalf*pow(qd[2], 2)*sin(q[3] + q[4]) + 1.0*mcalf*qd[2]*qd[3]*sin(q[4]) + 0.5*mcalf*pow(qd[3], 2)*sin(q[4]) + 1.0*mfoot*pow(qd[2], 2)*sin(q[4]) + 0.5*mfoot*pow(qd[2], 2)*sin(q[3] + q[4]) + 2.0*mfoot*qd[2]*qd[3]*sin(q[4]) + 1.0*mfoot*pow(qd[3], 2)*sin(q[4]));
	 out[5] = 0;
	 out[6] = pow(l, 2)*(0.5*mcalf*pow(qd[2], 2)*sin(q[6]) + 0.25*mcalf*pow(qd[2], 2)*sin(q[6] + q[7]) - 1.0*mcalf*qd[2]*qd[7]*sin(q[7]) - 1.0*mcalf*qd[6]*qd[7]*sin(q[7]) - 0.5*mcalf*pow(qd[7], 2)*sin(q[7]) + 0.5*mfoot*pow(qd[2], 2)*sin(q[6]) + 0.5*mfoot*pow(qd[2], 2)*sin(q[6] + q[7]) - 2.0*mfoot*qd[2]*qd[7]*sin(q[7]) - 2.0*mfoot*qd[6]*qd[7]*sin(q[7]) - 1.0*mfoot*pow(qd[7], 2)*sin(q[7]) + 0.25*mthigh*pow(qd[2], 2)*sin(q[6]));
	 out[7] = pow(l, 2)*(0.5*mcalf*pow(qd[2], 2)*sin(q[7]) + 0.25*mcalf*pow(qd[2], 2)*sin(q[6] + q[7]) + 1.0*mcalf*qd[2]*qd[6]*sin(q[7]) + 0.5*mcalf*pow(qd[6], 2)*sin(q[7]) + 1.0*mfoot*pow(qd[2], 2)*sin(q[7]) + 0.5*mfoot*pow(qd[2], 2)*sin(q[6] + q[7]) + 2.0*mfoot*qd[2]*qd[6]*sin(q[7]) + 1.0*mfoot*pow(qd[6], 2)*sin(q[7]));
	 out[8] = 0;
	return py::array(9,out);

}

py::array fkCOM(std::vector<double> q,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot)
{
	 double out[3];

	 out[0] = (mcalf*((1.0/2.0)*l*sin(q[2]) + l*sin(q[2] + q[3]) + (1.0/2.0)*l*sin(q[2] + q[3] + q[4]) + q[0]) + mcalf*((1.0/2.0)*l*sin(q[2]) + l*sin(q[2] + q[6]) + (1.0/2.0)*l*sin(q[2] + q[6] + q[7]) + q[0]) + mfoot*(l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + (1.0/2.0)*l*sin(q[2]) + q[0]) + mfoot*(l*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + (1.0/2.0)*l*sin(q[2]) + q[0]) + mthigh*((1.0/2.0)*l*sin(q[2]) + (1.0/2.0)*l*sin(q[2] + q[3]) + q[0]) + mthigh*((1.0/2.0)*l*sin(q[2]) + (1.0/2.0)*l*sin(q[2] + q[6]) + q[0]) + mtorso*q[0])/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[1] = (mcalf*(-1.0/2.0*l*cos(q[2]) - l*cos(q[2] + q[3]) - 1.0/2.0*l*cos(q[2] + q[3] + q[4]) + q[1]) + mcalf*(-1.0/2.0*l*cos(q[2]) - l*cos(q[2] + q[6]) - 1.0/2.0*l*cos(q[2] + q[6] + q[7]) + q[1]) + mfoot*(l*(-cos(q[2] + q[3]) - cos(q[2] + q[3] + q[4])) - 1.0/2.0*l*cos(q[2]) + q[1]) + mfoot*(l*(-cos(q[2] + q[6]) - cos(q[2] + q[6] + q[7])) - 1.0/2.0*l*cos(q[2]) + q[1]) + mthigh*(-1.0/2.0*l*cos(q[2]) - 1.0/2.0*l*cos(q[2] + q[3]) + q[1]) + mthigh*(-1.0/2.0*l*cos(q[2]) - 1.0/2.0*l*cos(q[2] + q[6]) + q[1]) + mtorso*q[1])/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[2] = (mcalf*(q[2] + q[3] + q[4]) + mcalf*(q[2] + q[6] + q[7]) + mfoot*(q[2] + q[3] + q[4] + q[5]) + mfoot*(q[2] + q[6] + q[7] + q[8]) + mthigh*(q[2] + q[3]) + mthigh*(q[2] + q[6]) + mtorso*q[2])/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	return py::array(3,out);

}

py::array fkCOMWRTFoot(std::vector<double> q,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot)
{
	 double out[3];

	 out[0] = (-1.0/2.0*l*mcalf*sin(q[0]) - mcalf*(l*sin(q[0]) + l*sin(q[0] + q[1]) + l*sin(q[0] + q[1] + q[3]) + (1.0/2.0)*l*sin(q[0] + q[1] + q[3] + q[4])) - mfoot*(l*sin(q[0]) + l*sin(q[0] + q[1]) + l*sin(q[0] + q[1] + q[3]) + l*sin(q[0] + q[1] + q[3] + q[4])) - mthigh*(l*sin(q[0]) + (1.0/2.0)*l*sin(q[0] + q[1])) - mthigh*(l*sin(q[0]) + l*sin(q[0] + q[1]) + (1.0/2.0)*l*sin(q[0] + q[1] + q[3])) - mtorso*(l*sin(q[0]) + l*sin(q[0] + q[1]) + (1.0/2.0)*l*sin(q[0] + q[1] + q[2])))/(2*mcalf + mfoot + 2*mthigh + mtorso);
	 out[1] = ((1.0/2.0)*l*mcalf*cos(q[0]) + mcalf*(l*cos(q[0]) + l*cos(q[0] + q[1]) + l*cos(q[0] + q[1] + q[3]) + (1.0/2.0)*l*cos(q[0] + q[1] + q[3] + q[4])) + mfoot*(l*cos(q[0]) + l*cos(q[0] + q[1]) + l*cos(q[0] + q[1] + q[3]) + l*cos(q[0] + q[1] + q[3] + q[4])) + mthigh*(l*cos(q[0]) + (1.0/2.0)*l*cos(q[0] + q[1])) + mthigh*(l*cos(q[0]) + l*cos(q[0] + q[1]) + (1.0/2.0)*l*cos(q[0] + q[1] + q[3])) + mtorso*(l*cos(q[0]) + l*cos(q[0] + q[1]) + (1.0/2.0)*l*cos(q[0] + q[1] + q[2])))/(2*mcalf + mfoot + 2*mthigh + mtorso);
	 out[2] = 0.0;
	return py::array(3,out);

}

py::array fkTorsoWRTFoot(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = -l*sin(q[0]) - l*sin(q[0] + q[1]) - 1.0/2.0*l*sin(q[0] + q[1] + q[2]);
	 out[1] = l*cos(q[0]) + l*cos(q[0] + q[1]) + (1.0/2.0)*l*cos(q[0] + q[1] + q[2]);
	 out[2] = q[0] + q[1] + q[2];
	return py::array(3,out);

}

py::array fkFootWRTFoot(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = -l*sin(q[0]) - l*sin(q[0] + q[1]) - l*sin(q[0] + q[1] + q[3]) - l*sin(q[0] + q[1] + q[3] + q[4]);
	 out[1] = l*cos(q[0]) + l*cos(q[0] + q[1]) + l*cos(q[0] + q[1] + q[3]) + l*cos(q[0] + q[1] + q[3] + q[4]);
	 out[2] = q[0] + q[1] + q[3] + q[4] + q[5];
	return py::array(3,out);

}

py::array fkTorso(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = q[0];
	 out[1] = q[1];
	 out[2] = q[2];
	return py::array(3,out);

}

py::array fkTorsoTop(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = (1.0/2.0)*l*sin(q[2]) + q[0];
	 out[1] = (1.0/2.0)*l*cos(q[2]) + q[1];
	 out[2] = q[2];
	return py::array(3,out);

}

py::array fkTorsoBot(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = (1.0/2.0)*l*sin(q[2]) + q[0];
	 out[1] = -1.0/2.0*l*cos(q[2]) + q[1];
	 out[2] = q[2];
	return py::array(3,out);

}

py::array fkThigh1Bot(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = (1.0/2.0)*l*sin(q[2]) + l*sin(q[2] + q[3]) + q[0];
	 out[1] = -1.0/2.0*l*cos(q[2]) - l*cos(q[2] + q[3]) + q[1];
	 out[2] = q[2] + q[3];
	return py::array(3,out);

}

py::array fkCalf1Bot(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + (1.0/2.0)*l*sin(q[2]) + q[0];
	 out[1] = l*(-cos(q[2] + q[3]) - cos(q[2] + q[3] + q[4])) - 1.0/2.0*l*cos(q[2]) + q[1];
	 out[2] = q[2] + q[3] + q[4];
	return py::array(3,out);

}

py::array fkFoot1(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + (1.0/2.0)*l*sin(q[2]) + q[0];
	 out[1] = l*(-cos(q[2] + q[3]) - cos(q[2] + q[3] + q[4])) - 1.0/2.0*l*cos(q[2]) + q[1];
	 out[2] = q[2] + q[3] + q[4] + q[5];
	return py::array(3,out);

}

py::array fkToe1(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + (1.0/2.0)*l*sin(q[2]) + (1.0/2.0)*lfoot*cos(q[2] + q[3] + q[4] + q[5]) + q[0];
	 out[1] = l*(-cos(q[2] + q[3]) - cos(q[2] + q[3] + q[4])) - 1.0/2.0*l*cos(q[2]) + (1.0/2.0)*lfoot*sin(q[2] + q[3] + q[4] + q[5]) + q[1];
	 out[2] = q[2] + q[3] + q[4] + q[5];
	return py::array(3,out);

}

py::array fkHeel1(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + (1.0/2.0)*l*sin(q[2]) - 1.0/2.0*lfoot*cos(q[2] + q[3] + q[4] + q[5]) + q[0];
	 out[1] = l*(-cos(q[2] + q[3]) - cos(q[2] + q[3] + q[4])) - 1.0/2.0*l*cos(q[2]) - 1.0/2.0*lfoot*sin(q[2] + q[3] + q[4] + q[5]) + q[1];
	 out[2] = q[2] + q[3] + q[4] + q[5];
	return py::array(3,out);

}

py::array fkThigh2Bot(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = (1.0/2.0)*l*sin(q[2]) + l*sin(q[2] + q[6]) + q[0];
	 out[1] = -1.0/2.0*l*cos(q[2]) - l*cos(q[2] + q[6]) + q[1];
	 out[2] = q[2] + q[6];
	return py::array(3,out);

}

py::array fkCalf2Bot(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = l*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + (1.0/2.0)*l*sin(q[2]) + q[0];
	 out[1] = l*(-cos(q[2] + q[6]) - cos(q[2] + q[6] + q[7])) - 1.0/2.0*l*cos(q[2]) + q[1];
	 out[2] = q[2] + q[6] + q[7];
	return py::array(3,out);

}

py::array fkFoot2(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = l*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + (1.0/2.0)*l*sin(q[2]) + q[0];
	 out[1] = l*(-cos(q[2] + q[6]) - cos(q[2] + q[6] + q[7])) - 1.0/2.0*l*cos(q[2]) + q[1];
	 out[2] = q[2] + q[6] + q[7] + q[8];
	return py::array(3,out);

}

py::array fkToe2(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = l*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + (1.0/2.0)*l*sin(q[2]) + (1.0/2.0)*lfoot*cos(q[2] + q[6] + q[7] + q[8]) + q[0];
	 out[1] = l*(-cos(q[2] + q[6]) - cos(q[2] + q[6] + q[7])) - 1.0/2.0*l*cos(q[2]) + (1.0/2.0)*lfoot*sin(q[2] + q[6] + q[7] + q[8]) + q[1];
	 out[2] = q[2] + q[6] + q[7] + q[8];
	return py::array(3,out);

}

py::array fkHeel2(std::vector<double> q,double l,double lfoot)
{
	 double out[3];

	 out[0] = l*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + (1.0/2.0)*l*sin(q[2]) - 1.0/2.0*lfoot*cos(q[2] + q[6] + q[7] + q[8]) + q[0];
	 out[1] = l*(-cos(q[2] + q[6]) - cos(q[2] + q[6] + q[7])) - 1.0/2.0*l*cos(q[2]) - 1.0/2.0*lfoot*sin(q[2] + q[6] + q[7] + q[8]) + q[1];
	 out[2] = q[2] + q[6] + q[7] + q[8];
	return py::array(3,out);

}

py::array jacCOM(std::vector<double> q,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot)
{
	 double out[27];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = (1.0/2.0)*l*(2*mcalf*cos(q[2]) + 2*mcalf*cos(q[2] + q[3]) + 2*mcalf*cos(q[2] + q[6]) + mcalf*cos(q[2] + q[3] + q[4]) + mcalf*cos(q[2] + q[6] + q[7]) + 2*mfoot*cos(q[2]) + 2*mfoot*cos(q[2] + q[3]) + 2*mfoot*cos(q[2] + q[6]) + 2*mfoot*cos(q[2] + q[3] + q[4]) + 2*mfoot*cos(q[2] + q[6] + q[7]) + 2*mthigh*cos(q[2]) + mthigh*cos(q[2] + q[3]) + mthigh*cos(q[2] + q[6]))/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[3] = (1.0/2.0)*l*(mcalf*(2*cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4])) + 2*mfoot*(cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4])) + mthigh*cos(q[2] + q[3]))/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[4] = (1.0/2.0)*l*(mcalf + 2*mfoot)*cos(q[2] + q[3] + q[4])/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[5] = 0;
	 out[6] = (1.0/2.0)*l*(mcalf*(2*cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7])) + 2*mfoot*(cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7])) + mthigh*cos(q[2] + q[6]))/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[7] = (1.0/2.0)*l*(mcalf + 2*mfoot)*cos(q[2] + q[6] + q[7])/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = (1.0/2.0)*l*(2*mcalf*sin(q[2]) + 2*mcalf*sin(q[2] + q[3]) + 2*mcalf*sin(q[2] + q[6]) + mcalf*sin(q[2] + q[3] + q[4]) + mcalf*sin(q[2] + q[6] + q[7]) + 2*mfoot*sin(q[2]) + 2*mfoot*sin(q[2] + q[3]) + 2*mfoot*sin(q[2] + q[6]) + 2*mfoot*sin(q[2] + q[3] + q[4]) + 2*mfoot*sin(q[2] + q[6] + q[7]) + 2*mthigh*sin(q[2]) + mthigh*sin(q[2] + q[3]) + mthigh*sin(q[2] + q[6]))/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[12] = (1.0/2.0)*l*(mcalf*(2*sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + 2*mfoot*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + mthigh*sin(q[2] + q[3]))/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[13] = (1.0/2.0)*l*(mcalf + 2*mfoot)*sin(q[2] + q[3] + q[4])/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[14] = 0;
	 out[15] = (1.0/2.0)*l*(mcalf*(2*sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + 2*mfoot*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + mthigh*sin(q[2] + q[6]))/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[16] = (1.0/2.0)*l*(mcalf + 2*mfoot)*sin(q[2] + q[6] + q[7])/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 1;
	 out[21] = (mcalf + mfoot + mthigh)/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[22] = (mcalf + mfoot)/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[23] = mfoot/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[24] = (mcalf + mfoot + mthigh)/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[25] = (mcalf + mfoot)/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	 out[26] = mfoot/(2*mcalf + 2*mfoot + 2*mthigh + mtorso);
	return py::array(27,out);

}

py::array jacCOMWRTFoot(std::vector<double> q,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot)
{
	 double out[27];

	 out[0] = -l*(3*mcalf*cos(q[0]) + 2*mcalf*cos(q[0] + q[1]) + 2*mcalf*cos(q[0] + q[1] + q[3]) + mcalf*cos(q[0] + q[1] + q[3] + q[4]) + 2*mfoot*cos(q[0]) + 2*mfoot*cos(q[0] + q[1]) + 2*mfoot*cos(q[0] + q[1] + q[3]) + 2*mfoot*cos(q[0] + q[1] + q[3] + q[4]) + 4*mthigh*cos(q[0]) + 3*mthigh*cos(q[0] + q[1]) + mthigh*cos(q[0] + q[1] + q[3]) + 2*mtorso*cos(q[0]) + 2*mtorso*cos(q[0] + q[1]) + mtorso*cos(q[0] + q[1] + q[2]))/(4*mcalf + 2*mfoot + 4*mthigh + 2*mtorso);
	 out[1] = -l*(mcalf*(2*cos(q[0] + q[1]) + 2*cos(q[0] + q[1] + q[3]) + cos(q[0] + q[1] + q[3] + q[4])) + 2*mfoot*(cos(q[0] + q[1]) + cos(q[0] + q[1] + q[3]) + cos(q[0] + q[1] + q[3] + q[4])) + mthigh*(2*cos(q[0] + q[1]) + cos(q[0] + q[1] + q[3])) + mthigh*cos(q[0] + q[1]) + mtorso*(2*cos(q[0] + q[1]) + cos(q[0] + q[1] + q[2])))/(4*mcalf + 2*mfoot + 4*mthigh + 2*mtorso);
	 out[2] = -l*mtorso*cos(q[0] + q[1] + q[2])/(4*mcalf + 2*mfoot + 4*mthigh + 2*mtorso);
	 out[3] = -l*(mcalf*(2*cos(q[0] + q[1] + q[3]) + cos(q[0] + q[1] + q[3] + q[4])) + 2*mfoot*(cos(q[0] + q[1] + q[3]) + cos(q[0] + q[1] + q[3] + q[4])) + mthigh*cos(q[0] + q[1] + q[3]))/(4*mcalf + 2*mfoot + 4*mthigh + 2*mtorso);
	 out[4] = -l*(mcalf + 2*mfoot)*cos(q[0] + q[1] + q[3] + q[4])/(4*mcalf + 2*mfoot + 4*mthigh + 2*mtorso);
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 0;
	 out[9] = -l*(3*mcalf*sin(q[0]) + 2*mcalf*sin(q[0] + q[1]) + 2*mcalf*sin(q[0] + q[1] + q[3]) + mcalf*sin(q[0] + q[1] + q[3] + q[4]) + 2*mfoot*sin(q[0]) + 2*mfoot*sin(q[0] + q[1]) + 2*mfoot*sin(q[0] + q[1] + q[3]) + 2*mfoot*sin(q[0] + q[1] + q[3] + q[4]) + 4*mthigh*sin(q[0]) + 3*mthigh*sin(q[0] + q[1]) + mthigh*sin(q[0] + q[1] + q[3]) + 2*mtorso*sin(q[0]) + 2*mtorso*sin(q[0] + q[1]) + mtorso*sin(q[0] + q[1] + q[2]))/(4*mcalf + 2*mfoot + 4*mthigh + 2*mtorso);
	 out[10] = -l*(2*mcalf*sin(q[0] + q[1]) + 2*mcalf*sin(q[0] + q[1] + q[3]) + mcalf*sin(q[0] + q[1] + q[3] + q[4]) + 2*mfoot*sin(q[0] + q[1]) + 2*mfoot*sin(q[0] + q[1] + q[3]) + 2*mfoot*sin(q[0] + q[1] + q[3] + q[4]) + 3*mthigh*sin(q[0] + q[1]) + mthigh*sin(q[0] + q[1] + q[3]) + 2*mtorso*sin(q[0] + q[1]) + mtorso*sin(q[0] + q[1] + q[2]))/(4*mcalf + 2*mfoot + 4*mthigh + 2*mtorso);
	 out[11] = -l*mtorso*sin(q[0] + q[1] + q[2])/(4*mcalf + 2*mfoot + 4*mthigh + 2*mtorso);
	 out[12] = -l*(2*mcalf*sin(q[0] + q[1] + q[3]) + mcalf*sin(q[0] + q[1] + q[3] + q[4]) + 2*mfoot*sin(q[0] + q[1] + q[3]) + 2*mfoot*sin(q[0] + q[1] + q[3] + q[4]) + mthigh*sin(q[0] + q[1] + q[3]))/(4*mcalf + 2*mfoot + 4*mthigh + 2*mtorso);
	 out[13] = -l*(mcalf + 2*mfoot)*sin(q[0] + q[1] + q[3] + q[4])/(4*mcalf + 2*mfoot + 4*mthigh + 2*mtorso);
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 0;
	 out[21] = 0;
	 out[22] = 0;
	 out[23] = 0;
	 out[24] = 0;
	 out[25] = 0;
	 out[26] = 0;
	return py::array(27,out);

}

py::array jacTorsoWRTFoot(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = -1.0/2.0*l*(2*cos(q[0]) + 2*cos(q[0] + q[1]) + cos(q[0] + q[1] + q[2]));
	 out[1] = -1.0/2.0*l*(2*cos(q[0] + q[1]) + cos(q[0] + q[1] + q[2]));
	 out[2] = -1.0/2.0*l*cos(q[0] + q[1] + q[2]);
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 0;
	 out[9] = -1.0/2.0*l*(2*sin(q[0]) + 2*sin(q[0] + q[1]) + sin(q[0] + q[1] + q[2]));
	 out[10] = -1.0/2.0*l*(2*sin(q[0] + q[1]) + sin(q[0] + q[1] + q[2]));
	 out[11] = -1.0/2.0*l*sin(q[0] + q[1] + q[2]);
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 1;
	 out[19] = 1;
	 out[20] = 1;
	 out[21] = 0;
	 out[22] = 0;
	 out[23] = 0;
	 out[24] = 0;
	 out[25] = 0;
	 out[26] = 0;
	return py::array(27,out);

}

py::array jacFootWRTFoot(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = -l*(cos(q[0]) + cos(q[0] + q[1]) + cos(q[0] + q[1] + q[3]) + cos(q[0] + q[1] + q[3] + q[4]));
	 out[1] = -l*(cos(q[0] + q[1]) + cos(q[0] + q[1] + q[3]) + cos(q[0] + q[1] + q[3] + q[4]));
	 out[2] = 0;
	 out[3] = -l*(cos(q[0] + q[1] + q[3]) + cos(q[0] + q[1] + q[3] + q[4]));
	 out[4] = -l*cos(q[0] + q[1] + q[3] + q[4]);
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 0;
	 out[9] = -l*(sin(q[0]) + sin(q[0] + q[1]) + sin(q[0] + q[1] + q[3]) + sin(q[0] + q[1] + q[3] + q[4]));
	 out[10] = -l*(sin(q[0] + q[1]) + sin(q[0] + q[1] + q[3]) + sin(q[0] + q[1] + q[3] + q[4]));
	 out[11] = 0;
	 out[12] = -l*(sin(q[0] + q[1] + q[3]) + sin(q[0] + q[1] + q[3] + q[4]));
	 out[13] = -l*sin(q[0] + q[1] + q[3] + q[4]);
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 1;
	 out[19] = 1;
	 out[20] = 0;
	 out[21] = 1;
	 out[22] = 1;
	 out[23] = 1;
	 out[24] = 0;
	 out[25] = 0;
	 out[26] = 0;
	return py::array(27,out);

}

py::array jacTorso(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = 0;
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = 0;
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 1;
	 out[21] = 0;
	 out[22] = 0;
	 out[23] = 0;
	 out[24] = 0;
	 out[25] = 0;
	 out[26] = 0;
	return py::array(27,out);

}

py::array jacTorsoWRTFoot1(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 0;
	 out[1] = 0;
	 out[2] = -l*((1.0/2.0)*cos(q[2]) + cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4]));
	 out[3] = -l*(cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4]));
	 out[4] = -l*cos(q[2] + q[3] + q[4]);
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 0;
	 out[11] = -l*((1.0/2.0)*sin(q[2]) + sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4]));
	 out[12] = -l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4]));
	 out[13] = -l*sin(q[2] + q[3] + q[4]);
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 0;
	 out[21] = -1;
	 out[22] = -1;
	 out[23] = -1;
	 out[24] = 0;
	 out[25] = 0;
	 out[26] = 0;
	return py::array(27,out);

}

py::array jacTorsoWRTFoot2(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 0;
	 out[1] = 0;
	 out[2] = -l*((1.0/2.0)*cos(q[2]) + cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7]));
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = -l*(cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7]));
	 out[7] = -l*cos(q[2] + q[6] + q[7]);
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 0;
	 out[11] = -l*((1.0/2.0)*sin(q[2]) + sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7]));
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = -l*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7]));
	 out[16] = -l*sin(q[2] + q[6] + q[7]);
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 0;
	 out[21] = 0;
	 out[22] = 0;
	 out[23] = 0;
	 out[24] = -1;
	 out[25] = -1;
	 out[26] = -1;
	return py::array(27,out);

}

py::array jacTorsoTop(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = (1.0/2.0)*l*cos(q[2]);
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = -1.0/2.0*l*sin(q[2]);
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 1;
	 out[21] = 0;
	 out[22] = 0;
	 out[23] = 0;
	 out[24] = 0;
	 out[25] = 0;
	 out[26] = 0;
	return py::array(27,out);

}

py::array jacTorsoBot(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = (1.0/2.0)*l*cos(q[2]);
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = (1.0/2.0)*l*sin(q[2]);
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 1;
	 out[21] = 0;
	 out[22] = 0;
	 out[23] = 0;
	 out[24] = 0;
	 out[25] = 0;
	 out[26] = 0;
	return py::array(27,out);

}

py::array jacThigh1Bot(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = (1.0/2.0)*l*(cos(q[2]) + 2*cos(q[2] + q[3]));
	 out[3] = l*cos(q[2] + q[3]);
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = (1.0/2.0)*l*(sin(q[2]) + 2*sin(q[2] + q[3]));
	 out[12] = l*sin(q[2] + q[3]);
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 1;
	 out[21] = 1;
	 out[22] = 0;
	 out[23] = 0;
	 out[24] = 0;
	 out[25] = 0;
	 out[26] = 0;
	return py::array(27,out);

}

py::array jacCalf1Bot(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = l*((1.0/2.0)*cos(q[2]) + cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4]));
	 out[3] = l*(cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4]));
	 out[4] = l*cos(q[2] + q[3] + q[4]);
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = l*((1.0/2.0)*sin(q[2]) + sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4]));
	 out[12] = l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4]));
	 out[13] = l*sin(q[2] + q[3] + q[4]);
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 1;
	 out[21] = 1;
	 out[22] = 1;
	 out[23] = 0;
	 out[24] = 0;
	 out[25] = 0;
	 out[26] = 0;
	return py::array(27,out);

}

py::array jacFoot1(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = l*((1.0/2.0)*cos(q[2]) + cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4]));
	 out[3] = l*(cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4]));
	 out[4] = l*cos(q[2] + q[3] + q[4]);
	 out[5] = 0;
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = l*((1.0/2.0)*sin(q[2]) + sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4]));
	 out[12] = l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4]));
	 out[13] = l*sin(q[2] + q[3] + q[4]);
	 out[14] = 0;
	 out[15] = 0;
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 1;
	 out[21] = 1;
	 out[22] = 1;
	 out[23] = 1;
	 out[24] = 0;
	 out[25] = 0;
	 out[26] = 0;
	return py::array(27,out);

}

py::array jacToe1(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = l*(cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4])) + (1.0/2.0)*l*cos(q[2]) - 1.0/2.0*lfoot*sin(q[2] + q[3] + q[4] + q[5]);
	 out[3] = l*(cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4])) - 1.0/2.0*lfoot*sin(q[2] + q[3] + q[4] + q[5]);
	 out[4] = l*cos(q[2] + q[3] + q[4]) - 1.0/2.0*lfoot*sin(q[2] + q[3] + q[4] + q[5]);
	 out[5] = -1.0/2.0*lfoot*sin(q[2] + q[3] + q[4] + q[5]);
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + (1.0/2.0)*l*sin(q[2]) + (1.0/2.0)*lfoot*cos(q[2] + q[3] + q[4] + q[5]);
	 out[12] = l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + (1.0/2.0)*lfoot*cos(q[2] + q[3] + q[4] + q[5]);
	 out[13] = l*sin(q[2] + q[3] + q[4]) + (1.0/2.0)*lfoot*cos(q[2] + q[3] + q[4] + q[5]);
	 out[14] = (1.0/2.0)*lfoot*cos(q[2] + q[3] + q[4] + q[5]);
	 out[15] = 0;
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 1;
	 out[21] = 1;
	 out[22] = 1;
	 out[23] = 1;
	 out[24] = 0;
	 out[25] = 0;
	 out[26] = 0;
	return py::array(27,out);

}

py::array jacHeel1(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = l*(cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4])) + (1.0/2.0)*l*cos(q[2]) + (1.0/2.0)*lfoot*sin(q[2] + q[3] + q[4] + q[5]);
	 out[3] = l*(cos(q[2] + q[3]) + cos(q[2] + q[3] + q[4])) + (1.0/2.0)*lfoot*sin(q[2] + q[3] + q[4] + q[5]);
	 out[4] = l*cos(q[2] + q[3] + q[4]) + (1.0/2.0)*lfoot*sin(q[2] + q[3] + q[4] + q[5]);
	 out[5] = (1.0/2.0)*lfoot*sin(q[2] + q[3] + q[4] + q[5]);
	 out[6] = 0;
	 out[7] = 0;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) + (1.0/2.0)*l*sin(q[2]) - 1.0/2.0*lfoot*cos(q[2] + q[3] + q[4] + q[5]);
	 out[12] = l*(sin(q[2] + q[3]) + sin(q[2] + q[3] + q[4])) - 1.0/2.0*lfoot*cos(q[2] + q[3] + q[4] + q[5]);
	 out[13] = l*sin(q[2] + q[3] + q[4]) - 1.0/2.0*lfoot*cos(q[2] + q[3] + q[4] + q[5]);
	 out[14] = -1.0/2.0*lfoot*cos(q[2] + q[3] + q[4] + q[5]);
	 out[15] = 0;
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 1;
	 out[21] = 1;
	 out[22] = 1;
	 out[23] = 1;
	 out[24] = 0;
	 out[25] = 0;
	 out[26] = 0;
	return py::array(27,out);

}

py::array jacThigh2Bot(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = (1.0/2.0)*l*(cos(q[2]) + 2*cos(q[2] + q[6]));
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = l*cos(q[2] + q[6]);
	 out[7] = 0;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = (1.0/2.0)*l*(sin(q[2]) + 2*sin(q[2] + q[6]));
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = l*sin(q[2] + q[6]);
	 out[16] = 0;
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 1;
	 out[21] = 0;
	 out[22] = 0;
	 out[23] = 0;
	 out[24] = 1;
	 out[25] = 0;
	 out[26] = 0;
	return py::array(27,out);

}

py::array jacCalf2Bot(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = l*((1.0/2.0)*cos(q[2]) + cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7]));
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = l*(cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7]));
	 out[7] = l*cos(q[2] + q[6] + q[7]);
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = l*((1.0/2.0)*sin(q[2]) + sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7]));
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = l*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7]));
	 out[16] = l*sin(q[2] + q[6] + q[7]);
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 1;
	 out[21] = 0;
	 out[22] = 0;
	 out[23] = 0;
	 out[24] = 1;
	 out[25] = 1;
	 out[26] = 0;
	return py::array(27,out);

}

py::array jacFoot2(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = l*((1.0/2.0)*cos(q[2]) + cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7]));
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = l*(cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7]));
	 out[7] = l*cos(q[2] + q[6] + q[7]);
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = l*((1.0/2.0)*sin(q[2]) + sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7]));
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = l*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7]));
	 out[16] = l*sin(q[2] + q[6] + q[7]);
	 out[17] = 0;
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 1;
	 out[21] = 0;
	 out[22] = 0;
	 out[23] = 0;
	 out[24] = 1;
	 out[25] = 1;
	 out[26] = 1;
	return py::array(27,out);

}

py::array jacToe2(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = l*(cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7])) + (1.0/2.0)*l*cos(q[2]) - 1.0/2.0*lfoot*sin(q[2] + q[6] + q[7] + q[8]);
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = l*(cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7])) - 1.0/2.0*lfoot*sin(q[2] + q[6] + q[7] + q[8]);
	 out[7] = l*cos(q[2] + q[6] + q[7]) - 1.0/2.0*lfoot*sin(q[2] + q[6] + q[7] + q[8]);
	 out[8] = -1.0/2.0*lfoot*sin(q[2] + q[6] + q[7] + q[8]);
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = l*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + (1.0/2.0)*l*sin(q[2]) + (1.0/2.0)*lfoot*cos(q[2] + q[6] + q[7] + q[8]);
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = l*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + (1.0/2.0)*lfoot*cos(q[2] + q[6] + q[7] + q[8]);
	 out[16] = l*sin(q[2] + q[6] + q[7]) + (1.0/2.0)*lfoot*cos(q[2] + q[6] + q[7] + q[8]);
	 out[17] = (1.0/2.0)*lfoot*cos(q[2] + q[6] + q[7] + q[8]);
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 1;
	 out[21] = 0;
	 out[22] = 0;
	 out[23] = 0;
	 out[24] = 1;
	 out[25] = 1;
	 out[26] = 1;
	return py::array(27,out);

}

py::array jacHeel2(std::vector<double> q,double l,double lfoot)
{
	 double out[27];

	 out[0] = 1;
	 out[1] = 0;
	 out[2] = l*(cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7])) + (1.0/2.0)*l*cos(q[2]) + (1.0/2.0)*lfoot*sin(q[2] + q[6] + q[7] + q[8]);
	 out[3] = 0;
	 out[4] = 0;
	 out[5] = 0;
	 out[6] = l*(cos(q[2] + q[6]) + cos(q[2] + q[6] + q[7])) + (1.0/2.0)*lfoot*sin(q[2] + q[6] + q[7] + q[8]);
	 out[7] = l*cos(q[2] + q[6] + q[7]) + (1.0/2.0)*lfoot*sin(q[2] + q[6] + q[7] + q[8]);
	 out[8] = (1.0/2.0)*lfoot*sin(q[2] + q[6] + q[7] + q[8]);
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = l*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) + (1.0/2.0)*l*sin(q[2]) - 1.0/2.0*lfoot*cos(q[2] + q[6] + q[7] + q[8]);
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = l*(sin(q[2] + q[6]) + sin(q[2] + q[6] + q[7])) - 1.0/2.0*lfoot*cos(q[2] + q[6] + q[7] + q[8]);
	 out[16] = l*sin(q[2] + q[6] + q[7]) - 1.0/2.0*lfoot*cos(q[2] + q[6] + q[7] + q[8]);
	 out[17] = -1.0/2.0*lfoot*cos(q[2] + q[6] + q[7] + q[8]);
	 out[18] = 0;
	 out[19] = 0;
	 out[20] = 1;
	 out[21] = 0;
	 out[22] = 0;
	 out[23] = 0;
	 out[24] = 1;
	 out[25] = 1;
	 out[26] = 1;
	return py::array(27,out);

}

PYBIND11_MODULE(anklekneewalkerdynamics,m) {
  m.def("calc_M",&calc_M,"a function description");
  m.def("calc_grav",&calc_grav,"another function description");
  m.def("calc_c",&calc_c,"yet another function description");
  m.def("fkCOM",&fkCOM,"a function description");
  m.def("fkCOMWRTFoot",&fkCOMWRTFoot,"a function description");
  m.def("fkTorsoWRTFoot",&fkTorsoWRTFoot,"a function description");
  m.def("fkFootWRTFoot",&fkFootWRTFoot,"a function description");
  m.def("fkTorso",&fkTorso,"a function description");
  m.def("fkTorsoTop",&fkTorsoTop,"a function description");
  m.def("fkTorsoBot",&fkTorsoBot,"a function description");
  m.def("fkThigh1Bot",&fkThigh1Bot,"a function description");
  m.def("fkCalf1Bot",&fkCalf1Bot,"a function description");
  m.def("fkFoot1",&fkFoot1,"a function description");
  m.def("fkToe1",&fkToe1,"a function description");
  m.def("fkHeel1",&fkHeel1,"a function description");
  m.def("fkThigh2Bot",&fkThigh2Bot,"a function description");
  m.def("fkCalf2Bot",&fkCalf2Bot,"a function description");
  m.def("fkFoot2",&fkFoot2,"a function description");
  m.def("fkToe2",&fkToe2,"a function description");
  m.def("fkHeel2",&fkHeel2,"a function description");
  m.def("jacCOM",&jacCOM,"a function description");
  m.def("jacCOMWRTFoot",&jacCOMWRTFoot,"a function description");
  m.def("jacTorsoWRTFoot",&jacTorsoWRTFoot,"a function description");
  m.def("jacFootWRTFoot",&jacFootWRTFoot,"a function description");
  m.def("jacTorso",&jacTorso,"a function description");
  m.def("jacTorsoWRTFoot1",&jacTorsoWRTFoot1,"a function description");
  m.def("jacTorsoWRTFoot2",&jacTorsoWRTFoot2,"a function description");
  m.def("jacTorsoTop",&jacTorsoTop,"a function description");
  m.def("jacTorsoBot",&jacTorsoBot,"a function description");
  m.def("jacThigh1Bot",&jacThigh1Bot,"a function description");
  m.def("jacCalf1Bot",&jacCalf1Bot,"a function description");
  m.def("jacFoot1",&jacFoot1,"a function description");
  m.def("jacToe1",&jacToe1,"a function description");
  m.def("jacHeel1",&jacHeel1,"a function description");
  m.def("jacThigh2Bot",&jacThigh2Bot,"a function description");
  m.def("jacCalf2Bot",&jacCalf2Bot,"a function description");
  m.def("jacFoot2",&jacFoot2,"a function description");
  m.def("jacToe2",&jacToe2,"a function description");
  m.def("jacHeel2",&jacHeel2,"a function description");
}
