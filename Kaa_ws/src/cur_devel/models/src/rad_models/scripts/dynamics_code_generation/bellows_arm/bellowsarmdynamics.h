#include <math.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

py::array calc_grav(std::vector<double> q, double jtHeight,double jt0Mass,double jt0MassPos,double l0Mass,double l0MassPos, double jt1Mass,double jt1MassPos,double l1Mass,double l1MassPos, double jt2Mass,double jt2MassPos, double g);

py::array calc_M(std::vector<double> q, double jtHeight,double jt0Mass,double jt0MassPos,double l0Mass,double l0MassPos, double jt1Mass,double jt1MassPos,double l1Mass,double l1MassPos, double jt2Mass,double jt2MassPos, double jt0Ixx, double jt0Iyy, double jt0Izz, double jt0Ixy, double jt0Ixz, double jt0Iyz, double l0Ixx, double l0Iyy, double l0Izz, double l0Ixy, double l0Ixz, double l0Iyz, double jt1Ixx, double jt1Iyy, double jt1Izz, double jt1Ixy, double jt1Ixz, double jt1Iyz, double l1Ixx, double l1Iyy, double l1Izz, double l1Ixy, double l1Ixz, double l1Iyz, double jt2Ixx, double jt2Iyy, double jt2Izz, double jt2Ixy, double jt2Ixz, double jt2Iyz, double g);

/* py::array fkJt0COM(std::vector<double> q); */
py::array fkJt0End(std::vector<double> q, double jtHeight);
py::array fkL0End(std::vector<double> q, double jtHeight);
/* py::array fkJt1COM(std::vector<double> q); */
py::array fkJt1End(std::vector<double> q, double jtHeight);
py::array fkL1End(std::vector<double> q, double jtHeight);
/* py::array fkJt2COM(std::vector<double> q); */
py::array fkJt2End(std::vector<double> q, double jtHeight);
