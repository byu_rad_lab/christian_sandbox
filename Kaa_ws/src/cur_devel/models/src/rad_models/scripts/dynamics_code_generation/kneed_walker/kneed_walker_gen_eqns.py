import sympy as sp
import numpy as np
sp.init_printing(use_unicode=True)

t = sp.symbols('t')

q0 = sp.Function('q0')(t)
q1 = sp.Function('q1')(t)
q2 = sp.Function('q2')(t)
q3 = sp.Function('q3')(t)
q4 = sp.Function('q4')(t)
q5 = sp.Function('q5')(t)
q6 = sp.Function('q6')(t)

l,Itorso,Ithigh,Icalf,mtorso,mthigh,mcalf,g = sp.symbols('l Itorso Ithigh Icalf mtorso mthigh mcalf g',real=True)

r_A_o = sp.Matrix([[q0],
                   [q1]])

r_B_o = sp.Matrix([[q0 + l/2*(sp.sin(q2) + sp.sin(q2+q3))],
                   [q1 - l/2*(sp.cos(q2) + sp.cos(q2+q3))]])

r_C_o = sp.Matrix([[q0 + l/2*(sp.sin(q2) + sp.sin(q2+q3+q4)) + l*sp.sin(q2+q3)],
                   [q1 - l/2*(sp.cos(q2) + sp.cos(q2+q3+q4)) - l*sp.cos(q2+q3)]])

r_D_o = sp.Matrix([[q0 + l/2*(sp.sin(q2) + sp.sin(q2+q5))],
                   [q1 - l/2*(sp.cos(q2) + sp.cos(q2+q5))]])

r_E_o = sp.Matrix([[q0 + l/2*(sp.sin(q2) + sp.sin(q2+q5+q6)) + l*sp.sin(q2+q5)],
                   [q1 - l/2*(sp.cos(q2) + sp.cos(q2+q5+q6)) - l*sp.cos(q2+q5)]])

rdot_A_o = sp.diff(r_A_o,t)
rdot_B_o = sp.diff(r_B_o,t)
rdot_C_o = sp.diff(r_C_o,t)
rdot_D_o = sp.diff(r_D_o,t)
rdot_E_o = sp.diff(r_E_o,t)

###---------------------Kinematics---------------------------###
# fki is the forward kinematics to the end of the i'th link
fk0 = sp.Matrix([[q0],
                 [0],
                 [0]])

fk1 = sp.Matrix([[q0],
                 [q1],
                 [0]])

fkhead = sp.Matrix([[q0 + l/2*sp.sin(q2)],
                    [q1 + l/2*sp.cos(q2)],
                    [q2]])

fk2 = sp.Matrix([[q0 + l/2*sp.sin(q2)],
                 [q1 - l/2*sp.cos(q2)],
                 [q2]])

fk3 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*sp.sin(q2+q3)],
                 [q1 - l/2*sp.cos(q2) - l*sp.cos(q2+q3)],
                 [q2+q3]])

fk4 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*(sp.sin(q2+q3+q4) + sp.sin(q2+q3))],
                 [q1 - l/2*sp.cos(q2) + l*(-sp.cos(q2+q3+q4) - sp.cos(q2+q3))],
                 [q2+q3+q4]])

fk5 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*sp.sin(q2+q5)],
                 [q1 - l/2*sp.cos(q2) - l*sp.cos(q2+q5)],
                 [q2+q5]])

fk6 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*(sp.sin(q2+q5+q6) + sp.sin(q2+q5))],
                 [q1 - l/2*sp.cos(q2) + l*(-sp.cos(q2+q5+q6) - sp.cos(q2+q5))],
                 [q2+q5+q6]])

def get_jac(xdoti):
    return sp.Matrix.hstack(sp.diff(xdoti,sp.Derivative(q0,t)),
                            sp.diff(xdoti,sp.Derivative(q1,t)),
                            sp.diff(xdoti,sp.Derivative(q2,t)),
                            sp.diff(xdoti,sp.Derivative(q3,t)),
                            sp.diff(xdoti,sp.Derivative(q4,t)),
                            sp.diff(xdoti,sp.Derivative(q5,t)),                  
                            sp.diff(xdoti,sp.Derivative(q6,t)))


xdot0 = sp.diff(fk0,t)
jac0 = get_jac(xdot0)

xdot1 = sp.diff(fk1,t)
jac1 = get_jac(xdot1)

xdothead = sp.diff(fkhead,t)
jachead = get_jac(xdothead)

xdot2 = sp.diff(fk2,t)
jac2 = get_jac(xdot2)

xdot3 = sp.diff(fk3,t)
jac3 = get_jac(xdot3)

xdot4 = sp.diff(fk4,t)
jac4 = get_jac(xdot4)

xdot5 = sp.diff(fk5,t)
jac5 = get_jac(xdot5)

xdot6 = sp.diff(fk6,t)
jac6 = get_jac(xdot6)


###---------------------Dynamics---------------------------###

qd2 = sp.Matrix([sp.Derivative(q2,t)])
qd3 = sp.Matrix([sp.Derivative(q3,t)])
qd4 = sp.Matrix([sp.Derivative(q4,t)])
qd5 = sp.Matrix([sp.Derivative(q5,t)])
qd6 = sp.Matrix([sp.Derivative(q6,t)])

T = 0.5*mtorso*rdot_A_o.T*rdot_A_o + 0.5*mthigh*rdot_B_o.T*rdot_B_o + 0.5*mcalf*rdot_C_o.T*rdot_C_o + 0.5*mthigh*rdot_D_o.T*rdot_D_o + 0.5*mcalf*rdot_E_o.T*rdot_E_o + 0.5*qd2.T*Itorso*qd2 + 0.5*(qd2+qd3).T*Ithigh*(qd2+qd3) + 0.5*(qd2+qd3+qd4).T*Icalf*(qd2+qd3+qd4) + 0.5*(qd2+qd5).T*Ithigh*(qd2+qd5) + 0.5*(qd2+qd5+qd6).T*Icalf*(qd2+qd5+qd6)

gvec = sp.Matrix([[0],
                  [1]])

V = g*(r_A_o.T*gvec*mtorso + r_B_o.T*gvec*mthigh + r_C_o.T*gvec*mcalf + r_D_o.T*gvec*mthigh + r_E_o.T*gvec*mcalf)

L = T-V

dL_dqdot = sp.Matrix([[sp.diff(L,sp.Derivative(q0,t))],
                      [sp.diff(L,sp.Derivative(q1,t))],
                      [sp.diff(L,sp.Derivative(q2,t))],
                      [sp.diff(L,sp.Derivative(q3,t))],
                      [sp.diff(L,sp.Derivative(q4,t))],
                      [sp.diff(L,sp.Derivative(q5,t))],
                      [sp.diff(L,sp.Derivative(q6,t))]])

dL_dq = sp.Matrix([[sp.diff(L,q0)],
                   [sp.diff(L,q1)],
                   [sp.diff(L,q2)],
                   [sp.diff(L,q3)],
                   [sp.diff(L,q4)],
                   [sp.diff(L,q5)],
                   [sp.diff(L,q6)]])

print("calculating EoM...")
EoM = sp.diff(dL_dqdot,t) - dL_dq

# Find the Matrices
q = sp.Matrix([[q0],[q1],[q2],[q3],[q4],[q5],[q6]])
qdot = sp.diff(q,t)
qddot = sp.diff(qdot,t)

print("calculating M...")
M = sp.simplify(sp.Matrix.hstack(sp.diff(EoM,sp.Derivative(q0,t,t)),
                                 sp.diff(EoM,sp.Derivative(q1,t,t)),
                                 sp.diff(EoM,sp.Derivative(q2,t,t)),
                                 sp.diff(EoM,sp.Derivative(q3,t,t)),
                                 sp.diff(EoM,sp.Derivative(q4,t,t)),
                                 sp.diff(EoM,sp.Derivative(q5,t,t)),
                                 sp.diff(EoM,sp.Derivative(q6,t,t))))

print("calculating grav...")
grav = sp.simplify(sp.diff(EoM,g)*g)

print("calculating c...")
c = sp.simplify(EoM - grav - M*sp.Matrix([[sp.Derivative(q0,t,t)],
                                          [sp.Derivative(q1,t,t)],
                                          [sp.Derivative(q2,t,t)],
                                          [sp.Derivative(q3,t,t)],
                                          [sp.Derivative(q4,t,t)],
                                          [sp.Derivative(q5,t,t)],
                                          [sp.Derivative(q6,t,t)]]))

# Get rid of time dependence within sympy
def dummify_undefined_functions(expr):
    mapping = {}    

    # replace all Derivative terms
    for der in expr.atoms(sp.Derivative):
        f_name = der.expr.func.__name__
        var_names = [var.name for var in der.variables]
        name = "qd%s" % (f_name[1])
        mapping[der] = sp.Symbol(name)

    for der in expr.atoms(q0,q1,q2,q3,q4,q5,q6):
        f_name = der.func.__name__
        name = f_name
        mapping[der] = sp.Symbol(name)
        
    return expr.subs(mapping)

M = dummify_undefined_functions(M)
grav = dummify_undefined_functions(grav)
c = dummify_undefined_functions(c)

fk0 = dummify_undefined_functions(fk0)
fk1 = dummify_undefined_functions(fk1)
fkhead = dummify_undefined_functions(fkhead)
fk2 = dummify_undefined_functions(fk2)
fk3 = dummify_undefined_functions(fk3)
fk4 = dummify_undefined_functions(fk4)
fk5 = dummify_undefined_functions(fk5)
fk6 = dummify_undefined_functions(fk6)
jac0 = dummify_undefined_functions(jac0)
jac1 = dummify_undefined_functions(jac1)
jac2 = dummify_undefined_functions(jac2)
jac3 = dummify_undefined_functions(jac3)
jac4 = dummify_undefined_functions(jac4)
jac5 = dummify_undefined_functions(jac5)
jac6 = dummify_undefined_functions(jac6)

print("Generating C code...")
from sympy.utilities.codegen import codegen

filename = "dynamics2"
c_code = ''

c_code+='#include "test.h"\n#include <math.h>\n\nnamespace py = pybind11;\n\n'

c_code+="py::array calc_M(std::vector<double> q,double l,double mtorso,double mthigh,double mcalf,double Itorso,double Ithigh,double Icalf)\n{"
c_code+="\n\t double out[49];\n"
for i in range(0,7):
    for j in range(0,7):
        c_code += "\n\t out["+str(i*7+j)+"] = "+sp.ccode(M[i,j].as_expr()[0])+";"
c_code+="\n\treturn py::array(49,out);\n"        
c_code+="\n}\n\n"

c_code+="py::array calc_grav(std::vector<double> q,double l,double mtorso,double mthigh,double mcalf,double g)\n{"
c_code+="\n\t double out[7];\n"
for i in range(0,7):
    c_code += "\n\t out["+str(i)+"] = "+sp.ccode(grav[i].as_expr()[0])+";"
c_code+="\n\treturn py::array(7,out);\n"
c_code+="\n}\n\n"

c_code+="py::array calc_c(std::vector<double> q,std::vector<double> qd,double l,double mtorso,double mthigh,double mcalf)\n{"
c_code+="\n\t double out[7];\n"
for i in range(0,7):
    c_code += "\n\t out["+str(i)+"] = "+sp.ccode(c[i].as_expr()[0])+";"
c_code+="\n\treturn py::array(7,out);\n"
c_code+="\n}\n\n"

def print_fk_function(code,fk,link):
    code+="py::array fk"+str(link)+"(std::vector<double> q,double l)\n{"
    code+="\n\t double out[3];\n"
    for i in range(0,3):
        code += "\n\t out["+str(i)+"] = "+sp.ccode(fk[i].as_expr())+";"
    code+="\n\treturn py::array(3,out);\n"
    code+="\n}\n\n"
    return code

c_code = print_fk_function(c_code,fk0,0)
c_code = print_fk_function(c_code,fk1,1)
c_code = print_fk_function(c_code,fkhead,'head')
c_code = print_fk_function(c_code,fk2,2)
c_code = print_fk_function(c_code,fk3,3)
c_code = print_fk_function(c_code,fk4,4)
c_code = print_fk_function(c_code,fk5,5)
c_code = print_fk_function(c_code,fk6,6)

def print_jac_function(code,jac,link):
    code+="py::array jac"+str(link)+"(std::vector<double> q,double l)\n{"
    code+="\n\t double out[21];\n"
    for i in range(0,3):
        for j in range(0,7):
            code += "\n\t out["+str(i*7+j)+"] = "+sp.ccode(jac[i,j].as_expr())+";"
    code+="\n\treturn py::array(21,out);\n"
    code+="\n}\n\n"
    return code

c_code = print_jac_function(c_code,jac0,0)
c_code = print_jac_function(c_code,jac1,1)
c_code = print_jac_function(c_code,jachead,'head')
c_code = print_jac_function(c_code,jac2,2)
c_code = print_jac_function(c_code,jac3,3)
c_code = print_jac_function(c_code,jac4,4)
c_code = print_jac_function(c_code,jac5,5)
c_code = print_jac_function(c_code,jac6,6)

for i in range(0,7):
    while c_code.find('qd'+str(i)) != -1:
        c_code = c_code.replace('qd'+str(i),'qd['+str(i)+']')
    while c_code.find('q'+str(i)) != -1:
        c_code = c_code.replace('q'+str(i),'q['+str(i)+']')
    
cf = open(filename+".c",'w')

cf.write(c_code)
cf.close()
