#include <math.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

py::array calc_M(std::vector<double> q,double l,double m,double I);
py::array calc_grav(std::vector<double> q,double l,double m,double g);
py::array calc_C(std::vector<double> q,std::vector<double> qd,double l,double m,double I);
py::array fkCoM0(std::vector<double> q,double l);
py::array fkCoM1(std::vector<double> q,double l);
py::array fkCoM2(std::vector<double> q,double l);

