#include <math.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

py::array calc_M(std::vector<double> q,double l,double m,double I);
py::array calc_grav(std::vector<double> q,double l,double m,double g);
py::array calc_c(std::vector<double> q,std::vector<double> qd,double l,double m);
py::array fk0(std::vector<double> q,double l);
py::array fk1(std::vector<double> q,double l);
py::array fk2(std::vector<double> q,double l);
py::array jac0(std::vector<double> q,double l);
py::array jac1(std::vector<double> q,double l);
py::array jac2(std::vector<double> q,double l);

