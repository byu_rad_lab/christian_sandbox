import sympy as sp
import numpy as np
sp.init_printing(use_unicode=True)

t = sp.symbols('t')

q0 = sp.Function('q0')(t)
q1 = sp.Function('q1')(t)
q2 = sp.Function('q2')(t)
q3 = sp.Function('q3')(t)
q4 = sp.Function('q4')(t)
q5 = sp.Function('q5')(t)
q6 = sp.Function('q6')(t)
q7 = sp.Function('q7')(t)
q8 = sp.Function('q8')(t)

n = 9

l,lfoot,Itorso,Ithigh,Icalf,Ifoot,mtorso,mthigh,mcalf,mfoot,g = sp.symbols('l lfoot Itorso Ithigh Icalf Ifoot mtorso mthigh mcalf mfoot g',real=True)

r_torso = sp.Matrix([[q0],
                     [q1]])

r_thigh1 = sp.Matrix([[q0 + l/2*(sp.sin(q2) + sp.sin(q2+q3))],
                      [q1 - l/2*(sp.cos(q2) + sp.cos(q2+q3))]])

r_calf1 = sp.Matrix([[q0 + l/2*(sp.sin(q2) + sp.sin(q2+q3+q4)) + l*sp.sin(q2+q3)],
                     [q1 - l/2*(sp.cos(q2) + sp.cos(q2+q3+q4)) - l*sp.cos(q2+q3)]])

r_foot1 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*sp.sin(q2+q3+q4) + l*sp.sin(q2+q3)],
                     [q1 - l/2*sp.cos(q2) - l*sp.cos(q2+q3+q4) - l*sp.cos(q2+q3)]])

r_thigh2 = sp.Matrix([[q0 + l/2*(sp.sin(q2) + sp.sin(q2+q6))],
                      [q1 - l/2*(sp.cos(q2) + sp.cos(q2+q6))]])

r_calf2 = sp.Matrix([[q0 + l/2*(sp.sin(q2) + sp.sin(q2+q6+q7)) + l*sp.sin(q2+q6)],
                     [q1 - l/2*(sp.cos(q2) + sp.cos(q2+q6+q7)) - l*sp.cos(q2+q6)]])

r_foot2 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*sp.sin(q2+q6+q7) + l*sp.sin(q2+q6)],
                     [q1 - l/2*sp.cos(q2) - l*sp.cos(q2+q6+q7) - l*sp.cos(q2+q6)]])

rdot_torso = sp.diff(r_torso,t)
rdot_thigh1 = sp.diff(r_thigh1,t)
rdot_calf1 = sp.diff(r_calf1,t)
rdot_foot1 = sp.diff(r_foot1,t)
rdot_thigh2 = sp.diff(r_thigh2,t)
rdot_calf2 = sp.diff(r_calf2,t)
rdot_foot2 = sp.diff(r_foot2,t)


###---------------------Kinematics---------------------------###
fkTorso = sp.Matrix([[q0],
                      [q1],
                      [q2]])

fkTorsoTop = sp.Matrix([[q0 + l/2*sp.sin(q2)],
                        [q1 + l/2*sp.cos(q2)],
                        [q2]])

fkTorsoBot = sp.Matrix([[q0 + l/2*sp.sin(q2)],
                        [q1 - l/2*sp.cos(q2)],
                        [q2]])

fkThigh1 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l/2*sp.sin(q2+q3)],
                      [q1 - l/2*sp.cos(q2) - l/2*sp.cos(q2+q3)],
                      [q2+q3]])

fkThigh1Bot = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*sp.sin(q2+q3)],
                         [q1 - l/2*sp.cos(q2) - l*sp.cos(q2+q3)],
                         [q2+q3]])

fkCalf1Bot = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*(sp.sin(q2+q3+q4) + sp.sin(q2+q3))],
                        [q1 - l/2*sp.cos(q2) + l*(-sp.cos(q2+q3+q4) - sp.cos(q2+q3))],
                        [q2+q3+q4]])

fkCalf1 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l/2*sp.sin(q2+q3+q4) + l*sp.sin(q2+q3)],
                     [q1 - l/2*sp.cos(q2) - l/2*sp.cos(q2+q3+q4) - l*sp.cos(q2+q3)],
                     [q2+q3+q4]])

fkFoot1 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*(sp.sin(q2+q3+q4) + sp.sin(q2+q3))],
                     [q1 - l/2*sp.cos(q2) + l*(-sp.cos(q2+q3+q4) - sp.cos(q2+q3))],
                     [q2+q3+q4+q5]])

fkToe1 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*(sp.sin(q2+q3+q4) + sp.sin(q2+q3)) + lfoot/2*sp.sin(q2+q3+q4+q5+sp.pi/2)],
                    [q1 - l/2*sp.cos(q2) + l*(-sp.cos(q2+q3+q4) - sp.cos(q2+q3)) - lfoot/2*sp.cos(q2+q3+q4+q5+sp.pi/2)],
                    [q2+q3+q4+q5]])

fkHeel1 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*(sp.sin(q2+q3+q4) + sp.sin(q2+q3)) - lfoot/2*sp.sin(q2+q3+q4+q5+sp.pi/2)],
                     [q1 - l/2*sp.cos(q2) + l*(-sp.cos(q2+q3+q4) - sp.cos(q2+q3)) + lfoot/2*sp.cos(q2+q3+q4+q5+sp.pi/2)],
                     [q2+q3+q4+q5]])

fkThigh2Bot = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*sp.sin(q2+q6)],
                         [q1 - l/2*sp.cos(q2) - l*sp.cos(q2+q6)],
                         [q2+q6]])

fkThigh2 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l/2*sp.sin(q2+q6)],
                      [q1 - l/2*sp.cos(q2) - l/2*sp.cos(q2+q6)],
                      [q2+q6]])

fkCalf2Bot = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*(sp.sin(q2+q6+q7) + sp.sin(q2+q6))],
                        [q1 - l/2*sp.cos(q2) + l*(-sp.cos(q2+q6+q7) - sp.cos(q2+q6))],
                        [q2+q6+q7]])

fkCalf2 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l/2*sp.sin(q2+q6+q7) + l*sp.sin(q2+q6)],
                     [q1 - l/2*sp.cos(q2) - l/2*sp.cos(q2+q6+q7) - l*sp.cos(q2+q6)],
                     [q2+q6+q7]])

fkFoot2 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*(sp.sin(q2+q6+q7) + sp.sin(q2+q6))],
                     [q1 - l/2*sp.cos(q2) + l*(-sp.cos(q2+q6+q7) - sp.cos(q2+q6))],
                     [q2+q6+q7+q8]])

fkToe2 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*(sp.sin(q2+q6+q7) + sp.sin(q2+q6)) + lfoot/2*sp.sin(q2+q6+q7+q8+sp.pi/2)],
                    [q1 - l/2*sp.cos(q2) + l*(-sp.cos(q2+q6+q7) - sp.cos(q2+q6)) - lfoot/2*sp.cos(q2+q6+q7+q8+sp.pi/2)],
                    [q2+q6+q7+q8]])

fkHeel2 = sp.Matrix([[q0 + l/2*sp.sin(q2) + l*(sp.sin(q2+q6+q7) + sp.sin(q2+q6)) - lfoot/2*sp.sin(q2+q6+q7+q8+sp.pi/2)],
                    [q1 - l/2*sp.cos(q2) + l*(-sp.cos(q2+q6+q7) - sp.cos(q2+q6)) + lfoot/2*sp.cos(q2+q6+q7+q8+sp.pi/2)],
                    [q2+q6+q7+q8]])

fkCOM = (fkTorso*mtorso + fkThigh1*mthigh + fkCalf1*mcalf + fkFoot1*mfoot + fkThigh2*mthigh + fkCalf2*mcalf + fkFoot2*mfoot)/(mtorso + 2*mthigh + 2*mcalf + 2*mfoot)

# This is the conversion between torso-centered and foot1-centered coordinates
# q0 = sp.pi-(q2+q3+q4)
# q1 = -q4
# q2 = -q3
# q3 = sp.pi-q3+q6
# q4 = q7

fkTorsoWRTFoot = sp.Matrix([[-l*sp.sin(q0) - l*sp.sin(q0+q1) - l/2*sp.sin(q0+q1+q2)],
                            [l*sp.cos(q0) + l*sp.cos(q0+q1) + l/2*sp.cos(q0+q1+q2)],
                            [q0+q1+q2]])

fkFootWRTFoot = sp.Matrix([[-(l*sp.sin(q0) + l*sp.sin(q0+q1) + l*sp.sin(q0+q1+q3) + l*sp.sin(q0+q1+q3+q4))],
                            [(l*sp.cos(q0) + l*sp.cos(q0+q1) + l*sp.cos(q0+q1+q3) + l*sp.cos(q0+q1+q3+q4))],
                            [q0+q1+q3+q4+q5]])

fkCOMWRTFoot = sp.Matrix([[-((l/2*sp.sin(q0))*mcalf +\
                             (l*sp.sin(q0) + l/2*sp.sin(q0+q1))*mthigh +\
                             (l*sp.sin(q0) + l*sp.sin(q0+q1) + l/2*sp.sin(q0+q1+q2))*mtorso +\
                             (l*sp.sin(q0) + l*sp.sin(q0+q1) + l/2*sp.sin(q0+q1+q3))*mthigh +\
                             (l*sp.sin(q0) + l*sp.sin(q0+q1) + l*sp.sin(q0+q1+q3) + l/2*sp.sin(q0+q1+q3+q4))*mcalf +\
                             (l*sp.sin(q0) + l*sp.sin(q0+q1) + l*sp.sin(q0+q1+q3) + l*sp.sin(q0+q1+q3+q4))*mfoot)/(mcalf+mthigh+mtorso+mthigh+mcalf+mfoot)],
                          [((l/2*sp.cos(q0))*mcalf +\
                            (l*sp.cos(q0) + l/2*sp.cos(q0+q1))*mthigh +\
                            (l*sp.cos(q0) + l*sp.cos(q0+q1) + l/2*sp.cos(q0+q1+q2))*mtorso +\
                            (l*sp.cos(q0) + l*sp.cos(q0+q1) + l/2*sp.cos(q0+q1+q3))*mthigh +\
                            (l*sp.cos(q0) + l*sp.cos(q0+q1) + l*sp.cos(q0+q1+q3) + l/2*sp.cos(q0+q1+q3+q4))*mcalf +\
                            (l*sp.cos(q0) + l*sp.cos(q0+q1) + l*sp.cos(q0+q1+q3) + l*sp.cos(q0+q1+q3+q4))*mfoot)/(mcalf+mthigh+mtorso+mthigh+mcalf+mfoot)],
                          [0.0]])

def get_jac(xdoti):
    return sp.simplify(sp.Matrix.hstack(sp.diff(xdoti,sp.Derivative(q0,t)),
                                        sp.diff(xdoti,sp.Derivative(q1,t)),
                                        sp.diff(xdoti,sp.Derivative(q2,t)),
                                        sp.diff(xdoti,sp.Derivative(q3,t)),
                                        sp.diff(xdoti,sp.Derivative(q4,t)),
                                        sp.diff(xdoti,sp.Derivative(q5,t)),
                                        sp.diff(xdoti,sp.Derivative(q6,t)),
                                        sp.diff(xdoti,sp.Derivative(q7,t)),
                                        sp.diff(xdoti,sp.Derivative(q8,t))))

xdotCOM = sp.diff(fkCOM,t)
jacCOM = get_jac(xdotCOM)

xdotCOMWRTFoot = sp.diff(fkCOMWRTFoot,t)
jacCOMWRTFoot = get_jac(xdotCOMWRTFoot)

xdotTorsoWRTFoot = sp.diff(fkTorsoWRTFoot,t)
jacTorsoWRTFoot = get_jac(xdotTorsoWRTFoot)

xdotFootWRTFoot = sp.diff(fkFootWRTFoot,t)
jacFootWRTFoot = get_jac(xdotFootWRTFoot)

xdotTorso = sp.diff(fkTorso,t)
jacTorso = get_jac(xdotTorso)

xdotTorsoWRTFoot1 = sp.diff(fkTorso-fkFoot1,t)
jacTorsoWRTFoot1 = get_jac(xdotTorsoWRTFoot1)

xdotTorsoWRTFoot2 = sp.diff(fkTorso-fkFoot2,t)
jacTorsoWRTFoot2 = get_jac(xdotTorsoWRTFoot2)

xdotTorsoTop = sp.diff(fkTorsoTop,t)
jacTorsoTop = get_jac(xdotTorsoTop)

xdotTorsoBot = sp.diff(fkTorsoBot,t)
jacTorsoBot = get_jac(xdotTorsoBot)

xdotThigh1Bot = sp.diff(fkThigh1Bot,t)
jacThigh1Bot = get_jac(xdotThigh1Bot)

xdotCalf1Bot = sp.diff(fkCalf1Bot,t)
jacCalf1Bot = get_jac(xdotCalf1Bot)

xdotFoot1 = sp.diff(fkFoot1,t)
jacFoot1 = get_jac(xdotFoot1)

xdotToe1 = sp.diff(fkToe1,t)
jacToe1 = get_jac(xdotToe1)

xdotHeel1 = sp.diff(fkHeel1,t)
jacHeel1 = get_jac(xdotHeel1)

xdotThigh2Bot = sp.diff(fkThigh2Bot,t)
jacThigh2Bot = get_jac(xdotThigh2Bot)

xdotCalf2Bot = sp.diff(fkCalf2Bot,t)
jacCalf2Bot = get_jac(xdotCalf2Bot)

xdotFoot2 = sp.diff(fkFoot2,t)
jacFoot2 = get_jac(xdotFoot2)

xdotToe2 = sp.diff(fkToe2,t)
jacToe2 = get_jac(xdotToe2)

xdotHeel2 = sp.diff(fkHeel2,t)
jacHeel2 = get_jac(xdotHeel2)

###---------------------Dynamics---------------------------###

qd2 = sp.Matrix([sp.Derivative(q2,t)])
qd3 = sp.Matrix([sp.Derivative(q3,t)])
qd4 = sp.Matrix([sp.Derivative(q4,t)])
qd5 = sp.Matrix([sp.Derivative(q5,t)])
qd6 = sp.Matrix([sp.Derivative(q6,t)])
qd7 = sp.Matrix([sp.Derivative(q7,t)])
qd8 = sp.Matrix([sp.Derivative(q8,t)])

T = 0.5*mtorso*rdot_torso.T*rdot_torso +\
    0.5*mthigh*rdot_thigh1.T*rdot_thigh1 +\
    0.5*mcalf*rdot_calf1.T*rdot_calf1 +\
    0.5*mfoot*rdot_foot1.T*rdot_foot1 +\
    0.5*mthigh*rdot_thigh2.T*rdot_thigh2 +\
    0.5*mcalf*rdot_calf2.T*rdot_calf2 +\
    0.5*mfoot*rdot_foot2.T*rdot_foot2 +\
    0.5*qd2.T*Itorso*qd2 +\
    0.5*(qd2+qd3).T*Ithigh*(qd2+qd3) +\
    0.5*(qd2+qd3+qd4).T*Icalf*(qd2+qd3+qd4) +\
    0.5*(qd2+qd3+qd4+qd5).T*Ifoot*(qd2+qd3+qd4+qd5) +\
    0.5*(qd2+qd6).T*Ithigh*(qd2+qd6) +\
    0.5*(qd2+qd6+qd7).T*Icalf*(qd2+qd6+qd7) +\
    0.5*(qd2+qd6+qd7+qd8).T*Ifoot*(qd2+qd6+qd7+qd8)

gvec = sp.Matrix([[0],
                  [1]])

V = g*(r_torso.T*gvec*mtorso +\
       r_thigh1.T*gvec*mthigh +\
       r_calf1.T*gvec*mcalf +\
       r_foot1.T*gvec*mfoot +\
       r_thigh2.T*gvec*mthigh +\
       r_calf2.T*gvec*mcalf +\
       r_foot2.T*gvec*mfoot)

L = T-V

dL_dqdot = sp.Matrix([[sp.diff(L,sp.Derivative(q0,t))],
                      [sp.diff(L,sp.Derivative(q1,t))],
                      [sp.diff(L,sp.Derivative(q2,t))],
                      [sp.diff(L,sp.Derivative(q3,t))],
                      [sp.diff(L,sp.Derivative(q4,t))],
                      [sp.diff(L,sp.Derivative(q5,t))],
                      [sp.diff(L,sp.Derivative(q6,t))],
                      [sp.diff(L,sp.Derivative(q7,t))],                      
                      [sp.diff(L,sp.Derivative(q8,t))]])

dL_dq = sp.Matrix([[sp.diff(L,q0)],
                   [sp.diff(L,q1)],
                   [sp.diff(L,q2)],
                   [sp.diff(L,q3)],
                   [sp.diff(L,q4)],
                   [sp.diff(L,q5)],
                   [sp.diff(L,q6)],
                   [sp.diff(L,q7)],                   
                   [sp.diff(L,q8)]])

print("calculating EoM...")
EoM = sp.diff(dL_dqdot,t) - dL_dq

# Find the Matrices
q = sp.Matrix([[q0],[q1],[q2],[q3],[q4],[q5],[q6],[q7],[q8]])
qdot = sp.diff(q,t)
qddot = sp.diff(qdot,t)

print("calculating M...")
M = sp.simplify(sp.Matrix.hstack(sp.diff(EoM,sp.Derivative(q0,t,t)),
                                 sp.diff(EoM,sp.Derivative(q1,t,t)),
                                 sp.diff(EoM,sp.Derivative(q2,t,t)),
                                 sp.diff(EoM,sp.Derivative(q3,t,t)),
                                 sp.diff(EoM,sp.Derivative(q4,t,t)),
                                 sp.diff(EoM,sp.Derivative(q5,t,t)),
                                 sp.diff(EoM,sp.Derivative(q6,t,t)),
                                 sp.diff(EoM,sp.Derivative(q7,t,t)),
                                 sp.diff(EoM,sp.Derivative(q8,t,t))))

print("calculating grav...")
grav = sp.simplify(sp.diff(EoM,g)*g)

print("calculating c...")
c = sp.simplify(EoM - grav - M*sp.Matrix([[sp.Derivative(q0,t,t)],
                                          [sp.Derivative(q1,t,t)],
                                          [sp.Derivative(q2,t,t)],
                                          [sp.Derivative(q3,t,t)],
                                          [sp.Derivative(q4,t,t)],
                                          [sp.Derivative(q5,t,t)],
                                          [sp.Derivative(q6,t,t)],
                                          [sp.Derivative(q7,t,t)],
                                          [sp.Derivative(q8,t,t)]]))

# Get rid of time dependence within sympy
def dummify_undefined_functions(expr):
    mapping = {}    

    # replace all Derivative terms
    for der in expr.atoms(sp.Derivative):
        f_name = der.expr.func.__name__
        var_names = [var.name for var in der.variables]
        name = "qd%s" % (f_name[1])
        mapping[der] = sp.Symbol(name)

    for der in expr.atoms(q0,q1,q2,q3,q4,q5,q6,q7,q8):
        f_name = der.func.__name__
        name = f_name
        mapping[der] = sp.Symbol(name)
        
    return expr.subs(mapping)

M = dummify_undefined_functions(M)
grav = dummify_undefined_functions(grav)
c = dummify_undefined_functions(c)

fkCOM = dummify_undefined_functions(fkCOM)
fkCOMWRTFoot = dummify_undefined_functions(fkCOMWRTFoot)
fkTorsoWRTFoot = dummify_undefined_functions(fkTorsoWRTFoot)
fkFootWRTFoot = dummify_undefined_functions(fkFootWRTFoot)
fkTorso = dummify_undefined_functions(fkTorso)
fkTorsoTop = dummify_undefined_functions(fkTorsoTop)
fkTorsoBot = dummify_undefined_functions(fkTorsoBot)
fkThigh1Bot = dummify_undefined_functions(fkThigh1Bot)
fkCalf1Bot = dummify_undefined_functions(fkCalf1Bot)
fkFoot1 = dummify_undefined_functions(fkFoot1)
fkToe1 = dummify_undefined_functions(fkToe1)
fkHeel1 = dummify_undefined_functions(fkHeel1)
fkThigh2Bot = dummify_undefined_functions(fkThigh2Bot)
fkCalf2Bot = dummify_undefined_functions(fkCalf2Bot)
fkFoot2 = dummify_undefined_functions(fkFoot2)
fkToe2 = dummify_undefined_functions(fkToe2)
fkHeel2 = dummify_undefined_functions(fkHeel2)

jacCOM = dummify_undefined_functions(jacCOM)
jacCOMWRTFoot = dummify_undefined_functions(jacCOMWRTFoot)
jacTorsoWRTFoot = dummify_undefined_functions(jacTorsoWRTFoot)
jacFootWRTFoot = dummify_undefined_functions(jacFootWRTFoot)
jacTorso = dummify_undefined_functions(jacTorso)
jacTorsoWRTFoot1 = dummify_undefined_functions(jacTorsoWRTFoot1)
jacTorsoWRTFoot2 = dummify_undefined_functions(jacTorsoWRTFoot2)
jacTorsoTop = dummify_undefined_functions(jacTorsoTop)
jacTorsoBot = dummify_undefined_functions(jacTorsoBot)
jacThigh1Bot = dummify_undefined_functions(jacThigh1Bot)
jacCalf1Bot = dummify_undefined_functions(jacCalf1Bot)
jacFoot1 = dummify_undefined_functions(jacFoot1)
jacToe1 = dummify_undefined_functions(jacToe1)
jacHeel1 = dummify_undefined_functions(jacHeel1)
jacThigh2Bot = dummify_undefined_functions(jacThigh2Bot)
jacCalf2Bot = dummify_undefined_functions(jacCalf2Bot)
jacFoot2 = dummify_undefined_functions(jacFoot2)
jacToe2 = dummify_undefined_functions(jacToe2)
jacHeel2 = dummify_undefined_functions(jacHeel2)

print("Generating C code...")
from sympy.utilities.codegen import codegen

filename = "dynamics2"
c_code = ''

c_code+='#include "test.h"\n#include <math.h>\n\nnamespace py = pybind11;\n\n'

c_code+="py::array calc_M(std::vector<double> q,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot,double Itorso,double Ithigh,double Icalf,double Ifoot)\n{"
c_code+="\n\t double out["+str(n*n)+"];\n"
for i in range(0,n):
    for j in range(0,n):
        c_code += "\n\t out["+str(i*n+j)+"] = "+sp.ccode(M[i,j].as_expr()[0])+";"
c_code+="\n\treturn py::array("+str(n*n)+",out);\n"        
c_code+="\n}\n\n"

c_code+="py::array calc_grav(std::vector<double> q,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot,double g)\n{"
c_code+="\n\t double out["+str(n)+"];\n"
for i in range(0,n):
    c_code += "\n\t out["+str(i)+"] = "+sp.ccode(grav[i].as_expr()[0])+";"
c_code+="\n\treturn py::array("+str(n)+",out);\n"
c_code+="\n}\n\n"

c_code+="py::array calc_c(std::vector<double> q,std::vector<double> qd,double l,double lfoot,double mtorso,double mthigh,double mcalf,double mfoot)\n{"
c_code+="\n\t double out["+str(n)+"];\n"
for i in range(0,n):
    c_code += "\n\t out["+str(i)+"] = "+sp.ccode(c[i].as_expr()[0])+";"
c_code+="\n\treturn py::array("+str(n)+",out);\n"
c_code+="\n}\n\n"

def print_fk_function(code,fk,link):
    code+="py::array fk"+str(link)+"(std::vector<double> q,double l,double lfoot)\n{"
    code+="\n\t double out[3];\n"
    for i in range(0,3):
        code += "\n\t out["+str(i)+"] = "+sp.ccode(fk[i].as_expr())+";"
    code+="\n\treturn py::array(3,out);\n"
    code+="\n}\n\n"
    return code

c_code = print_fk_function(c_code,fkCOM,'COM')
c_code = print_fk_function(c_code,fkCOMWRTFoot,'COMWRTFoot')
c_code = print_fk_function(c_code,fkTorsoWRTFoot,'TorsoWRTFoot')
c_code = print_fk_function(c_code,fkFootWRTFoot,'FootWRTFoot')                           
c_code = print_fk_function(c_code,fkTorso,'Torso')
c_code = print_fk_function(c_code,fkTorsoTop,'TorsoTop')
c_code = print_fk_function(c_code,fkTorsoBot,'TorsoBot')
c_code = print_fk_function(c_code,fkThigh1Bot,'Thigh1Bot')
c_code = print_fk_function(c_code,fkCalf1Bot,'Calf1Bot')
c_code = print_fk_function(c_code,fkFoot1,'Foot1')
c_code = print_fk_function(c_code,fkToe1,'Toe1')
c_code = print_fk_function(c_code,fkHeel1,'Heel1')
c_code = print_fk_function(c_code,fkThigh2Bot,'Thigh2Bot')
c_code = print_fk_function(c_code,fkCalf2Bot,'Calf2Bot')
c_code = print_fk_function(c_code,fkFoot2,'Foot2')
c_code = print_fk_function(c_code,fkToe2,'Toe2')
c_code = print_fk_function(c_code,fkHeel2,'Heel2')

def print_jac_function(code,jac,link):
    code+="py::array jac"+str(link)+"(std::vector<double> q,double l,double lfoot)\n{"
    code+="\n\t double out["+str(3*n)+"];\n"
    for i in range(0,3):
        for j in range(0,n):
            code += "\n\t out["+str(i*n+j)+"] = "+sp.ccode(jac[i,j].as_expr())+";"
    code+="\n\treturn py::array("+str(3*n)+",out);\n"
    code+="\n}\n\n"
    return code

c_code = print_jac_function(c_code,jacCOM,'COM')
c_code = print_jac_function(c_code,jacCOMWRTFoot,'COMWRTFoot')
c_code = print_jac_function(c_code,jacTorsoWRTFoot,'TorsoWRTFoot')
c_code = print_jac_function(c_code,jacFootWRTFoot,'FootWRTFoot')                           
c_code = print_jac_function(c_code,jacTorso,'Torso')
c_code = print_jac_function(c_code,jacTorsoWRTFoot1,'TorsoWRTFoot1')
c_code = print_jac_function(c_code,jacTorsoWRTFoot2,'TorsoWRTFoot2')
c_code = print_jac_function(c_code,jacTorsoTop,'TorsoTop')
c_code = print_jac_function(c_code,jacTorsoBot,'TorsoBot')
c_code = print_jac_function(c_code,jacThigh1Bot,'Thigh1Bot')
c_code = print_jac_function(c_code,jacCalf1Bot,'Calf1Bot')
c_code = print_jac_function(c_code,jacFoot1,'Foot1')
c_code = print_jac_function(c_code,jacToe1,'Toe1')
c_code = print_jac_function(c_code,jacHeel1,'Heel1')
c_code = print_jac_function(c_code,jacThigh2Bot,'Thigh2Bot')
c_code = print_jac_function(c_code,jacCalf2Bot,'Calf2Bot')
c_code = print_jac_function(c_code,jacFoot2,'Foot2')
c_code = print_jac_function(c_code,jacToe2,'Toe2')
c_code = print_jac_function(c_code,jacHeel2,'Heel2')

for i in range(0,n):
    while c_code.find('qd'+str(i)) != -1:
        c_code = c_code.replace('qd'+str(i),'qd['+str(i)+']')
    while c_code.find('q'+str(i)) != -1:
        c_code = c_code.replace('q'+str(i),'q['+str(i)+']')
    
cf = open(filename+".c",'w')

cf.write(c_code)
cf.close()
