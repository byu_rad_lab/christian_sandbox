#include "n1linkdynamics.h"

namespace py = pybind11;

py::array calc_M_py(std::vector<double> q,double l,double m,double I)
{
	 double out[1];

	double x0 = pow(sin(q[0]), 2);
	double x1 = pow(cos(q[0]), 2);
	double x2 = (1.0/2.0)*l;
	 out[0] = I*pow(x0 + x1, 2) + m*pow(-x0*x2 - x1*x2, 2);
	return py::array(1,out);

}

void calc_M(std::vector<double> q,Eigen::MatrixXd * M,double l,double m,double I)
{

	double x0 = pow(sin(q[0]), 2);
	double x1 = pow(cos(q[0]), 2);
	double x2 = (1.0/2.0)*l;
	(*M)(0,0) = I*pow(x0 + x1, 2) + m*pow(-x0*x2 - x1*x2, 2);
}

py::array calc_grav_py(std::vector<double> q,double l,double m,double g)
{
	 double out[1];

	 out[0] = -1.0/2.0*g*l*m*sin(q[0]);
	return py::array(1,out);
}

void calc_grav(std::vector<double> q,Eigen::MatrixXd * grav,double l,double m,double g)
{

	(*grav)(0,0) = -1.0/2.0*g*l*m*sin(q[0]);
}

py::array calc_C_py(std::vector<double> q,std::vector<double> qd,double l,double m,double I)
{
	 double out[1];

	 out[0] = 0;
	return py::array(1,out);
}

void calc_C(std::vector<double> q,std::vector<double> qd,Eigen::MatrixXd * C,double l,double m,double I)
{

	(*C)(0,0) = 0;
}

py::array fkCoM0_py(std::vector<double> q,double l)
{
	 double out[16];

	double x0 = cos(q[0]);
	double x1 = sin(q[0]);
	double x2 = (1.0/2.0)*l;
	 out[0] = x0;
	 out[1] = -x1;
	 out[2] = 0;
	 out[3] = -x1*x2;
	 out[4] = x1;
	 out[5] = x0;
	 out[6] = 0;
	 out[7] = x0*x2;
	 out[8] = 0;
	 out[9] = 0;
	 out[10] = 1;
	 out[11] = 0;
	 out[12] = 0;
	 out[13] = 0;
	 out[14] = 0;
	 out[15] = 1;
	return py::array(16,out);

}

void calc_A_B_w(std::vector<double> q,std::vector<double> qd,Eigen::MatrixXd * A, Eigen::MatrixXd * B, Eigen::MatrixXd * w,double l=1.0,double m=1.0,double I=.1,double gravity=9.81,double damping=.01)
{
  int n = 1;
  Eigen::MatrixXd M(n,n);
  Eigen::MatrixXd C(n,n);
  Eigen::MatrixXd F(n,n);
  F.setIdentity();
  F = F*damping;
  Eigen::MatrixXd g(n,1);

  calc_M(q,&M,l,m,I);
  calc_C(q,qd,&C,l,m,I);
  calc_grav(q,&g,l,m,gravity);

  Eigen::MatrixXd M_inv;
  M_inv = M.inverse();

  (*A).topLeftCorner(n,n) = -M_inv*(C+F);
  (*A).topRightCorner(n,n).setZero();
  (*A).bottomLeftCorner(n,n).setIdentity();
  (*A).bottomRightCorner(n,n).setZero();

  for(int i=0; i<M_inv.rows(); i++)
    {
      for(int j=0; j<M_inv.cols(); j++)
	{
	  (*B)(i,j) = M_inv(i,j);
	}
    }
  for(int i=n; i<2*n; i++)
    {
      for(int j=0; j<n; j++)
	{
	  (*B)(i,j) = 0.0;
	}
    }

  Eigen::MatrixXd b(n,1);
  b = M_inv*(-g);  
  for(int i=0; i<g.rows(); i++)
    {
      (*w)(i) = b(i);
    }
  for(int i=g.rows(); i<2*n; i++)
    {
      (*w)(i) = 0;
    }
  
}

void calc_discrete_A_B_w(std::vector<double> q,std::vector<double> qd, double dt, Eigen::MatrixXd * A_d, Eigen::MatrixXd * B_d, Eigen::MatrixXd * w_d,double l=1.0,double m=1.0,double I=.1,double gravity=9.81,double damping=.01)
{
  int n = 1;
  Eigen::MatrixXd A(2*n,2*n);
  Eigen::MatrixXd B(2*n,n);
  Eigen::MatrixXd w(2*n,1);
  calc_A_B_w(q,qd,&A,&B,&w,l,m,I,gravity,damping);
  
  Eigen::MatrixXd eye(A.rows(),A.cols());
  eye.setIdentity();
  *A_d = (eye - A*dt).inverse();
  *B_d = (eye - A*dt).inverse()*B*dt;
  
  *w_d = w*dt;
}

py::array calc_discrete_A_B_w_py(std::vector<double> q,std::vector<double> qd, double dt,double l=1.0,double m=1.0,double I=.1,double gravity=9.81,double damping=.01)
{
  int n = 1;
  int num_states = n*2;
  int num_inputs = n;
  double out[num_states*num_states + num_states*num_inputs + num_states];
  
  Eigen::MatrixXd A(num_states,num_states);
  Eigen::MatrixXd B(num_states,num_inputs);
  Eigen::MatrixXd w(num_states,1);
  Eigen::MatrixXd A_d(num_states,num_states);
  Eigen::MatrixXd B_d(num_states,num_inputs);
  Eigen::MatrixXd w_d(num_states,1);

  calc_A_B_w(q,qd,&A,&B,&w,l,m,I,gravity,damping);
  
  Eigen::MatrixXd eye(A.rows(),A.cols());
  eye.setIdentity();
  A_d = (eye - A*dt).inverse();
  B_d = (eye - A*dt).inverse()*B*dt;

  w_d = w*dt;

  for(int i=0; i<num_states; i++)
    {
      for(int j=0; j<num_states; j++)
	{
	  out[i*num_states+j] = A_d(i,j);
	}
      for(int j=0; j<num_inputs; j++)
	{
	  out[num_states*num_states + i*num_inputs+j] = B_d(i,j);
	}
      out[num_states*num_states + num_states*num_inputs + i] = w_d(i);
    }
  return py::array(num_states*num_states + num_states*num_inputs + num_states,out);
}


PYBIND11_MODULE(n1linkdynamics,m) {
  m.def("calc_discrete_A_B_w_py",&calc_discrete_A_B_w_py,"a function description");
  m.def("calc_M",&calc_M_py,"a function description");
  m.def("calc_grav",&calc_grav_py,"another function description");
  m.def("calc_C",&calc_C_py,"yet another function description");
  m.def("fkCoM0",&fkCoM0_py,"a function description");
}
