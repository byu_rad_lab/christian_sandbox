import sympy as sp
import numpy as np
sp.init_printing(use_unicode=True)
n = 3

t = sp.symbols('t')

q0 = sp.Function('q0')(t)
q1 = sp.Function('q1')(t)
q2 = sp.Function('q2')(t)

l,I,m,g = sp.symbols('l I m g',real=True)

r_A_o = sp.Matrix([[-l/2*sp.sin(q0)],
                   [l/2*sp.cos(q0)]])
r_B_o = sp.Matrix([[-l*sp.sin(q0) - l/2*sp.sin(q0+q1)],
                   [l*sp.cos(q0) + l/2*sp.cos(q0+q1)]])
r_C_o = sp.Matrix([[-l*sp.sin(q0) - l*sp.sin(q0+q1) - l/2*sp.sin(q0+q1+q2)],
                   [l*sp.cos(q0) + l*sp.cos(q0+q1) + l/2*sp.cos(q0+q1+q2)]])
                   
rdot_A_o = sp.diff(r_A_o,t)
rdot_B_o = sp.diff(r_B_o,t)
rdot_C_o = sp.diff(r_C_o,t)

###---------------------Kinematics---------------------------###
# fki is the forward kinematics to the end of the i'th link
fk0 = sp.Matrix([[-l/2*sp.sin(q0)],
                 [l/2*sp.cos(q0)],
                 [q0]])

fk1 = sp.Matrix([[-l*sp.sin(q0) - l/2*sp.sin(q0+q1)],
                 [l*sp.cos(q0) + l/2*sp.cos(q0+q1)],
                 [q0+q1]])

fk2 = sp.Matrix([[-l*sp.sin(q0) + -l*sp.sin(q0+q1) + -l/2*sp.sin(q0+q1+q2)],
                 [l*sp.cos(q0) + l*sp.cos(q0+q1) + l/2*sp.cos(q0+q1+q2)],
                 [q0+q1+q2]])

def get_jac(xdoti):
    return sp.Matrix.hstack(sp.diff(xdoti,sp.Derivative(q0,t)),
                            sp.diff(xdoti,sp.Derivative(q1,t)),
                            sp.diff(xdoti,sp.Derivative(q2,t)))


xdot0 = sp.diff(fk0,t)
jac0 = get_jac(xdot0)

xdot1 = sp.diff(fk1,t)
jac1 = get_jac(xdot1)

xdot2 = sp.diff(fk2,t)
jac2 = get_jac(xdot2)

###---------------------Dynamics---------------------------###

qd0 = sp.Matrix([sp.Derivative(q0,t)])
qd1 = sp.Matrix([sp.Derivative(q1,t)])
qd2 = sp.Matrix([sp.Derivative(q2,t)])

T = 0.5*m*rdot_A_o.T*rdot_A_o + 0.5*m*rdot_B_o.T*rdot_B_o + 0.5*m*rdot_C_o.T*rdot_C_o + 0.5*qd0.T*I*qd0 + 0.5*(qd0+qd1).T*I*(qd0+qd1) + 0.5*(qd0+qd1+qd2).T*I*(qd0+qd1+qd2)

gvec = sp.Matrix([[0],
                  [1]])

V = g*(r_A_o.T*gvec*m + r_B_o.T*gvec*m + r_C_o.T*gvec*m)

L = T-V

dL_dqdot = sp.Matrix([[sp.diff(L,sp.Derivative(q0,t))],
                      [sp.diff(L,sp.Derivative(q1,t))],
                      [sp.diff(L,sp.Derivative(q2,t))]])

dL_dq = sp.Matrix([[sp.diff(L,q0)],
                   [sp.diff(L,q1)],
                   [sp.diff(L,q2)]])

print("calculating EoM...")
EoM = sp.diff(dL_dqdot,t) - dL_dq

# Find the Matrices
q = sp.Matrix([[q0],[q1],[q2]])
qdot = sp.diff(q,t)
qddot = sp.diff(qdot,t)

print("calculating M...")
M = sp.simplify(sp.Matrix.hstack(sp.diff(EoM,sp.Derivative(q0,t,t)),
                                 sp.diff(EoM,sp.Derivative(q1,t,t)),
                                 sp.diff(EoM,sp.Derivative(q2,t,t))))

print("calculating grav...")
grav = sp.simplify(sp.diff(EoM,g)*g)

print("calculating c...")
c = sp.simplify(EoM - grav - M*sp.Matrix([[sp.Derivative(q0,t,t)],
                                          [sp.Derivative(q1,t,t)],
                                          [sp.Derivative(q2,t,t)]]))
                                          
# Get rid of time dependence within sympy
def dummify_undefined_functions(expr):
    mapping = {}    

    # replace all Derivative terms
    for der in expr.atoms(sp.Derivative):
        f_name = der.expr.func.__name__
        var_names = [var.name for var in der.variables]
        name = "qd%s" % (f_name[1])
        mapping[der] = sp.Symbol(name)

    for der in expr.atoms(q0,q1,q2):
        f_name = der.func.__name__
        name = f_name
        mapping[der] = sp.Symbol(name)
        
    return expr.subs(mapping)

M = dummify_undefined_functions(M)
grav = dummify_undefined_functions(grav)
c = dummify_undefined_functions(c)

fk0 = dummify_undefined_functions(fk0)
fk1 = dummify_undefined_functions(fk1)
fk2 = dummify_undefined_functions(fk2)
jac0 = dummify_undefined_functions(jac0)
jac1 = dummify_undefined_functions(jac1)
jac2 = dummify_undefined_functions(jac2)

print("Generating C code...")
from sympy.utilities.codegen import codegen

filename = "threelinkdynamics"
c_code = ''

c_code+='#include "threelinkdynamics.h"\n#include <math.h>\n\nnamespace py = pybind11;\n\n'
c_code+="py::array calc_M(std::vector<double> q,double l,double m,double I)\n{"
c_code+="\n\t double out["+str(n*n)+"];\n"
for i in range(0,n):
    for j in range(0,n):
        c_code += "\n\t out["+str(i*n+j)+"] = "+sp.ccode(M[i,j].as_expr()[0])+";"
c_code+="\n\treturn py::array("+str(n*n)+",out);\n"        
c_code+="\n}\n\n"

c_code+="py::array calc_grav(std::vector<double> q,double l,double m,double g)\n{"
c_code+="\n\t double out["+str(n)+"];\n"
for i in range(0,n):
    c_code += "\n\t out["+str(i)+"] = "+sp.ccode(grav[i].as_expr()[0])+";"
c_code+="\n\treturn py::array("+str(n)+",out);\n"
c_code+="\n}\n\n"

c_code+="py::array calc_c(std::vector<double> q,std::vector<double> qd,double l,double m)\n{"
c_code+="\n\t double out["+str(n)+"];\n"
for i in range(0,n):
    c_code += "\n\t out["+str(i)+"] = "+sp.ccode(c[i].as_expr()[0])+";"
c_code+="\n\treturn py::array("+str(n)+",out);\n"
c_code+="\n}\n\n"

def print_fk_function(code,fk,link):
    code+="py::array fk"+str(link)+"(std::vector<double> q,double l)\n{"
    code+="\n\t double out[3];\n"
    for i in range(0,3):
        code += "\n\t out["+str(i)+"] = "+sp.ccode(fk[i].as_expr())+";"
    code+="\n\treturn py::array(3,out);\n"
    code+="\n}\n\n"
    return code

c_code = print_fk_function(c_code,fk0,0)
c_code = print_fk_function(c_code,fk1,1)
c_code = print_fk_function(c_code,fk2,2)

def print_jac_function(code,jac,link):
    code+="py::array jac"+str(link)+"(std::vector<double> q,double l)\n{"
    code+="\n\t double out["+str(3*n)+"];\n"
    for i in range(0,3):
        for j in range(0,n):
            code += "\n\t out["+str(i*n+j)+"] = "+sp.ccode(jac[i,j].as_expr())+";"
    code+="\n\treturn py::array("+str(3*n)+",out);\n"
    code+="\n}\n\n"
    return code

c_code = print_jac_function(c_code,jac0,0)
c_code = print_jac_function(c_code,jac1,1)
c_code = print_jac_function(c_code,jac2,2)

for i in range(0,n):
    while c_code.find('qd'+str(i)) != -1:
        c_code = c_code.replace('qd'+str(i),'qd['+str(i)+']')
    while c_code.find('q'+str(i)) != -1:
        c_code = c_code.replace('q'+str(i),'q['+str(i)+']')
    
cf = open(filename+".c",'w')

cf.write(c_code)
cf.close()
