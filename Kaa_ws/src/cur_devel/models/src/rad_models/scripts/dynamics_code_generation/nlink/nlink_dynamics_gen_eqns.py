import sympy as sp
import numpy as np
sp.init_printing(use_unicode=True)

n = 5

t = sp.symbols('t')

qs = {}
for i in range(0,n):
    qs['q'+str(i)] = sp.Function('q'+str(i))(t)

qds = {}
for i in range(0,n):
    qds['qd'+str(i)] = sp.Matrix([sp.diff(qs['q'+str(i)],t)])

qdds = {}
for i in range(0,n):
    qdds['qdd'+str(i)] = sp.Matrix([sp.diff(qds['qd'+str(i)],t)])
    

l,I,m,g = sp.symbols('l I m g',real=True)

CoMs = {}
CoMs['CoM0'] = sp.Matrix([[-l/2*sp.sin(qs['q0'])],
                          [l/2*sp.cos(qs['q0'])]])
for i in range(1,n):
    CoMs['CoM'+str(i)] = sp.Matrix([[-l*sp.sin(qs['q0'])],
                                    [l*sp.cos(qs['q0'])]])
    
    for j in range(1,i+1):
        totalAngle = qs['q0']
        for k in range(1,j+1):
            totalAngle += qs['q'+str(k)]
        if(j < i):
            CoMs['CoM'+str(i)] += sp.Matrix([[-l*sp.sin(totalAngle)],
                                             [l*sp.cos(totalAngle)]])
        else:
            CoMs['CoM'+str(i)] += sp.Matrix([[-l/2*sp.sin(totalAngle)],
                                             [l/2*sp.cos(totalAngle)]])

CoMdots = {}
for i in range(0,n):
    CoMdots['CoMdot'+str(i)] = sp.diff(CoMs['CoM'+str(i)],t)
            

T = sp.Matrix([0])
for i in range(0,n):
    T += 0.5*m*CoMdots['CoMdot'+str(i)].T*CoMdots['CoMdot'+str(i)]
    total_omega = sp.Matrix([0])
    for j in range(0,i+1):
        total_omega += qds['qd'+str(j)]
    T += 0.5*total_omega.T*I*total_omega

gvec = sp.Matrix([[0],
                  [1]])

V = sp.Matrix([0])
for i in range(0,n):
    V += g*(CoMs['CoM'+str(i)].T*gvec*m)

L = T-V

dL_dqd = sp.Matrix(np.zeros((n,1)))
dL_dq = sp.Matrix(np.zeros((n,1)))
for i in range(0,n):
    # dL_dqd[i] = sp.diff(L,sp.Derivative(qs['q'+str(i)],t))
    dL_dqd[i] = sp.diff(L,qds['qd'+str(i)])[0]
    dL_dq[i] = sp.diff(L,qs['q'+str(i)])[0]

print("calculating EoM...")
EoM = sp.diff(dL_dqd,t) - dL_dq
# EoM = sp.trigsimp(sp.cancel(sp.diff(dL_dqd,t) - dL_dq))

print("calculating M...")
M = sp.Matrix(np.zeros((n,n)))
for i in range(0,n):
    print("calculating row ",i)
    M[:,i] = sp.diff(EoM,qdds['qdd'+str(i)])

for i in range(0,n):
    print("simplifying row ",i)
    M[:,i] = sp.trigsimp(M[:,i])

print("calculating grav...")
grav = sp.diff(EoM,g)*g

qdd = sp.Matrix(np.zeros((n,1)))
for i in range(0,n):
    qdd[i] = qdds['qdd'+str(i)]

print("calculating c...")
c = sp.Matrix(np.zeros((n,1)))
for i in range(0,n):
    print("calculating row ", i)
    c[i,:] = EoM[i,:] - grav[i,:] - M[i,:]*qdd
    
for i in range(0,n):
    print("simplifying row ", i)
    c[i,:] = sp.trigsimp(c[i,:])
                                          

def dummify_undefined_functions(expr):
    mapping = {}    

    # replace all Derivative terms
    for der in expr.atoms(sp.Derivative):
        f_name = der.expr.func.__name__
        var_names = [var.name for var in der.variables]
        name = "qd%s" % (f_name[1])
        mapping[der] = sp.Symbol(name)

    for key in list(qs.keys()):
        for der in expr.atoms(qs[key]):
            f_name = der.func.__name__
            name = f_name
            mapping[der] = sp.Symbol(name)
        
    return expr.subs(mapping)

M = dummify_undefined_functions(M)
grav = dummify_undefined_functions(grav)
c = dummify_undefined_functions(c)




print("Generating C code...")
from sympy.utilities.codegen import codegen

filename = "nlinkdynamics"
c_code = ''

c_code+='#include "nlinkdynamics.h"\n#include <math.h>\n\nnamespace py = pybind11;\n\n'
c_code+="py::array calc_M(std::vector<double> q,double l,double m,double I)\n{"
c_code+="\n\t double out["+str(n*n)+"];\n"
for i in range(0,n):
    for j in range(0,n):
        c_code += "\n\t out["+str(i*n+j)+"] = "+sp.ccode(M[i,j].as_expr())+";"
c_code+="\n\treturn py::array("+str(n*n)+",out);\n"        
c_code+="\n}\n\n"

c_code+="py::array calc_grav(std::vector<double> q,double l,double m,double g)\n{"
c_code+="\n\t double out["+str(n)+"];\n"
for i in range(0,n):
    c_code += "\n\t out["+str(i)+"] = "+sp.ccode(grav[i].as_expr())+";"
c_code+="\n\treturn py::array("+str(n)+",out);\n"
c_code+="\n}\n\n"

c_code+="py::array calc_c(std::vector<double> q,std::vector<double> qd,double l,double m)\n{"
c_code+="\n\t double out["+str(n)+"];\n"
for i in range(0,n):
    c_code += "\n\t out["+str(i)+"] = "+sp.ccode(c[i].as_expr())+";"
c_code+="\n\treturn py::array("+str(n)+",out);\n"
c_code+="\n}\n\n"

for i in range(0,n):
    while c_code.find('qd'+str(i)) != -1:
        c_code = c_code.replace('qd'+str(i),'qd['+str(i)+']')
    while c_code.find('q'+str(i)) != -1:
        c_code = c_code.replace('q'+str(i),'q['+str(i)+']')
    
cf = open(filename+".c",'w')

cf.write(c_code)
cf.close()
