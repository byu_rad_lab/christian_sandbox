# THIS IS ACTUALLY INCORRECT BECAUSE I DO NOT ROTATE THE INERTIA
# MATRIX DURING THE CALCULATION OF KINETIC ENERGY...OOPS
# BUT ASSUMING CONSTANT INERTIA IS WRONG TOO...SO...

import sympy as sp
import numpy as np
from copy import deepcopy
sp.init_printing(use_unicode=True)

t = sp.symbols('t')

q0 = sp.Function('q0')(t)
q1 = sp.Function('q1')(t)
q2 = sp.Function('q2')(t)
q3 = sp.Function('q3')(t)
q4 = sp.Function('q4')(t)
q5 = sp.Function('q5')(t)

n = 6

jtHeight,jt0Mass,jt0MassPos,l0Mass,l0MassPos, jt1Mass,jt1MassPos,l1Mass,l1MassPos, jt2Mass,jt2MassPos, jt0Ixx, jt0Iyy, jt0Izz, jt0Ixy, jt0Ixz, jt0Iyz, l0Ixx, l0Iyy, l0Izz, l0Ixy, l0Ixz, l0Iyz, jt1Ixx, jt1Iyy, jt1Izz, jt1Ixy, jt1Ixz, jt1Iyz, l1Ixx, l1Iyy, l1Izz, l1Ixy, l1Ixz, l1Iyz,jt2Ixx, jt2Iyy, jt2Izz, jt2Ixy, jt2Ixz, jt2Iyz, g = sp.symbols('jtHeight,jt0Mass,jt0MassPos,l0Mass,l0MassPos, jt1Mass,jt1MassPos,l1Mass,l1MassPos, jt2Mass,jt2MassPos, jt0Ixx, jt0Iyy, jt0Izz, jt0Ixy, jt0Ixz, jt0Iyz, l0Ixx, l0Iyy, l0Izz, l0Ixy, l0Ixz, l0Iyz, jt1Ixx, jt1Iyy, jt1Izz, jt1Ixy, jt1Ixz, jt1Iyz, l1Ixx, l1Iyy, l1Izz, l1Ixy, l1Ixz, l1Iyz,jt2Ixx, jt2Iyy, jt2Izz, jt2Ixy, jt2Ixz, jt2Iyz, g',real=True)

# jtHeight = .212
# jt0Mass = 6.534
# jt1Mass = 6.534
# jt2Mass = 1.5

# jt0Inertia = sp.Matrix([[.108, 0, 0],
#                         [0, .108, 0],
#                         [0, 0, .023]])
# jt1Inertia = sp.Matrix([[.108, 0, 0],
#                         [0, .108, 0],
#                         [0, 0, .023]])
# jt2Inertia = sp.Matrix([[.05, 0, 0],
#                         [0, .05, 0],
#                         [0, 0, .023]])
jt0Inertia = sp.Matrix([[jt0Ixx, -jt0Ixy, -jt0Ixz],
                        [-jt0Ixy, jt0Iyy, -jt0Iyz],
                        [-jt0Ixz, -jt0Iyz, jt0Izz]])

l0Inertia = sp.Matrix([[l0Ixx, -l0Ixy, -l0Ixz],
                        [-l0Ixy, l0Iyy, -l0Iyz],
                        [-l0Ixz, -l0Iyz, l0Izz]])

jt1Inertia = sp.Matrix([[jt1Ixx, -jt1Ixy, -jt1Ixz],
                        [-jt1Ixy, jt1Iyy, -jt1Iyz],
                        [-jt1Ixz, -jt1Iyz, jt1Izz]])

l1Inertia = sp.Matrix([[l1Ixx, -l1Ixy, -l1Ixz],
                        [-l1Ixy, l1Iyy, -l1Iyz],
                        [-l1Ixz, -l1Iyz, l1Izz]])

jt2Inertia = sp.Matrix([[jt2Ixx, -jt2Ixy, -jt2Ixz],
                        [-jt2Ixy, jt2Iyy, -jt2Iyz],
                        [-jt2Ixz, -jt2Iyz, jt2Izz]])

def get_rotation_matrix_from_uvh(u,v,h):
    phi = sp.sqrt(u**2 + v**2)
    sigma = sp.cos(phi)-1
    R = sp.Matrix([[sigma*(v/phi)**2+1, -sigma*u*v/(phi**2), v/phi*sp.sin(phi), -sigma*h*v/(phi**2)],
                   [-sigma*u*v/(phi**2), sigma*(u/phi)**2+1,  -u/phi*sp.sin(phi), sigma*h*u/(phi**2)],
                   [-v/phi*sp.sin(phi), u/phi*sp.sin(phi), sp.cos(phi), h*sp.sin(phi)/phi],
                   [0,0,0,1]])
    return R


fkJt0CoM = get_rotation_matrix_from_uvh(q0,q1,jtHeight+jt0MassPos)

fkJt0End = get_rotation_matrix_from_uvh(q0,q1,jtHeight)

fkL0CoM = get_rotation_matrix_from_uvh(q0,q1,jtHeight)*\
           sp.Matrix([[.8536, .1464, -.5, -.0148],
                      [.1464, .8536, .5, .0148],
                      [.5, -.5, .7071, l0MassPos],
                      [0, 0, 0, 1]])

fkL0End = get_rotation_matrix_from_uvh(q0,q1,jtHeight)*\
           sp.Matrix([[.8536, .1464, -.5, -.0148],
                      [.1464, .8536, .5, .0148],
                      [.5, -.5, .7071, .26],
                      [0, 0, 0, 1]])

fkJt1CoM = get_rotation_matrix_from_uvh(q0,q1,jtHeight)*\
           sp.Matrix([[.8536, .1464, -.5, -.0148],
                      [.1464, .8536, .5, .0148],
                      [.5, -.5, .7071, .26],
                      [0, 0, 0, 1]])*\
           get_rotation_matrix_from_uvh(q2,q3,jtHeight+jt1MassPos)

fkJt1End = get_rotation_matrix_from_uvh(q0,q1,jtHeight)*\
           sp.Matrix([[.8536, .1464, -.5, -.0148],
                      [.1464, .8536, .5, .0148],
                      [.5, -.5, .7071, .26],
                      [0, 0, 0, 1]])*\
           get_rotation_matrix_from_uvh(q2,q3,jtHeight)

fkL1CoM =  get_rotation_matrix_from_uvh(q0,q1,jtHeight)*\
           sp.Matrix([[.8536, .1464, -.5, -.0148],
                      [.1464, .8536, .5, .0148],
                      [.5, -.5, .7071, .26],
                      [0, 0, 0, 1]])*\
           get_rotation_matrix_from_uvh(q2,q3,jtHeight)*\
           sp.Matrix([[.8536, .1464, -.5, -.0148],
                      [.1464, .8536, .5, .0148],
                      [.5, -.5, .7071, l1MassPos],
                      [0, 0, 0, 1]])


fkL1End =  get_rotation_matrix_from_uvh(q0,q1,jtHeight)*\
           sp.Matrix([[.8536, .1464, -.5, -.0148],
                      [.1464, .8536, .5, .0148],
                      [.5, -.5, .7071, .26],
                      [0, 0, 0, 1]])*\
           get_rotation_matrix_from_uvh(q2,q3,jtHeight)*\
           sp.Matrix([[.8536, .1464, -.5, -.0148],
                      [.1464, .8536, .5, .0148],
                      [.5, -.5, .7071, .26],
                      [0, 0, 0, 1]])

fkJt2CoM = get_rotation_matrix_from_uvh(q0,q1,jtHeight)*\
           sp.Matrix([[.8536, .1464, -.5, -.0148],
                      [.1464, .8536, .5, .0148],
                      [.5, -.5, .7071, .26],
                      [0, 0, 0, 1]])*\
           get_rotation_matrix_from_uvh(q2,q3,jtHeight)*\
           sp.Matrix([[.8536, .1464, -.5, -.0148],
                      [.1464, .8536, .5, .0148],
                      [.5, -.5, .7071, .26],
                      [0, 0, 0, 1]])*\
           get_rotation_matrix_from_uvh(q4,q5,jtHeight+jt2MassPos)

fkJt2End = get_rotation_matrix_from_uvh(q0,q1,jtHeight)*\
           sp.Matrix([[.8536, .1464, -.5, -.0148],
                      [.1464, .8536, .5, .0148],
                      [.5, -.5, .7071, .26],
                      [0, 0, 0, 1]])*\
           get_rotation_matrix_from_uvh(q2,q3,jtHeight)*\
           sp.Matrix([[.8536, .1464, -.5, -.0148],
                      [.1464, .8536, .5, .0148],
                      [.5, -.5, .7071, .26],
                      [0, 0, 0, 1]])*\
           get_rotation_matrix_from_uvh(q4,q5,jtHeight)

def get_ginv(g):
    RT = g[0:3,0:3].T
    tinv = -RT*(g[0:3,3])
    top_part = sp.Matrix.hstack(RT,tinv)
    bot_part = sp.Matrix([[0,0,0,1]])

    ginv = sp.Matrix.vstack(top_part,bot_part)
    return ginv

def get_jac_column(fk,q):
    dgdu = sp.diff(fk,q)
    ginv = get_ginv(fk)
    dgdu_ginv = ginv*dgdu
                   
    omega = sp.Matrix([[dgdu_ginv[2,1]],
                       [-dgdu_ginv[2,0]],
                       [dgdu_ginv[1,0]]])
    xdot = dgdu_ginv[0:3,3]
    return sp.Matrix.vstack(xdot,omega)

def get_jac(fk):
    
    J =  sp.Matrix.hstack(get_jac_column(fk,q0),
                          get_jac_column(fk,q1),
                          get_jac_column(fk,q2),
                          get_jac_column(fk,q3),
                          get_jac_column(fk,q4),
                          get_jac_column(fk,q5))
    print("Jac done")
    return J

jacJt0CoM = get_jac(fkJt0CoM)
jacL0CoM = get_jac(fkL0CoM)
jacJt1CoM = get_jac(fkJt1CoM)
jacL1CoM = get_jac(fkL1CoM)
jacJt2CoM = get_jac(fkJt2CoM)

###---------------------Dynamics---------------------------###

q = sp.Matrix([[q0],[q1],[q2],[q3],[q4],[q5]])
qdot = sp.diff(q,t)
qddot = sp.diff(qdot,t)

print("Calculating gravity")
gvec = sp.Matrix([[0],
                  [0],
                  [1]])

V = g*(fkJt0CoM[0:3,3].T*gvec*jt0Mass +\
       fkL0CoM[0:3,3].T*gvec*l0Mass +\
       fkJt1CoM[0:3,3].T*gvec*jt1Mass +\
       fkL1CoM[0:3,3].T*gvec*l1Mass +\
       fkJt2CoM[0:3,3].T*gvec*jt2Mass)

grav = sp.diff(V,q)[:,:,0,0]

print("Calculating Mass Matrix")

M = jt0Mass*jacJt0CoM[0:3,:].T*jacJt0CoM[0:3,:] + jacJt0CoM[3:6,:].T*jt0Inertia*jacJt0CoM[3:6,:] +\
    l0Mass*jacL0CoM[0:3,:].T*jacL0CoM[0:3,:] + jacL0CoM[3:6,:].T*l0Inertia*jacL0CoM[3:6,:] +\
    jt1Mass*jacJt1CoM[0:3,:].T*jacJt1CoM[0:3,:] + jacJt1CoM[3:6,:].T*jt1Inertia*jacJt1CoM[3:6,:] +\
    l1Mass*jacL1CoM[0:3,:].T*jacL1CoM[0:3,:] + jacL1CoM[3:6,:].T*l1Inertia*jacL1CoM[3:6,:] +\
    jt2Mass*jacJt2CoM[0:3,:].T*jacJt2CoM[0:3,:] + jacJt2CoM[3:6,:].T*jt2Inertia*jacJt2CoM[3:6,:]

# print "Calculating Coriolis Matrix"
# C = sp.zeros(n,n)
# for i in xrange(0,n):
#     print "i: ",i
#     for j in xrange(0,n):
#         print "j: ",j
#         Cij = 0
#         for k in xrange(0,n):
#             print "k: ",k
#             Cij += .5*(sp.diff(M[i,j],q[k]) + sp.diff(M[i,k],q[j]) - sp.diff(M[j,k],q[i]))
#         C[i,j] = deepcopy(Cij)
# print "Calculated Coriolis Matrix"            


# Get rid of time dependence within sympy
def dummify_undefined_functions(expr):
    mapping = {}    

    for der in expr.atoms(q0,q1,q2,q3,q4,q5):
        f_name = der.func.__name__
        name = f_name
        mapping[der] = sp.Symbol(name)
        
    return expr.subs(mapping)

print("Generating C code...")
from sympy.utilities.codegen import codegen

filename = "dynamics"
c_code = ''
c_code+='#include "bellowsarmdynamics.h"\n#include <math.h>\n\nnamespace py = pybind11;\n\n'

###---------Generate code for gravity torques----------###
print("generating code for gravity torques")

c_code+="py::array calc_grav(std::vector<double> q, double jt0Mass, double jt1Mass, double jt2Mass, double g)\n{"
c_code+="\n\t double out["+str(n)+"];\n"

grav_terms_list = []
for i in range(0,n):
    grav_terms_list.append(grav[i])
    
grav_sub_exprs, grav_terms = sp.cse(grav_terms_list)

for i in range(0,len(grav_sub_exprs)):
    c_code += "\n\tdouble " + sp.ccode(grav_sub_exprs[i][0])+" = "+sp.ccode(dummify_undefined_functions(grav_sub_exprs[i][1]))+";"

for i in range(0,n):
    c_code += "\n\t out["+str(i)+"] = "+sp.ccode(dummify_undefined_functions(grav_terms[i]))+";"
c_code+="\n\treturn py::array("+str(n)+",out);\n"
c_code+="\n}\n\n"

###---------Generate code for mass matrix----------###
print("generating code for mass matrix")

c_code+="py::array calc_M(std::vector<double> q, double jt0Mass, double jt1Mass, double jt2Mass, double jt0Ixx, double jt0Iyy, double jt0Izz, double jt0Ixy, double jt0Ixz, double jt0Iyz, double jt1Ixx, double jt1Iyy, double jt1Izz, double jt1Ixy, double jt1Ixz, double jt1Iyz, double jt2Ixx, double jt2Iyy, double jt2Izz, double jt2Ixy, double jt2Ixz, double jt2Iyz, double g)\n{"
c_code+="\n\t double out["+str(n*n)+"];\n"

M_terms_list = []
for i in range(0,n):
    for j in range(0,n):
        M_terms_list.append(M[i,j])
    
M_sub_exprs, M_terms = sp.cse(M_terms_list)

for i in range(0,len(M_sub_exprs)):
    c_code += "\n\tdouble " + sp.ccode(M_sub_exprs[i][0])+" = "+sp.ccode(dummify_undefined_functions(M_sub_exprs[i][1]))+";"

for i in range(0,n):
    for j in range(0,n):
        c_code += "\n\t out["+str(i*n+j)+"] = "+sp.ccode(dummify_undefined_functions(M_terms[i*n+j]))+";"
c_code+="\n\treturn py::array("+str(n*n)+",out);\n"        
c_code+="\n}\n\n"


###---------Generate code for forward kinematics----------###
print("generating code for forward kinematics")


def print_fk_function(code,fk,link):
    code+="py::array fk"+str(link)+"(std::vector<double> q, double jtHeight)\n{"
    code+="\n\t double out[16];\n"

    fk_terms_list = []
    for i in range(0,4):
        for j in range(0,4):
            fk_terms_list.append(fk[i,j])
    
    fk_sub_exprs, fk_terms = sp.cse(fk_terms_list)

    for i in range(0,len(fk_sub_exprs)):
        code += "\n\tdouble " + sp.ccode(fk_sub_exprs[i][0])+" = "+sp.ccode(dummify_undefined_functions(fk_sub_exprs[i][1]))+";"

    for i in range(0,4):
        for j in range(0,4):
            code += "\n\t out["+str(i*4+j)+"] = "+sp.ccode(dummify_undefined_functions(fk_terms[i*4+j]))+";"

    code+="\n\treturn py::array(16,out);\n"
    code+="\n}\n\n"
    return code

# c_code = print_fk_function(c_code,fkJt0CoM,'Jt0CoM')
c_code = print_fk_function(c_code,fkJt0End,'Jt0End')
c_code = print_fk_function(c_code,fkL0End,'L0End')
# c_code = print_fk_function(c_code,fkJt1CoM,'Jt1CoM')
c_code = print_fk_function(c_code,fkJt1End,'Jt1End')
c_code = print_fk_function(c_code,fkL1End,'L1End')
# c_code = print_fk_function(c_code,fkJt2CoM,'Jt2CoM')
c_code = print_fk_function(c_code,fkJt2End,'Jt2End')

###---------Generate code for jacobians----------###

# def print_jac_function(code,jac,link):
#     code+="py::array jac"+str(link)+"(std::vector<double> q, double jt0Height, double l0Height, double jt1Height, double l1Height, double jt2Height, double l2Height)\n{"
#     code+="\n\t double out["+str(3*n)+"];\n"
#     for i in xrange(0,3):
#         for j in xrange(0,n):
#             code += "\n\t out["+str(i*n+j)+"] = "+sp.ccode(jac[i,j].as_expr())+";"
#     code+="\n\treturn py::array("+str(3*n)+",out);\n"
#     code+="\n}\n\n"
#     return code

# c_code = print_jac_function(c_code,jacJt0CoM,'Jt0CoM')
# # c_code = print_jac_function(c_code,jacL0COM,'L0CoM')
# c_code = print_jac_function(c_code,jacJt1CoM,'Jt1CoM')
# # c_code = print_jac_function(c_code,jacL1COM,'L1CoM')
# c_code = print_jac_function(c_code,jacJt2CoM,'Jt2CoM')
# # c_code = print_jac_function(c_code,jacL2COM,'L2CoM')

###---------Final code cleanup and writing to file----------###

for i in range(0,n):
    while c_code.find('qd'+str(i)) != -1:
        c_code = c_code.replace('qd'+str(i),'qd['+str(i)+']')
    while c_code.find('q'+str(i)) != -1:
        c_code = c_code.replace('q'+str(i),'q['+str(i)+']')

c_code +='\nPYBIND11_MODULE(bellowsarmdynamics,m) {'
c_code +='\n\tm.def("calc_M",&calc_M,"a function description");'
c_code +='\n\tm.def("calc_grav",&calc_grav,"a function description");'
c_code +='\n\tm.def("fkJt0CoM",&fkJt0CoM,"a function description");'
c_code +='\n\tm.def("fkJt0End",&fkJt0End,"a function description");'
c_code +='\n\tm.def("fkL0End",&fkL0End,"a function description");'
c_code +='\n\tm.def("fkJt1CoM",&fkJt1CoM,"a function description");'
c_code +='\n\tm.def("fkJt1End",&fkJt1End,"a function description");'
c_code +='\n\tm.def("fkL1End",&fkL1End,"a function description");'
c_code +='\n\tm.def("fkJt2CoM",&fkJt2CoM,"a function description");'
c_code +='\n\tm.def("fkJt2End",&fkJt2End,"a function description");'
c_code +='\n}'
    
cf = open(filename+".c",'w')

c_code.replace('(t)','')

cf.write(c_code)
cf.close()
