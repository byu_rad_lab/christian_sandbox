import sympy as sp
import numpy as np
from copy import deepcopy
sp.init_printing(use_unicode=True)

def get_transformation(theta,l):
    g = sp.Matrix([[sp.cos(theta), -sp.sin(theta),0, -l*sp.sin(theta)],
                   [sp.sin(theta), sp.cos(theta),0, l*sp.cos(theta)],
                   [0, 0, 1, 0],
                   [0,0,0,1]])
    return g

def get_ginv(g):
    RT = g[0:3,0:3].T
    tinv = -RT*(g[0:3,3])
    top_part = sp.Matrix.hstack(RT,tinv)
    bot_part = sp.Matrix([[0,0,0,1]])
    return sp.Matrix.vstack(top_part,bot_part)

def get_jac_column(fk,q):
    dgdu = sp.diff(fk,q)
    ginv = get_ginv(fk)
    dgdu_ginv = ginv*dgdu
    
    omega = sp.Matrix([[dgdu_ginv[2,1]],
                       [-dgdu_ginv[2,0]],
                       [dgdu_ginv[1,0]]])
    xdot = dgdu_ginv[0:3,3]
    return sp.Matrix.vstack(xdot,omega)

def get_jac(fk,qs):
    J = sp.Matrix(np.zeros((6,n)))
    for i in range(0,n):
        J[:,i] = get_jac_column(fk,qs['q'+str(i)])
    return J
    

def generate_code_for_n_link(n):

    l,I,m,g = sp.symbols('l I m g',real=True)

    qs = {}
    for i in range(0,n):
        qs['q'+str(i)] = sp.Symbol('q'+str(i))

    qds = {}
    for i in range(0,n):
        qds['qd'+str(i)] = sp.Symbol('qd'+str(i))

    q = sp.zeros(n,1)
    for i in range(0,n):
        q[i] = qs['q'+str(i)]

    print("Calculating FK")
        
    fkCoMs = {}
    fkCoMs['fkCoM0'] = get_transformation(qs['q0'],l/2)    
    for i in range(1,n):
        # fkCoMs['fkCoM'+str(i)] = sp.trigsimp(fkCoMs['fkCoM'+str(i-1)]*get_transformation(0,l/2))*get_transformation(qs['q'+str(i)],l/2)
        fkCoMs['fkCoM'+str(i)] = fkCoMs['fkCoM'+str(i-1)]*get_transformation(0,l/2)*get_transformation(qs['q'+str(i)],l/2)
    
    # fkEnds = {}
    # fkEnds['fkEnd0'] = get_transformation(qs['q0'],l)        
    # for i in xrange(1,n):
    #     fkEnds['fkEnd'+str(i)] = fkEnds['fkEnd'+str(i-1)]*get_transformation(qs['q'+str(i)],l)

    print("Calculating Jacobians")
    
    jacCoMs = {}
    # jacEnds = {}
    for i in range(0,n):
        jacCoMs['jacCoM'+str(i)] = get_jac(fkCoMs['fkCoM'+str(i)],qs)
        # jacEnds['jacEnd'+str(i)] = get_jac(fkEnds['fkEnd'+str(i)],qs)

    print("Calculating Gravity terms")
        
    gvec = sp.Matrix([[0],
                      [1],
                      [0]])

    V = 0
    for i in range(0,n):
        V += (fkCoMs['fkCoM'+str(i)][0:3,3].T*gvec*m*g)[0]

    grav = sp.diff(V,q)

    print("Calculating Mass Matrix")

    M = sp.zeros(n,n)
    for i in range(0,n):
        M += m*jacCoMs['jacCoM'+str(i)][0:3,:].T*jacCoMs['jacCoM'+str(i)][0:3,:]
        M += I*jacCoMs['jacCoM'+str(i)][3:6,:].T*jacCoMs['jacCoM'+str(i)][3:6,:]

    print("Calculating Coriolis Matrix")
    C = sp.zeros(n,n)
    for i in range(0,n):
        for j in range(0,n):
            print("i: ", i,"   j: ",j)
            Cij = 0
            for k in range(0,n):
                Cij += .5*(sp.diff(M[i,j],qs['q'+str(k)]) + sp.diff(M[i,k],qs['q'+str(j)]) - sp.diff(M[j,k],qs['q'+str(i)]))*qds['qd'+str(k)]
            C[i,j] = deepcopy(Cij)


    print("Generating C code...")
    from sympy.utilities.codegen import codegen

    def print_sympy_for_cpp(expr,eigen_matrix,cse=False):
        code = ''
        n,m = expr.shape
        terms_list = []
        for i in range(0,n):
            for j in range(0,m):
                terms_list.append(expr[i,j])

        if(cse==True):
            sub_exprs, terms_list = sp.cse(terms_list)

            for i in range(0,len(sub_exprs)):
                code += "\n\tdouble " + sp.ccode(sub_exprs[i][0])+" = "+sp.ccode((sub_exprs[i][1]))+";"

        for i in range(0,n):
            if(m>1):
                for j in range(0,m):
                    code += "\n\t(*"+eigen_matrix+")("+str(i)+","+str(j)+") = "+sp.ccode((terms_list[i*n+j]))+";"
            else:
                code += "\n\t(*"+eigen_matrix+")("+str(i)+","+str(j)+") = "+sp.ccode((terms_list[i]))+";"
        return code


    def print_sympy_for_python_wrapping(expr,cse=False):
        code = ''
        n,m = expr.shape
        terms_list = []
        for i in range(0,n):
            for j in range(0,m):
                terms_list.append(expr[i,j])

        if(cse==True):
            print('doing cse')
            sub_exprs, terms_list = sp.cse(terms_list)

            for i in range(0,len(sub_exprs)):
                code += "\n\tdouble " + sp.ccode(sub_exprs[i][0])+" = "+sp.ccode((sub_exprs[i][1]))+";"

        for i in range(0,n):
            if(m>1):
                for j in range(0,m):
                    code += "\n\t out["+str(i*n+j)+"] = "+sp.ccode((terms_list[i*n+j]))+";"
            else:
                code += "\n\t out["+str(i)+"] = "+sp.ccode((terms_list[i]))+";"
        return code
        


    filename = 'n'+str(n)+"linkdynamics"
    c_code = ''

    print("Generating Mass matrix code")
    c_code+='#include "'+'n'+str(n)+'linkdynamics.h"\n#include <Eigen/LU>\n#include <math.h>\n\nnamespace py = pybind11;\n\n'

    c_code+="py::array calc_M_py(std::vector<double> q,double l,double m,double I)\n{"
    c_code+="\n\t double out["+str(n*n)+"];\n"
    c_code+= print_sympy_for_python_wrapping(M)
    c_code+="\n\treturn py::array("+str(n*n)+",out);\n"        
    c_code+="\n}\n\n"

    c_code+="void calc_M(std::vector<double> q,Eigen::MatrixXd * M,double l,double m,double I)\n{\n"
    c_code+= print_sympy_for_cpp(M,'M')
    c_code+="\n}\n\n"



    print("Generating gravity vector code")
    c_code+="py::array calc_grav_py(std::vector<double> q,double l,double m,double g)\n{"
    c_code+="\n\t double out["+str(n)+"];\n"
    c_code+= print_sympy_for_python_wrapping(grav)
    c_code+="\n\treturn py::array("+str(n)+",out);\n}\n\n"

    c_code+="void calc_grav(std::vector<double> q,Eigen::MatrixXd * grav,double l,double m,double g)\n{\n"
    c_code+= print_sympy_for_cpp(grav,'grav')
    c_code+="\n}\n\n"



    c_code+="py::array calc_C_py(std::vector<double> q,std::vector<double> qd,double l,double m,double I)\n{"
    c_code+="\n\t double out["+str(n*n)+"];\n"

    print("Generating Coriolis Matrix code")
    c_code+= print_sympy_for_python_wrapping(C)
    c_code+="\n\treturn py::array("+str(n*n)+",out);\n}\n\n"

    c_code+="void calc_C(std::vector<double> q,std::vector<double> qd,Eigen::MatrixXd * C,double l,double m,double I)\n{\n"
    c_code+= print_sympy_for_cpp(C,'C')
    c_code+="\n}\n\n"



    def print_fk_function(code,fk,link):
        code+="py::array fk"+str(link)+"_py(std::vector<double> q,double l)\n{"
        code+="\n\t double out[16];\n"

        code += print_sympy_for_python_wrapping(fk)

        code+="\n\treturn py::array(16,out);\n"
        code+="\n}\n\n"
        return code

    print("Generating FK code")
    for i in range(0,n):
        c_code = print_fk_function(c_code,fkCoMs['fkCoM'+str(i)],'CoM'+str(i))

    for i in range(n-1,-1,-1):        
        c_code = c_code.replace('qd'+str(i),'qd['+str(i)+']')
        c_code = c_code.replace('q'+str(i),'q['+str(i)+']')


    
    print("Writing code to file")            
    cf = open(filename+".c",'w')

    cf.write(c_code)
    cf.close()

if __name__=='__main__':

    # generate_code_for_n_link(8)    

    for n in range(14,25):
        generate_code_for_n_link(n)
