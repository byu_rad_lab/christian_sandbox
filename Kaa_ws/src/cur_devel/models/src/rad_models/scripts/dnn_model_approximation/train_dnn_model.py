import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from rad_models.InvertedPendulum import InvertedPendulum
from rad_models.Threelink import Threelink
from rad_models.PDThreelink import PDThreelink
from rad_models.PDKneedWalker import PDKneedWalker
from rad_models.BellowsArm2 import BellowsArm
from rad_models.MyNet import *

np.set_printoptions(precision=2)

# Make a first principles model
# sys = PDKneedWalker()
# sys = Threelink()
# sys = PDThreelink()
# sys = InvertedPendulum()
sys = BellowsArm()

dt = .02

# model = MyNet(input_size=n+m,output_size=n/2).cuda()
# model = DynamicsNet(sys.numStates,sys.numInputs).cuda()
# model = SmallDynamicsNet(sys.numStates,sys.numInputs).cuda()
model = BellowsArmNet(sys.numStates,sys.numInputs).cuda()
# model = BellowsArmResNet(sys.numStates,sys.numInputs).cuda()

lr = 1e-2
optimizer = torch.optim.Adam([p for p in model.parameters() if p.requires_grad], lr)
loss_fn = torch.nn.MSELoss()


epochs = 10000000
batch_size = 1000
torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True

inputs = np.zeros([batch_size,sys.numStates+sys.numInputs])
outputs = np.zeros([batch_size,sys.numStates])

for epoch in range(0,epochs):
    
    optimizer.zero_grad()
    
    x = 1.0*np.pi*np.random.rand(sys.numStates,batch_size) + -np.pi/2.0
    x[12:18,:] = x[12:18,:]*2.0

    uMax = 400.0
    uMin = 8.0
    uRange = (uMax-uMin)
    x[0:12,:] = np.random.rand(12,batch_size)*uRange + uMin
    u = uRange*np.random.rand(sys.numInputs,batch_size) + uMin


    # output = sys.forward_simulate_dt(x,u,dt,wrapAngle=False)
    next_states = sys.forward_simulate_dt(x,u,dt)
    output = next_states[12:18,:]-x[12:18,:]

    inputs = np.vstack([x,u]).T    
    inputs[:,0:12] = inputs[:,0:12]/400.0
    inputs[:,24:36] = inputs[:,24:36]/400.0

    input_var = Variable(torch.from_numpy(inputs).float(),requires_grad=True).cuda()
    output_var = Variable(torch.from_numpy(output.T).float(),requires_grad=False).cuda()

    output_pred = model(input_var)

    # if epoch==500:
    #     # This freezes the alphas
    #     children = model.children()
    #     child = children.next()
    #     params = child.parameters()
    #     for param in params:
    #         param.requires_grad = False
            
    #     lr = 1e-3
    #     batch_size=100
    #     for param_group in optimizer.param_groups:
    #         param_group['lr'] = lr

    #     loss_fn = torch.nn.MSELoss()
    #     # loss_fn = torch.nn.L1Loss()

    # if epoch<500:
    #     loss = loss_fn(output_pred[:,0:12],output_var[:,0:12])
    if(0):
        pass
    else:
        loss = loss_fn(output_pred,output_var)
        # loss += loss_fn(output_pred[:,12:14],output_var[:,12:14])*100.0
        # loss += loss_fn(output_pred[:,14:16],output_var[:,14:16])*10.0
        # loss += loss_fn(output_pred[:,16:18],output_var[:,16:18])*1.0

    loss.backward()
    optimizer.step()

    # if epoch%10==0:
    if epoch%100==0:
        # torch.save(model, '../../src/rad_models/learned_models/ThreelinkModel.pt')
        # torch.save(model, '../../src/rad_models/learned_models/BellowsArmSmallModel.pt')
        torch.save(model, '../../src/rad_models/learned_models/BellowsArmSmallModelMorePrs2Torque.pt')
        # torch.save(model, '../../src/rad_models/learned_models/BellowsArmSmallModelFullDampingHalfSpring.pt')
        # torch.save(model, '../../src/rad_models/learned_models/BellowsArmResNetModel.pt')

        print("\nEpoch: ",epoch)
        print("Learning Rate: ",lr)        
        print("Average State Prediction Error: ",loss.cpu().data.numpy()/batch_size)

    if epoch%1000==0:
        lr = lr*.9
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr
        
    

    
