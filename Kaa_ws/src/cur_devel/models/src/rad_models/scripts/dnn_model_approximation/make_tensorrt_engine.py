import numpy as np
import torch
import tensorrt as trt
import pycuda.driver as cuda
import pycuda.autoinit


    # def forward_simulate_dt(self,x,u,dt=.01):
    #     u = np.reshape(u,[u.shape[0],-1])
    #     inputs = np.vstack([x,u]).T
    #     if self.use_gpu==True:
    #         self.input_var = Variable(torch.from_numpy(inputs).float(),requires_grad=False).cuda()
    #     else:
    #         self.input_var = Variable(torch.from_numpy(inputs).float(),requires_grad=False)
    #     x = self.model(self.input_var).cpu().detach().numpy().T
    #     return x
# Simple helper data class that's a little nicer to use than a 2-tuple.
class HostDeviceMem(object):
    def __init__(self, host_mem, device_mem):
        self.host = host_mem
        self.device = device_mem

    def __str__(self):
        return "Host:\n" + str(self.host) + "\nDevice:\n" + str(self.device)

    def __repr__(self):
        return self.__str__()
    
# Allocates all buffers required for an engine, i.e. host/device inputs/outputs.
def allocate_buffers(engine):
    inputs = []
    outputs = []
    bindings = []
    stream = cuda.Stream()
    for binding in engine:
        size = trt.volume(engine.get_binding_shape(binding)) * engine.max_batch_size
        dtype = trt.nptype(engine.get_binding_dtype(binding))
        # Allocate host and device buffers
        host_mem = cuda.pagelocked_empty(size, dtype)
        device_mem = cuda.mem_alloc(host_mem.nbytes)
        # Append the device buffer to device bindings.
        bindings.append(int(device_mem))
        # Append to the appropriate list.
        if engine.binding_is_input(binding):
            inputs.append(HostDeviceMem(host_mem, device_mem))
        else:
            outputs.append(HostDeviceMem(host_mem, device_mem))
    return inputs, outputs, bindings, stream

# This function is generalized for multiple inputs/outputs.
# inputs and outputs are expected to be lists of HostDeviceMem objects.
def do_inference(context, bindings, inputs, outputs, stream, batch_size=1):
    # Transfer input data to the GPU.
    [cuda.memcpy_htod_async(inp.device, inp.host, stream) for inp in inputs]
    # Run inference.
    context.execute_async(batch_size=batch_size, bindings=bindings, stream_handle=stream.handle)
    # Transfer predictions back from the GPU.
    [cuda.memcpy_dtoh_async(out.host, out.device, stream) for out in outputs]
    # Synchronize the stream
    stream.synchronize()
    # Return only the host outputs.
    return [out.host for out in outputs]

def make_tensorrt_network_from_pytorch_model(model,network):
    # Populate the network
    input_tensor = network.add_input('x',dtype=trt.float32,shape=(1,1,36))

    fc0_w = model.fc0.weight.cpu().numpy()
    fc0_b = model.fc0.bias.cpu().numpy()
    fc0 = network.add_fully_connected(input=input_tensor, num_outputs=100, kernel=fc0_w, bias=fc0_b)

    relu0 = network.add_activation(input=fc0.get_output(0),type=trt.ActivationType.RELU)

    fc1_w = model.fc1.weight.cpu().numpy()
    fc1_b = model.fc1.bias.cpu().numpy()
    fc1 = network.add_fully_connected(input=relu0.get_output(0), num_outputs=200, kernel=fc1_w, bias=fc1_b)

    relu1 = network.add_activation(input=fc1.get_output(0),type=trt.ActivationType.RELU)

    fc2_w = model.fc2.weight.cpu().numpy()
    fc2_b = model.fc2.bias.cpu().numpy()
    fc2 = network.add_fully_connected(input=relu1.get_output(0), num_outputs=400, kernel=fc2_w, bias=fc2_b)

    relu2 = network.add_activation(input=fc2.get_output(0),type=trt.ActivationType.RELU)
    
    fc3_w = model.fc3.weight.cpu().numpy()
    fc3_b = model.fc3.bias.cpu().numpy()
    fc3 = network.add_fully_connected(input=fc2.get_output(0), num_outputs=200, kernel=fc3_w, bias=fc3_b)

    relu3 = network.add_activation(input=fc3.get_output(0),type=trt.ActivationType.RELU)
    
    fc4_input = network.add_elementwise(relu3.get_output(0),fc1.get_output(0),trt.ElementWiseOperation.SUM)
    
    fc4_w = model.fc4.weight.cpu().numpy()
    fc4_b = model.fc4.bias.cpu().numpy()
    fc4 = network.add_fully_connected(input=fc4_input.get_output(0), num_outputs=100, kernel=fc4_w, bias=fc4_b)

    relu4 = network.add_activation(input=fc4.get_output(0),type=trt.ActivationType.RELU)

    fc5_input = network.add_elementwise(relu4.get_output(0),fc0.get_output(0),trt.ElementWiseOperation.SUM)
    fc5_w = model.fc5.weight.cpu().numpy()
    fc5_b = model.fc5.bias.cpu().numpy()
    fc5 = network.add_fully_connected(input=fc5_input.get_output(0), num_outputs=6, kernel=fc5_w, bias=fc5_b)
    
    network.mark_output(tensor=fc5.get_output(0))

    return network
        
if __name__=='__main__':
    model = torch.load('/home/radlab/git/byu/cur_devel/models/src/rad_models/src/rad_models/learned_models/BellowsArmSmallModel.pt').eval()

    for param in model.parameters():
        param.requires_grad = False

    builder = trt.Builder(trt.Logger())
    network = builder.create_network()

    network = make_tensorrt_network_from_pytorch_model(model,network)

    # Build the TensorRT Engine
    builder.max_batch_size = 100
    engine = builder.build_cuda_engine(network)

    inputs, outputs, bindings, stream = allocate_buffers(engine)
    context = engine.create_execution_context()

    batch_size = 100
    x = np.random.rand(36,batch_size)
    x = np.array(x,dtype=np.float32)

    inputs[0].host = x

    print("inputs: ",inputs)

    do_inference(context,bindings=bindings,inputs=inputs,outputs=outputs,stream=stream,batch_size=batch_size)

    print('outputs: ',outputs)
    
    

