import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from rad_models.Threelink import Threelink
from rad_models.MyNet import *

np.set_printoptions(precision=2)

# Make a first principles model - this could be data from a real robot instead or as well. 
sys = Threelink()

dt = .01

model = SmallDynamicsNet(sys.numStates,sys.numInputs).cuda()

lr = 1e-2
optimizer = torch.optim.Adam([p for p in model.parameters() if p.requires_grad], lr)
loss_fn = torch.nn.MSELoss()

epochs = 10000000
batch_size = 1000
torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True

inputs = np.zeros([batch_size,sys.numStates+sys.numInputs])
outputs = np.zeros([batch_size,sys.numStates])

for epoch in range(0,epochs):
    
    optimizer.zero_grad()

    # make random initial states
    x = np.random.randn(sys.numStates,batch_size)*np.pi/3.0
    # x = 2.0*np.pi*np.random.rand(sys.numStates,batch_size) + -np.pi
    # x[0:3,:] = x[0:3,:]*2.0

    # generating random inputs
    uMax = 10.0
    uMin = -10.0
    uRange = (uMax-uMin)
    u = uRange*np.random.rand(sys.numInputs,batch_size) + uMin
    
    u = uRange*np.random.rand(sys.numInputs,batch_size) + uMin    

    # this output data is from real system or from physics-based model - the method of sampling (random inputs, random outputs, or using time windows of data, not clear what is best at all ...)
    output = sys.forward_simulate_dt(x,u,dt,wrapAngle=False)
    
    # for neural network, the inputs are all of our states and inputs
    inputs = np.vstack([x,u]).T    
    
    #scaling inputs (or whitening) to have mean of zero and on order of scale of one (for optimization in NN) 
    # we should look into scaling more ... but hard to know how to whiten velocities, how do we know what the max will be even? or the variance? 
    inputs[:,6:9] = inputs[:,6:9]/sys.uMax

    input_var = Variable(torch.from_numpy(inputs).float(),requires_grad=True).cuda()
    output_var = Variable(torch.from_numpy(output.T).float(),requires_grad=False).cuda()

    output_pred = model(input_var)

    loss = loss_fn(output_pred,output_var)

    loss.backward()
    optimizer.step()

    if epoch%100==0:
        torch.save(model, '../../src/rad_models/learned_models/ThreelinkModel.pt')

        print("\nEpoch: ",epoch)
        print("Learning Rate: ",lr)        
        print("Average State Prediction Error: ",loss.cpu().data.numpy()/batch_size)

    if epoch%1000==0:
        lr = lr*.9
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr
        