import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter


np.set_printoptions(precision=3)

filename = 'data_for_training_dnn.mat'

data = sio.loadmat(filename)

q = data['q']
q_filt = savgol_filter(q,251,2,axis=1)

qd = data['qd']
qd_filt = savgol_filter(qd,251,2,axis=1)

p = data['p']
p_filt = savgol_filter(p,251,2,axis=1)

pref = data['pref']
pref_filt = savgol_filter(pref,251,2,axis=1)

q = q[:,0::20]
q_filt = q_filt[:,0::20]
qd = qd[:,0::20]
qd_filt = qd_filt[:,0::20]
p = p[:,0::20]
p_filt = p_filt[:,0::20]
pref = pref[:,0::20]
pref_filt = pref_filt[:,0::20]

if __name__ == '__main__':

    plt.ion()
    plt.figure()
    for i in range(0,6):
        plt.subplot(3,2,i+1)
        plt.plot(q[i,:].T,'b')
        plt.plot(q_filt[i,:].T,'r')
        plt.ylabel('Jt'+str(i)+' angle (Rad)')

    plt.figure()
    for i in range(0,6):
        plt.subplot(3,2,i+1)
        plt.plot(qd[i,:].T,'b')
        plt.plot(qd_filt[i,:].T,'r')
        plt.ylabel('Jt'+str(i)+' velocity (Rad/s)')

    plt.figure()
    for i in range(0,12):
        plt.subplot(6,2,i+1)
        plt.plot(p[i,:].T,'b')
        plt.plot(p[i,:].T,'r')
        plt.plot(pref[i,:].T,'r--')
        plt.plot(pref[i,:].T,'c--')
        plt.ylabel('Pressure '+str(i)+' (kPa)')

    plt.show()

    plt.pause(1000)
    
