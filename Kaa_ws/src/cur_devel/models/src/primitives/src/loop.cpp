/* Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 *protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 *MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED,
 *AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR
 *POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
 *TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#include "primitives/loop.h"

#include <string>

namespace primitives {
namespace loop {

Loop::Loop(const std::string &name)
    : thread::Thread(name),
      time_(ros::Time::now()),
      timeDiff_(ros::Duration()),
      period_(ros::Duration()),
      timeNext_(time_),
      startTime_(time_),
      logPeriod_(ros::Duration()),
      logTimeNext_(time_),
      loopCnt_(0),
      overrunCnt_(0) {}

Loop::Loop(thread::app_type_t type, thread::thread_prio_t prio)
    : thread::Thread("Loop", type, prio),
      time_(ros::Time::now()),
      timeDiff_(ros::Duration()),
      period_(ros::Duration()),
      timeNext_(time_),
      startTime_(time_),
      logPeriod_(ros::Duration()),
      logTimeNext_(time_),
      loopCnt_(0),
      overrunCnt_(0) {}

Loop::Loop(const std::string &name, thread::app_type_t type, thread::thread_prio_t prio)
    : thread::Thread(name, type, prio),
      time_(ros::Time::now()),
      timeDiff_(ros::Duration()),
      period_(ros::Duration()),
      timeNext_(time_),
      startTime_(time_),
      logPeriod_(ros::Duration()),
      logTimeNext_(time_),
      loopCnt_(0),
      overrunCnt_(0) {}

Loop::Loop(const std::string &name, const ros::Duration &period)
    : thread::Thread(name),
      time_(ros::Time::now()),
      timeDiff_(ros::Duration()),
      period_(period),
      timeNext_(time_),
      startTime_(time_),
      logPeriod_(ros::Duration()),
      logTimeNext_(time_),
      loopCnt_(0),
      overrunCnt_(0) {}

Loop::Loop(thread::app_type_t type,
           thread::thread_prio_t prio,
           const ros::Duration &period)
    : thread::Thread("Loop", type, prio),
      time_(ros::Time::now()),
      timeDiff_(ros::Duration()),
      period_(period),
      timeNext_(time_),
      startTime_(time_),
      logPeriod_(ros::Duration()),
      logTimeNext_(time_),
      loopCnt_(0),
      overrunCnt_(0) {}

Loop::Loop(const std::string &name,
           thread::app_type_t type,
           thread::thread_prio_t prio,
           const ros::Duration &period)
    : thread::Thread(name, type, prio),
      time_(ros::Time::now()),
      timeDiff_(ros::Duration()),
      period_(period),
      timeNext_(time_),
      startTime_(time_),
      logPeriod_(ros::Duration()),
      logTimeNext_(time_),
      loopCnt_(0),
      overrunCnt_(0) {}

Loop::Loop(const std::string &name, const std::string &parentName)
    : thread::Thread(name, parentName),
      time_(ros::Time::now()),
      timeDiff_(ros::Duration()),
      period_(ros::Duration()),
      timeNext_(time_),
      startTime_(time_),
      logPeriod_(ros::Duration()),
      logTimeNext_(time_),
      loopCnt_(0),
      overrunCnt_(0) {}

Loop::Loop(const std::string &name,
           const std::string &parentName,
           thread::app_type_t type,
           thread::thread_prio_t prio)
    : thread::Thread(name, parentName, type, prio),
      time_(ros::Time::now()),
      timeDiff_(ros::Duration()),
      period_(ros::Duration()),
      timeNext_(time_),
      startTime_(time_),
      logPeriod_(ros::Duration()),
      logTimeNext_(time_),
      loopCnt_(0),
      overrunCnt_(0) {}

Loop::Loop(const std::string &name,
           const std::string &parentName,
           const ros::Duration &period)
    : thread::Thread(name, parentName),
      time_(ros::Time::now()),
      timeDiff_(ros::Duration()),
      period_(period),
      timeNext_(time_),
      startTime_(time_),
      logPeriod_(ros::Duration()),
      logTimeNext_(time_),
      loopCnt_(0),
      overrunCnt_(0) {}

Loop::Loop(const std::string &name,
           const std::string &parentName,
           thread::app_type_t type,
           thread::thread_prio_t prio,
           const ros::Duration &period)
    : thread::Thread(name, parentName, type, prio),
      time_(ros::Time::now()),
      timeDiff_(ros::Duration()),
      period_(period),
      timeNext_(time_),
      startTime_(time_),
      logPeriod_(ros::Duration()),
      logTimeNext_(time_),
      loopCnt_(0),
      overrunCnt_(0) {}
Loop::~Loop() {}

void Loop::setLoopPeriod(const ros::Duration &period) { period_ = period; }

ros::Duration Loop::getLoopPeriod(void) const { return period_; }

void Loop::stop(void) {
  // Signal a stop
  stop_ = true;

  // Wait for thread to complete
  join();

  // Reset stop signal
  stop_ = false;
  initialized_ = false;
}

void *Loop::mainLoop(void) {
  running_ = true;

  /*
   * Configure the low level thread priority and scheduler policy
   */
  configureThread();

  /*
   * Initialize your work
   */
  doInit();
  initialized_ = true;
  startTime_ = ros::Time::now();

  // TODO(gabe) - shouldnt be doing doing printing from a potentially real-time loop
  Info(name() + " entering thread work loop");
  do {
    /*
     * Calculate loop timing
     */
    timeDiff_ = ros::Time::now() - time_;
    time_ = ros::Time::now();
    timeNext_ = time_ + period_;

    /*
     * Do your work
     */
    doIterate();

    /*
     * Sleep until next loop iteration
     */
    ros::Time now(ros::Time::now());
    if (!period_.isZero()) {
      /*
       * If now is after the next scheduled time we should run, then we over-ran our
       * alloted loop
       * execution
       * time
       */
      if (timeNext_ > now) {
        ros::Time::sleepUntil(timeNext_);
      } else {
        std::stringstream ss;
        // TODO(gabe) - shouldnt be doing doing printing from a potentially real-time
        // loop
        ss << "over-ran loop time by " << (now - timeNext_).toSec() << "s " << overrunCnt_
           << " of " << loopCnt_ << " iterations have over-run";
        Warn(ss.str());
        overrunCnt_++;
      }
    }
    loopCnt_++;
  } while (!stop_);

  /*
   * Cleanup after your work
   */
  doCleanup();
  initialized_ = false;

  running_ = false;
  stop_ = false;

  return 0;
}

}  // namespace loop
}  // namespace primitives
