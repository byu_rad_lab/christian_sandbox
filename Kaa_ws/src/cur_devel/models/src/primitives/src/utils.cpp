/* Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#include "primitives/utils.h"

#include <cstdlib>
#include <cmath>
#include <limits>

#include "primitives/exception.h"
#include "primitives/stringprintf.h"

namespace primitives {
namespace utils {

double Interp(
    double xmin, double xmax, double ymin, double ymax, double xeval, bool clamp) {
  double dx = xmax - xmin;
  double dy = ymax - ymin;

  if (clamp) {
    Saturate(xeval, xmin, xmax);
  }

  if (dx <= 0.0) {
    if (clamp) {
      return ymax;
    } else {
      throw exception::Exception(stringsprintf(
          "Interp exception: xmax (%f) must be strictly greater than xmin (%f)",
          xmax,
          xmin));
    }
  }

  if (dy < 0.0) {
    throw exception::Exception(stringsprintf(
        "Interp exception: ymax (%f) must be greater than or equal to ymin (%f)",
        ymax,
        ymin));
  }

  return ymax - dy * (xmax - xeval) / dx;
}

double Interp(double xmin, double xmax, double ymin, double ymax, double xeval) {
  return Interp(xmin, xmax, ymin, ymax, xeval, false);
}

// Box-Muller algorithm
// From: https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
double GenerateGaussianNoise(double mu, double sigma) {
  const double epsilon = std::numeric_limits<double>::min();
  const double two_pi = 2.0 * M_PI;

  static double z0, z1;
  static bool generate;
  generate = !generate;  // generates two at a time

  if (!generate) return z1 * sigma + mu;

  double u1, u2;
  do {
    u1 = rand() * (1.0 / RAND_MAX);
    u2 = rand() * (1.0 / RAND_MAX);
  } while (u1 <= epsilon);

  z0 = sqrt(-2.0 * log(u1)) * cos(two_pi * u2);
  z1 = sqrt(-2.0 * log(u1)) * sin(two_pi * u2);
  return z0 * sigma + mu;
}

}  // namespace utils
}  // namespace primitives
