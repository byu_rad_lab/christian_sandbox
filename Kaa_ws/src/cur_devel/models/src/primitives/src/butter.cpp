// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include "primitives/butter.h"

#include <complex>
#include <vector>

namespace primitives {
namespace filter {

typedef std::vector<std::complex<double>> VecComplex;

// calculates a scaling factor so that filters gain is one
double ButterScaleFactor(int order, double cutoff_ratio) {
  double sin_phi = sin(M_PI * cutoff_ratio);
  double half_phi = M_PI * cutoff_ratio / 2.0;
  double temp = 1.0;
  double sin_ord;
  double ord_arg = M_PI / static_cast<double>(2 * order);

  for (int i = 0; i < order / 2; i++) {
    sin_ord = sin(static_cast<double>(2 * i + 1) * ord_arg);
    temp *= 1.0 + sin_phi * sin_ord;
  }

  if (order % 2) {
    temp *= sin(half_phi) + cos(half_phi);
  }

  return pow(sin(half_phi), order) / temp;
}

// poly contains complex coefficients of the polynomial:
//   x^n + a0 x^(n-1) + a1 x^(n-2) + a2 x^(n-1) + ...
// recursively mulitplying by (x + pj) gives:
//   x^n+1 + (a0 + pk) x^n + (a1 + a0 pj) x^(n-1) + (a2 + a1 pj) x^(n-2) + ...
// thus:
//   a0' = a0 + pk
//   a_i' = a_{i-1} + pk a_i
VecComplex ExpandBinomial(VecComplex coefs) {
  VecComplex poly(coefs.size());
  for (size_t i = 0; i < coefs.size(); i++) {
    for (size_t j = i; j > 0; j--) {
      poly[j] += coefs[i] * poly[j - 1];
    }
    poly[0] += coefs[i];
  }

  return poly;
}

std::vector<double> ButterLowPassBCoef(int order, double cutoff_ratio) {
  // TODO(Gabe, Nic): Mimics SciPy, but not sure this is the desired behavior.
  if (order < 1) {
    return {1.0};
  }

  std::vector<double> bcoef((order + 1));

  bcoef[0] = 1.0;
  bcoef[order] = 1.0;

  bcoef[1] = static_cast<double>(order);
  bcoef[order - 1] = static_cast<double>(order);

  for (int i = 2; i <= order / 2; i++) {
    bcoef[i] = static_cast<double>((order - i + 1) * bcoef[i - 1] / i);
    bcoef[order - i] = bcoef[i];
  }

  double scale_factor = ButterScaleFactor(order, cutoff_ratio);
  for (auto& elem : bcoef) {
    elem *= scale_factor;
  }

  return bcoef;
}

// calculate the butterworth a coefficients
std::vector<double> ButterLowPassACoef(int order, double cutoff_ratio) {
  // TODO(Gabe, Nic): Mimics SciPy, but not sure this is the desired behavior.
  if (order < 1) {
    return {1.0};
  }
  // create complex coefficients of binomial
  VecComplex bincoefs(0);  // coefficients of binomial terms
  double phi = M_PI * cutoff_ratio;
  for (int i = 0; i < order; i++) {
    double psi = M_PI * static_cast<double>(2 * i + 1) / static_cast<double>(2 * order);
    double denom = 1.0 + sin(phi) * sin(psi);
    double re = -cos(phi) / denom;
    double im = -sin(phi) * cos(psi) / denom;
    bincoefs.push_back(std::complex<double>(re, im));
  }

  // create polynomial coefficients from binomial terms
  VecComplex poly = ExpandBinomial(bincoefs);

  // add leading term and pull out real components of poly
  // (imaginary components should be zero)
  std::vector<double> acoefs(0);
  acoefs.push_back(1.0);
  for (auto& elem : poly) {
    acoefs.push_back(elem.real());
  }

  return acoefs;
}

}  // namespace filter
}  // namespace primitives
