/* Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written permission
 * is obtained from COMPANY. Access to the source code contained herein is hereby
 * forbidden to anyone except current COMPANY employees, managers or contractors who
 * have executed Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 * MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE
 * OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY
 * PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE
 * RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
 * MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#include "primitives/object.h"

#include <ros/ros.h>

#include <string>
#include <iostream>

namespace primitives {

void Object::set_message_level(const MessageLevel message_level) {
  message_level_ = message_level;
}

void Object::SetBasicObjectInfo(const std::string& parent_name,
                                const std::string& name,
                                const MessageLevel message_level) {
  parent_name_ = parent_name;
  name_ = name;
  message_level_ = message_level;
}

void Object::SetBasicObjectInfo(const std::string& parent_name,
                                const std::string& name,
                                const std::string& message_level_string) {
  MessageLevel message_level = kDebug;
  if (message_level_string == "warn") {
    message_level = kWarn;
  }
  if (message_level_string == "error") {
    message_level = kError;
  }
  if (message_level_string == "info") {
    message_level = kInfo;
  }
  if (message_level_string == "fatal") {
    message_level = kFatal;
  }
  if (message_level_string == "debug") {
    message_level = kDebug;
  }

  parent_name_ = parent_name;
  name_ = name;
  message_level_ = message_level;
}

void Object::Debug(const std::string& message) {
  if (message_level_ >= kDebug) {
    std::string named_message = "[" + name_ + "]: " + message;
    fprintf(stderr, "\n(^^)\n%s\n(^^)\n", named_message.c_str());
  }
}

void Object::Info(const std::string& message) {
  if (message_level_ >= kInfo) {
    std::string named_message = "[" + name_ + "]: " + message;
    ROS_INFO("%s", named_message.c_str());
  }
}

void Object::Warn(const std::string& message) {
  if (message_level_ >= kWarn) {
    std::string named_message = "[" + name_ + "]: " + message;
    ROS_WARN("%s", named_message.c_str());
  }
}

void Object::Error(const std::string& message) {
  if (message_level_ >= kError) {
    std::string named_message = "[" + name_ + "]: " + message;
    ROS_ERROR("%s", named_message.c_str());
  }
}

void Object::Fatal(const std::string& message) {
  if (message_level_ >= kFatal) {
    std::string named_message = "[" + name_ + "]: " + message;
    ROS_FATAL("%s", named_message.c_str());
  }
}

}  // namespace primitives
