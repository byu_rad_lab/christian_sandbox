// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <eigen3/Eigen/Core>
#include <iostream>
#include <string>

#include "primitives/butter.h"
#include "primitives/eigen_bench_timer.h"

using primitives::BenchTimer;
using primitives::filter::VecDouble;
using primitives::filter::ButterLowPassACoef;
using primitives::filter::ButterLowPassBCoef;

// helper function, prints *best* time in microseconds
void PrintTimer(BenchTimer timer, int reps, std::string name) {
  printf(
      "%15s: %6.2f us\n", name.c_str(), timer.best() / static_cast<double>(reps) * 1e6);
}

// The function to time
void GenerateCoefs(int order, double cutoff_ratio) {
  VecDouble b = ButterLowPassBCoef(order, cutoff_ratio);
  VecDouble a = ButterLowPassACoef(order, cutoff_ratio);
}

int main() {
  // Calls the function n reps, and repeats this k times, where
  //   n = reps, k = tries.
  //   reps should probably be tuned to the function being called.
  // This example prints time per call of the *best* try, which is a good
  //   measure of how many cpu cycles it took.

  int reps = static_cast<int>(1e3);
  int tries = static_cast<int>(1e3);

  int order = 2;
  double cutoff_ratio = 0.25;

  // Here's a very simple example
  BenchTimer timer_01;
  BENCH(timer_01, tries, reps, GenerateCoefs(order, cutoff_ratio));
  PrintTimer(timer_01, reps, "GenerateCoefs took:");

  // Code can also be put inline
  VecDouble a, b;
  BenchTimer timer_02;
  BENCH(timer_02,
        tries,
        reps,
        {
          b = ButterLowPassBCoef(order, cutoff_ratio);
          a = ButterLowPassACoef(order, cutoff_ratio);
        });
  PrintTimer(timer_02, reps, "Inline version took:");

  return 0;
}
