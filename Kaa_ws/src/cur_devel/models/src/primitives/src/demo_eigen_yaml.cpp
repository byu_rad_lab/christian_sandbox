// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <eigen3/Eigen/Dense>
#include <exception>
#include <iostream>
#include <string>

#include "primitives/eigen_yaml.h"

typedef Eigen::Matrix<double, 1, 3> Mat13;
typedef Eigen::Matrix<double, 3, 1> Mat31;
typedef Eigen::Matrix<double, -1, -1> MatXX;
typedef Eigen::Matrix<double, -1, 1> MatX1;
typedef Eigen::Matrix<double, 1, -1> Mat1X;

typedef Eigen::Matrix<double, 2, 3> Mat23;

typedef Eigen::Matrix<double, 2, -1, 0, 2, 5> Mat2X25;

int main(void) {
  std::string str =
      "mat13: [5.0, 4.0, 3.0]\n"
      "mat31:\n"
      "  - [3.0]\n"
      "  - [4.0]\n"
      "  - [5.0]\n"
      "\n"
      "mat23:\n"
      "  - [0.0, 1.0, 2.0]\n"
      "  - [3.0, 4.0, 5.0]\n"
      "\n"
      "mat26:\n"
      "  - [0.0, 0.0, 0.0, 0.0, 1.0, 2.0]\n"
      "  - [3.0, 3.0, 3.0, 3.0, 4.0, 5.0]";

  YAML::Node n = YAML::Load(str);

  Mat13 mat13;
  Mat31 mat31;
  MatXX matXX;
  MatX1 matX1;
  Mat1X mat1X;
  Mat23 mat23;

  Mat2X25 mat2x25;

  std::cout << "Parsing mat13 as Mat13:" << std::endl;
  mat13 = n["mat13"].as<Mat13>();
  std::cout << mat13 << std::endl << std::endl;

  std::cout << "Parsing mat13 as Mat31:" << std::endl;
  mat13 = n["mat13"].as<Mat31>();
  std::cout << mat13 << std::endl << std::endl;

  std::cout << "Parsing mat31 as Mat31:" << std::endl;
  mat31 = n["mat31"].as<Mat31>();
  std::cout << mat31 << std::endl << std::endl;

  std::cout << "Parsing mat31 as MatXX:" << std::endl;
  matXX = n["mat31"].as<MatXX>();
  std::cout << matXX << std::endl << std::endl;

  std::cout << "Parsing mat31 as MatX1:" << std::endl;
  matX1 = n["mat31"].as<MatX1>();
  std::cout << matX1 << std::endl << std::endl;

  // Should break
  // std::cout << "Parsing mat31 as Mat1X:" << std::endl;
  // mat1X = n["mat31"].as<Mat1X>();
  // std::cout << mat1X << std::endl << std::endl;

  std::cout << "Parsing mat23 as Mat23:" << std::endl;
  mat23 = n["mat23"].as<Mat23>();
  std::cout << mat23 << std::endl << std::endl;

  std::cout << "Parsing mat23 as Mat2X25:" << std::endl;
  mat2x25 = n["mat23"].as<Mat2X25>();
  std::cout << mat2x25 << std::endl << std::endl;

  // Max cols exceeded.
  // std::cout << "Parsing mat26 as Mat2X25:" << std::endl;
  // mat2x25 = n["mat26"].as<Mat2X25>();
  // std::cout << mat2x25 << std::endl << std::endl;

  // std::cout << "Parsing DNE as Mat2X25:" << std::endl;
  // if (!n["DNE"]) {
  //   std::cout << "node key DNE does not exist." << std::endl;
  // }
  // mat2x25 = n["DNE"].as<Mat2X25>();
  // std::cout << mat2x25 << std::endl << std::endl;

  return 0;
}
