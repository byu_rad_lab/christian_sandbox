// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of COMPANY.
// The intellectual and technical concepts contained herein are proprietary to COMPANY
// and may be covered by U.S. and Foreign Patents, patents in process, and are
// protected  by trade secret or copyright law. Dissemination of this information or
// reproduction of this material is strictly forbidden unless prior written permission
// is obtained from COMPANY. Access to the source code contained herein is hereby
// forbidden to anyone except current COMPANY employees, managers or contractors who
// have executed Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended publication or
// disclosure of this source code, which includes information that is confidential
// and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
// MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE
// OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY
// PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE
// RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
// OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
// MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
#include "primitives/task.h"

#include <stdarg.h>

#include <string>

namespace primitives {
namespace task {

Task::Task() : initialized_(false), iterationCount_(0) {
  SetBasicObjectInfo("", "Task", MessageLevel::kDebug);
}

Task::Task(const std::string &name) : initialized_(false), iterationCount_(0) {
  SetBasicObjectInfo("", name, MessageLevel::kDebug);
}

Task::Task(const std::string &name, const std::string &parent_name)
    : initialized_(false), iterationCount_(0) {
  SetBasicObjectInfo(parent_name, name, MessageLevel::kDebug);
}

Task::~Task() {}

void Task::Init(void) {
  if (initialized_) {
    throw exception::TaskInitException(
        "Cannot initialize task that has already been initialized (call cleanup "
        "method first)");
  }
  doInit();
  iterationCount_ = 0;
  initialized_ = true;
}

void Task::Iterate(void) {
  if (!initialized_) {
    throw exception::TaskRunException(
        "Cannot iterate task that has not been initialized (call initialize method "
        "first)");
  }
  doIterate();
  iterationCount_++;
}

void Task::Cleanup(void) {
  if (!initialized_) {
    throw exception::TaskRunException(
        "Cannot cleanup task that has not been initialized");
  }
  doCleanup();
  initialized_ = false;
}

bool Task::Initialized(void) { return initialized_; }

IOTask::IOTask() : Task("IOTask") {}

IOTask::IOTask(const std::string &name) : Task(name) {}

IOTask::IOTask(const std::string &name, const std::string &parentName)
    : Task(name, parentName) {}

IOTask::~IOTask() {}

void IOTask::Iterate(void) {
  if (!initialized_) {
    throw exception::TaskRunException(
        "Cannot iterate task that has not been initialized (call initialize method "
        "first)");
  }
  doGetInputs();
  doIterate();
  doSetOutputs();
  iterationCount_++;
}

}  // namespace task
}  // namespace primitives
