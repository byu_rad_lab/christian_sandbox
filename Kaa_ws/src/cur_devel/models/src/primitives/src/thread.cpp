/* Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 *protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 *MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED,
 *AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR
 *POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
 *TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */

#include <errno.h>
#include <sched.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/utsname.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <string>

#include "primitives/exception.h"
#include "primitives/object.h"
#include "primitives/thread.h"

namespace primitives {
namespace thread {

Thread::Thread()
    : Object(),
      running_(false),
      stop_(false),
      initialized_(false),
      threadHandle_(0),
      threadPrio_(-1),
      appType_(THREAD_APP_TYPE_NRT) {}

Thread::Thread(const std::string &name)
    : running_(false),
      stop_(false),
      initialized_(false),
      threadHandle_(0),
      threadPrio_(-1),
      appType_(THREAD_APP_TYPE_NRT) {

  SetBasicObjectInfo("", name, MessageLevel::kDebug);
}

Thread::Thread(app_type_t type, thread_prio_t prio)
    : Object(),
      running_(false),
      stop_(false),
      initialized_(false),
      threadHandle_(0),
      threadPrio_(prio),
      appType_(type) {}

Thread::Thread(const std::string &name, app_type_t type, thread_prio_t prio)
    : running_(false),
      stop_(false),
      initialized_(false),
      threadHandle_(0),
      threadPrio_(prio),
      appType_(type) {

  SetBasicObjectInfo("", name, MessageLevel::kDebug);

  if (threadPrio_ == THREAD_APP_TYPE_NRT) {
    Warn("ignoring requested thread priority since running with THREAD_APP_TYPE_NRT");
  }
}

///
Thread::Thread(const std::string &name, const std::string &parentName)
    : running_(false),
      stop_(false),
      initialized_(false),
      threadHandle_(0),
      threadPrio_(-1),
      appType_(THREAD_APP_TYPE_NRT) {
  SetBasicObjectInfo(parentName, name, MessageLevel::kDebug);
}

Thread::Thread(const std::string &name,
               const std::string &parentName,
               app_type_t type,
               thread_prio_t prio)
    : running_(false),
      stop_(false),
      initialized_(false),
      threadHandle_(0),
      threadPrio_(prio),
      appType_(type) {

  SetBasicObjectInfo(parentName, name, MessageLevel::kDebug);

  if (threadPrio_ == THREAD_APP_TYPE_NRT) {
    Warn("ignoring requested thread priority since running with THREAD_APP_TYPE_NRT");
  }
}
Thread::~Thread() {}

void Thread::start(void) {
  if (running_) {
    exception::ThreadException(
        "Cannot call start method on thread that is already running");
  }

  if (stop_) {
    exception::ThreadException(
        "Cannot call start method on thread after requesting a stop but before it has "
        "actually stopped");
  }

  initialized_ = false;

  // Start the thread
  int rc = pthread_create(&threadHandle_, NULL, mainLoopCaller, this);
  if (rc) {
    std::stringstream ss;
    ss << "Failed to create thread with Linux system error " << rc;
    exception::ThreadException(ss.str());
  }

  while (!initialized_) {
  }
}

bool Thread::isAlive(void) { return running_; }

void Thread::join(void) {
  int rc = pthread_join(threadHandle_, NULL);
  if (rc) {
    std::stringstream ss;
    ss << "Failed to join thread with Linux system error " << rc;
    exception::ThreadException(ss.str());
  }
}

void Thread::cancel(void) {
  int rc = pthread_cancel(threadHandle_);
  if (rc) {
    std::stringstream ss;
    ss << "Failed to cancel thread with Linux system error " << rc;
    exception::ThreadException(ss.str());
  }
  running_ = false;
  stop_ = false;
  initialized_ = false;
}

void Thread::configureThread(void) {
  int policyOut = 0;
  struct sched_param paramIn;
  struct sched_param paramOut;
  int ret;
  struct utsname u;
  bool crit1 = false;
  bool crit2 = false;
  FILE *fd;
  std::stringstream ss;
  bool err = false;

  switch (appType_) {
    case THREAD_APP_TYPE_NRT:  // Use the normal SCHED_OTHER scheduler policy. Good for
                               // all non real time applications.
      Info(name() + " starting thread with default priority and scheduler policy");
      break;

    case THREAD_APP_TYPE_RT:    // Use the SCHED_RR scheduler policy. Good for most
                                // real-time applications.
    case THREAD_APP_TYPE_FIFO:  // Use the SCHED_FIFO scheduler policy. Can be used in
                                // special cases for real time applications.
      paramIn.sched_priority = threadPrio_;

      /*
       * Verify that we are running a Linux kernel with the RT_PREEMPT patch
       */
      uname(&u);
      crit1 = (strcasestr(reinterpret_cast<char *>(u.version), "PREEMPT RT") != NULL);

      if ((fd = fopen("/sys/kernel/realtime", "r")) != NULL) {
        int flag;
        crit2 = ((fscanf(fd, "%d", &flag) == 1) && (flag == 1));
        fclose(fd);
      }
      if (!(crit1 && crit2)) {
        Error("did not detect real time kernel: " + std::string(u.version));
      } else {
        Info("successfully detected real time kernel: " + std::string(u.version));
      }

      /*
       * Attempt to set thread priority and policy
       */
      ss.str("");
      ss << "attempting to set thread priority to " << threadPrio_ << "and scheduler "
         << THREAD_APP_TYPE_NAMES[appType_];
      Info(ss.str());

      ret = pthread_setschedparam(pthread_self(), appType_, &paramIn);
      if (ret != 0) {
        ss.str("");
        ss << "unsuccessful in setting thread priority and scheduler policy. ERROR = "
           << ret;
        Error(ss.str());
      } else {
        Info("successfully set thread priority and scheduler policy");
      }

      /*
       * Now verify the change in thread priority and policy
       */
      err = false;
      ret = pthread_getschedparam(pthread_self(), &policyOut, &paramOut);
      if (ret) {
        Error("couldn't retrieve parameters");
        err = true;
      }
      if (policyOut != appType_) {
        Error("verification of thread configuration failed");
        err = true;
      }
      if (paramOut.sched_priority != threadPrio_) {
        Error("verification of thread configuration failed");
        err = true;
      }
      if (err == false) {
        Info("successfully verified thread configuration");
      }

      if (mlockall(MCL_CURRENT | MCL_FUTURE) < 0) {
        Error("could not lock process in memory");
      } else {
        Info("successfully locked process in memory");
      }
      break;

    default:
      throw exception::ThreadException("Invalid application type");
      break;
  }
  Info(name() + " done with thread configuration");
}

void *Thread::mainLoopCaller(void *context) {
  return (reinterpret_cast<Thread *>(context))->mainLoop();
}

void *Thread::mainLoop(void) {
  running_ = true;
  configureThread();

  doInit();
  initialized_ = true;
  doIterate();
  doCleanup();
  initialized_ = false;

  running_ = false;
  stop_ = false;

  return 0;
}

}  // namespace thread
}  // namespace primitives
