// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#include <eigen3/Eigen/Core>
#include <stdio.h>
#include <iostream>
#include <vector>
#include "gtest/gtest.h"
#include "primitives/butter.h"

// using primitives::VecDouble;
// using primitives::ButterLowPassACoef;
// using primitives::ButterLowPassBCoef;
using primitives::filter::FilterButterLowPass;

TEST(FilterButterLowPassEigen, Exercise) {
  int order = 1;
  double freq_sample = 1000.0;
  double freq_cutoff = 250.0;
  typedef Eigen::VectorXd Vec;
  Vec x0 = Vec::Zero(3);
  Vec x_new = Vec::Ones(3);
  x_new[1] = 2.0;
  x_new[2] = 3.0;

  auto filter = FilterButterLowPass<Vec>();
  filter.Init(order, freq_sample, freq_cutoff, x0);
  // std::cout << "filter.Read(): " << filter.Read().transpose() << std::endl;
  filter.AddMeasurement(x0);
  // std::cout << "filter.Read(): " << filter.Read().transpose() << std::endl;
  // std::cout << "Adding measurements:" << std::endl;
  for (int i = 0; i < 10; i++) {
    filter.AddMeasurement(x_new);
    // std::cout << "filter.Read(): " << filter.Read().transpose() << std::endl;
  }
}

TEST(FilterButterLowPassDouble, Exercise) {
  int order = 1;
  double freq_sample = 1000.0;
  double freq_cutoff = 250.0;
  double x0 = 0.0;
  double x_new = 2.0;

  auto filter = FilterButterLowPass<double>();
  filter.Init(order, freq_sample, freq_cutoff, x0);
  // std::cout << "filter.Read(): " << filter.Read() << std::endl;
  filter.AddMeasurement(x0);
  // std::cout << "filter.Read(): " << filter.Read() << std::endl;
  // std::cout << "Adding measurements:" << std::endl;
  for (int i = 0; i < 10; i++) {
    filter.AddMeasurement(x_new);
    // std::cout << "filter.Read(): " << filter.Read() << std::endl;
  }
}

TEST(FilterDoubleButterLowPass, Uninitialized) {
  auto filter = FilterButterLowPass<double>();
  try {
    filter.AddMeasurement(0.0);
    FAIL() << "Expected ExceptionUninitialized.";
  } catch (primitives::filter::ExceptionUninitialized const& err) {
    EXPECT_EQ(err.what(), std::string("Cannot run uninitialized filter"));
  } catch (...) {
    FAIL() << "Expected ExceptionUninitialized.";
  }
}

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
