#!/usr/bin/env python

# Run this to generate butter_test_data.in.
# Then run clang-format on that file.
# Depends on scipy

import numpy as np
from scipy.signal import butter


def header():
    return '''// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
'''


def create_coefs(order, cutoff_ratio):
    b, a = butter(order, cutoff_ratio)

    a = np.asarray(a)
    b = np.asarray(b)
    return b, a


def write_vector(name, coefs):
    f.write("  VecDouble %s = {\n" % name)
    for c in coefs:
        f.write("    %.20e,  //\n" % c)
    f.write("  };\n")


def write_test(order, cutoff_ratio, case_num, a, b):
    f.write("\n")
    f.write("TEST(ButterLowPass, Order%02dCutoff%02d) {\n" %
            (order, case_num))
    f.write("  int order = %d;\n" % order)
    f.write("  double cutoff_ratio = %.20f;\n" % cutoff_ratio)
    f.write("  VecDouble b = ButterLowPassBCoef(order, cutoff_ratio);\n")
    f.write("  VecDouble a = ButterLowPassACoef(order, cutoff_ratio);\n")

    write_vector("a2", a)
    write_vector("b2", b)

    f.write("  ASSERT_EQ(a.size(), a2.size());\n")
    f.write("  ASSERT_EQ(b.size(), b2.size());\n")
    f.write("  for (size_t i = 0; i < a.size(); i++) {\n")
    f.write("    EXPECT_NEAR(a[i], a2[i], 1e-12);\n")
    f.write("  }\n")
    f.write("  for (size_t i = 0; i < b.size(); i++) {\n")
    f.write("    EXPECT_NEAR(b[i], b2[i], 1e-12);\n")
    f.write("  }\n")
    f.write("}\n")


def write_file():
    f.write(header())
    for order in range(10):
        for case_num, cutoff_ratio in enumerate([0.22, 0.35]):
            b, a = create_coefs(order, cutoff_ratio)
            write_test(order, cutoff_ratio, case_num, a, b)

if __name__ == "__main__":
    with open('butter_test_data.in', 'w') as f:
        write_file()
