// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_PRIMITIVES_INCLUDE_PRIMITIVES_EIGEN_YAML_H_
#define SRC_PRIMITIVES_INCLUDE_PRIMITIVES_EIGEN_YAML_H_

#include <yaml-cpp/yaml.h>

#include <eigen3/Eigen/Dense>
#include <string>
#include <vector>

#include "primitives/yaml.h"

namespace YAML {

using primitives::ExceptionYaml;

template <typename S, int R, int C, int O, int MR, int MC>
struct convert<Eigen::Matrix<S, R, C, O, MR, MC>> {
  static bool decode(
      const YAML::Node& node,
      Eigen::Matrix<S, R, C, O, MR, MC>& vec) {  // NOLINT(runtime/references)
    if (node.Type() == NodeType::Undefined) {
      throw ExceptionYaml(
          "Error converting Eigen::Matrix.  Node is undefined."
          " Likely the key didn't exist, which should be checked before getting here.");
    }

    // Scalar or maps not allowed
    if (!node.IsSequence()) {
      throw ExceptionYaml("Error converting Eigen::Matrix.  Node is not a sequence.");
    }
    if (node.size() == 0) {
      throw ExceptionYaml(
          "Error converting Eigen::Matrix.  Node is a sequence of size 0.");
    }
    if (node[0].IsMap()) {
      throw ExceptionYaml("Error converting Eigen::Matrix.  Node[0] is a map.");
    }

    std::vector<std::vector<S>> data;

    try {
      if (node[0].IsScalar()) {
        // Sequence of scalars - row data
        data.push_back(node.as<std::vector<S>>());
      } else {
        // Matrix or column vector
        for (size_t i = 0; i < node.size(); i++) {
          data.push_back(node[i].as<std::vector<S>>());
        }
      }
    } catch (const TypedBadConversion<S>& e) {
      throw ExceptionYaml(
          static_cast<std::string>("Error converting Eigen::Matrix.  Couldn't convert "
                                   "element to template parameter S (Eigen data "
                                   "type):\n") +
          e.what());
    } catch (const BadConversion& e) {
      throw ExceptionYaml(
          static_cast<std::string>("Error converting Eigen::Matrix.  A bad conversion "
                                   "other than to template paramter S:\n") +
          e.what());
    }

    // Handle row data:  If approprate, flip to column vector.
    if (data.size() == 1 && C == 1) {
      // Flip if we got a row and want a column.
      std::vector<S> temp = data[0];
      data.clear();
      for (size_t i = 0; i < temp.size(); i++) {
        data.push_back({temp[i]});
      }
    }

    size_t rows = data.size();
    size_t cols = data[0].size();

    // Check that sizes match
    if (!(R == -1 || R == static_cast<int>(rows))) {
      throw ExceptionYaml("Error converting Eigen::Matrix.  Row mismatch:  R = " +
                          std::to_string(R) + " while node has " + std::to_string(rows) +
                          " rows.");
    }
    if (!(C == -1 || C == static_cast<int>(cols))) {
      throw ExceptionYaml("Error converting Eigen::Matrix.  Column mismatch:  C = " +
                          std::to_string(C) + " while node has " + std::to_string(cols) +
                          " cols.");
    }

    // Check if maximum rows or columns has been exceeded.
    if (MR != -1 && MR < static_cast<int>(rows)) {
      throw ExceptionYaml(
          "Error converting Eigen::Matrix.  MaxRowsAtCompileTime Exceeded:  MR = " +
          std::to_string(R) + " while node has " + std::to_string(rows) + " rows.");
    }
    if (MC != -1 && MC < static_cast<int>(cols)) {
      throw ExceptionYaml(
          "Error converting Eigen::Matrix.  MaxColsAtCompileTime Exceeded:  MC = " +
          std::to_string(R) + " while node has " + std::to_string(cols) + " cols.");
    }

    // Check that the length of the rows are equal
    for (size_t r = 0; r < rows; r++) {
      if (data[r].size() != cols) {
        throw ExceptionYaml(std::string("") + "Error converting Eigen::Matrix. " +
                            "Rows are of different length:\n" + "Row " +
                            std::to_string(r) + " has " + std::to_string(data[r].size()) +
                            " elements, while row  0 has " + std::to_string(rows) +
                            " elements.");
      }
    }

    // Resize and populate
    vec.resize(rows, cols);
    for (size_t r = 0; r < rows; r++) {
      for (size_t c = 0; c < cols; c++) {
        vec(r, c) = data[r][c];
      }
    }
    return true;
  }
};

}  // namespace YAML

#endif  // SRC_PRIMITIVES_INCLUDE_PRIMITIVES_EIGEN_YAML_H_
