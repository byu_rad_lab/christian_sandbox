// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_PRIMITIVES_INCLUDE_PRIMITIVES_FILTER_SMOOTH_H_
#define SRC_PRIMITIVES_INCLUDE_PRIMITIVES_FILTER_SMOOTH_H_

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>

#include <deque>
#include <limits>
#include <string>
#include <vector>

#include "yaml-cpp/yaml.h"

#include "primitives/butter.h"
#include "primitives/exception.h"
#include "primitives/yaml.h"

namespace primitives {
namespace filter {

template <typename T>
class FilterSmooth {
 public:
  void Init(double freq_sample, double freq_cutoff, T x) {
    freq_sample_ = freq_sample;
    freq_cutoff_ = freq_cutoff;
    Initialize(x);
  }

  void SetFreqSample(double freq_sample) {
    if (!initialized_) {
      throw ExceptionUninitialized("Cannot set frequency on uninitialized filter");
    }
    freq_sample_ = freq_sample;
    Initialize(x_);
  }

  void SetFreqCutoff(double freq_cutoff) {
    if (!initialized_) {
      throw ExceptionUninitialized("Cannot set frequency on uninitialized filter");
    }
    freq_cutoff_ = freq_cutoff;
    Initialize(x_);
  }

  void SetFreqSampleUninit(double freq_sample) { freq_sample_ = freq_sample; }
  void SetFreqCutoffUninit(double freq_cutoff) { freq_cutoff_ = freq_cutoff; }

  void ResetValue(const T& x) {
    if (!initialized_) {
      throw ExceptionUninitialized("Cannot run uninitialized filter");
    }
    Initialize(x);
  }

  const T& AddMeasurement(const T& x) {
    if (!initialized_) {
      throw ExceptionUninitialized("Cannot run uninitialized filter");
    }
    x_ = x;
    y_ = one_minus_alpha_ * y_ + alpha_ * x;
    return y_;
  }

  // Make sure order, freq_sample, and freq_cutoff are set
  void Init(const T& x) { Initialize(x); }

  const T& Read(void) const { return y_; }
  const T& ReadUnfilt(void) const { return x_; }

  double GetFreqSample(void) const { return freq_sample_; }
  double GetFreqCutoff(void) const { return freq_cutoff_; }
  double GetAlpha(void) const { return alpha_; }

 private:
  void Initialize(const T& x) {
    assert(freq_cutoff_ >= 0.0);
    assert(freq_sample_ >= 0.0);
    CalcAlpha();
    x_ = x;
    y_ = x;
    initialized_ = true;
  }

  void CalcAlpha(void) {
    const double two_pi = 6.28318530718;
    double dt = 1.0 / freq_sample_;
    double dt_fc = dt * freq_cutoff_;
    alpha_ = two_pi * dt_fc / (two_pi * dt_fc + 1.0);
    one_minus_alpha_ = 1.0 - alpha_;
    // TODO(Tom): checks that alpha >= 0, alpha <= 1.0
  }

 private:
  double alpha_ = -1;
  double one_minus_alpha_ = -1;
  double freq_sample_ = -1;
  double freq_cutoff_ = -1;

  T x_;
  T y_;

  bool initialized_ = false;
};

template<typename T>
std::ostream& operator<<(std::ostream& os, const FilterSmooth<T>& rhs) {
  os << "freq_sample: " << rhs.GetFreqSample() << std::endl;
  os << "freq_cutoff: " << rhs.GetFreqCutoff() << std::endl;
  os << "alpha: " << rhs.GetAlpha() << std::endl;
  return os;
}

typedef Eigen::Quaterniond Quat;
template <>
const Quat& FilterSmooth<Quat>::AddMeasurement(const Quat& q);

}  // namespace filter
}  // namespace primitives

namespace YAML {

using primitives::filter::FilterSmooth;
using primitives::YamlCheckAndConvert;

template <typename T>
struct convert<FilterSmooth<T>> {
  static bool decode(const YAML::Node& node,
                     FilterSmooth<T>& filter) {  //  NOLINT(runtime/references)
    try {
      filter.SetFreqSampleUninit(YamlCheckAndConvert<double>(node, "freq_sample"));
      filter.SetFreqCutoffUninit(YamlCheckAndConvert<double>(node, "freq_cutoff"));
      // Filter isn't initialized here, must call Init(T intital_value)
    } catch (primitives::ExceptionYaml& e) {
      throw primitives::ExceptionYaml("primitives::FilterButterLowpass", e);
    }
    return true;
  }
};

}  // namespace YAML

#endif  // SRC_PRIMITIVES_INCLUDE_PRIMITIVES_FILTER_SMOOTH_H_
