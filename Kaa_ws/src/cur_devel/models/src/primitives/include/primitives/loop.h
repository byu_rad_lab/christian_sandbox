/* Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#ifndef _PNEUBOTICS_LOOP_H_
#define _PNEUBOTICS_LOOP_H_

#include <string>

#include "ros/ros.h"
#include "primitives/thread.h"

namespace primitives {
namespace loop {

class Loop : public thread::Thread {
 public:
  explicit Loop(const std::string &name);
  Loop(thread::app_type_t type, thread::thread_prio_t prio);
  Loop(const std::string &name, thread::app_type_t type, thread::thread_prio_t prio);

  Loop(const std::string &name, const ros::Duration &period);
  Loop(thread::app_type_t type, thread::thread_prio_t prio, const ros::Duration &period);
  Loop(const std::string &name, thread::app_type_t type, thread::thread_prio_t prio,
       const ros::Duration &period);

  Loop(const std::string &name, const std::string &parentName);
  Loop(const std::string &name, const std::string &parentName, thread::app_type_t type,
       thread::thread_prio_t prio);

  Loop(const std::string &name, const std::string &parentName, const ros::Duration &period);
  Loop(const std::string &name, const std::string &parentName, thread::app_type_t type,
       thread::thread_prio_t prio, const ros::Duration &period);

  void setLoopPeriod(const ros::Duration &period);
  ros::Duration getLoopPeriod(void) const;

  virtual ~Loop();

  virtual void stop(void);

 protected:
  virtual void *mainLoop(void);

  /* Variables for dealing with loop timing
   *
   */
  ros::Time time_;
  ros::Duration timeDiff_;
  ros::Duration period_;
  ros::Time timeNext_;
  ros::Time startTime_;

  /*
   * Data logging related variables
   */
  ros::Duration logPeriod_;
  ros::Time logTimeNext_;

  /*
   * Debugging related variables
   */
  std::uint32_t loopCnt_;
  std::uint32_t overrunCnt_;
};

}  // namespace loop
}  // namespace primitives
#endif  // _PNEUBOTICS_LOOP_H_
