/* Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 *protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 *MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED,
 *AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR
 *POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
 *TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#ifndef SRC_PRIMITIVES_INCLUDE_PRIMITIVES_TASK_H_
#define SRC_PRIMITIVES_INCLUDE_PRIMITIVES_TASK_H_

#include <stdint.h>

#include <string>

#include "primitives/object.h"
#include "primitives/exception.h"

namespace primitives {
namespace exception {
class TaskException : public primitives::exception::Exception {
 public:
  explicit TaskException(const std::string &msg)
      : primitives::exception::Exception(msg) {}

  virtual ~TaskException() throw() {}
};

class TaskInitException : public TaskException {
 public:
  explicit TaskInitException(const std::string &msg) : TaskException(msg) {}

  virtual ~TaskInitException() throw() {}
};

class TaskRunException : public TaskException {
 public:
  explicit TaskRunException(const std::string &msg) : TaskException(msg) {}

  virtual ~TaskRunException() throw() {}
};

}  // namespace exception

namespace task {

class Task : public Object {
 public:
  Task();
  explicit Task(const std::string &name);
  Task(const std::string &name, const std::string &parent_name);

  virtual ~Task();

  void Init();
  virtual void Iterate(void);
  void Cleanup(void);
  bool Initialized(void);

 protected:
  /*
   * Virtual methods to be overridden in concrete implementation
   */
  virtual void doInit(void) = 0;
  virtual void doIterate(void) = 0;
  virtual void doCleanup(void) = 0;

  bool initialized_;
  std::uint32_t iterationCount_;
};

class IOTask : public Task {
 public:
  IOTask();
  explicit IOTask(const std::string &name);
  IOTask(const std::string &name, const std::string &parentName);
  virtual ~IOTask();

  void Iterate(void);

 protected:
  void getInputs(void);
  void setOutputs(void);

  /*
   * Virtual methods to be overridden in concrete implementation
   */
  virtual void doGetInputs(void) = 0;
  virtual void doSetOutputs(void) = 0;
};

}  // namespace task
}  // namespace primitives

#endif  // SRC_PRIMITIVES_INCLUDE_PRIMITIVES_TASK_H_
