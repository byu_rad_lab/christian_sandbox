// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_PRIMITIVES_INCLUDE_PRIMITIVES_YAML_H_
#define SRC_PRIMITIVES_INCLUDE_PRIMITIVES_YAML_H_

#include <stdexcept>

#include "yaml-cpp/yaml.h"

#include "primitives/exception.h"
#include "primitives/stringprintf.h"

namespace primitives {

// TODO(Tom, Charlie): this should probably be renamed ExceptionParsing
//   and put in namespace primitives::yaml
class ExceptionYaml : public primitives::exception::Exception {
 public:
  explicit ExceptionYaml(const std::string& msg)
      : primitives::exception::Exception(msg) {}
  ExceptionYaml(const std::string& name, const std::runtime_error e)
      : primitives::exception::Exception("Problem parsing " + name + ":\n" + e.what()) {}
  ~ExceptionYaml() throw() {}
};

namespace yaml {

YAML::Node LoadFile(const std::string& filename);
YAML::Node Load(const std::string& str);

template <typename T>
T Convert(YAML::Node node, std::string key) {
  if (!node[key]) {
    throw ExceptionYaml(primitives::stringsprintf(
        "\nYAML parsing failure: key '%s' is missing.", key.c_str()));
  }
  try {
    return node[key].as<T>();
  } catch (YAML::Exception e) {
    throw ExceptionYaml(std::string("Problem parsing field ") + key + std::string("\n") +
                        e.what());
  }
}

}  // namespace yaml

template <typename T>
T YamlConvert(YAML::Node node, std::string name) {
  try {
    return node.as<T>();
  } catch (YAML::Exception e) {
    throw ExceptionYaml(std::string("Problem parsing field ") + name + std::string("\n") +
                        e.what());
  }
}

template <typename T>
T YamlCheckAndConvert(YAML::Node node, std::string key) {
  if (!node[key]) {
    throw ExceptionYaml(primitives::stringsprintf(
        "\nYAML parsing failure: key '%s' is missing.", key.c_str()));
  }
  return YamlConvert<T>(node[key], key);
}

// TODO(Tom): add overloaded method that checks specific size
inline void YamlCheckIsSequence(YAML::Node node, std::string type) {
  if (!node) {
    throw ExceptionYaml(
        primitives::stringsprintf("\nFailed to parse YAML node as '%s'. "
                                  "Node does not exist.",
                                  type.c_str()));
  }
  if (!node.IsSequence()) {
    throw ExceptionYaml(
        primitives::stringsprintf("\nFailed to parse YAML node as '%s'. "
                                  "Node is not a sequence.",
                                  type.c_str()));
  }
}

}  // namespace primitives

#endif  // SRC_PRIMITIVES_INCLUDE_PRIMITIVES_YAML_H_
