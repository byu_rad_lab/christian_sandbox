/* Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written permission
 * is obtained from COMPANY. Access to the source code contained herein is hereby
 * forbidden to anyone except current COMPANY employees, managers or contractors who
 * have executed Confidentiality and Non-disclosure agreements explicitly covering such
 * access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 * MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE
 * OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY
 * PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE
 * RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
 * MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#ifndef SRC_PRIMITIVES_INCLUDE_PRIMITIVES_OBJECT_H_
#define SRC_PRIMITIVES_INCLUDE_PRIMITIVES_OBJECT_H_

///
///  This file contains the class definition for the Object class.
///
#include <ros/ros.h>

#include <string>

namespace primitives {

///
/// Object stores the name of the object and the name of the object's
/// parent.  It also provides functions to write to the terminal
/// for debugging.
/// Usage: All objects should derive from this class.
class Object {
 public:
  /// Specification of the message level shows messages of that level or less
  /// ie. "message_level_ = kWarn" will show warnings, errors, info and fatal
  /// messages.
  enum MessageLevel {
    kFatal,
    kError,
    kWarn,
    kInfo,
    kDebug
  };

  /// Default Constructor
  Object() : name_(""), parent_name_("") {}

  /// Destructor
  virtual ~Object() {}

  /// set the name of the object
  void set_name(const std::string &name) { name_ = name; }
  void set_parent_name(const std::string &parent_name) { parent_name_ = parent_name; }
  void set_message_level(const MessageLevel message_level);

  void SetBasicObjectInfo(const std::string &parent_name, const std::string &name,
                          const std::string &message_level);

  void SetBasicObjectInfo(const std::string &parent_name, const std::string &name,
                          const MessageLevel message_level);

  // get the name of the object
  std::string name() const { return name_; }
  std::string parent_name() const { return parent_name_; }
  MessageLevel message_level() const { return message_level_; }

 protected:
  /// Logging Functions
  void Debug(const std::string &message);
  void Info(const std::string &message);
  void Warn(const std::string &message);
  void Error(const std::string &message);
  void Fatal(const std::string &message);

 private:
  std::string name_;
  std::string parent_name_;
  // Determines which messages will be shown
  MessageLevel message_level_ = kWarn;
};

}  // namespace primitives
#endif  // SRC_PRIMITIVES_INCLUDE_PRIMITIVES_OBJECT_H_
