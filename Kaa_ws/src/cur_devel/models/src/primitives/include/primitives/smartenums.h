/* Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */

#ifndef SOFTWARE_SRC_PCP_INCLUDE_PCP_SMARTENUMS_H_
#define SOFTWARE_SRC_PCP_INCLUDE_PCP_SMARTENUMS_H_

// Macros for the automatic generation of enum/string conversions
// adapted from:
// https://gist.github.com/saltares/8904478

/* Usage Example:
in header:
#include "smartenums.h"

#define SMARTENUMS_JOINT_TYPE_LIST(X)      \
  X(JointType, PrisXatic)  \
  X(JointType, Rotational) \
  X(JointType, ContinuuX)  \

SMARTENUM_CREATE_INCLUDE(JointType, SMARTENUMS_JOINT_TYPE_LIST)

in source:
SMARTENUM_CREATE_SOURCE(JointType, SMARTENUMS_JOINT_TYPE_LIST)
*/

/*
The above code will create four items:

- enum JointType;
- const std::string kJointTypeArray[];
- const std::string JointTypeToString(JointType type);
- JointType JointTypeFromString(const std::string str);

*/

#include <cstring>
#include <string>

// needed to put stuff in quotes
#define SMARTENUM_QUOTE(name) #name

// defines enum formatting as k<TypeName><EnumName>
#define SMARTENUM_VALUE(typeName, value) k##typeName##value,

// defines string formatting as k<TypeName><EnumName>
#define SMARTENUM_STRING(typeName, value) SMARTENUM_QUOTE(k##typeName##value),

// create enum
#define SMARTENUM_DEFINE_ENUM(typeName, values) \
  enum typeName {                               \
    values(SMARTENUM_VALUE) kNum##typeName##s,  \
  };

// create array of enum names
#define SMARTENUM_DEFINE_NAMES(typeName, values) \
  const std::string k##typeName##Array[] = {values(SMARTENUM_STRING)};

// create type to string conversion
#define SMARTENUM_DEFINE_GET_STRING_FROM_VALUE(typeName, name) \
  std::string typeName##ToString(typeName type) { return k##typeName##Array[type]; }

#define SMARTENUM_DEFINE_GET_STRING_FROM_VALUE_DECLARATION(typeName, name) \
  std::string typeName##ToString(typeName type);

// create string to type conversion
#define SMARTENUM_DEFINE_GET_VALUE_FROM_STRING_DECLARATION(typeName, name)     \
  typeName typeName##FromString(const std::string str);

#define SMARTENUM_DEFINE_GET_VALUE_FROM_STRING(typeName, name)     \
  typeName typeName##FromString(const std::string str) {            \
    for (int i = 0; i < kNum##typeName##s; ++i)                    \
      if (k##typeName##Array[i] == str) return (typeName)i; \
    return kNum##typeName##s;                                      \
  }

// shortcuts to run the macros above
// this does in header
#define SMARTENUM_CREATE_INCLUDE(typeName, value)                 \
  SMARTENUM_DEFINE_ENUM(typeName, value)                  \
  SMARTENUM_DEFINE_NAMES(typeName, value)                 \
  SMARTENUM_DEFINE_GET_VALUE_FROM_STRING_DECLARATION(typeName, value) \
  SMARTENUM_DEFINE_GET_STRING_FROM_VALUE_DECLARATION(typeName, value)

// this goes in source
#define SMARTENUM_CREATE_SOURCE(typeName, value)                 \
  SMARTENUM_DEFINE_GET_VALUE_FROM_STRING(typeName, value) \
  SMARTENUM_DEFINE_GET_STRING_FROM_VALUE(typeName, value)

#endif  // SOFTWARE_SRC_PCP_INCLUDE_PCP_SMARTENUMS_H_
