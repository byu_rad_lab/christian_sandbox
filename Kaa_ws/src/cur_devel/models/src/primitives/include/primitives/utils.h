/* Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#ifndef SRC_PRIMITIVES_INCLUDE_PRIMITIVES_UTILS_H_
#define SRC_PRIMITIVES_INCLUDE_PRIMITIVES_UTILS_H_

#include <algorithm>
#include <eigen3/Eigen/Core>

namespace primitives {
namespace utils {

static constexpr double kPi = 3.14159265358979323846;
static constexpr double kPascalsToPSI = 0.00014503778;

double Interp(double xmin, double xmax, double ymin, double ymax, double xeval);
double Interp(
    double xmin, double xmax, double ymin, double ymax, double xeval, bool clamp);

template <class T>
inline T Min(const T &x, const T &y) {
  std::min(x, y);
}

template <class T>
inline T Max(const T &x, const T &y) {
  std::max(x, y);
}

template <typename T>
inline T Saturate(const T &val, const T &valmin, const T &valmax) {
  return std::min(std::max(val, valmin), valmax);
}

template <typename T>
inline T Saturate(const T &val, const T &valmax) {
  return std::min(std::max(val, -valmax), valmax);
}

template <typename T>
inline bool InRange(const T &val, const T &valmin, const T &valmax) {
  if ((val <= valmax) && (val >= valmin)) {
    return true;
  }
  return false;
}

template <typename T>
int Sign(T val) {
  return (T(0) < val) - (val < T(0));
}

double GenerateGaussianNoise(double mu, double sigma);

template <typename T>
void SetGaussian(T *mat, double mean, double covariance) {
  for (int i = 0; i < mat->rows(); i++) {
    for (int j = 0; j < mat->cols(); j++) {
      (*mat)(i, j) = primitives::utils::GenerateGaussianNoise(mean, covariance);
      // I think this will loop over any size matrix.  Seems to work for vectors.
      // (*(mat->data() + i)) =
      // primitives::utils::GenerateGaussianNoise(mean, covariance);
    }
  }
}

}  // namespace utils
}  // namespace primitives

#endif  // SRC_PRIMITIVES_INCLUDE_PRIMITIVES_UTILS_H_
