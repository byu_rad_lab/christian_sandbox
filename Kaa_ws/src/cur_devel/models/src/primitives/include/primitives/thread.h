/* Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are
 *protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION,
 *MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED,
 *AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR
 *POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
 *TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#ifndef SRC_PRIMITIVES_INCLUDE_PRIMITIVES_THREAD_H_
#define SRC_PRIMITIVES_INCLUDE_PRIMITIVES_THREAD_H_

#include <string>
#include <vector>

#include "primitives/exception.h"
#include "primitives/object.h"
#include "primitives/task.h"

namespace primitives {
namespace exception {

class ThreadException : public Exception {
 public:
  explicit ThreadException(const std::string &msg) : Exception(msg) {}

  virtual ~ThreadException() throw() {}
};

}  // namespace exception

namespace thread {

typedef int thread_prio_t;

typedef enum {
  THREAD_APP_TYPE_NRT,  // Use the normal SCHED_OTHER scheduler policy. Good for all
                        // non real time applications.
  THREAD_APP_TYPE_RT,   // Use the SCHED_RR scheduler policy. Good for most real-time
                        // applications.
  THREAD_APP_TYPE_FIFO  // Use the SCHED_FIFO scheduler policy. Can be used in special
                        // cases for real time applications.
} app_type_t;

static const std::string THREAD_APP_TYPE_NAMES[3] = {
    "Non-Real-Time", "Real-Time", "FIFO"};

class Thread : public Object {
 public:
  Thread();
  explicit Thread(const std::string &name);
  Thread(app_type_t type, thread_prio_t prio);
  Thread(const std::string &name, app_type_t type, thread_prio_t prio);

  Thread(const std::string &name, const std::string &parentName);
  Thread(const std::string &name,
         const std::string &parentName,
         app_type_t type,
         thread_prio_t prio);

  virtual ~Thread();
  virtual void start(void);
  virtual bool isAlive(void);
  virtual void join(void);
  virtual void cancel(void);

 protected:
  static void *mainLoopCaller(void *context);
  void configureThread(void);  // Gets called from within the new thread
  virtual void *mainLoop(void);

  /*
   * Virtual functions for actually doing work
   */
  virtual void doInit(void) = 0;
  virtual void doIterate(void) = 0;
  virtual void doCleanup(void) = 0;

  volatile bool running_;
  volatile bool stop_;
  volatile bool initialized_;

 private:
  pthread_t threadHandle_;
  thread_prio_t threadPrio_;
  app_type_t appType_;
};

}  // namespace thread
}  // namespace primitives

#endif  // SRC_PRIMITIVES_INCLUDE_PRIMITIVES_THREAD_H_
