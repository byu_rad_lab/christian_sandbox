// Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_PRIMITIVES_INCLUDE_PRIMITIVES_BUTTER_H_
#define SRC_PRIMITIVES_INCLUDE_PRIMITIVES_BUTTER_H_

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>

#include <deque>
#include <limits>
#include <string>
#include <vector>

#include "yaml-cpp/yaml.h"

#include "primitives/exception.h"
#include "primitives/yaml.h"

namespace primitives {
namespace filter {

class ExceptionUninitialized : public primitives::exception::Exception {
 public:
  explicit ExceptionUninitialized(const std::string &msg)
      : ::primitives::exception::Exception(msg) {}

  virtual ~ExceptionUninitialized() throw() {}
};

class ExceptionFilterOrder : public primitives::exception::Exception {
 public:
  explicit ExceptionFilterOrder(const std::string &msg)
      : ::primitives::exception::Exception(msg) {}

  virtual ~ExceptionFilterOrder() throw() {}
};

class ExceptionFilterButter : public primitives::exception::Exception {
 public:
  explicit ExceptionFilterButter(const std::string &msg)
      : ::primitives::exception::Exception(msg) {}

  virtual ~ExceptionFilterButter() throw() {}
};

typedef std::vector<double> VecDouble;

VecDouble ButterLowPassBCoef(int order, double cutoff_ratio);
VecDouble ButterLowPassACoef(int order, double cutoff_ratio);

// TODO(Tom): test this
template <typename T>
class FilterButterLowPass {
 public:
  void Init(int order, double freq_sample, double freq_cutoff, T value) {
    freq_sample_ = freq_sample;
    freq_cutoff_ = freq_cutoff;
    order_ = order;
    Initialize(value);
  }

  // Make sure order, freq_sample, and freq_cutoff are set
  void Init(T value) { Initialize(value); }

  // Methods to set without initializing. Make sure to call Init(T value) after.
  void SetFreqSampleUninit(double freq_sample) { freq_sample_ = freq_sample; }
  void SetFreqCutoffUninit(double freq_cutoff) { freq_cutoff_ = freq_cutoff; }
  void SetOrderUninit(int order) { order_ = order; }

  double GetFreqSample(void) const { return freq_sample_; }
  double GetFreqCutoff(void) const { return freq_cutoff_; }
  double GetOrder(void) const { return order_; }

  void SetFreqSample(double freq_sample) {
    if (!initialized_) {
      throw ExceptionUninitialized("Cannot set frequency on uninitialized filter");
    }
    freq_sample_ = freq_sample;
    Initialize(y_[0]);
  }

  void SetFreqCutoff(double freq_cutoff) {
    if (!initialized_) {
      throw ExceptionUninitialized("Cannot set frequency on uninitialized filter");
    }
    freq_cutoff_ = freq_cutoff;
    Initialize(y_[0]);
  }

  void SetOrder(double order) {
    if (!initialized_) {
      throw ExceptionUninitialized("Cannot set frequency on uninitialized filter");
    }
    order_ = order;
    Initialize(y_[0]);
  }

  void ResetValue(T val) {
    if (!initialized_) {
      throw ExceptionUninitialized("Cannot run uninitialized filter");
    }
    Initialize(val);
  }

  const T& AddMeasurement(T meas) {
    if (!initialized_) {
      throw ExceptionUninitialized("Cannot run uninitialized filter");
    }
    x_.push_front(meas);
    x_.pop_back();

    T y = b_[0] * x_[0] - a_[0 + 1] * y_[0];
    for (int i = 1; i < order_; i++) {
      y += b_[i] * x_[i] - a_[i + 1] * y_[i];
    }
    y += b_[order_] * x_[order_];
    y /= a_[0];

    y_.push_front(y);
    y_.pop_back();
    return y_[0];
  }

  const T &Read(void) const { return y_[0]; }

 private:
  void Initialize(T value) {
    if (order_ < 1) {
      throw ExceptionFilterOrder("Order must be greater than 0.");
    }
    assert(freq_cutoff_ >= 0.0);
    assert(freq_sample_ >= 0.0);
    cutoff_ratio_ = 2.0 * freq_cutoff_ / freq_sample_;  // 2.0 for Nyquist normalization
    if (cutoff_ratio_ > 1.0) {
      cutoff_ratio_ = 1.0;
    }

    a_ = ButterLowPassACoef(order_, cutoff_ratio_);
    b_ = ButterLowPassBCoef(order_, cutoff_ratio_);
    x_.assign(order_ + 1, value);
    y_.assign(order_, value);
    initialized_ = true;
  }

  int order_ = 0;
  double cutoff_ratio_ = -1;
  double freq_sample_ = -1;
  double freq_cutoff_ = -1;

  VecDouble a_;
  VecDouble b_;

  std::deque<T> x_;
  std::deque<T> y_;
  bool initialized_ = false;
};

}  // namespace filter
}  // namespace primitives

namespace YAML {

using primitives::filter::FilterButterLowPass;
using primitives::YamlCheckAndConvert;

template <typename T>
struct convert<FilterButterLowPass<T>> {
  static bool decode(const YAML::Node &node,
                     primitives::filter::FilterButterLowPass<T>
                         &filter) {  //  NOLINT(runtime/references)
    try {
      filter.SetOrderUninit(YamlCheckAndConvert<int>(node, "order"));
      filter.SetFreqSampleUninit(YamlCheckAndConvert<double>(node, "freq_sample"));
      filter.SetFreqCutoffUninit(YamlCheckAndConvert<double>(node, "freq_cutoff"));
      // Filter must be initialzed once its size is know.
    } catch (primitives::ExceptionYaml &e) {
      throw primitives::ExceptionYaml("primitives::FilterButterLowpass", e);
    }
    return true;
  }
};

}  // namespace YAML

#endif  // SRC_PRIMITIVES_INCLUDE_PRIMITIVES_BUTTER_H_
