/* Canvas Construction, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Canvas Construction, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#ifndef SRC_PRIMITIVES_INCLUDE_PRIMITIVES_EXCEPTION_H_
#define SRC_PRIMITIVES_INCLUDE_PRIMITIVES_EXCEPTION_H_

#include <stdexcept>
#include <string>

namespace primitives {
namespace exception {

class Exception : public std::runtime_error {
 public:
  explicit Exception(const std::string& msg) : std::runtime_error(msg) {}

  ~Exception() throw() {}
};

}  // namespace exception

// TODO(Tom): change to inherit from std::runtime_error
class Exception : public primitives::exception::Exception {
 public:
  explicit Exception(const std::string& msg) : primitives::exception::Exception(msg) {}
};

class ExceptionUninit : public Exception {
 public:
  explicit ExceptionUninit(const std::string& msg, const Exception e)
      : Exception(std::string(msg) + std::string("\n") + e.what()) {}
};

}  // namespace primitives
#endif  // SRC_PRIMITIVES_INCLUDE_PRIMITIVES_EXCEPTION_H_
