#!/usr/bin/env python
import rospy
import numpy as np
from dynamics_utils import *
import math
from math import pi, pow
import sys
from pdb import set_trace as pause
sys.path.append('./..')


# Calculate the link center of gavity given the center of gravity of the bodies and the valves
class DynParams():
    def __init__(self,numJoints=[1,2,3,4,5,6], typemass="ViveNoValves"):
        self.numJoints = numJoints
        self.type = typemass
        self.params = self.compute(numJoints)


    # def calc_cog(self, m_cyl, m_valve_1, m_valve_2, r_cyl, r_valve_1, r_valve_2):
    #     r_cyl = np.array(r_cyl)
    #     r_valve_1 = np.array(r_valve_1)
    #     r_valve_2 = np.array(r_valve_2)
        
    #     inter1 = r_cyl*m_cyl
    #     inter2 = r_valve_1*m_valve_1
    #     inter3 = r_valve_2*m_valve_2
    #     inter4 = m_cyl + m_valve_1 + m_valve_2
        
    #     inter5 = inter1 + inter2 + inter3
        
    #     r_cog = np.zeros(3)
        
    #     r_cog[0] = inter5[0] / inter4
    #     r_cog[1] = inter5[1] / inter4
    #     r_cog[2] = inter5[2] / inter4
    
    #     return r_cog.tolist()
    
    # # Calculate the Inertia of the link in the CoG frame with the bodies and
    # # the valves combined. This assumes the body is a cylider and the valve
    # # blocks are rectangular prisms


    def calcI(self,m,r,radIn, radOut,hLink, hValve):

        # m lumped mass
        # r center of gravity of valve block
        # rad1 inner radius of passive
        # rad2 outter radius of passive
        # h length of link
        m1 = 0.25*m
        m2 = 0.75*m
        # print m1
        # print m2
        # print radIn
        # print radOut
        # print hLink
        # print hValve
        Izz1 = m1/2.0*(pow(radIn,2.) + pow(radOut,2.))
        # print radIn
        # print radOut
        Iyy1 = m1/12.0*(3.*(pow(radIn,2.) + pow(radOut,2.)) + pow(hLink,2.))
        Ixx1 = m1/12.0*(3.*(pow(radIn,2.) + pow(radOut,2.)) + pow(hLink,2.))

        Izz2 = m2*pow(radIn,2.)/2.0
        Iyy2 = m2/12.0*(3.*pow(radIn,2.) + hValve)
        Ixx2 = m2/12.0*(3.*pow(radIn,2.) + hValve)
        
        Izz1Link = Izz1
        Iyy1Link = Iyy1 + m1*(pow(hLink/2.0,2.))
        Ixx1Link = Ixx1 + m1*(pow(hLink/2.0,2.))
        
        Izz2Link = Izz2
        Iyy2Link = Iyy2 + m2*(pow(r,2.))
        Ixx2Link = Ixx2 + m2*(pow(r,2.))

        Ixx = Ixx1Link + Ixx2Link
        Iyy = Iyy1Link + Iyy2Link
        Izz = Izz1Link + Izz2Link
        
        return Ixx, Iyy, Izz

    def calcINoValves(self,m,r,radIn, radOut,hLink):
        Izz = m/2.0*(pow(radIn,2.) + pow(radOut,2.))
        Iyy = m/12.0*(3.*(pow(radIn,2.) + pow(radOut,2.)) + pow(hLink,2.))
        Ixx = m/12.0*(3.*(pow(radIn,2.) + pow(radOut,2.)) + pow(hLink,2.))

        return Ixx, Iyy, Izz

    def getMass(self):
        massJoint = [ 0.600, 0.600, 0.505, 0.4496, 0.4522, 0.7302]
        massLink = [0.000, 1.642, 1.2242, 0.9759, 0.9911,0.0] #w/o vive
        massLinkVive = [0.000, 1.7361, 1.2242, 1.070, 0.9911, 0.0941] #w vive
        massLinkViveandEE = [0.000, 1.7361, 1.2242, 1.070, 0.9911, 0.5788] #w vive & ee
        #----------------------------------------------------------------------------------------------
        # all masses in kg
        # these are the measurements with no valves inside
        mJ1L1J2 = 1.75
        mL2 = 0.5453
        mJ3 = 0.5076
        mL3 = 0.3218
        mJ4 = 0.4593
        mL4 = 0.1552
        mJ5 = 0.4536
        mL5 = 0.2052
        mJ6L6 = 0.7285
        hos = 0.06826 #hosing is 0.06826 kg/m
        mJ1 = mJ3 + .05 #assuming the step from joint 3 to joints 1 and 2 is 50 grams like the step from joint 4 to joint 3
        mJ2 = mJ3 + .05 #assuming the step from joint 3 to joints 1 and 2 is 50 grams like the step from joint 4 to joint 3
        mL1 = mJ1L1J2 - mJ1 - mJ2
        mJ6 = mJ5
        mL6 = mJ6L6 - mJ6
        mVive = .0896 # mass of a vive puck
        mBracket = .0396 # mass of l bracket and bolts holding last vive on
        lengths = [0.0600, 0.5270, 0.3460, 0.2240, 0.2700, 0.0300] #lengths between joints
        m1 = mJ1/2. + mL1 + mJ2/2. + 11.*hos*lengths[0]
        m2 = mJ2/2. + mL2 + mJ3/2. + 9.*hos*lengths[1] + mVive
        m3 = mJ3/2. + mL3 + mJ4/2. + 7.*hos*lengths[2]
        m4 = mJ4/2. + mL4 + mJ5/2. + 5.*hos*lengths[3] + mVive
        m5 = mJ5/2. + mL5 + mJ6/2. + 3.*hos*lengths[4]
        m6 = mJ6/2. + mL6 + mVive + mBracket

        massViveNoValve = [m1, m2, m3, m4, m5, m6]
        #----------------------------------------------------------------------------------------------
        m = [0.0]*len(massJoint)  
        mVive = [0.0]*len(massJoint)  
        mViveEE = [0.0]*len(massJoint)
        for i in range(len(massLink)-1):
            m[i] = massJoint[i]/2. + massLink[i] + massJoint[i+1]/2.
            mVive[i] = massJoint[i]/2. + massLinkVive[i] + massJoint[i+1]/2.
            mViveEE[i] = massJoint[i]/2. + massLinkViveandEE[i] + massJoint[i+1]/2.
            m[5] = massJoint[5]/2. + massLink[5]
            mVive[5] = massJoint[5]/2. + massLinkVive[5]
            mViveEE[5] = massJoint[5]/2. + massLinkViveandEE[5]

        if self.type == "No_Vive":
            returnMass = m
    
        elif self.type == "Vive":
            returnMass = mVive
            
        elif self.type == "ViveEE":
            returnMass = mViveEE
        elif self.type == "ViveNoValves":
            returnMass = massViveNoValve
            
        return returnMass

    def getCOG(self):
        if self.type == "ViveNoValves":
            # all masses in kg
            # these are the measurements with no valves inside
            mJ1L1J2 = 1.75
            mL2 = 0.5453
            mJ3 = 0.5076
            mL3 = 0.3218
            mJ4 = 0.4593
            mL4 = 0.1552
            mJ5 = 0.4536
            mL5 = 0.2052
            mJ6L6 = 0.7285
            hos = 0.06826 #hosing is 0.06826 kg/m
            mJ1 = mJ3 + .05 #assuming the step from joint 3 to joints 1 and 2 is 50 grams like the step from joint 4 to joint 3
            mJ2 = mJ3 + .05 #assuming the step from joint 3 to joints 1 and 2 is 50 grams like the step from joint 4 to joint 3
            mL1 = mJ1L1J2 - mJ1 - mJ2
            mJ6 = mJ5
            mL6 = mJ6L6 - mJ6
            mVive = .0896 # mass of a vive puck
            mBracket = .0396 # mass of l bracket and bolts holding last vive on
            lengths = [0.0600, 0.5270, 0.3460, 0.2240, 0.2700, 0.0300] #lengths between joints
            m1 = mJ1/2. + mL1 + mJ2/2. + 11.*hos*lengths[0]
            m2 = mJ2/2. + mL2 + mJ3/2. + 9.*hos*lengths[1] + mVive
            m3 = mJ3/2. + mL3 + mJ4/2. + 7.*hos*lengths[2]
            m4 = mJ4/2. + mL4 + mJ5/2. + 5.*hos*lengths[3] + mVive
            m5 = mJ5/2. + mL5 + mJ6/2. + 3.*hos*lengths[4]
            m6 = mJ6/2. + mL6 + mVive + mBracket

            jL = [.075, .075, .08, .07, .075, .075]# joint lengths

            p1 = ((mJ1/2.)*jL[0]/4. + (mL1 + 11*hos*lengths[0])*lengths[0]/2. + (mJ2/2.)*(lengths[0] - jL[1]/4.))/m1
            p2 = ((mJ2/2.)*jL[1]/4. + (mL2 + 9*hos*lengths[1] + mVive)*lengths[1]/2. + (mJ3/2.)*(lengths[1] - jL[2]/4.))/m2
            p3 = ((mJ3/2.)*jL[2]/4. + (mL3 + 7*hos*lengths[2])*lengths[2]/2. + (mJ4/2.)*(lengths[2] - jL[3]/4.))/m3
            p4 = ((mJ4/2.)*jL[3]/4. + (mL4 + 5*hos*lengths[3] + mVive)*lengths[3]/2. + (mJ5/2.)*(lengths[3] - jL[4]/4.))/m4
            p5 = ((mJ5/2.)*jL[4]/4. + (mL5 + 3*hos*lengths[4])*lengths[4]/2. + (mJ6/2.)*(lengths[4] - jL[5]/4.))/m5
            p6 = ((mJ6/2.)*jL[5]/4. + mL6*(lengths[5]/2.) + (mVive + mBracket)*(lengths[5] + .03))/m6
            rViveNoValve = [p1, p2, p3, p4, p5, p6]
            r = rViveNoValve
        else:
            original_r = [0.05, 0.5270, 0.15, 0.15, 0.14, 0.015]
            r = original_r
        return r

    def getLinkHeights(self):
        # hLink = [0.100, 0.460, 0.370, 0.230, 0.300, 0.0300] 
        # I added this -Dustan ------------------------------------------------------------------------
        hLink = [0.0600, 0.5270, 0.3460, 0.2240, 0.2700, 0.0300] #lengths between joints
        #----------------------------------------------------------------------------------------------
        return hLink
    def getRadius(self):
        m = self.getMass()
        radius = []
        for i in range(len(m)):
            
            if i == 0 or i == 1:
                radIn = (0.200-0.08)/2.
                radOut = 0.200/2.0
                radius.append(radOut)
            elif i == 2:
                radIn = (0.180-0.035*2.)/2.
                radOut = 0.180/2.0
                radius.append(radOut)
            else:
                radIn = (0.160-0.035*2.0)/2.0
                radOut = 0.160/2.0
                radius.append(radOut)

        return radius

    def getI(self):
        Ixx = []
        Iyy = []
        Izz = []
        
        IxxVive = []
        IyyVive = []
        IzzVive = []
        
        IxxViveEE = []
        IyyViveEE = []
        IzzViveEE = []
        
        
        
        # hLink = [0.100, 0.460, 0.370, 0.230, 0.300, 0.0300] 
        m = self.getMass()
        r = self.getCOG()
        hLink = self.getLinkHeights()
        for i in range(len(m)):
            
            if i == 0 or i == 1:
                radIn = (0.200-0.08)/2.
                radOut = 0.200/2.0
            elif i == 2:
                radIn = (0.180-0.035*2.)/2.
                radOut = 0.180/2.0
            else:
                radIn = (0.160-0.035*2.0)/2.0
                radOut = 0.160/2.0

            hValve = 0.140

            IxxBuff, IyyBuff, IzzBuff = self.calcI(m[i], r[i], radIn, radOut, hLink[i], hValve)
            IxxBuffVive, IyyBuffVive, IzzBuffVive = self.calcI(m[i], r[i], radIn, radOut, hLink[i], hValve)
            IxxBuffViveEE, IyyBuffViveEE, IzzBuffViveEE = self.calcI(m[i], r[i], radIn, radOut, hLink[i], hValve)


            Ixx.append(IxxBuff)
            Iyy.append(IyyBuff)
            Izz.append(IzzBuff)
            
            IxxVive.append(IxxBuffVive)
            IyyVive.append(IyyBuffVive)
            IzzVive.append(IzzBuffVive)
            
            IxxViveEE.append(IxxBuffViveEE)
            IyyViveEE.append(IyyBuffViveEE)
            IzzViveEE.append(IzzBuffViveEE)
    

        I1 = [Ixx[0], Iyy[0], Izz[0], 0.0, 0.0, 0.0]
        I2 = [Ixx[1], Iyy[1], Izz[1], 0.0, 0.0, 0.0]
        I3 = [Ixx[2], Iyy[2], Izz[2], 0.0, 0.0, 0.0]
        I4 = [Ixx[3], Iyy[3], Izz[3], 0.0, 0.0, 0.0]
        I5 = [Ixx[4], Iyy[4], Izz[4], 0.0, 0.0, 0.0]
        I6 = [Ixx[5], Iyy[5], Izz[5], 0.0, 0.0, 0.0]

        I1Vive = [IxxVive[0], IyyVive[0], IzzVive[0], 0.0, 0.0, 0.0]
        I2Vive = [IxxVive[1], IyyVive[1], IzzVive[1], 0.0, 0.0, 0.0]
        I3Vive = [IxxVive[2], IyyVive[2], IzzVive[2], 0.0, 0.0, 0.0]
        I4Vive = [IxxVive[3], IyyVive[3], IzzVive[3], 0.0, 0.0, 0.0]
        I5Vive = [IxxVive[4], IyyVive[4], IzzVive[4], 0.0, 0.0, 0.0]
        I6Vive = [IxxVive[5], IyyVive[5], IzzVive[5], 0.0, 0.0, 0.0]

        I1ViveEE = [IxxViveEE[0], IyyViveEE[0], IzzViveEE[0], 0.0, 0.0, 0.0]
        I2ViveEE = [IxxViveEE[1], IyyViveEE[1], IzzViveEE[1], 0.0, 0.0, 0.0]
        I3ViveEE = [IxxViveEE[2], IyyViveEE[2], IzzViveEE[2], 0.0, 0.0, 0.0]
        I4ViveEE = [IxxViveEE[3], IyyViveEE[3], IzzViveEE[3], 0.0, 0.0, 0.0]
        I5ViveEE = [IxxViveEE[4], IyyViveEE[4], IzzViveEE[4], 0.0, 0.0, 0.0]
        I6ViveEE = [IxxViveEE[5], IyyViveEE[5], IzzViveEE[5], 0.0, 0.0, 0.0]

        if self.type == "No_Vive":
            I1 = I1
            I2 = I2
            I3 = I3
            I4 = I4
            I5 = I5
            I6 = I6
    
        elif self.type == "Vive":
            I1 = I1Vive
            I2 = I2Vive
            I3 = I3Vive
            I4 = I4Vive
            I5 = I5Vive
            I6 = I6Vive
                               
        elif self.type == "ViveEE":
            I1 = I1ViveEE
            I2 = I2ViveEE
            I3 = I3ViveEE
            I4 = I4ViveEE
            I5 = I5ViveEE
            I6 = I6ViveEE

        return I1, I2, I3, I4, I5, I6
    
        
    def compute(self,jointNums):
        
        # Approximated center of gravity of each link. I measuerd 
        # to the middle of the valve manifold from the center of the
        # next joint.
        r = self.getCOG()
        
        # Masses of everything in kilograms if it is .100 even then it is not
        # available
        # massJoint = [ 0.600, 0.600, 0.505, 0.4496, 0.4522, 0.7302]
        # massLink = [0.000, 1.642, 1.2242, 0.9759, 0.9911,0.0] #w/o vive
        # massLinkVive = [0.000, 1.7361, 1.2242, 1.070, 0.9911, 0.0941] #w vive
        # massLinkViveandEE = [0.000, 1.7361, 1.2242, 1.070, 0.9911, 0.5788] #w vive & ee
        # m = [0.0]*len(massJoint)  
        # mVive = [0.0]*len(massJoint)  
        # mViveEE = [0.0]*len(massJoint)  
        # for i in xrange(len(massLink)-1):
        #     m[i] = massJoint[i]/2. + massLink[i] + massJoint[i+1]/2.
        #     mVive[i] = massJoint[i]/2. + massLinkVive[i] + massJoint[i+1]/2.
        #     mViveEE[i] = massJoint[i]/2. + massLinkViveandEE[i] + massJoint[i+1]/2.
        #     m[5] = massJoint[5]/2. + massLink[5]
        #     mVive[5] = massJoint[5]/2. + massLinkVive[5]
        #     mViveEE[5] = massJoint[5]/2. + massLinkViveandEE[5]

        m = self.getMass()
        hLink = self.getLinkHeights()
            
        fv1 = 0.7
        fv2 = 0.7
        fv3 = 0.7
        fv4 = 0.7
        fv5 = 0.7
        fv6 = 0.7
        
        Ixx = []
        Iyy = []
        Izz = []
        
        IxxVive = []
        IyyVive = []
        IzzVive = []

        IxxViveNoValve = []
        IyyViveNoValve = []
        IzzViveNoValve = []
        
        IxxViveEE = []
        IyyViveEE = []
        IzzViveEE = []
        
        
        
        # hLink = [0.100, 0.460, 0.370, 0.230, 0.300, 0.0300] 
        
        for i in range(len(m)):
            
            if i == 0 or i == 1:
                radIn = (0.200-0.08)/2.
                radOut = 0.200/2.0
            elif i == 2:
                radIn = (0.180-0.035*2.)/2.
                radOut = 0.180/2.0
            else:
                radIn = (0.160-0.035*2.0)/2.0
                radOut = 0.160/2.0

            hValve = 0.140
            IxxBuff, IyyBuff, IzzBuff = self.calcI(m[i], r[i], radIn, radOut, hLink[i], hValve)
            IxxBuffVive, IyyBuffVive, IzzBuffVive = self.calcI(m[i], r[i], radIn, radOut, hLink[i], hValve)
            IxxBuffViveEE, IyyBuffViveEE, IzzBuffViveEE = self.calcI(m[i], r[i], radIn, radOut, hLink[i], hValve)
            IxxBuffViveNoValve, IyyBuffViveNoValve, IzzBuffViveNoValve = self.calcINoValves(m[i], r[i], radIn, radOut, hLink[i])

            Ixx.append(IxxBuff)
            Iyy.append(IyyBuff)
            Izz.append(IzzBuff)
            
            IxxVive.append(IxxBuffVive)
            IyyVive.append(IyyBuffVive)
            IzzVive.append(IzzBuffVive)

            IxxViveNoValve.append(IxxBuffVive)
            IyyViveNoValve.append(IyyBuffVive)
            IzzViveNoValve.append(IzzBuffVive)
            
            IxxViveEE.append(IxxBuffViveEE)
            IyyViveEE.append(IyyBuffViveEE)
            IzzViveEE.append(IzzBuffViveEE)
    

        I1 = [Ixx[0], Iyy[0], Izz[0], 0.0, 0.0, 0.0]
        I2 = [Ixx[1], Iyy[1], Izz[1], 0.0, 0.0, 0.0]
        I3 = [Ixx[2], Iyy[2], Izz[2], 0.0, 0.0, 0.0]
        I4 = [Ixx[3], Iyy[3], Izz[3], 0.0, 0.0, 0.0]
        I5 = [Ixx[4], Iyy[4], Izz[4], 0.0, 0.0, 0.0]
        I6 = [Ixx[5], Iyy[5], Izz[5], 0.0, 0.0, 0.0]

        I1Vive = [IxxVive[0], IyyVive[0], IzzVive[0], 0.0, 0.0, 0.0]
        I2Vive = [IxxVive[1], IyyVive[1], IzzVive[1], 0.0, 0.0, 0.0]
        I3Vive = [IxxVive[2], IyyVive[2], IzzVive[2], 0.0, 0.0, 0.0]
        I4Vive = [IxxVive[3], IyyVive[3], IzzVive[3], 0.0, 0.0, 0.0]
        I5Vive = [IxxVive[4], IyyVive[4], IzzVive[4], 0.0, 0.0, 0.0]
        I6Vive = [IxxVive[5], IyyVive[5], IzzVive[5], 0.0, 0.0, 0.0]

        I1ViveNoValve = [IxxViveNoValve[0], IyyViveNoValve[0], IzzViveNoValve[0], 0.0, 0.0, 0.0]
        I2ViveNoValve = [IxxViveNoValve[1], IyyViveNoValve[1], IzzViveNoValve[1], 0.0, 0.0, 0.0]
        I3ViveNoValve = [IxxViveNoValve[2], IyyViveNoValve[2], IzzViveNoValve[2], 0.0, 0.0, 0.0]
        I4ViveNoValve = [IxxViveNoValve[3], IyyViveNoValve[3], IzzViveNoValve[3], 0.0, 0.0, 0.0]
        I5ViveNoValve = [IxxViveNoValve[4], IyyViveNoValve[4], IzzViveNoValve[4], 0.0, 0.0, 0.0]
        I6ViveNoValve = [IxxViveNoValve[5], IyyViveNoValve[5], IzzViveNoValve[5], 0.0, 0.0, 0.0]

        I1ViveEE = [IxxViveEE[0], IyyViveEE[0], IzzViveEE[0], 0.0, 0.0, 0.0]
        I2ViveEE = [IxxViveEE[1], IyyViveEE[1], IzzViveEE[1], 0.0, 0.0, 0.0]
        I3ViveEE = [IxxViveEE[2], IyyViveEE[2], IzzViveEE[2], 0.0, 0.0, 0.0]
        I4ViveEE = [IxxViveEE[3], IyyViveEE[3], IzzViveEE[3], 0.0, 0.0, 0.0]
        I5ViveEE = [IxxViveEE[4], IyyViveEE[4], IzzViveEE[4], 0.0, 0.0, 0.0]
        I6ViveEE = [IxxViveEE[5], IyyViveEE[5], IzzViveEE[5], 0.0, 0.0, 0.0]



        #I1 = [ Ixx1, Iyy1, Izz1, Ixy1, Iyz1, Ixz1]
        #I2 = [ Ixx2, Iyy2, Izz2, Ixy2, Iyz2, Ixz2]
        #I3 = [ Ixx3, Iyy3, Izz3, Ixy3, Iyz3, Ixz3]
        #I4 = [ Ixx4, Iyy4, Izz4, Ixy4, Iyz4, Ixz4]
        #I5 = [ Ixx5, Iyy5, Izz5, Ixy5, Iyz5, Ixz5]
        #I6 = [ Ixx6, Iyy6, Izz6, Ixy6, Iyz6, Ixz6]

        if self.type == "ViveNoValves":
            r1 = [r[0], 0, 0]
            r2 = [r[1], 0, 0]
            r3 = [r[2], 0, 0]
            r4 = [r[3], 0, 0]
            r5 = [r[4], 0, 0]
            r6 = [r[5], 0, 0]
        else:
            r1  = [-0.5*hLink[0], 0, 0]
            r2  = [- 0.5*hLink[1], 0, 0]
            r3  = [-0.75*r[2] - 0.25*hLink[2], 0, 0]
            r4  = [-0.75*r[3] - 0.25*hLink[3], 0, 0]
            r5  = [-0.75*r[4] - 0.25*hLink[4], 0, 0]
            r6  = [-0.75*r[5] - 0.25*hLink[5], 0, 0]

        if self.type == "No_Vive":
            L1 = I_to_matrix(I1)-m[0]*skew(r1)*skew(r1)
            L2 = I_to_matrix(I2)-m[1]*skew(r2)*skew(r2)
            L3 = I_to_matrix(I3)-m[2]*skew(r3)*skew(r3)
            L4 = I_to_matrix(I4)-m[3]*skew(r4)*skew(r4)
            L5 = I_to_matrix(I5)-m[4]*skew(r5)*skew(r5)
            L6 = I_to_matrix(I6)-m[5]*skew(r6)*skew(r6)
        elif self.type == "Vive":
            L1 = I_to_matrix(I1Vive)-m[0]*skew(r1)*skew(r1)
            L2 = I_to_matrix(I2Vive)-m[1]*skew(r2)*skew(r2)
            L3 = I_to_matrix(I3Vive)-m[2]*skew(r3)*skew(r3)
            L4 = I_to_matrix(I4Vive)-m[3]*skew(r4)*skew(r4)
            L5 = I_to_matrix(I5Vive)-m[4]*skew(r5)*skew(r5)
            L6 = I_to_matrix(I6Vive)-m[5]*skew(r6)*skew(r6)
        elif self.type == "ViveEE":
            L1 = I_to_matrix(I1ViveEE)-m[0]*skew(r1)*skew(r1)
            L2 = I_to_matrix(I2ViveEE)-m[1]*skew(r2)*skew(r2)
            L3 = I_to_matrix(I3ViveEE)-m[2]*skew(r3)*skew(r3)
            L4 = I_to_matrix(I4ViveEE)-m[3]*skew(r4)*skew(r4)
            L5 = I_to_matrix(I5ViveEE)-m[4]*skew(r5)*skew(r5)
            L6 = I_to_matrix(I6ViveEE)-m[5]*skew(r6)*skew(r6)
        elif self.type == "ViveNoValves":
            L1 = I_to_matrix(I1ViveNoValve)-m[0]*skew(r1)*skew(r1)
            L2 = I_to_matrix(I2ViveNoValve)-m[1]*skew(r2)*skew(r2)
            L3 = I_to_matrix(I3ViveNoValve)-m[2]*skew(r3)*skew(r3)
            L4 = I_to_matrix(I4ViveNoValve)-m[3]*skew(r4)*skew(r4)
            L5 = I_to_matrix(I5ViveNoValve)-m[4]*skew(r5)*skew(r5)
            L6 = I_to_matrix(I6ViveNoValve)-m[5]*skew(r6)*skew(r6)

        params = []

        params.append([L1[0, 0], L1[0, 1], L1[0, 2], L1[1, 1], L1[1, 2], L1[2, 2], r1[0]*m[0], r1[1]*m[0], r1[2]*m[0], m[0], fv1])
        params.append([L2[0, 0], L2[0, 1], L2[0, 2], L2[1, 1], L2[1, 2], L2[2, 2], r2[0]*m[1], r2[1]*m[1], r2[2]*m[1], m[1], fv2])
        params.append([L3[0, 0], -L3[0, 1], -L3[0, 2], L3[1, 1], -L3[1, 2], L3[2, 2], r3[0]*m[2], r3[1]*m[2], r3[2]*m[2], m[2], fv3])
        params.append([L4[0, 0], -L4[0, 1], -L4[0, 2], L4[1, 1], -L4[1, 2], L4[2, 2], r4[0]*m[3], r4[1]*m[3], r4[2]*m[3], m[3], fv4])
        params.append([L5[0,0], -L5[0,1], -L5[0,2], L5[1,1], -L5[1,2], L5[2,2], r5[0]*m[4], r5[1]*m[4], r5[2]*m[4], m[4], fv5])
        params.append([L6[0,0], -L6[0,1], -L6[0,2], L6[1,1], -L6[1,2], L6[2,2], r6[0]*m[5], r6[1]*m[5], r6[2]*m[5], m[5], fv6])
        
        print(params)
        passParams = []
        for i in range(len(jointNums)):
            passParams = passParams + params[jointNums[i]-1]
            #params = params[(startJoint-1)*11:11*endJoint]
        return passParams



