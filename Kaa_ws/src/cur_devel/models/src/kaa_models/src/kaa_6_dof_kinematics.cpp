void joint_fk00( double* pose, const double* q )
{
  double x0 = cos(q[0]);
  double x1 = sin(q[0]);
//
  pose[0] = x0;
  pose[1] = 0;
  pose[2] = x1;
  pose[3] = 0.059999999999999998*x0;
  pose[4] = x1;
  pose[5] = 0;
  pose[6] = -x0;
  pose[7] = 0.059999999999999998*x1;
  pose[8] = 0;
  pose[9] = 1;
  pose[10] = 0;
  pose[11] = 0;
  pose[12] = 0;
  pose[13] = 0;
  pose[14] = 0;
  pose[15] = 1;
//
  return;
}



void joint_fk01( double* pose, const double* q )
{
  double x0 = cos(q[0]);
  double x1 = cos(q[1]);
  double x2 = x0*x1;
  double x3 = sin(q[0]);
  double x4 = sin(q[1]);
  double x5 = x1*x3;
//
  pose[0] = x2;
  pose[1] = -x3;
  pose[2] = -x0*x4;
  pose[3] = 0.059999999999999998*x0 + 0.52700000000000002*x2;
  pose[4] = x5;
  pose[5] = x0;
  pose[6] = -x3*x4;
  pose[7] = 0.059999999999999998*x3 + 0.52700000000000002*x5;
  pose[8] = x4;
  pose[9] = 0;
  pose[10] = x1;
  pose[11] = 0.52700000000000002*x4;
  pose[12] = 0;
  pose[13] = 0;
  pose[14] = 0;
  pose[15] = 1;
//
  return;
}



void joint_fk02( double* pose, const double* q )
{
  double x0 = sin(q[0]);
  double x1 = sin(q[2]);
  double x2 = x0*x1;
  double x3 = cos(q[1]);
  double x4 = cos(q[0]);
  double x5 = cos(q[2]);
  double x6 = x4*x5;
  double x7 = x3*x6;
  double x8 = sin(q[1]);
  double x9 = x0*x5;
  double x10 = x1*x4;
  double x11 = 0.52700000000000002*x3;
  double x12 = x3*x9;
  double x13 = x5*x8;
//
  pose[0] = -x2 + x7;
  pose[1] = -x4*x8;
  pose[2] = x10*x3 + x9;
  pose[3] = x11*x4 - 0.34599999999999997*x2 + 0.059999999999999998*x4 + 0.34599999999999997*x7;
  pose[4] = x10 + x12;
  pose[5] = -x0*x8;
  pose[6] = x2*x3 - x6;
  pose[7] = x0*x11 + 0.059999999999999998*x0 + 0.34599999999999997*x10 + 0.34599999999999997*x12;
  pose[8] = x13;
  pose[9] = x3;
  pose[10] = x1*x8;
  pose[11] = 0.34599999999999997*x13 + 0.52700000000000002*x8;
  pose[12] = 0;
  pose[13] = 0;
  pose[14] = 0;
  pose[15] = 1;
//
  return;
}



void joint_fk03( double* pose, const double* q )
{
  double x0 = sin(q[3]);
  double x1 = cos(q[0]);
  double x2 = sin(q[1]);
  double x3 = x1*x2;
  double x4 = x0*x3;
  double x5 = cos(q[3]);
  double x6 = sin(q[0]);
  double x7 = sin(q[2]);
  double x8 = x6*x7;
  double x9 = cos(q[1]);
  double x10 = cos(q[2]);
  double x11 = x1*x10;
  double x12 = x11*x9;
  double x13 = x12 - x8;
  double x14 = x13*x5;
  double x15 = x10*x6;
  double x16 = x1*x7;
  double x17 = 0.52700000000000002*x9;
  double x18 = x2*x6;
  double x19 = x0*x18;
  double x20 = x15*x9;
  double x21 = x16 + x20;
  double x22 = x21*x5;
  double x23 = x0*x9;
  double x24 = x10*x2;
  double x25 = x24*x5;
//
  pose[0] = x14 - x4;
  pose[1] = -x15 - x16*x9;
  pose[2] = -x0*x13 - x3*x5;
  pose[3] = x1*x17 + 0.059999999999999998*x1 + 0.34599999999999997*x12 + 0.224*x14 - 0.224*x4 - 0.34599999999999997*x8;
  pose[4] = -x19 + x22;
  pose[5] = x11 - x8*x9;
  pose[6] = -x0*x21 - x18*x5;
  pose[7] = 0.34599999999999997*x16 + x17*x6 - 0.224*x19 + 0.34599999999999997*x20 + 0.224*x22 + 0.059999999999999998*x6;
  pose[8] = x23 + x25;
  pose[9] = -x2*x7;
  pose[10] = -x0*x24 + x5*x9;
  pose[11] = 0.52700000000000002*x2 + 0.224*x23 + 0.34599999999999997*x24 + 0.224*x25;
  pose[12] = 0;
  pose[13] = 0;
  pose[14] = 0;
  pose[15] = 1;
//
  return;
}



void joint_fk04( double* pose, const double* q )
{
  double x0 = sin(q[4]);
  double x1 = sin(q[0]);
  double x2 = cos(q[2]);
  double x3 = x1*x2;
  double x4 = cos(q[1]);
  double x5 = sin(q[2]);
  double x6 = cos(q[0]);
  double x7 = x5*x6;
  double x8 = -x3 - x4*x7;
  double x9 = x0*x8;
  double x10 = cos(q[4]);
  double x11 = sin(q[3]);
  double x12 = sin(q[1]);
  double x13 = x12*x6;
  double x14 = x11*x13;
  double x15 = cos(q[3]);
  double x16 = x1*x5;
  double x17 = x2*x6;
  double x18 = x17*x4;
  double x19 = -x16 + x18;
  double x20 = x15*x19;
  double x21 = -x14 + x20;
  double x22 = x10*x21;
  double x23 = 0.52700000000000002*x4;
  double x24 = -x16*x4 + x17;
  double x25 = x0*x24;
  double x26 = x1*x12;
  double x27 = x11*x26;
  double x28 = x3*x4;
  double x29 = x28 + x7;
  double x30 = x15*x29;
  double x31 = -x27 + x30;
  double x32 = x10*x31;
  double x33 = x12*x5;
  double x34 = x0*x33;
  double x35 = x11*x4;
  double x36 = x12*x2;
  double x37 = x15*x36;
  double x38 = x35 + x37;
  double x39 = x10*x38;
//
  pose[0] = x22 + x9;
  pose[1] = -x11*x19 - x13*x15;
  pose[2] = x0*x21 - x10*x8;
  pose[3] = -0.224*x14 - 0.34599999999999997*x16 + 0.34599999999999997*x18 + 0.224*x20 + 0.27000000000000002*x22 + x23*x6 + 0.059999999999999998*x6 + 0.27000000000000002*x9;
  pose[4] = x25 + x32;
  pose[5] = -x11*x29 - x15*x26;
  pose[6] = x0*x31 - x10*x24;
  pose[7] = x1*x23 + 0.059999999999999998*x1 + 0.27000000000000002*x25 - 0.224*x27 + 0.34599999999999997*x28 + 0.224*x30 + 0.27000000000000002*x32 + 0.34599999999999997*x7;
  pose[8] = -x34 + x39;
  pose[9] = -x11*x36 + x15*x4;
  pose[10] = x0*x38 + x10*x33;
  pose[11] = 0.52700000000000002*x12 - 0.27000000000000002*x34 + 0.224*x35 + 0.34599999999999997*x36 + 0.224*x37 + 0.27000000000000002*x39;
  pose[12] = 0;
  pose[13] = 0;
  pose[14] = 0;
  pose[15] = 1;
//
  return;
}



void joint_fk05( double* pose, const double* q )
{
  double x0 = sin(q[5]);
  double x1 = cos(q[3]);
  double x2 = cos(q[0]);
  double x3 = sin(q[1]);
  double x4 = x2*x3;
  double x5 = sin(q[3]);
  double x6 = sin(q[0]);
  double x7 = sin(q[2]);
  double x8 = x6*x7;
  double x9 = cos(q[1]);
  double x10 = cos(q[2]);
  double x11 = x10*x2;
  double x12 = x11*x9;
  double x13 = x12 - x8;
  double x14 = -x1*x4 - x13*x5;
  double x15 = x0*x14;
  double x16 = cos(q[5]);
  double x17 = sin(q[4]);
  double x18 = x10*x6;
  double x19 = x2*x7;
  double x20 = -x18 - x19*x9;
  double x21 = x17*x20;
  double x22 = cos(q[4]);
  double x23 = x4*x5;
  double x24 = x1*x13;
  double x25 = -x23 + x24;
  double x26 = x22*x25;
  double x27 = x21 + x26;
  double x28 = x16*x27;
  double x29 = 0.52700000000000002*x9;
  double x30 = x3*x6;
  double x31 = x18*x9;
  double x32 = x19 + x31;
  double x33 = -x1*x30 - x32*x5;
  double x34 = x0*x33;
  double x35 = x11 - x8*x9;
  double x36 = x17*x35;
  double x37 = x30*x5;
  double x38 = x1*x32;
  double x39 = -x37 + x38;
  double x40 = x22*x39;
  double x41 = x36 + x40;
  double x42 = x16*x41;
  double x43 = x10*x3;
  double x44 = x1*x9 - x43*x5;
  double x45 = x0*x44;
  double x46 = x3*x7;
  double x47 = x17*x46;
  double x48 = x5*x9;
  double x49 = x1*x43;
  double x50 = x48 + x49;
  double x51 = x22*x50;
  double x52 = -x47 + x51;
  double x53 = x16*x52;
//
  pose[0] = x15 + x28;
  pose[1] = -x0*x27 + x14*x16;
  pose[2] = x17*x25 - x20*x22;
  pose[3] = 0.34599999999999997*x12 + 0.029999999999999999*x15 + x2*x29 + 0.059999999999999998*x2 + 0.27000000000000002*x21 - 0.224*x23 + 0.224*x24 + 0.27000000000000002*x26 + 0.029999999999999999*x28 - 0.34599999999999997*x8;
  pose[4] = x34 + x42;
  pose[5] = -x0*x41 + x16*x33;
  pose[6] = x17*x39 - x22*x35;
  pose[7] = 0.34599999999999997*x19 + x29*x6 + 0.34599999999999997*x31 + 0.029999999999999999*x34 + 0.27000000000000002*x36 - 0.224*x37 + 0.224*x38 + 0.27000000000000002*x40 + 0.029999999999999999*x42 + 0.059999999999999998*x6;
  pose[8] = x45 + x53;
  pose[9] = -x0*x52 + x16*x44;
  pose[10] = x17*x50 + x22*x46;
  pose[11] = 0.52700000000000002*x3 + 0.34599999999999997*x43 + 0.029999999999999999*x45 - 0.27000000000000002*x47 + 0.224*x48 + 0.224*x49 + 0.27000000000000002*x51 + 0.029999999999999999*x53;
  pose[12] = 0;
  pose[13] = 0;
  pose[14] = 0;
  pose[15] = 1;
//
  return;
}



FK = {0:joint_fk00, 1:joint_fk01, 2:joint_fk02, 3:joint_fk03, 4:joint_fk04, 5:joint_fk05, }



void jacobian00( double* jacobian, const double* q )
{
//
  jacobian[0] = -0.059999999999999998*sin(q[0]);
  jacobian[1] = 0;
  jacobian[2] = 0;
  jacobian[3] = 0;
  jacobian[4] = 0;
  jacobian[5] = 0;
  jacobian[6] = 0.059999999999999998*cos(q[0]);
  jacobian[7] = 0;
  jacobian[8] = 0;
  jacobian[9] = 0;
  jacobian[10] = 0;
  jacobian[11] = 0;
  jacobian[12] = 0;
  jacobian[13] = 0;
  jacobian[14] = 0;
  jacobian[15] = 0;
  jacobian[16] = 0;
  jacobian[17] = 0;
  jacobian[18] = 0;
  jacobian[19] = 0;
  jacobian[20] = 0;
  jacobian[21] = 0;
  jacobian[22] = 0;
  jacobian[23] = 0;
  jacobian[24] = 0;
  jacobian[25] = 0;
  jacobian[26] = 0;
  jacobian[27] = 0;
  jacobian[28] = 0;
  jacobian[29] = 0;
  jacobian[30] = 1;
  jacobian[31] = 0;
  jacobian[32] = 0;
  jacobian[33] = 0;
  jacobian[34] = 0;
  jacobian[35] = 0;
//
  return;
    jacobian = np.array(jacobian).reshape(6,6)
}



void jacobian01( double* jacobian, const double* q )
{
  double x0 = sin(q[0]);
  double x1 = 0.52700000000000002*cos(q[1]);
  double x2 = cos(q[0]);
  double x3 = 0.52700000000000002*sin(q[1]);
//
  jacobian[0] = -x0*x1 - 0.059999999999999998*x0;
  jacobian[1] = -x2*x3;
  jacobian[2] = 0;
  jacobian[3] = 0;
  jacobian[4] = 0;
  jacobian[5] = 0;
  jacobian[6] = x1*x2 + 0.059999999999999998*x2;
  jacobian[7] = -x0*x3;
  jacobian[8] = 0;
  jacobian[9] = 0;
  jacobian[10] = 0;
  jacobian[11] = 0;
  jacobian[12] = 0;
  jacobian[13] = ((x0)*(x0))*x1 + x1*((x2)*(x2));
  jacobian[14] = 0;
  jacobian[15] = 0;
  jacobian[16] = 0;
  jacobian[17] = 0;
  jacobian[18] = 0;
  jacobian[19] = x0;
  jacobian[20] = 0;
  jacobian[21] = 0;
  jacobian[22] = 0;
  jacobian[23] = 0;
  jacobian[24] = 0;
  jacobian[25] = -x2;
  jacobian[26] = 0;
  jacobian[27] = 0;
  jacobian[28] = 0;
  jacobian[29] = 0;
  jacobian[30] = 1;
  jacobian[31] = 0;
  jacobian[32] = 0;
  jacobian[33] = 0;
  jacobian[34] = 0;
  jacobian[35] = 0;
//
  return;
    jacobian = np.array(jacobian).reshape(6,6)
}



void jacobian02( double* jacobian, const double* q )
{
  double x0 = sin(q[0]);
  double x1 = cos(q[1]);
  double x2 = 0.52700000000000002*x1;
  double x3 = x0*x2;
  double x4 = cos(q[0]);
  double x5 = 0.34599999999999997*sin(q[2]);
  double x6 = x4*x5;
  double x7 = 0.34599999999999997*cos(q[2]);
  double x8 = x0*x7;
  double x9 = x1*x8;
  double x10 = sin(q[1]);
  double x11 = x10*x7 + 0.52700000000000002*x10;
  double x12 = ((x10)*(x10));
  double x13 = x6 + x9;
  double x14 = x4*x7;
  double x15 = -x0*x5 + x1*x14;
  double x16 = x15 + x2*x4;
  double x17 = x0*x10;
  double x18 = x10*x4;
//
  jacobian[0] = -0.059999999999999998*x0 - x3 - x6 - x9;
  jacobian[1] = -x11*x4;
  jacobian[2] = -x1*x13 - x12*x8;
  jacobian[3] = 0;
  jacobian[4] = 0;
  jacobian[5] = 0;
  jacobian[6] = x16 + 0.059999999999999998*x4;
  jacobian[7] = -x0*x11;
  jacobian[8] = x1*x15 + x12*x14;
  jacobian[9] = 0;
  jacobian[10] = 0;
  jacobian[11] = 0;
  jacobian[12] = 0;
  jacobian[13] = x0*(x13 + x3) + x16*x4;
  jacobian[14] = -x13*x18 + x15*x17;
  jacobian[15] = 0;
  jacobian[16] = 0;
  jacobian[17] = 0;
  jacobian[18] = 0;
  jacobian[19] = x0;
  jacobian[20] = -x18;
  jacobian[21] = 0;
  jacobian[22] = 0;
  jacobian[23] = 0;
  jacobian[24] = 0;
  jacobian[25] = -x4;
  jacobian[26] = -x17;
  jacobian[27] = 0;
  jacobian[28] = 0;
  jacobian[29] = 0;
  jacobian[30] = 1;
  jacobian[31] = 0;
  jacobian[32] = x1;
  jacobian[33] = 0;
  jacobian[34] = 0;
  jacobian[35] = 0;
//
  return;
    jacobian = np.array(jacobian).reshape(6,6)
}



void jacobian03( double* jacobian, const double* q )
{
  double x0 = sin(q[0]);
  double x1 = cos(q[1]);
  double x2 = 0.52700000000000002*x1;
  double x3 = x0*x2;
  double x4 = sin(q[2]);
  double x5 = cos(q[0]);
  double x6 = x4*x5;
  double x7 = 0.34599999999999997*x6;
  double x8 = sin(q[1]);
  double x9 = x0*x8;
  double x10 = 0.224*sin(q[3]);
  double x11 = x10*x9;
  double x12 = cos(q[2]);
  double x13 = x0*x12;
  double x14 = x1*x13;
  double x15 = 0.34599999999999997*x14;
  double x16 = 0.224*cos(q[3]);
  double x17 = x16*(x14 + x6);
  double x18 = x12*x8;
  double x19 = x1*x10 + x16*x18;
  double x20 = 0.34599999999999997*x18 + x19;
  double x21 = x20 + 0.52700000000000002*x8;
  double x22 = -x11 + x17;
  double x23 = x15 + x22 + x7;
  double x24 = x12*x5;
  double x25 = x0*x4;
  double x26 = x1*x25 - x24;
  double x27 = x4*x8;
  double x28 = x1*x24;
  double x29 = x5*x8;
  double x30 = -x10*x29 + x16*(-x25 + x28);
  double x31 = -0.34599999999999997*x25 + 0.34599999999999997*x28 + x30;
  double x32 = x2*x5 + x31;
  double x33 = x1*x6 + x13;
//
  jacobian[0] = -0.059999999999999998*x0 + x11 - x15 - x17 - x3 - x7;
  jacobian[1] = -x21*x5;
  jacobian[2] = -x1*x23 - x20*x9;
  jacobian[3] = x19*x26 - x22*x27;
  jacobian[4] = 0;
  jacobian[5] = 0;
  jacobian[6] = x32 + 0.059999999999999998*x5;
  jacobian[7] = -x0*x21;
  jacobian[8] = x1*x31 + x20*x29;
  jacobian[9] = -x19*x33 + x27*x30;
  jacobian[10] = 0;
  jacobian[11] = 0;
  jacobian[12] = 0;
  jacobian[13] = x0*(x23 + x3) + x32*x5;
  jacobian[14] = -x23*x29 + x31*x9;
  jacobian[15] = x22*x33 - x26*x30;
  jacobian[16] = 0;
  jacobian[17] = 0;
  jacobian[18] = 0;
  jacobian[19] = x0;
  jacobian[20] = -x29;
  jacobian[21] = x33;
  jacobian[22] = 0;
  jacobian[23] = 0;
  jacobian[24] = 0;
  jacobian[25] = -x5;
  jacobian[26] = -x9;
  jacobian[27] = x26;
  jacobian[28] = 0;
  jacobian[29] = 0;
  jacobian[30] = 1;
  jacobian[31] = 0;
  jacobian[32] = x1;
  jacobian[33] = x27;
  jacobian[34] = 0;
  jacobian[35] = 0;
//
  return;
    jacobian = np.array(jacobian).reshape(6,6)
}



void jacobian04( double* jacobian, const double* q )
{
  double x0 = sin(q[0]);
  double x1 = cos(q[1]);
  double x2 = 0.52700000000000002*x1;
  double x3 = x0*x2;
  double x4 = sin(q[2]);
  double x5 = cos(q[0]);
  double x6 = x4*x5;
  double x7 = 0.34599999999999997*x6;
  double x8 = sin(q[3]);
  double x9 = sin(q[1]);
  double x10 = x0*x9;
  double x11 = x10*x8;
  double x12 = 0.224*x11;
  double x13 = cos(q[2]);
  double x14 = x0*x13;
  double x15 = x1*x14;
  double x16 = 0.34599999999999997*x15;
  double x17 = cos(q[3]);
  double x18 = x15 + x6;
  double x19 = x17*x18;
  double x20 = 0.224*x19;
  double x21 = x13*x5;
  double x22 = x0*x4;
  double x23 = x1*x22;
  double x24 = 0.27000000000000002*sin(q[4]);
  double x25 = x24*(x21 - x23);
  double x26 = 0.27000000000000002*cos(q[4]);
  double x27 = x26*(-x11 + x19);
  double x28 = x13*x9;
  double x29 = x1*x8;
  double x30 = x17*x28;
  double x31 = x4*x9;
  double x32 = -x24*x31 + x26*(x29 + x30);
  double x33 = 0.224*x29 + 0.224*x30 + x32;
  double x34 = 0.34599999999999997*x28 + x33;
  double x35 = x34 + 0.52700000000000002*x9;
  double x36 = x25 + x27;
  double x37 = -x12 + x20 + x36;
  double x38 = x16 + x37 + x7;
  double x39 = -x21 + x23;
  double x40 = -x10*x17 - x18*x8;
  double x41 = x1*x17 - x28*x8;
  double x42 = x1*x21;
  double x43 = -x22 + x42;
  double x44 = x17*x43;
  double x45 = x5*x9;
  double x46 = x45*x8;
  double x47 = x1*x6;
  double x48 = x24*(-x14 - x47) + x26*(x44 - x46);
  double x49 = 0.224*x44 - 0.224*x46 + x48;
  double x50 = -0.34599999999999997*x22 + 0.34599999999999997*x42 + x49;
  double x51 = x2*x5 + x50;
  double x52 = x14 + x47;
  double x53 = -x17*x45 - x43*x8;
//
  jacobian[0] = -0.059999999999999998*x0 + x12 - x16 - x20 - x25 - x27 - x3 - x7;
  jacobian[1] = -x35*x5;
  jacobian[2] = -x1*x38 - x10*x34;
  jacobian[3] = -x31*x37 + x33*x39;
  jacobian[4] = x32*x40 - x36*x41;
  jacobian[5] = 0;
  jacobian[6] = 0.059999999999999998*x5 + x51;
  jacobian[7] = -x0*x35;
  jacobian[8] = x1*x50 + x34*x45;
  jacobian[9] = x31*x49 - x33*x52;
  jacobian[10] = -x32*x53 + x41*x48;
  jacobian[11] = 0;
  jacobian[12] = 0;
  jacobian[13] = x0*(x3 + x38) + x5*x51;
  jacobian[14] = x10*x50 - x38*x45;
  jacobian[15] = x37*x52 - x39*x49;
  jacobian[16] = x36*x53 - x40*x48;
  jacobian[17] = 0;
  jacobian[18] = 0;
  jacobian[19] = x0;
  jacobian[20] = -x45;
  jacobian[21] = x52;
  jacobian[22] = x53;
  jacobian[23] = 0;
  jacobian[24] = 0;
  jacobian[25] = -x5;
  jacobian[26] = -x10;
  jacobian[27] = x39;
  jacobian[28] = x40;
  jacobian[29] = 0;
  jacobian[30] = 1;
  jacobian[31] = 0;
  jacobian[32] = x1;
  jacobian[33] = x31;
  jacobian[34] = x41;
  jacobian[35] = 0;
//
  return;
    jacobian = np.array(jacobian).reshape(6,6)
}



void jacobian05( double* jacobian, const double* q )
{
  double x0 = sin(q[0]);
  double x1 = cos(q[1]);
  double x2 = 0.52700000000000002*x1;
  double x3 = x0*x2;
  double x4 = sin(q[2]);
  double x5 = cos(q[0]);
  double x6 = x4*x5;
  double x7 = 0.34599999999999997*x6;
  double x8 = sin(q[3]);
  double x9 = sin(q[1]);
  double x10 = x0*x9;
  double x11 = x10*x8;
  double x12 = 0.224*x11;
  double x13 = cos(q[2]);
  double x14 = x0*x13;
  double x15 = x1*x14;
  double x16 = 0.34599999999999997*x15;
  double x17 = cos(q[3]);
  double x18 = x15 + x6;
  double x19 = x17*x18;
  double x20 = 0.224*x19;
  double x21 = sin(q[4]);
  double x22 = x13*x5;
  double x23 = x0*x4;
  double x24 = x1*x23;
  double x25 = x22 - x24;
  double x26 = x21*x25;
  double x27 = 0.27000000000000002*x26;
  double x28 = cos(q[4]);
  double x29 = -x11 + x19;
  double x30 = x28*x29;
  double x31 = 0.27000000000000002*x30;
  double x32 = -x10*x17 - x18*x8;
  double x33 = 0.029999999999999999*sin(q[5]);
  double x34 = x32*x33;
  double x35 = 0.029999999999999999*cos(q[5]);
  double x36 = x35*(x26 + x30);
  double x37 = x13*x9;
  double x38 = x1*x8;
  double x39 = x17*x37;
  double x40 = x38 + x39;
  double x41 = x28*x40;
  double x42 = x4*x9;
  double x43 = x21*x42;
  double x44 = x1*x17 - x37*x8;
  double x45 = x33*x44 + x35*(x41 - x43);
  double x46 = 0.27000000000000002*x41 - 0.27000000000000002*x43 + x45;
  double x47 = 0.224*x38 + 0.224*x39 + x46;
  double x48 = 0.34599999999999997*x37 + x47;
  double x49 = x48 + 0.52700000000000002*x9;
  double x50 = x34 + x36;
  double x51 = x27 + x31 + x50;
  double x52 = -x12 + x20 + x51;
  double x53 = x16 + x52 + x7;
  double x54 = -x22 + x24;
  double x55 = x21*x29 - x25*x28;
  double x56 = x21*x40 + x28*x42;
  double x57 = x1*x22;
  double x58 = -x23 + x57;
  double x59 = x17*x58;
  double x60 = x5*x9;
  double x61 = x60*x8;
  double x62 = x59 - x61;
  double x63 = x28*x62;
  double x64 = x1*x6;
  double x65 = -x14 - x64;
  double x66 = x21*x65;
  double x67 = -x17*x60 - x58*x8;
  double x68 = x33*x67 + x35*(x63 + x66);
  double x69 = 0.27000000000000002*x63 + 0.27000000000000002*x66 + x68;
  double x70 = 0.224*x59 - 0.224*x61 + x69;
  double x71 = -0.34599999999999997*x23 + 0.34599999999999997*x57 + x70;
  double x72 = x2*x5 + x71;
  double x73 = x14 + x64;
  double x74 = x21*x62 - x28*x65;
//
  jacobian[0] = -0.059999999999999998*x0 + x12 - x16 - x20 - x27 - x3 - x31 - x34 - x36 - x7;
  jacobian[1] = -x49*x5;
  jacobian[2] = -x1*x53 - x10*x48;
  jacobian[3] = -x42*x52 + x47*x54;
  jacobian[4] = x32*x46 - x44*x51;
  jacobian[5] = x45*x55 - x50*x56;
  jacobian[6] = 0.059999999999999998*x5 + x72;
  jacobian[7] = -x0*x49;
  jacobian[8] = x1*x71 + x48*x60;
  jacobian[9] = x42*x70 - x47*x73;
  jacobian[10] = x44*x69 - x46*x67;
  jacobian[11] = -x45*x74 + x56*x68;
  jacobian[12] = 0;
  jacobian[13] = x0*(x3 + x53) + x5*x72;
  jacobian[14] = x10*x71 - x53*x60;
  jacobian[15] = x52*x73 - x54*x70;
  jacobian[16] = -x32*x69 + x51*x67;
  jacobian[17] = x50*x74 - x55*x68;
  jacobian[18] = 0;
  jacobian[19] = x0;
  jacobian[20] = -x60;
  jacobian[21] = x73;
  jacobian[22] = x67;
  jacobian[23] = x74;
  jacobian[24] = 0;
  jacobian[25] = -x5;
  jacobian[26] = -x10;
  jacobian[27] = x54;
  jacobian[28] = x32;
  jacobian[29] = x55;
  jacobian[30] = 1;
  jacobian[31] = 0;
  jacobian[32] = x1;
  jacobian[33] = x42;
  jacobian[34] = x44;
  jacobian[35] = x56;
//
  return;
    jacobian = np.array(jacobian).reshape(6,6)
}



J = {0:jacobian00, 1:jacobian01, 2:jacobian02, 3:jacobian03, 4:jacobian04, 5:jacobian05, }
