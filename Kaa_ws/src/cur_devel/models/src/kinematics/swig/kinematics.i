%module kinematics

%{
  #define SWIG_FILE_WITH_INIT

  // #include "yaml-cpp/yaml.h"

  #include "kinematics/types.h"
  #include "kinematics/partials.h"
  #include "kinematics/joint_limits.h"
  #include "kinematics/joint_base.h"
  #include "kinematics/joint_ccc.h"
  #include "kinematics/joint_cc1d.h"
  #include "kinematics/joint_rot.h"
  #include "kinematics/link.h"
  #include "kinematics/sdls.h"
  #include "kinematics/arm.h"
  #include "kinematics/quadruped.h"
  #include "kinematics/arm_ik.h"
  #include "kinematics/yaml.h"
  #include "kinematics/gyro_bias_estimator.h"
  #include "kinematics/gravity_solver.h"
  #include "primitives/smartenums.h"
%}

%include <typemaps.i>
%include <std_vector.i>
%include <eigen.i>
%include <std_string.i>
%include <std_unique_ptr.i>

%init %{
  import_array();  // required for Numpy
%}

// typechecks needed for overloaded functions
%typecheck(SWIG_TYPECHECK_POINTER) Eigen::Matrix3d  { $1 = is_array($input) || PySequence_Check($input); }
%typecheck(SWIG_TYPECHECK_POINTER) Eigen::Vector3d  { $1 = is_array($input) || PySequence_Check($input); }
%typecheck(SWIG_TYPECHECK_POINTER) Eigen::Vector4d  { $1 = is_array($input) || PySequence_Check($input); }
%typecheck(SWIG_TYPECHECK_POINTER) kinematics::Vec  { $1 = is_array($input) || PySequence_Check($input); }
%typecheck(SWIG_TYPECHECK_POINTER) kinematics::Mat  { $1 = is_array($input) || PySequence_Check($input); }
%typecheck(SWIG_TYPECHECK_POINTER) kinematics::Hom  { $1 = is_array($input) || PySequence_Check($input); }
%typecheck(SWIG_TYPECHECK_POINTER) kinematics::Adj  { $1 = is_array($input) || PySequence_Check($input); }
%typecheck(SWIG_TYPECHECK_POINTER) kinematics::Jac  { $1 = is_array($input) || PySequence_Check($input); }
%typecheck(SWIG_TYPECHECK_POINTER) kinematics::Vec6  { $1 = is_array($input) || PySequence_Check($input); }

// typemaps have to come before %include "types.h"
// can't typemat two typedefs
%eigen_typemaps(Eigen::Vector3d)  // works for Vec3, too
%eigen_typemaps(Eigen::Vector4d)  // works for Vec4, too
%eigen_typemaps(kinematics::Vec6) // works for twist, too
%eigen_typemaps(Eigen::Matrix3d)  // works for Mat, InertiaTensor
%eigen_typemaps(Eigen::Matrix4d)  // works for Hom

%eigen_typemaps(kinematics::Vec)
%eigen_typemaps(kinematics::Mat)
%eigen_typemaps(kinematics::Adj)
%eigen_typemaps(kinematics::Jac)
// note: typedef of diagonal matrix doesn't work

// needs a __deepcopy__ method for python
// memodict unused, but seems to work
%extend kinematics::Arm {
  kinematics::Arm* kinematics::Arm::__deepcopy__(PyObject* /*memodict*/) {
    kinematics::Arm* copy = new kinematics::Arm(*$self);  
    // oddly, above line works without $ as well
    return copy;
  }
}

%warnfilter(362) operator=;  // warnings from JointPtr

wrap_unique_ptr(JointValuePtr, kinematics::JointBase);

// need a couple of things from: /yaml-cpp/node/node.h
%rename(YAMLLoadFile) YAML::LoadFile(const std::string&);  // because we lose the YAML namespace
%rename(YAMLLoad) YAML::Load(const std::string&);  // because we lose the YAML namespace

namespace YAML {
  template<typename T>
  struct convert;

  YAML::Node LoadFile(const std::string&);
}

// these have to come after typemaps and/or typechecks
// the order matters, too
%include "primitives/smartenums.h"
%include "kinematics/types.h"
%include "kinematics/partials.h"
%include "kinematics/joint_limits.h"
%include "kinematics/joint_base.h"
%include "kinematics/joint_ccc.h"
%include "kinematics/joint_cc1d.h"
%include "kinematics/joint_rot.h"
%include "kinematics/link.h"
%include "kinematics/arm.h"
%include "kinematics/quadruped.h"
%include "kinematics/sdls.h"
%include "kinematics/arm_ik.h"
%include "kinematics/yaml.h"
%include "kinematics/gyro_bias_estimator.h"
%include "kinematics/gravity_solver.h"

// the functions in C++ don't work in Python, these do
%inline %{
  kinematics::JointCCC* CastToJointCCC(kinematics::JointBase* base) {
    return dynamic_cast<kinematics::JointCCC*>(base);
  }
  kinematics::JointCC1D* CastToJointCC1D(kinematics::JointBase* base) {
    return dynamic_cast<kinematics::JointCC1D*>(base);
  }
  kinematics::JointRot* CastToJointRot(kinematics::JointBase* base) {
    return dynamic_cast<kinematics::JointRot*>(base);
  }
%}

/// Template instantiations ///

// vectors
%template(LinkVector) std::vector<kinematics::Link>;
%template(JointPartialsVector) std::vector<kinematics::JointPartials>;
%template(LinkPartialsVector) std::vector<kinematics::LinkPartials>;
%template(ArmPartialsVector) std::vector<kinematics::ArmPartials>;

// converting from file names
%template(YAMLArmFromFilename) kinematics::YAMLConvertFromFilename<kinematics::Arm>;
%template(YAMLLinkFromFilename) kinematics::YAMLConvertFromFilename<kinematics::Link>;
%template(YAMLJointCCCFromFilename) kinematics::YAMLConvertFromFilename<kinematics::JointCCC>;
%template(YAMLJointCC1DFromFilename) kinematics::YAMLConvertFromFilename<kinematics::JointCC1D>;
%template(YAMLJointRotFromFilename) kinematics::YAMLConvertFromFilename<kinematics::JointRot>;

// converting from strings
%template(YAMLArmFromString) kinematics::YAMLConvertFromFilestring<kinematics::Arm>;

// conversion methods
// generates convert.decode and convert_decode which do the same thing
%template(YAMLConvertArm) YAML::convert<kinematics::Arm>;
%template(YAMLConvertLink) YAML::convert<kinematics::Link>;
%template(YAMLConvertJointCCC) YAML::convert<kinematics::JointCCC>;
%template(YAMLConvertJointCC1D) YAML::convert<kinematics::JointCC1D>;
%template(YAMLConvertJointRot) YAML::convert<kinematics::JointRot>;
