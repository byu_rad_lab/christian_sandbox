#!/usr/bin/env python
from kinematics import arm_tools as at
from kinematics import kinematics as kin
import numpy as np
from tools import kin_tools as kt

if __name__ == '__main__':

    base_hom = kt.ax_vec2homog(np.array([0.0, -np.pi/2.0, 0]), np.array([0, 0, 0]))
    l = 0.1
    leg = at.simple_leg(0.0,[l],base_hom)
    mass = 2.0
    g = 9.81
    leg.calcMass()
    weight_wrench = np.array([[0.0], [-1.0],[(mass/2.0 + 2.0*leg.getMass())*g],[0.0],[0.0],[0.0]])

    jangles = np.array([15.0*np.pi/180.0])
    leg.setJointAngs(jangles)
    leg.calcJac()
    Jac = leg.getJacHybrid()
    print("jac: ", Jac)
    leg.calcTorqueGravity();
    torques_tot = -1.0*leg.getTorqueGravity() + np.dot(Jac.T, weight_wrench)
    print("torques_tot: ", torques_tot)
    required_friction_help = leg.calcFrictionForceLeg(torques_tot,weight_wrench)
    print("required_friction_help: ", required_friction_help)
    print("G_world_end: ", leg.g_world_tool())

    N = -weight_wrench[2]
    theta = jangles[0]
    tau_z = torques_tot[0]
    w_link = 5.0*g
    w_valve_block = 2.5*g
    calc_friction = (N*l*np.sin(theta) + w_link*l/2.0*np.sin(theta) + w_valve_block*(l-0.25/2.0)*np.sin(theta) - tau_z + weight_wrench[1]*l*np.cos(theta))/(l*np.cos(theta))
    print("friction: ", calc_friction)
    print(leg.getMass())

    print(leg.areTorquesPossible(np.array([-300.001])))
