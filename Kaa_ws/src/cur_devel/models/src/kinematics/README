Kinematics and dynamics of a general serial chain manipulator.

### Notation:  ###
Base on Murray, Li, Sastry
rot is a 3x3 rotation matrix, denoted R
vec3 is a 3x1 vector, denoted p
hom is a 4x4 homogenous transform, denoted g = [R, p; 0, 0, 0, 1]
ad is a 6x6 adjoint, denoted ad = [R, p_hat*R; R, 0]
inv means inverse
jac is a 6xn jacobian
Spatial and body jacobians are used - see MLS.


### Code structure ###
Data types are in types.h/.cpp, as well as some helper functions for those types.
  Pretty much everything is preresented as an Eigen matrix of doubles, fixed or dynamically sized.
  mat_t represents general matrices
  vec_t represents general vectors (and is a column vector)
  rot_t represents rotation matrices (3x3)
  hom_t represents homogeneous transforms (4x4)
  adj_t represents adjoints (6x6)

Arm (arm.h/.cpp) is composed of a std::vector of Link instances

Each Link (link.h/.cpp) has a Joint

Joint (joint.h/.cpp) represents different joint models with a different number of DOF.
Joint models various types: revolute, constant curvature continuum actuator.
More models may be added fiarly easily.
Each needs a calcGBotTop and calcJac method.  A calcDerivJac method is optional 
and a numeric method will be used if not provided.

Partials (partials.h) holds classes representing partial derivatives of data for Arm, Link, and Joint.
The position of the partial in the vector represents angle whose partial it is.
Example:
arm.partials_[i].jac_spatial_ is the ith partial derivative of arm.jac_spatial_


### Joint Model ###
The constant curvature continuum actuator (CCC) has two degrees of freedom,
denoted u, v.  The forward kineamtics are based on a rotation vector,
denote w, lying in the x-y plane, where w = [u, v, 0].
The rotation of the top plate relative to the bottom is given by a rotation
of phi = ||w|| around normalized w (w / ||w||).
Translation is determined by assuming that the cable running down the middle
remains constant length.


### Frame definitions: ###
Each transform is of the form g_<from>_<to>
world - self explanatory
arm - the base of the robot
bot - proximal base of link, before joint motion
com - center of mass
end - distal part of a link
  same as bot of next link

For the constant curvature continuum actuator (CCC):
bot - distal surface of the proximal plate, z up, x and y passing through bellows centers
top - proximal surface of the distal plate


