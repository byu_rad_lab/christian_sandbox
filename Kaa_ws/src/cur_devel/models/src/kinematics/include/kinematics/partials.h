// Otherlab, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_PARTIALS_H_
#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_PARTIALS_H_

#include "kinematics/types.h"

namespace kinematics {
// probably these should be structs
// Each is container for partial derivatives with respect to a single variable.
// Arm, Link, or Joint has a vector of these where the ith element is the
//   partial derivative with respect to the ith joint angle

class ArmPartials {
 public:
  int num_dof_;
  Jac jac_spatial_;
  Mat inertia_mat_;

  void resize(int num_dof);
  void checkMatSizes(void);
};

class LinkPartials {
 public:
  int num_dof_;
  Hom g_world_bot_;
  Hom g_world_com_;
  Adj ad_world_bot_;
  Adj ad_inv_world_com_;
  Jac jac_body_;

  void resize(int num_dof);
  void checkMatSizes(void);
};

class JointPartials {
 public:
  int num_dof_;
  Jac jac_;

  void resize(int num_dof);
  void checkMatSizes(void);
};

}  // namespace kinematics

#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_PARTIALS_H_
