// Otherlab, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_ARM_H_
#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_ARM_H_

#include <math.h>
#include <vector>

#include "kinematics/types.h"
#include "kinematics/sdls.h"
#include "kinematics/joint_base.h"
#include "kinematics/link.h"
#include "kinematics/partials.h"

// Manipulator dynamics per Murray, Li, Sastry 1994
// Dynamics equation is:
//   M * th_dot_dot + C * theta_dot = torque_net
// where:
//   th is the joint parameters
//   M is inertia_mat_
//   C is coriolis_mat_
//   torque_net = torque_joint_ + torque_gravity_ + torque_external_

namespace kinematics {

enum type {
    NASA,
    JABBERWOCK,
    LEG,
    QUADRUPED};

class Arm {
 public:
  Arm();
  Arm(double height, Vec lengths);
  Arm(double height, Vec lengths, JointType type);

  ~Arm();

  void resize(void);
  void addLink(const Link& link);

  // getters
  inline double getGravity(void) { return gravity_; }
  inline int getNumDof(void) const { return num_dof_; }
  inline Vec getJointAngs(void) const { return joint_angs_; }
  inline Vec getJointVels(void) const { return joint_vels_; }
  inline Vec getJointAccs(void) const { return joint_accs_; }
  Vec getJointAngsReal();

  inline Vec getTorqueGravity(void) const { return torque_gravity_; }
  inline Vec getTorqueGravityLeg(void) const { return torque_gravity_leg_; }
  inline Vec getTorqueExternal(void) const { return torque_external_; }
  inline Vec getTorqueLimitLower(void) const { return torque_limit_lower_; }
  inline Vec getTorqueLimitUpper(void) const { return torque_limit_upper_; }
  inline Vec getRealJointIdx(void) const { return real_joint_idx_; }
  inline Mat getTorquePassive(void) const { return torque_sum_passive_; }

  inline Jac getJacSpatial(void) const { return jac_spatial_; } //This is the Spatial Jacobian.  It maps the joint velocities to the cartesian velocities of the base of the arm relative to the end effector in the world frame (It is the "Inverse" Hybrid Jacobian expressed but in the world frame)
  inline Jac getJacBody(void) const { return jac_body_; } //This is the Body Jacobian.  It maps the joint velocities to the cartesian velocities of the end effector in the end effector frame.
  inline Jac getJacBodyReal(void) const { return jac_body_real_; }
  Jac getJacBodyRealLink(int link_num);
  inline Jac getJacHybrid(void) const { return jac_hybrid_; } //This is the Hybrid Jacobian.  It maps the joint velocites to the cartesian velocities of the end effector in the world frame
  inline Jac getJacHybridInverse(void) const { return jac_hybrid_inverse_; } //This is the "Inverse" Hybrid Jacobian.  It maps the joint velocities to the cartesian velocities of the base of the arm in the end effector frame. (Think if you made a new robot where the end effector of the old robot was its base and the base of the old robot was its end effector.  The "Inverse" Hybrid Jacobian is the Hybrid Jacobian of this new robot.)

  inline Jac getJacSpatialReal(void) const { return jac_spatial_real_; }
  inline Jac getJacHybridReal(void) const { return jac_hybrid_real_; }

  inline Mat getInertiaMat(void) const { return inertia_mat_; }
  inline double getMass(void) const { return mass_; }
  // inline Mat getStiffnessHybrid(void) const { return stiffness_hybrid_; }
  // inline Mat getStiffnessBody(void) const { return stiffness_body_; }
  inline Mat getFlexibilityHybrid(void) const { return flexibility_hybrid_; }
  inline Mat getFlexibilityGlobal(void) const { return flexibility_global_; }
  inline Mat getFlexibilityBody(void) const { return flexibility_body_; }

  inline Link* link(int link_num) { return &links_[link_num]; }
  std::vector<Link>* links(void) { return &links_; }

  inline Vec joint_angs(void) { return joint_angs_; }
  inline Vec joint_vels(void) { return joint_vels_; }
  inline int num_dof(void) { return num_dof_; }
  inline Hom g_world_tool(void) { return g_world_tool_; }
  inline Hom g_world_arm(void) { return g_world_arm_; }
  inline Hom g_end_tool(void) { return g_end_tool_; }
  inline Mat inertia_mat(void) { return inertia_mat_; }

  inline void set_g_end_tool(Hom hom) { g_end_tool_ = hom; }

  Hom getGWorldTool(void) const; //This is the tool frame expressed in the world frame
  Hom getGArmTool(void) const; //This is the tool frame expressed in the robot first link frame

  // setters

  inline void setGravity(double g) { gravity_ = g; }
  void setGEndToolVec(double x, double y, double z);
  void setGWorldArm(const Hom& hom);

  void setJointAngs(Vec angs);
  void setJointAngsReal(Vec angs);
  void setJointAngsZero(void);
  void setJointAngsDelta(Vec angs);
  void setJointAngsRandom(void);
  void resetJointAngsWithinLimit(void);

  void setJointVels(Vec vels);
  void setJointVelsZero(void);
  void setJointVelsRandom(void);

  void setJointStateZero(void);
  void setJointStateRandom(void);

  void calcStiffness();
  void setRealJointIDX(const Vec& idx);
  void calcJacReal(void);
  // void setJacReal(void);

  // void setStiffnessHybrid(Mat stiffness);
  // void setStiffnessBody(Mat stiffness);
  void setFlexibilityHybrid(Mat flexibility);
  void setFlexibilityBody(Mat flexibility);

  // useful calculations
  void calcFK(void);
  void calcJac(void);  // calculates spatial jacobian, link body jacobian
  void calcInertiaMat(void);
  void calcDynamicsMats(void);
  void calcMass(void);

  Vec calcJointAccels(const Vec& torque_joint);
  Vec calcTorqueJoint(const Vec& joint_accels);
  Vec calcTorqueJointStatic(const Vec6& external_wrench);

  void calcTorqueLimits(void);

  // Returns torque due to inertial forces (M * theta_dot_dot)
  // make sure that calcDynamicsMats has been called
  Vec getTorqueInertial(const Vec& joint_accels);

  // Returns torque due to Corilois forces
  // make sure that joint_vels_ is set and calcDynamicsMats has been called
  Vec getTorqueCoriolis(void);

  Vec3 calcForcesFromTorques(const Vec& torque_joint_net);

  void calcTorqueGravity(void);
  void calcTorqueGravityLeg(void); //This function calculates the torques due to gravity as if the end of the arm is fixed and it is lifting up a weight at its base, simulating a leg that is positioned on the ground.  This is assumign there is no slipping due to friction (will need to add a test for this later)
  Jac findJacfromHomProximalLinks(Hom hom, int i); //used in calcTorqueGravityLeg()
  Vec3 calcFrictionForceLeg(Vec torques, Vec6 applied_wrench); //finds the friction force required to keep the leg from slipping with the given torques and body mass.  Assumes z is down
  bool areTorquesPossible(Vec torques);

  double calcMaxStaticLiftForce(const double& delta_max);
  double calcMaxForceLimitTorque(const Vec3& direction);
  double calcMaxForceLimitDisplacement(const Vec3&, const double&);

  double calcUNorm(const Vec6& x_des);
  void calcJointTorqueLimits(void);

  // some functions to include torques caused by external wrenches
  // set methods set torque to zero, then apply wrench
  // add methods sum with existing torque
  // remember to call setTorqueExternalZero() when you're done
  // These are set in the LINK frame, which could be confusing
  void addTorqueExternal(int link_num,
                         const Frame& frame,
                         const Vec6& wrench,
                         const Hom& offset);
  void setTorqueExternal(int link_num,
                         const Frame& frame,
                         const Vec6& wrench,
                         const Hom& offset);

  // helper methods to apply in end effector frame
  // void addForceAtEE(const Frame& frame, const Vec3& force);
  // void setForceAtEE(const Frame& frame, const Vec3& force);

  inline void setTorqueExternalZero(void) { torque_external_.setZero(); }

  // verify that dynamically size Mat and Vec are the right size
  void checkMatSizes(void);


 protected:
  // sets state variables and various matrices to zero
  // necessary as zero values are not always calculated
  // called by resize
  void setDataZero(void);

  void calcDerivHom(void);
  void calcDerivJacSpatial(void);
  void calcDerivJacBody(void);
  void calcDerivInertiaMat(void);
  void calcCoriolisMat(void);
  void calcJacBody(void);    // called by calcJac
  void calcJacHybrid(void);  // called by calcJac
  void calcJacHybridInverse(void);  // called by calcJac

 public:
  /// Data ///
  //  These should be private, use getters instead.
  int num_dof_;

  Vec joint_angs_;
  Vec joint_vels_;
  Vec joint_accs_;

  std::vector<double> max_torques_;
  std::vector<double> min_torques_;

  Jac jac_spatial_;
  Jac jac_spatial_real_;

  Jac jac_body_real_;
  Jac jac_hybrid_real_;

  Mat inertia_mat_;  // M in dynamics equation
  Mat coriolis_mat_;

  Hom g_world_arm_;
  std::vector<Link> links_;
  std::vector<ArmPartials> partials_;

  type type_; // which type of arm it is (useful to know which optimization to run on it in arm_eval_fk.cpp)

  double mew_ = 0.3; // static coefficient of friction for end of arm.

 protected:
  double gravity_ = -9.81;  // make const/static?  could be useful source of uncertainty
  double mass_;

  Vec real_joint_idx_;
  // Mat stiffness_body_;
  // Mat stiffness_hybrid_;
  Mat flexibility_global_;
  Mat flexibility_body_;
  Mat flexibility_hybrid_;

  Jac jac_body_;    // world to EE
  Jac jac_hybrid_;  // world to EE
  Jac jac_hybrid_inverse_; // EE to world
  Vec torque_gravity_;
  Vec torque_gravity_leg_; //torques felt from gravity if assuming end effector is fixed and the base is free to move
  Vec torque_external_;  // from external wrenches other than gravity
  Vec torque_sum_;
  Vec torque_limit_lower_;  // max possible torque felt by joint in -x
  Vec torque_limit_upper_;  // max possible torque felt by joint in x
  Vec force_limit_;  // max force allowable in specified direction by joint (min taken)
  Vec torque_sum_passive_;

  Hom g_end_tool_ = Hom::Identity();

  Hom g_world_tool_;
  Adj ad_inv_world_tool_ = Adj::Identity();
  Adj ad_hybrid_ = Adj::Identity();
  Adj ad_inverse_ = Adj::Identity(); //adjunct used to find the Jacobian from the end effector to the world frame

};

}  // namespace kinematics

#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_ARM_H_
