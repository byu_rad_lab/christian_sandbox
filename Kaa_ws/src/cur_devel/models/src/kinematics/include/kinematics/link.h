// Otherlab, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_LINK_H_
#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_LINK_H_

#include <vector>

#include "kinematics/types.h"
#include "kinematics/joint_base.h"
#include "kinematics/partials.h"

namespace kinematics {

class Link {
 public:
  Link(void);
  Link(const JointBase& joint);
  Link(const JointBase& joint, int num_dof_prox);
  Link(JointType joint_type);

  inline Hom g_world_bot(void) const { return g_world_bot_; }
  inline Hom g_world_top(void) const { return g_world_top_; }
  inline Hom g_world_end(void) const { return g_world_end_; }
  inline Hom g_top_com(void) const { return g_top_com_; }
  inline Hom g_top_end(void) const { return g_top_end_; }
  inline Jac jac_body(void) const { return jac_body_; }
  inline int num_dof_total(void) const { return num_dof_total_; }
  inline LinkPartials* partials(int i) { return &partials_[i]; }

  JointBase* joint(void) { return joint_.get(); }

  inline Mat inertia_mat_full(void) {
    Mat inertia_mat_full = inertia_mat_;
    return inertia_mat_full;
  }

  // combines inertia_self and inertia_added, creates diagonal inertia matrix
  void InitInertia(void);  // inertia_self must be set first
  void SetInertiaDiag(double m, double Ix, double Iy, double Iz);  // don't use this

  void setGTopEnd(const Hom& hom);
  void setGTopEndXYZ(double x, double y, double z);

  // depricated
  inline void setGTopEnd(double x, double y, double z) { setGTopEndXYZ(x, y, z); }

  void setInertia(Inertia* inert, double mass, Vec3 pos, Vec3 inert_diag);
  inline void setInertiaSelf(double mass, Vec3 pos, Vec3 inert_diag) {
    setInertia(&inertia_self_, mass, pos, inert_diag);
  }
  inline void setInertiaAdded(double mass, Vec3 pos, Vec3 inert_diag) {
    setHasInertiaAdded(true);
    setInertia(&inertia_added_, mass, pos, inert_diag);
  }
  inline void setHasInertiaAdded(bool has_inertia_added) {
    has_inertia_added_ = has_inertia_added;
    InitInertia();
  }

  // helper method to set inertia_added_ as a cylinder whose axis lies along joint z
  void setInertiaAddedCylinder(
      double mass, double z_start, double thickness, double OD, double ID);

  void resize(int num_dof_prox);
  void setDefaults(void);
  // void initialize(int num_dof_prox);

  void calcFK(void);
  void calcJac(Jac* jac_spatial);
  void augmentInertiaMat(Mat* inertia_mat);
  void calcDeriveHom(const Jac& jac_spatial);
  void calcDerivJacSpatial(std::vector<ArmPartials>* arm_partials);
  void calcDerivJacBody(const Jac& jac_spatial,
                        const std::vector<ArmPartials>& arm_partials);
  void calcDerivInertiaMat(std::vector<ArmPartials>* arm_partials);
  void calcAdjoints(void);
  void calcDerivAdjoint(void);

  void checkMatSizes(void);

  /// Data ///
  JointPtr joint_;

  // should be set when added to Arm
  // int link_num_;
  int num_dof_prox_;   // number for proximal DOF
  int num_dof_total_;  // for jacobian, partial deriv sizes

  Hom g_world_bot_;  // set elsewhere

  // properties of this link
  Hom g_top_end_;

  bool has_inertia_added_;
  Inertia inertia_self_;
  Inertia inertia_added_;

  // calculated from Inertias above
  Mat6Diag inertia_mat_;  // inertia tensor

  // calculated by this link
  Hom g_world_top_;
  Hom g_world_com_;
  Hom g_world_end_;  // same as link[k+1].g_world_bot_

  Hom g_top_com_;  // calculated from inertia elements

  Adj ad_world_bot_;
  Adj ad_inv_world_com_;
  Adj ad_hybrid_;

  // calculated here any by Arm
  Jac jac_body_;    // 6 x num_dof_total_
  Jac jac_hybrid_;  // 6 x num_dof_total_

  std::vector<LinkPartials> partials_;  // one per dof
};

// Given angular rate (i.e. gyro) measurements above and below a joint,
//   estimate the joint velocities using damped pseudo-inverse.
//   damping_factor = 0.020 is a good number for CCC joint
Vec GyroSolve(const Link& link,
              const Vec3& gyro_top,
              const Vec3& gyro_bot,
              double damping_factor);

}  // namespace kinematics

#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_LINK_H_
