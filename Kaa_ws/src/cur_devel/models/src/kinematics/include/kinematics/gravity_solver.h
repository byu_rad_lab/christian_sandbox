/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_GRAVITY_SOLVER_H_
#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_GRAVITY_SOLVER_H_

#include <nlopt.hpp>

#include <kinematics/joint_base.h>
#include <vector>
#include <memory>

#include "kinematics/types.h"

namespace kinematics {

class GravitySolver {
 public:
  explicit GravitySolver(const JointPtr joint_ptr);
  explicit GravitySolver(const JointBase& joint);

  void solve(const Vec3& a0, const Vec3& a1, Vec* ang);
  Vec solve2(const Vec3& a0, const Vec3& a1, const Vec& guess);

  void updateBounds(void);

 protected:
  static double costCaller(const std::vector<double>& x,
                           std::vector<double>& grad,  // NOLINT(runtime/references)
                           void* f_data);

  double costFunc(const std::vector<double>& x,
                  std::vector<double>& grad,  // NOLINT(runtime/references)
                  void* f_data);

 public:
  JointPtr joint_;

 protected:
  nlopt::opt opt_;

  Vec3 a0_;
  Vec3 a1_;

  unsigned int feval_;
  std::vector<double> x_;

 private:
  // temporary variable
  Rot r_01_;
  Vec3 a1_calc_;
  Vec3 err_;
};

}  // namespace kinematics

#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_GRAVITY_SOLVER_H_
