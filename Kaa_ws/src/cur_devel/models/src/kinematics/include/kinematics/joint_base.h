// Otherlab, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_JOINT_BASE_H_
#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_JOINT_BASE_H_

// This is a base class for joints.
// Each joint must:
// - Inherit from JointBase
// - Have a clone method
// - Have a calcFK method
// - Have a calcJac method (should call calcFK)
// - The constructor must set num_dof_ and call initialize
// In addition:
// - Add the joint type KINEMATICS_JOINT_TYPE_LIST below.
// - A calcDerivJac method is optional, but should call calcJac

#include <vector>
#include <memory>
#include "primitives/smartenums.h"

#include "kinematics/types.h"
#include "kinematics/partials.h"
#include "kinematics/joint_limits.h"

namespace kinematics {

// a macro for joint type enum names
// creates JointType enum with elements kJointTypeCCC etc.
// also create kNumJointTypes as last element
// TODO:  Probably don't need this at all
#define KINEMATICS_JOINT_TYPE_LIST(X) \
  X(JointType, CCC)                   \
  X(JointType, CC1D)                  \
  X(JointType, Rot)

SMARTENUM_CREATE_INCLUDE(JointType, KINEMATICS_JOINT_TYPE_LIST)
// end macro, the rest is in the .cpp file

// class JointBase;
// typedef value_ptr<JointBase> JointPtr; // a unique_ptr to JointBase, that copies
// itself

class JointBase {
 public:
  virtual JointBase* clone() const = 0;
  virtual ~JointBase(){};  // very important

  // sets matrices to the right size, sets most to zero
  // void initialize(void);
  void setDefaults(void);
  void resize(void);

  inline Vec getAngs(void) const { return angs_; }
  inline Vec getVels(void) const { return vels_; }
  inline Hom getGBotTop(void) const { return g_bot_top_; }
  inline Jac getJac(void) const { return jac_; }

  inline Jac jac(void) const { return jac_; }
  inline Vec angs(void) const { return angs_; }
  inline Vec vels(void) const { return vels_; }
  inline double num_dof(void) const { return num_dof_; }
  inline Hom g_bot_top(void) const { return g_bot_top_; }

  JointLimits* limits(void);

  inline void setAngs(Vec angs) { angs_ = angs; }
  inline void setAngsDelta(Vec angs) { angs_ += angs; }
  inline void setVels(Vec vels) { vels_ = vels; }

  void setAngSingle(int index, double ang);

  void setAngsZero(void);
  void setVelsZero(void);
  void setStateZero(void);

  virtual void setAngsRandom(void);
  virtual void setVelsRandom(void);

  // this should be replaced with calls to limits
  virtual void resetAngsWithinLimit(void);

  virtual void calcFK(void) = 0;    // caculates g_bot_top_
  virtual void calcJac(void) = 0;   // calculates jacobian
  virtual void calcDerivJac(void);  // falls back to numerics
  virtual void calcTorqueLimits(void);
  void calcDerivJacNumeric(void);   // fallback method

  inline virtual Mat getJointStiffnessMatrix(void) const { return joint_stiffness_; }
  virtual void setJointStiffnessScalar(double k);
  virtual void setJointStiffnessVector(Vec vec);
  virtual void setJointStiffnessMatrix(Mat mat);

  void checkMatSizes(void);

  // These should be private.  Use getters/setters.

  // common fields
  Vec angs_;
  Vec vels_;

  // is this needed?  probably remove
  JointType type_;  // make const?

  // add NumDof(void) method
  int num_dof_;

  // for linear models (default)
  Vec spring_constant_;
  Vec damping_constant_;

  // needed for gravity solver
  // needed to set joint angles randomly
  // should be replaced with linear form A x <= b
  // probably need to be able to extract these from JointLimits class
  Vec limits_upper_;
  Vec limits_lower_;

  // calculated by joint
  Hom g_bot_top_;
  Hom g_top_com_; //used for center of mass of joint for weight calculations (mainly for continuum joints)
  Hom g_top_com_prev_; //used so the link can recalculate its g_top_com_
  Jac jac_;  // local spatial jacobian

  std::vector<JointPartials> partials_;  // one per dof

  Vec torque_max_; //estimated max torque output from joint model
  Vec torque_min_; //estimated min torque output from joint model

  double mass_ = 0.0; //initialize at 0

 private:
  JointLimits limits_;
  Mat joint_stiffness_;
};

// This is a pointer which owns its data
// Like std::unique_ptr but when copies creates a copy of JointBase
// Allows derived joint classes to live in Link
// TODO: should have been implemented as having a std::unique_ptr<JointBase>
//   probably should be implemented as CopyPtr templatized on what it points to
//   overload -> operator so it acts like a pointer
//   but copies calls clone when copied
struct JointPtr : public std::unique_ptr<JointBase> {
  using std::unique_ptr<JointBase>::unique_ptr;  // inherit constructors (C++11)

  JointPtr(const JointPtr& obj) : std::unique_ptr<JointBase>() {
    this->reset(obj ? obj->clone() : nullptr);
  }
  JointPtr(const JointBase& obj) : std::unique_ptr<JointBase>() {
    this->reset(obj.clone());
  }
  JointPtr& operator=(const JointPtr& obj) {
    this->reset(obj ? obj->clone() : nullptr);
    return *this;
  }
  JointPtr& operator=(const JointBase& obj) {
    this->reset(obj.clone());
    return *this;
  }
  JointPtr() { this->reset(); };  // default constructor
};

}  // namespace kinematics

#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_JOINT_BASE_H_
