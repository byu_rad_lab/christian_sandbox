#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_QUADRUPED_H_
#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_QUADRUPED_H_

#include "kinematics/arm.h"

namespace kinematics {

class Quadruped : public Arm {

    public: 

        Quadruped();
        ~Quadruped();

        void setBaseWidth(const double width);
        void setBaseLength(const double length);
        void setLegMountAngle(const double angle);
        void setShoulderMass(const double mass);
        void setPowerSupplyMass(const double mass);
        void setValveMass(const double mass);
        
        double getBaseWidth(){return base_width_;}
        double getBaseLength(){return base_length_;}
        double getLegMountAngle(){return leg_mount_angle_;}
        double getBaseMass(){return base_mass_;}
        double getShoulderMass(){return shoulder_mass_;}
        double getPowerSupplyMass(){return power_supply_mass_;}
        double getValveMass(){return valve_mass_;}
        Hom getGCoGLeg1(){return g_CoG_Leg1_;}
        Hom getGCoGLeg2(){return g_CoG_Leg2_;}
        Hom getGCoGLeg3(){return g_CoG_Leg3_;}
        Hom getGCoGLeg4(){return g_CoG_Leg4_;}
        Hom getGLeg1CoG(){return g_Leg1_CoG_;}
        Hom getGLeg2CoG(){return g_Leg2_CoG_;}
        Hom getGLeg3CoG(){return g_Leg3_CoG_;}
        Hom getGLeg4CoG(){return g_Leg4_CoG_;}
        std::vector<Hom> getCoGLegs(){return g_CoG_Legs_;}
        std::vector<Hom> getLegsCoG(){return g_Legs_CoG_;}

    protected:
        void calcBaseMass();

        double base_width_; //base dimensions (put in for quadruped)
        double base_length_;
        double leg_mount_angle_;
        double shoulder_mass_;
        double power_supply_mass_;
        double valve_mass_;

        double base_mass_;

        Hom g_CoG_Leg1_;
        Hom g_CoG_Leg2_;
        Hom g_CoG_Leg3_;
        Hom g_CoG_Leg4_;
        Hom g_Leg1_CoG_;
        Hom g_Leg2_CoG_;
        Hom g_Leg3_CoG_;
        Hom g_Leg4_CoG_;
        std::vector<Hom> g_CoG_Legs_;
        std::vector<Hom> g_Legs_CoG_;

        void calcTransforms();
};

}

#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_QUADRUPED_H_
