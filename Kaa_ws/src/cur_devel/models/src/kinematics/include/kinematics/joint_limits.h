// Otherlab, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_JOINT_LIMITS_H_
#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_JOINT_LIMITS_H_

#include "kinematics/types.h"

namespace kinematics {

class JointLimits {

  // data private
  // resize and check sizes stuff
  // joint limits of the form A x <= b, where x is the joint parameters
  // a_ is a matrix: num_constraints_ by num_dof_
  // b_ is a vector: num_constraints_
  // TODO: should throw if sizes don't match

  // bounds_{upper, lower}_ are used when independent joint limits are needed:
  //   - rejection sampling
  //   - joint solver

 public:
  // JointLimits(double num_dof_);

  // sets limits to limit <= joint_angles <= limit
  void setLimitsScalar(double limit);

  // sets bounds to bounds_lower <= joint_angles <= bounds_upper
  void setLimitsVector(const Vec& bounds_lower, const Vec& bounds_upper);

  void setLimitsCircular(double radius,
                         const Vec& center,
                         int num_constraints,
                         double phase);

  // Returns A x - b.  Should all be negative if within joint limits.
  Vec checkLimits(const Vec& joint_angles);

  inline Mat a(void) { return a_; }
  inline Vec b(void) { return b_; }
  inline Vec c(void) { return c_; }

  inline Vec bounds_lower(void) { return bounds_lower_; }
  inline Vec bounds_upper(void) { return bounds_upper_; }

  inline void set_a(const Mat& a) { a_ = a; }
  inline void set_b(const Vec& b) { b_ = b; }
  inline void set_num_dof(int num_dof) { num_dof_ = num_dof; }

  // find random joint angles
  // samples uniformly over bounds_{lower, upper}_
  // rejects based on A x <= b
  Vec getAngsRandom(void);

 private:
  int num_dof_;
  Mat a_;
  Vec b_;
  Vec c_;

  // scalar bounds on each joint variable
  // the cubiod formed by these should be a superset of the allowable joint angles
  Vec bounds_upper_;
  Vec bounds_lower_;
};

}  // namespace kinematics

#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_JOINT_LIMITS_H_
