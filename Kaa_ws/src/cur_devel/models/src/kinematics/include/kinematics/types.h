// Otherlab, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SRC_KINEMATICS_INCLUDE_KINEMATICS_TYPES_H_
#define SRC_KINEMATICS_INCLUDE_KINEMATICS_TYPES_H_

#include <eigen3/Eigen/Dense>
#include <iostream>

namespace kinematics {

static const int kNumDofMax = 32;

// speed depends on kNumDofMax, 32 faster than 8
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, 0, kNumDofMax, kNumDofMax>
    Mat;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1, 0, kNumDofMax, 1> Vec;

typedef Eigen::Vector3d Vec3;
typedef Eigen::Matrix3d Rot;
typedef Eigen::Vector4d Vec4;
typedef Eigen::Matrix4d Hom;
typedef Eigen::Matrix<double, 6, 1> Vec6;
typedef Vec6 Twist;

typedef Eigen::Quaterniond Quat;
typedef Eigen::AngleAxisd AngAx;

typedef Eigen::Matrix<double, 6, Eigen::Dynamic, 0, 6, kNumDofMax> Jac;
typedef Eigen::Matrix<double, 6, 6> Adj;

typedef Eigen::DiagonalMatrix<double, 6> Mat6Diag;  // inertia matrix, diagonal
                                                    // improves speed
typedef Eigen::Matrix<double, 3, 3> InertiaTensor;

// hat and vee operators
Rot HatVec(const Vec3& p);
Hom HatTwist(const Vec& xi);

// Inputs to these must be block diagonal
// Not sure they're actually useful.
// In both cases rot must be skew symetric, so not really a Rot or Hom
Vec3 VeeRot(const Rot& rot);
Twist VeeHom(const Hom& hom);

// conversion
Vec3 AxFromRot(const Rot& rot);
Rot RotFromAx(const Vec3& ax);
Quat QuatFromAx(const Vec3& ax);
Vec3 AxFromQuat(const Quat& q);

Adj AdjFromHom(const Hom& hom);

// converts OpenCV Hom to OpenGL Hom (rotation by 180 around x axis)
Hom HomGLFromCV(const Hom& cv);

// depricated, prefer "From" from
inline Adj HomToAdj(const Hom& hom) { return AdjFromHom(hom); }
inline Vec3 RotToAx(const Rot& rot) { return AxFromRot(rot); }
inline Rot AxToRot(const Vec3& ax) { return RotFromAx(ax); }

// depricated, use HatVec and HatTwist
inline Rot VecHat(const Vec3& p) { return HatVec(p); }
inline Hom TwistHat(const Vec& xi) { return HatTwist(xi); }

// Produces homogeneous transform with identity rotation and vector of (x, y, z)
Hom HomTranslate(const double x, const double y, const double z);
Hom HomInv(const Hom& hom);

// Converts ax to rotation matrix, concatentates with vec into homogeneous transform
Hom HomFromAxVec(const Vec3& ax, const Vec3& vec);

// Concatenates Rot and Vec3 to form Hom
Hom HomFromRotVec(const Rot& rot, const Vec3& vec);

// Returns a rotational equivalent of r1*(1 - scale) + r2*scale
Rot RotScale(const Rot& rot, double scale);
Rot RotInterp(const Rot& r1, const Rot& r2, double scale);
Hom HomScale(const Hom& hom, double scale);
Hom HomInterp(const Hom& h1, const Hom& h2, double scale);

Twist TwistFromHom(const Hom& hom);

// depricated, prefer HomFromAxVec
inline Hom HomFromAx(Vec3 ax) { return HomFromAxVec(ax, Vec3::Zero()); }
inline Hom HomRotate(Vec3 ax) { return HomFromAx(ax); }

inline Rot HomGetRot(const Hom& hom) { return hom.topLeftCorner(3, 3); }
inline Vec3 HomGetVec3(const Hom& hom) { return hom.topRightCorner(3, 1); }

// depricated
inline Twist HomToTwist(const Hom& hom) { return TwistFromHom(hom); }

Vec clamp(Vec w, double d, bool norm_type = true);

// Measure of error between rotations, given by trace(I - r1.transpose() * r2)
double RotError(const Rot& r1, const Rot& r2);

// Euclidean distance between vectors
double Vec3Error(const Vec3& v1, const Vec3& v2);

// Euclidean distance or VecError and error_ratio * RotError
// error_ratio of 1.0 means unit length is equivalent to 90 degrees
double HomError(const Hom& g1, const Hom& g2, double error_ratio);

enum class Frame {
  Spatial,
  Body,
  Hybrid,
};

// class to represent mass/inertia properties
struct Inertia {
  double mass;
  InertiaTensor inertia;  // inertia tensor, 3x3
  Vec3 vec;               // position of COM in base frame

  void Initialize(void);
  void CheckIsDiagonal(void);
  void set_inertia_diag(double Ix, double Iy, double Iz);
  void set_vec(double x, double y, double z);

  // Rot rot;  // implement later, if needed
};

// combines two inertia structs
// their inertia tensors must be diagonal and oriented along a common frame
// this requirement can be relaxed, but more implementation is required
Inertia InertiaCombine(Inertia i1, Inertia i2);

// function for checking if matrix sizes are correct
template <typename Derived>
void CheckMatSize(const Eigen::MatrixBase<Derived>& mat,
                  const int rows,
                  const int cols = 1) {
  if (mat.rows() != rows || mat.cols() != cols) {
    std::cout << "Error: matrix sizes aren't right." << std::endl;
    throw("Matrix sized aren't right.");
  }
}

}  // namespace kinematics

#endif  // SRC_KINEMATICS_INCLUDE_KINEMATICS_TYPES_H_
