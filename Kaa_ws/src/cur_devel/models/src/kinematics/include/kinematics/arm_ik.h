// Otherlab, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_ARM_IK_H_
#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_ARM_IK_H_

#include <eigen3/Eigen/Dense>
#include "kinematics/types.h"
#include "kinematics/arm.h"

namespace kinematics {
class ArmIK {
 public:
  ArmIK();
  ArmIK(Arm* arm);

  sdls sdls_solver;

  void calcIK(const Hom& g_des);
  void IKStep(const Hom& g_des);

  Arm* arm_;
  double err_t_ = 0;

  // Parameters for Solver
  int max_iter_ = 2;
  double end_tol_ = .001;  // For end check

  // controls weighting for position/orientation weighting
  double error_ratio_ = 90.0 / 1000.0;
  double e_clamp_ = 10.0;

  int solve_iteration_ = 0;
  int status_ = 0;

  // IK MODE
  bool verbose_ = false;
  enum Mode { SIXDOF, POS_ONLY, ROT_ONLY };
  Mode mode_ = SIXDOF;
};
}

#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_IK_ARM_IK_H_
