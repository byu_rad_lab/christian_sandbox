// Otherlab, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_JOINT_CC1D_H_
#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_JOINT_CC1D_H_

#include <vector>

#include "kinematics/types.h"
#include "kinematics/joint_base.h"

namespace kinematics {

class JointCC1D;
JointCC1D* getJointPtrAsCC1D(JointBase* base);

class JointCC1D : public JointBase {
 public:
  JointCC1D(void);
  JointCC1D(double height);

  JointCC1D* clone() const { return new JointCC1D(*this); }

  void calcFK(void);
  void calcJac(void);
  void calcDerivJac(void);

  static const int num_dof_derived_ = 1;
  static constexpr double zero_tol_ = (1.0e-4) * (1.0e-4);

  double h_;

 protected:
  double th_;

  // w = (u, v, 0)
  // wt = w / ||w|| = (ut_, vt_, 0)
  // phi_ = ||w||
  double th2_;    // th_^2
  double th3_;    // th_^3
  double sth_;    // sin(th_)
  double cth_;    // cos(th_)
  double cthm1_;  // cos(th_) - 1
};

}  // namespace kinematics

#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_JOINT_CC1D_H_
