/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.  */

#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_ARM_TESTING_H_
#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_ARM_TESTING_H_

#include "kinematics/types.h"
#include "kinematics/link.h"
#include "kinematics/arm.h"
#include "kinematics/partials.h"

#include "eigen3/Eigen/Dense"

#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <kinematics/joint_base.h>
#include <iostream>

//using kinematics::JointBase;
using kinematics::Link;
using kinematics::Arm;
using kinematics::Vec;
using kinematics::Mat;

using Eigen::MatrixBase;

using std::cout;
using std::endl;

template <typename Derived>
void print_Mat(const Eigen::MatrixBase<Derived>& a) {
  for (int i = 0; i < a.rows(); i++) {
    if (a.size() < 10) {
      printf(" ");
    } else {
      printf("\n");
    }

    for (int j = 0; j < a.cols(); j++) {
      printf("%.10f", (double)a(i, j));
      if (j < a.cols() - 1) {
        printf(", ");
      }
    }

    if (i < a.rows() - 1) {
      printf(",");
    } else {
      printf(";\n");
    }
  }
}

template <typename Derived>
void write_Mat(FILE* fd, const Eigen::MatrixBase<Derived>& a) {
  for (int i = 0; i < a.rows(); i++) {
    if (a.size() < 10) {
      fprintf(fd, " ");
    } else {
      fprintf(fd, "\n");
    }

    for (int j = 0; j < a.cols(); j++) {
      fprintf(fd, "%.10f", (double)a(i, j));
      if (j < a.cols() - 1) {
        fprintf(fd, ", ");
      }
    }

    if (i < a.rows() - 1) {
      fprintf(fd, ",");
    } else {
      fprintf(fd, ";\n");
    }
  }
}

template <typename Derived>
void write_Mat_initializer(FILE* fd, const char* type, const char* name,
                             const Eigen::MatrixBase<Derived>& a) {
  fprintf(fd, "\n%s %s(%lu, %lu);\n", type, name, a.rows(), a.cols());
  fprintf(fd, "%s << ", name);
  write_Mat(fd, a);
}

template <typename Derived>
void print_Mat_initializer(const char* type, const char* name, const Eigen::MatrixBase<Derived>& a) {
  printf("\n%s %s(%lu, %lu);\n", type, name, a.rows(), a.cols());
  printf("%s << ", name);
  print_Mat(a);
}

template <typename Derived>
void check_Mat(const char* name, const MatrixBase<Derived>& a, const MatrixBase<Derived>& b, double tol) {
  // void check_Mat(const MatrixBase<Derived>& a, const MatrixBase<Derived>& b) {
  Mat diff(a.rows(), a.cols());
  diff = b - a;
  double max = diff.maxCoeff();
  double min = diff.minCoeff();
  double max_diff = max > min ? max : -min;

  if (max_diff > tol) {
    printf("### Error: check of %s failed! ###\n", name);
    printf("max_diff = %.5e\n", max_diff);
    printf("tol = %.5e\n", tol);
    printf("a = \n");
    print_Mat(a);
    printf("b = \n");
    print_Mat(b);
    // throw;
  }
}

template <typename Derived>
void write_check(FILE* fd, int config_num, const char* type, const char* name, const MatrixBase<Derived>& a) {
  fprintf(fd,
          "\nvoid check_%s_%02d(const %s& %s, double "
          "tol) {\n",
          name, config_num, type, name);
  fprintf(fd, "printf(\"checking config %2d, %s \\n\");", config_num, name);
  char buffer[1024];
  sprintf(buffer, "%s2", name);

  write_Mat_initializer(fd, type, buffer, a);
  // print_Mat_initializer(type, buffer, a);
  fprintf(fd, "check_Mat(\"%s\", %s, %s2, tol);\n}\n\n", name, name, name);
  // cout << "a = " << a << endl;
}

template <typename Derived>
void print_check(int config_num, const char* type, const char* name, const MatrixBase<Derived>& a) {
  printf(
      "\nvoid check_%s_%02d(const %s& %s, double "
      "tol) {\n",
      name, config_num, type, name);
  printf("printf(\"checking config %2d, %s \\n\");", config_num, name);
  char buffer[1024];
  sprintf(buffer, "%s2", name);

  print_Mat_initializer(type, buffer, a);
  printf("check_Mat(\"%s\", %s, %s2, tol);\n}\n\n", name, name, name);
  // cout << "a = " << a << endl;
}

// void setup_arm(int config_num, Arm* arm, Vec* angs, Vec* vels) {
void setup_arm(int config_num, Arm* arm) {
  Vec angs(arm->num_dof_);
  Vec vels(arm->num_dof_);
  switch (config_num) {
    case 0:
      angs << 0.1, -.025, 0.56, 0.62, -0.32, 0.14, -0.22, -0.43;
      vels << -.025, 0.56, 0.62, -0.32, 0.14, -0.22, -0.43, 0.1;
      break;
    case 1:
      angs << 0.0, 0.0, 0.0, 0.0, -0.32, 0.14, -0.22, -0.43;
      vels << 0.0, 0.0, 0.0, 0.0, 0.14, -0.22, -0.43, 0.1;
      break;
    default:
      printf("### Warning:  config_num = %d not handled.\n ###", config_num);
      throw;
  }
  Vec torque_external(arm->num_dof_);
  torque_external.setZero();
  arm->setJointAngs(angs);
  arm->setJointVels(vels);
  arm->calcJointAccels(torque_external);
}

Arm make_arm_new(void) {
  Vec lengths = Vec(4);
  lengths << 0.3, 0.05, 0.3, 0.05;
  double height = 0.2;

  Arm arm(height, lengths, kinematics::kJointTypeCCC);
  return arm;
}

void write_all_checks(FILE* fd, int config_num, Arm* arm) {
  write_check(fd, config_num, "Vec", "joint_accs", arm->joint_accs_);
  write_check(fd, config_num, "Mat", "jac_spatial", arm->jac_spatial_);
  write_check(fd, config_num, "Mat", "inertia_mat", arm->inertia_mat_);
  write_check(fd, config_num, "Mat", "coriolis_mat", arm->coriolis_mat_);

  write_check(fd, config_num, "Mat", "links_0_g_world_top", arm->links_[0].g_world_top_);
  write_check(fd, config_num, "Mat", "links_3_g_world_top", arm->links_[3].g_world_top_);

  write_check(fd, config_num, "Mat", "links_0_jac_body", arm->links_[0].jac_body_);
  write_check(fd, config_num, "Mat", "links_3_jac_body", arm->links_[3].jac_body_);

  write_check(fd, config_num, "Mat", "arm_partials_0_inertia_mat", arm->partials_[0].inertia_mat_);
  write_check(fd, config_num, "Mat", "arm_partials_3_inertia_mat", arm->partials_[3].inertia_mat_);

  write_check(fd, config_num, "Mat", "arm_links_1_partials_1_jac_body",
              arm->links_[1].partials_[1].jac_body_);
  write_check(fd, config_num, "Mat", "arm_links_3_partials_3_jac_body",
              arm->links_[3].partials_[3].jac_body_);
}

void print_all_checks(int config_num, Arm* arm) {
  print_check(config_num, "Vec", "joint_accs", arm->joint_accs_);
  print_check(config_num, "Mat", "jac_spatial", arm->jac_spatial_);
  print_check(config_num, "Mat", "inertia_mat", arm->inertia_mat_);
  print_check(config_num, "Mat", "coriolis_mat", arm->coriolis_mat_);

  print_check(config_num, "Mat", "links_0_g_world_top", arm->links_[0].g_world_top_);
  print_check(config_num, "Mat", "links_3_g_world_top", arm->links_[3].g_world_top_);

  print_check(config_num, "Mat", "links_0_jac_body", arm->links_[0].jac_body_);
  print_check(config_num, "Mat", "links_3_jac_body", arm->links_[3].jac_body_);

  print_check(config_num, "Mat", "arm_partials_0_inertia_mat", arm->partials_[0].inertia_mat_);
  print_check(config_num, "Mat", "arm_partials_3_inertia_mat", arm->partials_[3].inertia_mat_);

  print_check(config_num, "Mat", "arm_links_1_partials_1_jac_body", arm->links_[1].partials_[1].jac_body_);
  print_check(config_num, "Mat", "arm_links_3_partials_3_jac_body", arm->links_[3].partials_[3].jac_body_);
}

// this should probably be auto coded
void write_do_checks(FILE* fd) {
  fprintf(fd, "void do_checks(int config_num, Arm* arm) {\n");
  fprintf(fd, "  double tol = 1e-9;\n");
  fprintf(fd, "  switch (config_num) {\n");
  fprintf(fd, "    case 0:\n");
  fprintf(fd, "      check_joint_accs_00(arm->joint_accs_, tol);\n");
  fprintf(fd, "      check_jac_spatial_00(arm->jac_spatial_, tol);\n");
  fprintf(fd, "      check_inertia_mat_00(arm->inertia_mat_, tol);\n");
  fprintf(fd, "      check_coriolis_mat_00(arm->coriolis_mat_, tol);\n");
  fprintf(fd, "\n");
  fprintf(fd, "      check_links_0_g_world_top_00(arm->links_[0].g_world_top_, tol);\n");
  fprintf(fd, "      check_links_3_g_world_top_00(arm->links_[3].g_world_top_, tol);\n");
  fprintf(fd, "\n");
  fprintf(fd, "      check_links_0_jac_body_00(arm->links_[0].jac_body_, tol);\n");
  fprintf(fd, "      check_links_3_jac_body_00(arm->links_[3].jac_body_, tol);\n");
  fprintf(fd, "\n");
  fprintf(fd, "      check_arm_partials_0_inertia_mat_00(arm->partials_[0].inertia_mat_, tol);\n");
  fprintf(fd, "      check_arm_partials_3_inertia_mat_00(arm->partials_[3].inertia_mat_, tol);\n");
  fprintf(fd, "\n");
  fprintf(fd,
          "      check_arm_links_1_partials_1_jac_body_00(arm->links_[1].partials_[1].jac_body_, tol);\n");
  fprintf(fd,
          "      check_arm_links_3_partials_3_jac_body_00(arm->links_[3].partials_[3].jac_body_, tol);\n");
  fprintf(fd, "      break;\n");
  fprintf(fd, "\n");
  fprintf(fd, "    case 1:\n");
  fprintf(fd, "      check_joint_accs_01(arm->joint_accs_, tol);\n");
  fprintf(fd, "      check_jac_spatial_01(arm->jac_spatial_, tol);\n");
  fprintf(fd, "      check_inertia_mat_01(arm->inertia_mat_, tol);\n");
  fprintf(fd, "      check_coriolis_mat_01(arm->coriolis_mat_, tol);\n");
  fprintf(fd, "\n");
  fprintf(fd, "      check_links_0_g_world_top_01(arm->links_[0].g_world_top_, tol);\n");
  fprintf(fd, "      check_links_3_g_world_top_01(arm->links_[3].g_world_top_, tol);\n");
  fprintf(fd, "\n");
  fprintf(fd, "      check_links_0_jac_body_01(arm->links_[0].jac_body_, tol);\n");
  fprintf(fd, "      check_links_3_jac_body_01(arm->links_[3].jac_body_, tol);\n");
  fprintf(fd, "\n");
  fprintf(fd, "      check_arm_partials_0_inertia_mat_01(arm->partials_[0].inertia_mat_, tol);\n");
  fprintf(fd, "      check_arm_partials_3_inertia_mat_01(arm->partials_[3].inertia_mat_, tol);\n");
  fprintf(fd, "\n");
  fprintf(fd,
          "      check_arm_links_1_partials_1_jac_body_01(arm->links_[1].partials_[1].jac_body_, tol);\n");
  fprintf(fd,
          "      check_arm_links_3_partials_3_jac_body_01(arm->links_[3].partials_[3].jac_body_, tol);\n");
  fprintf(fd, "      break;\n");
  fprintf(fd, "  }\n");
  fprintf(fd, "  printf(\"Case %%d checked.\\n\", config_num);\n");
  fprintf(fd, "}\n");
}

#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_ARM_TESTING_H_
