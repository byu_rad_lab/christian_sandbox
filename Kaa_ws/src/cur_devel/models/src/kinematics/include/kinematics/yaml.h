// Otherlab, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_YAML_H_
#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_YAML_H_
#include <kinematics/joint_base.h>
#include <stdio.h>

#include <iostream>

#include "yaml-cpp/yaml.h"

#include "kinematics/types.h"
#include "kinematics/joint_ccc.h"
#include "kinematics/joint_cc1d.h"
#include "kinematics/joint_rot.h"
#include "kinematics/link.h"
#include "kinematics/arm.h"

// Note:  encode methods not implemented
namespace YAML {

template <>
struct convert<kinematics::Vec> {
  static bool decode(const YAML::Node& node, kinematics::Vec& vec) {
    // if(!node.IsSequence() || node.size() != 3) {
    if (!node.IsSequence()) {
      return false;
    }

    vec.resize(node.size(), 1);
    for (int i = 0; i < (int)node.size(); i++) {
      vec[i] = node[i].as<double>();
    }
    return true;
  }
};

template <>
struct convert<kinematics::Vec3> {
  static bool decode(const YAML::Node& node, kinematics::Vec3& vec) {
    vec(0) = node["x"].as<double>(0.0);
    vec(1) = node["y"].as<double>(0.0);
    vec(2) = node["z"].as<double>(0.0);
    return true;
  }
};

template <>
struct convert<kinematics::Hom> {
  static bool decode(const YAML::Node& node, kinematics::Hom& hom) {
    hom.setIdentity();

    // maybe this bit should be a vec3
    hom(0, 3) = node["x"].as<double>(0.0);
    hom(1, 3) = node["y"].as<double>(0.0);
    hom(2, 3) = node["z"].as<double>(0.0);

    // rotation should be parsed here
    hom(0, 0) = node["R00"].as<double>(1.0);
    hom(0, 1) = node["R01"].as<double>(0.0);
    hom(0, 2) = node["R02"].as<double>(0.0);

    hom(1, 0) = node["R10"].as<double>(0.0);
    hom(1, 1) = node["R11"].as<double>(1.0);
    hom(1, 2) = node["R12"].as<double>(0.0);

    hom(2, 0) = node["R20"].as<double>(0.0);
    hom(2, 1) = node["R21"].as<double>(0.0);
    hom(2, 2) = node["R22"].as<double>(1.0);

    return true;
  }
};

template <>
struct convert<kinematics::InertiaTensor> {
  static bool decode(const YAML::Node& node, kinematics::InertiaTensor& inert) {
    inert.setZero();
    // diagonal stuff not implemented
    inert(0, 0) = node["Ix"].as<double>(0.0);
    inert(1, 1) = node["Iy"].as<double>(0.0);
    inert(2, 2) = node["Iz"].as<double>(0.0);
    return true;
  }
};

template <>
struct convert<kinematics::Mat6Diag> {
  static bool decode(const YAML::Node& node, kinematics::Mat6Diag& inert) {
    double mass = node["mass"].as<double>(1.0);
    double Ix = node["Ix"].as<double>(1.0);
    double Iy = node["Iy"].as<double>(1.0);
    double Iz = node["Iz"].as<double>(1.0);
    inert.diagonal()[0] = mass;
    inert.diagonal()[1] = mass;
    inert.diagonal()[2] = mass;
    inert.diagonal()[3] = Ix;
    inert.diagonal()[4] = Iy;
    inert.diagonal()[5] = Iz;
    return true;
  }
};

template <>
struct convert<kinematics::Inertia> {
  static bool decode(const YAML::Node& node, kinematics::Inertia& inert) {
    inert.vec = node["vec"].as<kinematics::Vec3>();
    inert.mass = node["mass"].as<double>(0.0);
    inert.inertia = node["inertia"].as<kinematics::InertiaTensor>();
    // rotation not implemented yet

    return true;
  }
};

// this can't be a struct convert as it can't return abstract jointbase
void DecodeJointBase(const YAML::Node& node, kinematics::JointBase* joint);

template <>
struct convert<kinematics::JointCCC> {
  static bool decode(const YAML::Node& node, kinematics::JointCCC& joint) {

    // default constructor
    joint = kinematics::JointCCC();
    DecodeJointBase(node, &joint);

    if (node["h"]) {
      joint.h_ = node["h"].as<double>(0.20);
    }
    joint.checkMatSizes();  // ensure matrix sizes are right
    return true;
  }
};

template <>
struct convert<kinematics::JointRot> {
  static bool decode(const YAML::Node& node, kinematics::JointRot& joint) {

    // default constructor
    joint = kinematics::JointRot();
    DecodeJointBase(node, &joint);

    if (node["h"]) {
      joint.h_ = node["h"].as<double>(0.20);
    }
    joint.checkMatSizes();  // ensure matrix sizes are right
    return true;
  }
};

template <>
struct convert<kinematics::JointCC1D> {
  static bool decode(const YAML::Node& node, kinematics::JointCC1D& joint) {

    // default constructor
    joint = kinematics::JointCC1D();
    DecodeJointBase(node, &joint);

    if (node["h"]) {
      joint.h_ = node["h"].as<double>(0.20);
    }
    joint.checkMatSizes();  // ensure matrix sizes are right
    return true;
  }
};

template <>
struct convert<kinematics::Link> {
  static bool decode(const YAML::Node& node, kinematics::Link& link) {

    // get the joint type
    std::string joint_type_name;
    kinematics::JointType joint_type;

    if (node["joint"] && node["joint"]["type"]) {
      joint_type_name = node["joint"]["type"].as<std::string>();
      joint_type = kinematics::JointTypeFromString(joint_type_name);
    } else {
      std::cout << "Warning: your link has no joint specified.\n";
      joint_type = kinematics::kJointTypeCCC;  // default
    }

    // call link constructor
    link = kinematics::Link(joint_type);

    // parse the joint
    if (node["joint"]) {
      switch (joint_type) {
        case kinematics::kJointTypeCCC:
          link.joint_ = node["joint"].as<kinematics::JointCCC>();
          break;
        case kinematics::kJointTypeCC1D:
          link.joint_ = node["joint"].as<kinematics::JointCC1D>();
          break;
        case kinematics::kJointTypeRot:
          link.joint_ = node["joint"].as<kinematics::JointRot>();
          break;
        default:
          std::cout << "Error: joint type not handled!" << std::endl;
          throw("Joint type not handled.");
      }
    }

    // parse the rest of the link
    if (node["g_top_end"]) {
      link.g_top_end_ = node["g_top_end"].as<kinematics::Hom>();
    }

    link.has_inertia_added_ = node["has_inertia_added"].as<bool>(false);
    if (node["inertia_self"]) {
      std::cout << "parsing inertia_self" << std::endl;
      link.inertia_self_ = node["inertia_self"].as<kinematics::Inertia>();
    }
    if (node["inertia_added"]) {
      link.inertia_added_ = node["inertia_added"].as<kinematics::Inertia>();
    }

    link.InitInertia();
    link.checkMatSizes();  // ensure matrix sizes are right
    return true;
  }
};

template <>
struct convert<kinematics::Arm> {
  static bool decode(const YAML::Node& node, kinematics::Arm& arm) {
    arm = kinematics::Arm();
    if (!node["links"] || (node["links"].IsSequence() && node["links"].size() == 0)) {
      std::cout << "Warning:  your arm has no links.\n";
    }
    if (node["g_world_arm"]) {
      arm.g_world_arm_ = node["g_world_arm"].as<kinematics::Hom>();
    }
    if (node["gravity"]) {
      arm.setGravity(node["gravity"].as<double>(-9.81));
    }
    for (int i = 0; i < (int)node["links"].size(); i++) {
      arm.addLink(node["links"][i].as<kinematics::Link>());
    }
    arm.checkMatSizes();
    return true;
  }
};

}  // namespace YAML

namespace kinematics {

void PrintParserDemoUsage(void);
YAML::Node LoadYAMLCommandLine(int argc, char* argv[]);

template <typename T>
T YAMLConvertFromFilestring(std::string filestring) {
  YAML::Node node;
  node = YAML::Load(filestring);
  T t = node.as<T>();
  return t;
}

template <typename T>
T YAMLConvertFromFilename(std::string filename) {
  YAML::Node node;
  try {
    node = YAML::LoadFile(filename);
  } catch (YAML::BadFile& e) {
    std::cout << "Warning: there was a problem with your file.\n";
    throw(e);
  }
  T t = node.as<T>();
  return t;
}

}  // namespace kinematics

#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_YAML_H_
