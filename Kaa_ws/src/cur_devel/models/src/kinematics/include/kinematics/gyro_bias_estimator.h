/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.  */

#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_GYRO_BIAS_ESTIMATOR_H_
#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_GYRO_BIAS_ESTIMATOR_H_

#include "kinematics/types.h"

#include <eigen3/Eigen/Dense>
#include <memory>
#include <iostream>

namespace kinematics {

class GyroBiasEstimator {

 public:
  inline void SetKpRot(double kp) { kp_rot_ = kp; };
  inline void SetKiRot(double kp) { ki_rot_ = kp; };
  inline void SetBias(Vec3 bias) { bias_ = bias; };
  inline void SetQ(Quat q) { q_ = q; };
  inline void SetQRot(Rot rot) { q_ = Quat(rot); };

  inline Quat GetQ(void) { return q_; };
  inline Vec3 GetQAsAx(void) { return AxFromQuat(q_); };
  inline Rot GetQAsRot(void) { return q_.matrix(); };
  inline Vec3 GetBias(void) { return bias_; };

  inline void NormalizeQ(void) { q_.normalize(); };

  void PredictGyro(Vec3 omega, double dt);
  void CorrectRot(Rot rot);

 private:
  double kp_rot_ = 0.0;
  double ki_rot_ = 0.0;
  Vec3 bias_ = Vec3::Zero();
  Quat q_ = Quat::Identity();
};

}  // namespace kinematics

#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_GYRO_BIAS_ESTIMATOR_H_
