// Otherlab, Inc ("COMPANY") CONFIDENTIAL
// Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
//
// NOTICE:  All information contained herein is, and remains the property of
// COMPANY.  The intellectual and technical concepts contained herein are
// proprietary to COMPANY and may be covered by U.S. and Foreign Patents,
// patents in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material is
// strictly forbidden unless prior written permission is obtained from COMPANY.
// Access to the source code contained herein is hereby forbidden to anyone
// except current COMPANY employees, managers or contractors who have executed
// Confidentiality and Non-disclosure agreements explicitly covering such
// access.
//
// The copyright notice above does not evidence any actual or intended
// publication or disclosure of this source code, which includes information
// that is confidential and/or proprietary, and is a trade secret, of COMPANY.
// ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC
// DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN
// CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
// LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE
// CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
// REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR
// SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_JOINT_CCC_H_
#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_JOINT_CCC_H_

#include <vector>

#include "kinematics/types.h"
#include "kinematics/joint_base.h"

namespace kinematics {

class JointCCC;
JointCCC* getJointPtrAsCCC(JointBase* base);

class JointCCC : public JointBase {
 public:
  JointCCC(void);
  JointCCC(double height);
  JointCCC(double height, double mass, double pressure_to_torque, double passive_spring_constant, double max_pressure);

  void Init(double height, double mass, double pressure_to_torque, double passive_spring_constant, double max_pressure);



  JointCCC* clone() const { return new JointCCC(*this); }

  void set_h(double h);

  void calcFK(void);
  Hom calcFKScaled(double scale);
  void calcCOM(void);
  void calcJac(void);
  void calcDerivJac(void);
  void calcTorqueLimits(void);

  void resetAngsWithinLimit(void);

  inline double getKt(void){return pressure_to_torque_;}

  static const int num_dof_derived_ = 2;
  static constexpr double zero_tol_ = (1.0e-6) * (1.0e-6);

  double h_;

 protected:
  double u_;
  double v_;
  double phi_;

  double pressure_to_torque_; //Nm/Pa
  double passive_spring_constant_; 
  double max_pressure_;

  // w = (u, v, 0)
  // wt = w / ||w|| = (ut_, vt_, 0)
  // phi_ = ||w||
  double ut_;        // u_ / phi_
  double vt_;        // v_ / phi_
  double u2_;        // u_^2
  double v2_;        // v_^2
  double ut2_;       // ut_^2
  double vt2_;       // vt_^2
  double sp_;        // sin(phi_)
  double cp_;        // cos(phi_)
  double phi2_;      // phi_ * phi_
  double phi_inv_;   // 1 / phi
  double phi2_inv_;  // 1 / phi_^2
  double phi3_inv_;  // 1 / phi_^3
  double cpm1_;      // cos(phi_) - 1
};

}  // namespace kinematics

#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_JOINT_CCC_H_
