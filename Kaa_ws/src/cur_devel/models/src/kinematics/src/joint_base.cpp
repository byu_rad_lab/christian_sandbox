#include <kinematics/joint_base.h>

#include <stdio.h>
#include <iostream>
#include <cstdlib>

namespace kinematics {

// cpp part of joint type enum creation
SMARTENUM_CREATE_SOURCE(JointType, KINEMATICS_JOINT_TYPE_LIST)

// might need a derived resize
void JointBase::resize(void) {
  angs_.resize(num_dof_);
  vels_.resize(num_dof_);

  torque_max_.resize(num_dof_);
  torque_min_.resize(num_dof_);

  spring_constant_.resize(num_dof_);
  damping_constant_.resize(num_dof_);

  limits_upper_.resize(num_dof_);
  limits_lower_.resize(num_dof_);
  limits()->set_num_dof(num_dof_);

  jac_.resize(6, num_dof_);  // not sure why the first parameter is needed

  partials_.clear();
  for (int i = 0; i < num_dof_; i++) {
    partials_.push_back(JointPartials());
    partials_.back().resize(num_dof_);
  }

  joint_stiffness_ = Vec::Zero(num_dof_, 1).asDiagonal();
  setDefaults();
}

void JointBase::setDefaults(void) {
  limits_lower_.setOnes();
  limits_upper_.setOnes();
  limits_lower_ *= -1.57;
  limits_upper_ *= 1.57;
  limits()->setLimitsScalar(1.57);

  spring_constant_.setZero();
  damping_constant_.setZero();

  joint_stiffness_ = Mat::Identity(num_dof_, num_dof_);
}

JointLimits* JointBase::limits(void) { return &limits_; }

void JointBase::setAngSingle(int index, double ang) { angs_[index] = ang; }

// actually call setAngs in case virtual funciton is overridden
void JointBase::setAngsZero(void) { setAngs(Vec(num_dof_).setZero()); }
void JointBase::setVelsZero(void) { setVels(Vec(num_dof_).setZero()); }

void JointBase::setStateZero(void) {
  setAngsZero();
  setVelsZero();
}

// TODO: replace this with a call to limits
void JointBase::setAngsRandom(void) {
  setAngs(limits_.getAngsRandom());
}

void JointBase::setVelsRandom(void) {
  vels_.setRandom();  // set to uniform distribution [-1, 1]
}

void JointBase::resetAngsWithinLimit(void) {
  for (int i = 0; i < num_dof_; i++) {
    if (angs_[i] > limits_upper_[i]) {
      angs_[i] = limits_upper_[i];
    } else if (angs_[i] < limits_lower_[i]) {
      angs_[i] = limits_lower_[i];
    }
  }
}

void JointBase::calcDerivJac() {
  calcJac();
  calcDerivJacNumeric();
}

void JointBase::calcTorqueLimits(void){
}

void JointBase::calcDerivJacNumeric(void) {
  const double eps = 1.0e-7;
  const double eps_inv = 1.0 / eps;

  Jac jac_0(6, num_dof_);
  std::vector<Jac> jacs;

  Vec angs0 = angs_;  // initial position
  Vec angs = angs0;

  for (int i = 0; i < num_dof_; i++) {
    angs = angs0;
    angs(i) = angs0(i) + eps;
    setAngs(angs);
    calcJac();
    jacs.push_back(jac_);
  }

  angs = angs0;
  setAngs(angs);
  calcJac();

  for (int i = 0; i < num_dof_; i++) {
    partials_[i].jac_ = (jacs[i] - jac_) * eps_inv;
  }
}

void JointBase::setJointStiffnessScalar(double k) {
  setJointStiffnessVector(k * Vec::Ones(num_dof_));
}
void JointBase::setJointStiffnessVector(Vec vec) { joint_stiffness_ = vec.asDiagonal(); }
void JointBase::setJointStiffnessMatrix(Mat mat) { joint_stiffness_ = mat; }

void JointBase::checkMatSizes(void) {
  CheckMatSize(angs_, num_dof_);
  CheckMatSize(vels_, num_dof_);

  CheckMatSize(limits_lower_, num_dof_);
  CheckMatSize(limits_upper_, num_dof_);

  CheckMatSize(spring_constant_, num_dof_);
  CheckMatSize(damping_constant_, num_dof_);

  CheckMatSize(jac_, 6, num_dof_);

  CheckMatSize(joint_stiffness_, num_dof_, num_dof_);

  // check for the right number of partials?
  for (auto& part : partials_) {
    part.checkMatSizes();
  }
}

}  // namespace kinematics
