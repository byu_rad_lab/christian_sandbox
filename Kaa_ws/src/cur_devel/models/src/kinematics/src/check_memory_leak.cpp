#include "kinematics/types.h"
#include <cmath>
#include <iostream>

using std::cout;
using std::endl;
using namespace kinematics;

int main () {
    // check for memory leaks
    Rot R;
    R << -1., 0., 0., 0., -1., 0., 0., 0., 1.; //put values into R
 
    for( int i = 1; i < 1000000; i++ ) {
        R = -1*R;
        AxFromRot(R);
        cout << i << endl;
    } 
    return 0;
}
