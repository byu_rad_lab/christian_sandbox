
#include "kinematics/partials.h"

namespace kinematics {

// ArmPartials methods
void ArmPartials::resize(int num_dof) {
  num_dof_ = num_dof;
  jac_spatial_.resize(6, num_dof);
  inertia_mat_.resize(num_dof, num_dof);

  jac_spatial_.setZero();
  inertia_mat_.setZero();
}

// ArmPartials methods
void ArmPartials::checkMatSizes(void) {
  CheckMatSize(jac_spatial_, 6, num_dof_);
  CheckMatSize(inertia_mat_, num_dof_, num_dof_);
}

void LinkPartials::resize(int num_dof) {
  num_dof_ = num_dof;
  jac_body_.resize(6, num_dof_);
  jac_body_.setZero();
}

void LinkPartials::checkMatSizes() { CheckMatSize(jac_body_, 6, num_dof_); }

// JointPartials methods
void JointPartials::resize(int num_dof) {
  num_dof_ = num_dof;
  jac_.resize(6, num_dof);
  jac_.setZero();
}

void JointPartials::checkMatSizes() { CheckMatSize(jac_, 6, num_dof_); }

}  // namespace kinematics
