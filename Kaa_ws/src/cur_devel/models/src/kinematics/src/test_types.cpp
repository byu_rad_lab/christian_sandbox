#include "kinematics/types.h"

#include <stdio.h>

#include <iostream>

using kinematics::Vec3;
using kinematics::Rot;
using kinematics::Hom;
using kinematics::HomError;
using kinematics::Vec3Error;
using kinematics::RotError;
using kinematics::RotFromAx;
using kinematics::HomFromRotVec;
using kinematics::HomFromAxVec;

using kinematics::RotScale;
using kinematics::RotInterp;

using kinematics::HomScale;
using kinematics::HomInterp;

using std::cout;
using std::endl;

int main() {

  int status = 0;

  double error;
  Rot r1 = RotFromAx(Vec3(1.0, 0.1, -0.2));

  // Test RotScale for a few different factors
  // rot = RotScale(rot, 1/factor)^factor
  const int num_factors = 3;
  int factors[num_factors] = {2, 3, 5};
  int factor;
  double scale;
  for (int f = 0; f < num_factors; f++) {
    factor = factors[f];
    scale = 1.0 / static_cast<double>(factor);
    Rot r1_scaled = RotScale(r1, scale);
    Rot r1_calc = r1_scaled;
    for (int i = 1; i < factor; i++) {
      r1_calc *= r1_scaled;
    }

    error = RotError(r1, r1_calc);
    if (fabs(error) > 1e-14) {
      cout << "Error:  For factor " << factors[f]
           << " RotError(r1, r1_calc) should be zero.  Instead it's: " << error
           << endl;
      status++;
    }
  }

  // Check that RotScale(rot, -1) returns rot inverse.
  Rot r2 = RotFromAx(Vec3(0.5, -0.1, -0.3));
  Rot r2_inv = RotScale(r2, -1);
  error = RotError(r2.transpose(), r2_inv);
  if (fabs(error) > 1e-14) {
    cout << "Error:  RotError(r2.transpose(), r2_inv) should be zero.  Instead it's: "
         << error << endl;
    status++;
  }

  // Check RotInterp
  Rot ri;

  // scale = 0 should return r1
  ri = RotInterp(r1, r2, 0.0);
  error = RotError(r1, ri);
  if (fabs(error) > 1e-14) {
    cout << "Error:  RotError(r2.transpose(), r2_inv) should be zero.  Instead it's: "
         << error << endl;
    status++;
  }

  // scale = 1 should return r1
  ri = RotInterp(r1, r2, 1.0);
  error = RotError(r2, ri);
  if (fabs(error) > 1e-14) {
    cout << "Error:  RotError(r2.transpose(), r2_inv) should be zero.  Instead it's: "
         << error << endl;
    status++;
  }

  ri = RotInterp(r1, r2, 1.0 / 3.0);
  Rot r_delta = r1.transpose() * ri;
  Rot r2_calc = r1 * r_delta * r_delta * r_delta;
  error = RotError(r2, r2_calc);
  if (fabs(error) > 1e-14) {
    cout << "Error:  RotError(r2, r2_calc) should be zero.  Instead it's: " << error
         << endl;
    status++;
  }

  double error_ratio = 1.0;
  Vec3 v1 = Vec3(1.0, -2.3, 0.1);
  Hom h1 = HomFromRotVec(r1, v1);
  Hom h1_scaled;

  // HomScale(h, 0.0) should be identity
  h1_scaled = HomScale(h1, 0.0);
  error = HomError(h1_scaled, Hom::Identity(), error_ratio);
  if (fabs(error) > 1e-14) {
    cout << "Error:  HomScale(h1, 0.0) should be identity.  Error is: " << error
         << endl;
    status++;
  }

  // HomScale(h, 1.0) should be h
  h1_scaled = HomScale(h1, 1.0);
  error = HomError(h1_scaled, h1, error_ratio);
  if (fabs(error) > 1e-14) {
    cout << "Error:  HomScale(h1, 1.0) should be h1.  Error is: " << error << endl;
    status++;
  }

  //  Note: hs = HomScale(h, 0.5) does not give h = hs * hs
  Hom h2 = HomFromAxVec(Vec3(0.2, -0.4, 0.6), Vec3(0.4, 0.8, -0.2));
  scale = 0.3;
  Hom h2a = HomScale(h2, scale);
  Hom h2b = HomInterp(Hom::Identity(), h2, scale);
  error = HomError(h2a, h2b, error_ratio);
  if (fabs(error) > 1e-14) {
    cout << "Error:  Scaling and interploating with identity should be the same. "
            "Error is: " << error << endl;
    status++;
  }

  // Make sure all tests passed
  if (status == 0) {
    cout << "All tests passed.\n" << endl;
  } else {
    cout << "### Warning: " << status << " tests failed! ###" << endl;
  }

  return status;
}
