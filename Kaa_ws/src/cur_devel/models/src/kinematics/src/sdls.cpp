#include "kinematics/arm.h"
#include <eigen3/Eigen/Dense>
#include "kinematics/sdls.h"

using std::cout;
using std::endl;
namespace kinematics {

sdls::sdls() {}

sdls::sdls(Mat J, Vec e) {
  J_ = J;
  e_ = e;
  n_rows_ = J.rows();
}

Vec sdls::solve(void) {
  // Decomposition
  Eigen::JacobiSVD<Mat> svd(J_, Eigen::ComputeFullU | Eigen::ComputeFullV);
  Mat s = svd.singularValues();
  Mat U = svd.matrixU();
  Mat V = svd.matrixV().transpose();  // transpose to match python implementation

  // Mat R = U*S*V;

  int nd = V.rows() - n_rows_;
  if (nd > 0) {
    null_ = V.bottomRows(nd);
  }

  Vec gamma_max;
  gamma_max.setOnes(n_rows_);
  gamma_max *= g_max_;

  // solve sdls
  Vec rho(J_.cols(), 1);
  for (uint i = 0; i < J_.cols(); i++) {
    rho(i) = J_.middleCols(i, 1).norm();
  }

  Vec phi_sum(J_.cols(), 1);
  phi_sum.setZero();

  int count = 0;
  for (int i = 0; i < s.size(); i++) {
    if (s(i) != 0) {
      count += 1;
    }
  }

  for (int i = 0; i < count; i++) {
    Vec Ui(U.rows(), 1);
    Ui = U.middleCols(i, 1);
    double alpha = Ui.transpose() * e_;
    double Ni = 1.0;  // <-- orthonormal decomposition always yields 1 in U_mags

    double sigi_inv = 1.0 / s(i);
    Vec vv(V.cols(), 1);
    vv = V.middleRows(i, 1).transpose();
    double Mi = 0;

    for (int j = 0; j < num_effectors_; j++) {
      Vec temp = vv.cwiseAbs();

      double Mil = temp.transpose() * rho;
      Mil *= sigi_inv;
      Mi += Mil;
    }

    double gamma_i = std::min(1.0, Ni / Mi) * gamma_max(i);
    bool norm_type = false;

    Vec phi_i = clamp(sigi_inv * alpha * vv, gamma_i, norm_type);
    phi_sum += phi_i;
  }
  return clamp(phi_sum, g_max_, false);
}
}  // end kin namespace
