#include "kinematics/types.h"
#include "kinematics/link.h"
#include "kinematics/arm.h"
#include "kinematics/partials.h"

#include "kinematics/arm_testing.h"

#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <kinematics/joint_base.h>
#include <iostream>

using kinematics::Arm;
using kinematics::Vec;

using std::cout;
using std::endl;

void write_arm_checks(FILE* fd, Arm* arm) {

  fprintf(fd, "#include <stdio.h>\n");
  fprintf(fd, "#include \"kinematics/types.h\"\n");
  fprintf(fd, "#include \"kinematics/arm.h\"\n");
  fprintf(fd, "#include \"kinematics/arm_testing.h\"\n");
  fprintf(fd, "\n");

  fprintf(fd, "using kinematics::Vec;\n");
  fprintf(fd, "using kinematics::Mat;\n");
  fprintf(fd, "\n");

  Vec angs(arm->num_dof_);
  Vec vels(arm->num_dof_);

  for (int config_num = 0; config_num < 2; config_num++) {
    // setup_arm(config_num, arm, &angs, &vels);
    setup_arm(config_num, arm);
    write_all_checks(fd, config_num, arm);
  }
}

int main() {

  Arm arm;
  FILE* fd;

  // old arm stuff
  fd = fopen("arm_old_test_data.h", "w");

  fprintf(fd, "#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_ARM_OLD_TEST_DATA_H\n");
  fprintf(fd, "#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_ARM_OLD_TEST_DATA_H\n");
  fprintf(fd, "\n");

  arm = make_arm_old();
  write_arm_checks(fd, &arm);
  write_do_checks(fd);

  fprintf(fd, "#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_ARM_OLD_TEST_DATA_H\n");
  fclose(fd);

  // new arm stuff
  fd = fopen("arm_new_test_data.h", "w");

  fprintf(fd, "#ifndef SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_ARM_NEW_TEST_DATA_H\n");
  fprintf(fd, "#define SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_ARM_NEW_TEST_DATA_H\n");
  fprintf(fd, "\n");

  arm = make_arm_new();
  write_arm_checks(fd, &arm);
  write_do_checks(fd);

  fprintf(fd, "#endif  // SOFTWARE_SRC_KINEMATICS_INCLUDE_KINEMATICS_ARM_NEW_TEST_DATA_H\n");
  fclose(fd);
  return 0;
}
