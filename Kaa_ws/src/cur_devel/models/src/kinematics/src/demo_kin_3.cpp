#include <stdio.h>
#include <iostream>

#include "kinematics/types.h"
#include "kinematics/arm.h"

using kinematics::Arm;
using kinematics::Vec;
using kinematics::Mat;

using kinematics::Frame;
using kinematics::Vec3;
using kinematics::Vec6;
using kinematics::Hom;
using kinematics::Rot;

using std::cout;
using std::endl;

int main() {
    //This shows how an arm can push on the ground
  Vec lengths = Vec(2);
  lengths << 1,1;
  double height = 0.01;

  Arm arm(height, lengths, kinematics::kJointTypeCC1D);
  Vec angs_arm(arm.num_dof_);
  angs_arm.setZero();
  // angs_arm(0) = 90*3.14159 / 180;  // bend 45 degrees
  // angs_arm(1) = 45*3.14159 / 180;  // bend 45 degrees
  // angs_arm(2) = 45*3.14159 / 180;  // bend 45 degrees
  // angs_arm(3) = 90*3.14159 / 180;  // bend 45 degrees
  arm.setJointAngs(angs_arm);
  Vec torque_external(arm.num_dof_);
  torque_external.setZero();

  for (unsigned int i = 0; i < arm.links_.size(); i++) {
    // Rotate actuators 90 degrees relative to eachother: very important
    Hom hom;
    kinematics::Vec3 w;
    w << 0, 0, -M_PI / 2.0;
    hom = kinematics::HomRotate(w);
    arm.links_[i].g_top_end_ *= hom;
  }
  //set mount
  Vec3 th;
  th << -M_PI / 2.0, 0.0, 0.0;
  Rot rot = kinematics::AxToRot(th);
  Vec3 z;
  z << 0, 0, .5;
  Hom g_world_arm;
  g_world_arm.setIdentity();
  g_world_arm.topLeftCorner(3, 3) = rot;
  g_world_arm.topRightCorner(3, 1) = z;
  arm.g_world_arm_ = g_world_arm;

  arm.calcJac();
  for (unsigned int i = 0; i < arm.links_.size(); i++) {
      cout << "transforms world - link -" << i << endl;
      cout << arm.links_[i].g_world_end_ << endl;
  }

  Vec3 direction;
  direction << 0,0,-1;
  double maxforce;
  maxforce = arm.calcMaxForceLimitTorque(direction);
  cout << "Max Force " << endl << maxforce << endl << endl;

  //Apply to last link
  int link_num = arm.links_.size()-1;
  // choose a frame
  // Frame frame = Frame::Spatial;
  Frame frame = Frame::Hybrid;
  // Frame frame = Frame::Body;

  Vec6 wrench = Vec6::Zero();
  // wrench(0) = maxforce;
  wrench(2) = -maxforce;  // z direction Apply the max force downwards and check joint torques

  //Define a offset for torque relative to the frame you choose!
  Hom offset = Hom::Identity();
  // HYBRID OFFSET
  offset.topRightCorner(3,1) =
  arm.links_[link_num].g_world_end_.topRightCorner(3,1)-arm.links_[link_num].g_world_com_.topRightCorner(3,1);

  // BODY OFFSET
  // offset(2, 3) = lengths[link_num]/2.0;
  // cout << "offset" << endl << offset << endl;

  cout << "wrench" << endl;
  cout << wrench << endl;
  arm.setTorqueExternal(link_num, frame, wrench, offset);
  cout << "joint torques:\n" << arm.calcTorqueJointStatic(Vec6::Zero())<<endl << endl;
  cout << "torque_gravity:\n" << arm.getTorqueGravity() << endl << endl;
  cout << "torque external:\n"
       << "base\n" << arm.getTorqueExternal() << "\ntop" << endl;
  cout << endl;

  return 0;
}
