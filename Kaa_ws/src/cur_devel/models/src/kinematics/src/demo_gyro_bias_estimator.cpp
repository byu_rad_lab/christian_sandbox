#include "kinematics/types.h"
#include "kinematics/arm.h"
#include "kinematics/gyro_bias_estimator.h"

#include <stdio.h>

#include <iostream>

using kinematics::Quat;
using kinematics::Arm;
using kinematics::Vec;
using kinematics::Vec3;
using kinematics::Rot;
using kinematics::Mat;
using kinematics::GyroBiasEstimator;

using std::cout;
using std::endl;

int main() {

  Arm arm = Arm();
  GyroBiasEstimator est = GyroBiasEstimator();
  est.SetKpRot(0.4);
  est.SetKiRot(0.02);

  Vec3 omega;
  omega << 0.15, 0.0, 0.0;
  double dt = 0.01;

  cout << "q_vec: \n" << est.GetQAsAx() << endl;
  cout << "bias_: \n" << est.GetBias() << endl;

  for (int i = 0; i < 10000; i++) {
    est.PredictGyro(omega, dt);
    est.CorrectRot(Rot::Identity());
  }
  cout << "q_vec: \n" << est.GetQAsAx() << endl;
  cout << "bias_: \n" << est.GetBias() << endl;

  arm.checkMatSizes();  // verify again
}
