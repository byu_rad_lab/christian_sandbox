#include <kinematics/joint_base.h>
#include <stdio.h>

#include <iostream>

#include "yaml-cpp/yaml.h"

#include "kinematics/types.h"
#include "kinematics/link.h"
#include "kinematics/arm.h"
#include "kinematics/yaml.h"

using std::cout;
using std::endl;

int main(int argc, char* argv[]) {
  YAML::Node node = kinematics::LoadYAMLCommandLine(argc, argv);

  cout << "### Parsed node: ###\n" << node << endl << "### End parsed node. ###\n\n";

  kinematics::Arm arm = node.as<kinematics::Arm>();

  cout << "arm.g_world_arm_ = \n" << arm.g_world_arm_ << endl;
  printf("arm has %lu links.\n", arm.links_.size());
  cout << "link 1 g_world_com_ is \n" << arm.links_[1].g_top_com_ << endl;

  return 0;
}
