#include "kinematics/joint_limits.h"

namespace kinematics {

// JointLimits::JointLimits(double num_dof) { num_dof_ = num_dof; }

void JointLimits::setLimitsScalar(double limit) {
  setLimitsVector(-limit * Vec::Ones(num_dof_), limit * Vec::Ones(num_dof_));
}

void JointLimits::setLimitsVector(const Vec& limits_lower, const Vec& limits_upper) {
  // TODO: check limits_lower, limits_upper are equal to num_dof
  //       should throw if not
  bounds_lower_ = limits_lower;
  bounds_upper_ = limits_upper;

  a_.resize(2 * num_dof_, num_dof_);
  a_.topRows(num_dof_) = -Mat::Identity(num_dof_, num_dof_);
  a_.bottomRows(num_dof_) = Mat::Identity(num_dof_, num_dof_);

  b_.resize(2 * num_dof_);
  b_.head(num_dof_) = -limits_lower;
  b_.tail(num_dof_) = limits_upper;

  c_ = Vec::Zero(num_dof_);
}

Vec JointLimits::checkLimits(const Vec& joint_angles) { return a_ * joint_angles - b_; }

void JointLimits::setLimitsCircular(double radius,
                                    const Vec& center,
                                    int num_constraints,
                                    double phase) {

  bounds_lower_ = center + radius * Vec::Ones(num_dof_);
  bounds_upper_ = center - radius * Vec::Ones(num_dof_);

  // TODO: check that num_dof = 2
  a_.resize(num_constraints, num_dof_);
  b_.resize(num_constraints);
  c_ = center;  // Should check that the size is right

  double theta;
  for (int i = 0; i < num_constraints; i++) {
    theta = 2.0 * M_PI * static_cast<double>(i) / static_cast<double>(num_constraints) +
            phase;
    a_.row(i) << cos(theta), sin(theta);
  }
  b_ = radius * Vec::Ones(num_constraints) + a_ * center;
}

Vec JointLimits::getAngsRandom(void) {
  Vec angs = Vec(num_dof_);
  Vec check = Vec(b_.size());
  int is_valid;
  while (1) {
    // set the angles randomly between 0 and 1
    // then add the average plus angs times the spread
    angs.setRandom();
    angs = 0.5 * (bounds_upper_ + bounds_lower_ +
                  (angs.cwiseProduct(bounds_upper_ - bounds_lower_)));

    // rejection sampling:  make sure A x <= b is satisfied
    is_valid = true;
    check = a_ * angs - b_;
    for (int i = 0; i < check.size(); i++) {
      if (check(i) > 0) {
        is_valid = false;
      }
    }

    if (is_valid) {
      break;
    }
  }
  return angs;
}

}  // end kin namespace
