#include <kinematics/joint_base.h>
#include "kinematics/types.h"
#include "kinematics/link.h"
#include "kinematics/arm.h"

#include "kinematics/BenchTimer.h"

#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#include <iostream>

using kinematics::JointBase;
using kinematics::Link;
using kinematics::Arm;
using kinematics::Vec;
using kinematics::Mat;

using std::cout;
using std::endl;

EIGEN_DONT_INLINE void testDynamicsMats(Arm* arm) { arm->calcDynamicsMats(); }

typedef unsigned long long timestamp_t;

// static timestamp_t get_timestamp() {
//   struct timeval now;
//   gettimeofday(&now, NULL);
//   return now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
// }

int main() {
  Vec lengths = Vec(4);
  lengths << 0.3, 0.05, 0.3, 0.05;
  double height = 0.2;

  Arm arm(height, lengths, kinematics::kJointTypeCCC);

  Vec angs_arm(arm.num_dof_);

  cout << "arm.num_dof_ = " << arm.num_dof_ << endl;
  angs_arm.setZero();
  angs_arm(2) = 0.1;
  arm.setJointAngs(angs_arm);

  Vec torque_external(arm.num_dof_);
  torque_external.setZero();

  int reps = (int)1e3;
  int tries = 100;
  // int reps = (int)1e2;
  // int tries = 10;

  Eigen::BenchTimer t0, t1, t2;
  Eigen::BenchTimer t3, t4, t5;
  BENCH(t0, tries, reps, arm.calcJointAccels(torque_external));
  BENCH(t1, tries, reps, arm.calcDynamicsMats());
  BENCH(t2, tries, reps, testDynamicsMats(&arm));
  BENCH(t3, tries, reps, arm.calcFK());
  cout << "calcJointAccels best: " << t0.best() / reps * 1e6 << " us" << endl;
  cout << "calcDynamicsMats best: " << t1.best() / reps * 1e6 << " us" << endl;
  cout << "testDynamicsMats best: " << t2.best() / reps * 1e6 << " us" << endl;
  cout << "FK best: " << t3.best() / reps * 1e6 << " us" << endl;

  return 0;
}
