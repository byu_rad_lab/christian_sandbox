#include "kinematics/types.h"

#include <stdio.h>

#include <iostream>

using kinematics::Vec3;
using kinematics::Rot;
using kinematics::Hom;
using kinematics::HomError;
using kinematics::Vec3Error;
using kinematics::RotError;

using std::cout;
using std::endl;

int main() {

  int status = 0;

  double error;

  // Test vector error

  Vec3 v1, v2;
  v1 << 1.0, 2.0, -3.0;
  v2 << 1.0, 1.0, -3.0;

  error = Vec3Error(v1, v1);
  if (error > 1e-14) {
    cout << "Error:  Vec3Error(v1, v1) should be zero.  Instead it's: " << error << endl;
    status++;
  }

  error = Vec3Error(v1, v2);
  if (fabs(error - 1.0) > 1e-14) {
    cout << "Error:  Vec3Error(v1, v2) should be 1.0.  Instead it's: " << error << endl;
    status++;
  }

  // Test rotation error
  Rot r1, r2;

  r1 = kinematics::RotFromAx(v1);
  error = RotError(r1, r1);
  if (fabs(error) > 1e-14) {
    cout << "Error:  RotError(r1, r1) should be zero.  Instead it's: " << error << endl;
    status++;
  }

  Vec3 v3;
  v3 << 0.0, M_PI / 2.0, 0.0;  // 90 degree rotation, should be error of 1.0
  r2 = kinematics::RotFromAx(v3);
  error = RotError(r1, r1 * r2);
  if (fabs(error - 1.0) > 1e-14) {
    cout << "Error:  Vec3Error(r1, r1 * r2) should be 1.0.  Instead it's: " << error << endl;
    status++;
  }

  error = RotError(r1 * r2, r1);
  if (fabs(error - 1.0) > 1e-14) {
    cout << "Error:  Vec3Error(r1 * r2, r1) should be 1.0.  Instead it's: " << error << endl;
    status++;
  }

  // Test homogeneous error
  double error_ratio = 1.0;  // 1 meter = 90 degrees
  Hom h1, h2;
  h1 = kinematics::HomFromRotVec(r1, v1);
  h2 = kinematics::HomFromRotVec(r1 * r2, v2);
  error = HomError(h1, h1, error_ratio);
  if (error > 1e-14) {
    cout << "Error:  HomError(h1, h1) should be zero.  Instead it's: " << error << endl;
    status++;
  }

  error = HomError(h1, h2, error_ratio);
  if (fabs(error - sqrt(2)) > 1e-14) {
    cout << "Error:  HomError(h1, h2, 1.0) should be sqrt(2).  Instead it's: " << error << endl;
    status++;
  }

  // Make sure all tests passed
  if (status == 0) {
    cout << "All tests passed.\n" << endl;
  } else {
    cout << "### Warning: " << status << " tests failed! ###" << endl;
  }
  return status;

}
