#include <stdio.h>
#include <iostream>

#include "kinematics/types.h"
#include "kinematics/gravity_solver.h"
#include "kinematics/joint_ccc.h"

// not a very thorough test of joint solver

using kinematics::GravitySolver;
using kinematics::Vec3;
using kinematics::Vec;
using kinematics::JointCCC;
using kinematics::JointPtr;

int main() {

  JointCCC joint = JointCCC(0.20);
  GravitySolver gs = GravitySolver(joint);

  Vec3 a0, a1;
  // a0 << 0.0, 0.0, 1.0;
  a0 << 0.0, 1.0, 0.0;
  // a0 << 1.0, 0.0, 0.0;

  a1 << 0.0, 0.0, 1.0;
  // a1 << 0.0, 1.0, 0.0;

  Vec angs(gs.joint_->num_dof_);
  angs.setZero();
  // angs(0) = 100.0;

  gs.solve(a0, a1, &angs);

  std::cout << "angs = \n" << angs << std::endl;
  return 0;
}
