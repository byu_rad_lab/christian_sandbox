#include <stdio.h>
#include <iostream>

#include "kinematics/types.h"
#include "kinematics/arm.h"

using kinematics::Arm;
using kinematics::Vec;
using kinematics::Mat;

using kinematics::Frame;
using kinematics::Vec6;
using kinematics::Hom;

using std::cout;
using std::endl;

int main() {
  Vec lengths = Vec(4);
  lengths << 0.3, 0.05, 0.3, 0.05;
  double height = 0.2;

  Arm arm(height, lengths);
  arm.setJointStateZero();
  cout << "arm.num_dof_ = " << arm.num_dof_ << endl;
  Vec angs_arm(arm.num_dof_);
  angs_arm.setZero();
  angs_arm(0) = -0.1;
  angs_arm(1) = 0.15;
  angs_arm(2) = 0.0;
  angs_arm(3) = 0.0;
  angs_arm(4) = -0.1;
  angs_arm(5) = -0.2;
  angs_arm(6) = 0.2;
  angs_arm(7) = -0.3;
  arm.setJointAngs(angs_arm);

  Vec torque_external = Vec::Zero(arm.num_dof_);
  arm.calcJointAccels(torque_external);

  Arm arm2 = arm;  // a test of copying

  bool print_all = true;
  // bool print_all = false;

  if (print_all) {

    cout << "arm.jac_spatial_ = \n" << arm.jac_spatial_ << endl;
    for (auto part = arm.partials_.begin(); part < arm.partials_.end(); part++) {
      printf("\n\n ### jac_spatial_ partial derivative %d ###\n",
             (int)std::distance(arm.partials_.begin(), part));
      cout << part->jac_spatial_ << endl;
    }

    for (auto& link : arm.links_) {
      cout << "link.joint_->g_bot_top_ = \n" << link.joint_->g_bot_top_ << endl;
    }

    for (auto& link : arm.links_) {
      cout << "link.joint_->jac_ = \n" << link.joint_->jac_ << endl;
    }

    cout << "Spatial jacobian is:\n" << arm.jac_spatial_ << endl;

    cout << "Printing g_world_bot_\n";
    for (auto& link : arm.links_) {
      cout << link.g_world_bot_ << endl
           << endl;
    }

    cout << "Printing g_world_top_\n";
    for (auto& link : arm.links_) {
      cout << link.g_world_top_ << endl
           << endl;
    }

    cout << "Printing g_world_com_\n";
    for (auto& link : arm.links_) {
      cout << link.g_world_com_ << endl
           << endl;
    }

    cout << "Printing g_world_end_\n";
    for (auto& link : arm.links_) {
      cout << link.g_world_end_ << endl
           << endl;
    }

    cout << "Printing jac_body_\n";
    for (auto& link : arm.links_) {
      cout << link.jac_body_ << endl
           << endl;
    }

    for (auto link = arm.links_.begin(); link < arm.links_.end(); link++) {
      printf("\n\n ### Link %d ###\n", (int)std::distance(arm.links_.begin(), link));
      for (auto part = link->partials_.begin(); part < link->partials_.end(); part++) {
        cout << "\npart->g_world_bot_ = \n" << part->g_world_bot_ << endl;
      }
    }

    for (auto link = arm.links_.begin(); link < arm.links_.end(); link++) {
      printf("\n\n ### Link %d ###\n", (int)std::distance(arm.links_.begin(), link));
      for (auto part = link->partials_.begin(); part < link->partials_.end(); part++) {
        cout << "\npart->g_world_com_ = \n" << part->g_world_com_ << endl;
      }
    }

    for (auto link = arm.links_.begin(); link < arm.links_.end(); link++) {
      printf("\n\n ### Link %d ###\n", (int)std::distance(arm.links_.begin(), link));
      for (auto part = link->partials_.begin(); part < link->partials_.end(); part++) {
        cout << "\npart->ad_world_bot_ = \n" << part->ad_world_bot_ << endl;
      }
    }

    for (auto link = arm.links_.begin(); link < arm.links_.end(); link++) {
      printf("\n\n ### Link %d ###\n", (int)std::distance(arm.links_.begin(), link));
      for (auto part = link->partials_.begin(); part < link->partials_.end(); part++) {
        cout << "\npart->ad_inv_world_com_ = \n" << part->ad_inv_world_com_ << endl;
      }
    }

    for (auto link = arm.links_.begin(); link < arm.links_.end(); link++) {
      printf("\n\n ### Link %d ###\n", (int)std::distance(arm.links_.begin(), link));
      for (auto part = link->partials_.begin(); part < link->partials_.end(); part++) {
        cout << "part->ad_world_bot_ = \n" << part->ad_world_bot_ << endl;
      }
    }

    for (auto link = arm.links_.begin(); link < arm.links_.end(); link++) {
      printf("\n\n ### Link %d ###\n", (int)std::distance(arm.links_.begin(), link));
      for (auto part = link->partials_.begin(); part < link->partials_.end(); part++) {
        cout << "\npart->jac_body_ = \n" << part->jac_body_ << endl;
      }
    }
  }

  cout << "##### torque stuff #####" << endl;
  arm.setJointStateZero();

  angs_arm.setZero();
  // angs_arm(3) = -3.14159 / 2.0;  // bend 90 degrees
  arm.setJointAngs(angs_arm);

  arm.calcJac();
  arm.calcTorqueGravity();

  int link_num = 3;

  // choose a frame
  // Frame frame = Frame::Spatial;
  // Frame frame = Frame::Hybrid;
  Frame frame = Frame::Body;

  Vec6 wrench = Vec6::Zero();
  wrench(0) = 1.0;  // x direction
  Hom offset = Hom::Identity();
  // offset(2, 3) = 2.0;

  arm.setTorqueExternal(link_num, frame, wrench, offset);
  // arm.setForceAtEE(frame, wrench.head(3));
  // arm.addForceAtEE(frame, wrench.head(3));  // double the force
  

  cout << "torque external:\n"
       << "base\n" << arm.getTorqueExternal() << "\ntop" << endl;
  // cout << "joint torques:\n" << arm.calcTorqueJointStatic() << endl;
  cout << "joint torques:\n" << arm.calcTorqueJointStatic(Vec6::Zero()) << endl;

  return 0;
}
