#include "kinematics/arm.h"
#include "kinematics/arm_testing.h"
#include "kinematics/arm_new_test_data.h"

#include <stdio.h>

using kinematics::Arm;

// has to be here to get do_checks from correct file
void test_arm(Arm* arm) {
  for (int config_num = 0; config_num < 2; config_num++) {
    setup_arm(config_num, arm);
    do_checks(config_num, arm);
  }
}

int main() {
  printf("Testing NEW arm:\n");
  Arm arm_new = make_arm_new();
  test_arm(&arm_new);

  return 0;
}
