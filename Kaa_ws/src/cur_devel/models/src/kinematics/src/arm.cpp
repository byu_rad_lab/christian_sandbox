#include "kinematics/arm.h"
#include <algorithm>

#include <stdio.h>
#include <iostream>

#include "kinematics/joint_cc1d.h"
#include "kinematics/joint_ccc.h"
#include "kinematics/joint_rot.h"
#include "kinematics/sdls.h"

using std::cout;
using std::endl;

namespace kinematics {

Arm::Arm() {
  g_world_arm_.setIdentity();
  num_dof_ = 0;
}

Arm::Arm(double height, Vec lengths) : Arm(height, lengths, kJointTypeCCC) {}

Arm::Arm(double height, Vec lengths, JointType type) : Arm() {
  for (int i = 0; i < lengths.size(); i++) {

    Link link;
    switch (type) {
      case kJointTypeCCC:
        link = Link(JointCCC(height));
        break;
      case kJointTypeCC1D:
        link = Link(JointCC1D(height));
        break;
      case kJointTypeRot:
        link = Link(JointRot(height));
        printf("Warning: this produces an arm of coaxial rotatoinal joints.\n");
        break;
      default:
        throw("Warning: joint type not handled.");
    }

    link.setGTopEndXYZ(0.0, 0.0, lengths[i]);

    // heuristic inertia matrix, matches Python implementation and checks
    double m = 25.0 * lengths[i];
    double r = 0.5 * 0.1651;  // cylinder radius
    double Ix = m * (3.0 * r * r + lengths[i]) / 12.0;
    double Iy = m * (3.0 * r * r + lengths[i]) / 12.0;
    double Iz = m * (r * r) / 2.0;
    link.setInertiaSelf(m, Vec3(0.0, 0.0, lengths[i] / 2.0), Vec3(Ix, Iy, Iz));

    addLink(link);
    g_world_arm_ = Hom::Identity();
  }
}

Arm::~Arm() {}

void Arm::resize(void) {
  joint_angs_.resize(num_dof_);
  joint_vels_.resize(num_dof_);
  joint_accs_.resize(num_dof_);

  torque_gravity_.resize(num_dof_);
  torque_gravity_leg_.resize(num_dof_);
  torque_external_.resize(num_dof_);
  torque_sum_.resize(num_dof_);
  torque_limit_lower_.resize(num_dof_);
  torque_limit_upper_.resize(num_dof_);
  force_limit_.resize(num_dof_);

  jac_spatial_.resize(6, num_dof_);
  jac_body_.resize(6, num_dof_);

  // stiffness_body_.resize(6, 6);
  // stiffness_hybrid_.resize(6, 6);
  flexibility_body_.resize(6, 6);
  flexibility_hybrid_.resize(6, 6);
  flexibility_global_.resize(num_dof_, num_dof_);

  inertia_mat_.resize(num_dof_, num_dof_);
  coriolis_mat_.resize(num_dof_, num_dof_);

  for (auto& part : partials_) {
    part.resize(num_dof_);
  }

  real_joint_idx_.resize(num_dof_);
  // Keeps track of real joint indices: must be manually overloaded after
  //   initialization.
  for (int i = 0; i < num_dof_; i++) {
    real_joint_idx_[i] = i;
  }
  // default
  jac_spatial_real_.resize(6, num_dof_);
  jac_body_real_.resize(6, num_dof_);
  jac_hybrid_real_.resize(6, num_dof_);

  setDataZero();
}

void Arm::setDataZero(void) {
  joint_angs_.setZero();
  joint_vels_.setZero();
  joint_accs_.setZero();

  jac_body_.setZero();
  jac_hybrid_.setZero();

  torque_gravity_.setZero();
  torque_gravity_leg_.setZero();
  torque_external_.setZero();
  torque_sum_.setZero();
  torque_limit_lower_.setZero();
  torque_limit_upper_.setZero();
  force_limit_.setZero();

  torque_sum_passive_.setZero();

  coriolis_mat_.setZero();
  inertia_mat_.setZero();
}

void Arm::addLink(const Link& link) {
  links_.push_back(link);
  links_.back().resize(num_dof_);  // number of DOF proximal

  num_dof_ += link.joint_->num_dof_;
  if (num_dof_ > kNumDofMax) {
    printf("### Error:  num_dof_ = %d > kNumDofMax = %d", num_dof_, kNumDofMax);
    printf("Aborting.");
    throw;
  }

  for (int i = 0; i < link.joint_->num_dof_; i++) {
    partials_.push_back(ArmPartials());
  }

  resize();
}

// accessors

void Arm::setGEndToolVec(double x, double y, double z) {
  g_end_tool_.setIdentity();
  g_end_tool_(0, 3) = x;
  g_end_tool_(1, 3) = y;
  g_end_tool_(2, 3) = z;
}

void Arm::setGWorldArm(const Hom& hom) { g_world_arm_ = hom; }

Hom Arm::getGWorldTool(void) const { return g_world_tool_; }

Hom Arm::getGArmTool(void) const { return HomInv(g_world_arm_) * g_world_tool_; }

void Arm::setJointAngs(Vec angs) {
  joint_angs_ = angs;
  for (auto& link : links_) {
    link.joint_->setAngs(angs.segment(link.num_dof_prox_, link.joint_->num_dof_));
  }
}
//
// Sets Valid Angles (U's)  V's and Rotation about Z is set to 0
void Arm::setJointAngsReal(Vec angs) {
  Vec angs_arm(num_dof_);
  angs_arm.setZero();
  for (int i = 0; i < real_joint_idx_.size(); i++) {
    angs_arm(real_joint_idx_[i]) = angs[i];
  }
  setJointAngs(angs_arm);
}

Vec Arm::getJointAngsReal() {
  Vec angs_arm(real_joint_idx_.size());
  angs_arm.setZero();
  for (int i = 0; i < real_joint_idx_.size(); i++) {
    angs_arm[i] = joint_angs_(real_joint_idx_[i]);
  }
  return angs_arm;
}

void Arm::setJointAngsZero(void) { setJointAngs(Vec::Zero(num_dof_)); }

void Arm::setJointAngsDelta(Vec angs) {
  joint_angs_ += angs;
  setJointAngs(joint_angs_);
}

void Arm::setJointAngsRandom(void) {
  for (auto& link : links_) {
    link.joint_->setAngsRandom();
    joint_angs_.segment(link.num_dof_prox_, link.joint_->num_dof_) = link.joint_->angs_;
  }
}

void Arm::resetJointAngsWithinLimit(void) {
  for (auto& link : links_) {
    link.joint_->resetAngsWithinLimit();
    joint_angs_.segment(link.num_dof_prox_, link.joint_->num_dof_) = link.joint_->angs_;
  }
}

void Arm::setJointVels(Vec vels) {
  joint_vels_ = vels;
  for (auto& link : links_) {
    link.joint_->setVels(vels.segment(link.num_dof_prox_, link.joint_->num_dof_));
  }
}

void Arm::setJointVelsZero(void) { setJointVels(Vec::Zero(num_dof_)); }

void Arm::setJointVelsRandom(void) {
  for (auto& link : links_) {
    link.joint_->setVelsRandom();
    joint_vels_.segment(link.num_dof_prox_, link.joint_->num_dof_) = link.joint_->vels_;
  }
}

void Arm::setJointStateZero(void) {
  setJointAngsZero();
  setJointVelsZero();
}

void Arm::setJointStateRandom(void) {
  setJointAngsRandom();
  setJointVelsRandom();
}

// void Arm::setStiffnessBody(Mat stiffness) { stiffness_body_ = stiffness; }

// void Arm::setStiffnessHybrid(Mat stiffness) { stiffness_hybrid_ = stiffness; }

void Arm::setFlexibilityBody(Mat flexibility) { flexibility_body_ = flexibility; }

void Arm::setFlexibilityHybrid(Mat flexibility) { flexibility_hybrid_ = flexibility; }

void Arm::calcFK(void) {
  for (auto link = links_.begin(); link != links_.end(); link++) {
    // set base of link
    if (link == links_.begin()) {
      link->g_world_bot_ = g_world_arm_;
    } else {
      link->g_world_bot_ = (link - 1)->g_world_end_;
    }

    link->calcFK();
  }
  g_world_tool_ = links_.back().g_world_end() * g_end_tool_;
}

void Arm::calcJac(void) {
  calcFK();
  for (auto& link : links_) {
    link.calcJac(&jac_spatial_);  // populates jac spatial
  }
  calcJacBody();
  calcJacHybrid();
  calcJacHybridInverse();

  // Keeps track of real/fictional joint Jacobians
  // setJacReal();
}

void Arm::calcJacBody(void) {
  ad_inv_world_tool_ = AdjFromHom(HomInv(g_world_tool_));
  jac_body_ = ad_inv_world_tool_ * jac_spatial_;
}

void Arm::calcJacHybrid(void) {
  ad_hybrid_ << g_world_tool_.topLeftCorner(3, 3), Rot::Zero(), Rot::Zero(),
      g_world_tool_.topLeftCorner(3, 3);
  jac_hybrid_ = ad_hybrid_ * jac_body_;
}

void Arm::calcJacHybridInverse(void) {
    Hom g_tool_world = HomInv(g_world_tool_);
    Rot R = g_tool_world.topLeftCorner<3,3>();
    Vec3 p = g_tool_world.topRightCorner<3,1>();
    Rot phat = VecHat(p);
    ad_inverse_ << R, -phat*R, Rot::Zero(), R;
    jac_hybrid_inverse_ = ad_inverse_ * jac_hybrid_;
}

void Arm::calcInertiaMat(void) {
  calcJac();
  inertia_mat_.setZero();
  // Each link does M += J.T * m * J, where:
  //   J is link body Jacobian
  //   m is link inertia matrix
  for (auto& link : links_) {
    link.augmentInertiaMat(&inertia_mat_);
  }
}

void Arm::calcDynamicsMats(void) {
  calcInertiaMat();

  calcDerivJacSpatial();
  calcDerivJacBody();
  calcDerivInertiaMat();

  calcCoriolisMat();
}

void Arm::calcMass(void) {
    mass_ = 0.0;
    for (uint32_t i = 0; i < links_.size(); ++i)
    {
        mass_ = mass_ + links_[i].inertia_self_.mass;
    }
}

Vec Arm::calcJointAccels(const Vec& torque_joint) {
  calcDynamicsMats();
  calcTorqueGravity();

  torque_sum_ = -coriolis_mat_ * joint_vels_  // "coriolis torque"
                + torque_gravity_             //
                + torque_external_            //
                + torque_joint;

  joint_accs_ = inertia_mat_.ldlt().solve(torque_sum_);

  // other methods:
  // joint_accels_ = inertia_mat_.colPivHouseholderQr().solve(torque_sum_);
  // joint_accels_ = inertia_mat_.fullPivHouseholderQr().solve(torque_sum_);
  return joint_accs_;
}

Vec Arm::calcTorqueJoint(const Vec& joint_accels) {
  calcDynamicsMats();
  calcTorqueGravity();
  // torque_external_.setZero();  // not implemented

  Vec torque_joint;
  torque_joint =
      inertia_mat_ * joint_accels    // inertial torque = -inertia_mat_ * joint_accs
      + coriolis_mat_ * joint_vels_  // coriolis torque = - coriolis_mat_ * joint_vels
      - torque_gravity_              //
      - torque_external_;            // zero for now

  return torque_joint;
}

// adds external
Vec Arm::calcTorqueJointStatic(const Vec6& external_wrench) {
  // External Set
  Hom offset = Hom::Identity();  // Set unit wrench -> slope of jt-torque x EE force lines
  Frame frame = Frame::Hybrid;   // Torque External uses com instead of EE, adjust offset
  offset.topRightCorner(3, 1) = links_.back().g_world_end_.topRightCorner(3, 1) -
                                links_.back().g_world_com_.topRightCorner(3, 1);
  setTorqueExternal(links_.size() - 1.0, frame, external_wrench, offset);

  // Gravity
  calcTorqueGravity();
  Vec torque_joint;
  torque_joint = torque_gravity_ + torque_external_;  // no accel/vel
  return torque_joint;
}

void Arm::calcTorqueLimits(){

    //calculate the FK to make sure everything is set for the current configuration of the robot
    calcFK();

    //clear the max and min torque vectors to reset them
    max_torques_.clear();
    min_torques_.clear();

    for(uint32_t i = 0; i < links_.size(); i++)
    {
        //find the torque limits from the joint
        links_[i].joint_->calcTorqueLimits();

        for(int j = 0; j < links_[i].joint_->num_dof_; j++)
        {
            max_torques_.push_back(links_[i].joint_->torque_max_(j));
            min_torques_.push_back(links_[i].joint_->torque_min_(j));
        }

    }

}

Vec Arm::getTorqueInertial(const Vec& joint_accels) {
  return inertia_mat_ * joint_accels;
}

Vec Arm::getTorqueCoriolis(void) { return coriolis_mat_ * joint_vels_; }

// TODO:
//   deal with redundant manipulator
//   check rank first
//   add damping factor?
//   deal with forces at different links?
Vec3 Arm::calcForcesFromTorques(const Vec& torque_joint_net) {

  Link* link = &(links_.back());           // last link
  Mat jac = link->jac_hybrid_.topRows(3);  // just force component

  Hom g_com_tool = HomInv(link->g_top_com_) * link->g_top_end_ * g_end_tool_;

  Rot rot = g_com_tool.topLeftCorner(3, 3);
  // Mat JTJ = jac.transpose() * jac;
  Mat JJT = jac * jac.transpose();
  Vec3 wrench = rot.transpose() * JJT.colPivHouseholderQr().solve(jac * torque_joint_net);

  return wrench;
}

// pass in torque vector, have link populate?
void Arm::calcTorqueGravity(void) {
  torque_gravity_.setZero();
  for (auto& link : links_) {
    torque_gravity_.head(link.num_dof_total_) +=
        link.jac_hybrid_.row(2) * link.inertia_mat_.diagonal()[0] * gravity_; //This seems backwards. This seems to be the torques required to make a force in the direction of gravity, but we should be resisting it... In my code I just will use the negative of this output.
  }
}

void Arm::calcTorqueGravityLeg(void) {
    torque_gravity_leg_.setZero(); //make sure the torque vector is initially set to zero

    //iterate from the second to last link to the first link since these will be the ones affecting joint torques
    for (int i = links_.size() - 2; i>-1; i--)
    {
        //Find where the center of mass of the link is in reference to the world frame
        Hom g_world_com = links_[i].g_world_com_;
        Hom g_world_com_joint = links_[i].g_world_top_*links_[i].joint_->g_top_com_;
        Jac jac = findJacfromHomProximalLinks(g_world_com,i);
        Jac jac_joint = findJacfromHomProximalLinks(g_world_com_joint,i);

        //create the wrench that simulates the weight of the link at the center of mass
        Vec6 wrench_link, wrench_joint;
        wrench_link.setZero();
        wrench_joint.setZero();
        wrench_link(2) = -gravity_*links_[i].inertia_self_.mass; //Only in the z direction
        wrench_joint(2) = -gravity_*links_[i].joint_->mass_; //Only in the z direction

        //calculate the torque from the weight of the current link and add it to the total torques due to weight of the leg
        torque_gravity_leg_ = torque_gravity_leg_ + jac.transpose()*wrench_link + jac_joint.transpose()*wrench_joint;
    }


}

Jac Arm::findJacfromHomProximalLinks(Hom hom, int i)
{
        Vec3 t = hom.topRightCorner(3,1);
        Hom g_world_at_com = HomTranslate(t(0), t(1), t(2));

        //create the jacobian that corresponds to the point where the weight of the link is applied
        Adj adj = AdjFromHom(HomInv(g_world_at_com));

        Jac jac = adj*jac_spatial_;

        //the joints before this point will have no effect lifting this up so make those columns zero in the jacobian (remember the end effector is fixed and the base is basically like the end effector)
        for(int j = 0; j<i+1; j++)
        {
            int dof_joint = links_[j].joint_->num_dof_;
            int dof_prox = links_[j].num_dof_prox_;
            for(int k = 0; k<dof_joint; k++)
            {
                for(int l = 0; l<jac.rows(); l++)
                {
                    jac(l,dof_prox+k) = 0.0;
                }
            }
        }

        return jac;

}

Vec3 Arm::calcFrictionForceLeg(Vec torques, Vec6 applied_wrench)
{
   //get number of dof of the last joint to know how many torques are being applied at the end of the link
   int num_dof_last = links_[links_.size() - 1].joint_->num_dof_;

   //initialize the torques at the beginning of last link (the last joint) to 0
   Vec3 torque_last;
   torque_last.setZero();


    //set this torque equal to the correct torques corresponding to the last joint
   if(links_[0].joint()->type_ == kJointTypeCCC)
   {
     torque_last.head(num_dof_last) = torques.tail(num_dof_last);
   }
   if(links_[0].joint()->type_ == kJointTypeRot)
   {
     torque_last.tail(num_dof_last) = torques.tail(num_dof_last); //this is for simple leg
   }

   //declare homogeneous transformations that are needed in the equations of motion
   Hom g_world_joint_top = links_[links_.size() - 1].g_world_top_; //need to check if top or bottom (need top for the t_joint_com, etc., but may not be accurate for rotating the joint torques I think I need bottom
   Hom g_world_joint_bot = links_[links_.size() - 1].g_world_bot_; //
   Hom g_world_com_last_whole_link = links_[links_.size() - 1].g_world_com_; 
   Hom g_world_com_last_joint = g_world_joint_top*links_[links_.size() - 1].joint_->g_top_com_;
   Hom g_world_end = g_world_tool(); //need to check if top or bottom

   double mass_last_whole_link = links_[links_.size() - 1].inertia_self_.mass;
   double mass_last_joint = links_[links_.size() - 1].joint_->mass_;

   //find the various translation vectors from the homogeneous transformations
   Vec3 t_world_joint = g_world_joint_top.topRightCorner(3,1);
   Vec3 t_world_com;
  for (uint32_t i = 0; i<3; ++i )
  {
    t_world_com(i) = (mass_last_whole_link*g_world_com_last_whole_link(i,3) - mass_last_joint*g_world_com_last_joint(i,3))/(mass_last_whole_link-mass_last_joint);
  }
  // std::cout << g_world_com_last_whole_link << std::endl;
  // std::cout << g_world_com_last_joint << std::endl;
  // std::cout << "mass rest: " << mass_last_whole_link-mass_last_joint << std::endl;
  // std::cout << "t_world_com: " << t_world_com << std::endl;
   Vec3 t_world_end = g_world_end.topRightCorner(3,1);

   //find the necessary relative translations
   Vec3 t_joint_com = t_world_com - t_world_joint;
   Vec3 t_joint_end = t_world_end - t_world_joint;

   //make skew symmetric matrices from the translation vectors to be able to do cross products
   Rot t_joint_com_skew = VecHat(t_joint_com);
   Rot t_joint_end_skew = VecHat(t_joint_end);


   //declare and assign link weight, normal force and friction vectors
   Vec3 link_weight, forces_ground, torques_ground, test_friction;
   link_weight << 0.0, 0.0, (mass_last_whole_link-mass_last_joint)*gravity_;
   forces_ground = -1.0*applied_wrench.topLeftCorner(3,1);
   torques_ground = -1.0*applied_wrench.bottomRightCorner(3,1);

   //correctly rotate the joint torques into the world frame (this is where top seems more correct, but may not be)
   Vec3 torque_last_world = g_world_joint_bot.topLeftCorner(3,3)*torque_last;
   Vec3 torque_last_world_top = g_world_joint_top.topLeftCorner(3,3)*torque_last;
   //std::cout << "torque_world_bot: \n" << torque_last_world << std::endl;
   //std::cout << std::endl;
   //std::cout << "torque_world_top: \n" << torque_last_world_top << std::endl;
   //std::cout << std::endl;

   //calculate the required torque from friction to keep the leg in static equilibrium
   Vec3 required_friction_torque = -torque_last_world - t_joint_com_skew*link_weight - t_joint_end_skew*forces_ground - torques_ground;

  //  std::cout << "Required friction torques: " << required_friction_torque << std::endl;
  //  std::cout << "Normal torque: " << -t_joint_end_skew*normal_force << std::endl;
  //  std::cout << "Weight torque: " << -t_joint_com_skew*link_weight << std::endl;
  //  std::cout << "torque world: " << torque_last_world << std::endl;

   //calculate friction force and moment about z axis at top of joint
   if(links_[0].joint()->type_ == kJointTypeCCC)
   {
    Vec3 mz_coeff;
    mz_coeff << g_world_joint_top(0,2), g_world_joint_top(1,2), g_world_joint_top(2,2);

    double dem1 = mz_coeff(2)*t_joint_end(2)*t_joint_end(2) + mz_coeff(0)*t_joint_end(0)*t_joint_end(2) + mz_coeff(1)*t_joint_end(1)*t_joint_end(2);

    double dem2 = mz_coeff(0)*t_joint_end(0) + mz_coeff(1)*t_joint_end(1) + mz_coeff(2)*t_joint_end(2);

    Rot inv_Mat;
    inv_Mat(0,0) = -mz_coeff(1)*t_joint_end(0)/dem1;
    inv_Mat(1,0) = -(mz_coeff(1)*t_joint_end(1) + mz_coeff(2)*t_joint_end(2))/dem1;
    inv_Mat(2,0) = t_joint_end(0)/dem2;
    inv_Mat(0,1) = (mz_coeff(0)*t_joint_end(0) + mz_coeff(2)*t_joint_end(2))/dem1;
    inv_Mat(1,1) = (mz_coeff(0)*t_joint_end(1))/dem1;
    inv_Mat(2,1) = t_joint_end(1)/dem2;
    inv_Mat(0,2) = -mz_coeff(1)/dem2;
    inv_Mat(1,2) = mz_coeff(0)/dem2;
    inv_Mat(2,2) = t_joint_end(2)/dem2;

    Vec3 friction_and_moment;

    friction_and_moment = inv_Mat*required_friction_torque;

    //  std::cout << "friction and moment: " << friction_and_moment << std::endl;

    test_friction << friction_and_moment(0), friction_and_moment(1), friction_and_moment(2);
   }

   if(links_[0].joint()->type_ == kJointTypeRot)
   {
     std::cout << "required_friction_torque: " << required_friction_torque << std::endl;
     std::cout << "t_joint_end: " << t_joint_end << std::endl;
    test_friction <<  required_friction_torque(1)/t_joint_end(2), -required_friction_torque(0)/t_joint_end(2), 0.0;
   }

   return test_friction;
   // test_friction <<  required_friction_torque(1)/t_joint_end(2), -required_friction_torque(0)/t_joint_end(2), 0.0;

  //  std::cout << "test_friction: \n" << test_friction << std::endl;
  //  std::cout << std::endl;
  //  std::cout << "friction norm: \n" << test_friction.norm() << std::endl;
  //  std::cout << std::endl;
  //  std::cout << "torque_last: \n" << torque_last << std::endl;
  //  std::cout << std::endl;
  //  std::cout << "transform: \n" << g_world_joint_bot.topLeftCorner(3,3) << std::endl;
  //  std::cout << std::endl;
  //  std::cout << "torque_last_world: \n" << torque_last_world << std::endl;
  //  std::cout << std::endl;
  //  std::cout << "t_world_joint: \n" << t_world_joint << std::endl;
  //  std::cout << std::endl;
  //  std::cout << "t_world_com: \n" << t_world_com << std::endl;
  //  std::cout << std::endl;
  //  std::cout << "t_joint_com: \n" << t_joint_com << std::endl;
  //  std::cout << std::endl;
  //  std::cout << "t_world_end: \n" << t_world_end << std::endl;
  //  std::cout << std::endl;
  //  std::cout << "t_joint_end: \n" << t_joint_end << std::endl;
  //  std::cout << std::endl;
  //  std::cout << "normal: \n" << normal_force << std::endl;
  //  std::cout << std::endl;
  //  std::cout << "link weight: \n" << link_weight << std::endl;
  //  std::cout << std::endl;
  //  std::cout << "t_link_cont: \n" << -t_joint_com_skew*link_weight << std::endl;
  //  std::cout << std::endl;
  //  std::cout << "t_norm_cont: \n" << -t_joint_end_skew*normal_force << std::endl;
  //  std::cout << std::endl;

   //this was used to test by model because this value should always end up as the same as the required_friction_torque(2)
   // double test_Z = required_friction_torque(0)*-t_joint_end(0)/t_joint_end(2) - t_joint_end(1)/t_joint_end(2)*required_friction_torque(1);

   // std::cout << "Test Z: " << test_Z << std::endl;
   // std::cout << "Act Z: " << required_friction_torque(2) << std::endl;
   // std::cout << "Test Friction: " << test_friction << std::endl;
   // std::cout << "test_friction: " << test_friction.norm() << std::endl;
   // std::cout << "greatest possible friction force: " << -mew_*normal_force(2) << std::endl;

  //   Jac jac = getJacHybrid();
  //   std::cout << "jac: " << jac << std::endl;
  //   Eigen::Matrix<double, 1, 4> J_z = jac.block(2,0,1,jac.cols()); //block of size (1,J.cols()) starting at (i,0)
  //   Hom eye;
  //   Hom task_null_space = eye.Identity() - J_z.transpose()*(J_z*J_z.transpose()).inverse()*J_z;
  //   std::cout << "null space: " << task_null_space << std::endl;

  //   double test_friction_norm = test_friction.norm();
       
  //  if (test_friction_norm > -mew_*normal_force(2)) //- because normal_force(2) is negative
  //  {
  //      // Find out amounts over
  //      std::cout << "full friction over: " << test_friction << std::endl;
  //      double force_over = test_friction_norm - std::fabs(mew_*normal_force);
  //      double friction_theta = atan2(test_friction(1),test_friction(0));
  //      double fx_over = force_over*cos(friction_theta);
  //      double fy_over = force_over*sin(friction_theta);

  //      // std::cout << "FALSE!!" << std::endl << std::endl;
  //      return false;
  //  }
  //  else{
  //      // std::cout << "TRUE!!" << std::endl << std::endl;
  //      return true;
  //  }

}

bool Arm::areTorquesPossible(Vec torques)
{
  calcTorqueLimits();
    for (int i=0; i < torques.size(); i++)
    {
        if (torques(i) - max_torques_[i] > 1e-6 || torques(i) - min_torques_[i] < -1e-6) //Did 1e-6 because of machine precision would cause errors to printout when it shouldn't after implementing payload in leg optimization.
        {
            // std::cout << "Can't support weight \n\t Torque: " << torques(i) << "\t Max: " << max_torques_[i] << "\t Min: " << min_torques_[i] << std::endl;
           return false;
        } 
    }
    return true;
}

void Arm::addTorqueExternal(int link_num,
                            const Frame& frame,
                            const Vec6& wrench,
                            const Hom& offset) {

  // transform wrench by offset
  // tau = J_a.T * Ad_g_ab.T * F_b
  // offset is g_ba

  Link* link = &links_[link_num];
  int num_dof = link->num_dof_total_;
  Adj adjoint = HomToAdj(HomInv(offset));

  Jac* jac;
  switch (frame) {
    case Frame::Spatial:
      jac = &jac_spatial_;
      break;
    case Frame::Body:
      jac = &(link->jac_body_);
      break;
    case Frame::Hybrid:
      jac = &(link->jac_hybrid_);
      break;
  }
  torque_external_.head(num_dof) += jac->transpose() * adjoint.transpose() * wrench;
}

void Arm::setTorqueExternal(int link_num,
                            const Frame& frame,
                            const Vec6& wrench,
                            const Hom& offset) {
  setTorqueExternalZero();
  addTorqueExternal(link_num, frame, wrench, offset);
}

void Arm::calcStiffness() {  // assumes calcJac has been called
  Mat flexibility_mat(num_dof_, num_dof_);
  flexibility_mat.setZero();
  flexibility_global_.setZero();

  int ndf;  // num dof proximate
  int nd;   // num dof of joint
  for (auto const& link : links_) {
    ndf = link.num_dof_prox_;
    nd = link.joint_->num_dof_;
    Mat flexibility_jt = (link.joint_->getJointStiffnessMatrix()).inverse();
    flexibility_global_.block(ndf, ndf, nd, nd) << flexibility_jt;
  }
  Eigen::Matrix<double, 6, 6> temp;
  temp.setZero();
  // Equivalent Stiffness/Flexibility - K_eff = inv(J*Fq*J.T), F_eff = inv(K_eff)
  temp = getJacBody() * flexibility_global_ * getJacBody().transpose();
  // setStiffnessBody(temp.inverse());
  setFlexibilityBody(temp);
  temp = getJacHybrid() * flexibility_global_ * getJacHybrid().transpose();
  // setStiffnessHybrid(temp.inverse());
  setFlexibilityHybrid(temp);
}

double Arm::calcMaxStaticLiftForce(
    const double& delta_max) {  // assumes calcJac calcStiffness has been called
  // This Metric characterizes how much weight the arm can bear while keeping the load
  // (1) Below a limit that would create torques in the active directions we can't handle
  // (2) Below a limit that would create a displacement greater than a unit ball defined
  // here

  // (1) - Active Force Limit
  double active_limit, passive_limit;
  Vec3 direction;
  direction << 0, 0, -1;
  active_limit = calcMaxForceLimitTorque(direction);

  // (2) - Passive Force Limit
  calcStiffness();
  passive_limit = calcMaxForceLimitDisplacement(direction, delta_max);  // 2norm

  return std::min(active_limit, passive_limit);
}

double Arm::calcMaxForceLimitTorque(const Vec3& direction) {
  // This computes an acceptable force while keeping torques at the joints (in both off
  // and on axes)  under a determined limit
  calcJointTorqueLimits();  // find limits: this will need to be incorporated later
  calcTorqueGravity();
  Hom offset = Hom::Identity();  // Set unit wrench -> slope of jt-torque x EE force lines
  offset.topRightCorner(3, 1) = links_.back().g_world_end_.topRightCorner(3, 1) -
                                links_.back().g_world_com_.topRightCorner(3, 1);
  Frame frame = Frame::Hybrid;  // Torque External uses com instead of EE, adjust offset
  Vec6 unit_wrench;
  unit_wrench << direction, 0, 0, 0;
  setTorqueExternal(links_.size() - 1.0, frame, unit_wrench, offset);

  // Backsolving to find force limits corresponding with torque limits
  for (int i = 0; i < num_dof_; i++) {
    double force_low =
        ((torque_limit_lower_[i] - torque_gravity_[i]) / torque_external_[i]);
    double force_high =
        ((torque_limit_upper_[i] - torque_gravity_[i]) / torque_external_[i]);

    // Check To Make Sure the arm can support itself (at F=0), If not MaxForce is 0
    // Second condition never happens (force would be needed to stay in nominal position)
    if ((force_low < 0 && force_high < 0) || (force_low > 0 && force_high > 0)) {
      return 0;
    }
    force_limit_[i] = std::max(force_low, force_high);
  }
  return force_limit_.minCoeff();  // choose min max allowable force for
                                   // each joint
}

// Assumes that Linear Stiffness Model holds through the displacement caused by gravity
double Arm::calcMaxForceLimitDisplacement(const Vec3& unit_force,
                                          const double& delta_max) {
  // This metric computes the magnitude of forces that would result in the end effector
  // touching a sphere around its nominal position.
  calcTorqueGravity();
  Vec6 delta_gravity;
  delta_gravity = jac_hybrid_ * flexibility_global_ * torque_gravity_;
  Vec6 delta_external;
  delta_external = flexibility_hybrid_.leftCols(3) * unit_force;

  // Solve Quadratic
  double a, b, c;
  a = 0;
  b = 0;
  c = 0;
  for (int i = 0; i < 3; i++) {
    a = a + pow(delta_external(i), 2);
    b = b + 2 * delta_gravity(i) * delta_external(i);
    c = c + pow(delta_gravity(i), 2);
  }
  c = c - pow(delta_max, 2);

  double root1 = (-b + sqrt(pow(b, 2) - 4 * a * c)) / (2 * a);
  double root2 = (-b - sqrt(pow(b, 2) - 4 * a * c)) / (2 * a);

  if (std::isnan(root1) || (root1 < 0 && root2 < 0)) {
    // cout << "WARNING: Manipulator cannot stay within displacement limits" << endl;
    return 0;
  } else {
    return std::max(root1, root2);
  }
}

double Arm::calcUNorm(const Vec6& x_des) {
  // Uses Moore-Penrose inverse to see how th_dot for given x_dot,returns 2norm
  double UNorm =
      (jac_spatial_real_.transpose() *
       (jac_spatial_real_ * jac_spatial_real_.transpose()).inverse() * x_des).norm();
  return UNorm;
}

void Arm::setRealJointIDX(const Vec& idx) {
  real_joint_idx_ = idx;
  int size = idx.size();
  jac_spatial_real_.resize(6, size);
  jac_body_real_.resize(6, size);
  jac_hybrid_real_.resize(6, size);
}

void Arm::calcJacReal(void) {
  calcJac();
  for (int i = 0; i < real_joint_idx_.size(); i++) {
    jac_spatial_real_.col(i) = jac_spatial_.col(real_joint_idx_[i]);
    jac_body_real_.col(i) = jac_body_.col(real_joint_idx_[i]);
    jac_hybrid_real_.col(i) = jac_hybrid_.col(real_joint_idx_[i]);
  }
}

Jac Arm::getJacBodyRealLink(int link_num) {
  Jac jac, jacreal;
  jac = links_[link_num].jac_body_;
  for (int i = 0; i < real_joint_idx_.size(); i++) {
    if (real_joint_idx_[i] < jac.cols()) {
      jacreal.resize(6, jacreal.cols() + 1);
      jacreal.col(i) = jac.col(real_joint_idx_[i]);
    }
  }
  return jacreal;
}

void Arm::calcJointTorqueLimits(void) {
  // add function here that sets torques according to joint angles
  torque_limit_lower_.setOnes();
  torque_limit_upper_.setOnes();
  // Only limiting torque in active directions (stiffness calc limits in passive) 
  for (int i = 0; i < torque_limit_lower_.size() / 3.0; i++) {
    torque_limit_lower_[3 * i] *= -100000;      // Torsional (half stiffness)
    torque_limit_lower_[3 * i + 1] *= -40;     // U <--- Active Control Capability
    torque_limit_lower_[3 * i + 2] *= -100000;  // V <--- Passive (half stiffness)

    torque_limit_upper_[3 * i] *= 100000;
    torque_limit_upper_[3 * i + 1] *= 40;
    torque_limit_upper_[3 * i + 2] *= 100000;
  }
}

void Arm::calcDerivHom(void) {
  for (auto& link : links_) {
    link.calcDeriveHom(jac_spatial_);
  }
}

// pass in jac_spatial to link, calculate there
void Arm::calcDerivJacSpatial(void) {
  calcDerivHom();
  for (auto& link : links_) {
    link.calcDerivJacSpatial(&partials_);
  }
}

void Arm::calcDerivJacBody(void) {
  for (auto& link : links_) {
    link.calcDerivJacBody(jac_spatial_, partials_);
  }
}

void Arm::calcDerivInertiaMat(void) {
  //   - Calculate (dp_Jb.T * lI * Jb) + (Jb.T * li * dp_Jb)
  for (int i = 0; i < num_dof_; i++) {
    partials_[i].inertia_mat_.setZero();
  }

  for (auto& link : links_) {
    link.calcDerivInertiaMat(&partials_);
  }
}

void Arm::calcCoriolisMat(void) {
  double temp;

  // speedup:  avoid adding transpose above, deal with here?
  // TODO: this might be better to sum over j too, and skip multiplying by theta later
  for (int i = 0; i < num_dof_; i++) {
    for (int j = 0; j < num_dof_; j++) {
      temp = 0.0;
      for (int k = 0; k < num_dof_; k++) {
        temp += 0.5 * (partials_[k].inertia_mat_(i, j) + partials_[j].inertia_mat_(i, k) -
                       partials_[i].inertia_mat_(k, j)) *
                joint_vels_(k);
      }
      coriolis_mat_(i, j) = temp;
    }
  }
}

void Arm::checkMatSizes() {
  CheckMatSize(jac_spatial_, 6, num_dof_);
  CheckMatSize(inertia_mat_, num_dof_, num_dof_);
  CheckMatSize(coriolis_mat_, num_dof_, num_dof_);

  CheckMatSize(joint_angs_, num_dof_);
  CheckMatSize(joint_vels_, num_dof_);
  CheckMatSize(joint_accs_, num_dof_);

  CheckMatSize(torque_gravity_, num_dof_);
  CheckMatSize(torque_external_, num_dof_);
  CheckMatSize(torque_sum_, num_dof_);

  for (auto& link : links_) {
    link.checkMatSizes();
  }

  // check for the right number of partials?
  for (auto& part : partials_) {
    part.checkMatSizes();
  }
}

}  // namespace kinematics
