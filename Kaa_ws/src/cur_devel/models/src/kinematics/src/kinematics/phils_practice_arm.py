#!/usr/bin/env python

# Before running this you have to source the models and utils workspaces

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # NOQA
import numpy as np

import kinematics as kin
import arm_tools as at
from arm_plotter import SimpleArmPlotter, set_axes_equal

fig = plt.figure()
ax = fig.gca(projection='3d')


# Create an arm
height = 0.1
lengths = np.array([1.0,0.5])
arm = kin.Arm()

jt_limit=np.pi/2

# Link 1

j = kin.JointRot(height)
l = kin.Link(j)
l.setGTopEnd(lengths[0], 0.0, 0)

# inertia about CoM of a cylinder with height in z
m = .5
r = .07
Ix = m * (3.0 * r * r + lengths[0]*lengths[0]) / 12.0
Iy = m * (3.0 * r * r + lengths[0]*lengths[0]) / 12.0
Iz = m * (r * r) / 2.0
I = np.array([Ix, Iy, Iz])

pos = np.array([lengths[0]/2.0, 0, 0]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
l.setInertiaSelf(m, pos, I)

# Add the link to the robot
arm.addLink(l)
for i in range(len(arm.links_)):
    arm.links_[0].joint_.limits().setLimitsScalar(jt_limit)

# Link 2

j = kin.JointRot(height)
l = kin.Link(j)
l.setGTopEnd(lengths[1], 0.0, 0)

# inertia about CoM of a cylinder with height in z
m = .25
r = .07
Ix = m * (r * r) / 2.0
Iy = m * (3.0 * r * r + lengths[1]*lengths[1]) / 12.0
Iz = m * (3.0 * r * r + lengths[1]*lengths[1]) / 12.0
I = np.array([Ix, Iy, Iz])

pos = np.array([lengths[1]/2.0, 0, 0.0]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
l.setInertiaSelf(m, pos, I)

# Add the link to the robot
arm.addLink(l)
for i in range(len(arm.links_)):
    arm.links_[1].joint_.limits().setLimitsScalar(jt_limit)



# Set the initial conditions
q = np.array([[0.0],
              [0.0]])
qd = np.array([[0.0],
               [0.0]])
arm.setJointAngsReal(q)
arm.calcFK()

# plotter = SimpleArmPlotter(arm,color='k')
# plotter.plot(ax)

# for i in xrange(1,10):
#     # Set different joint angles
#     jangles = np.array([i/5.0,i/5.0])
#     arm.setJointAngsReal(jangles)
#     arm.calcFK()

#     plotter = SimpleArmPlotter(arm,color='b')
#     plotter.plot(ax)

# set_axes_equal(ax)
# plt.show()

# Apply a constant torque and see where the arm goes
qdes = np.array([[np.pi/2],
                [np.pi/2]])

Kp = np.diag([5.0,1.0])
Kd = np.diag([1.0,0.1])


for i in range(0,500):

    # Calculate the impedance control torques
    tau = np.dot(Kp,(qdes-q)) - np.dot(Kd,qd)
    
    # Update the arm configuration
    arm.setJointAngsReal(q)
    arm.setJointVels(qd)
    arm.calcFK()

    # Calculate the dynamics matrices
    arm.calcDynamicsMats()
    M = arm.getInertiaMat()

    # print("")
    # print(q)
    
    M_inv = np.linalg.inv(M)

    # Calculate joint accelerations
    qdd = np.dot(M_inv,tau - arm.getTorqueCoriolis())

    # Integrate to get velocity and position
    qd = qd + .01*qdd
    q = q + .01*qd

    if(i%2==0):
        ax.clear()
        plotter = SimpleArmPlotter(arm,color='b')
        plotter.plot(ax)
        
        set_axes_equal(ax)
        plt.ion()
        plt.show()
        plt.pause(.0001)

set_axes_equal(ax)
plt.ioff()
plt.show()
        
    



