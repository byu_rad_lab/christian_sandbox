#!/usr/bin/env python

import tools.pickle_tools as pk
import rospy
import time
import datetime
import os
from collections import deque
import ego_byu.kinematics as kin
import numpy as np
# from tools import kin_tools as kt
import tools.kin_tools as kt
import matplotlib.pyplot as plt
from pdb import set_trace as pause
from bisect import bisect_left
import pickle as pickle
from copy import copy, deepcopy
import arm_plotter as ap

np.set_printoptions(threshold=50000)


def simple_arm(height, lengths, jt_limit=np.pi/2):
    arm = kin.Arm()

    # Link Type Initialization
    for i in range(len(lengths)):
        # ROTATIONAL JOINT BEFORE EACH LINK
        j = kin.JointRot(height)
        l = kin.Link(j)
        l.setGTopEnd(lengths[i], 0.0, 0)
        # inertia
        m = .5
        r = .07
        Ix = m * (3.0 * r * r + lengths[i]*lengths[i]) / 12.0
        Iy = m * (3.0 * r * r + lengths[i]*lengths[i]) / 12.0
        Iz = m * (r * r) / 2.0
        I = np.array([Ix, Iy, Iz])
        pos = np.array([0.0, 0, 0]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
        l.setInertiaSelf(m, pos, I)
        arm.addLink(l)

        # Rotation about Z for each Joint
        # g = kt.ax_vec2homog(np.array([0, 0, -np.pi/2]), np.array([0, 0, 0]))
        # g = kt.ax_vec2homog(np.array([0, 0, np.pi/2]), np.array([0, 0, 0]))
        # l.g_top_end_ = np.dot(g, l.g_top_end_)
        # l.g_top_com_ = np.eye(4)
        # l.g_top_com_[2][3] = lengths[i]/2.0

        # addLink
        for i in range(len(arm.links_)):
            arm.links_[i].joint_.limits().setLimitsScalar(jt_limit)

    return arm

def simple_leg(height, lengths, base_hom=np.identity(4), jt_limit=np.pi/2):
    arm = kin.Quadruped()
    arm.mew_ = 0.2 #0.2 is not really significant since this is a fictional robot, just a reasonable value

    # Link Type Initialization
    for i in range(len(lengths)):
        # ROTATIONAL JOINT BEFORE EACH LINK
        j = kin.JointRot(height) #height is never used with a JointRot, don't know why it is there
        if i==0:
            j.mass_ = 0.2 #7.11
        else:
            j.mass_ = 0.2 #2.5

        l = kin.Link(j)
        l.setGTopEnd(lengths[i], 0.0, 0)
        # inertia
        m_link = 0.75 #5.0 #.051 #makes it have a weight of .5 N


        # m_joint = 2.5
        m_valve_block = 0.0
        if i==0:
            # m_joint = 7.11
            m_valve_block = 2.5

        # m_joint = 0.0
        # m = m_link+m_joint+m_valve_block

        # m = m_link+m_valve_block+j.mass_
        m = m_link + j.mass_
        valve_block_length = .25
        # x_COM = (m_link*lengths[i]/2.0 + m_valve_block*(lengths[i]-valve_block_length/2.0))/(m)
        x_COM = (m_link*lengths[i]/2.0)/m

        r = .03

        Ix = m * (r * r) / 2.0
        Iy = m * (3.0 * r * r + lengths[i]*lengths[i]) / 12.0
        Iz = m * (3.0 * r * r + lengths[i]*lengths[i]) / 12.0
        I = np.array([Ix, Iy, Iz])

        pos = np.array([x_COM, 0.0, 0.0]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
        l.setInertiaSelf(m, pos, I)
        arm.addLink(l)

        # Rotation about Z for each Joint
        # g = kt.ax_vec2homog(np.array([0, 0, -np.pi/2]), np.array([0, 0, 0]))
        # g = kt.ax_vec2homog(np.array([0, 0, np.pi/2]), np.array([0, 0, 0]))
        # l.g_top_end_ = np.dot(g, l.g_top_end_)
        # l.g_top_com_ = np.eye(4)
        # l.g_top_com_[2][3] = lengths[i]/2.0

        # addLink
        for i in range(len(arm.links_)):
            arm.links_[i].joint_.limits().setLimitsScalar(jt_limit)

    arm.setGravity(9.81)
    arm.g_world_arm_ = base_hom
    arm.type_ = 2
    arm.calcMass()
    print("Potato: ", arm.getMass())

    return arm
# Nasa arm constructor
# Stiffness based off of a diagonal stiffness matrix for each joint -
# torsional, active, passive.  High active stiffness makes assumption that
# we can completely control torque in active directions
def nasa_arm(jt_height, lengths, base_hom, stiffness=np.array([50, 100000000, 270])):
    arm = kin.Arm()
    valves_placed = 0
    arm.type_ = 0 #0 means it is a nasa arm (mainly used to know which objectives to evaluate in arm_eval_fk.cpp)

    # Link Type Initialization
    for i in range(len(lengths)):
        # ROTATIONAL JOINT BEFORE EACH LINK
        j = kin.JointRot(0)
        l = kin.Link(j)
        l.setGTopEnd(0, 0, 0)
        # inertia
        I = np.array([0, 0, 0])
        pos = np.array([0.0, 0, 0])
        l.setInertiaSelf(0, pos, I)
        arm.addLink(l)

        # JOINT CCC For each link
        j = kin.JointCCC(jt_height)
        j.setJointStiffnessScalar(0) #Set Later
        # print np.array(j.getJointStiffnessMatrix())
        l = kin.Link(j)
        l.setGTopEnd(0.0, 0.0, lengths[i])

        # Add Inertia for Valves this is taken from hardware specs but needs to be updated continually
        m = .4+.7*lengths[i]
        if i == 0 or i == len(lengths)-1:
            m = m-.2  # half joint on beginning and end
        # Valve Condition
        if lengths[i] > .182 and valves_placed != len(lengths)-1:
            m += .7
            valves_placed += 1
        if i != 0 and lengths[i-1] < .182 and valves_placed != len(lengths)-1:
            m += .7
            valves_placed += 1

        r = .07
        Ix = m * (3.0 * r * r + lengths[i]) / 12.0
        Iy = m * (3.0 * r * r + lengths[i]) / 12.0
        Iz = m * (r * r) / 2.0
        I = np.array([Ix, Iy, Iz])
        pos = np.array([0, 0, lengths[i]/2.0])
        l.setInertiaSelf(m, pos, I)
        l.InitInertia()

        # Rotation about Z for each Joint
        if i % 2 == 0:
            g = kt.ax_vec2homog(np.array([0, 0, np.pi/2]), np.array([0, 0, 0]))
        else:
            g = kt.ax_vec2homog(np.array([0, 0, -np.pi/2]), np.array([0, 0, 0]))

        l.g_top_end_ = np.dot(g, l.g_top_end_)

        # addLink
        arm.addLink(l)

    # Valves placed optimilly for joint
    if valves_placed != len(lengths)-1:
        print("ERROR: Not enough valves placed")
        assert(False)

    # stiffness - When passing to Vec signature calls, inputs must be floats, NOT INTS
    for i in range(len(lengths)):
        arm.links_[2*i].joint_.setJointStiffnessScalar(float(stiffness[0]))
        arm.links_[2*i+1].joint_.setJointStiffnessVector(np.array([float(stiffness[1]), float(stiffness[2])]))

    # Real Joint Initialization
    real_joint_idx = np.array([])
    for i in range(len(lengths)):
        real_joint_idx = np.append(real_joint_idx, 3*i+1)

    arm.setRealJointIDX(real_joint_idx)

    # Extra Variables for Evolution
    arm.lengths = lengths
    arm.jt_positions = get_jt_positions(lengths)
    arm.num_lengths = len(lengths)

    # Base variables
    arm.base_hom = base_hom
    arm.g_world_arm_ = base_hom

    # We re-evaluate every 5 generations (see evaluate_design function)
    arm.next_eval_gen = 0

    # Limits <- Problem here?
    limit = np.pi/2.0
    for i in range(len(arm.links_)):
        if np.mod(i, 2) == 0:  # Rotational Joint
            arm.links_[i].joint_.limits().setLimitsScalar(0)
        elif np.mod(i+1, 2) == 0:  # CCC Joint
            bounds_upper = np.array([limit, 0])
            bounds_lower = -bounds_upper
            arm.links_[i].joint_.limits().setLimitsVector(bounds_lower, bounds_upper)
    return arm

def ArmFromYAML():
    LinkTransforms = rospy.get_param("LinkTransforms")
    LinkNum = len(LinkTransforms)
    JointHeights = rospy.get_param("JointHeights")
    LinkMasses = rospy.get_param("LinkMasses")
    LinkRadii = rospy.get_param("LinkRadii")
    BaseHom = rospy.get_param("g_world_base")

    arm = kin.Arm()
    # Link Type Initialization
    for i in range(len(LinkTransforms)):
        # NO ROTATIONAL JOINT BEFORE EACH LINK

        # JOINT CCC For each link
        j = kin.JointCCC(JointHeights[i])
        j.setJointStiffnessScalar(0) #Set Later
        l = kin.Link(j)
        gtopend = kt.ax_vec2homog(np.array(LinkTransforms[i][:3]),np.array(LinkTransforms[i][3:]))
        l.g_top_end_ = gtopend

        #Inertia Taken form Z offset only for now
        r = LinkRadii[i]
        m = LinkMasses[i]
        length = LinkTransforms[i][-1]
        Ix = m * (3.0 * r * r + length) / 12.0
        Iy = m * (3.0 * r * r + length) / 12.0
        Iz = m * (r * r) / 2.0
        I = np.array([Ix, Iy, Iz])
        pos = np.array([LinkTransforms[i][3]/2.0, LinkTransforms[i][4]/2.0, LinkTransforms[i][5]/2.0])
        l.setInertiaSelf(m, pos, I)
        l.InitInertia()

        # addLink
        arm.addLink(l)

    # Real Joint Initialization for CCC Joints
    # CCC Joints have 2 DOFS natively, Bending aboud +X axis (u) and Bending about +Y (v)
    # real_joint_idx specifies which axes motion is occuring about
    jts = rospy.get_param("ControllableJointAxes")
    real_joint_idx =[]
    for i in range(len(jts)):
        if jts[i] == "X":
            real_joint_idx.append(2.0*i)
        if jts[i] == "Y":
            real_joint_idx.append(2.0*i+1.0)
    arm.setRealJointIDX(np.array(real_joint_idx))


    # Base variables
    g = kt.ax_veclist2homog(BaseHom)
    arm.g_world_arm_ = np.array(g)

    # Limits
    l = rospy.get_param("JointLimitsLower")
    u = rospy.get_param("JointLimitsUpper")
    for i in range(LinkNum):
        bounds_upper = np.array([u[i],u[i]])
        bounds_lower = np.array([l[i],l[i]])
        arm.links_[i].joint_.limits().setLimitsVector(bounds_lower, bounds_upper)

    arm.setJointAngsZero()
    arm.calcJacReal()
    return arm

def baxter_arm(base_hom = kt.eul_trans2homog(np.array([0,0,0]),np.array([0, 0, 0]))):
    #Parameters obtained from http://sdk.rethinkrobotics.com/wiki/Hardware_Specifications
    arm = kin.Arm()
    arm.base_hom = base_hom
    m1 = 5.700440
    m2 = 3.226980
    m3 = 4.312720
    m4 = 2.072060
    m5 = 2.246650
    m6 = 1.609790
    m7 = 0.350930

    #Joint1: S0
    j = kin.JointRot(0)
    l = kin.Link(j)
    l.joint_.limits().setLimitsScalar(97*np.pi/180)
    # l.g_top_end_ = kt.ax_vec2homog(np.array([-np.pi/2.0,0,0]),np.array([0, .069, .27035]))
    l.g_top_end_ = kt.ax_vec2homog(np.array([-np.pi/2.,0.,0]),np.array([0.069, 0., .27035]))
    # I = np.array([0.0, 0, 0])
    I = np.array([1., 1., 1.])
    pos = np.array([0.069/2., 0., 0.27035/2.])
    l.setInertiaSelf(m1, pos, I)
    arm.addLink(l)

    #Joint2: S1
    # j = kin.JointCC1D(0)
    j = kin.JointRot(0)
    l = kin.Link(j)
    # l.joint_.limits().setLimitsVector(np.array([-60.0*np.pi/180.0]),np.array([123*np.pi/180.0]))
    l.joint_.limits().setLimitsVector(np.array([-123.*np.pi/180.0]),np.array([60.*np.pi/180.0]))
    # l.g_top_end_ = kt.ax_vec2homog(np.array([0.0,0.0,0.0]),np.array([0.0, 0.0, .102]))
    l.g_top_end_ = kt.eulxzx_trans2homog(np.array([0., np.pi/2., np.pi/2.]),np.array([0.1, 0., 0.]))
    # I = np.array([0.0, 0, 0])
    I = np.array([1., 1., 1.])
    pos = np.array([0.05, 0., 0.])
    # pos = np.array([0.0, 0.05, 0])
    l.setInertiaSelf(m2, pos, I)
    # l.setInertiaSelf(100, pos, I)
    arm.addLink(l)

    #Joint3: E0
    j = kin.JointRot(0)
    l = kin.Link(j)
    # l.joint_.limits().setLimitsScalar(174.25*np.pi/180)
    l.joint_.limits().setLimitsScalar(174.987*np.pi/180)
    # l.g_top_end_=kt.ax_vec2homog(np.array([0.0,0.0,0.0]),np.array([0, .069, .262]))
    l.g_top_end_=kt.ax_vec2homog(np.array([-np.pi/2.,0.0,0.0]),np.array([0.069, 0., .36435-0.1]))
    # I = np.array([0.0, 0, 0])
    I = np.array([1., 1., 1.])
    pos = np.array([0.069/2., 0., (.36435-0.1)/2.])
    l.setInertiaSelf(m3, pos, I)
    arm.addLink(l)

    #Joint4: E1
    # j = kin.JointCC1D(0)
    j = kin.JointRot(0)
    l = kin.Link(j)
    # l.joint_.limits().setLimitsVector(np.array([-150.0*np.pi/180.0]),np.array([2.864*np.pi/180.0]))
    l.joint_.limits().setLimitsVector(np.array([-2.864*np.pi/180.0]),np.array([150.*np.pi/180.0]))
    # l.g_top_end_=kt.ax_vec2homog(np.array([0.0,0.0,0.0]),np.array([0.0, 0.0, .104]))
    l.g_top_end_=kt.ax_vec2homog(np.array([np.pi/2.,0.0,0.0]),np.array([0.0, -0.09, 0.]))
    # I = np.array([0.0, 0, 0])
    I = np.array([1., 1., 1.])
    pos = np.array([0., -0.09/2., 0.])
    l.setInertiaSelf(m4, pos, I)
    arm.addLink(l)

    #Joint5: W0
    j = kin.JointRot(0)
    l = kin.Link(j)
    l.joint_.limits().setLimitsScalar(175.25*np.pi/180)
    # l.g_top_end_=kt.ax_vec2homog(np.array([0.0,0.0,0.0]),np.array([0, .01, .271]))
    l.g_top_end_=kt.ax_vec2homog(np.array([-np.pi/2.,0.0,0.0]),np.array([0.01, .0, 0.37429-0.09]))
    # I = np.array([0.0, 0, 0])
    I = np.array([1., 1., 1.])
    pos = np.array([0.005, 0., (0.37429-0.09)/2.])
    l.setInertiaSelf(m5, pos, I)
    arm.addLink(l)

    #Joint6: W1
    # j = kin.JointCC1D(0)
    j = kin.JointRot(0)
    l = kin.Link(j)
    # l.joint_.limits().setLimitsVector(np.array([-120.0*np.pi/180.0]),np.array([90*np.pi/180.0]))
    l.joint_.limits().setLimitsVector(np.array([-90.0*np.pi/180.0]),np.array([120.*np.pi/180.0]))
    # l.g_top_end_=kt.ax_vec2homog(np.array([0.0,0.0,0.229]),np.array([0.0, 0.0, .104]))
    l.g_top_end_=kt.ax_vec2homog(np.array([np.pi/2.,0.0,0.0]),np.array([0.0, -0.17, 0.]))
    # I = np.array([0.0, 0, 0])
    # pos = np.array([0.0, 0, 0])
    # l.setInertiaSelf(0, pos, I)
    I = np.array([1., 1., 1.])
    pos = np.array([0., -0.17/2., 0.])
    l.setInertiaSelf(m6, pos, I)
    arm.addLink(l)

    #Joint7: W2
    j = kin.JointRot(0)
    l = kin.Link(j)
    l.joint_.limits().setLimitsScalar(175.25*np.pi/180)
    # l.g_top_end_=kt.ax_vec2homog(np.array([0.0,0.0,0.0]),np.array([0.0, 0.0, 0.0]))
    l.g_top_end_=kt.ax_vec2homog(np.array([0.0,0.0,0.0]),np.array([0.0, 0.0, 0.229525-0.17]))
    # I = np.array([0.0, 0, 0])
    I = np.array([1., 1., 1.])
    pos = np.array([0.0, 0., (0.229525-0.17)/2.])
    l.setInertiaSelf(m7, pos, I)
    arm.addLink(l)
    return arm


# Jabberwock Constructor
def jabberwock(lengths, angles, base_hom):
    height = 0.182

    # Link1
    length1 = np.array([0.0, 0.0, lengths[0]])
    length2 = np.array([0.0, 0.0, lengths[1]])
    bend1 = np.array([angles[0], 0.0, 0.0])
    hom_trans = kin.HomFromAxVec(np.zeros(3), length2)
    hom_bend = kin.HomFromAxVec(bend1, length1)
    hom_offset1 = np.dot(hom_bend, hom_trans)

    # Link1
    length1 = np.array([0.0, 0.0, lengths[2]])
    length2 = np.array([0.0, 0.0, lengths[3]])
    bend1 = np.array([angles[1], 0.0, 0.0])
    hom_trans = kin.HomFromAxVec(np.zeros(3), length2)
    hom_bend = kin.HomFromAxVec(bend1, length1)
    hom_offset2 = np.dot(hom_bend, hom_trans)

    # Link3
    hom_offset3 = kin.HomTranslate(0.0, 0.0, lengths[4])

    # Offsets
    link_offsets = []
    link_offsets.append(hom_offset1)
    link_offsets.append(hom_offset2)
    link_offsets.append(hom_offset3)

    # Limits
    limits = np.array([56.0, 90.0, 90.0]) * np.pi / 180.0

    arm = kin.Arm()
    for i in range(len(limits)):
        joint = kin.JointCCC(height)
        link = kin.Link(joint)

        # should be a method of link, but only allows x, y, z setting
        link.g_top_end_ = link_offsets[i]
        arm.addLink(link)

    for i in range(len(limits)):
        bounds_upper = np.array([limits[i], limits[i]])
        bounds_lower = -bounds_upper
        arm.links_[i].joint_.limits().setLimitsVector(bounds_lower, bounds_upper)

    # Add A Rot on the End
    j = kin.JointRot(0)
    l = kin.Link(j)
    l.setGTopEnd(0, 0, 0)
    arm.addLink(l)
    arm.links_[-1].joint_.limits().setLimitsScalar(np.pi)

    arm.g_world_arm_ = base_hom

    # For Evolution
    arm.bend_angles = angles
    arm.lengths = lengths
    arm.num_lengths = len(lengths)
    arm.base_hom = base_hom
    arm.next_eval_gen = 0
    arm.type_ = 1; #1 means it is a Jabberwock arm (mainly used so can tell which optimization to run in arm_eval_fk.cpp)

    return arm

def leg(lengths, angles, base_hom, height, density = 0.5, min_lengths = [0.0, 0.0, 0.0, 0.0]):
    angles[1] = 0.0
    angles[3] = 0.0

    hom_offset = []
    offset_angle = -np.pi/4.0

    limits = np.array([90.0, 90.0]) * np.pi / 180.0
    arm = kin.Quadruped()
    arm.mew_ = 0.184 #.184 from experiments between wheeled foot and carpet

    #Build links
    for i in range(int(len(lengths)/2)):
        #define length and rotation vectors to make the link
        length1 = np.array([0.0, 0.0, lengths[2*i]])
        length2 = np.array([0.0, 0.0, lengths[2*i+1]])
        bend1 = np.array([angles[2*i],0.0,0.0])
        bend2 = np.array([0.0, angles[2*i+1],0.0])

        # create the joint and link
        if i==0: #if statement for the big 8 bellows (height, mass, pressure_to_torque constant, passive_spring_constant, max_pressure (Pa))
            #make the homogeneous transform from the base of the link to the end of the link
            hom_length1 = np.array(kin.HomFromAxVec(np.array([0.0,0.0,-offset_angle]), length1))
            hom_bend1 = np.array(kin.HomFromAxVec(bend1, np.array([0.0,0.0,0.0])))
            hom_bend2 = np.array(kin.HomFromAxVec(bend2, np.array([0.0,0.0,0.0])))
            hom_length2 = np.array(kin.HomFromAxVec(np.array([0.0,0.0,-np.pi/2.0+offset_angle]), length2))
            hom_offset = np.dot(hom_length1, np.dot(hom_bend1, np.dot(hom_bend2,hom_length2)))

            joint = kin.JointCCC(height, 6.125, 1/2160.0, 136.0, 599844.0) #6.125 is the empirically measured mass of an 8 bellows with 4 circular ends (2 big and 2 small), 1 Medulla Board, and 4 pressure sensors.
            # 599844.0 is around 87 psi
            joint.setAngsZero()
            joint.calcFK()
            joint.calcCOM()
            hom_COM_Joint = joint.g_top_com_

            # inertia
            #mass of adjustable aluminium
            mass_al = 4.018
            hom_COMal = hom_length1

            length1_min = np.array([0.0,0.0,min_lengths[2*i]])
            length2_min = np.array([0.0,0.0,min_lengths[2*i+1]])
            m1 = density*(lengths[2*i] - min_lengths[2*i]) #mass of first additional link
            m2 = density*(lengths[2*i+1] - min_lengths[2*i+1]) #mass of second additional link

            #find the COMs of each length
            hom_COM1 = np.array(kin.HomFromAxVec(np.array([0.0,0.0,-offset_angle]), np.array([0.0,0.0,0.055]) + (length1 - length1_min)/2.0)) #the 0.055 is the offset to where the aluminum starts
            hom_COM2_int = np.array(kin.HomFromAxVec(np.zeros(3), length2_min + (length2 - length2_min)/2.0))
            hom_COM2 = np.dot(hom_length1, np.dot(hom_bend1, np.dot(hom_bend2,hom_COM2_int)))


            mass_valve_block = 1.365 #empirically approximated weight of a smaller valve block (using VS-05 enfield valves) taking weight of valve block with bigger valves, and swapping out their masses
            valve_block_offset = -0.08 #distance back to COM from end of link
            hom_end_to_valve = np.array(kin.HomFromAxVec(np.array([0.0,0.0,-offset_angle]), np.array([0.0,0.0,valve_block_offset])))
            hom_COM_valve = np.dot(hom_offset,hom_end_to_valve)

            m = m1 + m2 + mass_al + mass_valve_block + joint.mass_ #total mass

            #find the center of mass of the whole link.  The joint COM is approximated as halfway between its top and bottom.
            # Originally it did not update and was only correct in the zero configuration.  Now this updates
            # in the Link::calcFK(). The following notes the error from the approximation when it did not
            # update.
            # For our purposes, it is the most incorrect when it is at pi/2 which for the 8 bellows, equates to about a
            # .537 Nm difference which is not a lot in terms of the joints torque output (91 Nm for the 4 bellows and 278 Nm for the 8-bellows)
            x_COM = (m1*hom_COM1[0][3] + m2*hom_COM2[0][3] + mass_al*hom_COMal[0][3] + joint.mass_*hom_COM_Joint[0][3] + mass_valve_block*hom_COM_valve[0][3])/m
            y_COM = (m1*hom_COM1[1][3] + m2*hom_COM2[1][3] + mass_al*hom_COMal[1][3] + joint.mass_*hom_COM_Joint[1][3] + mass_valve_block*hom_COM_valve[1][3])/m
            z_COM = (m1*hom_COM1[2][3] + m2*hom_COM2[2][3] + mass_al*hom_COMal[2][3] + joint.mass_*hom_COM_Joint[2][3] + mass_valve_block*hom_COM_valve[2][3])/m

        else: #default joint is for 4 bellows
            #make the homogeneous transform from the base of the link to the end of the link
            hom_length1 = np.array(kin.HomFromAxVec(np.array([0.0,0.0,np.pi/2.0-offset_angle]), length1))
            hom_bend1 = np.array(kin.HomFromAxVec(bend1, np.array([0.0,0.0,0.0])))
            hom_bend2 = np.array(kin.HomFromAxVec(bend2, np.array([0.0,0.0,0.0])))
            #hom_length2 = np.array(kin.HomFromAxVec(np.array([0.0,0.0,-np.pi/2.0+offset_angle]), length2))
            hom_length2 = np.array(kin.HomFromAxVec(np.array([0.0,0.0,0.0]), length2))
            hom_offset = np.dot(hom_length1, np.dot(hom_bend1, np.dot(hom_bend2,hom_length2)))

            joint = kin.JointCCC(height)
            joint.setAngsZero()
            joint.calcFK()
            joint.calcCOM()
            hom_COM_Joint = joint.g_top_com_

            # inertia
            #mass of adjustable aluminium minus 9.2 cm at the end since we do not need room for valve block
            mass_al = 4.018 - 0.092*density
            hom_COMal = hom_length1

            length1_min = np.array([0.0,0.0,min_lengths[2*i]])
            length2_min = np.array([0.0,0.0,min_lengths[2*i+1]])
            m1 = density*(lengths[2*i] - min_lengths[2*i]) #mass of first additional link
            m2 = density*(lengths[2*i+1] - min_lengths[2*i+1]) #mass of second additional link

            #find the COMs of each length
            hom_COM1 = np.array(kin.HomFromAxVec(np.array([0.0,0.0,-offset_angle]), np.array([0.0,0.0,0.013]) + (length1 - length1_min)/2.0)) #0.013 is the offset to where the aluminum starts
            hom_COM2_int = np.array(kin.HomFromAxVec(np.zeros(3), length2_min + (length2 - length2_min)/2.0))
            hom_COM2 = np.dot(hom_length1, np.dot(hom_bend1, np.dot(hom_bend2,hom_COM2_int)))

            mass_foot = 1.6517 #2.0 estimated for Gillette design, 1.6517 for wheel design
            foot_COM_offset = 0.085 #distance to COM from end of link. 0.06256 estimated for Gillette design, .085 for wheel design
            hom_end_to_foot_COM = np.array(kin.HomFromAxVec(np.zeros(3), np.array([0.0,0.0,foot_COM_offset])))
            hom_COM_foot = np.dot(hom_offset,hom_end_to_foot_COM)

            m = m1 + m2 + mass_al + mass_foot + joint.mass_ #total mass

            #find the center of mass of the whole link
            x_COM = (m1*hom_COM1[0][3] + m2*hom_COM2[0][3] + mass_al*hom_COMal[0][3] + joint.mass_*hom_COM_Joint[0][3] + mass_foot*hom_COM_foot[0][3])/m
            y_COM = (m1*hom_COM1[1][3] + m2*hom_COM2[1][3] + mass_al*hom_COMal[1][3] + joint.mass_*hom_COM_Joint[1][3] + mass_foot*hom_COM_foot[1][3])/m
            z_COM = (m1*hom_COM1[2][3] + m2*hom_COM2[2][3] + mass_al*hom_COMal[2][3] + joint.mass_*hom_COM_Joint[2][3] + mass_foot*hom_COM_foot[2][3])/m


            foot_end_offset = 0.17 #.127 for Gillette design, .17 for wheel design
            hom_end_to_foot_end = np.array(kin.HomFromAxVec(np.zeros(3), np.array([0.0,0.0,foot_end_offset])))
            hom_offset = np.dot(hom_offset,hom_end_to_foot_end)

        # joint = kin.JointCCC(height,1/6612.0, 27.0, 599844.0)
        link = kin.Link(joint)

        # should be a method of link, but only allows x, y, z setting
        link.g_top_end_ = hom_offset

        #find COM of joint
        # print "g_world_top_: ", link.g_world_top_
        # print "g_world_bot_: ", link.g_world_bot_
        # g_top_bot = np.dot(np.transpose(link.g_world_top_), link.g_world_bot_)
        # x_COM_joint = g_top_bot[0][3]/2.0
        # y_COM_joint = g_top_bot[1][3]/2.0
        # z_COM_joint = g_top_bot[2][3]/2.0
        # print "x joint: ", x_COM_joint
        # print "y joint: ", y_COM_joint
        # print "z joint: ", z_COM_joint


        # print "x, y, and z"
        # print x_COM
        # print y_COM
        # print z_COM

        Ix = 1.0 #need to look into Is.  Currently my optimization doesn't make use of this, so probably okay
        Iy = 1.0
        Iz = 1.0
        I = np.array([Ix, Iy, Iz])
        pos = np.array([x_COM,y_COM,z_COM])
        #print "pos: ", pos
        link.setInertiaSelf(m, pos, I)
        # add the link to the arm
        arm.addLink(link)

        #set the joint limits for each joint
        bounds_upper = np.array([limits[i], limits[i]])
        bounds_lower = -bounds_upper
        arm.links_[i].joint_.limits().setLimitsVector(bounds_lower, bounds_upper)

    #this negative rotation about z is because of how we ended up mounting the first joint on the shoulder mount
    shoulder_joint_offset = kt.ax_vec2homog(np.array([0.0, 0.0, offset_angle]), np.array([0.0, 0.0, 0.0]))
    arm.g_world_arm_ = np.dot(base_hom, shoulder_joint_offset)
    # arm.g_world_arm_ = base_hom

    # For Evolution
    arm.bend_angles = angles
    arm.lengths = lengths
    arm.num_lengths = len(lengths)
    arm.base_hom = base_hom
    arm.next_eval_gen = 0
    arm.type_ = 2 #2 means it is a leg (mainly used so can tell which optimization to run in arm_eval_fk.cpp)
    arm.setGravity(9.81)

    return arm

def simple_quadruped(lengths, base_hom, base_width, base_length, leg_mount_angle, height):
    quadruped = simple_leg(height, lengths, base_hom)
    quadruped.setBaseWidth(base_width)
    quadruped.setBaseLength(base_length)
    quadruped.setLegMountAngle(leg_mount_angle)
    quadruped.type_ = 3 #3 means it is a quadruped

    return quadruped

def quadruped(lengths, angles, base_hom, base_width, base_length, leg_mount_angle, height, density = 0.5, min_lengths = [0.0, 0.0, 0.0, 0.0]):

    # quadruped = kin.Quadruped()
    quadruped = leg(lengths, angles, base_hom, height, density, min_lengths)
    quadruped.setBaseWidth(base_width)
    quadruped.setBaseLength(base_length)
    quadruped.setLegMountAngle(leg_mount_angle)
    quadruped.type_ = 3 #3 means it is a quadruped

    return quadruped

def king_louie_left_arm(lengths = [0.185, 0.39, 0.155, 0.38]
, jt_limits_lower = np.array([[-np.pi/2.], [-np.pi/2.], [-np.pi/2.], [-np.pi/2.]])
, jt_limits_upper= np.array([[0.0], [np.pi/2.], [np.pi/2.], [np.pi/2.]])
):
    # lengths: lengths of each link from base out to end (list)
    # jt_limits_lower: lower joint limit of each joint from first joint to last (np array in radians)
    # jt_limits_upper: upper joint limit of each joint from first joint to last (np array in radians)
    # which-arm: 'l' for left, 'r' for right, or 'b' for both
    # each link is defined as a joint then a link, the transform g_top_end means the end of the link represented in the top of the joints frame
    # each joint starts at the previous links end frame
    left_arm = kin.Arm()
    j = kin.JointRot()
    l = kin.Link(j)
    l.g_top_end_ = kt.eul_trans2homog(np.array([-np.pi/2.0,0,0]),np.array([lengths[0], 0, 0]))
    left_arm.addLink(l)

    j = kin.JointRot()
    l = kin.Link(j)
    l.g_top_end_ = kt.eul_trans2homog(np.array([0,0,0]),np.array([lengths[1], 0, 0]))
    left_arm.addLink(l)

    j = kin.JointRot()
    l = kin.Link(j)
    l.g_top_end_ = kt.eul_trans2homog(np.array([np.pi/2.0,0,0]),np.array([lengths[2], 0, 0]))
    left_arm.addLink(l)

    j = kin.JointRot()
    l = kin.Link(j)
    l.g_top_end_ = kt.eul_trans2homog(np.array([np.pi,0,0]),np.array([lengths[3], 0, 0]))
    left_arm.addLink(l)

    # add joint limits
    for i in range(len(left_arm.links_)):
        left_arm.links_[i].joint_.limits().setLimitsVector(jt_limits_lower[i], jt_limits_upper[i])

    return left_arm

def king_louie_right_arm(lengths = [0.185, 0.39, 0.155, 0.38]
, jt_limits_lower = np.array([[-np.pi/2.], [-np.pi/2.], [-np.pi/2.], [-np.pi/2.]])
, jt_limits_upper= np.array([[0.0], [np.pi/2.], [np.pi/2.], [np.pi/2.]])
):
    # lengths: lengths of each link from base out to end (list)
    # jt_limits_lower: lower joint limit of each joint from first joint to last (np array in radians)
    # jt_limits_upper: upper joint limit of each joint from first joint to last (np array in radians)
    # which-arm: 'l' for left, 'r' for right, or 'b' for both
    # each link is defined as a joint then a link, the transform g_top_end means the end of the link represented in the top of the joints frame
    # each joint starts at the previous links end frame

    right_arm = kin.Arm()
    j = kin.JointRot()
    l = kin.Link(j)
    l.g_top_end_ = kt.eul_trans2homog(np.array([-np.pi/2.0,0,np.pi]),np.array([-lengths[0], 0, 0]))
    right_arm.addLink(l)

    j = kin.JointRot()
    l = kin.Link(j)
    l.g_top_end_ = kt.eul_trans2homog(np.array([0,0,0]),np.array([lengths[1], 0, 0]))
    right_arm.addLink(l)

    j = kin.JointRot()
    l = kin.Link(j)
    l.g_top_end_ = kt.eul_trans2homog(np.array([-np.pi/2.0,0,0]),np.array([lengths[2], 0, 0]))
    right_arm.addLink(l)

    j = kin.JointRot()
    l = kin.Link(j)
    l.g_top_end_ = kt.eul_trans2homog(np.array([0,0,0]),np.array([lengths[3], 0, 0]))
    right_arm.addLink(l)

    # add joint limits
    for i in range(len(right_arm.links_)):
        right_arm.links_[i].joint_.limits().setLimitsVector(jt_limits_lower[i], jt_limits_upper[i])

    return right_arm

# def kaa_arm(lengths = [0.0600, 0.5270, 0.3460, 0.2240, 0.2700, 0.0300], jt_limits_lower = np.array([[-np.pi/2.], [-np.pi/2.], [-np.pi/2.], [-np.pi/2.], [-np.pi/2.],[-np.pi/2.]]), jt_limits_upper = np.array([[np.pi/2.], [np.pi/2.], [np.pi/2.], [np.pi/2.], [np.pi/2.],[np.pi/2.]])):
def kaa_arm(lengths = [0.0600, 0.5270, 0.3460, 0.2240, 0.2700, 0.0300], jt_limits_lower = np.array([[-.68], [-.81], [-1.], [-1.3], [-1.4],[-1.2]]), jt_limits_upper = np.array([[.6], [.87], [1.], [1.1], [1.3],[.98]])):
    # lengths: lengths of each link from base out to end (list)
    # jt_limits_lower: lower joint limit of each joint from first joint to last (np array in radians) found by commanding +/-90 deg and recording what it reached
    # jt_limits_upper: upper joint limit of each joint from first joint to last (np array in radians) found by commanding +/-90 deg and recording what it reached
    # each link is defined as a joint then a link, the transform g_top_end means the end of the link represented in the top of the joints frame
    # each joint starts at the previous links end frame
    # all masses in kg
    mJ1L1J2 = 1.75
    mL2 = 0.5453
    mJ3 = 0.5076
    mL3 = 0.3218
    mJ4 = 0.4593
    mL4 = 0.1552
    mJ5 = 0.4536
    mL5 = 0.2052
    mJ6L6 = 0.7285
    hos = 0.06826 #hosing is 0.06826 kg/m

    m1 = mJ1L1J2 - 0.2 + 11*hos*lengths[0]# subtracted of 0.2 for the first joint that doesn't contribute to inertia
    pos1 = np.array([lengths[0]/2., 0., 0.])
    m2 = mL2 + mJ3 + 9*hos*lengths[1]
    pos2 = np.array([(mL2*lengths[1]/2. + mJ3*lengths[1] + 9*hos*lengths[1]/2.)/m2, 0., 0.])
    m3 = mL3 + mJ4 + 7*hos*lengths[2]
    pos3 = np.array([(mL3*lengths[2]/2. + mJ4*lengths[2] + 7*hos*lengths[2]/2.)/m3, 0., 0.])
    m4 = mL4 + mJ5 + 5*hos*lengths[3]
    pos4 = np.array([(mL4*lengths[3]/2. + mJ5*lengths[3] + 5*hos*lengths[3]/2.)/m4, 0., 0.])
    m5 = mL5 + mJ5 + 3*hos*lengths[4] # added 5 here cuz I don't have the individual mass of joint 6 and it is about the same size as joint 6
    pos5 = np.array([(mL5*lengths[4]/2. + mJ5*lengths[4] + 3*hos*lengths[4]/2.)/m5, 0., 0.])
    m6 = mJ6L6 - mJ5 # since I only want the mass of link 6
    pos6 = np.array([lengths[5]/2., 0., 0.])

    kaa = kin.Arm()
    j = kin.JointRot()
    l = kin.Link(j)
    l.g_top_end_ = kt.eul_trans2homog(np.array([np.pi/2.,0.,0.]), np.array([lengths[0],0., 0.]))
    I = np.array([1., 1., 1.])
    l.setInertiaSelf(m1, pos1, I)
    kaa.addLink(l)

    j = kin.JointRot()
    l = kin.Link(j)
    l.g_top_end_ = kt.eul_trans2homog(np.array([-np.pi/2.,0.,0.]), np.array([lengths[1],0., 0.]))
    I = np.array([1., 1., 1.])
    l.setInertiaSelf(m2, pos2, I)
    kaa.addLink(l)

    j = kin.JointRot()
    l = kin.Link(j)
    l.g_top_end_ = kt.eul_trans2homog(np.array([np.pi/2.,0.,0.]), np.array([lengths[2],0., 0.]))
    I = np.array([1., 1., 1.])
    l.setInertiaSelf(m3, pos3, I)
    kaa.addLink(l)

    j = kin.JointRot()
    l = kin.Link(j)
    l.g_top_end_ = kt.eul_trans2homog(np.array([-np.pi/2.,0.,0.]), np.array([lengths[3],0., 0.]))
    I = np.array([1., 1., 1.])
    l.setInertiaSelf(m4, pos4, I)
    kaa.addLink(l)

    j = kin.JointRot()
    l = kin.Link(j)
    l.g_top_end_ = kt.eul_trans2homog(np.array([np.pi/2.,0.,0.]), np.array([lengths[4],0., 0.]))
    I = np.array([1., 1., 1.])
    l.setInertiaSelf(m5, pos5, I)
    kaa.addLink(l)

    j = kin.JointRot()
    l = kin.Link(j)
    l.g_top_end_ = kt.eul_trans2homog(np.array([0.,0.,0.]), np.array([lengths[5],0., 0.]))
    I = np.array([1., 1., 1.])
    l.setInertiaSelf(m6, pos6, I)
    kaa.addLink(l)

    # kaa.g_world_arm_ = kt.eul_trans2homog(np.array([0.,0.,0.]), np.array([0.5, 0., 0.]))
    kaa.setGWorldArm(kt.eul_trans2homog(np.array([0.,0.,0.]), np.array([0.5, 0., 0.])))
    # add joint limits
    for i in range(len(kaa.links_)):
        kaa.links_[i].joint_.limits().setLimitsVector(jt_limits_lower[i], jt_limits_upper[i])

    return kaa

def multi_arm(arms, design_variables, design_variables_boolean, fixed_variables, arms_names):
    # design variables should be in a list of lists as follows [[arm1_variables],[arm2_variables],....,[armn_variables]]
    #       where armi_variables is in the following format [xr, zr, xr, xt, yt, zt] but leaving out the design variables you can't change
    # so a design variables design_variables_boolean pair would look as follows:
    #       [zr, xt, yt] - [0, 1, 0, 1, 1, 0]
    # fixed_variables is a list of lists containing the values for the rotations and translations the optimization isn't tweaking, so for the above example, this would look like: [xr, xr, zt]
    for i in range(len(arms)):
        k = 0
        l = 0
        rot_trans = [0,0,0,0,0,0]
        for j in range(6):
            if design_variables_boolean[i][j] == 1:
                rot_trans[j] = design_variables[i][k]
                k += 1
            else:
                rot_trans[j] = fixed_variables[i][l]
                l += 1
        x1r = rot_trans[0]
        zr = rot_trans[1]
        x2r = rot_trans[2]
        xt = rot_trans[3]
        yt = rot_trans[4]
        zt = rot_trans[5]
        if arms_names[i] == 'kaa':
            T_j1_a = np.array([[1., 0., 0., 0.5],[0., 1., 0., 0.],[0.,0.,1.,0.],[0.,0.,0.,1.]])# add the transform back to the arm base frame which isn't at the first joint for kaa, since kaa starts with a 0.5 m link
            T_a_w = kt.eulxzx_trans2homog(np.array([x1r, zr, x2r]),np.array([xt, yt, zt]))
            T_j1_w =  np.matmul(T_a_w, T_j1_a)
            arms[i].setGWorldArm(T_j1_w)
        else:
            arms[i].setGWorldArm(kt.eulxzx_trans2homog(np.array([x1r, zr, x2r]),np.array([xt, yt, zt])))
    return arms

def plot_arm(arm, ax, fig):
    x = np.array([])
    y = np.array([])
    z = np.array([])
    for i in range(len(arm.links_)):
        x = np.append(x, arm.links_[i].g_world_end_[0][3])
        y = np.append(y, arm.links_[i].g_world_end_[1][3])
        z = np.append(z, arm.links_[i].g_world_end_[2][3])
    ax = fig.gca(projection='3d')
    ax.plot(x, y, z)
    # ax.scatter(x,y,z)
    ax.set_xlim([-1.5, 1.5])
    ax.set_ylim([0, 3])
    ax.set_zlim([0, 3])
    fig.canvas.set_window_title('Forward Kinematic Sampling')
    plt.draw()
    plt.show()


# Optional function that can be called
def check_variability(arm):
    # This function checks the variability in the FK evaluator, the arm should be evaluated +50x
    # The delay needs to stay for random seeding purposes
    scores = np.array([])
    cutoffs = np.array([])
    variance = np.array([])

    plt.ion()
    fig, ax = plt.subplots()
    # for rep in np.array([.9,.91,.92,.93,.94,.95,.96,.97,.98,.99,.999]):
    for cutoff in np.array([.1, .2, .3, .4, .5, .6, .7, .8, .9, .95, .96, .97, .98, .99]):
        for x in range(50):
            time.sleep(1)
            score = evaluate_design(arm, 0, cutoff, True, 'single_objective')
            scores = np.append(scores, score)

        variance = np.append(variance, np.std(scores))
        cutoffs = np.append(cutoffs, cutoff)
        scores = np.array([])
        ax.scatter(cutoffs, variance)
        plt.xlabel("Redundancy Cutoff")
        plt.ylabel("Std. of 50 Evaluations")
        plt.title("Variability of Arm Evaluation")
        plt.draw()

    plt.ioff()

    plt.show()
    input("Check Variability")


def pickle_arms(arms, robot_type="nasa_arm"):
    #check to make sure there is a file to save it
    directorypath = os.path.expanduser("~/radlab/data/evolutionary_optimization/")
    if not os.path.exists(directorypath):
        os.makedirs(directorypath)
    today = datetime.date.today()
    todaystr = today.isoformat()
    filepath = os.path.expanduser(directorypath+todaystr)
    try: #This will make a new file path if there already isn't one.  If there is, it will ask if it should replace it.
        os.mkdir(filepath)
        for i, arm in enumerate(arms):
            print("Saving Pickle of Best Arm")
            pk.save_pickle(os.path.join(filepath, 'best_arm_lengths_%s' % str(i)), arm.lengths)
            pk.save_pickle(os.path.join(filepath, 'best_arm_hom_%s' % str(i)), arm.base_hom)
            if robot_type == "leg" or robot_type == "jabberwock":
                pk.save_pickle(os.path.join(filepath, 'best_arm_bend_angles_%s' % str(i)), arm.bend_angles)

    except:
        key = input("Replace Already Existing Optimal Design? y/n")
        while(key != 'y' and key != 'n'):
            key = input("Replace Already Existing Optimal Design? y/n")

        if key == 'y':
            print(filepath)
            for i, arm in enumerate(arms):
                print("Optimal Design replaced")
                pk.save_pickle(os.path.join(filepath, 'best_arm_lengths_%s' % str(i)), arm.lengths)
                pk.save_pickle(os.path.join(filepath, 'best_arm_hom_%s' % str(i)), arm.base_hom)
                if robot_type == "leg" or robot_type == "jabberwock":
                    pk.save_pickle(os.path.join(filepath, 'best_arm_bend_angles_%s' % str(i)), arm.bend_angles)

        if key == 'n':
            print("Optimal Design Not Saved")

def load_leg_pickle(lengths_filename, hom_filename, bend_angles_filename=""):
    lengths = pk.load_pickle(lengths_filename)
    hom = pk.load_pickle(hom_filename)
    bend_angles = pk.load_pickle(bend_angles_filename)
    arm = leg(lengths, bend_angles, hom, .2286)
    return arm

def load_nasa_arm_pickle(lengths_filename, hom_filename):
    lengths = pk.load_pickle(lengths_filename)
    hom = pk.load_pickle(hom_filename)
    print("Lengths: ", lengths)
    print("Hom: ", hom)

    base_hom = kt.ax_vec2homog(np.array([hom[0], 0, 0]), np.array([0.0, 0.0, hom[1]]))
    arm = nasa_arm(.06, lengths, base_hom)
    return arm, lengths, hom

def load_quadruped_pickle(directory, num):
    lengths = pk.load_pickle(directory + 'best_design_Lengths_' + str(num))
    bend_angles = pk.load_pickle(directory + 'best_design_Bend_Angles_' + str(num))
    x_rotation = pk.load_pickle(directory + 'best_design_Leg_x_rotation_' + str(num))
    base_width = pk.load_pickle(directory + 'best_design_Base_width_' + str(num))
    base_length = pk.load_pickle(directory + 'best_design_Base_length_' + str(num))
    mount_angle = pk.load_pickle(directory + 'best_design_Leg_mount_angle_' + str(num))

    print("Lengths: ", lengths)
    print("Bend Angles: ", bend_angles)
    print("X Rotation: ", x_rotation)
    print("Leg Mount Angle: ", mount_angle)
    print("base_length: ", base_length)
    print("base_width: ", base_width)

    R = 0.18 #radius of shoulder mount
    d = 0.145 #offset to beginning of joint from shoulder mount rotation center
    base_hom = kt.ax_vec2homog(np.array([x_rotation[0], 0, 0]), np.array([0.0, R + d*np.sin(x_rotation[0]), d*np.cos(x_rotation[0])]))

    angles = [bend_angles[0], 0.0, bend_angles[1], 0.0]

    density = 4.980 #4.980 was measured empirically of the aluminium used in the adjustable links made for the leg
    min_lengths = [0.185, 0.263, 0.143, 0.15]

    quad = quadruped(lengths,angles,base_hom,base_width[0],base_length[0],mount_angle[0],0.205, density, min_lengths)
    quad.design_variables = [lengths, bend_angles, x_rotation, mount_angle, base_length, base_width]

    return quad

def get_jt_positions(link_lengths):
    pos = []
    for i in range(len(link_lengths)):
        pos.append(sum(link_lengths[0:i]))
    pos.append(sum(link_lengths))
    return pos


def get_link_lengths(jt_positions):
    lengths = []
    for i in range(len(jt_positions)-1):
        lengths.append(jt_positions[i+1]-jt_positions[i])
    return lengths

def gen_keys(s, f, bin_size, bin_type):
    # this function generates keys for forward kinematics dictionary
    # s = start of bins (for example 0.0 meters)
    # f = finish of bins (for example 2.0 meters)
    # bin_size = size of each bin passed in as a string (in either meters or degrees)
    # bin_type = either 'c' for cartesian or 'ax' for axis angle bin
    # compute number of bins, and adjust final bin position up to be the same size as the other bins
    n_dec = comp_dec_places(bin_size) + 1
    if bin_type == 'ax':
        n_bins = int(np.ceil((f - s) / (float(bin_size)*np.pi/180.)))
    else:
        n_bins = int(np.ceil((f - s)/float(bin_size))) # using ceil makes sure to round up, so 3.3 becomes 4 bins
    key_list = [None]*n_bins
    if bin_type == 'ax':
        if 180. % float(bin_size) != 0:
            raise Exception("Please enter an axis/angle bin size that divides evenly into 180")
        else:
            bin_size = str(float(bin_size)*np.pi/180.)
    key_list[0] = s + float(bin_size)/2.
    for i in range(1,n_bins):
        key_list[i] = key_list[i-1] + float(bin_size)
        key_list[i-1] = str(round(key_list[i-1], n_dec))
    key_list[n_bins-1] = str(round(key_list[n_bins-1], n_dec))
    return key_list

def comp_dec_places(bin_size):
    # computes the number of decimal places in bin_size for rounding purposes
    if not '.' in bin_size:
        n_decimals = 0
    else:
        n_decimals = len(bin_size) - bin_size.index('.') - 1
    return n_decimals

def frange(start, end=None, inc=None):
    """A range function, that does accept float increments and hits the end no matter what..."""
    import math

    if end == None:
        end = start + 0.0
        start = 0.0
    else: start += 0.0 # force it to be a float

    if inc == None:
        inc = 1.0
    count = int(math.ceil((end - start) / inc))

    L = [None,] * count

    L[0] = start
    for i in range(1,count):
        L[i] = L[i-1] + inc
    if L[-1] != end:
        L.append(end)
    return L

def takeClosest(myList, myNumber):
    """
    Assumes myList is sorted. Returns closest value to myNumber.

    If two numbers are equally close, return the smallest number.
    """
    # note that this is 4.25x faster than using the np.argmin method
    pos = bisect_left(myList, myNumber)
    if pos == 0:
        # return [myList[0], 0]
        return 0
    if pos == len(myList):
        # return [myList[-1], -1]
        return -1
    before = myList[pos - 1]
    after = myList[pos]
    if after - myNumber < myNumber - before:
       # return [after, pos]
       return pos
    else:
       # return [before, pos - 1]
       return pos - 1

def create_fk_dict(load, load_name, bin_size_cart, bin_size_ax, cart_s, cart_f):
    if load:
        # load_name = raw_input('Enter filename to load: ')
        fileObject = open(load_name, 'r')
        fk = pickle.load(fileObject)
        ax_s = [-np.pi, -np.pi, -np.pi] # start of the axis angle bins
        ax_f = [np.pi, np.pi, np.pi] # end of the axis angle bins
        # create list of keys
        xcartkeys = gen_keys(cart_s[0], cart_f[0], bin_size_cart, 'c')
        ycartkeys = gen_keys(cart_s[1], cart_f[1], bin_size_cart, 'c')
        zcartkeys = gen_keys(cart_s[2], cart_f[2], bin_size_cart, 'c')
        xaxkeys = gen_keys(ax_s[0], ax_f[0], bin_size_ax, 'ax')
        yaxkeys = gen_keys(ax_s[1], ax_f[1], bin_size_ax, 'ax')
        zaxkeys = gen_keys(ax_s[2], ax_f[2], bin_size_ax, 'ax')
        keys = [xcartkeys, ycartkeys, zcartkeys, xaxkeys, yaxkeys, zaxkeys]
        keys_f = [list(map(float, x)) for x in keys]
    else:
        # if 90. % deg_step_size != 0.:
        #     raise Exception('Please enter a joint angle step size that cleanly divides into 90')
        ax_s = [-np.pi, -np.pi, -np.pi] # start of the axis angle bins
        ax_f = [np.pi, np.pi, np.pi] # end of the axis angle bins

        # create list of keys
        xcartkeys = gen_keys(cart_s[0], cart_f[0], bin_size_cart, 'c')
        ycartkeys = gen_keys(cart_s[1], cart_f[1], bin_size_cart, 'c')
        zcartkeys = gen_keys(cart_s[2], cart_f[2], bin_size_cart, 'c')
        xaxkeys = gen_keys(ax_s[0], ax_f[0], bin_size_ax, 'ax')
        yaxkeys = gen_keys(ax_s[1], ax_f[1], bin_size_ax, 'ax')
        zaxkeys = gen_keys(ax_s[2], ax_f[2], bin_size_ax, 'ax')
        keys = [xcartkeys, ycartkeys, zcartkeys, xaxkeys, yaxkeys, zaxkeys]
        keys_f = [list(map(float, x)) for x in keys]

        # create dictionary (6 levels deep for 3 dof in both cartesian position and orientation)
        fk = {}
        for i in range(len(keys[0])):
            fk[keys[0][i]] = {}
            for j in range(len(keys[1])):
                fk[keys[0][i]][keys[1][j]] = {}
                for k in range(len(keys[2])):
                    fk[keys[0][i]][keys[1][j]][keys[2][k]] = {}
                    for l in range(len(keys[3])):
                        fk[keys[0][i]][keys[1][j]][keys[2][k]][keys[3][l]] = {}
                        for m in range(len(keys[4])):
                            fk[keys[0][i]][keys[1][j]][keys[2][k]][keys[3][l]][keys[4][m]] = {}
                            for n in range(len(keys[5])):
                                # fk[keys[0][i]][keys[1][j]][keys[2][k]][keys[3][l]][keys[4][m]][keys[5][n]] = deque()
                                fk[keys[0][i]][keys[1][j]][keys[2][k]][keys[3][l]][keys[4][m]][keys[5][n]] = [np.float16(-1.), np.float16(0.)]
                                # fk[keys[0][i]][keys[1][j]][keys[2][k]][keys[3][l]][keys[4][m]][keys[5][n]] = np.float16(0.)
    return fk, keys, keys_f


# def get_fk(arm, load, load_name = 'bax_10deg2.pkl', deg_step_size=10, bin_size_cart='0.1', bin_size_ax='30.0', cart_s=[-0.8, -1.1, -0.7], cart_f=[1.1, 1.1, 1.3]):
# def get_fk(arm, load, load_name = 'kaa_10deg2.pkl', deg_step_size=10, bin_size_cart='0.1', bin_size_ax='30.0', cart_s=[-0.4, -1.5, -1.4], cart_f=[2.0, 1.5, 1.4]):
def get_fk(arm, load, load_name = 'kaa_10deg2.pkl', deg_step_size=10, bin_size_cart='0.1', bin_size_ax='30.0',arm_name='kaa'):
    # computes (or loads) binned forward kinematics for arm (an arm object)
    # arm is the arm object created using the functions in this file... kaa_arm() creates a kaa arm, and baxter_arm() creates a baxter arm
    # load is a boolean True if you have already computed fk, stored it, and simply want to load it; False if you want to calculate it
    # load_name is the location of the .pkl file that is loaded if load is set to True
    # deg_step_size is the size of steps you want to take in degrees in running your fk simulation in each joint
    # bin_size_cart is a string of the size of your cartesian binning (0.2 m for example)
    # bin_size_ax is a string of the bin size of your axis angle binning (in degrees)
    # arm_name is a string of the arm name to create, must be either 'kaa' for kaa for 'b' for baxter
    if arm_name == 'kaa':
        cart_s=[-0.4, -1.5, -1.4]
        cart_f=[2.0, 1.5, 1.4]
    elif arm_name == 'b': #baxter
        cart_s=[-0.8, -1.1, -0.7]
        cart_f=[1.1, 1.1, 1.3]
    else:
        print('arm_name must be b or kaa')
        raise SystemExit

    fk, keys, keys_f = create_fk_dict(load, load_name, bin_size_cart, bin_size_ax, cart_s, cart_f)
    if not load:
        print("Finished creating dictionary structure to store manipulability and weight bearing capability")
        one_newton = np.array([[0.],[0.],[1.],[0.],[0.],[0.]])
        dof = arm.getNumDof()

        # Systematically move through joint angles on the arm
        if dof == 4:
            joint1limits = [arm.links_[0].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[0].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            joint2limits = [arm.links_[1].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[1].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            joint3limits = [arm.links_[2].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[2].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            joint4limits = [arm.links_[3].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[3].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            num_loops = (joint1limits[1] - joint1limits[0] + deg_step_size)/deg_step_size;

            joint1list = frange(joint1limits[0], joint1limits[1], deg_step_size)
            joint2list = frange(joint2limits[0], joint2limits[1], deg_step_size)
            joint3list = frange(joint3limits[0], joint3limits[1], deg_step_size)
            joint4list = frange(joint4limits[0], joint4limits[1], deg_step_size)

            for i in joint1list:
                for j in joint2list:
                    for k in joint3list:
                        for l in joint4list:
                            arm.setJointAngs(np.array([i, j, k, l])*np.pi/180.) #sets joint angles
                            arm.calcJac() # this calculates the jacobians and also calls arm.calcFK()
                            T = arm.getGWorldTool() # gets homogeneous transform of end effector in world frame
                            R = copy(T[0:3, 0:3])
                            x = T[0, 3] # extract cartestian x position
                            y = T[1, 3] # extract cartestian y position
                            z = T[2, 3] # extract cartestian z position
                            ax = kin.AxFromRot(R) # gets axis angle representation of end effector orientation
                            ox = ax[0][0] # extract x portion of axis angle representation
                            oy = ax[1][0] # extract x portion of axis angle representation
                            oz = ax[2][0] # extract x portion of axis angle representation
                            # find bin in fk to store manipulability and weight bearing capability
                            xkey = keys[0][takeClosest(keys_f[0], x)]
                            ykey = keys[1][takeClosest(keys_f[1], y)]
                            zkey = keys[2][takeClosest(keys_f[2], z)]
                            oxkey = keys[3][takeClosest(keys_f[3], ox)]
                            oykey = keys[4][takeClosest(keys_f[4], oy)]
                            ozkey = keys[5][takeClosest(keys_f[5], oz)]
                            J = arm.getJacHybrid()  # this is the jacobian of the end effector in the world frame
                            J_Jt = np.matmul(J[0:3][:],J[0:3][:].T)
                            manip_sqrd =  J_Jt[0,0]*(J_Jt[1,1]*J_Jt[2,2] - J_Jt[2,1]*J_Jt[1,2]) - J_Jt[0,1]*(J_Jt[1,0]*J_Jt[2,2] - J_Jt[2,0]*J_Jt[1,2]) + J_Jt[0,2]*(J_Jt[1,0]*J_Jt[2,1] - J_Jt[2,0]*J_Jt[1,1])#determinant of J*J.T (a lot faster than np.linalg.det)
                            if manip_sqrd < 0.:
                                manip = 0.
                                print("Joint Angles: ", [i, j, k, l, m, n])
                            else:
                                manip = np.sqrt(manip_sqrd) # calculate manipulability

                            # Calculate max force liftable in the vertical direction in this configuration
                            arm.calcTorqueGravity()
                            Tau_g = -1.*arm.getTorqueGravity()
                            Tau1 = np.matmul(J.T, one_newton)
                            factor_prev = 10000000000;
                            for p in range(dof):
                                if abs(Tau1[p])>0.000001:
                                    factor = ((Tau_max[p] - abs(Tau_g[p]))/(abs(Tau1[p])))[0]
                                    if factor < factor_prev:
                                        max_force = factor
                                        factor_prev = factor

                            # # use this to append a list of manipulabilites
                            # fk[xkey][ykey][zkey][oxkey][oykey][ozkey].append([np.float16(manip), True])

                            # use this to keep the highest manipulability
                            if manip > fk[xkey][ykey][zkey][oxkey][oykey][ozkey][0]:
                                fk[xkey][ykey][zkey][oxkey][oykey][ozkey][0] = np.float16(manip)
                                fk[xkey][ykey][zkey][oxkey][oykey][ozkey][1] = np.float16(max_force)
                loops_complete = (i - joint1limits[0] + deg_step_size)/deg_step_size;
                loops_remaining = num_loops - loops_complete;
                percent_done = (loops_complete/num_loops)*100.;
                print(round(percent_done, 2), '% Complete with FK simulation')

        elif dof == 6:
            print("Running FK for Kaa")
            joint1limits = [arm.links_[0].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[0].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            joint2limits = [arm.links_[1].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[1].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            joint3limits = [arm.links_[2].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[2].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            joint4limits = [arm.links_[3].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[3].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            joint5limits = [arm.links_[4].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[4].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            joint6limits = [arm.links_[5].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[5].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            pause()
            num_loops = (joint1limits[1] - joint1limits[0] + deg_step_size)/deg_step_size;
            Tau_max = np.array([86.84, 86.84, 65., 40., 40., 40.])

            joint1list = frange(joint1limits[0], joint1limits[1], deg_step_size)
            joint2list = frange(joint2limits[0], joint2limits[1], deg_step_size)
            joint3list = frange(joint3limits[0], joint3limits[1], deg_step_size)
            joint4list = frange(joint4limits[0], joint4limits[1], deg_step_size)
            joint5list = frange(joint5limits[0], joint5limits[1], deg_step_size)
            joint6list = frange(joint6limits[0], joint6limits[1], deg_step_size)

            for i in joint1list:
                for j in joint2list:
                    for k in joint3list:
                        for l in joint4list:
                            for m in joint5list:
                                for n in joint6list:
                                    arm.setJointAngs(np.array([i, j, k, l, m, n])*np.pi/180.) #sets joint angles
                                    arm.calcJac() # this calculates the jacobians and also calls arm.calcFK()
                                    T = arm.getGWorldTool() # gets homogeneous transform of end effector in world frame
                                    R = copy(T[0:3, 0:3])
                                    x = T[0, 3] # extract cartestian x position
                                    y = T[1, 3] # extract cartestian y position
                                    z = T[2, 3] # extract cartestian z position
                                    ax = kin.AxFromRot(R) # gets axis angle representation of end effector orientation
                                    ox = ax[0][0] # extract x portion of axis angle representation
                                    oy = ax[1][0] # extract x portion of axis angle representation
                                    oz = ax[2][0] # extract x portion of axis angle representation
                                    # find bin in fk to store manipulability and weight bearing capability
                                    xkey = keys[0][takeClosest(keys_f[0], x)]
                                    ykey = keys[1][takeClosest(keys_f[1], y)]
                                    zkey = keys[2][takeClosest(keys_f[2], z)]
                                    oxkey = keys[3][takeClosest(keys_f[3], ox)]
                                    oykey = keys[4][takeClosest(keys_f[4], oy)]
                                    ozkey = keys[5][takeClosest(keys_f[5], oz)]
                                    J = arm.getJacHybrid()  # this is the jacobian of the end effector in the world frame
                                    J_Jt = np.matmul(J[0:3][:],J[0:3][:].T)
                                    manip_sqrd =  J_Jt[0,0]*(J_Jt[1,1]*J_Jt[2,2] - J_Jt[2,1]*J_Jt[1,2]) - J_Jt[0,1]*(J_Jt[1,0]*J_Jt[2,2] - J_Jt[2,0]*J_Jt[1,2]) + J_Jt[0,2]*(J_Jt[1,0]*J_Jt[2,1] - J_Jt[2,0]*J_Jt[1,1])#determinant of J*J.T (a lot faster than np.linalg.det)
                                    if manip_sqrd < 0.:
                                        manip = 0.
                                        print("Joint Angles: ", [i, j, k, l, m, n])
                                    else:
                                        manip = np.sqrt(manip_sqrd) # calculate manipulability

                                    # Calculate max force liftable in the vertical direction in this configuration
                                    arm.calcTorqueGravity()
                                    Tau_g = -1.*arm.getTorqueGravity()
                                    Tau1 = np.matmul(J.T, one_newton)
                                    factor_prev = 10000000000;
                                    for p in range(dof):
                                        if abs(Tau1[p])>0.000001:
                                            factor = ((Tau_max[p] - abs(Tau_g[p]))/(abs(Tau1[p])))[0]
                                            if factor < factor_prev:
                                                max_force = factor
                                                factor_prev = factor

                                    # # use this to append a list of manipulabilites
                                    # fk[xkey][ykey][zkey][oxkey][oykey][ozkey].append([np.float16(manip), True])

                                    # use this to keep the highest manipulability
                                    if manip > fk[xkey][ykey][zkey][oxkey][oykey][ozkey][0]:
                                        fk[xkey][ykey][zkey][oxkey][oykey][ozkey][0] = np.float16(manip)
                                        fk[xkey][ykey][zkey][oxkey][oykey][ozkey][1] = np.float16(max_force) # in newtons
                loops_complete = (i - joint1limits[0] + deg_step_size)/deg_step_size;
                loops_remaining = num_loops - loops_complete;
                percent_done = (loops_complete/num_loops)*100.;
                print(round(percent_done, 2), '% Complete with FK simulation')

        elif dof == 7:
            print("Running FK for Baxter Arm")
            joint1limits = [arm.links_[0].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[0].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            joint2limits = [arm.links_[1].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[1].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            joint3limits = [arm.links_[2].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[2].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            joint4limits = [arm.links_[3].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[3].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            joint5limits = [arm.links_[4].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[4].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            joint6limits = [arm.links_[5].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[5].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            joint7limits = [arm.links_[6].joint_.limits().bounds_lower()[0][0]*180./np.pi, arm.links_[6].joint_.limits().bounds_upper()[0][0]*180/np.pi]
            Tau_max = np.array([50., 50., 50., 50., 15., 15., 15.])
            num_loops = (joint1limits[1] - joint1limits[0] + deg_step_size)/deg_step_size;

            joint1list = frange(joint1limits[0], joint1limits[1], deg_step_size)
            joint2list = frange(joint2limits[0], joint2limits[1], deg_step_size)
            joint3list = frange(joint3limits[0], joint3limits[1], deg_step_size)
            joint4list = frange(joint4limits[0], joint4limits[1], deg_step_size)
            joint5list = frange(joint5limits[0], joint5limits[1], deg_step_size)
            joint6list = frange(joint6limits[0], joint6limits[1], deg_step_size)
            joint7list = frange(joint7limits[0], joint7limits[1], deg_step_size)

            for i in joint1list:
                for j in joint2list:
                    for k in joint3list:
                        for l in joint4list:
                            for m in joint5list:
                                for n in joint6list:
                                    for o in joint7list:
                                        arm.setJointAngs(np.array([i, j, k, l, m, n, o])*np.pi/180.) #sets joint angles
                                        arm.calcJac() # this calculates the jacobians and also calls arm.calcFK()
                                        Tr = arm.getGWorldTool() # gets homogeneous transform of end effector in world frame
                                        R = copy(Tr[0:3, 0:3])
                                        x = Tr[0, 3] # extract cartestian x position
                                        y = Tr[1, 3] # extract cartestian y position
                                        z = Tr[2, 3] # extract cartestian z position
                                        ax = kin.AxFromRot(R) # gets axis angle representation of end effector orientation
                                        ox = ax[0][0] # extract x portion of axis angle representation
                                        oy = ax[1][0] # extract x portion of axis angle representation
                                        oz = ax[2][0] # extract x portion of axis angle representation
                                        # find bin in fk to store manipulability and weight bearing capability
                                        xkey = keys[0][takeClosest(keys_f[0], x)]
                                        ykey = keys[1][takeClosest(keys_f[1], y)]
                                        zkey = keys[2][takeClosest(keys_f[2], z)]
                                        oxkey = keys[3][takeClosest(keys_f[3], ox)]
                                        oykey = keys[4][takeClosest(keys_f[4], oy)]
                                        ozkey = keys[5][takeClosest(keys_f[5], oz)]
                                        J = arm.getJacHybrid()  # this is the jacobian of the end effector in the world frame
                                        J_Jt = np.matmul(J[0:3][:],J[0:3][:].T)
                                        manip_sqrd =  J_Jt[0,0]*(J_Jt[1,1]*J_Jt[2,2] - J_Jt[2,1]*J_Jt[1,2]) - J_Jt[0,1]*(J_Jt[1,0]*J_Jt[2,2] - J_Jt[2,0]*J_Jt[1,2]) + J_Jt[0,2]*(J_Jt[1,0]*J_Jt[2,1] - J_Jt[2,0]*J_Jt[1,1])#determinant of J*J.T (a lot faster than np.linalg.det)
                                        if manip_sqrd < 0.:
                                            manip = 0.
                                            print("Joint Angles: ", [i, j, k, l, m, n])
                                        else:
                                            manip = np.sqrt(manip_sqrd) # calculate manipulability

                                        # Calculate max force liftable in the vertical direction in this configuration
                                        arm.calcTorqueGravity()
                                        Tau_g = -1.*arm.getTorqueGravity()
                                        Tau1 = np.matmul(J.T, one_newton)
                                        factor_prev = 10000000000;
                                        for p in range(dof):
                                            if abs(Tau1[p])>0.000001:
                                                factor = ((Tau_max[p] - abs(Tau_g[p]))/(abs(Tau1[p])))[0]
                                                if factor < factor_prev:
                                                    max_force = factor
                                                    factor_prev = factor

                                        # # use this to append a list of manipulabilites
                                        # fk[xkey][ykey][zkey][oxkey][oykey][ozkey].append([np.float16(manip), True])

                                        # use this to keep the highest manipulability
                                        if manip > fk[xkey][ykey][zkey][oxkey][oykey][ozkey][0]:
                                            fk[xkey][ykey][zkey][oxkey][oykey][ozkey][0] = np.float16(manip)
                                            fk[xkey][ykey][zkey][oxkey][oykey][ozkey][1] = np.float16(max_force)
                loops_complete = (i - joint1limits[0] + deg_step_size)/deg_step_size;
                loops_remaining = num_loops - loops_complete;
                percent_done = (loops_complete/num_loops)*100.;
                print(round(percent_done, 2), '% Complete with FK simulation')

        # save_bool = raw_input('Would you like to save this fk simulation (y/n)? ')
        save_bool = 'n'
        if save_bool == 'y':
            # filename = raw_input('Enter filename: ')
            filename = 'kaa_10degstep_point1_cart_bin_30deg_ax_bin_correct_jlimits.pkl'
            output = open(filename, 'wb')
            pickle.dump(fk, output)
            output.close()
    return fk, keys, keys_f

if __name__ == '__main__':
    # Plots arm in pck
    # lengths = pk.load_pickle('invalid_arm_lengths')
    # hom = pk.load_pickle('invalid_arm_hom')
    # r,t = kt.hom2ax_vec(hom)
    # print r
    # print t
    # print lengths
    lengths = np.array([.0,.27,.27,.17,.20])
    r = np.array([-np.pi/2.0,0,0])
    t = np.array([0,0,.5])
    hom = kt.ax_vec2homog(r,t)
    jt_height = 0
    arm = nasa_arm(jt_height,lengths,hom)

    # arm.setJointAngsRandom()
    # arm.calcFK()

    # pizza = kin.Arm()

    # # print np.identity(4)
    R = 0.18 #radius of shoulder mount
    d = 0.145 #offset to beginning of joint from shoulder mount rotation center
    theta = -np.pi/4.0
    base_hom = kt.ax_vec2homog(np.array([theta, 0, 0]), np.array([0.0, R + d*np.sin(theta), d*np.cos(theta)]))
    pizza = leg(np.array([0.185, 0.263, 0.143, 0.15]),np.array([0,0,0,0]),base_hom,.20)
    # # print np.array([[1.0, 0.0, 0.0, 0.0],[0.0,1.0,0.0,0.0],[0.0,0.0,1.0,0.0],[0.0,0.0,0.0,1.0]])

    pizza.setJointAngs(np.array([np.pi/2.0*np.cos(np.pi/4.0), np.pi/2.0*np.cos(np.pi/4.0), 0.0, 0.0]))
    # # # pizza.setJointAngsRandom()
    # print pizza.joint_angs_
    pizza.calcFK()


    print(pizza.g_world_tool())
    # '''angles = np.array([-np.pi/4, -np.pi/4])
    # # angles = np.array([0,0])
    # lengths = np.array([.2, .2, .2, .2, .2])
    # angs = np.array([
    #     0.789074,
    #     0.404205,
    #     0.0513736,
    #     -1.52079,
    #     1.10384,
    #     0.321514])
    # base_hom = np.identity(4)
    # arm = jabberwock(lengths, angles, base_hom)

    # pickle_arms([arm])'''
    # arm.setJointAngs(angs)
    # arm.calcFK()

    # Eval = ae.ArmEvalFK(arm)
    # Eval.print_on = False
    # Eval.XYZRes_ = 20
    # Eval.SO3Res_ = 3
    # Eval.Initialize()

    # #Scoring
    # Eval.redundancy_cutoff_ = .9
    # Eval.Run()
    # scores = unwrap_scores(Eval.scores_)
    # print scores
    # plot_fks(arm)
    # print "Success"

    # base_hom= np.eye(4)
    # my_arm = baxter_arm(base_hom)
    arm = kaa_arm()
    # arm = king_louie_left_arm()
    # arm = baxter_arm()
    # arm.setJointAngsZero()
    # arm.setJointAngs(np.array([30., 30., -30., -30., 0., 0.])*np.pi/180.) #sets joint angles
    # # # arm.setJointAngs(np.array([0., 0., -90., 0., 0., 0.])*np.pi/180.) #sets joint angles
    # arm.calcJac() # this calculates the jacobians and also calls arm.calcFK()
    # J = arm.getJacHybrid()
    # print J
    # manip = np.sqrt(np.linalg.det(np.matmul(J[0:3][:],J[0:3][:].T)))
    # print manip
    # T = arm.getGWorldTool() # gets homogeneous transform of end effector in world frame
    # print arm.getGArmTool()
    # print T
    # arm.calcTorqueGravity()
    # Tau_g = -1.*arm.getTorqueGravity()
    # print Tau_g
    # one_newton = np.array([[0.],[0.],[1.],[0.],[0.],[0.]])
    # Tau1 = np.matmul(J.T, one_newton)
    # # Tau_max = np.array([50., 50., 50., 50., 15., 15., 15.])
    # Tau_max = np.array([86.84, 86.84, 65., 40., 40., 40.])
    # factor_prev = 10000000000;
    # for i in range(len(Tau_max)):
    #     if abs(Tau1[i])>0.000001:
    #         factor = ((Tau_max[i] - abs(Tau_g[i]))/(abs(Tau1[i])))[0]
    #         if factor < factor_prev:
    #             max_force = copy(factor)
    #             factor_prev = copy(factor)
    # print max_force
    # print np.matmul(J.T, one_newton*max_force) + Tau_g
    # arm_plot = ap.SimpleArmPlotter(arm)
    # # arm_plot = ap.BaxterPlotter(arm)
    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # ax.view_init(azim=85, elev = -65)
    # arm_plot.plot(ax)
    # ap.set_axes_equal(ax)
    # plt.show()


    # fk = get_fk(arm, False, 10., '0.1', '30.0', [0., 0., 0.], [2., 2., 2.])
    # fk, keys, keys_f = get_fk(arm, True)
    fk, keys, keys_f = get_fk(arm, False, arm_name='kaa')
    # keys = get_fk(kaa, False, 30., '0.1', '30.0', [0., 0., 0.], [2., 2., 2.])
    # fileObject = open('myfile.pkl', 'r')
    # fk = pickle.load(fileObject)
    # check_memory_leak()


    # print fk

    # # test list sorter thing
    # list_test = [1., 2., 3., 4., 5.]
    # number1 = 1.2
    # number2 = 4.3
    # number4 = 4.6
    # print takeClosest(list_test, number1)
    # print takeClosest(list_test, number2)
    # print takeClosest(list_test, number4)
    # print list_test[-1]

