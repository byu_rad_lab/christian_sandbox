#include "kinematics/arm.h"
#include "kinematics/joint_ccc.h"

kinematics::Arm BELLOWSARM;

void create_arm()
{
  // First Joint
  double height = .212;
  kinematics::JointCCC joint1(height);
  kinematics::Link link1(joint1);

  Eigen::Matrix3d rot1;
  rot1<<.8536, .1464, -.5,
    .1464, .8536, .5,
    .5, -.5, .7071;
  
  Eigen::Vector3d vec1;
  vec1<<-.0148,.0148,.293;
  
  Eigen::Matrix4d hom1 = kinematics::HomFromRotVec(rot1,vec1);
  link1.g_top_end_ = hom1;

  double m1=6.534;
  Eigen::Vector3d I1;
  I1<<.108, .108, .023;
  Eigen::Vector3d pos1;
  pos1<<0.0,0.0,.115;
  
  link1.setInertiaSelf(m1,pos1,I1);

  BELLOWSARM.addLink(link1);

  // Second Joint
  kinematics::JointCCC joint2(height);
  kinematics::Link link2(joint2);

  Eigen::Matrix3d rot2;
  rot2<<.8536, .1464, -.5,
    .1464, .8536, .5,
    .5, -.5, .7071;
  
  Eigen::Vector3d vec2;
  vec2<<-.0148,.0148,.293;
  
  Eigen::Matrix4d hom2 = kinematics::HomFromRotVec(rot2,vec2);
  link2.g_top_end_ = hom2;

  double m2=6.534;
  Eigen::Vector3d I2;
  I2<<.108, .108, .023;
  Eigen::Vector3d pos2;
  pos2<<0.0,0.0,.115;
  
  link2.setInertiaSelf(m2,pos2,I2);

  BELLOWSARM.addLink(link2);

  // Third Joint
  kinematics::JointCCC joint3(height);
  kinematics::Link link3(joint3);

  Eigen::Matrix3d rot3;
  rot3<<1.0,0,0,
    0,1,0,
    0,0,1;
  
  Eigen::Vector3d vec3;
  vec3<<0.0,0.0,.025;
  
  Eigen::Matrix4d hom3 = kinematics::HomFromRotVec(rot3,vec3);
  link3.g_top_end_ = hom3;

  double m3 = 1.5;
  Eigen::Vector3d I3;
  I3<<.05,.05,.023;
  Eigen::Vector3d pos3;
  pos3<<0.0,0.0,.0125;
  
  link3.setInertiaSelf(m3,pos3,I3);
  BELLOWSARM.addLink(link3);
  
}

void calc_M(std::vector<double> qvec, Eigen::MatrixXd * M)
{
  Eigen::VectorXd q = Eigen::Map<Eigen::Matrix<double,6,1>>(qvec.data());
  BELLOWSARM.setJointAngsReal(q);
  BELLOWSARM.calcInertiaMat();
  *M = BELLOWSARM.getInertiaMat();
}
void calc_C(std::vector<double> qvec,std::vector<double> qdvec, Eigen::MatrixXd * C)
{
  
  Eigen::VectorXd q = Eigen::Map<Eigen::Matrix<double,6,1>>(qvec.data());
  Eigen::VectorXd qd = Eigen::Map<Eigen::Matrix<double,6,1>>(qdvec.data());
  BELLOWSARM.setJointAngsReal(q);
  BELLOWSARM.setJointVels(qd);
  BELLOWSARM.calcDynamicsMats();
  *C = BELLOWSARM.coriolis_mat_;
}
void calc_grav_torque(std::vector<double> qvec, Eigen::VectorXd * grav_torque)
{
  Eigen::VectorXd q = Eigen::Map<Eigen::Matrix<double,6,1>>(qvec.data());
  BELLOWSARM.setJointAngsReal(q);
  BELLOWSARM.calcTorqueGravity();
  *grav_torque = BELLOWSARM.getTorqueGravity();
}

void calc_A_B_w(std::vector<double> qvec,std::vector<double> qdvec, Eigen::MatrixXd * A, Eigen::MatrixXd * B, Eigen::MatrixXd * w)
{
  Eigen::MatrixXd KPassiveSpring(6,6);
  KPassiveSpring<< 136.336,0,0,0,0,0,
    0,136.336,0,0,0,0,
    0,0,22.119,0,0,0,
    0,0,0,22.119,0,0,
    0,0,0,0,22.119,0,
    0,0,0,0,0,22.119;

  Eigen::MatrixXd KPassiveDamper(6,6);
  KPassiveDamper<< 35.5,0,0,0,0,0,
    0,35.5,0,0,0,0,
    0,0,6.3,0,0,0,
    0,0,0,6.3,0,0,
    0,0,0,0,6.3,0,
    0,0,0,0,0,6.3;

  Eigen::MatrixXd Prs2Torque(6,12);
  Prs2Torque<< 4.629,-4.629,0,0,0,0,0,0,0,0,0,0,
    0,0,4.629,-4.629,0,0,0,0,0,0,0,0,
    0,0,0,0,1.512,-1.512,0,0,0,0,0,0,
    0,0,0,0,0,0,1.512,-1.512,0,0,0,0,
    0,0,0,0,0,0,0,0,1.512,-1.512,0,0,
    0,0,0,0,0,0,0,0,0,0,1.512,-1.512;

  Eigen::MatrixXd alphas(12,12);
  alphas.setIdentity();

  Eigen::MatrixXd M;
  Eigen::MatrixXd Minv;
  Eigen::MatrixXd C;
  Eigen::VectorXd grav_torque;  
  calc_M(qvec,&M);
  calc_C(qvec,qdvec,&C);
  calc_grav_torque(qvec,&grav_torque);

  Minv = M.inverse();  
  
  Eigen::MatrixXd eye(6,6);
  eye.setIdentity();
  Eigen::MatrixXd Azeros(6,6);
  Azeros.setZero();

  Eigen::MatrixXd A1(12,24);
  A1<< -alphas,Azeros,Azeros;
  Eigen::MatrixXd A2(6,24);
  A2<< Minv*Prs2Torque, -Minv*(KPassiveDamper+C), -KPassiveSpring;
  Eigen::MatrixXd A3(6,24);
  A3<< Azeros, eye, Azeros;
  Eigen::MatrixXd Acont(24,24);
  Acont<<A1,A2,A3;

  Eigen::MatrixXd Bzeros(12,12);
  Bzeros.setZero();

  Eigen::MatrixXd Bcont(24,12);
  Bcont<< alphas,Bzeros;

  Eigen::MatrixXd wzeros(6,1);
  wzeros.setZero();
  Eigen::MatrixXd wcont(24,1);
  wcont<< wzeros, grav_torque, wzeros;

  *A = Acont;
  *B = Bcont;
  *w = wcont;
}

void calc_discrete_A_B_w(std::vector<double> q,std::vector<double> qd, double dt, Eigen::MatrixXd * A_d, Eigen::MatrixXd * B_d, Eigen::MatrixXd * w_d)
{
  int NUM_INPUTS = 12;
  int NUM_STATES = 24;
  Eigen::MatrixXd A(NUM_STATES,NUM_STATES);
  Eigen::MatrixXd B(NUM_STATES,NUM_INPUTS);
  Eigen::MatrixXd w(NUM_STATES,1);

  calc_A_B_w(q,qd,&A,&B,&w);

  Eigen::MatrixXd eye(A.rows(),A.cols());
  eye.setIdentity();
  *A_d = (eye - A*dt).inverse();
  *B_d = (eye - A*dt).inverse()*B*dt;
  *w_d = w*dt;
}

int main()
{
  create_arm();
  
  double dt = .01;
  Eigen::MatrixXd A(24,24);
  Eigen::MatrixXd B(24,12);
  Eigen::MatrixXd w(24,1);
  std::vector<double> q(6,1.0);
  std::vector<double> qd(6,1.0);
  calc_discrete_A_B_w(q,qd,dt,&A,&B,&w);

  std::cout<<A<<std::endl;
  std::cout<<"\n"<<std::endl;  
  std::cout<<B<<std::endl;  
  std::cout<<"\n"<<std::endl;  
  std::cout<<w<<std::endl;  

  return 0;
}
