#!/usr/bin/env python

# Before running this you have to source the models and utils workspaces

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

import kinematics as kin
import copy

fig = plt.figure()
ax = fig.gca(projection='3d')

def plot_line(ax, a, b, color, linewidth_custom, alpha=1.0):  # Ax must be projection 3d
    ax.plot([a[0], b[0]], [a[1], b[1]], [a[2], b[2]], color=color, linewidth=linewidth_custom, alpha=alpha)
    # Error here may mean you forgot ax projection


def plot_cylinder(ax, g1, g2, r=0.05, color='k', n=80):
    th_vals = [2 * np.pi * i / n for i in range(n+1)]
    circle = [[r * np.sin(th), r * np.cos(th), 0.0, 1.0] for th in th_vals]
    circle = np.array(circle).T

    circle1 = g1.dot(circle)
    g3 = copy.copy(g1)  # Gets rid of twist in Z
    g3[0:3, 3] = g3[0:3, 3]+(g2[0:3, 3]-g1[0:3, 3])
    circle2 = g3.dot(circle)

    for i in range(circle1.shape[1] - 1):
        plot_line(ax, circle1[:, i], circle1[:, i+1], color, 1.0)
        plot_line(ax, circle2[:, i], circle2[:, i+1], color, 1.0)
        plot_line(ax, circle1[:, i], circle2[:, i], color, 1.0)
        plot_line(ax, circle1[:, i], circle2[:, i+1], color, 1.0)


def set_axes_equal(ax):
    '''
    Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    '''

    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = x_limits[1] - x_limits[0]
    y_range = y_limits[1] - y_limits[0]
    z_range = z_limits[1] - z_limits[0]

    x_mean = np.mean(x_limits)
    y_mean = np.mean(y_limits)
    z_mean = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.4*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_mean - plot_radius, x_mean + plot_radius])
    ax.set_ylim3d([y_mean - plot_radius, y_mean + plot_radius])
    ax.set_zlim3d([z_mean - plot_radius, z_mean + plot_radius])



# Create an arm
height = 0.1
lengths = np.array([1.0,0.5])
arm = kin.Arm()
num_joints = 3

jt_limit=np.pi/2

# Create the first joint
joint = kin.JointCCC(.212)

# Add the first joint to the first link
l = kin.Link(joint)

# Specify the transformation from the top of the joint to the end of the link
rot = np.array([[.8536, .1464, -.5],
                [.1464, .8536, .5],
                [.5, -.5, .7071]])

vec = np.array([[-.0148],
                [.0148],
                [.293]])
hom0 = kin.HomFromRotVec(rot,vec)
l.g_top_end_ = np.array(hom0)

# Specify the inertia of this joint/link
m = 6.534
I = np.array([.108, .108, .023])
pos = np.array([0.0, 0, .115]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
l.setInertiaSelf(m, pos, I)

# Add the link to the robot
arm.addLink(l)





# Create the third joint
joint = kin.JointCCC(.212)

# Add the second joint to the third link
l = kin.Link(joint)

# Specify the transformation from the top of the joint to the end of the link
rot = np.array([[.8536, .1464, -.5],
                [.1464, .8536, .5],
                [.5, -.5, .7071]])

vec = np.array([[-.0148],
                [.0148],
                [.293]])

hom0 = np.array(kin.HomFromRotVec(rot,vec))
l.g_top_end_ = np.array(hom0)

# Specify the inertia of this joint/link
m = 6.534
I = np.array([.108, .108, .023])
pos = np.array([0.0, 0, .115]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
l.setInertiaSelf(m, pos, I)

# Add the link to the robot
arm.addLink(l)



# Create the second joint
joint = kin.JointCCC(.212)

# Add the second joint to the second link
l = kin.Link(joint)

# Specify the transformation from the top of the joint to the end of the link
# rot = np.array([[.8536, .1464, -.5],
#                 [.1464, .8536, .5],
#                 [.5, -.5, .7071]])
rot = np.eye(3)

vec = np.array([[0.0],
                [0.0],
                [.025]])

hom0 = np.array(kin.HomFromRotVec(rot,vec))
l.g_top_end_ = np.array(hom0)

# Specify the inertia of this joint/link
m = 6.534
I = np.array([.108, .108, .023])
pos = np.array([0.0, 0, .115]) #VERY IMPORTANT TO HAVE 0.0 (EIGEN NUMPY ISSUES)
l.setInertiaSelf(m, pos, I)

# Add the link to the robot
arm.addLink(l)






# Set the initial conditions
q = np.zeros([num_joints*2,1])
qd = np.zeros([num_joints*2,1])
arm.setJointAngsReal(q)
arm.calcFK()

# Apply a constant torque and see where the arm goes
qdes = np.ones([num_joints*2,1])*0.0
qdes = np.array([[1.0],
                 [1.0],
                 [1.0],
                 [1.0],
                 [1.0],
                 [1.0]])*-.0             

Kp = np.eye(num_joints*2)*10
Kd = np.eye(num_joints*2)

arm.lengths = [.25,.25,.25,.25]
arm.bend_angles = [0,0,0,0]

# Plot the arm
for i in range(0,len(arm.links())):
    plot_cylinder(ax, arm.links_[i].g_world_bot(), arm.links_[i].g_world_top(), r=.05, color='b', n=10)
    plot_cylinder(ax, arm.links_[i].g_world_top(), arm.links_[i].g_world_end(), r=.05, color='r', n=10)            


for i in range(0,1000):

    # Update the arm configuration
    arm.setJointAngsReal(q)
    arm.setJointVels(qd)
    arm.calcFK()

    # Calculate the impedance control torques
    tau_pressure = np.dot(Kp,(qdes-q)) - np.dot(Kd,qd)

    # Calculate the gravity torques
    arm.calcTorqueGravity()
    tau_gravity = arm.getTorqueGravity()
    print(tau_gravity)

    # Calculate the position based (parasitic) torques
    # tau_position = 
    

    # Calculate the dynamics matrices
    arm.calcDynamicsMats()
    M = arm.getInertiaMat()

    # print("")
    # print(q)
    
    M_inv = np.linalg.inv(M)

    # Calculate joint accelerations
    qdd = np.dot(M_inv,tau - arm.getTorqueCoriolis())

    # Integrate to get velocity and position
    qd = qd + .01*qdd
    q = q + .01*qd
    print(q)

    if(i%10==0):
        ax.clear()
        for i in range(0,len(arm.links())):
            plot_cylinder(ax, arm.links_[i].g_world_bot(), arm.links_[i].g_world_top(), r=.05, color='r', n=10)
            plot_cylinder(ax, arm.links_[i].g_world_top(), arm.links_[i].g_world_end(), r=.05, color='b', n=10)            
        
        
        set_axes_equal(ax)
        plt.ion()
        plt.show()
        plt.pause(.000001)

# set_axes_equal(ax)
# plt.ioff()
# plt.show()
        
    



