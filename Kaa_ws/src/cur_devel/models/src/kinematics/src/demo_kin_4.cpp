#include <stdio.h>
#include <iostream>

#include "kinematics/types.h"
#include "kinematics/arm.h"
#include "kinematics/link.h"
#include "kinematics/joint_rot.h"
#include "kinematics/joint_ccc.h"

using namespace kinematics;
// using kinematics::Arm;
// using kinematics::Vec;
// using kinematics::Mat;

// using kinematics::Frame;
// using kinematics::Vec3;
// using kinematics::Vec6;
// using kinematics::Hom;
// using kinematics::Rot;
// using kinematics::Link;
// using kinematics::JointRot;
// using kinematics::JointCCC;

using std::cout;
using std::endl;

//This is the mixed joint type formulation
int main() {
  // This shows how an arm can push on the ground
  Vec lengths = Vec(7);
  lengths << .2,.2,.2,.2,.2,.2,.2;
  double height = 0.2;
  Arm arm;  // height, lengths, kinematics::kJointTypeCC1D);
  for (unsigned int i = 0; i < lengths.size(); i++) {
    // Rotational Joint
    Link link;
    link = Link(JointRot(0));
    link.setGTopEndXYZ(0, 0, 0);
    link.setInertiaSelf(0, Vec3(0, 0, 0), Vec3(0, 0, 0));
    arm.addLink(link);

    // CCC Joint
    link = Link(JointCCC(height));
    link.setGTopEndXYZ(0, 0, lengths[i]);
    
    double m = 1.0 * lengths[i];
    double r = 0.5 * 0.1651;  // cylinder radius
    double Ix = m * (3.0 * r * r + lengths[i]) / 12.0;
    double Iy = m * (3.0 * r * r + lengths[i]) / 12.0;
    double Iz = m * (r * r) / 2.0;
    link.setInertiaSelf(m, Vec3(0.0, 0.0, lengths[i] / 2.0), Vec3(Ix, Iy, Iz));
    arm.addLink(link);
    // Rotate actuators 90 degrees relative to eachother: very important
        // l.g_top_end_ = np.dot(g, l.g_top_end_)
    Vec3 w;
    w << 0, 0, -M_PI / 2.0;
    Hom hom = kinematics::HomRotate(w);
    arm.links_[2*i+1].g_top_end_ *= hom;
  }
  arm.calcJac();

  Vec angs_arm(arm.num_dof_);
  cout << "arm dof" << endl << arm.num_dof_ << endl;
  angs_arm.setZero();
  angs_arm(0) = 90*3.14159 / 180;  // bend 45 degrees
  angs_arm(1) = 90*3.14159 / 180;  // bend 45 degrees
  // angs_arm(2) = 45*3.14159 / 180;  // bend 45 degrees
  // angs_arm(4) = 90*3.14159 / 180;  // bend 45 degrees
  arm.setJointAngs(angs_arm);

  cout << "joint_angs_" << endl;
  cout << endl;
  cout << arm.joint_angs_<< endl;

  Vec torque_external(arm.num_dof_);
  torque_external.setZero();

  // set mount
  Vec3 th;
  th << -M_PI / 2.0, 0.0, 0.0;
  Rot rot = kinematics::AxToRot(th);
  Vec3 z;
  z << 0, 0, .5;
  Hom g_world_arm;
  g_world_arm.setIdentity();
  g_world_arm.topLeftCorner(3, 3) = rot;
  g_world_arm.topRightCorner(3, 1) = z;
  arm.g_world_arm_ = g_world_arm;
  arm.calcJac();

  for (unsigned int i = 0; i < arm.links_.size(); i++) {
    cout << "transforms world - link -" << i << endl;
    cout << arm.links_[i].g_world_end_ << endl;
  }
  ////////////////////////BEGIN CHECKING CalcForceStatic ///////////////////////

  Vec3 direction;
  direction << 0, 0, -1;  // How much can we press downwards?
  double maxforce;
  maxforce = arm.calcMaxForceLimitTorque(direction);
  cout << "Max Force " << endl
       << maxforce << endl
       << endl;

  // Apply to last link
  int link_num = arm.links_.size() - 1;
  // choose a frame
  // Frame frame = Frame::Spatial;
  Frame frame = Frame::Hybrid;
  // Frame frame = Frame::Body;

  Vec6 wrench = Vec6::Zero();
  // wrench(0) = maxforce;
  wrench(2) = -maxforce;  // z direction Apply the max force downwards and check joint torques

  // Define a offset for torque relative to the frame you choose!
  Hom offset = Hom::Identity();
  // HYBRID OFFSET
  offset.topRightCorner(3, 1) = arm.links_[link_num].g_world_end_.topRightCorner(3, 1) -
                                arm.links_[link_num].g_world_com_.topRightCorner(3, 1);

  // BODY OFFSET
  // offset(2, 3) = lengths[link_num]/2.0;
  // cout << "offset" << endl << offset << endl;

  cout << "wrench" << endl;
  cout << wrench << endl;
  arm.setTorqueExternal(link_num, frame, wrench, offset);

  cout << "joint torques:\n" << arm.calcTorqueJointStatic(Vec6::Zero()) << endl
       << endl;
  cout << "torque_gravity:\n" << arm.getTorqueGravity() << endl
       << endl;
  cout << "torque external:\n"
       << "base\n" << arm.getTorqueExternal() << "\ntop" << endl;
  cout << endl;

  return 0;
}
