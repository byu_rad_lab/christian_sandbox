#define _USE_MATH_DEFINES
#include "kinematics/types.h"
#include <cmath>
#include <iostream>

using std::cout;
using std::endl;

namespace kinematics {

Rot HatVec(const Vec3& p) {
  Rot phat;
  phat << 0, -p(2), p(1), p(2), 0, -p(0), -p(1), p(0), 0;
  return phat;
}

Hom HatTwist(const Vec& xi) {
  Hom xi_hat;
  xi_hat << 0.0, -xi(5), xi(4), xi(0),  //
      xi(5), 0.0, -xi(3), xi(1),        //
      -xi(4), xi(3), 0, xi(2),          //
      0.0, 0.0, 0.0, 0.0;               //
  return xi_hat;
}

Vec3 VeeRot(const Rot& rot) { return Vec3(-rot(1, 2), rot(0, 2), -rot(0, 1)); }

Twist VeeHom(const Hom& hom) {
  Twist t;
  t << hom(0, 3), hom(1, 3), hom(2, 3), -hom(1, 2), hom(0, 2), -hom(0, 1);
  return t;
}

Hom HomGLFromCV(const Hom& cv) {
  Hom gl_from_cv = HomFromAxVec(Vec3(M_PI, 0.0, 0.0), Vec3::Zero());
  return gl_from_cv * cv;
}

Vec3 AxFromRot(const Rot& rot) {
  Quat q(rot);
  return AxFromQuat(q);
}

Rot RotFromAx(const Vec3& w) {
  Rot R;
  R.setIdentity();
  double th = sqrt(w[0] * w[0] + w[1] * w[1] + w[2] * w[2]);
  if (th < 1e-9) {
    return R;
  } else {
    Vec3 w2 = w / th;
    Rot wh = VecHat(w2);
    return R + wh * sin(th) + wh * wh * (1.0 - cos(th));
  }
}

Quat QuatFromAx(const Vec3& ax) {
  double mag_half = 0.5 * ax.norm();
  if (mag_half < 1e-6) {
    return Quat(cos(mag_half), ax[0], ax[1], ax[2]);
  } else {
    double smh = sin(mag_half);
    Vec3 axn = ax.normalized();
    return Quat(cos(mag_half), smh * axn[0], smh * axn[1], smh * axn[2]);
  }
}

Vec3 AxFromQuat(const Quat& q) {
  AngAx aa(q);
  if (aa.angle() > M_PI) {
    return (aa.angle() - 2.0 * M_PI) * aa.axis();
  } else {
    return aa.angle() * aa.axis();
  }
}

Adj AdjFromHom(const Hom& hom) {
  Rot R = hom.topLeftCorner<3, 3>();
  Vec3 p = hom.topRightCorner<3, 1>();
  Rot phat = VecHat(p);

  Adj adj;
  adj << R, phat* R, Rot::Zero(), R;
  return adj;
}

Hom HomInv(const Hom& hom) {
  Rot R = hom.topLeftCorner<3, 3>();
  Vec3 p = hom.topRightCorner<3, 1>();
  Hom inv;
  inv << R.transpose(), -R.transpose() * p, 0.0, 0.0, 0.0, 1.0;
  return inv;
}

Hom HomTranslate(const double x, const double y, const double z) {
  Hom hom;
  hom.setIdentity();
  hom(0, 3) = x;
  hom(1, 3) = y;
  hom(2, 3) = z;

  return hom;
}

Hom HomFromAxVec(const Vec3& ax, const Vec3& vec) {
  return HomFromRotVec(RotFromAx(ax), vec);
}

Hom HomFromRotVec(const Rot& rot, const Vec3& vec) {
  Hom hom = Hom::Identity();
  hom.topLeftCorner<3, 3>() = rot;
  hom.topRightCorner<3, 1>() = vec;
  return hom;
}

Rot RotScale(const Rot& rot, double scale) {
  Vec3 ax = AxFromRot(rot);
  return RotFromAx(scale * ax);
}

// Returns a rotational equivalent of r1*(1 - scale) + r2*scale
Rot RotInterp(const Rot& r1, const Rot& r2, double scale) {
  Rot r_delta = r1.transpose() * r2;
  return r1 * RotScale(r_delta, scale);
}

Hom HomScale(const Hom& hom, double scale) {
  Vec3 ax = AxFromRot(hom.topLeftCorner(3, 3));
  Vec3 vec = hom.topRightCorner(3, 1);
  return HomFromAxVec(scale * ax, scale * vec);
}

Hom HomInterp(const Hom& h1, const Hom& h2, double scale) {
  Hom h_delta = HomInv(h1) * h2;
  return h1 * HomScale(h_delta, scale);
}

Twist TwistFromHom(const Hom& hom) {
  Rot R = hom.topLeftCorner<3, 3>();
  Vec3 p = hom.topRightCorner<3, 1>();
  double cos_th = (R.trace() - 1.0) / 2.0;
  double th;
  Vec3 v, w;

  if (cos_th > 1.0) {
    th = 0.0;
  } else if (cos_th < -1.0) {
    th = M_PI;
  } else {
    th = acos(cos_th);
  }

  Twist out;
  if (th < 1e-6) {
    // TODO(Tom): Use small angle approximation here.
    w << 0.0, 0.0, 0.0;
    v = p;
    out << v(0), v(1), v(2), w(0), w(1), w(2);
  } else {
    w << (R(2, 1) - R(1, 2)), (R(0, 2) - R(2, 0)), (R(1, 0) - R(0, 1));
    w = w / (2.0 * sin(th));
    Rot wh = VecHat(w);
    Rot A = ((Rot::Identity() - R) * wh) + w * w.transpose() * th;

    // You really do need this inversion, see MLS p. 43
    v = A.colPivHouseholderQr().solve(p);

    w = w.transpose() * th;
    v = v.transpose() * th;
    out << v(0), v(1), v(2), w(0), w(1), w(2);
  }
  return out;
}

Vec clamp(Vec w, double d, bool norm_type) {
  double n;
  if (norm_type == true) {  // normal type
    n = w.norm();
  } else {
    n = w.cwiseAbs().maxCoeff();
  }
  if (n < d) {
    return w;
  } else {
    return w * d / n;
  }
}

double RotError(const Rot& r1, const Rot& r2) {
  return 0.5 * (Rot::Identity() - r1.transpose() * r2).trace();
}

double Vec3Error(const Vec3& v1, const Vec3& v2) { return (v1 - v2).norm(); }

double HomError(const Hom& g1, const Hom& g2, double error_ratio) {
  double error_rot =
      error_ratio * RotError(g1.topLeftCorner(3, 3), g2.topLeftCorner(3, 3));
  double error_vec = Vec3Error(g1.topRightCorner(3, 1), g2.topRightCorner(3, 1));
  return sqrt(error_rot * error_rot + error_vec * error_vec);
}

void Inertia::Initialize(void) {
  mass = 0;
  inertia.setZero();
  vec.setZero();
}

void Inertia::CheckIsDiagonal(void) {
  // check to make sure inertia is diagonal
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      if (i != j && inertia(i, j) > 1e-9) {
        printf("Warning: non-diagonal inertia element found!\n");
        printf("Needs to be diagonalized, which hasn't been implemented yet.\n");
        printf("Maybe your link inertia wasn't initialized properly.\n");
      }
    }
  }
}

void Inertia::set_inertia_diag(double Ix, double Iy, double Iz) {
  inertia.setZero();
  inertia(0, 0) = Ix;
  inertia(1, 1) = Iy;
  inertia(2, 2) = Iz;
}

void Inertia::set_vec(double x, double y, double z) {
  vec(0) = x;
  vec(1) = y;
  vec(2) = z;
}

InertiaTensor InertiaParallelAxis(Vec3 vec, double mass) {
  InertiaTensor outer = vec * vec.transpose();
  double inner = vec.transpose() * vec;
  return mass * ((inner)*InertiaTensor::Identity() - outer);
}

Inertia InertiaCombine(Inertia i1, Inertia i2) {
  Inertia i3 = Inertia();
  i3.mass = i1.mass + i2.mass;
  i3.vec = (i1.vec * i1.mass + i2.vec * i2.mass) / i3.mass;

  Vec3 r13 = i1.vec - i3.vec;
  Vec3 r23 = i2.vec - i3.vec;

  // i1, i2.inertia must be rotated into i3 frame before adding (implement if needed)
  // rotate with R * inertia * R.transpose()
  InertiaTensor j13 = i1.inertia + InertiaParallelAxis(r13, i1.mass);
  InertiaTensor j23 = i2.inertia + InertiaParallelAxis(r23, i2.mass);

  // # sum both inertia tensors
  InertiaTensor j3 = j13 + j23;

  // diagonalize j3 and set i3.rot (implement if needed)
  // SVD of positive definite matrix give M = UDU*
  //   where U is orthonormal
  //   i3.rot = U, but axes may be oriented unusually
  i3.inertia = j3;

  return i3;
}

struct metrics {
  double velocity_ellipse_volume_;
  double max_force_neg_Z_;
};

}  // namespace kinematics
