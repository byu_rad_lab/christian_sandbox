#include "kinematics/yaml.h"

namespace YAML {

// this can't be a struct convert as it can't return abstract jointbase
void DecodeJointBase(const YAML::Node& node, kinematics::JointBase* joint) {

  if (node["angs"]) {
    joint->angs_ = node["angs"].as<kinematics::Vec>();
  }
  if (node["vels"]) {
    joint->vels_ = node["vels"].as<kinematics::Vec>();
  }
  if (node["joint_limits_circular"]) {
    YAML::Node limits = node["joint_limits_circular"];
    double radius = limits["radius"].as<double>(M_PI / 2.0);
    double phase = limits["phase"].as<double>(0.0);
    int num_constraints = limits["num_constraints"].as<int>(8);
    kinematics::Vec center = limits["center"].as<kinematics::Vec>();
    joint->limits()->setLimitsCircular(radius, center, num_constraints, phase);
  }
  if (node["limits_lower"]) {
    joint->limits_lower_ = node["limits_lower"].as<kinematics::Vec>();
  }
  if (node["limits_upper"]) {
    joint->limits_upper_ = node["limits_upper"].as<kinematics::Vec>();
  }
  if (node["spring_constant"]) {
    joint->spring_constant_ = node["spring_constant"].as<kinematics::Vec>();
  }
  if (node["damping_constant"]) {
    joint->damping_constant_ = node["damping_constant"].as<kinematics::Vec>();
  }
  joint->checkMatSizes();  // ensure matrix sizes are right
}

}  // namespace YAML

namespace kinematics {

void PrintParserDemoUsage(void) {
  std::cout << "usage: \n"
            << "    ./demo_parse_type filepath/filename.yaml\n"
            << "         OR\n"
            << "    rosrun kinematics demo_parse_type filepath/filename.yaml\n"
            << "    file paths are relative to where it's run\n";
}

YAML::Node LoadYAMLCommandLine(int argc, char* argv[]) {
  std::cout << "Loading file.\n";
  std::cout << "argc = " << argc << std::endl;
  YAML::Node node;
  std::string filename;
  if (argc > 1) {
    filename = argv[1];
    std::cout << "parsing filename = " << filename << std::endl;
  } else {
    std::cout << "Error:  you need to input a file name.\n";
    PrintParserDemoUsage();
    throw;
  }
  try {
    node = YAML::LoadFile(filename);
  } catch (YAML::BadFile& e) {
    std::cout << "Error: there was a problem with your file.\n";
    PrintParserDemoUsage();
    throw e;
  }
  return node;
}

}  // namespace kinematics
