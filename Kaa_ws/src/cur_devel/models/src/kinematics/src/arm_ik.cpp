#include "kinematics/arm.h"
#include "kinematics/sdls.h"
#include "kinematics/arm_ik.h"
#include "kinematics/arm.h"
#include <stdio.h>
#include <iostream>

namespace kinematics {

ArmIK::ArmIK() {}

ArmIK::ArmIK(Arm* arm) : arm_(arm) {}

void ArmIK::calcIK(const Hom& g_des) {
  status_ = 0;
  for (int i = 0; i < max_iter_; i++) {
    IKStep(g_des);

    // Check Error
    err_t_ = HomError(arm_->links_.back().g_world_end_, g_des, error_ratio_);

    if (err_t_ < end_tol_) {
      status_ = 1;
      solve_iteration_ = i + 1;
      break;
    }
  }
}

void ArmIK::IKStep(const Hom& g_des) {
  Hom g_tool = arm_->links_.back().g_world_end_;
  Hom g_tool_inv = HomInv(g_tool);
  Adj adj_tool = HomToAdj(g_tool_inv);
  Hom g_move_s = g_des * g_tool_inv;
  Hom dg = g_des - g_tool;

  Twist v_move_s = HomToTwist(g_move_s);
  Twist v_move_b = adj_tool * v_move_s;

  Vec v_move;
  Eigen::MatrixXd J;

  switch (mode_) {

    case POS_ONLY:
      v_move = clamp(dg.topRightCorner(3, 1), e_clamp_);
      J = arm_->links_.back().jac_hybrid_.topRows<3>();
      break;

    case ROT_ONLY:
      v_move = clamp(v_move_b.tail<3>(), e_clamp_);
      J = arm_->links_.back().jac_body_.bottomRows<3>();
      break;

    case SIXDOF:
      Vec c1 = clamp(v_move_s.head(3), e_clamp_);
      Vec c2 = clamp(v_move_s.tail<3>(), e_clamp_);
      Vec temp(6);
      temp << c1, c2;
      v_move = temp;
      J = arm_->getJacSpatial();
  }

  sdls_solver.J_ = J;
  sdls_solver.e_ = v_move;
  sdls_solver.n_rows_ = J.rows();

  Vec output = sdls_solver.solve();

  // TODO: SUBTASK
  // Mat null = sdls_solver.null_;
  // Mat P = null.transpose()*null;
  arm_->setJointAngsDelta(output);
  arm_->calcJac();
}
}
