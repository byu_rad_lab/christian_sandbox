#include <stdio.h>
#include <iostream>

#include "kinematics/types.h"
#include "kinematics/arm.h"

using kinematics::Arm;
using kinematics::Vec;
using kinematics::Mat;

using kinematics::Frame;
using kinematics::Vec6;
using kinematics::Hom;
using kinematics::Rot;

using std::cout;
using std::endl;

int main() {

  Vec lengths = Vec(2);
  lengths << 1, 1;
  double height = 0.01;

  Arm arm(height, lengths, kinematics::kJointTypeCC1D);

  Vec angs_arm(arm.num_dof_);
  angs_arm.setZero();
  angs_arm(0) = 3.14159 / 2.0;  
  angs_arm(1) = 3.14159 / 2.0;  
  // angs_arm(2) = -3.14159 / 2.0; 
  arm.setJointAngs(angs_arm);

  Vec torque_external(arm.num_dof_);
  torque_external.setZero();
  arm.calcJointAccels(torque_external);
  arm.calcFK();
  for (unsigned int i = 0; i < arm.links_.size(); i++) {
    // Rotate actuators 90 degrees relative to eachother: very important
    Hom hom;
    kinematics::Vec3 w;
    w << 0, 0, -M_PI / 2.0;
    hom = kinematics::HomRotate(w);
    arm.links_[i].g_top_end_ *= hom;
  }
  //set mount
  kinematics::Vec3 th;
  th << -M_PI / 2.0, 0.0, 0.0;
  Rot rot = kinematics::AxToRot(th);
  kinematics::Vec3 z;
  z << 0, 0, .5;
  Hom g_world_arm;
  g_world_arm.setIdentity();
  g_world_arm.topLeftCorner(3, 3) = rot;
  g_world_arm.topRightCorner(3, 1) = z;
  arm.g_world_arm_ = g_world_arm;

  arm.calcJac();
  for (unsigned int i = 0; i < arm.links_.size(); i++) {
      cout << "transforms world - link -" << i << endl;
      cout << arm.links_[i].g_world_com_ << endl;
  }

  cout << "g_worled_end" << endl << arm.links_.back().g_world_end_ << endl;
  //
  //
  //Check where we end 
  // for (unsigned int i = 0; i < arm.links_.size(); i++) {
  //   cout << "Link Transforms" << i << endl;
  //   cout << "arm_bot" << endl;
  //   cout << arm.links_[i].g_world_bot_ << endl;
  //   cout << "arm_top" << endl;
  //   cout << arm.links_[i].g_world_top_ << endl;
  //   cout << "arm_com" << endl;
  //   cout << arm.links_[i].g_world_com_ << endl;
  //   cout << "arm_end" << endl;
  //   cout << arm.links_[i].g_world_end_ << endl;

  // }

  // Arm arm2 = arm;  // a test of copying

  // bool print_all = true;
  bool print_all = false;

  if (print_all) {

    cout << "arm.jac_spatial_ = \n" << arm.jac_spatial_ << endl;
    for (auto part = arm.partials_.begin(); part < arm.partials_.end(); part++) {
      printf("\n\n ### jac_spatial_ partial derivative %d ###\n",
             (int)std::distance(arm.partials_.begin(), part));
      cout << part->jac_spatial_ << endl;
    }

    for (auto& link : arm.links_) {
      cout << "link.joint_->g_bot_top_ = \n" << link.joint_->g_bot_top_ << endl;
    }

    for (auto& link : arm.links_) {
      cout << "link.joint_->jac_ = \n" << link.joint_->jac_ << endl;
    }

    cout << "Spatial jacobian is:\n" << arm.jac_spatial_ << endl;

    cout << "Printing g_world_bot_\n";
    for (auto& link : arm.links_) {
      cout << link.g_world_bot_ << endl
           << endl;
    }

    cout << "Printing g_world_top_\n";
    for (auto& link : arm.links_) {
      cout << link.g_world_top_ << endl
           << endl;
    }

    cout << "Printing g_world_com_\n";
    for (auto& link : arm.links_) {
      cout << link.g_world_com_ << endl
           << endl;
    }

    cout << "Printing g_world_end_\n";
    for (auto& link : arm.links_) {
      cout << link.g_world_end_ << endl
           << endl;
    }

    cout << "Printing jac_body_\n";
    for (auto& link : arm.links_) {
      cout << link.jac_body_ << endl
           << endl;
    }

    for (auto link = arm.links_.begin(); link < arm.links_.end(); link++) {
      printf("\n\n ### Link %d ###\n", (int)std::distance(arm.links_.begin(), link));
      for (auto part = link->partials_.begin(); part < link->partials_.end(); part++) {
        cout << "\npart->g_world_bot_ = \n" << part->g_world_bot_ << endl;
      }
    }

    for (auto link = arm.links_.begin(); link < arm.links_.end(); link++) {
      printf("\n\n ### Link %d ###\n", (int)std::distance(arm.links_.begin(), link));
      for (auto part = link->partials_.begin(); part < link->partials_.end(); part++) {
        cout << "\npart->g_world_com_ = \n" << part->g_world_com_ << endl;
      }
    }

    for (auto link = arm.links_.begin(); link < arm.links_.end(); link++) {
      printf("\n\n ### Link %d ###\n", (int)std::distance(arm.links_.begin(), link));
      for (auto part = link->partials_.begin(); part < link->partials_.end(); part++) {
        cout << "\npart->ad_world_bot_ = \n" << part->ad_world_bot_ << endl;
      }
    }

    for (auto link = arm.links_.begin(); link < arm.links_.end(); link++) {
      printf("\n\n ### Link %d ###\n", (int)std::distance(arm.links_.begin(), link));
      for (auto part = link->partials_.begin(); part < link->partials_.end(); part++) {
        cout << "\npart->ad_inv_world_com_ = \n" << part->ad_inv_world_com_ << endl;
      }
    }

    for (auto link = arm.links_.begin(); link < arm.links_.end(); link++) {
      printf("\n\n ### Link %d ###\n", (int)std::distance(arm.links_.begin(), link));
      for (auto part = link->partials_.begin(); part < link->partials_.end(); part++) {
        cout << "part->ad_world_bot_ = \n" << part->ad_world_bot_ << endl;
      }
    }

    for (auto link = arm.links_.begin(); link < arm.links_.end(); link++) {
      printf("\n\n ### Link %d ###\n", (int)std::distance(arm.links_.begin(), link));
      for (auto part = link->partials_.begin(); part < link->partials_.end(); part++) {
        cout << "\npart->jac_body_ = \n" << part->jac_body_ << endl;
      }
    }
  }

  cout << "##### torque stuff #####" << endl;
  cout << "ANGLES"<< endl;
  cout<< arm.joint_angs_<<endl;

  arm.calcJac();
  arm.calcTorqueGravity();

  int link_num = 1;

  // choose a frame
  // Frame frame = Frame::Spatial;
  Frame frame = Frame::Hybrid;
  // Frame frame = Frame::Body;

  Vec6 wrench = Vec6::Zero();
  // wrench(0) = 1000.0;  // x direction
  // wrench(1) = 1000.0;  // y direction
  wrench(2) = -10.0;  // z direction

  //Define a offset for torque relative to the frame you choose!
  Hom offset = Hom::Identity();
  // HYBRID OFFSET
  offset.topRightCorner(3,1) =
  arm.links_[link_num].g_world_end_.topRightCorner(3,1)-arm.links_[link_num].g_world_com_.topRightCorner(3,1);

  // BODY OFFSET
  // offset(2, 3) = lengths[link_num]/2.0;
  cout << "offset" << endl << offset << endl;

  cout << "wrench" << endl;
  cout << wrench << endl;
  arm.setTorqueExternal(link_num, frame, wrench, offset);
  // arm.setForceAtEE(frame, wrench.head(3));
  // arm.addForceAtEE(frame, wrench.head(3));  // double the force

  cout << "torque_gravity:\n" << arm.getTorqueGravity() << endl << endl;
  cout << "torque external:\n"
       << "base\n" << arm.getTorqueExternal() << "\ntop" << endl;
  cout << endl;
  cout << "joint torques:\n" << arm.calcTorqueJointStatic(Vec6::Zero()) << endl;

  return 0;
}
