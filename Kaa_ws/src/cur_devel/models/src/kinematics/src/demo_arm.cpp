#include <kinematics/joint_base.h>
#include <kinematics/joint_ccc.h>
#include "kinematics/types.h"
#include "kinematics/link.h"
#include "kinematics/arm.h"

#include <stdio.h>

#include <iostream>

using kinematics::Link;
using kinematics::Arm;
using kinematics::Vec;
using kinematics::Mat;
using kinematics::JointBase;
using kinematics::JointCCC;

using std::cout;
using std::endl;

int main() {

  Arm arm = Arm();

  Link link0 = Link(kinematics::kJointTypeCCC);
  link0.setGTopEndXYZ(0.0, 0.0, 0.3);

  Link link1 = Link(kinematics::kJointTypeCCC);
  link1.setGTopEndXYZ(0.0, 0.0, 0.15);

  Link link2 = Link(kinematics::kJointTypeRot);
  link2.setGTopEndXYZ(0.0, 0.0, 0.15);

  Link link3 = Link(kinematics::kJointTypeCC1D);
  link3.setGTopEndXYZ(0.0, 0.0, 0.3);

  cout << "Link0 inertia mass is: \n" << link0.inertia_self_.mass << endl;
  cout << "Link0 inertia_mat is: \n" << link0.inertia_mat_.diagonal() << endl;

  // add links of different types to arm
  arm.addLink(link0);
  arm.addLink(link1);
  arm.addLink(link2);
  arm.addLink(link3);
  arm.addLink(link1);

  Vec angs(arm.num_dof_);
  Vec vels(arm.num_dof_);
  angs << 0.1, -.025, 0.56, 0.62, -0.32, 0.14, -0.22, -0.43;
  vels << -.025, 0.56, 0.62, -0.32, 0.14, -0.22, -0.43, 0.12;

  arm.checkMatSizes();  // verify everything has the right rows, cols

  arm.setJointAngs(angs);
  arm.setJointVels(vels);

  arm.calcFK();
  cout << "Base is at:\n" << arm.g_world_arm_ << endl;
  // for (auto link = arm.links_.begin(); link != arm.links_.end(); link++) {
  for (auto link = arm.links_.begin(); link != arm.links_.end(); link++) {
    printf("\nLink %d: ", (int)std::distance(arm.links_.begin(), link));
    cout << "link end (link.g_world_end) is at:\n" << link->g_world_end_ << endl;
  }

  arm.calcJac();
  cout << "\nArm spatial jacobian is:\n" << arm.jac_spatial_ << endl;
  for (auto link = arm.links_.begin(); link != arm.links_.end(); link++) {
    printf("\nLink %d: ", (int)std::distance(arm.links_.begin(), link));
    cout << "\nBody jacobian is:\n" << link->jac_body_ << endl;
  }

  cout << "\ngetJacBody() gives:\n" << arm.getJacBody() << endl;

  arm.calcDynamicsMats();
  cout << "\nArm inertia matrix is:\n" << arm.inertia_mat_ << endl;
  cout << "\nArm coriolis matrix is:\n" << arm.coriolis_mat_ << endl;

  for (auto link = arm.links_.begin(); link != arm.links_.end(); link++) {
    printf("\nLink %d: ", (int)std::distance(arm.links_.begin(), link));
    cout << "link->joint_->spring_constant_:\n" << link->joint_->spring_constant_ << endl;
  }

  Vec torque_external(arm.num_dof_);
  torque_external.setZero();
  arm.calcJointAccels(torque_external);
  cout << "\nArm joint acceleration (transpose) is:\n" << arm.joint_accs_.transpose();
  cout << endl
       << endl;  // on next line to keep eclipse happy

  JointCCC* joint_ccc_ptr = kinematics::getJointPtrAsCCC(arm.links_[0].joint_.get());
  cout << "joint_ccc_ptr->h_ = " << joint_ccc_ptr->h_ << endl;
  joint_ccc_ptr->setAngsZero();
  cout << "joint_ccc_ptr->angs_ = \n" << joint_ccc_ptr->angs_ << endl;
  cout << "arm.links_[0].joint_->angs_ = \n" << arm.links_[0].joint_->angs_ << endl;
  cout << "joint address is: " << arm.links_[0].joint_.get() << endl;
  cout << "joint_ccc_ptr is: " << joint_ccc_ptr << endl;

  arm.checkMatSizes();  // verify again

  // set joint stiffness
  for (unsigned long i = 0; i < arm.links_.size(); i++) {
    arm.links_[i].joint_->setJointStiffnessScalar(static_cast<double>(i + 1));
  }

  Mat stiffness_mat = Mat::Zero(arm.num_dof_, arm.num_dof_);
  int ndf;  // num dof proximate
  int nd;   // num dof of joint
  for (auto const& link : arm.links_) {
    ndf = link.num_dof_prox_;
    nd = link.joint_->num_dof_;
    stiffness_mat.block(ndf, ndf, nd, nd) << link.joint_->getJointStiffnessMatrix();
  }

  cout << "Manipulator stiffness matrix is:\n" << stiffness_mat << endl;
}
