#include "kinematics/link.h"

#include "kinematics/joint_base.h"
#include "kinematics/joint_ccc.h"
#include "kinematics/joint_cc1d.h"

#include "kinematics/joint_rot.h"

namespace kinematics {

Link::Link(void) {}
Link::Link(const JointBase& joint) : Link(joint, 0) {}

Link::Link(const JointBase& joint, int num_dof_prox) : joint_(joint.clone()) {
  num_dof_prox_ = num_dof_prox;
  num_dof_total_ = num_dof_prox + joint_->num_dof_;

  resize(num_dof_prox_);
  setDefaults();
}

Link::Link(JointType joint_type) {
  switch (joint_type) {
    case kJointTypeCCC:
      joint_ = JointCCC();
      break;
    case kJointTypeRot:
      joint_ = JointRot();
      break;
    case kJointTypeCC1D:
      joint_ = JointCC1D();
      break;
    default:
      throw("Joint type not handled.");
  }
  int num_dof_prox = 0;
  resize(num_dof_prox);
  setDefaults();
}

void Link::setGTopEndXYZ(double x, double y, double z) {
  g_top_end_ = HomTranslate(x, y, z);
}

void Link::setGTopEnd(const Hom& hom) {
  g_top_end_ = hom;
}

void Link::setInertia(Inertia* inert, double mass, Vec3 pos, Vec3 inert_diag) {
  inert->Initialize();
  inert->mass = mass;
  inert->vec = pos;
  for (int i = 0; i < 3; i++) {
    inert->inertia(i, i) = inert_diag(i);
  }
  InitInertia();
}

void Link::setInertiaAddedCylinder(
    double mass, double z_start, double thickness, double OD, double ID) {
  double IR = ID / 2.0;
  double OR = OD / 2.0;
  double Ix = 1.0 / 12.0 * mass * (3.0 * (OR * OR + IR * IR) + thickness * thickness);
  double Iz = 0.5 * mass * (OR * OR + IR * IR);
  setInertiaAdded(mass, Vec3(0.0, 0.0, z_start + thickness / 2.0), Vec3(Ix, Ix, Iz));
}

void Link::InitInertia(void) {
  Inertia total;
  if (has_inertia_added_) {
    total = InertiaCombine(inertia_self_, inertia_added_);
  } else {
    total = inertia_self_;
  }

  total.CheckIsDiagonal();  // must diagonalize first (not implemented)

  SetInertiaDiag(
      total.mass, total.inertia(0, 0), total.inertia(1, 1), total.inertia(2, 2));
  g_top_com_.setIdentity();
  g_top_com_.topRightCorner(3, 1) = total.vec;
}

void Link::SetInertiaDiag(double m, double Ix, double Iy, double Iz) {
  inertia_mat_.diagonal()[0] = m;
  inertia_mat_.diagonal()[1] = m;
  inertia_mat_.diagonal()[2] = m;
  inertia_mat_.diagonal()[3] = Ix;
  inertia_mat_.diagonal()[4] = Iy;
  inertia_mat_.diagonal()[5] = Iz;
}

void Link::resize(int num_dof_prox) {
  num_dof_prox_ = num_dof_prox;
  num_dof_total_ = num_dof_prox_ + joint_->num_dof_;

  jac_body_.resize(6, num_dof_total_);
  jac_hybrid_.resize(6, num_dof_total_);

  partials_.clear();
  for (int i = 0; i < num_dof_total_; i++) {
    partials_.push_back(LinkPartials());
    partials_.back().resize(num_dof_total_);
  }
}

void Link::setDefaults(void) {
  has_inertia_added_ = false;
  inertia_self_.Initialize();
  inertia_added_.Initialize();
  InitInertia();

  setGTopEndXYZ(0.0, 0.0, 0.2);
  setInertiaSelf(joint_->mass_, Vec3(0.0, 0.0, 0.15), Vec3(1.0, 1.0, 1.0));
}

void Link::calcFK(void) {
  joint_->calcFK();
  Vec3 new_com;
  for (uint32_t i = 0; i<3; ++i )
  {
    new_com(i) = (inertia_self_.mass*g_top_com_(i,3) - joint_->mass_*joint_->g_top_com_prev_(i,3) + joint_->mass_*joint_->g_top_com_(i,3))/(inertia_self_.mass);
  }
  setInertiaSelf(inertia_self_.mass, new_com, inertia_self_.inertia.diagonal());

  g_world_top_ = g_world_bot_ * joint_->g_bot_top_;
  g_world_com_ = g_world_top_ * g_top_com_;
  g_world_end_ = g_world_top_ * g_top_end_;
}

void Link::calcJac(Jac* jac_spatial) {
  joint_->calcJac();
  calcAdjoints();

  jac_spatial->middleCols(num_dof_prox_, joint_->num_dof_) = ad_world_bot_ * joint_->jac_;

  jac_body_ = ad_inv_world_com_ * jac_spatial->leftCols(num_dof_total_);

  jac_hybrid_ = ad_hybrid_ * jac_body_;
}

void Link::augmentInertiaMat(Mat* inertia_mat) {
  inertia_mat->topLeftCorner(num_dof_total_, num_dof_total_) +=
      jac_body_.transpose() * inertia_mat_ * jac_body_;
}

void Link::calcDeriveHom(const Jac& jac_spatial) {
  Hom js_hat;
  for (int i = 0; i < num_dof_total_; i++) {
    js_hat = TwistHat(jac_spatial.col(i));

    // derivative of g_world_bot_
    if (i < num_dof_prox_) {
      partials_[i].g_world_bot_.noalias() = js_hat * g_world_bot_;
    } else {
      partials_[i].g_world_bot_.setZero();  // this appears necessary
    }

    // derivative of g_world_com_
    partials_[i].g_world_com_.noalias() = js_hat * g_world_com_;
  }
}

void Link::calcDerivJacSpatial(std::vector<ArmPartials>* arm_partials) {
  calcDerivAdjoint();
  joint_->calcDerivJac();

  // d/dp(jac_spatial) = (d/dp)ad_world_bot * joint_jac + ad_world_bot * d/dp(joint_jac)
  int param_num;
  for (int i = 0; i < num_dof_total_; i++) {
    (*arm_partials)[i].jac_spatial_.middleCols(num_dof_prox_, joint_->num_dof_) =
        partials_[i].ad_world_bot_ * joint_->jac_;
  }
  // d/dp(joint_jac) is zero except for angles on this joint
  for (int i = 0; i < joint_->num_dof_; i++) {
    param_num = num_dof_prox_ + i;
    (*arm_partials)[param_num].jac_spatial_.middleCols(num_dof_prox_, joint_->num_dof_) +=
        ad_world_bot_ * joint_->partials_[i].jac_;
  }
}

void Link::calcDerivJacBody(const Jac& jac_spatial,
                            const std::vector<ArmPartials>& arm_partials) {
  // d/dp_i(Jbl) = d/dp_i(Ad_g_cm_inv) * Js + Ad_g_cm_inv * d/dp_i(Js)
  for (int i = 0; i < num_dof_total_; i++) {
    int ndof = partials_[i].num_dof_;
    partials_[i].jac_body_ =
        partials_[i].ad_inv_world_com_ * jac_spatial.leftCols(ndof) +
        ad_inv_world_com_ * arm_partials[i].jac_spatial_.leftCols(ndof);
  }
}

void Link::calcDerivInertiaMat(std::vector<ArmPartials>* arm_partials) {
  //  Calculate (dp_Jb.T * li * Jb) + (Jb.T * li * dp_Jb)
  //    where: Jb is link body Jacobian, li is link inertia matrix
  //    dp_ is partial derivative wrt an angle
  Mat temp;  // gets resized
  int ndt = num_dof_total_;
  for (int i = 0; i < ndt; i++) {
    // There are two terms to add, but one is the transpose of the other.  Save as temp
    // and add.
    temp = partials_[i].jac_body_.transpose() * inertia_mat_ * jac_body_;
    (*arm_partials)[i].inertia_mat_.topLeftCorner(ndt, ndt) += temp + temp.transpose();
  }
}

void Link::calcAdjoints(void) {
  ad_world_bot_ = AdjFromHom(g_world_bot_);
  ad_inv_world_com_ = AdjFromHom(HomInv(g_world_com_));
  // use g_hybrid?
  ad_hybrid_ << g_world_com_.topLeftCorner(3, 3), Rot::Zero(), Rot::Zero(),
      g_world_com_.topLeftCorner(3, 3);
}

void Link::calcDerivAdjoint(void) {
  // g = [R, p; 0, 0, 0, 1]
  // d_p = partial derivative of p
  // d_R = partial derivative of R

  // speedup:  save temporaries in place to avoid allocation
  Vec3 d_p;
  Rot d_R;

  // calculate ad_world_bot
  // d/dp(ad_world_bot) = [d/dp(R), hat(d/dp(p)) * R + hat(p) * d/dp(R); Zero(3,3),
  // d/dp(R)]
  Vec3 p = g_world_bot_.topRightCorner(3, 1);
  Rot R = g_world_bot_.topLeftCorner(3, 3);

  for (int i = 0; i < num_dof_prox_; i++) {
    d_p = partials_[i].g_world_bot_.topRightCorner(3, 1);
    d_R = partials_[i].g_world_bot_.topLeftCorner(3, 3);
    partials_[i].ad_world_bot_ << d_R, VecHat(d_p) * R + VecHat(p) * d_R, Rot::Zero(),
        d_R;
  }
  for (int i = num_dof_prox_; i < num_dof_total_; i++) {
    partials_[i].ad_world_bot_.setZero();  // speedup:  skip this(?)
  }

  // calculate ad_arm_com_inv
  p = g_world_com_.topRightCorner(3, 1);
  R = g_world_com_.topLeftCorner(3, 3).transpose();  // R.transpose()
  for (int i = 0; i < num_dof_total_; i++) {
    d_p = partials_[i].g_world_com_.topRightCorner(3, 1);
    d_R = partials_[i].g_world_com_.topLeftCorner(3, 3).transpose();  // derivative of
                                                                      // R.transpose()
    partials_[i].ad_inv_world_com_ << d_R, -(d_R * VecHat(p) + R * VecHat(d_p)),
        Rot::Zero(), d_R;
  }
}

void Link::checkMatSizes(void) {
  CheckMatSize(jac_body_, 6, num_dof_total_);
  CheckMatSize(jac_hybrid_, 6, num_dof_total_);

  joint_->checkMatSizes();

  // check for the right number of partials?
  for (auto part = partials_.begin(); part < partials_.end(); part++) {
    part->checkMatSizes();
  }
}

Vec GyroSolve(const Link& link,
              const Vec3& gyro_top,
              const Vec3& gyro_bot,
              double damping_factor) {

  int num_dof = link.joint_->num_dof_;
  Rot R = HomGetRot(link.joint_->g_bot_top_);
  Vec3 gyro_delta = gyro_top - R.transpose() * gyro_bot;

  // body jacobian component for this joint only, rotation component only
  Mat jac = link.jac_body_.bottomRightCorner(3, num_dof);

  // A = J.T * J + rho * I
  Mat A = jac.transpose() * jac + damping_factor * Mat::Identity(num_dof, num_dof);

  // use LDLT method
  Vec joint_vels = A.ldlt().solve(jac.transpose() * gyro_delta);
  return joint_vels;
}

}  // namespace kinematics
