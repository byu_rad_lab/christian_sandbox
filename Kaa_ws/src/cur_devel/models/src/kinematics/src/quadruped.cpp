#include "kinematics/quadruped.h"



namespace kinematics {

Quadruped::Quadruped() {
    base_width_ = 1.0;
    base_length_ = 1.0;
    leg_mount_angle_ = 0.0;

    shoulder_mass_ = 4*3.800; //kg
    power_supply_mass_ = 2*2.082 + 4*0.087; //kg (assuming 2 power supplies and 4 power boards)
    valve_mass_ = 4*4.124; //kg (4.124 is for 4 valves)

    g_CoG_Legs_.resize(4);
    g_Legs_CoG_.resize(4);

    calcTransforms();
    calcBaseMass();
}

Quadruped::~Quadruped() {}

void Quadruped::setBaseWidth(const double width)
{
    base_width_ = width;

    calcTransforms();
    calcBaseMass();
}

void Quadruped::setBaseLength(const double length)
{
    base_length_ = length;

    calcTransforms();
    calcBaseMass();
}

void Quadruped::setLegMountAngle(const double angle)
{
    leg_mount_angle_ = angle;

    calcTransforms();
}

void Quadruped::setShoulderMass(const double mass)
{
    shoulder_mass_ = mass;

    calcBaseMass();
}

void Quadruped::setPowerSupplyMass(const double mass)
{
    power_supply_mass_ = mass;

    calcBaseMass();
}

void Quadruped::setValveMass(const double mass)
{
    valve_mass_ = mass;

    calcBaseMass();
}

void Quadruped::calcTransforms()
{
    Hom reflect_x;
    reflect_x.setIdentity();
    reflect_x(0,0) = -1.0;

    g_CoG_Leg1_ = HomFromAxVec(Vec3(0.0, 0.0, -1.0*(M_PI_2 - leg_mount_angle_)),Vec3(base_length_/2.0, base_width_/2.0, 0.0));
    g_CoG_Leg2_ = HomFromAxVec(Vec3(0.0, 0.0, -1.0*(M_PI_2 + leg_mount_angle_)),Vec3(base_length_/2.0, -base_width_/2.0, 0.0))*reflect_x;
    g_CoG_Leg3_ = HomFromAxVec(Vec3(0.0, 0.0, (M_PI_2 + leg_mount_angle_)),Vec3(-base_length_/2.0, -base_width_/2.0, 0.0));
    g_CoG_Leg4_ = HomFromAxVec(Vec3(0.0, 0.0, (M_PI_2 - leg_mount_angle_)),Vec3(-base_length_/2.0, base_width_/2.0, 0.0))*reflect_x;

    g_Leg1_CoG_ = g_CoG_Leg1_.inverse();
    g_Leg2_CoG_ = g_CoG_Leg2_.inverse();
    g_Leg3_CoG_ = g_CoG_Leg3_.inverse();
    g_Leg4_CoG_ = g_CoG_Leg4_.inverse();

    g_CoG_Legs_[0] = g_CoG_Leg1_;
    g_CoG_Legs_[1] = g_CoG_Leg2_;
    g_CoG_Legs_[2] = g_CoG_Leg3_;
    g_CoG_Legs_[3] = g_CoG_Leg4_;

    g_Legs_CoG_[0] = g_Leg1_CoG_;
    g_Legs_CoG_[1] = g_Leg2_CoG_;
    g_Legs_CoG_[2] = g_Leg3_CoG_;
    g_Legs_CoG_[3] = g_Leg4_CoG_;
}

void Quadruped::calcBaseMass()
{
    //mass of plywood
    double plywood_density = 6.93; // kg/m^2 (a little less than 3 lb/(ft^2*in) which is the standard before sanding) assuming 1/2 in thick
    double plywood_mass = base_width_*base_length_*plywood_density;

    double frame_volume;
    if (base_length_ > 0.61 || base_width_ > 0.61) //0.61 m is around 24 in
    {
        if (base_length_ > base_width_) //0.0381 m is about 1.5 in and 0.0889 is about 3.5 in (the dimensions of a 2x4)
        {
            frame_volume = 2*0.0381*0.0889*base_length_ + 3.0*(base_width_ - 2*0.0381)*0.0381*0.0889;
        }

        else 
        {
            frame_volume = 2*0.0381*0.0889*base_width_ + 3.0*(base_length_ - 2*0.0381)*0.0381*0.0889;
        }
    }

    else
    {
        frame_volume = 2*0.0381*0.0889*base_length_ + 2.0*(base_width_ - 2*0.0381)*0.0381*0.0889;
    }

    double frame_mass = frame_volume*420.0; //420.0 kg/m^3 is the density of yellow pine

    base_mass_ = frame_mass + plywood_mass + shoulder_mass_ + power_supply_mass_ + valve_mass_;

}

}
