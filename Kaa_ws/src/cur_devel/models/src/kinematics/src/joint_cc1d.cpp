#include "kinematics/joint_cc1d.h"

#include <stdio.h>
#include <iostream>

namespace kinematics {

JointCC1D* getJointPtrAsCC1D(JointBase* base) { return dynamic_cast<JointCC1D*>(base); }

JointCC1D::JointCC1D(void) : JointCC1D(0.20) {}

JointCC1D::JointCC1D(double height) : h_(height) {
  num_dof_ = num_dof_derived_;
  resize();
  type_ = kJointTypeCC1D;
  limits()->setLimitsScalar(M_PI / 2.0);
}

void JointCC1D::calcFK(void) {
  th_ = angs_(0);

  th2_ = th_ * th_;
  th3_ = th2_ * th_;

  sth_ = sin(th_);
  cth_ = cos(th_);
  cthm1_ = cth_ - 1;

  if (th2_ > zero_tol_) {
    g_bot_top_(0, 0) = 1.0;
    g_bot_top_(0, 1) = 0.0;
    g_bot_top_(0, 2) = 0.0;
    g_bot_top_(0, 3) = 0.0;

    g_bot_top_(1, 0) = 0.0;
    g_bot_top_(1, 1) = cth_;
    g_bot_top_(1, 2) = -sth_;
    g_bot_top_(1, 3) = cthm1_ * h_ / th_;

    g_bot_top_(2, 0) = 0.0;
    g_bot_top_(2, 1) = sth_;
    g_bot_top_(2, 2) = cth_;
    g_bot_top_(2, 3) = h_ * sth_ / th_;

    g_bot_top_(3, 0) = 0.0;
    g_bot_top_(3, 1) = 0.0;
    g_bot_top_(3, 2) = 0.0;
    g_bot_top_(3, 3) = 1.0;
  } else {
    g_bot_top_.setIdentity();
    g_bot_top_(2, 3) = h_;
  }
}

void JointCC1D::calcJac(void) {
  calcFK();
  if (th2_ > zero_tol_) {
    jac_(0, 0) = 0.0;
    jac_(1, 0) = -cthm1_ * h_ / th2_;
    jac_(2, 0) = h_ * (th_ - sth_) / th2_;
    jac_(3, 0) = 1.0;
    jac_(4, 0) = 0.0;
    jac_(5, 0) = 0.0;
  } else {
    jac_(0, 0) = 0.0;
    jac_(1, 0) = 0.5 * h_;
    jac_(2, 0) = 0.0;
    jac_(3, 0) = 1.0;
    jac_(4, 0) = 0.0;
    jac_(5, 0) = 0.0;
  }
}

void JointCC1D::calcDerivJac(void) {
  calcJac();
  if (th2_ > zero_tol_) {
    partials_[0].jac_(0, 0) = 0.0;
    partials_[0].jac_(1, 0) = 2.0 * cthm1_ * h_ / th3_ + h_ * sth_ / th2_;
    partials_[0].jac_(2, 0) = -cthm1_ * h_ / th2_ - 2.0 * h_ * (th_ - sth_) / th3_;
    partials_[0].jac_(3, 0) = 0.0;
    partials_[0].jac_(4, 0) = 0.0;
    partials_[0].jac_(5, 0) = 0.0;
  } else {
    partials_[0].jac_.setZero();

    partials_[0].jac_(2, 0) = h_ / 6.0;
  }
}

}  // namespace kinematics
