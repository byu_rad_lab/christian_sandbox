#include "kinematics/joint_rot.h"

#include <stdio.h>
#include <iostream>

namespace kinematics {

JointRot* getJointPtrAsRot(JointBase* base) { return dynamic_cast<JointRot*>(base); }

JointRot::JointRot(double height) {
  h_ = height;
  num_dof_ = num_dof_derived_;
  resize();
  type_ = kJointTypeRot;
  limits()->setLimitsScalar(M_PI / 2.0);
}

void JointRot::calcFK(void) {
  th_ = angs_(0);

  sth_ = sin(th_);
  cth_ = cos(th_);

  g_bot_top_.setIdentity();
  g_bot_top_(0, 0) = cth_;
  g_bot_top_(0, 1) = -sth_;
  g_bot_top_(1, 0) = sth_;
  g_bot_top_(1, 1) = cth_;
}

void JointRot::calcJac(void) {
  jac_.setZero();
  jac_(5) = 1.0;
}

void JointRot::calcDerivJac(void) { partials_[0].jac_.setZero(); }

void JointRot::calcTorqueLimits(void){
    torque_max_ (0) = 15; //these numbers are not real.  Need to put real values for these models.  Used these to test some functions on an easier case than with the continuum joints
    torque_min_ (0) = -15;
}

}  // namespace kinematics
