#include "kinematics/joint_ccc.h"

#include <stdio.h>
#include <iostream>

namespace kinematics {

const double kOneSixth = 1.0 / 6.0;

JointCCC* getJointPtrAsCCC(JointBase* base) { return dynamic_cast<JointCCC*>(base); }

JointCCC::JointCCC(void) {
    Init(0.20, 1.917, 1/6612.0, 27.0, 599844.0); //1.917 is empirally measured of the bellows, 2 circular ends, and a medulla board
}

JointCCC::JointCCC(double height) {

    Init(height, 1.917, 1/6612.0, 27.0, 599844.0);
}

JointCCC::JointCCC(double height, double mass, double pressure_to_torque, double passive_spring_constant, double max_pressure) {
    Init(height, mass, pressure_to_torque, passive_spring_constant, max_pressure);
}

void JointCCC::Init(double height, double mass, double pressure_to_torque, double passive_spring_constant, double max_pressure) {
    h_ = height;
    pressure_to_torque_ = pressure_to_torque;
    passive_spring_constant_ = passive_spring_constant;
    max_pressure_ = max_pressure;

    num_dof_ = num_dof_derived_;
    resize();
    type_ = kJointTypeCCC;

    // set default joint limits
    double radius = 1.5;  // radians
    Vec center = Vec::Zero(num_dof_);
    int num_constraints = 8;
    double phase = 0.0;
    limits()->setLimitsCircular(radius, center, num_constraints, phase);
    mass_ = mass;
    angs_(0) = 0.0; //set initial configuration to 0.0
    angs_(1) = 0.0;
}


void JointCCC::set_h(double h) { h_ = h; }

void JointCCC::calcFK(void) {
  u_ = angs_(0);
  v_ = angs_(1);

  u2_ = u_ * u_;
  v2_ = v_ * v_;

  phi2_ = u2_ + v2_;
  phi_ = sqrt(phi2_);
  phi_inv_ = 1.0 / phi_;
  phi2_inv_ = 1.0 / phi2_;
  phi3_inv_ = phi_inv_ * phi2_inv_;

  sp_ = sin(phi_);
  cp_ = cos(phi_);
  cpm1_ = cp_ - 1.0;

  ut_ = u_ * phi_inv_;
  vt_ = v_ * phi_inv_;
  ut2_ = u2_ * phi2_inv_;
  vt2_ = v2_ * phi2_inv_;

  if (phi2_ > zero_tol_) {
    g_bot_top_(0, 0) = cpm1_ * vt2_ + 1.0;
    g_bot_top_(0, 1) = -cpm1_ * ut_ * vt_;
    g_bot_top_(0, 2) = vt_ * sp_;
    g_bot_top_(0, 3) = -cpm1_ * h_ * vt_ * phi_inv_;

    g_bot_top_(1, 0) = -cpm1_ * ut_ * vt_;
    g_bot_top_(1, 1) = cpm1_ * ut2_ + 1.0;
    g_bot_top_(1, 2) = -ut_ * sp_;
    g_bot_top_(1, 3) = cpm1_ * h_ * ut_ * phi_inv_;

    g_bot_top_(2, 0) = -vt_ * sp_;
    g_bot_top_(2, 1) = ut_ * sp_;
    g_bot_top_(2, 2) = cp_;
    g_bot_top_(2, 3) = h_ * sp_ * phi_inv_;

  } else {
    g_bot_top_(0, 0) = 1.0 - 0.5 * v2_;
    g_bot_top_(0, 1) = 0.5 * u_ * v_;
    g_bot_top_(0, 2) = v_;
    g_bot_top_(0, 3) = 0.5 * h_ * v_;

    g_bot_top_(1, 0) = 0.5 * u_ * v_;
    g_bot_top_(1, 1) = 1.0 - 0.5 * u2_;
    g_bot_top_(1, 2) = -u_;
    g_bot_top_(1, 3) = -0.5 * h_ * u_;

    g_bot_top_(2, 0) = -v_;
    g_bot_top_(2, 1) = u_;
    g_bot_top_(2, 2) = 1.0 - 0.5 * (u2_ + v2_);
    g_bot_top_(2, 3) = h_ * (1.0 - kOneSixth * (u2_ + v2_));
  }

  g_bot_top_(3, 0) = 0.0;
  g_bot_top_(3, 1) = 0.0;
  g_bot_top_(3, 2) = 0.0;
  g_bot_top_(3, 3) = 1.0;

  calcCOM(); //calculate new center of mass
}

Hom JointCCC::calcFKScaled(double scale) {
  double u = angs_(0) * scale;
  double v = angs_(1) * scale;
  double h = h_ * scale;

  double u2 = u * u;
  double v2 = v * v;

  double phi2 = u2 + v2;
  double phi = sqrt(phi2);
  double phi_inv = 1 / phi;
  double phi2_inv = 1 / phi2;

  double sp = sin(phi);
  double cp = cos(phi);
  double cpm1 = cp - 1;

  double ut = u * phi_inv;
  double vt = v * phi_inv;
  double ut2 = u2 * phi2_inv;
  double vt2 = v2 * phi2_inv;

  Hom g_bot_top;

  if (phi2_ > zero_tol_) {
    g_bot_top(0, 0) = cpm1 * vt2 + 1.0;
    g_bot_top(0, 1) = -cpm1 * ut * vt;
    g_bot_top(0, 2) = vt * sp;
    g_bot_top(0, 3) = -cpm1 * h * vt * phi_inv;

    g_bot_top(1, 0) = -cpm1 * ut * vt;
    g_bot_top(1, 1) = cpm1 * ut2 + 1.0;
    g_bot_top(1, 2) = -ut * sp;
    g_bot_top(1, 3) = cpm1 * h * ut * phi_inv;

    g_bot_top(2, 0) = -vt * sp;
    g_bot_top(2, 1) = ut * sp;
    g_bot_top(2, 2) = cp;
    g_bot_top(2, 3) = h * sp * phi_inv;

    g_bot_top(3, 0) = 0.0;
    g_bot_top(3, 1) = 0.0;
    g_bot_top(3, 2) = 0.0;
    g_bot_top(3, 3) = 1.0;
  } else {
    g_bot_top.setIdentity();
    g_bot_top(2, 3) = h;
  }
  calcCOM(); //calculate new center of mass
  return g_bot_top;
}

void JointCCC::calcCOM(void){
    g_top_com_prev_ = g_top_com_;
    g_top_com_ = g_bot_top_.inverse();
    g_top_com_.topRightCorner(3,1) = g_top_com_.topRightCorner(3,1)/2.0;
}

void JointCCC::calcJac(void) {
  calcFK();
  double cpm1ophi2 = cp_ * phi2_inv_ - phi2_inv_;
  if (phi2_ > zero_tol_) {
    jac_(0, 0) = 0.0;
    jac_(1, 0) = -cpm1ophi2 * h_;
    jac_(2, 0) = h_ * ut_ * (phi_ - sp_) * phi2_inv_;
    jac_(3, 0) = ut2_ * ut2_ + ut2_ * vt2_ + vt2_ * sp_ * phi_inv_;
    jac_(4, 0) = ut_ * vt_ * (phi_ - sp_) * phi_inv_;
    jac_(5, 0) = cpm1_ * vt_ * phi_inv_;

    jac_(0, 1) = cpm1ophi2 * h_;
    jac_(1, 1) = 0.0;
    jac_(2, 1) = h_ * vt_ * (phi_ - sp_) * phi2_inv_;
    jac_(3, 1) = ut_ * vt_ * (phi_ - sp_) * phi_inv_;
    jac_(4, 1) = ut2_ * vt2_ + vt2_ * vt2_ + ut2_ * sp_ * phi_inv_;
    jac_(5, 1) = -cpm1_ * ut_ * phi_inv_;
  } else {
    jac_(0, 0) = 0.0;
    jac_(1, 0) = 0.5 * h_;
    jac_(2, 0) = kOneSixth * u_ * h_;
    jac_(3, 0) = 1.0 - kOneSixth * v2_;
    jac_(4, 0) = kOneSixth * u_ * v_;
    jac_(5, 0) = -0.5 * v_;

    jac_(0, 1) = -0.5 * h_;
    jac_(1, 1) = 0.0;
    jac_(2, 1) = kOneSixth * v_ * h_;
    jac_(3, 1) = kOneSixth * u_ * v_;
    jac_(4, 1) = 1.0 - kOneSixth * u2_;
    jac_(5, 1) = 0.5 * u_;
  }
}

void JointCCC::calcDerivJac(void) {
  calcJac();
  if (phi2_ > zero_tol_) {
    // partials_[0].jac_ = partials wrt u
    // column of jac_ for u
    partials_[0].jac_(0, 0) = 0.0;
    partials_[0].jac_(1, 0) = h_ * ut_ * (2.0 * cpm1_ + phi_ * sp_) * phi3_inv_;
    partials_[0].jac_(2, 0) =
        -h_ * (phi_ * (ut2_ * cp_ + 2.0 * ut2_ - 1.0) - (3.0 * ut2_ - 1.0) * sp_) *
        phi3_inv_;
    partials_[0].jac_(3, 0) = ut_ * (phi_ * (-4.0 * ut2_ * ut2_ - 4.0 * ut2_ * vt2_ +
                                             4.0 * ut2_ + vt2_ * cp_ + 2.0 * vt2_) -
                                     3.0 * vt2_ * sp_) *
                              phi2_inv_;
    partials_[0].jac_(4, 0) =
        -vt_ * (phi_ * (ut2_ * cp_ + 2.0 * ut2_ - 1.0) - (3.0 * ut2_ - 1.0) * sp_) *
        phi2_inv_;
    partials_[0].jac_(5, 0) = -ut_ * vt_ * (2.0 * cpm1_ + phi_ * sp_) * phi2_inv_;

    // column of jac_ for v
    partials_[0].jac_(0, 1) = -h_ * ut_ * (2.0 * cpm1_ + phi_ * sp_) * phi3_inv_;
    partials_[0].jac_(1, 1) = 0.0;
    partials_[0].jac_(2, 1) =
        -h_ * ut_ * vt_ * (phi_ * (cpm1_ + 3.0) - 3.0 * sp_) * phi3_inv_;
    partials_[0].jac_(3, 1) =
        -vt_ * (phi_ * (ut2_ * cp_ + 2.0 * ut2_ - 1.0) - (3.0 * ut2_ - 1.0) * sp_) *
        phi2_inv_;
    partials_[0].jac_(4, 1) =
        -ut_ * (phi_ * (4.0 * ut2_ * vt2_ - ut2_ * cp_ + 4.0 * vt2_ * vt2_ - 2.0 * vt2_) +
                (3.0 * ut2_ - 2.0) * sp_) *
        phi2_inv_;
    partials_[0].jac_(5, 1) =
        (-cpm1_ + phi_ * ut2_ * sp_ + 2.0 * ut2_ * cp_ - 2.0 * ut2_) * phi2_inv_;

    // partials_[1].jac_ = partials wrt v
    // column of jac_ for u
    partials_[1].jac_(0, 0) = 0.0;
    partials_[1].jac_(1, 0) = h_ * vt_ * (2.0 * cpm1_ + phi_ * sp_) * phi3_inv_;
    partials_[1].jac_(2, 0) =
        -h_ * ut_ * vt_ * (phi_ * (cpm1_ + 3.0) - 3.0 * sp_) * phi3_inv_;
    partials_[1].jac_(3, 0) =
        -vt_ * (phi_ * (4.0 * ut2_ * ut2_ + 4.0 * ut2_ * vt2_ - 2.0 * ut2_ - vt2_ * cp_) +
                (3.0 * vt2_ - 2.0) * sp_) *
        phi2_inv_;
    partials_[1].jac_(4, 0) =
        -ut_ * (phi_ * (vt2_ * cp_ + 2.0 * vt2_ - 1.0) - (3.0 * vt2_ - 1.0) * sp_) *
        phi2_inv_;
    partials_[1].jac_(5, 0) =
        (cpm1_ - phi_ * vt2_ * sp_ - 2.0 * vt2_ * cp_ + 2.0 * vt2_) * phi2_inv_;

    // column of jac_ for v
    partials_[1].jac_(0, 1) = -h_ * vt_ * (2.0 * cpm1_ + phi_ * sp_) * phi3_inv_;
    partials_[1].jac_(1, 1) = 0.0;
    partials_[1].jac_(2, 1) =
        -h_ * (phi_ * (vt2_ * cp_ + 2.0 * vt2_ - 1.0) - (3.0 * vt2_ - 1.0) * sp_) *
        phi3_inv_;
    partials_[1].jac_(3, 1) =
        -ut_ * (phi_ * (vt2_ * cp_ + 2.0 * vt2_ - 1.0) - (3.0 * vt2_ - 1.0) * sp_) *
        phi2_inv_;
    partials_[1].jac_(4, 1) = vt_ *
                              (phi_ * (-4.0 * ut2_ * vt2_ + ut2_ * cp_ + 2.0 * ut2_ -
                                       4.0 * vt2_ * vt2_ + 4.0 * vt2_) -
                               3.0 * ut2_ * sp_) *
                              phi2_inv_;
    partials_[1].jac_(5, 1) = ut_ * vt_ * (2.0 * cpm1_ + phi_ * sp_) * phi2_inv_;
  } else {
    partials_[0].jac_.setZero();
    partials_[1].jac_.setZero();

    partials_[0].jac_(2, 0) = h_ / 6.0;
    partials_[0].jac_(5, 1) = 0.5;
    partials_[1].jac_(5, 0) = -0.5;
    partials_[1].jac_(2, 1) = h_ / 6.0;
  }
}

void JointCCC::calcTorqueLimits(void)
{
    torque_max_(0) = pressure_to_torque_*max_pressure_ - passive_spring_constant_*u_;
    torque_max_(1) = pressure_to_torque_*max_pressure_ - passive_spring_constant_*v_;
    torque_min_(0) = -1.0*pressure_to_torque_*max_pressure_ - passive_spring_constant_*u_;
    torque_min_(1) = -1.0*pressure_to_torque_*max_pressure_ - passive_spring_constant_*v_;
}

void JointCCC::resetAngsWithinLimit(void) {
  double mag = sqrt(angs_[0] * angs_[0] + angs_[1] * angs_[1]);
  if (mag > limits_upper_[0]) {
    for (int i = 0; i < num_dof_; i++) {
      angs_[i] = angs_[i] / mag * limits_upper_[0];
    }
  }
}

}  // namespace kinematics
