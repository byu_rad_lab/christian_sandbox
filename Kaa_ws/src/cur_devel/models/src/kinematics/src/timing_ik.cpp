#include <kinematics/joint_base.h>
#include "kinematics/types.h"
#include "kinematics/link.h"
#include "kinematics/arm.h"
#include "kinematics/arm_ik.h"

#include "kinematics/BenchTimer.h"

#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#include <iostream>


using kinematics::JointBase;
using kinematics::Link;
using kinematics::Arm;
using kinematics::Vec;
using kinematics::Mat;

using std::cout;
using std::endl;

EIGEN_DONT_INLINE void testDynamicsMats(Arm* arm) { arm->calcDynamicsMats(); }

typedef unsigned long long timestamp_t;

// static timestamp_t get_timestamp() {
//   struct timeval now;
//   gettimeofday(&now, NULL);
//   return now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
// }

int main() {
  Vec lengths = Vec(4);
  lengths << 0.3, 0.05, 0.3, 0.05;
  double height = 0.2;

  Arm arm(height, lengths, kinematics::kJointTypeCCC);

  Vec angs_arm(arm.num_dof_);
  angs_arm.setZero();
  angs_arm(1) = 3.14159265/4;
  angs_arm(2) = 3.14159265/6;
  angs_arm(3) = 3.14159265/4;
  angs_arm(4) = 3.14159265/8;
  arm.setJointAngs(angs_arm);
  kinematics::Hom gdes;
  gdes = arm.links_.back().g_top_end_;

  angs_arm.setZero();
  arm.setJointAngs(angs_arm);
  arm.calcJac();

  kinematics::ArmIK armik(&arm);

  cout << "arm.num_dof_ = " << arm.num_dof_ << endl;
  angs_arm.setZero();

  int reps = (int)1e3;
  int tries = 100;
  // int reps = (int)1e2;
  // int tries = 10;

  Eigen::BenchTimer t1, t2;
  Eigen::BenchTimer t3, t4, t5;
  BENCH(t1, tries, reps, arm.calcDynamicsMats());
  BENCH(t2, tries, reps, testDynamicsMats(&arm));
  BENCH(t3, tries, reps, arm.calcFK());
  BENCH(t4, tries, reps, armik.IKStep(gdes));
  BENCH(t5, tries, reps, armik.calcIK(gdes));
  cout << "calcDynamicsMats best: " << t1.best() / reps * 1e6 << " us" << endl;
  cout << "testDynamicsMats best: " << t2.best() / reps * 1e6 << " us" << endl;
  cout << "FK best: " << t3.best() / reps * 1e6 << " us" << endl;
  cout << "IK best: " << t4.best() / reps * 1e6 << " us" << endl;
  cout << "IK solve best: " << t5.best() / reps * 1e6 << " us" << endl;

  return 0;
}
