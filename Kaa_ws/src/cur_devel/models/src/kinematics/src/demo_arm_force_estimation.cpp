#include <kinematics/joint_base.h>
#include <kinematics/joint_ccc.h>
#include "kinematics/types.h"
#include "kinematics/link.h"
#include "kinematics/arm.h"

#include <stdio.h>

#include <iostream>

using kinematics::Link;
using kinematics::Arm;
using kinematics::Vec;
using kinematics::Vec3;
// using kinematics::Vec6;
using kinematics::Hom;
using kinematics::Mat;
using kinematics::Frame;
using kinematics::JointBase;
using kinematics::JointCCC;

using std::cout;
using std::endl;

int main() {

  Arm arm = Arm();

  Link link0 = Link(kinematics::kJointTypeCCC);
  link0.setGTopEndXYZ(0.0, 0.0, 0.3);

  Link link1 = Link(kinematics::kJointTypeCCC);
  link1.setGTopEndXYZ(0.0, 0.0, 0.15);

  // add links of different types to arm
  arm.addLink(link0);
  arm.addLink(link1);

  Vec angs(arm.num_dof_);
  Vec vels(arm.num_dof_);
  // angs << 0.0, 0.0, 0.0, 0.0;
  angs << 0.0, 1.57, 0.0, 0.0;

  arm.checkMatSizes();  // verify everything has the right rows, cols

  arm.setJointAngs(angs);
  arm.setJointVelsZero();

  arm.calcFK();
  cout << "Base is at:\n" << arm.g_world_arm_ << endl;
  // for (auto link = arm.links_.begin(); link != arm.links_.end(); link++) {
  for (auto link = arm.links_.begin(); link != arm.links_.end(); link++) {
    printf("\nLink %d: ", (int)std::distance(arm.links_.begin(), link));
    cout << "link end (link.g_world_end) is at:\n" << link->g_world_end_ << endl;
  }

  arm.calcJac();
  cout << "\nArm spatial jacobian is:\n" << arm.jac_spatial_ << endl;
  for (auto link = arm.links_.begin(); link != arm.links_.end(); link++) {
    printf("\nLink %d: ", (int)std::distance(arm.links_.begin(), link));
    cout << "\nBody jacobian is:\n" << link->jac_body_ << endl;
  }

  arm.checkMatSizes();  // verify again

  Vec joint_torques_net(arm.num_dof_);
  joint_torques_net << 0.0, 0.0, 0.0, 1.0;
  // joint_torques_net << 0.0, 0.0, 0.0, 0.0;

  Vec3 forces;
  forces = arm.calcForcesFromTorques(joint_torques_net);

  cout << "\nforces arm:\n" << forces.transpose() << endl;
  cout << "Demo Finished." << endl;

}
