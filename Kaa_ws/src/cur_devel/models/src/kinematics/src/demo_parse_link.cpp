#include <kinematics/joint_base.h>
#include <stdio.h>

#include <iostream>

#include "yaml-cpp/yaml.h"

#include "kinematics/types.h"
#include "kinematics/link.h"
#include "kinematics/yaml.h"

using std::cout;
using std::endl;

int main(int argc, char* argv[]) {
  YAML::Node node = kinematics::LoadYAMLCommandLine(argc, argv);

  cout << "### Parsed node: ###\n" << node << endl << "### End parsed node. ###\n\n";

  kinematics::Link link = node.as<kinematics::Link>();

  // cout << "link.joint_.h_ = " << link.joint_.h_ << endl;  // has to be cast to derived to get h
  cout << "link.joint_.limits_lower_ = \n" << link.joint_->limits_lower_ << endl;
  cout << "link.g_top_end_ = \n" << link.g_top_end_ << endl;

  cout << "link.inertia_self_.mass = " << link.inertia_self_.mass << endl;
  cout << "link.inertia_self_.inertia = \n" << link.inertia_self_.inertia << endl;
  cout << "link.g_top_com_ = \n" << link.g_top_com_ << endl;

  return 0;
}
