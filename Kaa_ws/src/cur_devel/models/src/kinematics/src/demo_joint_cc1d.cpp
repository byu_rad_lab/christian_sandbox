#include "kinematics/types.h"
#include "kinematics/joint_cc1d.h"
#include "kinematics/joint_ccc.h"

#include <stdio.h>

#include <iostream>

using std::cout;
using std::endl;

using kinematics::Vec;
using kinematics::Mat;
using kinematics::JointCC1D;
using kinematics::JointCCC;

int compare_joints(JointCCC joint_ccc, JointCC1D joint_cc1d, double theta) {
  Vec angs_cc1d = Vec::Zero(1);
  Vec angs_ccc = Vec::Zero(2);
  angs_cc1d(0) = theta;
  angs_ccc(0) = theta;

  joint_ccc.setAngs(angs_ccc);
  joint_cc1d.setAngs(angs_cc1d);

  joint_cc1d.calcDerivJac();
  joint_ccc.calcDerivJac();

  cout << "\nChecking against JointCCC" << endl;
  Mat diff;

  diff = joint_cc1d.g_bot_top_ - joint_ccc.g_bot_top_;
  cout << "g_bot_top_ diff = \n" << diff << endl;

  diff = joint_cc1d.jac_ - joint_ccc.jac_.leftCols(1);
  cout << "jac_ diff = \n" << diff << endl;

  diff = joint_cc1d.partials_[0].jac_ - joint_ccc.partials_[0].jac_.leftCols(1);
  cout << "partials_[0].jac_ diff = \n" << diff << endl;
  return 0;
}

int main() {

  double height = 0.2;

  JointCC1D joint_cc1d(height);
  joint_cc1d.setAngsZero();
  // joint_cc1d.calcFK();
  // joint_cc1d.calcJac();
  joint_cc1d.calcDerivJac();
  cout << "joint_cc1d.g_bot_top_ = \n" << joint_cc1d.g_bot_top_ << endl;
  cout << "joint_cc1d.jac_ = \n" << joint_cc1d.jac_ << endl;

  JointCCC joint_ccc(height);

  compare_joints(joint_ccc, joint_cc1d, 0.0);
  compare_joints(joint_ccc, joint_cc1d, 0.1);
  compare_joints(joint_ccc, joint_cc1d, -1.1);
}
