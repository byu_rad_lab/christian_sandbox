#include "kinematics/types.h"
#include "kinematics/joint_ccc.h"
#include "kinematics/arm.h"

#include <stdio.h>

#include <iostream>

using std::cout;
using std::endl;

using kinematics::Vec;
using kinematics::Arm;
using kinematics::Link;
using kinematics::JointCCC;
using kinematics::JointLimits;

int main() {

  double height = 0.2;
  JointCCC joint(height);
  joint.setAngsZero();

  Vec bounds_upper = Vec(2);
  Vec bounds_lower = Vec(2);
  bounds_upper << -0.050, 0.0;
  bounds_lower << .05, 0.0;

  joint.limits()->setLimitsVector(bounds_upper,
                                  bounds_lower);  // sets bounds and A, b of A x <= b

  // joint.limits()->setLimitsScalar(0.25);  // sets all DOF to +/- 0.25
  // joint.limits()->setLimitsCircular(3.0, Vec::Zero(joint.num_dof_), 8, 0.0);  // sets
  // to octagonal region
  JointLimits* limits = joint.limits();

  cout << "a is:\n" << limits->a() << endl;
  cout << "b is:\n" << limits->b() << endl;

  cout << "bounds_lower is:\n" << limits->bounds_lower() << endl;
  cout << "bounds_upper is:\n" << limits->bounds_upper() << endl;

  double mag;
  Vec angs;
  for (int i = 0; i < 30; i++) {
    joint.setAngsRandom();
    angs = joint.getAngs();
    mag = sqrt(angs(0) * angs(0) + angs(1) * angs(1));
    printf("angs: %6.3f, %6.3f,  mag:  %6.3f\n", angs(0), angs(1), mag);
    // cout << "angs are: " << angs.transpose() << endl;
  }
  // // also test joint_base implementation
  //
  //Arm set random from arm level
  Arm arm = Arm();
  Link link0 = Link(joint);
  arm.addLink(link0);
  arm.setJointAngsRandom();

  Vec angs2;
  angs2 = arm.getJointAngs();
  cout << "Arm set random from arm level" << endl;
  cout << angs2(0) << "  " <<  angs2(1) << endl;
}
