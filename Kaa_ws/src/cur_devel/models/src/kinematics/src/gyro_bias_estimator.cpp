#include "kinematics/gyro_bias_estimator.h"
#include <cmath>
#include <iostream>

namespace kinematics {

void GyroBiasEstimator::PredictGyro(Vec3 omega, double dt) {
  q_ *= QuatFromAx((omega - bias_) * dt);
}

// TODO: switch q_em to q_me, kp to -kp, -= ki to += ki
void GyroBiasEstimator::CorrectRot(Rot rot) {
  // q_ is q_re, quaternion from reference to estimate
  Quat q_rm = Quat(rot);                            // quaternion from reference to measurement
  Quat q_em = q_.conjugate() * q_rm;                // estimate to measurement
  q_ = q_ * Quat::Identity().slerp(kp_rot_, q_em);  // scale down and add
  q_.normalize();
  bias_ -= ki_rot_ * AxFromQuat(q_em);  // update the bias
}

}  // namespace kinematics
