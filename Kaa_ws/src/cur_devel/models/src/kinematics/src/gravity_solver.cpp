/* Otherlab, Inc ("COMPANY") CONFIDENTIAL
 * Unpublished Copyright (c) 2014-2015 Otherlab, Inc, All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of COMPANY.
 * The intellectual and technical concepts contained herein are proprietary to COMPANY
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from COMPANY. Access to the source code contained herein is hereby forbidden to
 * anyone except current COMPANY employees, managers or contractors who have executed
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
 * IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO
 * REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
#include "kinematics/gravity_solver.h"

#include <stdio.h>

#include <iostream>
#include <vector>

namespace kinematics {

GravitySolver::GravitySolver(const JointBase& joint)
    : joint_(joint), opt_(nlopt::opt(nlopt::LN_BOBYQA, joint_->num_dof_)), feval_(0) {
  //: joint_(joint), opt_(nlopt::opt(nlopt::LN_COBYLA, joint_->num_dof_)), feval_(0) {
  updateBounds();
  opt_.set_min_objective(GravitySolver::costCaller, this);
  opt_.set_xtol_rel(1e-4);
  for (int i = 0; i < joint_->num_dof_; i++) {
    x_.push_back(0.0);
  }
}

GravitySolver::GravitySolver(const JointPtr joint_ptr)
    : joint_(joint_ptr), opt_(nlopt::opt(nlopt::LN_BOBYQA, joint_->num_dof_)), feval_(0) {
  //: joint_(joint), opt_(nlopt::opt(nlopt::LN_COBYLA, joint_->num_dof_)), feval_(0) {
  updateBounds();
  opt_.set_min_objective(GravitySolver::costCaller, this);
  opt_.set_xtol_rel(1e-12);
  for (int i = 0; i < joint_->num_dof_; i++) {
    x_.push_back(0.0);
  }
}

void GravitySolver::updateBounds(void) {
  std::vector<double> lb(joint_->limits_lower_.data(),
                         joint_->limits_lower_.data() + joint_->limits_lower_.size());
  std::vector<double> ub(joint_->limits_upper_.data(),
                         joint_->limits_upper_.data() + joint_->limits_upper_.size());
  opt_.set_lower_bounds(lb);
  opt_.set_upper_bounds(ub);
}

// GravitySolver::~GravitySolver() {
// }

void GravitySolver::solve(const Vec3& a0, const Vec3& a1, Vec* ang) {
  feval_ = 0;
  a0_ = a0;
  a1_ = a1;
  double minf;

  for (int i = 0; i < joint_->num_dof_; i++) {
    x_[i] = (*ang)[i];
  }

  try {
    opt_.optimize(x_, minf);
  } catch (nlopt::roundoff_limited& e) {
    // This is ok
  } catch (std::invalid_argument& e) {
    std::cout << "### Error:" << std::endl;
    std::cout << "nlopt invalid argument. Fix this!" << std::endl;
    std::cout << "minf = " << minf << std::endl;
    for (int i = 0; i < joint_->num_dof_; i++) {
      printf("x[%d] = %7.3f\n", i, x_[i]);
    }
    std::cout << "*ang = \n" << *ang << std::endl;
    std::cout << "Gravity - \n";
    std::cout << "a0 = \n" << a0 << std::endl;
    std::cout << "a1 = \n" << a1 << std::endl;
    std::cout << "Joint limits are - \n";
    std::cout << "lower: \n" << joint_->limits_lower_ << std::endl;
    std::cout << "upper: \n" << joint_->limits_upper_ << std::endl;
  }

  for (int i = 0; i < joint_->num_dof_; i++) {
    (*ang)[i] = x_[i];
  }
}

Vec GravitySolver::solve2(const Vec3& a0, const Vec3& a1, const Vec& guess) {
  Vec tmp;
  int n = joint_.get()->num_dof_;
  tmp.resize(n);
  for (int i = 0; i < n; i++) {
    tmp[i] = guess[i];
  }
  solve(a0, a1, &tmp);
  return tmp;
}

double GravitySolver::costFunc(const std::vector<double>& x,
                               std::vector<double>& /*grad*/,
                               void* /*f_data*/) {
  joint_->setAngs(Eigen::Map<const Eigen::VectorXd>(x.data(), x.size()));

  joint_->calcFK();
  r_01_ = joint_->g_bot_top_.topLeftCorner(3, 3);
  a1_calc_ = r_01_.transpose() * a0_;
  err_ = (a1_calc_ - a1_);

  return err_.dot(err_);
}

double GravitySolver::costCaller(const std::vector<double>& x,
                                 std::vector<double>& grad,
                                 void* f_data) {
  return (reinterpret_cast<GravitySolver*>(f_data))->costFunc(x, grad, f_data);
}

}  // namespace kinematics
