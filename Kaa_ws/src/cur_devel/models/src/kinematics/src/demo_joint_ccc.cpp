#include "kinematics/types.h"
#include "kinematics/joint_ccc.h"

#include <stdio.h>

#include <iostream>

using std::cout;
using std::endl;

using kinematics::JointCCC;

int main() {

  double height = 0.2;

  JointCCC joint(height);
  joint.setAngsZero();
  joint.calcFK();
  joint.calcJac();
  joint.calcDerivJac();
  cout << "joint.g_bot_top_ = \n" <<joint.g_bot_top_ << endl;
  cout << "joint.jac_ = \n" <<joint.jac_ << endl;
}
