#include <kinematics/joint_base.h>
#include <stdio.h>

#include <iostream>

#include "yaml-cpp/yaml.h"

#include "kinematics/types.h"
#include "kinematics/joint_rot.h"
#include "kinematics/yaml.h"

using std::cout;
using std::endl;

int main(int argc, char* argv[]) {
  YAML::Node node = kinematics::LoadYAMLCommandLine(argc, argv);

  cout << "### Parsed node: ###\n" << node << endl << "### End parsed node. ###\n\n";

  kinematics::JointRot joint = node.as<kinematics::JointRot>();

  // now check the joint
  printf("joint.h_ = %f\n", joint.h_);
  cout << "joint.limits_lower_ = \n" << joint.limits_lower_ << endl;
  cout << "joint.limits_upper_ = \n" << joint.limits_upper_ << endl;
  cout << "joint.spring_constant_ = \n" << joint.spring_constant_ << endl;
  cout << "joint.damping_constant_ = \n" << joint.damping_constant_ << endl;

  printf("joint.num_dof_ = %d\n", joint.num_dof_);

  // switch on joint type
  return 0;
}
