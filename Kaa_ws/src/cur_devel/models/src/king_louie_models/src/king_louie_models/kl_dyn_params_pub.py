from dynamics_utils import *
import math
import rospy
import time
from threading import RLock, Timer

from louie_control.msg import param_msg
from low_level_control.msg import joint_angles_msg as jaMsg
from low_level_control.msg import joint_angles_msg_array as jaMsgArr



class dynParamEst():
    def __init__(self):

        self.lock = RLock()

        self.fv1 = 0.0
        self.fv2 = 0.0
        self.fv3 = 0.0
        self.fv4 = 0.0

        grub_mass = 0.85
        self.m1 = grub_mass
        self.m2 = grub_mass
        self.m3 = grub_mass
        self.m4 = grub_mass

        # center of mass (wrt each DH frame)
        self.r1 = [0.09, 0.0,  0.0]
        self.r2 = [0.19, 0.0,  0.0]
        self.r3 = [0.06, 0.0,  0.0] 
        self.r4 = [0.12, 0.0,  0.0]

        #               Ixx    Iyy    Izz   Ixy Iyz Ixz
        grub_inertia = [0.006, 0.013, 0.013, 0, 0, 0]
        self.I1 = grub_inertia
        self.I2 = grub_inertia
        self.I3 = grub_inertia
        self.I4 = grub_inertia

        self.q = []
        self.q_ = []
        self.qgoal = np.array([0, 0, 0, 0, 0])

        self.qgMsg = jaMsg()
        self.qgMsg.appendage = 'Right' 
        self.qgMsg.q = np.array([0, 0, 0, 0, 0])
        self.qgMsg.q_dot = np.array([0, 0, 0, 0, 0])
        self.qgMsg.flag = 1

        self.qgMsgArr = jaMsgArr()

        self.d2r = math.pi/180

        self.time = 0
        self.time_0 = None
        self.goal_list1 = np.array([-20, -20, 0, -20, -20]) * self.d2r
        self.goal_list2 = np.array([-60, 20, 0, 20, 20]) * self.d2r

        self.param_pub = rospy.Publisher('/KL_ParamEst', param_msg, queue_size = 1, tcp_nodelay = True)
        self.qgoal_pub = rospy.Publisher('/KL_qgoal', jaMsgArr, queue_size = 1, tcp_nodelay = True)

        rospy.Subscriber("/KLJointAngles", jaMsgArr, self.angle_callback, tcp_nodelay = True)

    def angle_callback(self, data):
        self.lock.acquire()
        tj = 0

        try:
            for i in range(0,len(data.joint_angles)):  # loops through appendages (right, left, hip)
                if data.joint_angles[i].appendage == self.appendage:
                    for j in range(self.njts):  # loop through joints in the appendage
                        
                        self.q_.append( data.joint_angles[i].q[tj] )

                        # skip elbow
                        if tj == 1: tj = tj + 2
                        else: tj = tj + 1
        finally:
            self.lock.release()  

    def get_data(self):
        self.lock.acquire()
        try:
            self.q = self.q_

            t = time.time()
            
            if self.time_0 == None:
                self.time_0 = t
            self.time = t - self.time_0
        finally:
            self.lock.release()

    def adaptMass(self):
      q_ref_dot = self.alpha*(self.q)


    def calcParams(self):
        self.get_data()

        L1 = I_to_matrix(self.I1)-self.m1*skew(self.r1)*skew(self.r1)
        L2 = I_to_matrix(self.I2)-self.m2*skew(self.r2)*skew(self.r2)
        L3 = I_to_matrix(self.I3)-self.m3*skew(self.r3)*skew(self.r3)
        L4 = I_to_matrix(self.I4)-self.m4*skew(self.r4)*skew(self.r4)

        params = [L1[0,0], -L1[0,1], -L1[0,2], L1[1,1], -L1[1,2], L1[2,2], self.r1[0]*self.m1, self.r1[1]*self.m1, self.r1[2]*self.m1, self.m1, self.fv1,
                  L2[0,0], -L2[0,1], -L2[0,2], L2[1,1], -L2[1,2], L2[2,2], self.r2[0]*self.m2, self.r2[1]*self.m2, self.r2[2]*self.m2, self.m2, self.fv2,
                  L3[0,0], -L3[0,1], -L3[0,2], L3[1,1], -L3[1,2], L3[2,2], self.r3[0]*self.m3, self.r3[1]*self.m3, self.r3[2]*self.m3, self.m3, self.fv3,
                  L4[0,0], -L4[0,1], -L4[0,2], L4[1,1], -L4[1,2], L4[2,2], self.r4[0]*self.m4, self.r4[1]*self.m4, self.r4[2]*self.m4, self.m4, self.fv4]

        pMsg = param_msg(params, 1)
        self.param_pub.publish(pMsg)

        self.qgoal = self.getQGoal()
        self.qgMsg.q = self.qgoal

        self.qgMsgArr.joint_angles = [self.qgMsg]
        self.qgoal_pub.publish(self.qgMsgArr)

    def getQGoal(self):
        period = 20
        if math.sin(2*math.pi*self.time/period) > 0:
            return self.goal_list1
        else:
            return self.goal_list2



if __name__ == '__main__':

    estimator = dynParamEst()

    rospy.init_node('kl_est', anonymous=True)
    # rospy.on_shutdown(estimator.write_data)
    rate = rospy.Rate(300.0)

    while not rospy.is_shutdown():
        estimator.calcParams()
        rate.sleep()