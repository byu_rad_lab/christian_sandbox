from math import sin, cos
from offset_util import offset_and_reshape
import numpy as np
pi = np.pi



def joint_fk00(q):
#
    pose = [0]*16
#
    x0 = cos(q[0])
    x1 = sin(q[0])
#
    pose[0] = x0
    pose[1] = 0
    pose[2] = -x1
    pose[3] = 0.22*x0
    pose[4] = x1
    pose[5] = 0
    pose[6] = x0
    pose[7] = 0.22*x1
    pose[8] = 0
    pose[9] = -1
    pose[10] = 0
    pose[11] = 0
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    pose = offset_and_reshape(pose,0.0,0.0,0.0)
    return pose



def joint_fk01(q):
#
    pose = [0]*16
#
    x0 = cos(q[0])
    x1 = cos(q[1])
    x2 = x0*x1
    x3 = sin(q[1])
    x4 = sin(q[0])
    x5 = x1*x4
#
    pose[0] = x2
    pose[1] = -x0*x3
    pose[2] = -x4
    pose[3] = 0.22*x0 + 0.4*x2
    pose[4] = x5
    pose[5] = -x3*x4
    pose[6] = x0
    pose[7] = 0.22*x4 + 0.4*x5
    pose[8] = -x3
    pose[9] = -x1
    pose[10] = 0
    pose[11] = -0.4*x3
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    pose = offset_and_reshape(pose,0.0,0.0,0.0)
    return pose



def joint_fk02(q):
#
    pose = [0]*16
#
    x0 = cos(q[0])
    x1 = cos(q[1])
    x2 = cos(q[2])
    x3 = x1*x2
    x4 = x0*x3
    x5 = sin(q[1])
    x6 = sin(q[2])
    x7 = x5*x6
    x8 = x0*x7
    x9 = sin(q[0])
    x10 = x2*x5
    x11 = x1*x6
    x12 = 0.4*x1
    x13 = x3*x9
    x14 = x7*x9
#
    pose[0] = x4 - x8
    pose[1] = -x9
    pose[2] = x0*x10 + x0*x11
    pose[3] = x0*x12 + 0.22*x0 + 0.14*x4 - 0.14*x8
    pose[4] = x13 - x14
    pose[5] = x0
    pose[6] = x10*x9 + x11*x9
    pose[7] = x12*x9 + 0.14*x13 - 0.14*x14 + 0.22*x9
    pose[8] = -x10 - x11
    pose[9] = 0
    pose[10] = x3 - x7
    pose[11] = -0.14*x10 - 0.14*x11 - 0.4*x5
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    pose = offset_and_reshape(pose,0.0,0.0,0.0)
    return pose



def joint_fk03(q):
#
    pose = [0]*16
#
    x0 = sin(q[0])
    x1 = sin(q[3])
    x2 = x0*x1
    x3 = cos(q[3])
    x4 = cos(q[0])
    x5 = cos(q[1])
    x6 = cos(q[2])
    x7 = x5*x6
    x8 = x4*x7
    x9 = sin(q[1])
    x10 = sin(q[2])
    x11 = x10*x9
    x12 = x11*x4
    x13 = -x12 + x8
    x14 = x13*x3
    x15 = x6*x9
    x16 = x10*x5
    x17 = 0.4*x5
    x18 = x1*x4
    x19 = x0*x7
    x20 = x0*x11
    x21 = x19 - x20
    x22 = x21*x3
    x23 = -x15 - x16
    x24 = x23*x3
#
    pose[0] = x14 - x2
    pose[1] = -x0*x3 - x1*x13
    pose[2] = x15*x4 + x16*x4
    pose[3] = -0.14*x12 + 0.24*x14 + x17*x4 - 0.24*x2 + 0.22*x4 + 0.14*x8
    pose[4] = x18 + x22
    pose[5] = -x1*x21 + x3*x4
    pose[6] = x0*x15 + x0*x16
    pose[7] = x0*x17 + 0.22*x0 + 0.24*x18 + 0.14*x19 - 0.14*x20 + 0.24*x22
    pose[8] = x24
    pose[9] = -x1*x23
    pose[10] = -x11 + x7
    pose[11] = -0.14*x15 - 0.14*x16 + 0.24*x24 - 0.4*x9
    pose[12] = 0
    pose[13] = 0
    pose[14] = 0
    pose[15] = 1
#
    pose = offset_and_reshape(pose,0.0,0.0,0.0)
    return pose



FK = {0:joint_fk00, 1:joint_fk01, 2:joint_fk02, 3:joint_fk03, }



def jacobian00(q):
#
    jacobian = [0]*24
#

    jacobian[0] = -0.22*sin(q[0])
    jacobian[1] = 0
    jacobian[2] = 0
    jacobian[3] = 0
    jacobian[4] = 0.22*cos(q[0])
    jacobian[5] = 0
    jacobian[6] = 0
    jacobian[7] = 0
    jacobian[8] = 0
    jacobian[9] = 0
    jacobian[10] = 0
    jacobian[11] = 0
    jacobian[12] = 0
    jacobian[13] = 0
    jacobian[14] = 0
    jacobian[15] = 0
    jacobian[16] = 0
    jacobian[17] = 0
    jacobian[18] = 0
    jacobian[19] = 0
    jacobian[20] = 1
    jacobian[21] = 0
    jacobian[22] = 0
    jacobian[23] = 0
#
    jacobian = np.array(jacobian).reshape(6,4)
    return jacobian



def jacobian01(q):
#
    jacobian = [0]*24
#
    x0 = sin(q[0])
    x1 = cos(q[1])
    x2 = 0.4*x0
    x3 = sin(q[1])
    x4 = cos(q[0])
    x5 = 0.4*x4
    x6 = 0.4*x1
#
    jacobian[0] = -0.22*x0 - x1*x2
    jacobian[1] = -x3*x5
    jacobian[2] = 0
    jacobian[3] = 0
    jacobian[4] = x1*x5 + 0.22*x4
    jacobian[5] = -x2*x3
    jacobian[6] = 0
    jacobian[7] = 0
    jacobian[8] = 0
    jacobian[9] = -x0**2*x6 - x4**2*x6
    jacobian[10] = 0
    jacobian[11] = 0
    jacobian[12] = 0
    jacobian[13] = -x0
    jacobian[14] = 0
    jacobian[15] = 0
    jacobian[16] = 0
    jacobian[17] = x4
    jacobian[18] = 0
    jacobian[19] = 0
    jacobian[20] = 1
    jacobian[21] = 0
    jacobian[22] = 0
    jacobian[23] = 0
#
    jacobian = np.array(jacobian).reshape(6,4)
    return jacobian



def jacobian02(q):
#
    jacobian = [0]*24
#
    x0 = sin(q[0])
    x1 = cos(q[1])
    x2 = 0.4*x1
    x3 = x0*x2
    x4 = sin(q[1])
    x5 = sin(q[2])
    x6 = 0.14*x4*x5
    x7 = x0*x6
    x8 = cos(q[2])
    x9 = 0.14*x1*x8
    x10 = x0*x9
    x11 = cos(q[0])
    x12 = -0.14*x1*x5 - 0.14*x4*x8
    x13 = x12 - 0.4*x4
    x14 = -x11*x6 + x11*x9
    x15 = x11*x2 + x14
    x16 = x10 - x7
    x17 = -x0
#
    jacobian[0] = -0.22*x0 - x10 - x3 + x7
    jacobian[1] = x11*x13
    jacobian[2] = x11*x12
    jacobian[3] = 0
    jacobian[4] = 0.22*x11 + x15
    jacobian[5] = x0*x13
    jacobian[6] = x0*x12
    jacobian[7] = 0
    jacobian[8] = 0
    jacobian[9] = -x0*(x16 + x3) - x11*x15
    jacobian[10] = -x0*x16 - x11*x14
    jacobian[11] = 0
    jacobian[12] = 0
    jacobian[13] = x17
    jacobian[14] = x17
    jacobian[15] = 0
    jacobian[16] = 0
    jacobian[17] = x11
    jacobian[18] = x11
    jacobian[19] = 0
    jacobian[20] = 1
    jacobian[21] = 0
    jacobian[22] = 0
    jacobian[23] = 0
#
    jacobian = np.array(jacobian).reshape(6,4)
    return jacobian



def jacobian03(q):
#
    jacobian = [0]*24
#
    x0 = sin(q[0])
    x1 = cos(q[1])
    x2 = 0.4*x1
    x3 = x0*x2
    x4 = cos(q[0])
    x5 = 0.24*sin(q[3])
    x6 = x4*x5
    x7 = sin(q[1])
    x8 = sin(q[2])
    x9 = x7*x8
    x10 = x0*x9
    x11 = 0.14*x10
    x12 = cos(q[2])
    x13 = x1*x12
    x14 = x0*x13
    x15 = 0.14*x14
    x16 = 0.24*cos(q[3])
    x17 = x16*(-x10 + x14)
    x18 = x12*x7
    x19 = x1*x8
    x20 = x16*(-x18 - x19)
    x21 = -0.14*x18 - 0.14*x19 + x20
    x22 = x21 - 0.4*x7
    x23 = x0*x18 + x0*x19
    x24 = x13 - x9
    x25 = x17 + x6
    x26 = x4*x9
    x27 = x13*x4
    x28 = -x0*x5 + x16*(-x26 + x27)
    x29 = -0.14*x26 + 0.14*x27 + x28
    x30 = x2*x4 + x29
    x31 = x18*x4 + x19*x4
    x32 = -x11 + x15 + x25
    x33 = -x0
#
    jacobian[0] = -0.22*x0 + x11 - x15 - x17 - x3 - x6
    jacobian[1] = x22*x4
    jacobian[2] = x21*x4
    jacobian[3] = x20*x23 - x24*x25
    jacobian[4] = x30 + 0.22*x4
    jacobian[5] = x0*x22
    jacobian[6] = x0*x21
    jacobian[7] = -x20*x31 + x24*x28
    jacobian[8] = 0
    jacobian[9] = -x0*(x3 + x32) - x30*x4
    jacobian[10] = -x0*x32 - x29*x4
    jacobian[11] = -x23*x28 + x25*x31
    jacobian[12] = 0
    jacobian[13] = x33
    jacobian[14] = x33
    jacobian[15] = x31
    jacobian[16] = 0
    jacobian[17] = x4
    jacobian[18] = x4
    jacobian[19] = x23
    jacobian[20] = 1
    jacobian[21] = 0
    jacobian[22] = 0
    jacobian[23] = x24
#
    jacobian = np.array(jacobian).reshape(6,4)
    return jacobian



J = {0:jacobian00, 1:jacobian01, 2:jacobian02, 3:jacobian03, }
