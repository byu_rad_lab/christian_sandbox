#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl/frames_io.hpp>
#include <stdio.h>
#include <iostream>
#include <Eigen/Core>
#include <trac_ik/trac_ik.hpp>
#include <fstream>
#include <math.h>
#include "ros/ros.h"
#include "geometry_msgs/Pose.h"
#include "visualization_msgs/InteractiveMarkerFeedback.h"
#include "king_louie/joint_angles_msg.h"
#include "king_louie/joint_angles_msg_array.h" 
#include <stdlib.h>
// #include "low_level_control/vent_valves.h" 

KDL::Chain chain_right;
ros::Publisher pub_jnts; 

namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

float distance_off_target(KDL::Vector desPos, KDL::Vector actPos)
{
    float err_x = desPos.x() - actPos.x();
    float err_y = desPos.y() - actPos.y();
    float err_z = desPos.z() - actPos.z();
    float err = sqrt(err_x*err_x + err_y*err_y + err_z*err_z);
    
    return err;
}

int print_output(int result, int nJoints, KDL::JntArray q_out, KDL::Frame desFrame, KDL::Frame actualFrame)
{
    if(result >= 0)
    {
        std::cout << std::endl;
        std::cout << "Desired Position: " << patch::to_string(desFrame.p) << std::endl;
        std::cout << "Actual Position:  " << patch::to_string(actualFrame.p) << std::endl;
        std::cout << "Error:            " << distance_off_target(desFrame.p, actualFrame.p);   
        std::cout << std::endl << std::endl;
        std::cout << "Success - "; // (acc = " << acc - 0.05 << ")" << std::endl;        
        std::cout << "Output angles: " << std::endl;
        std::cout << "hip " << round(q_out(0)*180/M_PI*100)/100 << std::endl;
        std::cout << "sh1 " << round(q_out(1)*180/M_PI*100)/100 << std::endl;
        std::cout << "pa1 " << round(q_out(2)*180/M_PI*100)/100 << std::endl;
        std::cout << "sh2 " << round(q_out(3)*180/M_PI*100)/100 << std::endl;
        std::cout << "wr1 " << round(q_out(4)*180/M_PI*100)/100 << std::endl;
        std::cout << "pa2 " << round(q_out(5)*180/M_PI*100)/100 << std::endl;
        std::cout << "wr2 " << round(q_out(6)*180/M_PI*100)/100 << std::endl;     
        std::cout << std::endl << std::endl;
    }
    else {std::cout << "Failure" << std::endl;}
}

KDL::Chain build_arm(std::string which_arm)
{
    KDL::Chain chain;

    // DH params: a, alpha, d, theta

    // if (which_arm == "left")
    // {
    //     chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(-.159,1.85,-.066,.011)));  // cortex to intermediate
    //     chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(-.061,.062,.005,1.204)));  // intermediate to shoulder 1
    //     chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(-.022,1.364,.007,-1.285)));  // shoulder 1 to passive
    //     chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(.016,-1.708,.194,-1.316)));  // passive to shoulder 2
    //     chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(-.366,-.038,-.084,1.46)));  // shoulder 2 to wrist 1
    //     chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(-.152,1.644,.033,.06)));  // wrist 1 to wrist 2
    //     chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(.11,-.25,-.012,3.035)));  // wrist 2 to EF
    // }
    if (which_arm == "right")
    {
        chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(-0.5329,0.0324,-0.0330,-0.7169)));  // hip to shoulder 1
        chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(-0.0963,1.5727,0.0199,0.6938)));  // shoulder 1 to passive 1
        chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(-0.0374,-1.5932,0.1076,1.4926)));  //passive 1 to shoulder 2
        chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(-0.3902,-0.1857,0.0588,1.568)));  // shoulder 2 to wrist 1
        chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(-0.0994,-1.5783,-0.0366,1.6111)));  // wrist 1 to passive 2
        chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(0.0903,-1.5459,0.1417,1.4699)));  // passive 2 to wrist 2
        chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(0.1227,-0.0636,-0.107,-1.7129)));  // wrist 2 to EF
    }
    return chain;
}

KDL::JntArray init_jnt_angles(int nJoints)
{
    KDL::JntArray q_init(nJoints);
    //for (int i = 0; i < nJoints; ++i){q_init(i) = 0;}
    
    q_init(0) = 0.0;
    q_init(1) = 0.0;
    q_init(2) = 0.0;
    q_init(3) = 0.0;
    q_init(4) = 0.0;
    q_init(5) = 0.0;
    q_init(6) = 0.0;

    return q_init;
}

KDL::JntArray min_jnt_angles(int nJoints)
{
    KDL::JntArray q_min(nJoints); 
    
    q_min(0) = 0.0;           //hip to sh1
    q_min(1) = -M_PI/2.0;     // sh1 to pas1
    q_min(2) = 0.0;           // pas1 to sh2
    q_min(3) = -M_PI/2.0;     // sh2 to wr1
    q_min(4) = -M_PI/2.0;     // wr1 to pas2
    q_min(5) = 0.0;           // pas2 to wr2
    q_min(6) = -M_PI/2.0;     // wr2 ti EE
    
    

    return q_min;
}

KDL::JntArray max_jnt_angles(int nJoints)
{
    KDL::JntArray q_max(nJoints); 

    q_max(0) = 0.0;           //hip to sh1
    q_max(1) = M_PI/2.0;     // sh1 to pas1
    q_max(2) = 0.0;           // pas1 to sh2
    q_max(3) = M_PI/2.0;     // sh2 to wr1
    q_max(4) = M_PI/2.0;     // wr1 to pas2
    q_max(5) = 0.0;           // pas2 to wr2
    q_max(6) = M_PI/2.0;     // wr2 ti EE
    
    return q_max;
}

KDL::Frame forward_kinematics(KDL::JntArray jntAngles, KDL::Chain chain)
{
    KDL::ChainFkSolverPos_recursive fk(chain);
    KDL::Frame finalFrame;

    // KDL::JntArray jntAngles_m(7);
    // //for (int i = 0; i < nJoints; ++i){q_init(i) = 0;}
    
    // jntAngles_m(0) = 0.0;
    // jntAngles_m(1) = -1.0576;
    // jntAngles_m(2) = 0.0;
    // jntAngles_m(3) = 0.9326;
    // jntAngles_m(4) = 0.3647;
    // jntAngles_m(5) = 0.0;
    // jntAngles(6) = 0.0;

    fk.JntToCart(jntAngles, finalFrame);
    
    return finalFrame;
}

king_louie::joint_angles_msg build_ja_msg(KDL::JntArray q_ik, int nJoints)
{
    king_louie::joint_angles_msg msg;

    float sh1, sh2, elb, wr1, wr2;


    msg.appendage = "Right";

    sh1 = q_ik(1)*180/M_PI;
    sh2 = q_ik(3)*180/M_PI;
    elb = 0.0;
    wr1 = q_ik(4)*180/M_PI;
    wr2 = q_ik(6)*180/M_PI;


    msg.q.push_back(round(sh1*100)/100);
    msg.q.push_back(round(sh2*100)/100);
    msg.q.push_back(round(elb*100)/100);
    msg.q.push_back(round(wr1*100)/100);
    msg.q.push_back(round(wr2*100)/100);

    for (int i = 0; i < nJoints-1; ++i){msg.q_dot.push_back(0);}
    msg.flag = 1;

    return msg;
}

int inverse_kinematics(geometry_msgs::Point coord, geometry_msgs::Quaternion orient, KDL::Chain chain, int nJoints, KDL::JntArray q_init, KDL::JntArray q_min, KDL::JntArray q_max)
{
    KDL::Frame desFrame;
    desFrame.p = KDL::Vector(coord.x, coord.y, coord.z);   // desired position
    desFrame.M = KDL::Rotation::RPY(0, 0, 0);

    float acc_p = 1.0e-2;
    float acc_o = 1.0e6;   // this value can be bigger than 1. 
    KDL::JntArray q_ik(nJoints);
    

    KDL::Vector vel(acc_p, acc_p, acc_p);
    KDL::Vector rot(acc_o, acc_o, acc_o);
    KDL::Twist bound(vel, rot);
    TRAC_IK::TRAC_IK ik(chain, q_min, q_max);
    
    
    int result = ik.CartToJnt(q_init, desFrame, q_ik, bound);
	std::cout << "result = " << result << "\n";
	int i = 1;
	while(result <= 0) //keep running inverse kinematics until the solution is found
    {
    	result = ik.CartToJnt(q_init, desFrame, q_ik, bound);
    	std::cout << "result = " << result << "\n";
    	i = i + 1;
    	if(i > 30){
    		exit(1);
    	}
    }
    
    //std::cout << "acc_p = " << acc_p << "\n";
    //std::cout << "acc_o = " << acc_o << "\n";
    
    king_louie::joint_angles_msg ja_msg = build_ja_msg(q_ik, nJoints);
    king_louie::joint_angles_msg_array ja_msg_ar;
    ja_msg_ar.joint_angles.push_back(ja_msg);

    pub_jnts.publish(ja_msg_ar);

    KDL::Frame finalFrame = forward_kinematics(q_ik, chain);    
        
    print_output(result, nJoints, q_ik, desFrame, finalFrame);
}


void send_default_pose(std::string which_hand)
{
    king_louie::joint_angles_msg ja_msg;

    ja_msg.q.push_back(-40);
    ja_msg.q.push_back(0);
    ja_msg.q.push_back(0);
    ja_msg.q.push_back(0);

    if (which_hand == "kl_right")
        {
            ja_msg.appendage = "Right";
            ja_msg.q.push_back(0);
        }
    else if (which_hand == "kl_left")
        {
            ja_msg.appendage = "Left";
        }

    for (int i = 0; i < 6; ++i){ja_msg.q_dot.push_back(0);}
    ja_msg.flag = 1;

    king_louie::joint_angles_msg_array ja_msg_ar;
    ja_msg_ar.joint_angles.push_back(ja_msg);

    pub_jnts.publish(ja_msg_ar);
}

// void toggle_control(bool venter)
// {
//     low_level_control::vent_valves msg;
//     msg.vent = venter;
//     pub_vent.publish(msg);
// }

void callback(const geometry_msgs::Pose& msg)
{
    int nJoints;
    KDL::JntArray q_init, q_min, q_max;

    // send the default pose to king louie
    //send_default_pose("kl_right");
    
    geometry_msgs::Point coord = msg.position;
    geometry_msgs::Quaternion orient = msg.orientation;
    //std::cout << "coord = " << coord;

    //I took this part from below and commented it all out.
    nJoints = chain_right.getNrOfJoints();
    q_init = init_jnt_angles(nJoints);
    q_min = min_jnt_angles(nJoints);
    q_max = max_jnt_angles(nJoints);
    inverse_kinematics(coord, orient, chain_right, nJoints, q_init, q_min, q_max);
}

int main(int argc, char **argv)

{
    chain_right = build_arm("right");
  
    ros::init(argc, argv, "desired_joint_angles");
    ros::NodeHandle n;
    pub_jnts = n.advertise<king_louie::joint_angles_msg_array>("SetJointGoal",1000);
 
    ros::Subscriber sub_pose = n.subscribe("desired_position", 1000, callback);
    
    ros::spin();
}


