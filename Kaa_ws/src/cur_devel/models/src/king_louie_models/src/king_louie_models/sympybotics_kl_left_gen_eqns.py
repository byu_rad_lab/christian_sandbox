#
# Copyright (c) 2013, Georgia Tech Research Corporation
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the Georgia Tech Research Corporation nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY GEORGIA TECH RESEARCH CORPORATION ''AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL GEORGIA TECH BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# \\ author Marc Killpack (Healthcare Robotics Lab, Georgia Tech.)
# \\ adviser Charlie Kemp (Healthcare Robotics Lab, Georgia Tech.)
# based on some sample code from Cristovao Sousa and his sympybotics library


import sympy
import sympybotics
#import symcode
import math

# used for differentiating code with respect to x
def code_diff( code, x ):
    ivars = code[0]
    exprs = code[1]
    ivars_diff = []
    exprs_diff = []

    d_dx = dict() # to store derivative symbols
    d_dx[x] = sympy.sympify(1) # the derivative of a variable relatively to itself is 1

    for ivar,f in ivars:

        # chain rule for composite functions
        # df/dx = sum_vi dvi/dx*dvi/dx:
        df_dx = sympy.sympify(0)
        for v in f.free_symbols:
            if v in d_dx: # if v is not in d_dx it is because its derivative is 0
                df_dx += f.diff(v) * d_dx[v] # df/dx += df(v)/dv * dv(x)/dx

        d_ivar = sympy.Symbol('d'+str(ivar),real=True)

        d_dx[ivar] = d_ivar

        ivars_diff.append( (ivar,f) )
        ivars_diff.append( (d_ivar,df_dx) )

    for f in exprs:
        df_dx = sympy.sympify(0)
        for v in f.free_symbols:
            if v in d_dx:
                df_dx += f.diff(v) * d_dx[v]
        exprs_diff.append( df_dx )

    return (ivars_diff,exprs_diff)


pi = sympy.pi
q = sympybotics.robotdef.q

# defining constants for the offset dh params from the body to the first joint
x_offset = 0.0
y_offset = 0.0
z_offset = 0.0
 
### First, create a robot object:
### list of tuples with Denavit-Hartenberg parameters (alpha, a, d, theta)
#Standard DH PARAMS
#             alpha    a        d   theta
dh_params = [(-pi/2,  0.22,    0.0,  q),
             (0,      0.40,    0.0,  q),
             (pi/2,   0.14,    0.0,  q),
             (0,      0.24,    0.0,  q)]

#rbtdef = sympybotics.RobotDef('King Louie', dh_params , 'modified')
rbtdef = sympybotics.RobotDef('King Louie', dh_params , 'standard')


# defining which friction model to use
rbtdef.frictionmodel = {'viscous'} #'simple' # options are 'simple' and None, defaults to None

# printing the definition of which variables are used as dynamic parameters (i.e. mass, center of mass, etc.)
print((rbtdef.dynparms()))
rbt = sympybotics.RobotDynCode(rbtdef)

# the partial derviative of the mass matrix with respect to each joint, this is used to calculate Coriolis matrix
#dMdq = []
#for i in xrange(rbt.dof):
#    dMdq.append(code_diff(rbt.M_code, rbt.rbtdef.q[i]))


arm = 'kl_left'


# this is an ugly way to write everything to python files that are then executable/callable.
# the same can be done for C code
f_handle = open('./'+arm+'_dynamics.py', 'w+')
#f_dMdq_handle = open('./'+arm+'_dMdq_func.py', 'w+')
print("from math import sin, cos", file=f_handle)
print("import numpy as np\n\n\n", file=f_handle)

#print >> f_dMdq_handle, "from math import sin, cos"

print(sympybotics.robotcodegen.robot_code_to_func( 'python', rbt.invdyn_code, 'tau_out', 'tau', rbtdef), file=f_handle)
print('\n\n\n', file=f_handle)
print(sympybotics.robotcodegen.robot_code_to_func( 'python', rbt.H_code, 'regressor_out', 'regressor', rbtdef ), file=f_handle)
print('\n\n\n', file=f_handle)
print(sympybotics.robotcodegen.robot_code_to_func( 'python', rbt.M_code, 'M_out', 'M',  rbtdef), file=f_handle)
print('\n\n\n', file=f_handle)
print(sympybotics.robotcodegen.robot_code_to_func( 'python', rbt.c_code, 'c_out', 'c', rbtdef), file=f_handle)
print('\n\n\n', file=f_handle)
print(sympybotics.robotcodegen.robot_code_to_func('python', rbt.C_code, 'C_out', 'C', rbtdef), file=f_handle)
print('\n\n\n', file=f_handle)
#print >> f_handle, sympybotics.robotcodegen.robot_code_to_func( 'python', rbt.f_code, 'f_out', 'f', rbtdef)
#print >> f_handle, '\n\n\n'
print(sympybotics.robotcodegen.robot_code_to_func( 'python', rbt.g_code, 'g_out', 'g', rbtdef), file=f_handle)
print('\n\n\n', file=f_handle)


#jac_string = sympybotics.robotcodegen.robot_code_to_func('python', joint_jac_code, 'jacobian', 'jacobian' + str(i).zfill(2), rbtdef)

#print >> f_dMdq_handle, "def dMdq(parms, q, jt_num):\n"
#for i in xrange(len(dMdq)):
#    print "type is :", type(dMdq[i])
#    print "dMdq[i] is :\n", dMdq[i]
#    cor_int = sympy.ces(dMdq[i])
#    func_buf = sympybotics.robotcodegen.robot_code_to_func( 'python',  dMdq[i], 'dMdq_out'+str(i), 'dMdq'+str(i), rbtdef )
#    if i != 0:
#        func_buf = func_buf.replace('['+str(i-1)+']_out', str(i)+'_out')
#    func_buf = func_buf.replace('def dMdq'+str(i)+'( parms, q ) :', '    if jt_num == '+str(i)+':')
#    func_buf = func_buf.replace('\n', '\n    ')
#    print >> f_dMdq_handle, func_buf

print('\n\n\n', file=f_handle)
#print >> f_handle, 'from coriolis import *'
print('\n\n\n', file=f_handle) 
print('#dynparms = '+str(rbtdef.dynparms()), file=f_handle)
 

#f_dMdq_handle.close()
f_handle.close()


offsets = str(x_offset)+', '+str(y_offset)+', '+str(z_offset)
f_kin = open('./'+arm+'_kinematics.py', 'w+')
print("from math import sin, cos", file=f_kin)
print("from offset_util import offset_and_reshape", file=f_kin)
print("import numpy as np", file=f_kin)
print("pi = np.pi\n\n\n", file=f_kin)
fk_dict = "FK = {"
for i in range(len(rbt.geo.T)):
    joint_fk_code = sympy.cse(rbt.geo.T[i])
    fk_string = sympybotics.robotcodegen.robot_code_to_func('python', joint_fk_code, 'pose', 'joint_fk' + str(i).zfill(2), rbtdef)
    fk_list_string = fk_string.split('\n')
    fk_list_string.insert(-1, '    pose = offset_and_reshape(pose,'+str(x_offset)+','
                           +str(y_offset)+','
                           +str(z_offset)+')')
    fk_final = "\n".join(fk_list_string)
    print(fk_final + '\n\n\n', file=f_kin)
    fk_dict = fk_dict+str(i)+":joint_fk"+str(i).zfill(2)+", "

print(fk_dict+"}\n\n\n", file=f_kin)

jac_dict = "J = {"
for i in range(len(rbt.kin.J)):
    joint_jac_code = sympy.cse(rbt.kin.J[i])
    jac_string = sympybotics.robotcodegen.robot_code_to_func('python', joint_jac_code, 'jacobian', 'jacobian' + str(i).zfill(2), rbtdef)

    jac_list_string = jac_string.split('\n')
    jac_list_string.insert(-1, '    jacobian = np.array(jacobian).reshape(6,'+str(rbt.dof)+')')
    jac_final = "\n".join(jac_list_string)
    print(jac_final + '\n\n\n', file=f_kin)
    jac_dict = jac_dict + str(i)+":jacobian"+str(i).zfill(2)+", "

print(jac_dict+"}", file=f_kin)

f_kin.close()

