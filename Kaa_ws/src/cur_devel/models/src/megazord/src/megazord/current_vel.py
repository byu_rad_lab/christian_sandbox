#!/usr/bin/env python

from megazord_kdl import MegazordKDL
import numpy as np

class CurVel():
    ''' This class gets the current velocity given current joint angles and joint velocities by calling on the MegazordKDL class. Input a limb, current joint angles, and current joint velocities'''
    def __init__(self,limb,jt_angles,jt_velocities):
        MK = MegazordKDL(limb,jt_angles)
        self.J = MK.calc_jacobian(jt_angles)
        self.xdot = self.J.dot(jt_velocities)
        self.R,self.pos,self.Frame = MK.megazord_fk(jt_angles)
