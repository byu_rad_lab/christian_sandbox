#!/usr/bin/env python

# Date:    Oct. 17, 2016
# Author:  Erich Mielke
# Project: Variable Impedance Control

from PyKDL import *
from math import *
import numpy as np

class MegazordKDL():
    ''' This class is for Megazord (Base + Baxter). Inputting a limb ('right','left') and joint angles for said limb will build a PyKDL chain using DH parameters and you will be able to run Jacobian, Forward Kinematics so far.'''
    def __init__(self,limb,jt_angles):

        # # Set up DH Parameters RPPRRRRRRR
        # a3 = 0.219645 + sqrt(2) / 2 * 0.055695
        # a4 = .069
        # a6 = .069
        # a8 = .01
        # rz3 = -pi/4
        # d3 = 0.024645 + sqrt(2) / 2 * 0.055695
        # d4 = 0.118588 + 0.011038 + .27035
        # d6 = .36435
        # d8 = .37429
        # d10 = .229525

        # # DH Parameter Segments (a,alpha,d,theta) RPPRRRRRRR
        # if limb == 'left':
        #     s0 = Segment(Joint(Joint.RotZ),Frame().DH(0, -pi/2, 0, 0))
        #     s1 = Segment(Joint(Joint.TransZ),Frame().DH(0, -pi/2, 0, -pi/2))
        #     s2 = Segment(Joint(Joint.TransZ),Frame().DH(a3, -pi/2, d3, -pi/2))
        #     s3 = Segment(Joint(Joint.RotZ),Frame().DH(a4, -pi/2, d4, rz3))
        #     s4 = Segment(Joint(Joint.RotZ),Frame().DH(0, pi/2, 0, pi/2))
        #     s5 = Segment(Joint(Joint.RotZ),Frame().DH(a6, -pi/2, d6, 0))
        #     s6 = Segment(Joint(Joint.RotZ),Frame().DH(0, pi/2, 0, 0))
        #     s7 = Segment(Joint(Joint.RotZ),Frame().DH(a8, -pi/2, d8, 0))
        #     s8 = Segment(Joint(Joint.RotZ),Frame().DH(0, pi/2, 0, 0))
        #     s9 = Segment(Joint(Joint.RotZ),Frame().DH(0, 0, d10, 0))
        # elif limb == 'right':
        #     s0 = Segment(Joint(Joint.RotZ),Frame().DH(0, -pi/2, 0, 0))
        #     s1 = Segment(Joint(Joint.TransZ),Frame().DH(0, -pi/2, 0, -pi/2))
        #     s2 = Segment(Joint(Joint.TransZ),Frame().DH(-a3, -pi/2, d3, -pi/2))
        #     s3 = Segment(Joint(Joint.RotZ),Frame().DH(a4, -pi/2, d4, 3 * rz3))
        #     s4 = Segment(Joint(Joint.RotZ),Frame().DH(0, pi/2, 0, pi/2))
        #     s5 = Segment(Joint(Joint.RotZ),Frame().DH(a6, -pi/2, d6, 0))
        #     s6 = Segment(Joint(Joint.RotZ),Frame().DH(0, pi/2, 0, 0))
        #     s7 = Segment(Joint(Joint.RotZ),Frame().DH(a8, -pi/2, d8, 0))
        #     s8 = Segment(Joint(Joint.RotZ),Frame().DH(0, pi/2, 0, 0))
        #     s9 = Segment(Joint(Joint.RotZ),Frame().DH(0, 0, d10, 0))

        # # Build Chain
        # self.chain = Chain()
        # self.chain.addSegment(s0)
        # self.chain.addSegment(s1)
        # self.chain.addSegment(s2)
        # self.chain.addSegment(s3)
        # self.chain.addSegment(s4)
        # self.chain.addSegment(s5)
        # self.chain.addSegment(s6)
        # self.chain.addSegment(s7)
        # self.chain.addSegment(s8)
        # self.chain.addSegment(s9)


        # DH setup for PPRRRRRRRR
        d0 = 0.118588 + 0.011038
        deg = pi/2 - 78.472777347500042 * pi/180.0
        d3 = 270.35/1000.0 + d0
        d5 = 364.35/1000.0
        d7 = 374.29/1000.0
        d9 = 229.525/1000.0
        a2 = 0.285548293244685
        a3 = 69.0/1000.0
        a5 = 69.0/1000.0
        a7 = 10.0/1000.0
        
        # DH Parameter Segments (a,alpha,d,theta) PPRRRRRRRR
        if limb == 'left':
            s0 = Segment(Joint(Joint.TransZ),Frame().DH(0, -pi/2, 0, 0))
            s1 = Segment(Joint(Joint.TransZ),Frame().DH(0, -pi/2, 0, pi/2))
            s2 = Segment(Joint(Joint.RotZ),Frame().DH(a2, 0, 0, 3*pi/2 - deg))
            s3 = Segment(Joint(Joint.RotZ),Frame().DH(a3, -pi/2, d3, -pi/4 + deg))
            s4 = Segment(Joint(Joint.RotZ),Frame().DH(0, pi/2, 0, pi/2))
            s5 = Segment(Joint(Joint.RotZ),Frame().DH(a5, -pi/2, d5, 0))
            s6 = Segment(Joint(Joint.RotZ),Frame().DH(0, pi/2, 0, 0))
            s7 = Segment(Joint(Joint.RotZ),Frame().DH(a7, -pi/2, d7, 0))
            s8 = Segment(Joint(Joint.RotZ),Frame().DH(0, pi/2, 0, 0))
            s9 = Segment(Joint(Joint.RotZ),Frame().DH(0, 0, d9, 0))
        elif limb == 'right':
            s0 = Segment(Joint(Joint.TransZ),Frame().DH(0, -pi/2, 0, 0))
            s1 = Segment(Joint(Joint.TransZ),Frame().DH(0, -pi/2, 0, pi/2))
            s2 = Segment(Joint(Joint.RotZ),Frame().DH(a2, 0, 0, pi/2 + deg))
            s3 = Segment(Joint(Joint.RotZ),Frame().DH(a3, -pi/2, d3, pi/4 - deg))
            s4 = Segment(Joint(Joint.RotZ),Frame().DH(0, pi/2, 0, pi/2))
            s5 = Segment(Joint(Joint.RotZ),Frame().DH(a5, -pi/2, d5, 0))
            s6 = Segment(Joint(Joint.RotZ),Frame().DH(0, pi/2, 0, 0))
            s7 = Segment(Joint(Joint.RotZ),Frame().DH(a7, -pi/2, d7, 0))
            s8 = Segment(Joint(Joint.RotZ),Frame().DH(0, pi/2, 0, 0))
            s9 = Segment(Joint(Joint.RotZ),Frame().DH(0, 0, d9, 0))


        # Build Chain
        self.chain = Chain()
        # the segment below adds the initial static transform
        self.chain.addSegment(Segment(Joint(Joint.None),Frame(Rotation.RotY(pi/2))))
        # these are the chains in order of joints
        self.chain.addSegment(s0)
        self.chain.addSegment(s1)
        self.chain.addSegment(s2)
        self.chain.addSegment(s3)
        self.chain.addSegment(s4)
        self.chain.addSegment(s5)
        self.chain.addSegment(s6)
        self.chain.addSegment(s7)
        self.chain.addSegment(s8)
        self.chain.addSegment(s9)

        # Joints
        self.nJoints = len(jt_angles)
        self.jangles_var(jt_angles)

        # Variables
        self.J = np.mat(np.zeros((6,len(jt_angles))))
        self.R = Rotation()
        self.pos = Vector()

        # Joint Limits (Arbitrary for joints 1 and 2, since these are infinite essentially)
        self.q_min = JntArray(self.nJoints)
        self.q_min[0] = -2.0
        self.q_min[1] = -2.0
        self.q_min[2] = -3.14
        self.q_min[3] = -1.7016
        self.q_min[4] = -2.147
        self.q_min[5] = -3.0541
        self.q_min[6] = -.05
        self.q_min[7] = -3.059
        self.q_min[8] = -1.5707
        self.q_min[9] = -3.059
        
        self.q_max = JntArray(self.nJoints)
        self.q_max[0] = 2.0
        self.q_max[1] = 2.0
        self.q_max[2] = 3.14
        self.q_max[3] = 1.7016
        self.q_max[4] = 1.047
        self.q_max[5] = 3.0541
        self.q_max[6] = 2.618
        self.q_max[7] = 3.059
        self.q_max[8] = 2.094
        self.q_max[9] = 3.059


    # Joint Angle Variable
    def jangles_var(self,jt_angles):
        ''' Updates the jAngles variable'''
        self.jAngles = JntArray(self.nJoints)
        for i in range(self.nJoints):
            self.jAngles[i] = jt_angles[i]

    # Calculate Jacobian
    def calc_jacobian(self,jt_angles):
        ''' Calculates Jacobian for certain joint angles. Returns Jacobian as numpy matrix'''
        jnt2jac = ChainJntToJacSolver(self.chain)
        jacobian = Jacobian(self.nJoints)
        self.jangles_var(jt_angles)
        jnt2jac.JntToJac(self.jAngles,jacobian)
        self.J = np.mat(np.zeros((jacobian.rows(),jacobian.columns())))
        for i in range(jacobian.rows()):
            for j in range(jacobian.columns()):
                self.J[i,j] = jacobian[i,j]

        return self.J

    # Forward Kinematics
    def megazord_fk(self,jt_angles):
        ''' Solves Forward Kinematics for certain joint angles. Returns Rotation Matrix and position Vector (PyKDL objects) '''
        fk = ChainFkSolverPos_recursive(self.chain)
        finalFrame = Frame()
        self.jangles_var(jt_angles)
        fk.JntToCart(self.jAngles,finalFrame)
        self.R = finalFrame.M
        self.pos = finalFrame.p

        return self.R,self.pos,finalFrame
