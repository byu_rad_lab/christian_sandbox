%% RLforTICTACTOE

% X goes first (We are X)
% X = 1; O=0; -= 2;
blankstate = [2 2 2; 2 2 2; 2 2 2];

for i = 1:9
    state(:,:,i) = blankstate;
    row=fix(i/3)+1;
    column=mod(i,3);
    if column==0
        column=3;
    end
    if mod(i,3)==0
        row=i/3
    end
    
    state(row,column,i) = 1;
    state(:,:,i)
    i
end
adjust = 0;


for i = 1:9
    for j=1:9
    
    if i == j
        adjust = adjust+1;
        continue
    else
        
        stateidx=i*9+j-adjust;
        state(:,:,stateidx) = state(:,:,i);
    end
    state(:,:,stateidx) = state(:,:,i);
    row=fix(j/3)+1;
    column=mod(j,3);
    if column==0
        column=3;
    end
    if mod(j,3)==0
        row=j/3;
    end
    
    state(row,column,stateidx) = 0;
    state(:,:,stateidx)
    end
end

